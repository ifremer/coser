.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Foire aux questions
===================

.. contents::

Au lancement de l'application JavaWebStart le message "Impossible de lancer l'application" apparaît
---------------------------------------------------------------------------------------------------

Si le détail de l'erreur cite une erreur de certificats, essayez de mettre à
jour Java avant de recommencer.


L'application refuse de se lancer ou bloque lors du chargement ou la vérification des données
---------------------------------------------------------------------------------------------

Il s'agit certainement d'une erreur mémoire. Par défaut, Coser se lance en
utilisant 1Go de mémoire. Cependant, si une telle quantité de mémoire n'est pas
disponible, l'application ne se lancera pas. Si la mémoire est trop faible
et n'arrivera pas a traiter toutes les données.

Il est possible de configurer la mémoire utilisée en modifiant les scripts
coser.bat (windows) et coser (linux) ou le fichier launch.jnlp (webstart).

Dans ces scripts, il suffit ensuite d'ajuster la quantité de mémoire à utiliser.

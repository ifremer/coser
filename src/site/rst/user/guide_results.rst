.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Résultats RSufi
===============

Un fois que les données de votre sélection ont été utilisées pour faire
tourner RSufi, celui ci vous fournit des résultats qu'il est possible
d'ajouter dans la liste des résultats d'une sélection.

Pour ce faire, ré-ouvrez la sélection concernée via le menu *Données/Sélection/<nomdelaselection>*
et sélectionnez le troisième onglet ``RSufi``.

.. image:: ../images/fr/25-selectionresults.png

Cette fois ci, c'est la seconde partie de l'interface qui nous intéresse. Elle
récapitule la liste des résultats actuellement enregistré dans Coser.

Pour ajouter un nouveau résultat, cliquez sur le bouton ``Ajouter un résultat``.

.. image:: ../images/fr/26-selectionaddresult.png

Un résultat est composé de :

  * un nom unique parmi les résultats de la sélection (obligatoire et composé
    uniquement de caractères alphanumériques)
  * la version de rsufi avec laquelle le résultat a été obtenu (obligatoire)
  * la date d'ajout du résultat (obligatoire)
  * le fichier de données ``estcomind`̀  (obligatoire)
  * le fichier de données ``estpopind`` (obligatoire)

À cela s'ajoute des options facultatives :

  * la zone d'application (utilisé lors de la publication de résultat sur
    le site internet pour placé un résultat dans un zone définie)
  * le répertoire de cartes (utilisé par le site internet pour afficher
    les cartes de distribution)
  * publiable (utilisé par les administrateurs pour différencier les résultats
    publiable des autres résultats)
  * autre fichier (fichier supplémentaire devant être conservé par Coser)

Une fois le résultat paramétré sans erreur, cliquer sur ``Ajouter le résultat``
pour valider l'action. Ce résultat pourra ensuite être sélectionné par les
administrateur de Coser en vue d'être publié sur le site internet :
http://www.ifremer.fr/SIH-indices-campagnes/

.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Format des tables
=================

Coser a besoin de 4 tables par projet :

  * captures
  * strates
  * tailles
  * traits

Chaque table doit être formaté suivant le format CSV en utilisant le séparateur
';' (point-virgule) et comporter des noms de colonne définit (anglais ou français)
**dans le même ordre**.


Détails des 4 tables
--------------------

captures
~~~~~~~~

En-tête français::

  Campagne;Annee;Trait;Espece;Nombre;Poids

En-tête anglais::

  Survey;Year;Haul;Species;Number;Weight

Exemple (captures.csv)::

  Campagne;Annee;Trait;Espece;Nombre;Poids
  EVHOE_TOT;1997;B1008;ALLO;160.00;0.47
  EVHOE_TOT;1997;B1008;ARNOLAT;1.00;0.01
  EVHOE_TOT;1997;B1008;ARNOTHO;1.00;0.03


strates
~~~~~~~

En-tête français::

  Campagne;Strate;Surface

En-tête anglais::

  Survey;Stratum;Surface

Exemple (strates.csv)::

  Campagne;Strate;Surface
  EVHOE_TOT;Gn1;8201.69199214
  EVHOE_TOT;Gn2;11771.068813
  EVHOE_TOT;Gn3;17327.206597099997


tailles
~~~~~~~

En-tête français::

  Campagne;Annee;Trait;Espece;Sexe;Maturite;Longueur;Nombre;Poids;Age

En-tête anglais::

  Survey;Year;Haul;Species;Sex;Maturity;Length;Number;Weight;Age

Exemple (tailles.csv)::

  Campagne;Annee;Trait;Espece;Sexe;Maturite;Longueur;Nombre;Poids;Age
  EVHOE_TOT;1997;B1008;ARNOLAT;i;NA;11.00;1.00;;NA
  EVHOE_TOT;1997;B1008;ARNOTHO;i;NA;13.00;1.00;;NA
  EVHOE_TOT;1997;B1008;BOOPBOO;i;NA;14.00;1.00;0.02;NA


traits
~~~~~~

En-tête français::

  Campagne;Annee;Trait;Mois;Strate;SurfaceBalayee;Lat;Long;ProfMoy

En-tête anglais::

  Survey;Year;Haul;Month;Stratum;SweptSurface;Lat;Long;Depth

Exemple (traits.csv)::

  Campagne;Annee;Trait;Mois;Strate;SurfaceBalayee;Lat;Long;ProfMoy
  EVHOE_TOT;1997;B1008;10;Gs2;0.06;43.75;-1.50;45.50
  EVHOE_TOT;1997;B1010;10;Gs1;0.06;43.81;-1.45;31.00
  EVHOE_TOT;1997;B1012;10;Gs3;0.06;43.89;-1.73;115.00

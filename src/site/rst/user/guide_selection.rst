.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Sélection
=========

L'étape de sélection consiste a sélectionner les listes d'espèces suivante
en paramètres d'entrée de RSufi :

  * L1 : Liste des espèces sélectionnées
  * L2 : Liste des espèces de L1 respectant un filtre d'occurrence/densité
  * L3 : Liste des espèces de L2 avec des tailles pour toutes les années
  * L4 : Liste des espèces de L3 avec des maturités


Création de la sélection
------------------------

La première étape consiste a définir les paramètres de la sélection pour Coser.

Pour cela, utiliser le menu ``Sélections/Nouvelle sélection``

.. image:: ../images/fr/27-selectionmenu0.png

**Note**: Ce menu n'est disponible que si l'étape de contrôle est été validée.

.. image:: ../images/fr/20-selectiondetail.png

Le nom est obligatoire et ne doit contenir que des caractères alphanumériques.

Il faut ensuite sélectionner les années sur lesquelles porterons les données
de la sélection. Les données des 4 tables seront restreinte aux années
sélectionnées.

.. image:: ../images/fr/21-selectionyears.png

Il faut ensuite sélectionner les strates pour lesquelles on souhaite
sélectionner des espèces.

.. image:: ../images/fr/22-selectionstrata.png

Dans cette étape il est également possible d'afficher les données
de l'effort d’échantillonnage est cliquant sur l’icône en forme de table.

Arrive enfin l'étape de sélection des espèces à inclure dans la liste L1. Cette
étape récapitule la liste de toutes les espèces disponible suite aux choix
des années et des strates.

.. image:: ../images/fr/23-selectionspecies.png

La liste ``Espèces filtrées`` correspond à la liste de toutes les espèces. Cette
liste est filtrable par les types de la liste ``Filtrer par type``.

Pour ajouter une espèce dans la liste L1, il faut la sélectionner et cliquer
sur le bouton ``flèche droite``.

Les deux listes de cette étape comporte de menu contextuel permettant de :

  * fusionner des espèces
  * générer les graphiques de comparaison captures/tailles
  * etc...

.. image:: ../images/fr/28-selectionmenu1.png


Rapport
-------

Il est possible de générer un rapport comportant toutes les informations de
la sélection est cliquant sur le bouton ``Rapport de sélection``. Ce rapport
est au format html et doit s'ouvrir dans votre navigateur pour être
visualisé et imprimé.


Listes des espèces
------------------

Une fois la liste L1 définie, il faut sauvegarder la sélection pour
que l'onglet ``Listes des espèces`` devienne actif.

.. image:: ../images/fr/24-selectionlists.png

Dans cet écran :

  * La liste L1 correspond à la liste défini lors de l'étape précédente
  * La liste L2 correspond à la liste des espèces filtrées par le filtre
    du pourcentage minimum d'occurrence et du nombre d'individu minimum
    au km2
  * La liste L3 récapitule parmi les espèces sélectionnées de la liste L2,
    celles qui ont des tailles pour toutes les années
  * enfin la liste L4 récapitule, parmi les espèces sélectionnées de la
    liste L3, celles qui ont des maturités.

À noter:

  * seule les espèces **sélectionnée** (en surbrillance) sont prise en compte
  * si vous modifier la sélection par défaut proposé par le logiciel, vous
    devez justifier votre choix en renseignant un commentaire en dessous
    de la liste concernée

Une fois les 4 listes d'espèces définie, vous pouvez finaliser la sélection
en la validant. Pour cela cliquer sur le bouton ``Valider la sélection``.


Export des données en entrée de RSUfi
-------------------------------------

Une fois les 4 listes définie et la sélection validée, le troisième onglet
"RSUfi" devient actif.

.. image: ../images/fr/25-selectionresults.png

Dans cet écran, vous pouvez exporter les fichiers nécessaires à l’exécution
de RSufi. Attention cependant, Coser ne pilote pas RSUfi, il ne fait que
générer les fichiers nécessaire à son exécution.

Le dossier généré contient:

  * les 4 tables de données (avec seulement les années sélectionnées lors
    de l'étape de sélection)
  * un fichier détaillant les 4 listes d’espèces (L1, L2, L3, l4)
  * un fichier d'information relatif au projet


Résultat
--------

Une fois que RSufi a été exécute avec les paramètres de votre sélection,
vous pouvez passer à l'étape d'`enregistrement des résultats`_.

.. _enregistrement des résultats: guide_results.html

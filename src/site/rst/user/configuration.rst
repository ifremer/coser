.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Configuration
=============

Avant d'utiliser Coser, il faut le configurer, notament conernant les
fichier non fournit avec coser.

Un assistant de configuration est disponible depuis le menu
*Options/Configuration*

Chemins
-------

.. image:: ../images/fr/90-configurationpath.png

Dans cette fenêtre, il est possible de configurer les options:

coser.projects.directory
  Dossier où sont stockés les projets de Coser.

coser.validator.directory
  Dossier où sont stockés les fichiers de contrôle dans le cas où
  l'utilisateur souhaite les personnaliser.

coser.reference.species
  Emplacement du fichier référentiel taxonomique.

coser.reference.typeSpecies
  Emplacement du fichier contenant les codes des types d’espèces.

coser.web.frontend
  Adresse du site web SIH indice campagne (utilisé par les administrateurs
  pour la publication des résultats).

coser.web.zones.file
  Emplacement du fichier définissant la liste des zones d'applications auxquelles
  sont affectés les résultats.


Configuration de l'étape de contrôle
------------------------------------

.. image:: ../images/fr/91-configurationvalues.png

Cette fenêtre sert a ajusté certaines valeurs utilisé lors de l'étape de
contrôle.

coser.control.nobsmin
  Nombre minimal d'individu observé.

coser.control.diffcatchlength
  Pourcentage de différences accepté entre la somme des nombres d'individu
  capturé dans le fichier capture et la même somme dans le fichier taille.

coser.control.typeFish
  Nom du type d’espèce qui correspond aux poissons. Utilisé par certains
  contrôle pour exclure certaines espèces.

coser.selection.occurrenceFilter
  Pourcentage par défaut de l’occurrence minimum utilisé par le filtre de
  l'étape de sélection

coser.selection.densityFilter
  Pourcentage par défaut de la densité utilisé par le filtre de l'étape
  de sélection

.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Contrôle
========

L'étape de contrôle consiste à détecter les erreurs présentes sur les 4 tables
de données et éventuellement à les corriger si c'est possible.

Une fois un projet chargé dans l'application, utilisez le menu *Données/Contrôle*
pour accéder à l'interface de contrôle.

.. image:: ../images/fr/10-controlmain.png

Vérification des données
------------------------

Dans cette étape, la première action a effectuer est de lancer une vérification
globale des données en cliquant sur le bouton ``Vérifier les données``.

Coser va entreprendre une longue séries de contrôles :

  * ligne par ligne sur les 4 tables de données
  * vérification par croisement de fichiers
  * vérification par croisement avec le référentiel taxonomique

À la fin des 10 étapes de vérification, un arbre de toutes les erreurs
s'affiche avec toutes les erreurs détectées.

Pour plus de détails concernant les erreurs : `liste des contrôles`_.

.. _liste des contrôles: guide_listcontrols.html

Corrections des données
-----------------------

Lors de la sélection d'une erreur, Coser affiche la ligne concernée par l'erreur
vous permettant d'apporter une correction.
Les lignes en rouge correspondent à des champs en erreurs. La partie droite
détaille les messages d'erreurs.

Dans l'exemple précédant, l'année est manquante, l'utilisateur peut l'ajouter
à la main et valider la modification.


Corrections de masse
--------------------

Dans le cas où une erreur concerne plusieurs lignes:

  * soit l'erreur est due à une erreur d'export des 4 tables et il est
    préférable de refaire l'export en amont, la correction se révérant impossible
    depuis coser
  * soit l'erreur est corrigeable

Le tableau affichant toute les données comporte un menu contextuel permettant:

  * de supprimer plusieurs lignes sélectionné
  * de chercher un motif et de le remplacer par un autre

.. image:: ../images/fr/11-controlmenu1.png

Par exemple, dans le cas ou tous les nombres comporterais une virgule (,) au
lieu d'un point (.), il est possible de remplacer un la virgule par un point
pour toutes les lignes sur une colonne en une seule opération.


Graphique
---------

Coser est également capable de générer des graphiques pour faciliter
la prise de décision (notamment concernant les warning détectés par la
vérification globale).

Le bouton ``Comparaison Captures/Tailles`` affiche une interface où un
graphique est générée pour chaque espèce. Les données correspondent à la somme
des ``Nombre``s dans les fichiers captures et tailles par année.
Une différence peut traduire une erreur dans les données d'origines.

.. image:: ../images/fr/13-controlgraphdiff.png

Le bouton ``Strutures en taille`` affiche une interface un peu plus compliquée
mais permettant de générer des graphiques sur des sous ensembles de données
sélectionnés.

Il est par exemple possible d'afficher la structure en taille mais seulement
pour une strate, ou seulement pour une espèce, de faire la somme...

.. image:: ../images/fr/14-controlgraphlength.png


Rapport
-------

Dans l'étape de contrôle, il est possible de générer deux rapports.

Rapport de modification
~~~~~~~~~~~~~~~~~~~~~~~

Le rapport des modifications, accessible via le bouton ``Rapport des
modifications``, est généré sous forme de rapport HTML et doit normalement
s'ouvrir dans un navigateur internet. Vous pouvez à partir de ce navigateur
visualisé ou imprimer ce rapport.

Rapport de contrôle
~~~~~~~~~~~~~~~~~~~

Ce rapport est accessible par clic droit sur l'arbre des erreurs après
vérification des erreurs sur les données.

.. image:: ../images/fr/12-controlmenu2.png

Ca rapport contient les mêmes erreurs que celle affiché dans l'arbre et
les graphiques de comparaison Captures/Tailles.


Sauvegarde et validation
------------------------

Vous pouvez à tout moment sauvegarder l'état des données au fur et à mesure
de vos corrections.

Après la vérification des données, et si celle ci c'est terminée **sans erreurs**
le bouton ``Valider le contrôle`` devient actif et vous permet de valider.

La validation de l'étape de contrôle est nécessaire pour pouvoir passer
à l'`étape de sélection`_.

.. _étape de sélection: guide_selection.html

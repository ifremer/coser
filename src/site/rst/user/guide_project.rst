.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Projet
======

La première action a effectuer pour traiter les 4 tables de données est de
créer un projet en y renseignant ses paramètres (y compris les 4 tables).

Configuration
-------------

Pour se faire, il suffit d'afficher l'interface de création de projet depuis
le menu *Fichier/Nouveau projet*. L'interface suivante s'affiche:

.. image:: ../images/fr/02-createproject.png

Cette interface doit contenir:

  * le nom du projet (les caractères autre que les lettres et chiffres
    sont interdit)
  * le nom de l'auteur (obligatoire)
  * les fichiers csv correspondant aux 4 tables (obligatoire)
  * les cartes aux format shapes (*.shp) utilisé dans l'étape de sélection
    pour affiché la zone d'application sur la carte du monde
  * un commentaire

Il est également possible d'utiliser pour ce projet seulement un autre
fichier de référence taxonomique que celui présent dans la configuration
par défaut du logiciel. Il sera utilisé uniquement par ce projet.

Validation
----------

Après avoir cliqué sur le bouton ``Créer le projet``, les 4 tables et le
référentiel taxonomique sont chargés en mémoire.
Le logiciel vérifie ensuite que les fichiers sont dans le format attendu
(CSV, séparateur, nom des entêtes) puis crée le projet et affiche l'interface
de résumé de projet :

.. image:: ../images/fr/04-projectsummary.png

Réouverture de projet
---------------------

Pour ré-ouvrir un projet précédemment créé, il suffit de le sélectionner
dans l'interface *Fichier/Ouvrir un projet* puis de cliquer sur `Ouvrir`:

.. image:: ../images/fr/03-openproject.png

Il est ensuite possible de passer aux étapes de contrôle_ ou de sélection_.

.. _contrôle: guide_control.html
.. _sélection: guide_selection.html

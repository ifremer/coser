.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Contrôles
=========

Coser dispose d'un interface permettant de personnaliser les contrôles.

Cependant, leurs personnalisation est très technique et est réservé aux
utilisateurs expérimentée.

Cette interface est composé d'un arbre avec la liste des 4 fichiers (captures,
tailles, traits, strates) et pour chacun d'eux, les niveaux des erreurs
détectable.

A chaque niveau, un fichier XML décrivant les contrôles peut être associés
pour les décrire.

Documentation
-------------

Les fichiers XML sont basé sur une syntaxe utilisé par le framework Java XWork.

La documentation de cette syntaxe peut être trouvé à cette adresse :
http://struts.apache.org/2.x/docs/validation.html


Champs
------

Pour chaque fichier, les champs sont accessible par leur nom anglais
(``survey``, ``stratum``...) dans leur type relatif (par exemple, ``number``
est un double java).

Pour chaque champs, il y a également un autre champs correspondant à sa
donnée sous forme de chaine de caractères. Par exemple, le champs ``survey`` est
également accessible via le nom ``surveyAsString``.
Le champs ``number`` (de type double) est également accessible sous le nom
``numberAsString`` (de type chaine).

Exemple
-------

Exemple du fichier de validation strate (niveau erreur) ::

  <validators>
    <field name="survey">
        <field-validator type="requiredstring">
            <param name="trim">true</param>
            <message>Missing survey name</message>
        </field-validator>
    </field>
    <field name="stratum">
        <field-validator type="requiredstring">
            <param name="trim">true</param>
            <message>Missing stratum name</message>
        </field-validator>
    </field>
    <field name="surfaceAsString">
        <field-validator type="checkDouble">
            <param name="notAvailable">NA</param>
            <message>surface attribute is not a valid double</message>
        </field-validator>
    </field>
  </validators>


Dans cet exemple:

  * le champs ``survey`` est obligatoire (requiredstring)
  * le champs ``surfaceAsString`` doit être un réel valide (la valeur NA étant
    autorisée)

.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Liste des contrôles
===================

Cette page détaille la liste des contrôles effectué par Coser lors
de l'étape de vérification des erreurs.

Captures
--------

+------------+----------------------------------------------------------------+----------+
| Champs     | Vérification                                                   | Niveau   |
+============+================================================================+==========+
| Campagne   | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Année      | Champs composé de 4 chiffres obligatoires et 2 chiffres        | Erreur   |
|            | facultatifs séparés par un point (trimestre) ex: 2002.25       |          |
+------------+----------------------------------------------------------------+----------+
| Traits     | Composé de lettres et chiffres                                 | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Especes    | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Nombre     | Réel, vide ou NA                                               | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Poids      | Réel, vide ou NA                                               | Erreur   |
+------------+----------------------------------------------------------------+----------+


+-----------------------------------------------------------------------------+----------+
| Autres vérifications                                                        | Niveau   |
+=============================================================================+==========+
| Si le Poids est un réel différent de 0, alors le Nombre doit être           | Erreur   |
| également différent de 0                                                    |          |
+-----------------------------------------------------------------------------+----------+
| Vérification des lignes dupliquées sur la clé "Campagne", "Annee", "Trait", | Erreur   |
| "Espece"                                                                    |          |
+-----------------------------------------------------------------------------+----------+
| Vérification du nombre minimal d'observations dans table CAPTURES$Nombre    | Warning  |
| par espèce par strate par année                                             |          |
+-----------------------------------------------------------------------------+----------+

Strates
-------

+------------+----------------------------------------------------------------+----------+
| Champs     | Vérification                                                   | Niveau   |
+============+================================================================+==========+
| Campagne   | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Strate     | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Surface    | Réel strictement positif                                       | Erreur   |
+------------+----------------------------------------------------------------+----------+


+-----------------------------------------------------------------------------+----------+
| Autres vérifications                                                        | Niveau   |
+=============================================================================+==========+
| Vérification des lignes dupliquées sur la clé "Campagne", "Strate"          | Erreur   |
+-----------------------------------------------------------------------------+----------+

Tailles
-------

+------------+----------------------------------------------------------------+----------+
| Champs     | Vérification                                                   | Niveau   |
+============+================================================================+==========+
| Campagne   | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Année      | Champs composé de 4 chiffres obligatoires et 2 chiffres        |          |
|            | facultatifs séparés par un point (trimestre) ex: 2002.25       | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Traits     | Composé de lettres et chiffres                                 | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Espece     | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Sexe       | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Maturite   | Champs obligatoire                                             | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Longueur   | Réel valide, vide et strictement positif                       | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Nombre     | Réel valide, vide ou NA                                        | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Poids      | Réel valide, vide ou NA                                        | Erreur   |
+------------+----------------------------------------------------------------+----------+
| Age        | Réel valide, vide ou NA                                        | Erreur   |
+------------+----------------------------------------------------------------+----------+


+-----------------------------------------------------------------------------+----------+
| Autres vérifications                                                        | Niveau   |
+=============================================================================+==========+
| Si le Poids est un réel différent de 0, alors le Nombre doit être           | Erreur   |
| également différent de 0                                                    |          |
+-----------------------------------------------------------------------------+----------+
| Vérification des lignes dupliquées sur la clé "Campagne", "Annee", "Trait", | Erreur   |
| "Espece", "Sexe", "Maturite", "Longueur"                                    |          |
+-----------------------------------------------------------------------------+----------+
| Vérification du nombre minimal d'observation dans table TAILLES$Nombre par  | Erreur   |
| espèce par trait par année                                                  |          |
+-----------------------------------------------------------------------------+----------+
| Pour les poissons, le champs Longueur doit utiliser un pas entier ou un     | Warning  |
| demi pas (par exemple: 4.0 ou 4.5, mais pas 4.25)                           |          |
+-----------------------------------------------------------------------------+----------+

Traits
------

+-----------------+----------------------------------------------------------------+----------+
| Champs          | Vérification                                                   | Niveau   |
+=================+================================================================+==========+
| Campagne        | Champs obligatoire                                             | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| Année           | Champs composé de 4 chiffres obligatoires et 2 chiffres        | Erreur   |
|                 | facultatifs séparés par un point (trimestre) ex: 2002.25       |          |
+-----------------+----------------------------------------------------------------+----------+
| Traits          | Composé de lettres et chiffres                                 | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| Mois            | 2 chiffres                                                     | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| Strates         | Champs obligatoire                                             | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| SurfaceBalayee  | Champs obligatoire et réel valide positif                      | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| Lat             | Réel valide, vide ou NA                                        | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| Long            | Réel valide, vide ou NA                                        | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| ProfMoy         | Réel valide, vide ou NA                                        | Erreur   |
+-----------------+----------------------------------------------------------------+----------+
| SurfaceBalayee  | Composé de au moins 3 décimales                                | Fatal    |
+-----------------+----------------------------------------------------------------+----------+
| Lat             | Composé de au moins 4 décimales                                | Warning  |
+-----------------+----------------------------------------------------------------+----------+
| Long            | Composé de au moins 4 décimales                                | Warning  |
+-----------------+----------------------------------------------------------------+----------+


+-----------------------------------------------------------------------------+----------+
| Autres vérifications                                                        | Niveau   |
+=============================================================================+==========+
| Vérification des lignes dupliquées sur la clé "Campagne", "Annee", "Trait", | Erreur   |
| "Mois"                                                                      |          |
+-----------------------------------------------------------------------------+----------+

Croisement de fichier
---------------------

+-----------------------------------------------------------------------------+----------+
| Vérifications                                                               | Niveau   |
+=============================================================================+==========+
| Vérification que le nom de campagne est le même dans les 4 tables           | Fatal    |
+-----------------------------------------------------------------------------+----------+
| La liste des années doit être identique entre les fichiers Captures,        |          |
| Tailles et Traits                                                           | Fatal    |
+-----------------------------------------------------------------------------+----------+
| Vérification que pour chaque espèce présent dans un trait d'une année       | Fatal    |
| dans le fichier tailles (noté Annee/Trait/Espece) il y a une donnée         |          |
| dans le fichier captures (le contraire n'est pas vrai)                      |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que pour chaque trait présent une année (noté Annee/Trait)     | Fatal    |
| dans les fichiers tailles et capt il y a une donnée dans le fichier         |          |
| traits                                                                      |          |
+-----------------------------------------------------------------------------+----------+
| Vérification des années pour lesquelle il y a des captures dans le fichier  | Warning  |
| captures, mais aucune dans le fichiers tailles                              |          |
+-----------------------------------------------------------------------------+----------+
| Vérification de la différence des captures entre le fichier captures et le  | Warning  |
| fichier taille, si celle ci dépasse le pourcentage renseigné dans la        |          |
| configuration                                                               |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des strates du fichier traits est présente dans   | Fatal    |
| le fichier strates                                                          |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des strates du fichier strates est présente dans  | Warning  |
| le fichier traits                                                           |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des traits du fichier captures est présente dans  | Fatal    |
| le fichier traits                                                           |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des traits du fichier traits est définie dans le  | Warning  |
| fichier captures                                                            |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des traits du fichier tailles est présente dans le| Fatal    |
| fichier captures                                                            |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des traits du fichier captures est définie dans le| Warning  |
| fichier tailles                                                             |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des espèces du fichier tailles est présente dans  | Fatal    |
| le fichier captures                                                         |          |
+-----------------------------------------------------------------------------+----------+
| Vérification que la liste des espèces du fichier captures est présente dans | Warning  |
| le fichier tailles                                                          |          |
+-----------------------------------------------------------------------------+----------+

Croisement avec le référentiel taxonomique
------------------------------------------

+-----------------------------------------------------------------------------+----------+
| Vérifications                                                               | Niveau   |
+=============================================================================+==========+
| Le champs Especes du fichier Captures doit correspondre à une espèce        | Erreur   |
| présente dans le référentiel taxonomique                                    |          |
+-----------------------------------------------------------------------------+----------+
| Le champs Especes du fichier Taille doit correspondre à une espèce          | Erreur   |
| présente dans le référentiel taxonomique                                    |          |
+-----------------------------------------------------------------------------+----------+

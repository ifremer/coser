.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Accueil
=======

Coser (Contrôle et Sélection R­Sufi) est une application de contrôle de données
(export CSV à partir d'une base de données) et de sélection de données avant
traitement par une application externe : RSufi.

Les résultats issus de RSufi sont ensuite enregistrés dans l'application coser
en vue d'être sélectionné pour être publié sur le site : 
http://www.ifremer.fr/SIH-indices-campagnes.


Fonctionnalités
---------------

  * gestion des 4 fichiers de données (captures, strates, tailles, traits).
  * contrôle des erreurs sur le format des données
  * contrôle en lien avec le référentiel taxonomique (reftax)
  * historisation des modifications
  * rapport de modification
  * génération de graphique
  * sélection des listes d’espèces pour RSUfi (L1, L2, L3, L4)
  * enregistrement des résultats RSUfi

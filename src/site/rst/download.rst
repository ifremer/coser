.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Lancement
=========

Prérequis
---------

Une installation de Java version 6 est requise pour pouvoir executer Coser.
Vous pouvez le télécharger depuis cette adresse
http://www.oracle.com/technetwork/java/javase/downloads/index.html.

Préférez la version la plus récente disponible et au moins la version
1.6.0_20.

Lancement en ligne
------------------
Vous pouvez lancer l'application directement en ligne (JavaWebStart) depuis
`cette page`_.

Téléchargement
--------------
Vous pouvez également télécharger l'application pour la lancer localement :
`télécharger coser`_.

.. _cette page: coser-ui/jnlp-report.html
.. _télécharger coser: http://forge.codelutin.com/projects/coser/files

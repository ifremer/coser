.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Organisation des dossiers des projets Coser
-------------------------------------------

Structure de répertoires ::

  testProject/
  ├── project.properties
  ├── modification.properties
  ├── reftaxSpecies.csv
  ├── original/
  │   ├── catch.csv
  │   ├── haul.csv
  │   ├── length.csv
  │   └── strata.csv
  ├── control/
  │   ├── catch_co.csv
  │   ├── haul_co.csv
  │   ├── length_co.csv
  │   └── strata_co.csv
  └── selections/
      ├── selection1/
      │   ├── catch_se.csv
      │   ├── haul_se.csv
      │   ├── length_se.csv
      │   ├── strata_se.csv
      │   ├── lists.properties
      │   ├── runrufi1/
      │   └── runrufi2/
      └── selection2/
          ├── catch_se.csv
          ├── haul_se.csv
          ├── length_se.csv
          ├── strata_se.csv
          ├── lists.properties
          └── runrufi1/

+-------------------------+---------------------------------------------------+
| Nom                     | Description                                       |
+=========================+===================================================+
| project.properties      | Configuration générale du projet                  |
+-------------------------+---------------------------------------------------+
| modification.properties | Configuration générale du projet                  |
+-------------------------+---------------------------------------------------+
| reftaxSpecies.csv       | Configuration générale du projet                  |
+-------------------------+---------------------------------------------------+
| original/               | Dossier contenant les fichiers données originaux  |
+-------------------------+---------------------------------------------------+
|                         | Dossier contenant les fichiers données après      |
| control/                | validation                                        |
+-------------------------+---------------------------------------------------+
| selections/             | Contient un dossier par selection                 |
+-------------------------+---------------------------------------------------+
| lists.properties        | Contient les information de la sélection (4listes)|
+-------------------------+---------------------------------------------------+
| runrufi1/               | Dossier contenant les resultat d'un run RSufi     |
+-------------------------+---------------------------------------------------+

.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Mise à jour des textes du site
==============================

Cette documentation détaille comment mettre à jour le contenu textuel du site:
http://www.ifremer.fr/SIH-indices-campagnes/.

**Cette documentation n'est plus à jour. Nous avons changé de système de versioning pour passer sous Git.**

Source
------
Pour pouvoir modifier n'importe quel contenu, il faut être développeur autorisé
à commiter sur le dépôt de source de coser.

Ensuite, il est possible de récupérer les sources via un client subversion:
  * Windows : http://tortoisesvn.net/
  * Linux : commande console ``svn``

L'adresse du dépôt à renseigner est : http://svn.forge.codelutin.com/svn/coser/trunk


Organisation des dossiers
-------------------------
Le répertoire racine est constitué de 3 modules maven:

  * *coser-business* : contenant le code métier (tout le code utilisé par les deux
    autres modules)
  * *coser-ui* : client swing
  * *coser-web* : site internet

Dans le cas de la modification du contenu text du site, le module *coser-web*
devra être utilisé.


Source du modules coser-web
---------------------------
Dans le module coser-web, deux groupes de fichiers concernent la modification
du contenu textuel:

  * ``src/main/webapp/WEB-INF/content`` : qui contient les pages html
  * ``src/main/resources/fr/ifremer/coser/web`` : qui contient les traductions de
    texte à inclure dans les pages html

En effet, dans les pages html, on trouve ce genre d'instruction::

  <head>
    <title><s:text name="message.index.title" /></title>
  </head>

Ce code défini un titre de page html, contenant la traduction de le clé
`message.layout.title`.

Et dans les fichiers ``package_en.properties`` et ``package_fr.properties``, on trouve
respectivement::

  message.index.title=Home

et::

  message.index.title=Accueil


Modification et commit sur le serveur
-------------------------------------
Il est possible de modifier la mise en forme et le contenu des pages html.
Toutefois, il est important de veiller à ce que le contenu des fichiers de
traductions soit en adequation avec les fichiers html.

Une fois les modifications effectuées, vous pouvez commiter ces modifications
sur le serveur avec votre client svn. Les identifiant et mot de passe qui vous
seront demandés sont ceux de votre compte sur la forge.


Mise en prod
------------
Le fait de commiter ne signifie pas que le site de production sera mis à jour.
En effet, une nouvelle version stable (release) doit être préparée et mise
en production. Les modifications que vous pourrez effectuer ne seront
intégrées que lors de la prochaine mise en production.

Il est toutefois possible de demander à l'equipe de developpement la production
d'une nouvelle version si vos modifications se révélaient urgentes.

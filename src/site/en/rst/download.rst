.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Launch
======

Prerequisites
-------------

To run Coser, you need a Java 6 installation. You can download it at this address:
http://www.oracle.com/technetwork/java/javase/downloads/index.html

Prefer last available version, and at least the 1.6.0_20 version.

Online launch
-------------

You can launch application directly from this website with Java WebStart from
`this page`_.

Download
--------

You can also download Coser to run it locally. `Download coser`_.

.. _this page: ../coser-ui/en/jnlp-report.html
.. _Download coser: http://forge.codelutin.com/projects/coser/files

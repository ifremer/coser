.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Controls
========

Coser has a user interface for customizing the data controls.

Warning: this customization is quite technical and must only be performed by
advanced users!

This interface consists of a tree with the four main data file branches (catch, length,
hauls, strata). For each main branch the controls to be carried out are specified as well
as their error level (fatal, error, warning).

For each branch, a XML file describing the controls can be associated.

Documentation
-------------

The XML files are based on the syntax used by the Java framework XWork.

The documentation of this syntax can be found at the address:
http://struts.apache.org/2.x/docs/validation.html

Fields
------

For each file, the fields are accessible using the english name (``survey``,
``stratum``…) in their relative type (for example,``number`` is a Java double).

All fields, including numeric fields, are also accesible in String format. For example, the field ``survey`` 
is also accessible using the name ``surveyAsString``.
The field ``number`` (of type double) is also accessible under the name ``numberAsString`` (of string type).

Example
-------

Example of a strata control file (error level) ::

  <validators>
    <field name="survey">
        <field-validator type="requiredstring">
            <param name="trim">true</param>
            <message>Missing survey name</message>
        </field-validator>
    </field>
    <field name="stratum">
        <field-validator type="requiredstring">
            <param name="trim">true</param>
            <message>Missing stratum name</message>
        </field-validator>
    </field>
    <field name="surfaceAsString">
        <field-validator type="checkDouble">
            <param name="notAvailable">NA</param>
            <message>Surface attribute is not a valid double</message>
        </field-validator>
    </field>
  </validators>

In this example:

  * the field ``survey`` is mandatory (requiredstring)
  * the field ``surface`` must be a valid double number (the value NA is valid).
  

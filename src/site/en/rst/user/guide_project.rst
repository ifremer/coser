.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Project
=======

The first step is to create a project by providing file paths and parameters.

Configuration
-------------

To do so, display the project creation interface using the menu
*File/New project*. The following interface displays :

.. image:: ../../images/en/02-createproject.png

This interface must contains:

  * the project name (only alphanumerical characters are allowed).
  * the name of the author (mandatory!)
  * The csv files correspond to the 4 data tables (mandatory!)
  * the maps in shapes format (*.shp) used in the selection step to display the
    application zone on the world map (optional).
  * a comment (optional)

It is also possible to use, only for this project, a specific taxonomic
reference file instead of the default file which is part of Coser. 

Validation
----------

After having clicked on ``Create project``, the 4 data tables and the
taxonomic reference file are loaded into memory.
The software then checks that the files are in the correct format (CSV,
separator, headers names) then creates the project and displays the project
summary interface :

.. image:: ../../images/en/04-projectsummary.png

Project reopening
-----------------

To reopen a project previously created, you just have to select it in the
*File/Open project* then click on `Open`:

.. image:: ../../images/en/03-openproject.png

It is then possible to go to the control_ or selection_ step.

.. _control: guide_control.html
.. _selection: guide_selection.html

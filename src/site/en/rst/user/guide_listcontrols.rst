.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Controls list
=============

This page lists the controls made by Coser when verifying the data.

Catches
-------

+------------+----------------------------------------------------------------+----------+
| Field      | Check                                                          | Level    |
+============+================================================================+==========+
| Survey     | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
|            | Field with 4 mandatory numbers and 2 optional numbers          |          |
| Year       | separated by a point (quarter) ex : 2002.25                    | Error    |
+------------+----------------------------------------------------------------+----------+
| Haul       | Made up of letters and numbers                                 | Error    |
+------------+----------------------------------------------------------------+----------+
| Species    | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
| Number     | Number, empty or NA                                            | Error    |
+------------+----------------------------------------------------------------+----------+
| Weight     | Number, empty or NA                                            | Error    |
+------------+----------------------------------------------------------------+----------+

+-----------------------------------------------------------------------------+----------+
| Other checks                                                                | Level    |
+=============================================================================+==========+
| If Weight has a value different from 0, then Number also needs to be        |          |
| different from 0                                                            | Error    |
+-----------------------------------------------------------------------------+----------+
| Check for duplicate entries with the keys "Survey", "Year", "Haul",         |          |
| "Species"                                                                   | Error    |
+-----------------------------------------------------------------------------+----------+
| Check of the minimum number of observations (minobs) in the table           |          |
| CATCH$Number by species by stratum by year                                  | Warning  |
+-----------------------------------------------------------------------------+----------+

Stratas
-------

+------------+----------------------------------------------------------------+----------+
| Fields     | Check                                                          | Level    |
+============+================================================================+==========+
| Survey     | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
| Stratum    | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
| Surface    | Number strictly positive                                       | Error    |
+------------+----------------------------------------------------------------+----------+

+-----------------------------------------------------------------------------+----------+
| Other Checks                                                                | Level    |
+=============================================================================+==========+
| Check for duplicated lines using the key "Survey", "Stratum"                | Error    |
+-----------------------------------------------------------------------------+----------+

Lengths
-------

+------------+----------------------------------------------------------------+----------+
| Fields     | Check                                                          | Level    |
+============+================================================================+==========+
| Survey     | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
|            | Field with 4 mandatory numbers and 2 optional numbers          |          |
| Year       | separated by a point (quarter) ex : 2002.25                    | Error    |
+------------+----------------------------------------------------------------+----------+
| Haul       | Made up of letters and numbers                                 | Error    |
+------------+----------------------------------------------------------------+----------+
| Species    | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
| Sex        | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
| Maturity   | Mandatory field                                                | Error    |
+------------+----------------------------------------------------------------+----------+
| Length     | Number or empty, strictly positive                             | Error    |
+------------+----------------------------------------------------------------+----------+
| Number     | Number, empty or NA                                            | Error    |
+------------+----------------------------------------------------------------+----------+
| Weight     | Number, empty or NA                                            | Error    |
+------------+----------------------------------------------------------------+----------+
| Age        | Number, empty or NA                                            | Error    |
+------------+----------------------------------------------------------------+----------+

+-----------------------------------------------------------------------------+----------+
| Other Checks                                                                | Level    |
+=============================================================================+==========+
| If Weight has a value different from 0, the corresponding Number must aslo  | Error    |
| be different from 0                                                         |          |
+-----------------------------------------------------------------------------+----------+
| Check for duplicated lines using the key "Survey", "Year", "Haul",          | Error    |
| "Species", "Sex", "Maturity", "Length"                                      |          |
+-----------------------------------------------------------------------------+----------+
| Check the minumum number of observations is >=minobs in the table           | Error    |
| LENGTH$Number by species by stratum by year                                 |          |
+-----------------------------------------------------------------------------+----------+
| For the fishes, the Length field must be integer cm or half cm              | Warning  |
| (for example: 4.0 or 4.5 but not 4.25)                                      |          |
+-----------------------------------------------------------------------------+----------+

Hauls
-----

+-----------------+----------------------------------------------------------------+----------+
| Fields          | Check                                                          | Level    |
+=================+================================================================+==========+
| Survey          | Mandatory field                                                | Error    |
+-----------------+----------------------------------------------------------------+----------+
|                 | Field with 4 mandatory numbers and 2 optional numbers          |          |
| Year            | separated by a point (trimester) ex : 2002.25                  | Error    |
+-----------------+----------------------------------------------------------------+----------+
| Haul            | Made up of letters and numbers                                 | Error    |
+-----------------+----------------------------------------------------------------+----------+
| Month           | 2 numbers                                                      | Error    |
+-----------------+----------------------------------------------------------------+----------+
| Stratum         | Mandatory field                                                | Error    |
+-----------------+----------------------------------------------------------------+----------+
| SweptSurface    | Mandatory field and number or NA                               | Error    |
+-----------------+----------------------------------------------------------------+----------+
| Lat             | Number, empty or NA                                            | Error    |
+-----------------+----------------------------------------------------------------+----------+
| Long            | Number, empty or NA                                            | Error    |
+-----------------+----------------------------------------------------------------+----------+
| Depth           | Number, empty or NA                                            | Error    |
+-----------------+----------------------------------------------------------------+----------+
| SweptSurface    | In km2 with at least 3 decimal values                          | Fatal    |
+-----------------+----------------------------------------------------------------+----------+
| Lat             | Decimal degrees with at least 4 decimal values  (ex. 50.4443)  | Warning  |
+-----------------+----------------------------------------------------------------+----------+
| Long            | Decimal degress with at least 4 decimal values (ex. -7.5554)   | Warning  |
+-----------------+----------------------------------------------------------------+----------+

+-----------------------------------------------------------------------------+----------+
| Other Checks                                                                | Level    |
+=============================================================================+==========+
| Check for duplicated lines using key "Survey", "Year", "Haul", "Month"      | Error    |
+-----------------------------------------------------------------------------+----------+

Files cross-checking
--------------------

+-----------------------------------------------------------------------------+----------+
| Checks                                                                      | Level    |
+=============================================================================+==========+
| Check the survey name is the same in the 4 files                            | Fatal    |
+-----------------------------------------------------------------------------+----------+
| The list of years must be the same in the Catchs, Length and Hauls files    | Fatal    |
+-----------------------------------------------------------------------------+----------+
| Check that for each species present in a haul of a year in the length file  | Fatal    |
| (noted Year/Haul/Species) there is an entry in the catch file (the opposite |          |
| is not true)                                                                |          |
+-----------------------------------------------------------------------------+----------+
| Check that for each haul present in a year (noted Year/Haul) in the         | Fatal    |
| length and catch files, there is an entry in the haul file                  |          |
+-----------------------------------------------------------------------------+----------+
| Check if, for a given year, there is an entry in catch file, there is a     | Warning  |
| corresponding one in the length file too.                                   |          |
+-----------------------------------------------------------------------------+----------+
| Check that the difference between total numbers in the catch file and       | Warning  |
| the corresonding numbers in the length file is below the percentage         |          | 
| defined in configuration file.                                              |          |
+-----------------------------------------------------------------------------+----------+
| Check that all strata in the haul file are also present in strata file.     | Fatal    |
+-----------------------------------------------------------------------------+----------+
| Check that all strata present in strata file are also in the haul file.     | Warning  |
+-----------------------------------------------------------------------------+----------+
| Check that each haul present in the catch file is also in the haul file.    | Fatal    |
+-----------------------------------------------------------------------------+----------+
| Check that each haul present in the haul file is also in the catch file.    | Warning  |
+-----------------------------------------------------------------------------+----------+
| Check that each haul present in the length file is also in the catch file.  | Fatal    |
+-----------------------------------------------------------------------------+----------+
| Check that each haul present in the catch file is also in the length file.  | Warning  |
+-----------------------------------------------------------------------------+----------+
| Check that each species present in the length file is also in the catch file| Fatal    |
+-----------------------------------------------------------------------------+----------+
| Check that each species present in the catch file is also in the length file| Warning  |
+-----------------------------------------------------------------------------+----------+


Cross checking with the taxonomic reference file
------------------------------------------------

+-----------------------------------------------------------------------------+----------+
| Checks                                                                      | Level    |
+=============================================================================+==========+
| The Species field in the catch file must correspond to a species            | Error    |
| present in the taxonomic reference file                                     |          |
+-----------------------------------------------------------------------------+----------+
| The Species filed in the length file must correspond to a species           | Error    |
| present in the taxonomic reference file                                     |          |
+-----------------------------------------------------------------------------+----------+

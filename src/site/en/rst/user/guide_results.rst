.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

RSufi Results
=============

Once your selected data have been used in RSufi, the results can be 
added to the results list of the project selection.

To do so, reopen the corresponding selection using the
*Data/Selection/<selectionname>* menu and Select the third tab ``RSufi``.

.. image:: ../../images/en/25-selectionresults.png

Now, the second part of the interface is of interest. It
lists the results stored in Coser.

To add a new result, click on the button ``Add a result``.

.. image:: ../../images/en/26-selectionaddresult.png

A Rsufi result is composed of :

  * a unique name amongst the selection results (mandatory and composed only
  of alphanumerical characters)
  * the RSufi version with which the result have been obtained (mandatory)
  * the date of the results addition (mandatory)
  * the ``estcomind`` (commnity indicators) Rsufi results file (mandatory)
  * the ``estpopind`` (population indicators) Rsufi results file (mandatory)

You can add optionally information on:

  * the study area (used when publishing results on the internet website
    to place the results in the appropriate geographic zone)
  * the maps directory (used by the website to display the species distribution maps)
  * publishable flag (used by administrator to differenciate publishable results from
    others)
  * other files (other files that need to be stored by Coser)

Once the results are parmaterized with no error, click on ``Add the result`` to
validate the action. This result will be then available to the Coser administrator so
that they can publish it on the Ifremer internet website :
http://www.ifremer.fr/SIH-indices-campagnes/


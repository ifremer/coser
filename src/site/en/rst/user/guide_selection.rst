.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Selection
=========
The objective of the selection phase is to create coherent and consistent data tables 
for use in the Rsufi package.

During the selection the following lists are created:

  * L1 : List of the selected species
  * L2 : List of species as subset from L1 respecting an occurence & density filter
  * L3 : List of species as subset from L2 with length measurements for all years
  * L4 : List of species as subset from L3 with maturity information

Selection creation
------------------

The first step consists in defining the selection parameters for Coser.

To do so, use the menu ``Selection/New selection``

.. image:: ../../images/en/27-selectionmenu0.png

**Note**: This menu is available only if the control step has been validated.

.. image:: ../../images/en/20-selectiondetail.png

Providing a name is mandatory; the name can only contain alphanumerical characters.

In the second step, the years are selected (gaps are possible). The 4 tables data will
be restricted to those years.

.. image:: ../../images/en/21-selectionyears.png

In the third step the strata are selected.

.. image:: ../../images/en/22-selectionstrata.png

To aid the selection of strata it is also possible to display the sampling effort table by clicking on
the table-shaped icon.

In the fourth step the species to be included in species list L1 are selected. At this point only 
species present in the previously selected years and strata can be selected.

.. image:: ../../images/en/23-selectionspecies.png

The list L1 ``Filtered species`` correspond to the list of all the species. This
list is filterable by the ``Filter by type`` list types.

To add a species in the L1 list, you have to select it and click on the
``right arrow`` button.

The two lists of this step have a contextual menu allowing to :

  * merge species, e.g. merge two species at the genus level
  * generate catch/length comparison figures
  * etc…

.. image:: ../../images/en/28-selectionmenu1.png


Report
------

It is possible to generate a report that contains all selection information by
clicking on the ``Selection report`` button. This report is in html format and
should be opened in your browser to be visualised and printed.

Species list
------------

Once the L1 list has been defined, it needs to be saved so that the ``Species list`` 
tab becomes active.

.. image:: ../../images/en/24-selectionlists.png

On this screen :

  * The L1 list corresponds to the species list defined in the previous step
  * The L2 list correspond to the species list of filtered species using the average
    occurence percentage and the average number of individuals per square
    kilometers as selection criteria
  * The L3 list summarize the species selected in L2 list that have length 
    measurements for every year
  * The L4 list summarize, amongst the L3 species, those with maturity information.

To be noted :

  * Only the **selected** species (highlighted) are taken into account
  * If the default selection is modified, the choice needs to be justified by
    filling in a comment box below the corresponding list

Once the 4 species lists have been defined, final selection is validated
it clicking on the `` Validate selection`` button.

Export data for RSufi
---------------------

Once the 4 lists have been defined and the selection validated, the third menu tab "RSufi"
becomes active.

.. image: ../../images/en/25-selectionresults.png

On this screen, the necessary data files for RSufi can be exported. The consist of 
the controlled and selected data from previous steps. Attention, Coser does not 
run RSufi, it only generates the files needed for its execution.

The generated files contain:

  * the 4 data tables (with only the selected years, strata and species)
  * a file detailing the 4 species lists (L1, L2, L3 and L4)
  * a project information file

Results
-------

Once RSufi has been executed with the selected data, move to 
`results storing`_ step.

.. _results storing: guide_results.html

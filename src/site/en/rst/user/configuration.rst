.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Configuration
=============

Before using Coser, the software needs to be configured, especially for the files that have to be supplied by the user.

A wizard is available with the menu *Options/Configuration*

Paths
-----

.. image:: ../../images/en/90-configurationpath.png

In this wizard, you can define following options:

coser.projects.directory
  Coser projects directory location.

coser.validator.directory
  Coser controls directory location. Used to store user defined control rules.

coser.reference.species
  Taxonomy reference file location.

coser.reference.typeSpecies
  Species types code file location.

coser.web.frontend
  SIH indice campagne website address (used by Ifremer administrator to publish results).

coser.web.zones.file
  Application zone file location (used to store Rsufi results).

Control step configuration
--------------------------

.. image:: ../../images/en/91-configurationvalues.png

In this view, you can define configuration values used for data controls :

coser.control.nobsmin
  Minimum number of individuals per species per stratum per year for data to be useable.

coser.control.diffcatchlength
  Maximum absolute acceptable difference (percentage) between total numbers in the catch data file and corresponding 
  total numbers in the length data file.

coser.control.typeFish
  Species type code (fish, crustacean,...). Used by some controls to exclude certain types.

coser.selection.occurrenceFilter
  Default percentage value used in minimum occurrence field filter in selection step.

coser.selection.densityFilter
  Default percentage value used in minimum density (No per km2) field filter in selection step.

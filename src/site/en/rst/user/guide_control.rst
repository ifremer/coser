.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Data control step
=================

The aim of the data control step is to detect errors in the four data tables
and possibly correct them if possible.

.. image:: ../../images/en/10-controlmain.png

Data check
----------

During this step, the first action to do is launching global check, by clicking
on the ``Check data`` button.

Coser will check:

  * line by line for errors in each of the 4 tables
  * verification by cross-checking files
  * verification by cross-checking with the taxonomic reference file

After the 10 verifications steps, a tree with all the detected errors is displayed.

For more information on the errors : `controls list`_.

.. _controls list: guide_listcontrols.html

Correcting data (fixing errors)
-------------------------------

When selecting an error, Coser displays the corresponding line of the data allowing you to
correct it.
The lines in red corresponds to the fields with detected errors. The right part details
the error messages.

If for example the year is missing, the user can add it by hand and then validate the modification.

Bulk data correction
--------------------

In the case of the same error on several lines :

  * either the error is caused by an export error of the 4 tables from the original data base and it is
    better to do the export again, bulk correction being impossible in coser.
  * or the errors can be corrected line by line.

The table that displays the data has a contextual menu allowing to :

  * delete several selected lines.
  * find a pattern and replace it by another one

.. image:: ../../images/en/11-controlmenu1.png


For example, if all numbers have a decimal comma (,) instead of a decimal point (.), 
it is possible to replace all commas by points in a single operation.

Charts
------

Coser generates figures to facilitate decision making (specifically for warnings).

The button ``Comparison Catch/Length`` displays an interface where a figure
is generated for each species. The data correspond to total ``Number``s per year in the 
catch and length files. A difference between the two values indicates an error in the original data, most 
likely caused during the raising procedure from the sub-sample to the haul level.

.. image:: ../../images/en/13-controlgraphdiff.png

The button``Length structures`` displays an interface for generating
figures for a data subset.

It is for example possible to display the length structure but only for a
stratum, or a species, or cumulated…

.. image:: ../../images/en/14-controlgraphlength.png

Reports
-------

In the control step, it is possible to generate two reports.

Modifications report
~~~~~~~~~~~~~~~~~~~~
The modifications report, accessible through the ``Modifications report``
button, is generated in HTML format and should be opened in a web browser. You can visualize
and print the report in/from the browser.

Controls report
~~~~~~~~~~~~~~~

This report is accessible by a right clic on the errors tree after verifying the
errors in the data.

.. image:: ../../images/en/12-controlmenu2.png

This report contains the same errors that are displayed in the tree and the
Catch/length comparison figures.

Save and validation
-------------------

You can save the current state of the corrected data at any time.

After the data verification ended with **no errors** the
``Valid control`` button becomes active to validate the (corrected) data.

Validation of the control step is necessary to be able to move on to the `selection step`_.

.. _selection step: guide_selection.html

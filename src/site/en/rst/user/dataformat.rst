.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Tables file format
==================

Coser needs four data tables for each project :

  * catches
  * strata
  * length
  * hauls

Each data table must be formatted using the CSV file format with ';' (semicolon)
separator and begin with predefined CSV headers (in English or French)
**respecting the colomn order**.

Tables details
--------------

catches
~~~~~~~

French headers::

  Campagne;Annee;Trait;Espece;Nombre;Poids

English headers::

  Survey;Year;Haul;Species;Number;Weight

Example (catches.csv)::

  Survey;Year;Haul;Species;Number;Weight
  EVHOE_TOT;1997;B1008;ALLO;160.00;0.47
  EVHOE_TOT;1997;B1008;ARNOLAT;1.00;0.01
  EVHOE_TOT;1997;B1008;ARNOTHO;1.00;0.03


strata
~~~~~~

French headers::

  Campagne;Strate;Surface

English headers::

  Survey;Stratum;Surface

Example (strata.csv)::

  Survey;Stratum;Surface
  EVHOE_TOT;Gn1;8201.69199214
  EVHOE_TOT;Gn2;11771.068813
  EVHOE_TOT;Gn3;17327.206597099997


length
~~~~~~

French headers::

  Campagne;Annee;Trait;Espece;Sexe;Maturite;Longueur;Nombre;Poids;Age

English headers::

  Survey;Year;Haul;Species;Sex;Maturity;Length;Number;Weight;Age

Example (length.csv)::

  Survey;Year;Haul;Species;Sex;Maturity;Length;Number;Weight;Age
  EVHOE_TOT;1997;B1008;ARNOLAT;i;NA;11.00;1.00;;NA
  EVHOE_TOT;1997;B1008;ARNOTHO;i;NA;13.00;1.00;;NA
  EVHOE_TOT;1997;B1008;BOOPBOO;i;NA;14.00;1.00;0.02;NA


hauls
~~~~~

French headers::

  Campagne;Annee;Trait;Mois;Strate;SurfaceBalayee;Lat;Long;ProfMoy

English headers::

  Survey;Year;Haul;Month;Stratum;SweptSurface;Lat;Long;Depth

Example (hauls.csv)::

  Survey;Year;Haul;Month;Stratum;SweptSurface;Lat;Long;Depth
  EVHOE_TOT;1997;B1008;10;Gs2;0.06;43.75;-1.50;45.50
  EVHOE_TOT;1997;B1010;10;Gs1;0.06;43.81;-1.45;31.00
  EVHOE_TOT;1997;B1012;10;Gs3;0.06;43.89;-1.73;115.00

.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -


Website text update
===================

This documentation shows hom to update text content of website available at:
http://www.ifremer.fr/SIH-indices-campagnes/.

**This documentation is not up-to-date. We have change the scm system, we are now using Git.**

Source
------
In order to modify source content, you have to be part of coser project developper
to be allowed to commit on repository.

After this autorisation, sources can be retreived with a subversion client:

  * Windows : http://tortoisesvn.net/
  * Linux : command line ``svn``

Repository address is : http://svn.forge.codelutin.com/svn/coser/trunk

Directory layout
----------------
Root directory contains 3 maven modules:

  * *coser-business* : contains all business code (used by both other modules)
  * *coser-ui* : swing client
  * *coser-web* : website

In order to modify website content *coser-web* module will be used.


coser-web module source
-----------------------
In coser-web module, two groups of files concern the modification of text content:

  * ``src/main/webapp/WEB-INF/content`` : html pages
  * ``src/main/resources/fr/ifremer/coser/web`` : text translation to include in html pages


Indeed, in the html pages, we find this kind of code ::

  <head>
    <title><s:text name="message.index.title" /></title>
  </head>

This code define a html page title, with the translation key `message.layout.title`.

And in files ``package_en.properties`` and ``package_fr.properties`` we find
respectively::

  message.index.title=Home

et::

  message.index.title=Accueil


Change and commit to the server
-------------------------------
You can change the formatting and content of HTML pages.
However, it is important to ensure that the contents of translation files
are in adequacy with the html.

After making changes, you can commiting these changes
on the server with your svn client.


Production
----------
The act of commiting does not mean that the production site will be updated.
Indeed, a new stable release must be prepared and installed in production.
The changes you make will only be integrated in the next production.

However, it is possible to ask the development team to prepare
a new version if your changes were urgent.

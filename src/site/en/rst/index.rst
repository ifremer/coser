.. -
.. * #%L
.. * Coser
.. * %%
.. * Copyright (C) 2010 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Coser
=====

Coser (Control and Selection R­Sufi) is a data control (CSV export from
a database) and data selection before treatment by an external application
(RSufi) software.

The results from RSufi are then stored in the Coser software in order to be
selected and published on the website :
http://www.ifremer.fr/SIH-indices-campagnes.

Features
========

  * managing the 4 data files (catches, strata, lengthes, hauls)
  * control errors on the data format
  * control links with the taxonomic referential (reftax)
  * modifications historisation
  * charts generation
  * species lists selection for RSufi (L1, L2, L3 and L4)
  * storing RSufi's results

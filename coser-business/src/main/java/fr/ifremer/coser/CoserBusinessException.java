/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

/**
 * Coser exception.
 *
 * @author chatellier
 */
public class CoserBusinessException extends Exception {

    private static final long serialVersionUID = 8197217202331463505L;

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message the detail message
     */
    public CoserBusinessException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   the cause
     */
    public CoserBusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}

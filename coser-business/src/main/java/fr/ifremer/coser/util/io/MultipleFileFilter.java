package fr.ifremer.coser.util.io;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;

import java.io.File;
import java.io.FileFilter;
import java.util.Collection;
import java.util.Iterator;

/**
 * Aggrege plusieurs file filters.
 *
 * @since 1.5
 */
public class MultipleFileFilter implements FileFilter {

    protected Collection<FileFilter> fileFilters = Lists.newArrayList();

    public void add(FileFilter f) {
        fileFilters.add(f);
    }

    @Override
    public boolean accept(File pathname) {

        boolean result = false;
        Iterator<FileFilter> it = fileFilters.iterator();
        while (it.hasNext() && !result) {
            result = it.next().accept(pathname);
        }
        return result;
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.util;

import com.google.common.collect.Maps;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;

/**
 * Les types de données gérées par le site internet, sur lesquelle il est
 * possible de filtrer dans le formulaire de recherche.
 *
 * @author echatellier
 * @since 1.4
 */
public enum DataType {
    MAP(n("coser.business.data.type.map"), true, false),
    POPULATION(n("coser.business.data.type.population"), true, true),
    COMMUNITY(n("coser.business.data.type.community"), true, true),
    SOURCE(n("coser.business.data.type.source"), false, false);

    private final String i18nKey;

    private final boolean needSpecies;

    private final boolean indicator;

    DataType(String i18nKey, boolean needSpecies, boolean indicator) {
        this.i18nKey = i18nKey;
        this.needSpecies = needSpecies;
        this.indicator = indicator;
    }

    public String getLabel(Locale locale) {
        return l(locale, i18nKey);
    }

    public static Map<String, String> getExtractTypes(Locale locale) {
        Map<String, String> result = Maps.newLinkedHashMap();
        for (DataType dataType : DataType.values()) {
            result.put(dataType.name(), dataType.getLabel(locale));
        }
        return result;
    }

    public static boolean isNeedSpecies(List<DataType> types) {

        boolean result = false;

        for (DataType type : types) {
            if (type.needSpecies) {
                result = true;
                break;
            }
        }

        return result;

    }

    public static boolean isIndicator(List<DataType> types) {

        boolean result = false;

        for (DataType type : types) {
            if (type.indicator) {
                result = true;
                break;
            }
        }

        return result;

    }
}

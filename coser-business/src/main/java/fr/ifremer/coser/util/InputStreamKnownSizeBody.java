/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.util;

import org.apache.http.entity.mime.content.InputStreamBody;

import java.io.InputStream;

/**
 * Override of apache httpcomponent's InputStreamBody to fix content length of
 * input stream.
 *
 * Otherwize, this result in request blocked by modsecurity.
 *
 * See : http://www.radomirml.com/blog/2009/02/13/file-upload-with-httpcomponents-successor-of-commons-httpclient/
 *
 * @author echatellier
 * @since 1.4.2
 */
public class InputStreamKnownSizeBody extends InputStreamBody {

    /** Input stream length. */
    private long lenght;

    /**
     * Constructor with length.
     *
     * @param in       input stream
     * @param lenght   length
     * @param mimeType mime type
     * @param filename file name
     */
    public InputStreamKnownSizeBody(InputStream in, long lenght, String mimeType, String filename) {
        super(in, mimeType, filename);
        this.lenght = lenght;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getContentLength() {
        return this.lenght;
    }
}

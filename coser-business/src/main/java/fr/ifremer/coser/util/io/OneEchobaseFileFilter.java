package fr.ifremer.coser.util.io;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.bean.EchoBaseProject;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

/**
 * Filter pour un resultat echobase donné.
 * Created on 3/19/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class OneEchobaseFileFilter implements FileFilter {

    protected final String basedir;

    protected final boolean publishData;

    protected final String rawDataBasedir;

    public OneEchobaseFileFilter(EchoBaseProject project,
                                 boolean publishData) {
        this.publishData = publishData;
        try {
            this.basedir = project.getBasedir().getCanonicalPath();
            this.rawDataBasedir = project.getRawDataDirectory().getCanonicalPath();
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not get canonical path", e);
        }
    }

    @Override
    public boolean accept(File pathname) {
        try {
            String currentPathName = pathname.getCanonicalPath();
            boolean result = currentPathName.startsWith(basedir);
            if (result && !publishData) {
                result = !currentPathName.startsWith(rawDataBasedir);
            }
            return result;
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not get canonical path", e);
        }
    }
}

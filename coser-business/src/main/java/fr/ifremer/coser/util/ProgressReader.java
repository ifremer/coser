/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.util;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;

/**
 * Reader that notify read progression.
 *
 * @author chatellier
 */
public class ProgressReader extends Reader {

    /** Delegate reader */
    protected Reader delegateReader;

    /** Progress monitor notified. */
    protected ProgressMonitor delegateProgress;

    public ProgressReader(Reader delegateReader, ProgressMonitor delegateProgress) {
        this.delegateReader = delegateReader;
        this.delegateProgress = delegateProgress;
    }

    /*
     * @see java.io.Reader#read(char[], int, int)
     */
    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int nb = delegateReader.read(cbuf, off, len);
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.Reader#read(java.nio.CharBuffer)
     */
    @Override
    public int read(CharBuffer target) throws IOException {
        int nb = delegateReader.read(target);
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.Reader#read()
     */
    @Override
    public int read() throws IOException {
        int nb = delegateReader.read();
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.Reader#read(char[])
     */
    @Override
    public int read(char[] cbuf) throws IOException {
        int nb = delegateReader.read(cbuf);
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.Reader#skip(long)
     */
    @Override
    public long skip(long n) throws IOException {
        return delegateReader.skip(n);
    }

    /*
     * @see java.io.Reader#ready()
     */
    @Override
    public boolean ready() throws IOException {
        return delegateReader.ready();
    }

    /*
     * @see java.io.Reader#markSupported()
     */
    @Override
    public boolean markSupported() {
        return delegateReader.markSupported();
    }

    /*
     * @see java.io.Reader#mark(int)
     */
    @Override
    public void mark(int readAheadLimit) throws IOException {
        delegateReader.mark(readAheadLimit);
    }

    /*
     * @see java.io.Reader#reset()
     */
    @Override
    public void reset() throws IOException {
        delegateReader.reset();
    }

    /*
     * @see java.io.Reader#close()
     */
    @Override
    public void close() throws IOException {
        delegateReader.close();
    }
}

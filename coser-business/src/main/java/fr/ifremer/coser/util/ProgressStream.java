/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Stream that notify read progression.
 *
 * @author chatellier
 */
public class ProgressStream extends InputStream {

    /** Delegate stream/ */
    protected InputStream delegateStream;

    /** Progress monitor notified. */
    protected ProgressMonitor delegateProgress;

    public ProgressStream(InputStream delegateStream, ProgressMonitor delegateProgress) {
        this.delegateStream = delegateStream;
        this.delegateProgress = delegateProgress;
    }

    /*
     * @see java.io.InputStream#read()
     */
    @Override
    public int read() throws IOException {
        int nb = delegateStream.read();
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.InputStream#read(byte[], int, int)
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int nb = delegateStream.read(b, off, len);
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.InputStream#read(byte[])
     */
    @Override
    public int read(byte[] b) throws IOException {
        int nb = delegateStream.read(b);
        if (nb != -1) {
            delegateProgress.addCurrent(nb);
        }
        return nb;
    }

    /*
     * @see java.io.InputStream#skip(long)
     */
    @Override
    public long skip(long n) throws IOException {
        return delegateStream.skip(n);
    }

    /*
     * @see java.io.InputStream#available()
     */
    @Override
    public int available() throws IOException {
        return delegateStream.available();
    }

    /*
     * @see java.io.InputStream#mark(int)
     */
    @Override
    public synchronized void mark(int readlimit) {
        delegateStream.mark(readlimit);
    }

    /*
     * @see java.io.InputStream#reset()
     */
    @Override
    public synchronized void reset() throws IOException {
        delegateStream.reset();
    }

    /*
     * @see java.io.InputStream#markSupported()
     */
    @Override
    public boolean markSupported() {
        return delegateStream.markSupported();
    }

    /*
     * @see java.io.InputStream#close()
     */
    @Override
    public void close() throws IOException {
        delegateStream.close();
    }
}

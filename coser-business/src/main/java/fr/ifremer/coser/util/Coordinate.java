/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.util;

import java.io.Serializable;

/**
 * Coordinate used to manipulate haul in map.
 *
 * @author chatellier
 */
public class Coordinate implements Serializable {


    private static final long serialVersionUID = -5111417883474394223L;

    protected int serie;

    protected String name;

    protected double latitude;

    protected double longitude;

    public Coordinate() {

    }

    public Coordinate(int serie, String name, double latitude, double longitude) {
        this();
        this.serie = serie;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

package fr.ifremer.coser.util.io;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.regex.Matcher;

/**
 * Filter pour un resultat rsufi donné.
 *
 * Attention, implémentation que ne doit fonctionner que avec ZipUtil
 * car meme si on refuse en répertoire, il redemande quand même
 * les fils (et il faut qu'il les demande)
 *
 * @since 1.5
 */
public class OneRSufiResultFileFilter implements FileFilter {

    protected File projectsDirectory;

    /** Doit etre un project chargé avec nom de fichier originaux. */
    protected Project project;

    protected Selection selection;

    protected RSufiResult rsufi;

    protected boolean exportWithData;

    public OneRSufiResultFileFilter(File projectsDirectory,
                                    Project project,
                                    Selection selection,
                                    RSufiResult rsufi,
                                    boolean exportWithData) {
        this.projectsDirectory = projectsDirectory;
        this.project = project;
        this.selection = selection;
        this.rsufi = rsufi;
        this.exportWithData = exportWithData;
    }

    @Override
    public boolean accept(File pathname) {

        boolean result;

        try {
            String currentPathName = pathname.getCanonicalPath() + File.separator;

            File projectDirectory = new File(projectsDirectory, project.getName());
            File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
            File selectionDirectory = new File(selectionsDirectory, selection.getName());
            File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
            File resultDirectory = new File(resultsDirectory, rsufi.getName());
            File originsDirectory = new File(projectDirectory, CoserConstants.STORAGE_ORIGINAL_DIRECTORY);
            File controlsDirectory = new File(projectDirectory, CoserConstants.STORAGE_CONTROL_DIRECTORY);

            String projectPath = projectDirectory.getCanonicalPath() + File.separator;
            String selectionsPath = selectionsDirectory.getCanonicalPath() + File.separator;
            String selectionPath = selectionDirectory.getCanonicalPath() + File.separator;
            String resultsPath = resultsDirectory.getCanonicalPath() + File.separator;
            String resultPath = resultDirectory.getCanonicalPath() + File.separator;
            String originsPath = originsDirectory.getCanonicalPath() + File.separator;
            String controlsPath = controlsDirectory.getCanonicalPath() + File.separator;

            // on prend
            // - tout ce qu'il y a dans le projet
            // - sauf les répertoire "selections", "original" et "controls"
            // - ou la selection entierement
            // - sauf le répertoire result
            // - ou le resultat entierrement
            result = (currentPathName.startsWith(projectPath)
                      && !(currentPathName.startsWith(originsPath)||currentPathName.startsWith(controlsPath))||currentPathName.startsWith(selectionsPath))
                     || (currentPathName.startsWith(selectionPath)
                         && !currentPathName.startsWith(resultsPath))
                    || currentPathName.startsWith(resultPath);

            // cas ou les données sources ne doivent pas être exporter
            // condition sur les noms de fichiers ?
            if (!exportWithData) {
                String fileName = pathname.getName();

                // on exclu tout les fichiers qui commencent
                // par les memes noms de fichiers que ceux du projet
                // (les noms de fichiers sont personnalisables)
                for (CoserConstants.Category category : CoserConstants.Category.values()) {
                    if (category.isDataCategory()) {
                        String sourceFileName = project.getDataStorageFileName(category, null);
                        Matcher matcher = CoserUtils.FILENAME_SUFFIX_PATTERN.matcher(sourceFileName);
                        if (matcher.matches()) {
                            result &= !(fileName.startsWith(matcher.group(1)) && fileName.endsWith(matcher.group(2)));
                        } else {
                            result &= !fileName.startsWith(sourceFileName);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't get system canonical path");
        }

        return result;
    }
}

package fr.ifremer.coser;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.CoserCommandFactory;
import fr.ifremer.coser.result.CoserMainRepositoryProvider;
import fr.ifremer.coser.result.repository.ResultRepositoryProvider;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.util.Charts;
import fr.ifremer.coser.result.util.Extracts;
import fr.ifremer.coser.result.util.Reports;
import org.nuiton.config.ArgumentsParserException;

import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class DefaultCoserApplicationContext implements CoserApplicationContext {

    /**
     * Application configuration.
     */
    protected CoserBusinessConfig config;

    /**
     * Result repository types (loaded by service loader).
     */
    protected Set<ResultRepositoryType> repositoryTypes;

    /**
     * Result repository provider.
     */
    protected CoserMainRepositoryProvider repositoryProvider;

    /**
     * Factory of commands.
     */
    protected CoserCommandFactory commandFactory;

    /**
     * Cache of indicator definition.
     */
    protected IndicatorMap indicatorsMap;

    /**
     * Cache of zone definition.
     */
    protected ZoneMap zonesMap;

    /**
     * Reports helper.
     */
    protected Reports reports;

    /**
     * Charts helper.
     */
    protected Charts charts;

    private Extracts extracts;

    public DefaultCoserApplicationContext(CoserBusinessConfig config) {
        this.config = config;
        try {
            config.parse();
        } catch (ArgumentsParserException ex) {
            throw new CoserTechnicalException("Can't read configuration", ex);
        }
    }

    @Override
    public CoserBusinessConfig getConfig() {
        return config;
    }

    @Override
    public Set<ResultRepositoryType> getRepositoryTypes() {
        if (repositoryTypes == null) {
            ServiceLoader<ResultRepositoryType> loader = ServiceLoader.load(ResultRepositoryType.class);
            repositoryTypes = Sets.newHashSet(loader);
        }
        return repositoryTypes;
    }

    @Override
    public IndicatorMap getIndicatorMap() {
        if (indicatorsMap == null) {
            indicatorsMap = new IndicatorMap(config.getWebIndicatorsFile());
        }
        return indicatorsMap;
    }

    @Override
    public ZoneMap getZoneMap() {
        if (zonesMap == null) {
            zonesMap = new ZoneMap(config.getWebZonesFile());
        }
        return zonesMap;
    }

    @Override
    public Reports getReports() {
        if (reports == null) {
            reports = new Reports();
        }
        return reports;
    }

    @Override
    public Charts getCharts() {
        if (charts == null) {
            charts = new Charts();
        }
        return charts;
    }

    @Override
    public Extracts getExtracts() {
        if (extracts == null) {
            extracts = new Extracts();
        }
        return extracts;
    }

    @Override
    public CoserMainRepositoryProvider getRepositoryProvider() {
        if (repositoryProvider == null) {

            Set<ResultRepositoryProvider<?>> repositoryProviders = CoserMainRepositoryProvider.createDefaultRepositoryProviders(config);
            repositoryProvider = new CoserMainRepositoryProvider(repositoryProviders);
        }
        return repositoryProvider;
    }

    @Override
    public CoserCommandFactory getCommandFactory() {
        if (commandFactory == null) {
            commandFactory = new CoserCommandFactory(getRepositoryTypes());
        }
        return commandFactory;
    }

}

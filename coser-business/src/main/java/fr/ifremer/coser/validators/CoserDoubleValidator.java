/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.validators;

import com.opensymphony.xwork2.validator.ValidationException;

/**
 * Coser double validator.
 *
 * @author chatellier
 */
public class CoserDoubleValidator extends AbstractFieldValidator {

    protected Double max = null;

    protected Double min = null;

    protected Double minExclusive = null;

    protected Double maxExclusive = null;

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getMax() {
        return max;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMin() {
        return min;
    }

    public Double getMinExclusive() {
        return minExclusive;
    }

    public void setMinExclusive(Double minExclusive) {
        this.minExclusive = minExclusive;
    }

    public Double getMaxExclusive() {
        return maxExclusive;
    }

    public void setMaxExclusive(Double maxExclusive) {
        this.maxExclusive = maxExclusive;
    }

    public void validate(Object object) throws ValidationException {
        Object obj = getFieldValue(getFieldName(), object);
        String value = (String) obj;

        // if there is no value - don't do comparison
        // if a value is required, a required validator should be added to the field
        if (value == null || value.isEmpty()) {
            return;
        }

        try {
            Double doubleValue = Double.valueOf(value);

            if ((max != null && doubleValue.compareTo(max) > 0) ||
                (min != null && doubleValue.compareTo(min) < 0) ||
                (maxExclusive != null && doubleValue.compareTo(maxExclusive) >= 0) ||
                (minExclusive != null && doubleValue.compareTo(minExclusive) <= 0)) {
                addFieldError(getFieldName(), object);
            }
        } catch (NumberFormatException ex) {
            // skipped, already validated in check double
        }
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.validators;

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * Abstract validator.
 *
 * @author chatellier
 */
public abstract class AbstractFieldValidator extends FieldValidatorSupport {

    private ValueStack localStack;

    public void setValueStack(ValueStack localStack) {
        super.setValueStack(localStack);
        this.localStack = localStack;
    }

    /**
     * Return the field value named <code>name</code> from <code>object</code>,
     * <code>object</code> should have the appropriate getter/setter.
     *
     * @param name
     * @param object
     * @return Object as field value
     * @throws ValidationException
     */
    protected Object getFieldValue(String name, Object object, boolean throwExceptionOnFailure) throws ValidationException {

        boolean pop = false;

        if (!localStack.getRoot().contains(object)) {
            localStack.push(object);
            pop = true;
        }

        Object retVal = localStack.findValue(name, throwExceptionOnFailure);

        if (pop) {
            localStack.pop();
        }

        return retVal;
    }
}

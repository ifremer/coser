/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.validators;

import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validateur de double de coser en tant que string.
 *
 * @author chatellier
 */
public class CoserCheckDoubleValidator extends AbstractFieldValidator {

    /**
     * Pattern pour un double
     * - group0 => all double
     * - group1 => .xxxx
     * - group2 => xxxx (after .)
     * - group3 => exposant with e
     * - group4 => exposant without e
     */
    protected static final Pattern DOUBLE_PATTERN = Pattern.compile("^-?[0-9]+(\\.([0-9]+))?([eE](\\-?[0-9]+))?$");

    protected String notAvailable;

    protected Integer minDecimals;

    public String getNotAvailable() {
        return notAvailable;
    }

    public void setNotAvailable(String notAvailable) {
        this.notAvailable = notAvailable;
    }

    public Integer getMinDecimals() {
        return minDecimals;
    }

    public void setMinDecimals(Integer minDecimals) {
        this.minDecimals = minDecimals;
    }

    public void validate(Object object) throws ValidationException {
        Object obj = getFieldValue(getFieldName(), object);
        String value = (String) obj;

        // if there is no value - don't do comparison
        // if a value is required, a required validator should be added to the field
        if (value == null || value.isEmpty()) {
            return;
        }

        // If NA value = object it's ok too
        if (notAvailable != null && notAvailable.equals(value)) {
            return;
        }

        if (minDecimals != null) {
            Matcher matcher = DOUBLE_PATTERN.matcher(value);
            if (matcher.find()) {
                int decimaleSize = StringUtils.length(matcher.group(2));
                int exposant = 0;
                if (matcher.group(4) != null) {
                    exposant = Integer.parseInt(matcher.group(4));
                }

                // 5.44e-2 => 2 - (-2) => 4 < minDecimale
                if (decimaleSize - exposant < minDecimals) {
                    addFieldError(getFieldName(), object);
                }
            } else {
                addFieldError(getFieldName(), object);
            }
        }

        try {
            Double.parseDouble(value);
        } catch (NumberFormatException ex) {
            addFieldError(getFieldName(), object);
        }
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package fr.ifremer.coser.validators;

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.ExpressionValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Modification de {@link ExpressionValidator} pour avoir les exceptions.
 *
 * @author chatellier
 */
public class CoserExpressionValidator extends ExpressionValidator {

    private static final Log log = LogFactory.getLog(CoserExpressionValidator.class);

    private ValueStack localStack;

    public void setValueStack(ValueStack localStack) {
        super.setValueStack(localStack);
        this.localStack = localStack;
    }

    public void validate(Object object) throws ValidationException {
        Boolean answer = Boolean.FALSE;
        Object obj = null;

        try {
            obj = getFieldValue(getExpression(), object, true);
        } catch (ValidationException e) {
            throw e;
        } catch (Exception e) {

            // arrive si des valeurs NA sont comparée par exemple
            // le test lance une exception, mais on le considere
            // valide pour la suite
            obj = Boolean.TRUE;
        }

        if ((obj != null) && (obj instanceof Boolean)) {
            answer = (Boolean) obj;
        } else {
            log.warn("Got result of " + obj + " when trying to get Boolean.");
        }

        if (!answer.booleanValue()) {
            if (log.isDebugEnabled())
                log.debug("Validation failed on expression " + getExpression() + " with validated object " + object);
            addActionError(object);
        }
    }

    /**
     * Return the field value named <code>name</code> from <code>object</code>,
     * <code>object</code> should have the appropriate getter/setter.
     *
     * @param name
     * @param object
     * @return Object as field value
     * @throws ValidationException
     */
    protected Object getFieldValue(String name, Object object, boolean throwExceptionOnFailure) throws ValidationException {

        boolean pop = false;

        if (!localStack.getRoot().contains(object)) {
            localStack.push(object);
            pop = true;
        }

        Object retVal = localStack.findValue(name, throwExceptionOnFailure);

        if (pop) {
            localStack.pop();
        }

        return retVal;
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.data;

import fr.ifremer.coser.bean.AbstractEntity;

import static org.nuiton.i18n.I18n.n;

/**
 * Abstract entity to add all common data array code.
 *
 * @author chatellier
 */
public abstract class AbstractDataEntity extends AbstractEntity {


    private static final long serialVersionUID = 4188448448464323807L;

    public static final int INDEX_LINE = 0;

    public static final String PROPERTY_LINE = n("coser.business.line");

    protected String[] data;

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data.clone();
    }

    public void setLine(String value) {
        String oldValue = data[INDEX_LINE];
        data[INDEX_LINE] = value;
        getPropertyChangeSupport().firePropertyChange(PROPERTY_LINE, oldValue, value);
    }

    public String getLine() {
        return data[INDEX_LINE];
    }

    /**
     * Return CSV header according to properties names.
     *
     * @return header array
     */
    public abstract String[] getHeaders();
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.data;

/**
 * Length entity delegating to ordered array.
 *
 * "Campagne","Annee","Trait","Espece","Sexe","Maturite","Longueur","Nombre","Poids","Age"
 * "Survey","Year","Haul","Species","Sex","Maturity","Length","Number","Weight","Age"
 *
 * @author chatellier
 */
public class Length extends AbstractDataEntity {


    private static final long serialVersionUID = 1283486477462355761L;

    public static final String[] FR_HEADERS = {
            "Campagne", "Annee", "Trait", "Espece", "Sexe", "Maturite", "Longueur", "Nombre", "Poids", "Age"
    };

    public static final String[] EN_HEADERS = {
            "Survey", "Year", "Haul", "Species", "Sex", "Maturity", "Length", "Number", "Weight", "Age"
    };

    public static final int INDEX_SURVEY = 1;

    public static final int INDEX_YEAR = 2;

    public static final int INDEX_HAUL = 3;

    public static final int INDEX_SPECIES = 4;

    public static final int INDEX_SEX = 5;

    public static final int INDEX_MATURITY = 6;

    public static final int INDEX_LENGTH = 7;

    public static final int INDEX_NUMBER = 8;

    public static final int INDEX_WEIGHT = 9;

    public static final int INDEX_AGE = 10;

    public String[] getHeaders() {
        return EN_HEADERS;
    }

    public void setSurvey(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("survey", oldValue, value);
    }

    public String getSurvey() {
        return data[INDEX_SURVEY];
    }

    public void setYear(String value) {
        String oldValue = data[INDEX_YEAR];
        data[INDEX_YEAR] = value;
        getPropertyChangeSupport().firePropertyChange("year", oldValue, value);
    }

    public String getYear() {
        return data[INDEX_YEAR];
    }

    public void setHaul(String value) {
        String oldValue = data[INDEX_HAUL];
        data[INDEX_HAUL] = value;
        getPropertyChangeSupport().firePropertyChange("haul", oldValue, value);
    }

    public String getHaul() {
        return data[INDEX_HAUL];
    }

    public void setSpecies(String value) {
        String oldValue = data[INDEX_SPECIES];
        data[INDEX_SPECIES] = value;
        getPropertyChangeSupport().firePropertyChange("species", oldValue, value);
    }

    public String getSpecies() {
        return data[INDEX_SPECIES];
    }

    public void setSex(String value) {
        String oldValue = data[INDEX_SEX];
        data[INDEX_SEX] = value;
        getPropertyChangeSupport().firePropertyChange("sex", oldValue, value);
    }

    public String getSex() {
        return data[INDEX_SEX];
    }

    public void setMaturity(String value) {
        String oldValue = data[INDEX_MATURITY];
        data[INDEX_MATURITY] = value;
        getPropertyChangeSupport().firePropertyChange("maturity", oldValue, value);
    }

    public String getMaturity() {
        return data[INDEX_MATURITY];
    }

    public void setLength(String value) {
        String oldValue = data[INDEX_LENGTH];
        data[INDEX_LENGTH] = value;
        getPropertyChangeSupport().firePropertyChange("length", oldValue, value);
    }

    public String getLength() {
        return data[INDEX_LENGTH];
    }

    public void setNumber(String value) {
        String oldValue = data[INDEX_NUMBER];
        data[INDEX_NUMBER] = value;
        getPropertyChangeSupport().firePropertyChange("number", oldValue, value);
    }

    public String getNumber() {
        return data[INDEX_NUMBER];
    }

    public void setWeight(String value) {
        String oldValue = data[INDEX_WEIGHT];
        data[INDEX_WEIGHT] = value;
        getPropertyChangeSupport().firePropertyChange("weight", oldValue, value);
    }

    public String getWeight() {
        return data[INDEX_WEIGHT];
    }

    /**
     * Age field.
     *
     * @param value new value to set
     */
    public void setAge(String value) {
        String oldValue = data[INDEX_AGE];
        data[INDEX_AGE] = value;
        getPropertyChangeSupport().firePropertyChange("age", oldValue, value);
    }

    /**
     * Age field.
     *
     * @return age
     */
    public String getAge() {
        return data[INDEX_AGE];
    }

    public void setSurveyAsString(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("surveyAsString", oldValue, value);
    }

    public String getSurveyAsString() {
        return data[INDEX_SURVEY];
    }

    public void setYearAsString(String value) {
        String oldValue = data[INDEX_YEAR];
        data[INDEX_YEAR] = value;
        getPropertyChangeSupport().firePropertyChange("yearAsString", oldValue, value);
    }

    public String getYearAsString() {
        return data[INDEX_YEAR];
    }

    public void setHaulAsString(String value) {
        String oldValue = data[INDEX_HAUL];
        data[INDEX_HAUL] = value;
        getPropertyChangeSupport().firePropertyChange("haulAsString", oldValue, value);
    }

    public String getHaulAsString() {
        return data[INDEX_HAUL];
    }

    public void setSpeciesAsString(String value) {
        String oldValue = data[INDEX_SPECIES];
        data[INDEX_SPECIES] = value;
        getPropertyChangeSupport().firePropertyChange("speciesAsString", oldValue, value);
    }

    public String getSpeciesAsString() {
        return data[INDEX_SPECIES];
    }

    public void setSexAsString(String value) {
        String oldValue = data[INDEX_SEX];
        data[INDEX_SEX] = value;
        getPropertyChangeSupport().firePropertyChange("sexAsString", oldValue, value);
    }

    public String getSexAsString() {
        return data[INDEX_SEX];
    }

    public void setMaturityAsString(String value) {
        String oldValue = data[INDEX_MATURITY];
        data[INDEX_MATURITY] = value;
        getPropertyChangeSupport().firePropertyChange("maturityAsString", oldValue, value);
    }

    public String getMaturityAsString() {
        return data[INDEX_MATURITY];
    }

    public void setLengthAsString(String value) {
        String oldValue = data[INDEX_LENGTH];
        data[INDEX_LENGTH] = value;
        getPropertyChangeSupport().firePropertyChange("lengthAsString", oldValue, value);
    }

    public String getLengthAsString() {
        return data[INDEX_LENGTH];
    }

    public void setNumberAsString(String value) {
        String oldValue = data[INDEX_NUMBER];
        data[INDEX_NUMBER] = value;
        getPropertyChangeSupport().firePropertyChange("numberAsString", oldValue, value);
    }

    public String getNumberAsString() {
        return data[INDEX_NUMBER];
    }

    public void setWeightAsString(String value) {
        String oldValue = data[INDEX_WEIGHT];
        data[INDEX_WEIGHT] = value;
        getPropertyChangeSupport().firePropertyChange("weightAsString", oldValue, value);
    }

    public String getWeightAsString() {
        return data[INDEX_WEIGHT];
    }

    /**
     * Age field.
     *
     * @param value new value to set
     */
    public void setAgeAsString(String value) {
        String oldValue = data[INDEX_AGE];
        data[INDEX_AGE] = value;
        getPropertyChangeSupport().firePropertyChange("ageAsString", oldValue, value);
    }

    /**
     * Age field.
     *
     * @return age
     */
    public String getAgeAsString() {
        return data[INDEX_AGE];
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.data;

/**
 * Catch entity delegating to ordered array.
 *
 * "Campagne","Annee","Trait","Espece","Nombre","Poids"
 * "Survey","Year","Haul","Species","Number","Weight"
 *
 * @author chatellier
 */
public class Catch extends AbstractDataEntity {


    private static final long serialVersionUID = -3868116128268974801L;

    public static final String[] FR_HEADERS = {
            "Campagne", "Annee", "Trait", "Espece", "Nombre", "Poids"
    };

    public static final String[] EN_HEADERS = {
            "Survey", "Year", "Haul", "Species", "Number", "Weight"
    };

    public static final int INDEX_SURVEY = 1;

    public static final int INDEX_YEAR = 2;

    public static final int INDEX_HAUL = 3;

    public static final int INDEX_SPECIES = 4;

    public static final int INDEX_NUMBER = 5;

    public static final int INDEX_WEIGHT = 6;

    public String[] getHeaders() {
        return EN_HEADERS;
    }

    public void setSurvey(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("survey", oldValue, value);
    }

    public String getSurvey() {
        return data[INDEX_SURVEY];
    }

    public void setYear(String value) {
        String oldValue = data[INDEX_YEAR];
        data[INDEX_YEAR] = value;
        getPropertyChangeSupport().firePropertyChange("year", oldValue, value);
    }

    public String getYear() {
        return data[INDEX_YEAR];
    }

    public void setHaul(String value) {
        String oldValue = data[INDEX_HAUL];
        data[INDEX_HAUL] = value;
        getPropertyChangeSupport().firePropertyChange("haul", oldValue, value);
    }

    public String getHaul() {
        return data[INDEX_HAUL];
    }

    public void setSpecies(String value) {
        String oldValue = data[INDEX_SPECIES];
        data[INDEX_SPECIES] = value;
        getPropertyChangeSupport().firePropertyChange("species", oldValue, value);
    }

    public String getSpecies() {
        return data[INDEX_SPECIES];
    }

    public void setNumber(double value) {
        double oldValue = getNumber();
        data[INDEX_NUMBER] = String.valueOf(value);
        getPropertyChangeSupport().firePropertyChange("number", oldValue, value);
    }

    public double getNumber() {
        double result = Double.parseDouble(data[INDEX_NUMBER]);
        return result;
    }

    public void setWeight(double value) {
        double oldValue = getWeight();
        data[INDEX_WEIGHT] = String.valueOf(value);
        getPropertyChangeSupport().firePropertyChange("weight", oldValue, value);
    }

    public double getWeight() {
        double result = Double.parseDouble(data[INDEX_WEIGHT]);
        return result;
    }

    public void setSurveyAsString(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("surveyAsString", oldValue, value);
    }

    public String getSurveyAsString() {
        return data[INDEX_SURVEY];
    }

    public void setYearAsString(String value) {
        String oldValue = data[INDEX_YEAR];
        data[INDEX_YEAR] = value;
        getPropertyChangeSupport().firePropertyChange("yearAsString", oldValue, value);
    }

    public String getYearAsString() {
        return data[INDEX_YEAR];
    }

    public void setHaulAsString(String value) {
        String oldValue = data[INDEX_HAUL];
        data[INDEX_HAUL] = value;
        getPropertyChangeSupport().firePropertyChange("haulAsString", oldValue, value);
    }

    public String getHaulAsString() {
        return data[INDEX_HAUL];
    }

    public void setSpeciesAsString(String value) {
        String oldValue = data[INDEX_SPECIES];
        data[INDEX_SPECIES] = value;
        getPropertyChangeSupport().firePropertyChange("speciesAsString", oldValue, value);
    }

    public String getSpeciesAsString() {
        return data[INDEX_SPECIES];
    }

    public void setNumberAsString(String value) {
        String oldValue = data[INDEX_NUMBER];
        data[INDEX_NUMBER] = value;
        getPropertyChangeSupport().firePropertyChange("numberAsString", oldValue, value);
    }

    public String getNumberAsString() {
        return data[INDEX_NUMBER];
    }

    public void setWeightAsString(String value) {
        String oldValue = data[INDEX_WEIGHT];
        data[INDEX_WEIGHT] = value;
        getPropertyChangeSupport().firePropertyChange("weightAsString", oldValue, value);
    }

    public String getWeightAsString() {
        return data[INDEX_WEIGHT];
    }
}

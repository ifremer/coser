/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.data;

/**
 * Haul entity delegating to ordered array.
 *
 * "Campagne","Annee","Trait","Mois","Strate","SurfaceBalayee","Lat","Long","ProfMoy"
 * "Survey","Year","Haul","Month","Stratum","SweptSurface","Lat","Long","Depth"
 *
 * @author chatellier
 */
public class Haul extends AbstractDataEntity {


    private static final long serialVersionUID = 3931599935237369209L;

    public static final String[] FR_HEADERS = {
            "Campagne", "Annee", "Trait", "Mois", "Strate", "SurfaceBalayee", "Lat", "Long", "ProfMoy"
    };

    public static final String[] EN_HEADERS = {
            "Survey", "Year", "Haul", "Month", "Stratum", "SweptSurface", "Lat", "Long", "Depth"
    };

    public static final int INDEX_SURVEY = 1;

    public static final int INDEX_YEAR = 2;

    public static final int INDEX_HAUL = 3;

    public static final int INDEX_MONTH = 4;

    public static final int INDEX_STRATUM = 5;

    public static final int INDEX_SWEPT_SURFACE = 6;

    public static final int INDEX_LAT = 7;

    public static final int INDEX_LONG = 8;

    public static final int INDEX_DEPTH = 9;

    public String[] getHeaders() {
        return EN_HEADERS;
    }

    public void setSurvey(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("survey", oldValue, value);
    }

    public String getSurvey() {
        return data[INDEX_SURVEY];
    }

    public void setYear(String value) {
        String oldValue = data[INDEX_YEAR];
        data[INDEX_YEAR] = value;
        getPropertyChangeSupport().firePropertyChange("year", oldValue, value);
    }

    public String getYear() {
        return data[INDEX_YEAR];
    }

    public void setHaul(String value) {
        String oldValue = data[INDEX_HAUL];
        data[INDEX_HAUL] = value;
        getPropertyChangeSupport().firePropertyChange("haul", oldValue, value);
    }

    public String getHaul() {
        return data[INDEX_HAUL];
    }

    public void setMonth(String value) {
        String oldValue = data[INDEX_MONTH];
        data[INDEX_MONTH] = value;
        getPropertyChangeSupport().firePropertyChange("month", oldValue, value);
    }

    public String getMonth() {
        return data[INDEX_MONTH];
    }

    public void setStratum(String value) {
        String oldValue = data[INDEX_STRATUM];
        data[INDEX_STRATUM] = value;
        getPropertyChangeSupport().firePropertyChange("stratum", oldValue, value);
    }

    public String getStratum() {
        return data[INDEX_STRATUM];
    }

    public void setSweptSurface(String value) {
        String oldValue = data[INDEX_SWEPT_SURFACE];
        data[INDEX_SWEPT_SURFACE] = value;
        getPropertyChangeSupport().firePropertyChange("sweptSurface", oldValue, value);
    }

    public String getSweptSurface() {
        return data[INDEX_SWEPT_SURFACE];
    }

    public void setLat(String value) {
        String oldValue = data[INDEX_LAT];
        data[INDEX_LAT] = value;
        getPropertyChangeSupport().firePropertyChange("lat", oldValue, value);
    }

    public String getLat() {
        return data[INDEX_LAT];
    }

    public void setLong(String value) {
        String oldValue = data[INDEX_LONG];
        data[INDEX_LONG] = value;
        getPropertyChangeSupport().firePropertyChange("long", oldValue, value);
    }

    public String getLong() {
        return data[INDEX_LONG];
    }

    public void setDepth(String value) {
        String oldValue = data[INDEX_DEPTH];
        data[INDEX_DEPTH] = value;
        getPropertyChangeSupport().firePropertyChange("depth", oldValue, value);
    }

    public String getDepth() {
        return data[INDEX_DEPTH];
    }

    public void setSurveyAsString(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("surveyAsString", oldValue, value);
    }

    public String getSurveyAsString() {
        return data[INDEX_SURVEY];
    }

    public void setYearAsString(String value) {
        String oldValue = data[INDEX_YEAR];
        data[INDEX_YEAR] = value;
        getPropertyChangeSupport().firePropertyChange("yearAsString", oldValue, value);
    }

    public String getYearAsString() {
        return data[INDEX_YEAR];
    }

    public void setHaulAsString(String value) {
        String oldValue = data[INDEX_HAUL];
        data[INDEX_HAUL] = value;
        getPropertyChangeSupport().firePropertyChange("haulAsString", oldValue, value);
    }

    public String getHaulAsString() {
        return data[INDEX_HAUL];
    }

    public void setMonthAsString(String value) {
        String oldValue = data[INDEX_MONTH];
        data[INDEX_MONTH] = value;
        getPropertyChangeSupport().firePropertyChange("monthAsString", oldValue, value);
    }

    public String getMonthAsString() {
        return data[INDEX_MONTH];
    }

    public void setStratumAsString(String value) {
        String oldValue = data[INDEX_STRATUM];
        data[INDEX_STRATUM] = value;
        getPropertyChangeSupport().firePropertyChange("stratumAsString", oldValue, value);
    }

    public String getStratumAsString() {
        return data[INDEX_STRATUM];
    }

    public void setSweptSurfaceAsString(String value) {
        String oldValue = data[INDEX_SWEPT_SURFACE];
        data[INDEX_SWEPT_SURFACE] = value;
        getPropertyChangeSupport().firePropertyChange("sweptSurfaceAsString", oldValue, value);
    }

    public String getSweptSurfaceAsString() {
        return data[INDEX_SWEPT_SURFACE];
    }

    public void setLatAsString(String value) {
        String oldValue = data[INDEX_LAT];
        data[INDEX_LAT] = value;
        getPropertyChangeSupport().firePropertyChange("latAsString", oldValue, value);
    }

    public String getLatAsString() {
        return data[INDEX_LAT];
    }

    public void setLongAsString(String value) {
        String oldValue = data[INDEX_LONG];
        data[INDEX_LONG] = value;
        getPropertyChangeSupport().firePropertyChange("longAsString", oldValue, value);
    }

    public String getLongAsString() {
        return data[INDEX_LONG];
    }

    public void setDepthAsString(String value) {
        String oldValue = data[INDEX_DEPTH];
        data[INDEX_DEPTH] = value;
        getPropertyChangeSupport().firePropertyChange("depthAsString", oldValue, value);
    }

    public String getDepthAsString() {
        return data[INDEX_DEPTH];
    }
}

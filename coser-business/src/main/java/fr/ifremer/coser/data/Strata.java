/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.data;

/**
 * Strata entity delegating to ordered array.
 *
 * "Campagne","Strate","Surface"
 * "Survey","Stratum","Surface"
 *
 * @author chatellier
 */
public class Strata extends AbstractDataEntity {


    private static final long serialVersionUID = 3176546952537796549L;

    public static final String[] FR_HEADERS = {
            "Campagne", "Strate", "Surface"
    };

    public static final String[] EN_HEADERS = {
            "Survey", "Stratum", "Surface"
    };

    public static final int INDEX_SURVEY = 1;

    public static final int INDEX_STRATUM = 2;

    public static final int INDEX_SURFACE = 3;

    public String[] getHeaders() {
        return EN_HEADERS;
    }

    public void setSurvey(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("survey", oldValue, value);
    }

    public String getSurvey() {
        return data[INDEX_SURVEY];
    }

    public void setStratum(String value) {
        String oldValue = data[INDEX_STRATUM];
        data[INDEX_STRATUM] = value;
        getPropertyChangeSupport().firePropertyChange("stratum", oldValue, value);
    }

    public String getStratum() {
        return data[INDEX_STRATUM];
    }

    public void setSurface(String value) {
        String oldValue = data[INDEX_SURFACE];
        data[INDEX_SURFACE] = value;
        getPropertyChangeSupport().firePropertyChange("surface", oldValue, value);
    }

    public String getSurface() {
        return data[INDEX_SURFACE];
    }

    public void setSurveyAsString(String value) {
        String oldValue = data[INDEX_SURVEY];
        data[INDEX_SURVEY] = value;
        getPropertyChangeSupport().firePropertyChange("surveyAsString", oldValue, value);
    }

    public String getSurveyAsString() {
        return data[INDEX_SURVEY];
    }

    public void setStratumAsString(String value) {
        String oldValue = data[INDEX_STRATUM];
        data[INDEX_STRATUM] = value;
        getPropertyChangeSupport().firePropertyChange("stratumAsString", oldValue, value);
    }

    public String getStratumAsString() {
        return data[INDEX_STRATUM];
    }

    public void setSurfaceAsString(String value) {
        String oldValue = data[INDEX_SURFACE];
        data[INDEX_SURFACE] = value;
        getPropertyChangeSupport().firePropertyChange("surfaceAsString", oldValue, value);
    }

    public String getSurfaceAsString() {
        return data[INDEX_SURFACE];
    }

}

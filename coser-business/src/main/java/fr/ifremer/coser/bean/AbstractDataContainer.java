/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.storage.DataStorage;

import java.util.List;

/**
 * Common container code for {@link Control} and {@link Selection}.
 *
 * @author chatellier
 */
public class AbstractDataContainer extends AbstractEntity {


    private static final long serialVersionUID = 3963187480579783560L;

    public static final String PROPERTY_HISTORY_COMMANDS = "historyCommands";

    protected DataStorage dataCatch;

    /** Deleted catch (can be null). */
    protected DataStorage deletedDataCatch;

    protected DataStorage dataStrata;

    /** Deleted strata (can be null). */
    protected DataStorage deletedDataStrata;

    protected DataStorage dataHaul;

    /** Deleted haul (can be null). */
    protected DataStorage deletedDataHaul;

    protected DataStorage dataLength;

    /** Deleted length (can be null). */
    protected DataStorage deletedDataLength;

    /** L'historique des commandes do/undo . */
    protected List<Command> historyCommands;

    /**
     * Clear all data to force free memory.
     */
    public void clearData() {
        dataCatch = null;
        deletedDataCatch = null;
        dataStrata = null;
        deletedDataStrata = null;
        dataHaul = null;
        deletedDataHaul = null;
        dataLength = null;
        deletedDataLength = null;
        historyCommands = null;
    }

    public DataStorage getCatch() {
        return dataCatch;
    }

    public void setCatch(DataStorage dataCatch) {
        this.dataCatch = dataCatch;
    }

    public DataStorage getStrata() {
        return dataStrata;
    }

    public void setStrata(DataStorage dataStrata) {
        this.dataStrata = dataStrata;
    }

    public DataStorage getHaul() {
        return dataHaul;
    }

    public void setHaul(DataStorage dataHaul) {
        this.dataHaul = dataHaul;
    }

    public DataStorage getLength() {
        return dataLength;
    }

    public void setLength(DataStorage dataLength) {
        this.dataLength = dataLength;
    }

    public DataStorage getDeletedCatch() {
        return deletedDataCatch;
    }

    public void setDeletedCatch(DataStorage deletedDataCatch) {
        this.deletedDataCatch = deletedDataCatch;
    }

    public DataStorage getDeletedStrata() {
        return deletedDataStrata;
    }

    public void setDeletedStrata(DataStorage deletedDataStrata) {
        this.deletedDataStrata = deletedDataStrata;
    }

    public DataStorage getDeletedHaul() {
        return deletedDataHaul;
    }

    public void setDeletedHaul(DataStorage deletedDataHaul) {
        this.deletedDataHaul = deletedDataHaul;
    }

    public DataStorage getDeletedLength() {
        return deletedDataLength;
    }

    public void setDeletedLength(DataStorage deletedDataLength) {
        this.deletedDataLength = deletedDataLength;
    }

    public List<Command> getHistoryCommands() {
        return historyCommands;
    }

    public void setHistoryCommands(List<Command> historyCommand) {
        this.historyCommands = historyCommand;
        getPropertyChangeSupport().firePropertyChange(PROPERTY_HISTORY_COMMANDS, null, historyCommands);
    }

    public void addHistoryCommand(Command historyCommand) {
        historyCommands.add(historyCommand);
        getPropertyChangeSupport().firePropertyChange(PROPERTY_HISTORY_COMMANDS, null, historyCommands);
    }

    public void removeHistoryCommand(Command historyCommand) {
        historyCommands.remove(historyCommand);
        getPropertyChangeSupport().firePropertyChange(PROPERTY_HISTORY_COMMANDS, null, historyCommands);
    }

    public void clearHistoryCommands() {
        historyCommands.clear();
        getPropertyChangeSupport().firePropertyChange(PROPERTY_HISTORY_COMMANDS, null, historyCommands);
    }

    /**
     * Return {@code true} if data are loaded.
     *
     * If lists are non {@code null}.
     *
     * @return {@code true} if at least one list is not {@code null}
     */
    public boolean isDataLoaded() {
        boolean result = false;
        result |= dataCatch != null;
        result |= dataStrata != null;
        result |= dataHaul != null;
        result |= dataLength != null;
        return result;
    }
}

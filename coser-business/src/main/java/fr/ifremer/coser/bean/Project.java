/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.storage.DataStorage;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Project.
 *
 * For now, just composed of four simple String lists for data list.
 *
 * Also composed of loaded reftax (with header {@link #REFTAX_SPECIES_HEADER}).
 * "C_Perm";"NumSys";"NivSys";"C_VALIDE";"L_VALIDE";"AA_VALIDE";"C_TxPère";"Taxa"
 *
 * @author chatellier
 */
public class Project extends AbstractEntity {


    private static final long serialVersionUID = 8372568232663922521L;

    public static final String[] REFTAX_SPECIES_HEADER = {
            "C_Perm", "NumSys", "NivSys", "C_VALIDE", "L_VALIDE", "AA_VALIDE", "C_TxP\u00E8re", "Taxa"
    };

    public static final String[] TYPE_ESPECE_HEADER = {
            "Types", "Commentaire", "NumSys min", "NumSys max", "Code"
    };

    public static final String PROPERTY_SELECTIONS = "selections";

    protected String name;

    protected String author;

    /** Utilisé seulement lors de la création du projet (sinon, non valorisé). */
    protected String catchFile;

    /** Utilisé seulement lors de la création du projet (sinon, non valorisé). */
    protected String haulFile;

    /** Utilisé seulement lors de la création du projet (sinon, non valorisé). */
    protected String lengthFile;

    /** Utilisé seulement lors de la création du projet (sinon, non valorisé). */
    protected String strataFile;

    /** Nom original du fichier de captures. */
    protected String catchFileName;

    /** Nom original du fichier de traits. */
    protected String haulFileName;

    /** Nom original du fichier de tailles. */
    protected String lengthFileName;

    /** Nom original du fichier de strates. */
    protected String strataFileName;

    /** File des cartes (plus facile a gerer en tant que file dans l'ui, mais seul le filename est sauvegardé). */
    protected List<File> maps;

    /** Commentaire du projet. */
    protected String comment;

    protected Date creationDate;

    /** Project control. */
    protected Control control;

    /** Project selections. */
    protected Map<String, Selection> selections;

    /** Reftax SIH. */
    protected DataStorage refTaxSpecies;

    /** Map de cache entre les codes especes et la visualisation utilisateur. */
    protected LinkedHashMap<String, String> refTaxSpeciesMap;

    /** Type especes */
    protected DataStorage typeEspeces;

    /** Species field type used in storage. */
    protected SpeciesFieldType storageSpeciesType = SpeciesFieldType.C_Valide;

    /** Species field type used for user display. */
    protected SpeciesFieldType displaySpeciesType = SpeciesFieldType.C_Valide;

    /** Species field type used for rsufi output files. */
    protected SpeciesFieldType outputSpeciesType = SpeciesFieldType.C_Valide;

    public Project() {

    }

    public Project(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;
        getPropertyChangeSupport().firePropertyChange("name", oldValue, name);
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        String oldValue = this.author;
        this.author = author;
        getPropertyChangeSupport().firePropertyChange("author", oldValue, author);
    }

    public String getCatchFile() {
        return catchFile;
    }

    public void setCatchFile(String catchFile) {
        String oldValue = this.catchFile;
        this.catchFile = catchFile;
        getPropertyChangeSupport().firePropertyChange("catchFile", oldValue, catchFile);
    }

    public String getHaulFile() {
        return haulFile;
    }

    public void setHaulFile(String haulFile) {
        String oldValue = this.haulFile;
        this.haulFile = haulFile;
        getPropertyChangeSupport().firePropertyChange("haulFile", oldValue, haulFile);
    }

    public String getLengthFile() {
        return lengthFile;
    }

    public void setLengthFile(String lengthFile) {
        String oldValue = this.lengthFile;
        this.lengthFile = lengthFile;
        getPropertyChangeSupport().firePropertyChange("lengthFile", oldValue, lengthFile);
    }

    public String getStrataFile() {
        return strataFile;
    }

    public void setStrataFile(String strataFile) {
        String oldValue = this.strataFile;
        this.strataFile = strataFile;
        getPropertyChangeSupport().firePropertyChange("strataFile", oldValue, strataFile);
    }

    public String getCatchFileName() {
        return catchFileName;
    }

    public void setCatchFileName(String catchFileName) {
        this.catchFileName = catchFileName;
    }

    public String getHaulFileName() {
        return haulFileName;
    }

    public void setHaulFileName(String haulFileName) {
        this.haulFileName = haulFileName;
    }

    public String getLengthFileName() {
        return lengthFileName;
    }

    public void setLengthFileName(String lengthFileName) {
        this.lengthFileName = lengthFileName;
    }

    public String getStrataFileName() {
        return strataFileName;
    }

    public void setStrataFileName(String strataFileName) {
        this.strataFileName = strataFileName;
    }

    public void setMaps(List<File> maps) {
        this.maps = maps;
    }

    public List<File> getMaps() {
        return maps;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = this.comment;
        this.comment = comment;
        getPropertyChangeSupport().firePropertyChange("comment", oldValue, comment);
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Control getControl() {
        return control;
    }

    public void setControl(Control control) {
        Control oldValue = this.control;
        this.control = control;
        getPropertyChangeSupport().firePropertyChange("control", oldValue, control);
    }

    public Map<String, Selection> getSelections() {
        return selections;
    }

    public void setSelections(Map<String, Selection> selections) {
        this.selections = selections;
        getPropertyChangeSupport().firePropertyChange(PROPERTY_SELECTIONS, null, selections);
    }

    public void addSelections(Selection selection) {
        this.selections.put(selection.getName(), selection);
        getPropertyChangeSupport().firePropertyChange(PROPERTY_SELECTIONS, null, selections);
    }

    public DataStorage getRefTaxSpecies() {
        return refTaxSpecies;
    }

    public void setRefTaxSpecies(DataStorage refTaxSpecies) {
        this.refTaxSpecies = refTaxSpecies;
    }

    /**
     * Recuperation de la map de cache code especes/visualisation utilisateur.
     *
     * @return refTax species map
     * @since 1.3
     */
    public LinkedHashMap<String, String> getRefTaxSpeciesMap() {
        return this.refTaxSpeciesMap;
    }

    /**
     * Modification de la map de cache code especes/visualisation utilisateur.
     *
     * @param refTaxSpeciesMap new map
     * @since 1.3
     */
    public void setRefTaxSpeciesMap(LinkedHashMap<String, String> refTaxSpeciesMap) {
        this.refTaxSpeciesMap = refTaxSpeciesMap;
    }

    /**
     * Retourne le nom de visualisation de l'espèce conformement à la configuration
     * du projet.
     *
     * @return le label espece
     * @since 1.3
     */
    public String getDisplaySpeciesText(String speciesCode) {
        String speciesText = speciesCode;
        if (refTaxSpeciesMap.containsKey(speciesCode)) {
            speciesText = refTaxSpeciesMap.get(speciesCode);
        }
        return speciesText;
    }

    public DataStorage getTypeEspeces() {
        return typeEspeces;
    }

    public void setTypeEspeces(DataStorage typeEspeces) {
        this.typeEspeces = typeEspeces;
    }

    public SpeciesFieldType getStorageSpeciesType() {
        return storageSpeciesType;
    }

    public void setStorageSpeciesType(SpeciesFieldType storageSpeciesType) {
        SpeciesFieldType oldValue = this.storageSpeciesType;
        this.storageSpeciesType = storageSpeciesType;
        getPropertyChangeSupport().firePropertyChange("storageSpeciesType", oldValue, storageSpeciesType);
    }

    public SpeciesFieldType getDisplaySpeciesType() {
        return displaySpeciesType;
    }

    public void setDisplaySpeciesType(SpeciesFieldType displaySpeciesType) {
        SpeciesFieldType oldValue = this.displaySpeciesType;
        this.displaySpeciesType = displaySpeciesType;
        getPropertyChangeSupport().firePropertyChange("displaySpeciesType", oldValue, displaySpeciesType);
    }

    public SpeciesFieldType getOutputSpeciesType() {
        return outputSpeciesType;
    }

    public void setOutputSpeciesType(SpeciesFieldType outputSpeciesType) {
        SpeciesFieldType oldValue = this.outputSpeciesType;
        this.outputSpeciesType = outputSpeciesType;
        getPropertyChangeSupport().firePropertyChange("outputSpeciesType", oldValue, outputSpeciesType);
    }

    /**
     * Force all container data clear to force free memory.
     */
    public void clearData() {
        if (control != null) {
            control.clearData();
        }
        if (selections != null) {
            for (Selection selection : selections.values()) {
                selection.clearData();
            }
        }
    }

    public Properties toProperties() {
        Properties props = new Properties();
        if (author != null) {
            props.setProperty("project.author", author);
        }
        if (catchFileName != null) {
            props.setProperty("project.catchFileName", catchFileName);
        }
        if (lengthFileName != null) {
            props.setProperty("project.lengthFileName", lengthFileName);
        }
        if (haulFileName != null) {
            props.setProperty("project.haulFileName", haulFileName);
        }
        if (strataFileName != null) {
            props.setProperty("project.strataFileName", strataFileName);
        }
        if (CollectionUtils.isNotEmpty(maps)) {
            StringBuilder mapsAsString = new StringBuilder();
            Iterator<File> itMaps = maps.iterator();
            while (itMaps.hasNext()) {
                mapsAsString.append(itMaps.next().getName());
                if (itMaps.hasNext()) {
                    mapsAsString.append(',');
                }
            }
            props.setProperty("project.maps", mapsAsString.toString());
        }
        if (comment != null) {
            props.setProperty("project.comment", comment);
        }
        if (creationDate != null) {
            props.setProperty("project.creationdate", String.valueOf(creationDate.getTime()));
        }
        if (storageSpeciesType != null) {
            props.setProperty("project.storageSpeciesType", storageSpeciesType.toString());
        }
        if (displaySpeciesType != null) {
            props.setProperty("project.displaySpeciesType", displaySpeciesType.toString());
        }
        if (outputSpeciesType != null) {
            props.setProperty("project.outputSpeciesType", outputSpeciesType.toString());
        }
        return props;
    }

    public void fromProperties(Properties props, File mapDirectory) {
        if (props.containsKey("project.author")) {
            setAuthor(props.getProperty("project.author"));
        }
        if (props.containsKey("project.catchFileName")) {
            setCatchFileName(props.getProperty("project.catchFileName"));
        }
        if (props.containsKey("project.lengthFileName")) {
            setLengthFileName(props.getProperty("project.lengthFileName"));
        }
        if (props.containsKey("project.haulFileName")) {
            setHaulFileName(props.getProperty("project.haulFileName"));
        }
        if (props.containsKey("project.strataFileName")) {
            setStrataFileName(props.getProperty("project.strataFileName"));
        }
        if (StringUtils.isNotBlank(props.getProperty("project.maps"))) {
            List<String> mapsAsString = CoserUtils.splitAsList(props.getProperty("project.maps"));
            List<File> maps = new ArrayList<File>();
            for (String mapAsString : mapsAsString) {
                File mapFile = new File(mapDirectory, mapAsString);
                maps.add(mapFile);
            }
            setMaps(maps);
        }
        if (props.containsKey("project.comment")) {
            setComment(props.getProperty("project.comment"));
        }
        if (props.containsKey("project.creationdate")) {
            Date date = new Date(Long.parseLong(props.getProperty("project.creationdate")));
            setCreationDate(date);
        }

        // species field type was added in 1.3+
        // missing values for previous project are considered as L_Valide
        if (props.containsKey("project.storageSpeciesType")) {
            setStorageSpeciesType(SpeciesFieldType.valueOf(props.getProperty("project.storageSpeciesType")));
        } else {
            setStorageSpeciesType(SpeciesFieldType.C_Valide);
        }
        if (props.containsKey("project.displaySpeciesType")) {
            setDisplaySpeciesType(SpeciesFieldType.valueOf(props.getProperty("project.displaySpeciesType")));
        } else {
            setDisplaySpeciesType(SpeciesFieldType.C_Valide);
        }
        if (props.containsKey("project.outputSpeciesType")) {
            setOutputSpeciesType(SpeciesFieldType.valueOf(props.getProperty("project.outputSpeciesType")));
        } else {
            setOutputSpeciesType(SpeciesFieldType.C_Valide);
        }
    }

    /**
     * Retourne le nom de stockage d'un fichier de données suivant la categories
     * de fichier demandé. Les fichiers sont stockés avec leurs noms originaux
     * (sauf les fichiers de réference) donc il n'est pas fixe.
     *
     * @param category category to get file name
     * @param suffix   suffix to add into file name
     * @return storage file name
     * @since 1.5
     */
    public String getDataStorageFileName(CoserConstants.Category category, String suffix) {

        String result = null;

        switch (category) {
            case CATCH:
                result = getCatchFileName();
                break;
            case HAUL:
                result = getHaulFileName();
                break;
            case LENGTH:
                result = getLengthFileName();
                break;
            case STRATA:
                result = getStrataFileName();
                break;
        }

        if (suffix != null) {
            result = CoserUtils.addSuffixBeforeExtension(result, suffix);
        }

        return result;
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

import fr.ifremer.coser.CoserTechnicalException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * RSufi result.
 *
 * @author chatellier
 */
public class RSufiResult extends AbstractEntity {


    private static final long serialVersionUID = -1337710082675120199L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(RSufiResult.class);

    protected Date creationDate;

    protected String name;

    protected String rsufiVersion;

    /** Zone d'application du resultat. */
    protected String zone;

    /** Utilisé seulement pour la validation (sinon, non valorisé). */
    protected String estComIndPath;

    /** Utilisé seulement pour la validation (sinon, non valorisé). */
    protected String estPopIndPath;

    protected String estComIndName;

    protected String estPopIndName;

    /** Utilisé seulement pour la validation (sinon, non valorisé). */
    protected String mapsPath;

    /** Utilisé seulement dans l'ui de visu des resultats (presence de carte ou non). */
    protected boolean mapsAvailable;

    /** Liste des nom de fichiers/dossier present dans other files */
    protected List<File> otherFiles;

    /** Result publiable. */
    protected boolean publiableResult;

    /** Result flaged as indicators result. */
    protected boolean indicatorsResult;

    /** Result flaged as maps result. */
    protected boolean mapsResult;

    /** Data download allowed. */
    protected boolean dataAllowed;

    public RSufiResult() {

    }

    public RSufiResult(String name) {
        this();
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;
        getPropertyChangeSupport().firePropertyChange("name", oldValue, name);
    }

    public String getRsufiVersion() {
        return rsufiVersion;
    }

    public void setRsufiVersion(String rsufiVersion) {
        String oldValue = this.rsufiVersion;
        this.rsufiVersion = rsufiVersion;
        getPropertyChangeSupport().firePropertyChange("name", oldValue, name);
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        String oldValue = this.zone;
        this.zone = zone;
        getPropertyChangeSupport().firePropertyChange("zone", oldValue, zone);
    }

    public String getEstComIndPath() {
        return estComIndPath;
    }

    public void setEstComIndPath(String estComIndPath) {
        String oldValue = this.estComIndPath;
        this.estComIndPath = estComIndPath;
        getPropertyChangeSupport().firePropertyChange("estComIndPath", oldValue, estComIndPath);
    }

    public String getEstPopIndPath() {
        return estPopIndPath;
    }

    public void setEstPopIndPath(String estPopIndPath) {
        String oldValue = this.estPopIndPath;
        this.estPopIndPath = estPopIndPath;
        getPropertyChangeSupport().firePropertyChange("estPopIndPath", oldValue, estPopIndPath);
    }

    public String getEstComIndName() {
        return estComIndName;
    }

    public void setEstComIndName(String estComIndName) {
        String oldValue = this.estComIndName;
        this.estComIndName = estComIndName;
        getPropertyChangeSupport().firePropertyChange("estComIndName", oldValue, estComIndName);
    }

    public String getEstPopIndName() {
        return estPopIndName;
    }

    public void setEstPopIndName(String estPopIndName) {
        String oldValue = this.estPopIndName;
        this.estPopIndName = estPopIndName;
        getPropertyChangeSupport().firePropertyChange("estPopIndName", oldValue, estPopIndName);
    }

    public void setMapsPath(String mapsPath) {
        String oldValue = this.mapsPath;
        this.mapsPath = mapsPath;
        getPropertyChangeSupport().firePropertyChange("mapsPath", oldValue, mapsPath);
    }

    public String getMapsPath() {
        return mapsPath;
    }

    public boolean isMapsAvailable() {
        return mapsAvailable;
    }

    public void setMapsAvailable(boolean mapsAvailable) {
        this.mapsAvailable = mapsAvailable;
    }

    public List<File> getOtherFiles() {
        return otherFiles;
    }

    public void setOtherFiles(List<File> otherFiles) {
        this.otherFiles = otherFiles;
    }

    public void setPubliableResult(boolean publiableResult) {
        boolean oldValue = this.publiableResult;
        this.publiableResult = publiableResult;
        getPropertyChangeSupport().firePropertyChange("publiableResult", oldValue, publiableResult);
    }

    public boolean isPubliableResult() {
        return publiableResult;
    }

    public boolean isIndicatorsResult() {
        return indicatorsResult;
    }

    public void setIndicatorsResult(boolean indicatorsResult) {
        boolean oldValue = this.indicatorsResult;
        this.indicatorsResult = indicatorsResult;
        getPropertyChangeSupport().firePropertyChange("indicatorsResult", oldValue, indicatorsResult);
    }

    public boolean isMapsResult() {
        return mapsResult;
    }

    public void setMapsResult(boolean mapsResult) {
        boolean oldValue = this.mapsResult;
        this.mapsResult = mapsResult;
        getPropertyChangeSupport().firePropertyChange("mapsResult", oldValue, mapsResult);
    }

    public boolean isDataAllowed() {
        return dataAllowed;
    }

    public void setDataAllowed(boolean dataAllowed) {
        boolean oldValue = this.dataAllowed;
        this.dataAllowed = dataAllowed;
        getPropertyChangeSupport().firePropertyChange("dataAllowed", oldValue, dataAllowed);
    }

    public Properties toProperties() {
        Properties props = new Properties();
        if (getCreationDate() != null) {
            props.setProperty("result.creationdate", String.valueOf(creationDate.getTime()));
        }
        if (getRsufiVersion() != null) {
            props.setProperty("result.rsufiversion", getRsufiVersion());
        }
        if (getZone() != null) {
            props.setProperty("result.zone", getZone());
        }
        if (getEstComIndName() != null) {
            props.setProperty("result.estComIndName", getEstComIndName());
        }
        if (getEstPopIndName() != null) {
            props.setProperty("result.estPopIndName", getEstPopIndName());
        }
        props.setProperty("result.publiableResult", String.valueOf(isPubliableResult()));
        props.setProperty("result.indicatorsResult", String.valueOf(isIndicatorsResult()));
        props.setProperty("result.mapsResult", String.valueOf(isMapsResult()));
        props.setProperty("result.dataAllowed", String.valueOf(isDataAllowed()));
        return props;
    }

    public void fromProperties(Properties props) {
        if (props.containsKey("result.creationdate")) {
            Date date = new Date(Long.parseLong(props.getProperty("result.creationdate")));
            setCreationDate(date);
        }
        if (props.containsKey("result.rsufiversion")) {
            setRsufiVersion(props.getProperty("result.rsufiversion"));
        }
        if (props.containsKey("result.zone")) {
            setZone(props.getProperty("result.zone"));
        }
        if (props.containsKey("result.estComIndName")) {
            setEstComIndName(props.getProperty("result.estComIndName"));
        }
        if (props.containsKey("result.estPopIndName")) {
            setEstPopIndName(props.getProperty("result.estPopIndName"));
        }
        if (props.containsKey("result.publiableResult")) {
            setPubliableResult(Boolean.parseBoolean(props.getProperty("result.publiableResult")));
        }
        if (props.containsKey("result.dataAllowed")) {
            setDataAllowed(Boolean.parseBoolean(props.getProperty("result.dataAllowed")));
        }
        if (props.containsKey("result.indicatorsResult")) {
            setIndicatorsResult(Boolean.parseBoolean(props.getProperty("result.indicatorsResult")));
        }
        if (props.containsKey("result.mapsResult")) {
            setMapsResult(Boolean.parseBoolean(props.getProperty("result.mapsResult")));
        }
    }

    /**
     * Save rsufi result (only properties file).
     *
     * @param rsufiResultDirectory rsufiresult directory
     * @since 1.5
     */
    public void save(File rsufiResultDirectory) {

        OutputStream outputStream = null;
        try {
            // sauvegarde des informations du resultat (properties)
            File propertiesFile = new File(rsufiResultDirectory, "result.properties");
            Properties props = toProperties();
            outputStream = new FileOutputStream(propertiesFile);
            props.store(outputStream, "Saved by " + getClass());
            outputStream.close();
            if (log.isDebugEnabled()) {
                log.debug("Saving result properties file : " + propertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't save result", ex);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }
}

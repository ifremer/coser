/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

import fr.ifremer.coser.CoserUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Project selection.
 *
 * @author chatellier
 */
public class Selection extends AbstractDataContainer {


    private static final long serialVersionUID = -1484104459960459854L;

    public static final String PROPERTY_VALIDATED = "validated";

    public static final String PROPERTY_RSUFI_RESULTS = "rsufiResults";

    protected String name;

    protected String description;

    protected List<String> allYears;

    protected List<String> selectedYears;

    protected List<String> selectedStrata;

    protected String comment;

    protected double occurrenceFilter;

    protected double densityFilter;

    /** L1 : liste des especes. */
    protected List<String> selectedSpecies;

    /** L2 : */
    protected List<String> selectedSpeciesOccDens;

    /** L3 : */
    protected List<String> selectedSpeciesSizeAllYear;

    /** L4 : */
    protected List<String> selectedSpeciesMaturity;

    /** L1 : comment. */
    protected String selectedSpeciesComment;

    /** L2 : comment. */
    protected String selectedSpeciesOccDensComment;

    /** L3 : comment. */
    protected String selectedSpeciesSizeAllYearComment;

    /** L4 : comment. */
    protected String selectedSpeciesMaturityComment;

    protected boolean validated;

    /** Liste des nom de fichiers/dossier present dans other files */
    protected List<File> otherFiles;

    protected List<RSufiResult> rsufiResults;

    public Selection() {
        // c'est plus facile de ne pas gerer le cas "null" partout dans le code
        allYears = Collections.EMPTY_LIST;
        selectedYears = Collections.EMPTY_LIST;
        selectedStrata = Collections.EMPTY_LIST;
        selectedSpecies = Collections.EMPTY_LIST;
        selectedSpeciesOccDens = Collections.EMPTY_LIST;
        selectedSpeciesSizeAllYear = Collections.EMPTY_LIST;
        selectedSpeciesMaturity = Collections.EMPTY_LIST;
    }

    public Selection(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;
        getPropertyChangeSupport().firePropertyChange("name", oldValue, name);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        String oldValue = this.description;
        this.description = description;
        getPropertyChangeSupport().firePropertyChange("description", oldValue, description);
    }

    public List<String> getAllYears() {
        return allYears;
    }

    public void setAllYears(List<String> allYears) {
        this.allYears = allYears;
    }

    public List<String> getSelectedYears() {
        return selectedYears;
    }

    public void setSelectedYears(List<String> selectedYears) {
        this.selectedYears = selectedYears;
    }

    public List<String> getSelectedStrata() {
        return selectedStrata;
    }

    public void setSelectedStrata(List<String> selectedStrata) {
        this.selectedStrata = selectedStrata;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = this.comment;
        this.comment = comment;
        getPropertyChangeSupport().firePropertyChange("comment", oldValue, comment);
    }

    public double getOccurrenceFilter() {
        return occurrenceFilter;
    }

    public void setOccurrenceFilter(double occurrenceFilter) {
        double oldValue = this.occurrenceFilter;
        this.occurrenceFilter = occurrenceFilter;
        getPropertyChangeSupport().firePropertyChange("occurrenceFilter", oldValue, occurrenceFilter);
    }

    public double getDensityFilter() {
        return densityFilter;
    }

    public void setDensityFilter(double densityFilter) {
        double oldValue = this.densityFilter;
        this.densityFilter = densityFilter;
        getPropertyChangeSupport().firePropertyChange("densityFilter", oldValue, densityFilter);
    }

    public List<String> getSelectedSpecies() {
        return selectedSpecies;
    }

    public void setSelectedSpecies(List<String> selectedSpecies) {
        this.selectedSpecies = selectedSpecies;
    }

    public List<String> getSelectedSpeciesOccDens() {
        return selectedSpeciesOccDens;
    }

    public void setSelectedSpeciesOccDens(List<String> selectedSpeciesOccDens) {
        this.selectedSpeciesOccDens = selectedSpeciesOccDens;
    }

    public List<String> getSelectedSpeciesSizeAllYear() {
        return selectedSpeciesSizeAllYear;
    }

    public void setSelectedSpeciesSizeAllYear(
            List<String> selectedSpeciesSizeAllYear) {
        this.selectedSpeciesSizeAllYear = selectedSpeciesSizeAllYear;
    }

    public List<String> getSelectedSpeciesMaturity() {
        return selectedSpeciesMaturity;
    }

    public void setSelectedSpeciesMaturity(List<String> selectedSpeciesMaturity) {
        this.selectedSpeciesMaturity = selectedSpeciesMaturity;
    }

    public String getSelectedSpeciesComment() {
        return selectedSpeciesComment;
    }

    public void setSelectedSpeciesComment(String selectedSpeciesComment) {
        String oldValue = this.selectedSpeciesComment;
        this.selectedSpeciesComment = selectedSpeciesComment;
        getPropertyChangeSupport().firePropertyChange("selectedSpeciesComment", oldValue, selectedSpeciesComment);
    }

    public String getSelectedSpeciesOccDensComment() {
        return selectedSpeciesOccDensComment;
    }

    public void setSelectedSpeciesOccDensComment(
            String selectedSpeciesOccDensComment) {
        String oldValue = this.selectedSpeciesOccDensComment;
        this.selectedSpeciesOccDensComment = selectedSpeciesOccDensComment;
        getPropertyChangeSupport().firePropertyChange("selectedSpeciesOccDensComment", oldValue, selectedSpeciesOccDensComment);
    }

    public String getSelectedSpeciesSizeAllYearComment() {
        return selectedSpeciesSizeAllYearComment;
    }

    public void setSelectedSpeciesSizeAllYearComment(
            String selectedSpeciesSizeAllYearComment) {
        String oldValue = this.selectedSpeciesSizeAllYearComment;
        this.selectedSpeciesSizeAllYearComment = selectedSpeciesSizeAllYearComment;
        getPropertyChangeSupport().firePropertyChange("selectedSpeciesSizeAllYearComment", oldValue, selectedSpeciesSizeAllYearComment);
    }

    public String getSelectedSpeciesMaturityComment() {
        return selectedSpeciesMaturityComment;
    }

    public void setSelectedSpeciesMaturityComment(
            String selectedSpeciesMaturityComment) {
        String oldValue = this.selectedSpeciesMaturityComment;
        this.selectedSpeciesMaturityComment = selectedSpeciesMaturityComment;
        getPropertyChangeSupport().firePropertyChange("selectedSpeciesMaturityComment", oldValue, selectedSpeciesMaturityComment);
    }

    public List<File> getOtherFiles() {
        return otherFiles;
    }

    public void setOtherFiles(List<File> otherFiles) {
        this.otherFiles = otherFiles;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        boolean oldValue = this.validated;
        this.validated = validated;
        getPropertyChangeSupport().firePropertyChange(PROPERTY_VALIDATED, oldValue, validated);
    }

    public List<RSufiResult> getRsufiResults() {
        return rsufiResults;
    }

    public void setRsufiResults(List<RSufiResult> rsufiResults) {
        this.rsufiResults = rsufiResults;
        getPropertyChangeSupport().firePropertyChange(PROPERTY_RSUFI_RESULTS, null, rsufiResults);
    }

    public Properties toProperties() {
        Properties props = new Properties();
        if (description != null) {
            props.setProperty("selection.description", description);
        }
        if (getAllYears() != null) {
            props.setProperty("selection.allYears", StringUtils.join(getAllYears(), ','));
        }
        if (getSelectedYears() != null) {
            props.setProperty("selection.selectedYears", StringUtils.join(getSelectedYears(), ','));
        }
        if (getSelectedStrata() != null) {
            props.setProperty("selection.selectedStrata", StringUtils.join(getSelectedStrata(), ','));
        }
        if (comment != null) {
            props.setProperty("selection.comment", comment);
        }
        props.setProperty("selection.occurrenceFilter", String.valueOf(occurrenceFilter));
        props.setProperty("selection.densityFilter", String.valueOf(densityFilter));
        if (getSelectedSpecies() != null) {
            props.setProperty("selection.selectedSpecies", StringUtils.join(getSelectedSpecies(), ','));
        }
        if (getSelectedSpeciesOccDens() != null) {
            props.setProperty("selection.selectedSpeciesOccDens", StringUtils.join(getSelectedSpeciesOccDens(), ','));
        }
        if (getSelectedSpeciesSizeAllYear() != null) {
            props.setProperty("selection.selectedSpeciesSizeAllYear", StringUtils.join(getSelectedSpeciesSizeAllYear(), ','));
        }
        if (getSelectedSpeciesMaturity() != null) {
            props.setProperty("selection.selectedSpeciesMaturity", StringUtils.join(getSelectedSpeciesMaturity(), ','));
        }
        if (selectedSpeciesComment != null) {
            props.setProperty("selection.selectedSpeciesComment", selectedSpeciesComment);
        }
        if (selectedSpeciesOccDensComment != null) {
            props.setProperty("selection.selectedSpeciesOccDensComment", selectedSpeciesOccDensComment);
        }
        if (selectedSpeciesSizeAllYearComment != null) {
            props.setProperty("selection.selectedSpeciesSizeAllYearComment", selectedSpeciesSizeAllYearComment);
        }
        if (selectedSpeciesMaturityComment != null) {
            props.setProperty("selection.selectedSpeciesMaturityComment", selectedSpeciesMaturityComment);
        }
        props.setProperty("selection.validated", String.valueOf(isValidated()));

        return props;
    }

    public void fromProperties(Properties props) {
        if (props.containsKey("selection.description")) {
            setDescription(props.getProperty("selection.description"));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.allYears"))) {
            setAllYears(CoserUtils.splitAsList(props.getProperty("selection.allYears")));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.selectedYears"))) {
            setSelectedYears(CoserUtils.splitAsList(props.getProperty("selection.selectedYears")));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.selectedStrata"))) {
            setSelectedStrata(CoserUtils.splitAsList(props.getProperty("selection.selectedStrata")));
        }
        if (props.containsKey("selection.comment")) {
            setComment(props.getProperty("selection.comment"));
        }
        if (props.containsKey("selection.occurrenceFilter")) {
            setOccurrenceFilter(Double.parseDouble(props.getProperty("selection.occurrenceFilter")));
        }
        if (props.containsKey("selection.densityFilter")) {
            setDensityFilter(Double.parseDouble(props.getProperty("selection.densityFilter")));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.selectedSpecies"))) {
            setSelectedSpecies(CoserUtils.splitAsList(props.getProperty("selection.selectedSpecies")));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.selectedSpeciesOccDens"))) {
            setSelectedSpeciesOccDens(CoserUtils.splitAsList(props.getProperty("selection.selectedSpeciesOccDens")));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.selectedSpeciesSizeAllYear"))) {
            setSelectedSpeciesSizeAllYear(CoserUtils.splitAsList(props.getProperty("selection.selectedSpeciesSizeAllYear")));
        }
        if (StringUtils.isNotBlank(props.getProperty("selection.selectedSpeciesMaturity"))) {
            setSelectedSpeciesMaturity(CoserUtils.splitAsList(props.getProperty("selection.selectedSpeciesMaturity")));
        }
        if (props.containsKey("selection.selectedSpeciesComment")) {
            setSelectedSpeciesComment(props.getProperty("selection.selectedSpeciesComment"));
        }
        if (props.containsKey("selection.selectedSpeciesOccDensComment")) {
            setSelectedSpeciesOccDensComment(props.getProperty("selection.selectedSpeciesOccDensComment"));
        }
        if (props.containsKey("selection.selectedSpeciesSizeAllYearComment")) {
            setSelectedSpeciesSizeAllYearComment(props.getProperty("selection.selectedSpeciesSizeAllYearComment"));
        }
        if (props.containsKey("selection.selectedSpeciesMaturityComment")) {
            setSelectedSpeciesMaturityComment(props.getProperty("selection.selectedSpeciesMaturityComment"));
        }
        if (props.containsKey("selection.validated")) {
            setValidated(Boolean.parseBoolean(props.getProperty("selection.validated")));
        }
    }
}

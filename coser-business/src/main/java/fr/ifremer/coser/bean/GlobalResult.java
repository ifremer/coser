package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

/**
 * To box a rsufi result, or a echobase result.
 *
 * TODO Should use the result request API instead, but no more time for this.
 *
 * Created on 3/17/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GlobalResult implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final RSufiResultPath rsufiProject;

    protected final EchoBaseProject echobaseProject;

    public GlobalResult(RSufiResultPath result) {
        Preconditions.checkNotNull(result);
        this.rsufiProject = result;
        this.echobaseProject = null;
    }

    public GlobalResult(EchoBaseProject echoBaseProject) {
        Preconditions.checkNotNull(echoBaseProject);
        this.echobaseProject = echoBaseProject;
        this.rsufiProject = null;
    }

    public static boolean isRsufiProject(File projectDirectory) {
        boolean result = projectDirectory.isDirectory();
        if (result) {
            File file = new File(projectDirectory, EchoBaseProject.METADATA_FILE);
            result = file.exists() && file.isFile();
        }
        return result;
    }

    public static boolean isEchobaseProject(File projectDirectory) {
        boolean result = projectDirectory.isDirectory();
        if (result) {
            File file = new File(projectDirectory, EchoBaseProject.METADATA_FILE);
            result = file.exists() && file.isFile();
        }
        return result;
    }

    public boolean isRsufi() {
        return rsufiProject != null;
    }

    public boolean isEchoBase() {
        return echobaseProject != null;
    }

    public Date getCreationDate() {
        Date result;
        if (isRsufi()) {
            result = rsufiProject.getRsufiResult().getCreationDate();
        } else {
            result = echobaseProject.getCreationDate();
        }
        return result;
    }

    public String getZone() {
        String result;
        if (isRsufi()) {
            result = rsufiProject.getRsufiResult().getZone();
        } else {
            result = echobaseProject.getZoneName();
        }
        return result;
    }

    public RSufiResultPath getRsufiProject() {
        return rsufiProject;
    }

    public EchoBaseProject getEchobaseProject() {
        return echobaseProject;
    }

    public boolean isPubliableResult() {
        return isRsufi() ? rsufiProject.getRsufiResult().isPubliableResult() : echobaseProject.isPubliableResult();
    }

    public boolean isIndicatorsResult() {
        return !isRsufi() || rsufiProject.getRsufiResult().isPubliableResult();
    }

    public void setIndicatorsResult(boolean indicatorsResult) {
        if (isRsufi()) {
            rsufiProject.getRsufiResult().setIndicatorsResult(indicatorsResult);
        }
    }

    public boolean isMapsResult() {
        return !isRsufi() || rsufiProject.getRsufiResult().isMapsResult();
    }

    public void setMapsResult(boolean mapsResult) {
        if (isRsufi()) {
            rsufiProject.getRsufiResult().setMapsResult(mapsResult);
        }
    }

    public boolean isDataAllowed() {
        return !isRsufi() || rsufiProject.getRsufiResult().isDataAllowed();
    }

    public void setDataAllowed(boolean dataAllowed) {
        if (isRsufi()) {
            rsufiProject.getRsufiResult().setDataAllowed(dataAllowed);
        }
    }

    @Override
    public int hashCode() {
        return isRsufi() ? rsufiProject.hashCode() : echobaseProject.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = obj instanceof GlobalResult &&
                         (isRsufi() ? rsufiProject.equals(obj) : echobaseProject.equals(obj));
        return result;
    }

    public String getName() {
        return isRsufi() ? String.format("%s/%s/%s", rsufiProject.getProject().getName(),
                                         rsufiProject.getSelection().getName(),
                                         rsufiProject.getRsufiResult().getName()) : echobaseProject.getName();
    }
}

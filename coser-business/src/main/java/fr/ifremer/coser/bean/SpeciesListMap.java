package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import fr.ifremer.coser.result.repository.legacy.LegacyPredicates;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorageWalker;
import fr.ifremer.coser.storage.DataStorages;
import org.nuiton.i18n.I18n;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class SpeciesListMap implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Species' list storage.
     */
    protected final DataStorage storage;

    public SpeciesListMap(File speciesFile) {
        storage = DataStorages.load(speciesFile);
    }

    public String getSpeciesListName(Locale locale, String specesList) {
        // recherche de la traduction de l'id de liste
        // les liste sont a1, T1, T2 ...

        Predicate<String[]> predicate = LegacyPredicates.speciesListPredicate(specesList);
        GetNameWalker walker = new GetNameWalker(locale, specesList);
        DataStorages.walk(storage, predicate, walker);
        String translation = walker.getTranslation();

//        String listLetter = String.valueOf(specesList.charAt(0));
//        String translation = "## " + specesList + " not found ##";
//        Iterator<String[]> typeIterator = iterator(true);
//        while (typeIterator.hasNext()) {
//            // "Types";"Commentaire";"NumSys min";"NumSys max";"Code"
//            String[] tupleType = typeIterator.next();
//            if (tupleType[4].equals(listLetter)) {
//
//                // gestion du groupe "Tous"
//                // cas special, c'est la seule valeur du fichier
//                // code type espece qui a besoin d'une traduction
//                if (tupleType[4].equalsIgnoreCase("T")) {
//
//                    translation = I18n.l(locale, "coser.business.specesList.nameForAll", specesList.charAt(1));
////                    if (locale != null && "fr".equals(locale.getLanguage())) {
////                        translation = "Tous Liste " + specesList.charAt(1);
////                    } else if (locale != null && "en".equals(locale.getLanguage())) {
////                        translation = "Todo Lista " + specesList.charAt(1);
////                    } else {
////                        translation = "All List " + specesList.charAt(1);
////                    }
//                } else {
//                    // ajout de la traduction du nom de liste plus le numéro
//                    translation = I18n.l(locale, "coser.business.specesList.name", tupleType[0], specesList.charAt(1));
////                    if (locale != null && "fr".equals(locale.getLanguage())) {
////                        translation = tupleType[0] + " Liste " + specesList.charAt(1);
////                    } else if (locale != null && "en".equals(locale.getLanguage())) {
////                        translation = tupleType[0] + " Lista " + specesList.charAt(1);
////                    } else {
////                        translation = tupleType[0] + " List " + specesList.charAt(1);
////                    }
//                }
//                break;
//            }
//        }
        return translation;
    }

    public Map<String, String> getSpeciesSubMap(Locale locale, Collection<String> speciesList) {

        // keep incoming order
        Map<String, String> result = Maps.newLinkedHashMap();

        if (speciesList != null) {

            for (String species : speciesList) {
                String speciesLabel = getSpeciesListName(locale, species);
                result.put(species, speciesLabel);
            }
        }

        return result;
    }

    protected Iterator<String[]> iterator(boolean skipFirstLine) {
        // "Types";"Commentaire";"NumSys min";"NumSys max";"Code"
        Iterator<String[]> iterator = storage.iterator(skipFirstLine);
        return iterator;
    }

    public static class GetNameWalker implements DataStorageWalker {

        protected String translation;

        protected String speciesList;

        protected final Locale locale;

        public GetNameWalker(Locale locale, String speciesList) {
            this.speciesList = speciesList;
            this.locale = locale;
            translation = "## " + speciesList + " not found ##";
        }

        public String getTranslation() {
            return translation;
        }

        @Override
        public void onRow(String... tuple) {

            // gestion du groupe "Tous"
            // cas special, c'est la seule valeur du fichier
            // code type espece qui a besoin d'une traduction
            if (tuple[4].equalsIgnoreCase("T")) {
                translation = I18n.l(locale, "coser.business.specesList.nameForAll", speciesList.charAt(1));
            } else {
                // ajout de la traduction du nom de liste plus le numéro
                translation = I18n.l(locale, "coser.business.specesList.name", tuple[0], speciesList.charAt(1));
            }
        }
    }
}

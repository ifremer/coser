/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

import java.util.Properties;

/**
 * Control entity.
 *
 * @author chatellier
 */
public class Control extends AbstractDataContainer {


    private static final long serialVersionUID = 3693938021315541627L;

    public static final String PROPERTY_VALIDATED = "validated";

    protected String comment;

    /** If control has been explicitly validated. */
    protected boolean validated;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = this.comment;
        this.comment = comment;
        getPropertyChangeSupport().firePropertyChange("comment", oldValue, comment);
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        boolean oldValue = this.validated;
        this.validated = validated;
        getPropertyChangeSupport().firePropertyChange(PROPERTY_VALIDATED, oldValue, validated);
    }

    public Properties toProperties() {
        Properties props = new Properties();
        if (comment != null) {
            props.setProperty("control.comment", comment);
        }
        props.setProperty("control.validated", String.valueOf(validated));
        return props;
    }

    public void fromProperties(Properties props) {
        if (props.containsKey("control.comment")) {
            setComment(props.getProperty("control.comment"));
        }
        if (props.containsKey("control.validated")) {
            setValidated(Boolean.parseBoolean(props.getProperty("control.validated")));
        }
    }
}

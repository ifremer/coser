package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Date;
import java.util.Properties;

/**
 * Represent the storage of the EchoBase project published to CoserWeb application.
 *
 * Created on 3/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseProject implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String METADATA_FILE = "project.echobase";

    /**
     * Base directory of the project.
     */
    protected final File basedir;

    protected String author;

    protected String facadeName;

    protected String zoneName;

    protected String surveyName;

    protected String comment;

    protected Date creationDate;

    /** Result publiable. */
    protected boolean publiableResult;

    public EchoBaseProject(File basedir) {
        Preconditions.checkNotNull(basedir);
        this.basedir = basedir;
    }

    public File getBasedir() {
        return basedir;
    }

    public String getName() {
        return getBasedir().getName();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getFacadeName() {
        return facadeName;
    }

    public void setFacadeName(String facadeName) {
        this.facadeName = facadeName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isPubliableResult() {
        return publiableResult;
    }

    public void setPubliableResult(boolean publiableResult) {
        this.publiableResult = publiableResult;
    }

    public File getSpeciesDefinitionFile() {
        return new File(basedir, "species.csv");
    }

    public File getPopulationIndicatorsFile() {
        return new File(basedir, "populationIndicators.csv");
    }

    public File getCommunityIndicatorsFile() {
        return new File(basedir, "communityIndicators.csv");
    }

    public File getMapsDirectory() {
        return new File(basedir, "maps");
    }

    public File getRawDataDirectory() {
        return new File(basedir, "source");
    }

    /**
     * Load a project from his basedir.
     *
     * @throws IOException if could not read the meta data file
     */
    public void load() throws IOException {
        File metadataFile = getMetaDataFile();
        Reader reader = Files.newReader(metadataFile, Charsets.UTF_8);
        try {
            Properties p = new Properties();
            p.load(reader);
            reader.close();
            fromProperties(p);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    /**
     * Save the project.
     *
     * @throws IOException if could not write the meta-data file
     */
    public void save() throws IOException {

        File metadataFile = getMetaDataFile();
        Writer writer = Files.newWriter(metadataFile, Charsets.UTF_8);
        try {
            Properties p = toProperties();
            p.store(writer, "Saved by " + EchoBaseProject.class.getName());
            writer.close();
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EchoBaseProject)) return false;
        EchoBaseProject that = (EchoBaseProject) o;
        return basedir.equals(that.basedir);
    }

    @Override
    public int hashCode() {
        return basedir.hashCode();
    }

    protected File getMetaDataFile() {
        return new File(basedir, METADATA_FILE);
    }

    protected Properties toProperties() {
        Properties props = new Properties();
        if (author != null) {
            props.setProperty("project.author", author);
        }
        if (facadeName != null) {
            props.setProperty("project.facadeName", facadeName);
        }
        if (zoneName != null) {
            props.setProperty("project.zoneName", zoneName);
        }
        if (surveyName != null) {
            props.setProperty("project.surveyName", surveyName);
        }
        if (comment != null) {
            props.setProperty("project.comment", comment);
        }
        if (creationDate != null) {
            props.setProperty("project.creationDate", String.valueOf(creationDate.getTime()));
        }
        props.setProperty("project.publiableResult", String.valueOf(publiableResult));

        return props;
    }

    protected void fromProperties(Properties props) {
        if (props.containsKey("project.author")) {
            setAuthor(props.getProperty("project.author"));
        }
        if (props.containsKey("project.facadeName")) {
            setFacadeName(props.getProperty("project.facadeName"));
        }
        if (props.containsKey("project.zoneName")) {
            setZoneName(props.getProperty("project.zoneName"));
        }
        if (props.containsKey("project.surveyName")) {
            setSurveyName(props.getProperty("project.surveyName"));
        }
        if (props.containsKey("project.comment")) {
            setComment(props.getProperty("project.comment"));
        }
        if (props.containsKey("project.creationDate")) {
            Date date = new Date(Long.parseLong(props.getProperty("project.creationDate")));
            setCreationDate(date);
        }
        if (props.containsKey("project.publiableResult")) {
            boolean v = Boolean.valueOf(props.getProperty("project.publiableResult"));
            setPubliableResult(v);
        }
    }

    public static FilenameFilter newMapSpeciesFilenameFilter(String surveyName) {
        RegexFileFilter result = new RegexFileFilter("^" + surveyName + "_[^_]+?\\.png");
        return result;
    }

    public static Function<File, String> newMapFileToSpeciesCode(String surveyName) {
        return new MapFileToSpeciesCodeFunction(surveyName);
    }

    public static Function<String, String> newSpeciesCodeToMapFileName(String surveyName) {
        return new SpeciesCodeToMapFileNameFunction(surveyName);
    }

    protected static class MapFileToSpeciesCodeFunction implements Function<File, String> {

        private final String zoneName;

        public MapFileToSpeciesCodeFunction(String zoneName) {
            this.zoneName = zoneName;
        }

        @Override
        public String apply(File input) {
            String fileName = input.getName();
            String species = fileName.substring(zoneName.length() + 1);
            species = StringUtils.substringBefore(species, ".");
            return species;
        }
    }

    protected static class SpeciesCodeToMapFileNameFunction implements Function<String, String> {

        private final String zoneName;

        public SpeciesCodeToMapFileNameFunction(String zoneName) {
            this.zoneName = zoneName;
        }

        @Override
        public String apply(String input) {

            String fileName = zoneName + "_" + input + ".png";
            return fileName;
        }
    }
}

package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorageWalker;
import fr.ifremer.coser.storage.DataStorages;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Contains the localized indicator definitions.
 *
 * Created on 3/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class IndicatorMap implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(IndicatorMap.class);

    /**
     * To get french translations.
     */
    protected static final String KEY_FRENCH = Locale.FRENCH.getLanguage();

    /**
     * To get english translations.
     */
    protected static final String KEY_ENGLISH = Locale.ENGLISH.getLanguage();

    /**
     * To get spanish translations.
     */
    protected static final String KEY_SPANISH = "es";

    /**
     * To get unit translations.
     */
    protected static final String KEY_UNIT = "unit";

    /**
     * Universe of safe keys usable in the cache.
     *
     * @see #getSafeLocaleKey(String)
     */
    protected static final Set<String> SAFE_KEYS = Collections.unmodifiableSet(
            Sets.newHashSet(KEY_FRENCH, KEY_ENGLISH, KEY_UNIT, KEY_SPANISH));

    /**
     * Localized Indicator map (id, locale > translation, id, "unit" > unit).
     */
    protected final MultiKeyMap<String, String> map;

    /**
     * Set of indicator ids.
     */
    protected final Set<String> ids;

    public IndicatorMap(File indicatorsFile) {
        Preconditions.checkNotNull(indicatorsFile);
        Preconditions.checkArgument(indicatorsFile.exists(), "Indicator file: " + indicatorsFile + " does not exist.");

        if (log.isInfoEnabled()) {
            log.info("Loading Indicator file: " + indicatorsFile);
        }
        this.map = new MultiKeyMap<String, String>();
        final Set<String> indicatorIs = Sets.newHashSet();

        DataStorage storage = DataStorages.load(indicatorsFile);
        DataStorages.walk(storage, new DataStorageWalker() {

            @Override
            public void onRow(String... row) {
                String indicatorCode = row[0];
                indicatorIs.add(indicatorCode);
                map.put(indicatorCode, KEY_FRENCH, row[1]);
                map.put(indicatorCode, KEY_ENGLISH, row[2]);
                map.put(indicatorCode, KEY_SPANISH, row[3]);
                map.put(indicatorCode, KEY_UNIT, row[4]);
            }
        });
        this.ids = Collections.unmodifiableSet(indicatorIs);
    }

    public Set<String> getIds() {
        return ids;
    }

    public String getIndicatorValue(Locale locale, String indicator) {
        Preconditions.checkNotNull(locale);
        Preconditions.checkNotNull(indicator);
        String localeCode = getSafeLocaleKey(locale.getLanguage());
        String translation = getIndicatorValue(localeCode, indicator);
        return translation;
    }

    public String getIndicatorUnit(String indicator) {
        Preconditions.checkNotNull(indicator);
        return getIndicatorValue(KEY_UNIT, indicator);
    }

    public Map<String, String> getIndicatorsValues(Locale locale,
                                                   Collection<String> indicatorList) {

        Map<String, String> result = Maps.newTreeMap();

        if (indicatorList != null) {

            String localeCode = getSafeLocaleKey(locale.getLanguage());
            for (String indicator : indicatorList) {
                String indicatorLabel = getIndicatorValue(localeCode, indicator);
                result.put(indicator, indicatorLabel);
            }
        }
        return result;
    }

    protected String getIndicatorValue(String localeCode, String indicator) {
        Preconditions.checkNotNull(localeCode);
        Preconditions.checkNotNull(indicator);
        Preconditions.checkArgument(SAFE_KEYS.contains(localeCode));
        String translation = map.get(indicator, localeCode);
        if (translation == null) {
            translation = "##" + indicator + "##" + localeCode;
        }
        return translation;
    }

    protected String getSafeLocaleKey(String localeCode) {
        if (!SAFE_KEYS.contains(localeCode)) {
            localeCode = KEY_ENGLISH; // en by default
        }
        return localeCode;
    }

}

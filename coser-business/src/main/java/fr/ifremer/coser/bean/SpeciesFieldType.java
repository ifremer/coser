/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

/**
 * Since 1.3, species can be managed by C_Perm, AA_Valide or L_Valide.
 * They are always presented to user with latin name.
 *
 * @author echatellier
 * @since 1.3
 */
public enum SpeciesFieldType {

    /** Species alpha code (BONA, BONAOST...) */
    C_Valide("C_Valide"),

    /** Species num code (1, 2, 3...). */
    C_PERM("C_Perm"),

    /** Species latin name. */
    L_Valide("L_Valide");

    protected String reftaxField;

    private SpeciesFieldType(String reftaxField) {
        this.reftaxField = reftaxField;
    }

    public String getReftaxField() {
        return reftaxField;
    }
}

package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorages;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class SpeciesMap implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Species' storage.
     */
    protected final DataStorage storage;

    /**
     * Cache of species definition.
     */
    protected Map<String, String> speciesMap;


    public SpeciesMap(File speciesFile) {
        storage = DataStorages.load(speciesFile);
    }

    public DataStorage getStorage() {
        return storage;
    }

    public Iterator<String[]> iterator(boolean skipFirstLine) {
        // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
        Iterator<String[]> iterator = storage.iterator(skipFirstLine);
        return iterator;
    }

    public Map<String, String> getSpeciesMap() {
        if (speciesMap == null) {
            speciesMap = Maps.newTreeMap();

            // load species file
            Iterator<String[]> iterator = iterator(true);
            while (iterator.hasNext()) {
                String[] tuple = iterator.next();
                String speciesCode = tuple[3];
                String speciesLabel = tuple[4] + " " + tuple[5];
                speciesMap.put(speciesCode, speciesLabel);
            }
            speciesMap = Collections.unmodifiableMap(speciesMap);
        }
        return speciesMap;
    }

    public String getSpeciesName(String species) {
        return getSpeciesMap().get(species);
    }

    public Map<String, String> getSpeciesSubMap(Collection<String> speciesList) {

        Map<String, String> result = Maps.newTreeMap();

        if (speciesList != null) {

            Map<String, String> map = getSpeciesMap();
            for (String species : speciesList) {
                String speciesLabel = map.get(species);
                if (StringUtils.isNotEmpty(speciesLabel)) {
                    result.put(species, speciesLabel);
                }
            }
        }

        return result;
    }

    /**
     * Retourne le nom d'affichage d'une especes pour les rapports.
     *
     * @param species species code
     * @return species display name
     * @since 1.5
     */
    public String getReportDisplayName(String species) {
        String displayName = null;

        Iterator<String[]> iterator = iterator(true);
        while (iterator.hasNext()) {
            String[] tuple = iterator.next();

            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String speciesCode = tuple[3];
            if (speciesCode.equals(species)) {
                // nom + auteur (sans ajout de parenthese : important)
                displayName = tuple[4] + " " + tuple[5];
                break;
            }
        }

        return displayName;
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.bean;

import com.google.common.base.Preconditions;

import java.io.Serializable;

/**
 * Object utilitaire represantant un path vers un résultat (projet / selection
 * / rsufiresult) utilisé par l'interface d'admin et l'upload de resultat
 * vers l'interface web.
 *
 * L'egalité et le hashcode sont basé sur les {@link Project#name},
 * {@link Selection#name}, et {@link RSufiResult#name}.
 *
 * Les attribut sont finaux car le hashcode est basé dessus.
 *
 * @author chatellier
 */
public class RSufiResultPath implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Final project. */
    protected final Project project;

    /** Final selection. */
    protected final Selection selection;

    /** Final rsufiresult. */
    protected final RSufiResult rsufiResult;

    /**
     * Constructor with full path.
     *
     * @param project     project (can't be null)
     * @param selection   selection (can't be null)
     * @param rsufiResult rsufiresult (can't be null)
     */
    public RSufiResultPath(Project project, Selection selection, RSufiResult rsufiResult) {
        Preconditions.checkNotNull(project);
        Preconditions.checkNotNull(selection);
        Preconditions.checkNotNull(rsufiResult);
        this.project = project;
        this.selection = selection;
        this.rsufiResult = rsufiResult;
    }

    /**
     * Get project.
     *
     * @return project
     */
    public Project getProject() {
        return project;
    }

    /**
     * Get selection.
     *
     * @return selection
     */
    public Selection getSelection() {
        return selection;
    }

    /**
     * Get result.
     *
     * @return result
     */
    public RSufiResult getRsufiResult() {
        return rsufiResult;
    }

    @Override
    public int hashCode() {
        int result = 31 + project.getName().hashCode();
        result = 31 * result + selection.getName().hashCode();
        result = 31 * result + rsufiResult.getName().hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = true;

        if (!(obj instanceof RSufiResultPath)) {
            result = false;
        } else {
            RSufiResultPath other = (RSufiResultPath) obj;
            result &= project.getName().equals(other.project.getName());
            result &= selection.getName().equals(other.selection.getName());
            result &= rsufiResult.getName().equals(other.rsufiResult.getName());
        }

        return result;
    }
}

package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorageWalker;
import fr.ifremer.coser.storage.DataStorages;
import fr.ifremer.coser.storage.MemoryDataStorage;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Contains all usefull methods on zone definitions.
 *
 * Created on 3/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ZoneMap implements Serializable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneMap.class);

    private static final long serialVersionUID = 1L;

    protected final DataStorage storage;

    public ZoneMap(File zonesFile) {
        // l'operation n'est pas obligatoire pour tous les clients
        // lourd, le fichier peut donc ne pas exister
        if (zonesFile == null || zonesFile.isFile()) {

            if (log.isInfoEnabled()) {
                log.info("Using zone file: " + zonesFile);
            }

            storage = DataStorages.load(zonesFile);
        } else {
            storage = new MemoryDataStorage();
        }
    }

    /**
     * Retourne les nom d'une zone (avec la facade, l'année et la serie) en
     * fonction de l'id de la zone.
     *
     * @param zoneId zone id
     * @return zone name (or {@code null} if not found)
     */
    public String getZoneFullName(String zoneId) {
        String resultName = null;
        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
        int zoneIndex = storage.indexOf(zoneId);
        if (zoneIndex != -1) {
            String[] tuple = storage.get(zoneIndex);
            resultName = tuple[2];
            resultName += " - " + tuple[3];
            resultName += " - " + tuple[4];
            resultName += " - " + tuple[5];
        }
        return resultName;
    }

    /**
     * Retourne les nom d'une zone (avec l'année et la serie) en
     * fonction de l'id de la zone.
     *
     * @param zoneId zone id
     * @return zone name (or {@code null} if not found)
     */
    public String getZoneFullNameWithNoFacade(String zoneId) {
        String resultName = null;
        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
        int zoneIndex = storage.indexOf(zoneId);
        if (zoneIndex != -1) {
            String[] tuple = storage.get(zoneIndex);
            resultName = tuple[3];
            resultName += " - " + tuple[4];
            resultName += " - " + tuple[5];
        }
        return resultName;
    }

    /**
     * Recupere la liste des meta info pour chaque id de zone sous forme de Map.
     *
     * @param locale locale
     * @return zone meta info map
     */
    public Map<String, String> getZoneMetaInfo(Locale locale) {
        Map<String, String> result = new HashMap<String, String>();

        GetZoneMetaInfoWalker walker = new GetZoneMetaInfoWalker(locale, result);
        DataStorages.walk(storage, walker);

//        Iterator<String[]> itZone = storage.iterator(true);
//        while (itZone.hasNext()) {
//            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
//            String[] tuple = itZone.next();
//            if (locale != null && "fr".equals(locale.getLanguage())) {
//                result.put(tuple[0], tuple[6]);
//            } else if (locale != null && "es".equals(locale.getLanguage())) {
//                result.put(tuple[0], tuple[8]);
//            } else {
//                result.put(tuple[0], tuple[7]);
//            }
//        }

        return result;
    }

    /**
     * Recupere la liste des cartes pour chaque id de zone sous forme de Map.
     *
     * @return zone images map
     */
    public Map<String, String> getZonePictures() {

        Map<String, String> result = new HashMap<String, String>();

        GetZonePictureWalker walker = new GetZonePictureWalker(result);
        DataStorages.walk(storage, walker);

//        Iterator<String[]> itZone = storage.iterator(true);
//        while (itZone.hasNext()) {
//            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
//            String[] tuple = itZone.next();
//            result.put(tuple[0], tuple[9]);
//        }

        return result;
    }

    /**
     * Retourne les zones disponible par facade.
     *
     * @return couple facadeid/list<zoneid>
     */
    public Map<String, List<String>> getZoneByFacade() {

        Map<String, List<String>> zonesByFacade = new HashMap<String, List<String>>();
        GetZoneByFacadeWalker walker = new GetZoneByFacadeWalker(zonesByFacade);
        DataStorages.walk(storage, walker);

//        // get subzone for main zone
//        Iterator<String[]> itZone = storage.iterator(true);
//        while (itZone.hasNext()) {
//            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
//            String[] tuple = itZone.next();
//            String facadeid = tuple[1];
//            String zoneid = tuple[0];
//            List<String> zones = zonesByFacade.get(facadeid);
//            if (zones == null) {
//                zones = new ArrayList<String>();
//                zonesByFacade.put(facadeid, zones);
//            }
//            zones.add(zoneid);
//        }

        return zonesByFacade;
    }

    /**
     * Get facades list (as facadeid/facadename).
     *
     * @return facades map
     */
    public Map<String, String> getFacades() {
        Map<String, String> facades = new LinkedHashMap<String, String>();

        GetFacadesWalker walker = new GetFacadesWalker(facades);
        DataStorages.walk(storage, walker);

//        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
//        Iterator<String[]> itZone = storage.iterator(true);
//        while (itZone.hasNext()) {
//            String[] tuple = itZone.next();
//            // on a pas d'id pour les facades
//            facades.put(tuple[1], tuple[2]);
//        }
        return facades;
    }

    /**
     * Get all zones for a given facade.
     *
     * If no facade is given, then return all zones.
     *
     * @return zones for the given facade (or all zones if no facade is given)
     */
    public List<String> getZonesForFacade(final String facade) {

        List<String> facades = Lists.newArrayList();

        GetZonesForFacadeWalker walker = new GetZonesForFacadeWalker(facade, facades);
        FacadeNameOrAllPredicate predicate = new FacadeNameOrAllPredicate(facade);
        DataStorages.walk(storage, predicate, walker);

//        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
//        Iterator<String[]> itZone = storage.iterator(true);
//        while (itZone.hasNext()) {
//            String[] tuple = itZone.next();
//            if (facade == null || tuple[1].equals(facade)) {
//                facades.add(tuple[0]);
//            }
//        }
        return facades;
    }

    public DataStorage getStorage() {
        return storage;
    }

    public Map<String, String> getSubZonesMap(String zoneId, List<String> allowedZones) {
        Map<String, String> result = Maps.newHashMap();

        if (allowedZones != null) {

            if (allowedZones.contains(zoneId)) {
                String zoneFullName = getZoneFullNameWithNoFacade(zoneId);
                result.put(zoneId, zoneFullName);
            }
        }

        return result;
    }

    protected static class GetZonePictureWalker implements DataStorageWalker {

        protected final Map<String, String> result;

        public GetZonePictureWalker(Map<String, String> result) {
            this.result = result;
        }

        @Override
        public void onRow(String... tuple) {

            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            String pictureName = tuple[9];
            String zoneId = tuple[0];
            result.put(zoneId, pictureName);

            if (StringUtils.isBlank(pictureName)) {
                if (log.isWarnEnabled()) {
                    log.warn(String.format("No picture defined for zone: %s", zoneId));
                }
            }

            if (log.isDebugEnabled()) {
                log.debug(String.format("zone: %s -- picture: %s", zoneId, pictureName));
            }

        }
    }

    protected static class GetZonesForFacadeWalker implements DataStorageWalker {

        protected final String facade;

        protected final List<String> facades;

        public GetZonesForFacadeWalker(String facade, List<String> facades) {
            this.facade = facade;
            this.facades = facades;
        }

        @Override
        public void onRow(String... tuple) {

            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            facades.add(tuple[0]);

        }
    }

    protected static class GetFacadesWalker implements DataStorageWalker {

        protected final Map<String, String> facades;

        public GetFacadesWalker(Map<String, String> facades) {
            this.facades = facades;
        }

        @Override
        public void onRow(String... tuple) {

            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            // on a pas d'id pour les facades
            facades.put(tuple[1], tuple[2]);

        }
    }

    protected static class GetZoneMetaInfoWalker implements DataStorageWalker {

        protected final Map<String, String> result;

        protected final int localeIndex;

        public GetZoneMetaInfoWalker(Locale locale, Map<String, String> result) {

            this.result = result;

            if (locale != null && "fr".equals(locale.getLanguage())) {
                localeIndex = 6;
            } else if (locale != null && "es".equals(locale.getLanguage())) {
                localeIndex = 8;
            } else {
                localeIndex = 7;
            }

        }

        @Override
        public void onRow(String... tuple) {

            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            result.put(tuple[0], tuple[localeIndex]);

        }
    }

    protected static class GetZoneByFacadeWalker implements DataStorageWalker {

        protected final Map<String, List<String>> zonesByFacade;

        public GetZoneByFacadeWalker(Map<String, List<String>> zonesByFacade) {
            this.zonesByFacade = zonesByFacade;
        }


        @Override
        public void onRow(String... tuple) {

            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            String facadeid = tuple[1];
            String zoneid = tuple[0];
            List<String> zones = zonesByFacade.get(facadeid);
            if (zones == null) {
                zones = new ArrayList<String>();
                zonesByFacade.put(facadeid, zones);
            }
            zones.add(zoneid);

        }
    }

    protected static class FacadeNameOrAllPredicate implements Predicate<String[]> {

        private final String facade;

        public FacadeNameOrAllPredicate(String facade) {
            this.facade = facade;
        }

        @Override
        public boolean apply(String[] input) {
            return facade == null || input[1].equals(facade);
        }
    }

}

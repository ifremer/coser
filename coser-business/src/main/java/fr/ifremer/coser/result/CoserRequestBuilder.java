package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.request.CopyRepositoryRequest;
import fr.ifremer.coser.result.request.CoserRequestExtractTypeListAware;
import fr.ifremer.coser.result.request.CoserRequestFacadeAware;
import fr.ifremer.coser.result.request.CoserRequestIndicatorAware;
import fr.ifremer.coser.result.request.CoserRequestRepositoryResultTypeAware;
import fr.ifremer.coser.result.request.CoserRequestRepositoryTypeAware;
import fr.ifremer.coser.result.request.CoserRequestSpeciesAware;
import fr.ifremer.coser.result.request.CoserRequestZoneAware;
import fr.ifremer.coser.result.request.CoserRequestZoneListAware;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.util.DataType;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * To build requests.
 *
 * TO build a new request, instanciate a new build, add some parameters and then use the {@link #toRequest()} method.
 *
 * Example:
 * <pre>
 *     MapRequest r = CoserRequestBuilder.
 *                      newBuilder(locale,MapRequest.class).
 *                      addFacade(facade).
 *                      addZone(zone).
 *                      toRequest();
 * </pre>
 * Created on 3/7/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserRequestBuilder<R extends CoserRequest> {

    protected static final String PARAMETER_RESULT_TYPE = "resultType";

    protected static final String PARAMETER_REPOSITORY_TYPE = "resultRepositoryType";

    protected static final String PARAMETER_INDICATOR = "indicator";

    protected static final String PARAMETER_ZONE_LIST = "zoneList";

    protected static final String PARAMETER_SPECIES = "species";

    protected static final String PARAMETER_ZONE = "zone";

    protected static final String PARAMETER_FACADE = "facade";

    protected static final String PARAMETER_SPECIES_LIST = "speciesList";

    protected static final String PARAMETER_POPULATION_INDICATOR_LIST = "populationIndicatorList";

    protected static final String PARAMETER_COMMUNITY_INDICATOR_LIST = "communityIndicatorList";

    protected static final String PARAMETER_EXTRACT_TYPE_LIST = "extractTypeList";


    protected static final String PARAMETER_TARGET_DIRECTORY = "targetDirectory";

    /**
     * Locale to inject in request.
     */
    protected final Locale locale;

    /**
     * Type of request to build.
     */
    protected final Class<R> requestType;

    /**
     * Holder of parameters to inject in request.
     */
    protected final Map<String, Object> parameters;

    public static <R extends CoserRequest> CoserRequestBuilder<R> newBuilder(Locale locale,
                                                                             Class<R> requestType) {
        return new CoserRequestBuilder<R>(locale, requestType);
    }

    protected CoserRequestBuilder(Locale locale, Class<R> requestType) {
        Preconditions.checkNotNull(locale);
        Preconditions.checkNotNull(requestType);
        this.locale = locale;
        this.requestType = requestType;
        this.parameters = Maps.newTreeMap();
    }

    public CoserRequestBuilder<R> addFacade(String facade) {
        parameters.put(PARAMETER_FACADE, facade);
        return this;
    }

    public CoserRequestBuilder<R> addZone(String zone) {
        parameters.put(PARAMETER_ZONE, zone);
        return this;
    }

    public CoserRequestBuilder<R> addSpecies(String species) {
        parameters.put(PARAMETER_SPECIES, species);
        return this;
    }

    public CoserRequestBuilder<R> addIndicator(String indicator) {
        parameters.put(PARAMETER_INDICATOR, indicator);
        return this;
    }

    public CoserRequestBuilder<R> addExtractTypeList(List<DataType> extractTypeList) {
        parameters.put(PARAMETER_EXTRACT_TYPE_LIST, extractTypeList);
        return this;
    }

    public CoserRequestBuilder<R> addZoneList(List<String> zoneList) {
        parameters.put(PARAMETER_ZONE_LIST, zoneList);
        return this;
    }

    public CoserRequestBuilder<R> addSpeciesList(List<String> speciesList) {
        parameters.put(PARAMETER_SPECIES_LIST, speciesList);
        return this;
    }

    public CoserRequestBuilder<R> addPopulationIndicatorList(List<String> populationIndicatorList) {
        parameters.put(PARAMETER_POPULATION_INDICATOR_LIST, populationIndicatorList);
        return this;
    }

    public CoserRequestBuilder<R> addCommunityIndicatorList(List<String> communityIndicatorList) {
        parameters.put(PARAMETER_COMMUNITY_INDICATOR_LIST, communityIndicatorList);
        return this;
    }

    public CoserRequestBuilder<R> addRepositoryType(String repositoryType) {
        parameters.put(PARAMETER_REPOSITORY_TYPE, repositoryType);
        return this;
    }

    public CoserRequestBuilder<R> addResultType(ResultType resultType) {
        parameters.put(PARAMETER_RESULT_TYPE, resultType);
        return this;
    }

    public CoserRequestBuilder<R> addTargetDirectory(File targetDirectory) {
        parameters.put(PARAMETER_TARGET_DIRECTORY, targetDirectory);
        return this;
    }

    public R toRequest() {
        try {
            R request = requestType.newInstance();
            flushCoserRequestFacadeAware(request);
            flushCoserRequestZoneAware(request);
            flushCoserRequestZoneListAware(request);
            flushCoserRequestSpeciesAware(request);
            flushCoserRequestIndicatorAware(request);
            flushCoserRequestRepositoryTypeAware(request);
            flushCoserRequestRepositoryResultTypeAware(request);
            flushCoserRequestExtractTypeListAware(request);
            flushExtractRawDataAndResultsRequest(request);
            flushCopyRepositoryRequest(request);
            return request;
        } catch (InstantiationException e) {
            throw new CoserTechnicalException(e);
        } catch (IllegalAccessException e) {
            throw new CoserTechnicalException(e);
        }
    }

    protected void flushCopyRepositoryRequest(R request) {
        if (request instanceof CopyRepositoryRequest) {
            CopyRepositoryRequest r = (CopyRepositoryRequest) request;

            File targetDirectory = getParam(PARAMETER_TARGET_DIRECTORY);
            if (targetDirectory != null) {
                r.setTargetDirectory(targetDirectory);
            }
        }
    }

    protected void flushExtractRawDataAndResultsRequest(R request) {
        if (request instanceof ExtractRawDataAndResultsRequest) {
            ExtractRawDataAndResultsRequest r = (ExtractRawDataAndResultsRequest) request;

            List<String> populationIndicatorList = getListParam(PARAMETER_POPULATION_INDICATOR_LIST);
            if (CollectionUtils.isNotEmpty(populationIndicatorList)) {
                r.setPopulationIndicatorList(populationIndicatorList);
            }
            List<String> communityIndicatorList = getListParam(PARAMETER_COMMUNITY_INDICATOR_LIST);
            if (CollectionUtils.isNotEmpty(communityIndicatorList)) {
                r.setCommunityIndicatorList(communityIndicatorList);
            }
            List<String> speciesList = getListParam(PARAMETER_SPECIES_LIST);
            if (CollectionUtils.isNotEmpty(speciesList)) {
                r.setSpeciesList(speciesList);
            }
        }
    }

    protected void flushCoserRequestFacadeAware(R request) {
        if (request instanceof CoserRequestFacadeAware) {
            String facade = getParam(PARAMETER_FACADE);
            if (facade != null) {
                ((CoserRequestFacadeAware) request).setFacade(facade);
            }
        }
    }

    protected void flushCoserRequestZoneAware(R request) {
        if (request instanceof CoserRequestZoneAware) {
            String zone = getParam(PARAMETER_ZONE);
            if (zone != null) {
                ((CoserRequestZoneAware) request).setZone(zone);
            }
        }
    }

    protected void flushCoserRequestSpeciesAware(R request) {
        if (request instanceof CoserRequestSpeciesAware) {
            String species = getParam(PARAMETER_SPECIES);
            if (species != null) {
                ((CoserRequestSpeciesAware) request).setSpecies(species);
            }
        }
    }

    protected void flushCoserRequestZoneListAware(R request) {
        if (request instanceof CoserRequestZoneListAware) {
            List<String> zoneList = getListParam(PARAMETER_ZONE_LIST);
            if (CollectionUtils.isNotEmpty(zoneList)) {
                ((CoserRequestZoneListAware) request).setZoneList(zoneList);
            }
        }
    }

    protected void flushCoserRequestExtractTypeListAware(R request) {
        if (request instanceof CoserRequestExtractTypeListAware) {
            List<DataType> zoneList = getListParam(PARAMETER_EXTRACT_TYPE_LIST);
            if (CollectionUtils.isNotEmpty(zoneList)) {
                ((CoserRequestExtractTypeListAware) request).setExtractTypeList(zoneList);
            }
        }
    }

    protected void flushCoserRequestIndicatorAware(R request) {
        if (request instanceof CoserRequestIndicatorAware) {
            String indicator = getParam(PARAMETER_INDICATOR);
            if (indicator != null) {
                ((CoserRequestIndicatorAware) request).setIndicator(indicator);
            }
        }
    }

    protected void flushCoserRequestRepositoryTypeAware(R request) {
        if (request instanceof CoserRequestRepositoryTypeAware) {
            String repositoryType = getParam(PARAMETER_REPOSITORY_TYPE);
            if (repositoryType != null) {
                ((CoserRequestRepositoryTypeAware) request).setRepositoryType(repositoryType);
            }
        }
    }

    protected void flushCoserRequestRepositoryResultTypeAware(R request) {
        if (request instanceof CoserRequestRepositoryResultTypeAware) {
            ResultType resultType = getParam(PARAMETER_RESULT_TYPE);
            if (resultType != null) {
                ((CoserRequestRepositoryResultTypeAware) request).setResultType(resultType);
            }
        }
    }

    protected <O> O getParam(String parameterName) {
        Object o = parameters.get(parameterName);
        return (O) o;
    }

    protected <O> List<O> getListParam(String parameterName) {
        Object o = parameters.get(parameterName);
        return (List<O>) o;
    }

}

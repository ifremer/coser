package fr.ifremer.coser.result.repository.legacy;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created on 3/13/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class LegacyPredicates {

    protected LegacyPredicates() {
        // avoid instances
    }

    public static Predicate<String[]> communitySpeciesListPredicate(String species) {
        return new CommunitySpeciesListPredicate(Lists.newArrayList(species));
    }

    public static Predicate<String[]> communityIndicatorPredicate(String indicator) {
        return new CommunityIndicatorPredicate(Lists.newArrayList(indicator));
    }

    public static Predicate<String[]> communityIndicatorPredicate(List<String> indicator) {
        return new CommunityIndicatorPredicate(indicator);
    }

    public static Predicate<String[]> communitySpeciesPredicate(List<String> species) {
        return new CommunitySpeciesPredicate(Lists.newArrayList(species));
    }

    public static Predicate<String[]> populationSpeciesPredicate(String species) {
        return new PopulationSpeciesPredicate(Lists.newArrayList(species));
    }

    public static Predicate<String[]> populationSpeciesPredicate(List<String> species) {
        return new PopulationSpeciesPredicate(species);
    }

    public static Predicate<String[]> populationIndicatorPredicate(String indicator) {
        return new PopulationIndicatorPredicate(Lists.newArrayList(indicator));
    }

    public static Predicate<String[]> populationIndicatorPredicate(List<String> indicator) {
        return new PopulationIndicatorPredicate(indicator);
    }

    public static Predicate<String[]> speciesListPredicate(String speciesList) {
        return new SpeciesListPredicate(speciesList);
    }

    protected static class SpeciesListPredicate implements Predicate<String[]> {

        protected final String speciesListLetter;

        SpeciesListPredicate(String speciesList) {
            Preconditions.checkNotNull(speciesList, "SpeciesList can not be null");
            this.speciesListLetter = String.valueOf(speciesList.charAt(0));
        }

        @Override
        public boolean apply(String[] input) {
            String speciesListCode = input[4];
            boolean result = speciesListLetter.equals(speciesListCode);
            return result;
        }
    }

    protected static class CommunitySpeciesListPredicate implements Predicate<String[]> {

        private final List<String> speciesList;

        CommunitySpeciesListPredicate(List<String> speciesList) {
            Preconditions.checkNotNull(speciesList, "SpeciesList can not be null");
            this.speciesList = speciesList;
        }

        @Override
        public boolean apply(String[] input) {
            // here we match a *speciesList*
            String speciesListCode = input[2];
            boolean result = speciesList.contains(speciesListCode);
            return result;
        }
    }

    protected static class CommunitySpeciesPredicate implements Predicate<String[]> {

        private final List<String> species;

        CommunitySpeciesPredicate(List<String> species) {
            Preconditions.checkNotNull(species, "Species can not be null");
            this.species = species;
        }

        @Override
        public boolean apply(String[] input) {
            // here we match a *species*
            String speciesCode = input[3];
            boolean result = species.contains(speciesCode);
            return result;
        }
    }

    protected static class CommunityIndicatorPredicate implements Predicate<String[]> {

        private final List<String> indicator;

        CommunityIndicatorPredicate(List<String> indicator) {
            Preconditions.checkNotNull(indicator, "Indicators can not be null");
            this.indicator = indicator;
        }

        @Override
        public boolean apply(String[] input) {
            String indicatorCode = input[1];
            boolean result = indicator.contains(indicatorCode);
            return result;
        }
    }

    protected static class PopulationSpeciesPredicate implements Predicate<String[]> {

        private final List<String> species;

        PopulationSpeciesPredicate(List<String> species) {
            Preconditions.checkNotNull(species, "Species can not be null");
            this.species = species;
        }

        @Override
        public boolean apply(String[] input) {
            String speciesCode = input[3];
            boolean result = species.contains(speciesCode);
            return result;
        }
    }

    protected static class PopulationIndicatorPredicate implements Predicate<String[]> {

        private final List<String> indicator;

        PopulationIndicatorPredicate(List<String> indicator) {
            Preconditions.checkNotNull(indicator, "Indicators can not be null");
            this.indicator = indicator;
        }

        @Override
        public boolean apply(String[] input) {
            String indicatorCode = input[1];
            boolean result = indicator.contains(indicatorCode);
            return result;
        }
    }
}

package fr.ifremer.coser.result.util;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.CoserRequestContext;
import fr.ifremer.coser.result.result.ExtractRawDataAndResultsResult;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Useful methods for extract commands.
 * Created on 3/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class Extracts {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Extracts.class);

    public File assemblyExtractResult(CoserRequestContext context,
                                      File extractDirectory,
                                      List<String> zoneList,
                                      List<ExtractRawDataAndResultsResult> results) {

        Locale locale = context.getLocale();

        // merge all results
        MultiKeyMap<String, File> pdfMaps = new MultiKeyMap<String, File>();
        MultiKeyMap<String, Pair<File, String>> pdfCharts = new MultiKeyMap<String, Pair<File, String>>();

        for (ExtractRawDataAndResultsResult result : results) {
            Pair<MultiKeyMap<String, File>, MultiKeyMap<String, Pair<File, String>>> repositoryResult = result.getResult();
            MultiKeyMap<String, File> pdfMap = repositoryResult.getLeft();
            MultiKeyMap<String, Pair<File, String>> pdfChart = repositoryResult.getRight();
            if (MapUtils.isNotEmpty(pdfMap)) {
                pdfMaps.putAll(pdfMap);
            }
            if (MapUtils.isNotEmpty(pdfChart)) {
                pdfCharts.putAll(pdfChart);
            }

        }

        Reports reports = context.getReports();

        // generate pdf if necessary
        if (MapUtils.isNotEmpty(pdfMaps) || MapUtils.isNotEmpty(pdfCharts)) {
            if (log.isDebugEnabled()) {
                log.debug("Generated Extract PDF");
            }
            reports.generateExtractPDF(extractDirectory,
                                       zoneList,
                                       pdfMaps,
                                       pdfCharts,
                                       context.getZoneMap(),
                                       locale);
        }

        // fichier de décharge en pdf
        if (log.isDebugEnabled()) {
            log.debug("Generated decharge PDF");
        }
        String filename = Reports.getDechargeFilename(locale);
        File dechargePDF = new File(extractDirectory, filename);
        Date lastDataUpdateDate = context.getConfig().getLastDataUpdateDate();
        reports.generateDechargePDF(dechargePDF, locale, lastDataUpdateDate, null);

        // make zip
        if (log.isDebugEnabled()) {
            log.debug("Make final archive");
        }

        try {
            File resultZip = File.createTempFile("coser-extract-", ".zip");
            resultZip.deleteOnExit();
            ZipUtil.compress(resultZip, extractDirectory);

            // clean directory
            FileUtils.deleteDirectory(extractDirectory.getParentFile());
            return resultZip;
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't create zip file", ex);
        } finally {
            // clean directory
            FileUtils.deleteQuietly(extractDirectory.getParentFile());
        }
    }
}

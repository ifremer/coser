package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.result.request.GetIndicatorsForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.result.MapResult;
import fr.ifremer.coser.util.DataType;

import java.util.Map;
import java.util.Set;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetIndicatorsForExtractRawDataAndResultsCommand extends AbstractEchoBaseCommand<GetIndicatorsForExtractRawDataAndResultsRequest> {

    @Override
    public boolean accept(GetIndicatorsForExtractRawDataAndResultsRequest request) {
        return repository.matchExtractTypeList(request) &&
               repository.matchZone(request);
    }

    @Override
    public MapResult execute(GetIndicatorsForExtractRawDataAndResultsRequest request) {

        Set<String> indicatorList = null;

        if (request.getExtractTypeList().contains(DataType.COMMUNITY)) {

            // get all community indicators for given zone
            indicatorList = getCommunityIndicators();
        } else if (request.getExtractTypeList().contains(DataType.POPULATION)) {

            // get all population indicators for given zone
            indicatorList = getPopulationIndicators();
        }

        Map<String, String> map = null;

        if (indicatorList != null) {
            map = getIndicatorsMap().getIndicatorsValues(getLocale(), indicatorList);
        }

        MapResult result = newMapResult(map);
        return result;
    }

}

package fr.ifremer.coser.result.repository.legacy.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import fr.ifremer.coser.result.repository.legacy.LegacyPredicates;
import fr.ifremer.coser.result.request.GetSpeciesListForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.result.MapResult;
import fr.ifremer.coser.storage.DataStorageWalker;

import java.util.Map;
import java.util.Set;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetSpeciesListForCommunityIndicatorResultCommand extends AbstractLegacyCommand<GetSpeciesListForCommunityIndicatorResultRequest> {

    @Override
    public boolean accept(GetSpeciesListForCommunityIndicatorResultRequest request) {
        boolean result = repository.isIndicatorsResult() &&
                         repository.matchFacade(request) &&
                         repository.matchZone(request);
        if (result) {

            Predicate<String[]> predicate =
                    LegacyPredicates.communityIndicatorPredicate(request.getIndicator());
            result = repository.matchCommunity(predicate);
        }
        return result;
    }

    @Override
    public MapResult execute(GetSpeciesListForCommunityIndicatorResultRequest request) {

        // linked hash map (doit respecter l'ordre d'insertion)
        final Set<String> speciesList = Sets.newLinkedHashSet();
        DataStorageWalker walker = new DataStorageWalker() {

            @Override
            public void onRow(String... tuple) {
                String speciesListCode = tuple[2];
                speciesList.add(speciesListCode);
            }
        };
        Predicate<String[]> predicate = LegacyPredicates.communityIndicatorPredicate(request.getIndicator());
        walkOnCommunity(predicate, walker);
        Map<String, String> map = repository.getSpeciesListMap().getSpeciesSubMap(getLocale(),
                                                                                  speciesList);

        MapResult result = newMapResult(map);
        return result;
    }

}

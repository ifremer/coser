package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.result.request.CopyRepositoryRequest;
import fr.ifremer.coser.result.result.VoidResult;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created on 3/18/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CopyRepositoryCommand extends AbstractEchoBaseCommand<CopyRepositoryRequest> {

    @Override
    public boolean accept(CopyRepositoryRequest request) {
        return repository.matchZone(request);
    }

    @Override
    public VoidResult execute(CopyRepositoryRequest request) {
        File basedir = repository.getBasedir();
        File targetDirectory = new File(request.getTargetDirectory(), repository.getProjectName());
        try {
            CoserUtils.customCopyDirectory(basedir, targetDirectory, FileFilterUtils.trueFileFilter());
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not copy files", e);
        }
        return newVoidResult();
    }
}

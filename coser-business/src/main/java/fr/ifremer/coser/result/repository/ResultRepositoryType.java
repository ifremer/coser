package fr.ifremer.coser.result.repository;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.result.CoserCommand;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.request.GetAllResultsRequest;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * To define a new type of result repository.
 *
 * <strong>Important Note:</strong> Each repository type must have a unique id ({@link #getId()}).
 *
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see GetAllResultsRequest
 * @since 1.5
 */
public interface ResultRepositoryType extends Serializable {

    String getId();

    String getLabelKey();

    Set<ResultType> getResultTypes();

    Map<Class<? extends CoserRequest>, Class<? extends CoserCommand>> getCommandTypes();
}

package fr.ifremer.coser.result.util;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.itextpdf.text.DocumentException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorages;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;

import static org.nuiton.i18n.I18n.l;

/**
 * For reports generation.
 *
 * Created on 3/7/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class Reports {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Reports.class);

    /** Freemarker. */
    protected Configuration freemarkerConfiguration;

    public Reports() {

        freemarkerConfiguration = new Configuration();

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(Reports.class, "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

        // pour les maps dans les template (entre autre)
        freemarkerConfiguration.setObjectWrapper(new BeansWrapper());
    }

    public static String getDechargeFilename(Locale locale) {
        Preconditions.checkNotNull(locale);
        String filename = I18n.l(locale, "coser.business.dataDisclaimer.filename");
//        if (locale != null && "fr".equals(locale.getLanguage())) {
//            filename = "DechargeDonnees.pdf";
//        } else if (locale != null && "es".equals(locale.getLanguage())) {
//            filename = "DatosDeExencionDeResponsabilidad.pdf";
//        } else {
//            filename = "DataDisclaimer.pdf";
//        }
        return filename;
    }

    /**
     * Genere le fichier PDF d'information sur les espèces incluses dans les
     * calculs des indicateurs de communautés, à jointe à chaque téléchargement.
     *
     * @param path                  path to result
     * @param resultDirectory       result directory
     * @param locale                locale
     * @param indicatorLocalizedMap localized indicator map
     * @return generated pdf file
     */
    public File generateMetaFilePDF(RSufiResultPath path,
                                    File resultDirectory,
                                    Locale locale,
                                    IndicatorMap indicatorLocalizedMap) {

        Project project = path.getProject();
        Selection selection = path.getSelection();
        RSufiResult rsufiResult = path.getRsufiResult();

        File result = null;

        // chargement du reftax et des code types especes pour connaitre
        // le type des especes de poissons
        // parcourt du fichier des types de données
        Map<String, Integer> refTaxSpeciesNumSys = Maps.newHashMap();
        Map<String, String> refTaxSpeciesName = Maps.newHashMap();
        Iterator<String[]> itReftax = project.getRefTaxSpecies().iterator(true);
        while (itReftax.hasNext()) {
            String[] tuple = itReftax.next();
            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String speciesCode = tuple[3];
            Integer iNumSys = Integer.valueOf(tuple[1]);
            refTaxSpeciesNumSys.put(speciesCode, iNumSys);

            // fix html entities bug
            String speciesSciName = StringEscapeUtils.escapeXml(tuple[4]);
            String speciesAuthor = StringEscapeUtils.escapeXml(tuple[5]);

            // TODO little hack for italic species
            refTaxSpeciesName.put(speciesCode, "<span style='font-style:italic'>" + speciesSciName + "</span> " + speciesAuthor);
        }

        // code type / especes
        Map<String, Integer[]> specyTypes = Maps.newHashMap();
        Iterator<String[]> itTypeSpecies = project.getTypeEspeces().iterator(true);
        while (itTypeSpecies.hasNext()) {
            // "Types";"Commentaire";"NumSys min";"NumSys max","Code"
            String[] tuple = itTypeSpecies.next();
            String specyTypeCode = tuple[4];

            Integer iMinNumSys = Integer.valueOf(tuple[2]);
            Integer iMaxNumSys = Integer.valueOf(tuple[3]);
            specyTypes.put(specyTypeCode, new Integer[]{iMinNumSys, iMaxNumSys});
        }

        // le fichier estpopind
        File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

        // donnees intermediare (map liste id > liste des indicateurs)
        Map<String, SortedSet<String>> indicatorMap = Maps.newHashMap();

        // le resutat sera une map complexe
        // map liste id > liste des especes (nom complet)
        Map<String, SortedSet<String>> speciesMap = Maps.newHashMap();

        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        DataStorage dataStorage = DataStorages.load(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
        Iterator<String[]> estComIndIterator = dataStorage.iterator(true);
        while (estComIndIterator.hasNext()) {
            String[] tuple = estComIndIterator.next();

            String indicatorCode = tuple[1];
            String listIdCode = tuple[2]; // c1, p2, T3 ...

            String listNumber = listIdCode.substring(1); // 1, 2, 3

            // get indicator list
            SortedSet<String> indicatorList = indicatorMap.get(listNumber);
            if (indicatorList == null) {
                indicatorList = Sets.newTreeSet();
                indicatorMap.put(listNumber, indicatorList);
            }

            // get indicator full name
            String indicatorName = indicatorLocalizedMap.getIndicatorValue(locale, indicatorCode);
            // peut arriver pour les indicateurs inconnu par coser
            if (indicatorName != null) {
                indicatorList.add(indicatorName);
            }
        }

        // seconde pass, remplit la map speciesMap avec les listes configurées
        // dans la selection
        for (String listNumber : indicatorMap.keySet()) {
            List<String> selectionSpeciesList = null;
            if ("1".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpecies();
            } else if ("2".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpeciesOccDens();
            } else if ("3".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpeciesSizeAllYear();
            } else if ("4".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpeciesMaturity();
            }

            if (selectionSpeciesList != null) {
                SortedSet<String> speciesList = Sets.newTreeSet();

                for (String speciesCode : selectionSpeciesList) {
                    // get species full name
                    String speciesName = refTaxSpeciesName.get(speciesCode);

                    // recupere le code type de l'espece, "m", "c", "p" ...
                    Integer speciesNumSys = refTaxSpeciesNumSys.get(speciesCode);
                    for (Map.Entry<String, Integer[]> speciesTypeEntry : specyTypes.entrySet()) {
                        String speciesTypeCode = speciesTypeEntry.getKey();
                        Integer[] bound = speciesTypeEntry.getValue();

                        if (speciesNumSys >= bound[0] && speciesNumSys <= bound[1]) {
                            speciesName = "(" + speciesTypeCode + ") " + speciesName;
                            break;
                        }
                    }
                    // end code type espece

                    speciesList.add(speciesName);
                }
                speciesMap.put(listNumber, speciesList);
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get species list for list id " + listNumber);
                }
            }
        }

        OutputStream os = null;
        try {
            // render freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate("metainfo.ftl", locale);

            Map<String, Object> root = Maps.newHashMap();
            root.put("indicatorsMap", indicatorMap);
            root.put("speciesMap", speciesMap);

            Writer out = new StringWriter();
            mapTemplate.process(root, out);
            out.flush();

            // get content as w3c document
            Document document = CoserUtils.parseDocument(out.toString());

            // render template output as pdf
            result = File.createTempFile("coser-metainfo-", ".pdf");
            result.deleteOnExit();
            os = new FileOutputStream(result);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(document, null);
            renderer.layout();
            renderer.createPDF(os);

            os.close();
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't generate meta info file", ex);
        } catch (TemplateException ex) {
            throw new CoserTechnicalException("Can't generate meta info file", ex);
        } catch (DocumentException ex) {
            throw new CoserTechnicalException("Can't generate meta info file", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }

        return result;
    }

    /**
     * Genere le PDF dynamique de decharge à partir du template freemarker.
     *
     * @param disclamerPdf pdf file to generate
     * @param locale       generated pdf locale
     * @param updateDate   last update date of results
     * @param surveyName   optional survey name
     */
    public void generateDechargePDF(File disclamerPdf,
                                    Locale locale,
                                    Date updateDate,
                                    String surveyName) {

        Preconditions.checkNotNull(disclamerPdf);
        Preconditions.checkNotNull(locale);
        Preconditions.checkNotNull(updateDate);
        OutputStream os = null;

        try {
            // get some info to put into pdf
//            Date updateDate = getLastDataUpdateDate();

//            // il  se peut que pour l'extraction un fichier de décharge ne soit
//            // pas généré pour une campagne en particulier
//            // on passe un nom vide a freemarker
//            String surveyName = "";
//            if (resultDirectory != null && rSufiResult != null) {
//                surveyName = projectService.getProjectSurveyName(resultDirectory, rSufiResult);
//            }

            // render freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate("decharge.ftl", locale);

            Map<String, Object> root = Maps.newHashMap();
            root.put("updateDate", updateDate);
            // il  se peut que pour l'extraction un fichier de décharge ne soit
            // pas généré pour une campagne en particulier
            // on passe un nom vide a freemarker
            root.put("surveyName", surveyName == null ? "" : surveyName);

            Writer out = new StringWriter();
            mapTemplate.process(root, out);
            out.flush();

            // get content as w3c document
            Document document = CoserUtils.parseDocument(out.toString());

            // render template output as pdf
            os = new FileOutputStream(disclamerPdf);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(document, null);
            renderer.layout();
            renderer.createPDF(os);

            os.close();
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't generate decharge file", ex);
        } catch (TemplateException ex) {
            throw new CoserTechnicalException("Can't generate decharge file", ex);
        } catch (DocumentException ex) {
            throw new CoserTechnicalException("Can't generate decharge file", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }
    }

    /**
     * Generate pdf file filled with maps and charts.
     *
     * @param directory directory to generate pdf to
     * @param zones     zones ids
     * @param pdfMaps   pdf maps (can be {@code null})
     * @param pdfCharts pdf charts (can be {@code null})
     * @param zoneMap   zoneMap (to get zone fullnames)
     * @param locale    locale to use for translations
     */
    public void generateExtractPDF(File directory,
                                   List<String> zones,
                                   MultiKeyMap<String, File> pdfMaps,
                                   MultiKeyMap<String, Pair<File, String>> pdfCharts,
                                   ZoneMap zoneMap,
                                   Locale locale) {

        for (String zone : zones) {
            Collection<File> toDelete = Lists.newArrayList();
            OutputStream os = null;

            try {
                StringBuilder htmlContent = new StringBuilder();
                htmlContent.append("<html><head>");
                htmlContent.append("<title>").append(l(locale, "coser.business.extract.extracttitle")).append("</title>");
                htmlContent.append("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
                htmlContent.append("</head><body>");

                if (MapUtils.isNotEmpty(pdfMaps)) {
                    for (Map.Entry<MultiKey<? extends String>, File> mapEntry : pdfMaps.entrySet()) {
                        String zoneId = mapEntry.getKey().getKey(0);
                        if (zoneId.equals(zone)) {
                            String speciesName = mapEntry.getKey().getKey(1);
                            File file = mapEntry.getValue();

                            String zoneName = zoneMap.getZoneFullName(zoneId);
                            htmlContent.append("<div style='page-break-after: always'>");
                            htmlContent.append("<p>").append(zoneName).append(" - ").append(speciesName).append("</p>");
                            htmlContent.append("<img src='file://").append(file.getAbsolutePath()).append("' />");
                            htmlContent.append("</div>");
                        }
                    }
                }

                if (MapUtils.isNotEmpty(pdfCharts)) {
                    for (MultiKeyMap.Entry<MultiKey<? extends String>, Pair<File, String>> chartFileAndData : pdfCharts.entrySet()) {
                        String zoneId = chartFileAndData.getKey().getKey(0);
                        if (zoneId.equals(zone)) {
                            File chartFile = chartFileAndData.getValue().getLeft();
                            String content = chartFileAndData.getValue().getRight();

                            htmlContent.append("<div style='page-break-after: always'>");
                            htmlContent.append("<img src='file://");
                            htmlContent.append(chartFile.getAbsolutePath());
                            htmlContent.append("' />");
                            htmlContent.append("<br />");
                            htmlContent.append(l(locale, "coser.business.extract.extractdata")).append(" :");
                            htmlContent.append("<pre>").append(content).append("</pre>");
                            htmlContent.append("</div>");

                            // les graphiques ont été générés, a supprimer donc
                            // a ne surtout pas faire avec les cartes !!!
                            toDelete.add(chartFile);
                        }
                    }
                }

                htmlContent.append("</body></html>");

                // get content as w3c document
                String content = htmlContent.toString();
                content = content.replaceAll("&", "&amp;");
                Document document = CoserUtils.parseDocument(content);

                // render template output as pdf
                // remove accents and strange characters from zone display name
                String zoneDisplay = zoneMap.getZoneFullName(zone);
                zoneDisplay = StringUtils.stripAccents(zoneDisplay);
                zoneDisplay = zoneDisplay.replaceAll("[^\\w- ]", "_");
                File pdfFile = new File(directory, zoneDisplay + ".pdf");
                os = new FileOutputStream(pdfFile);

                ITextRenderer renderer = new ITextRenderer();
                renderer.setDocument(document, null);
                renderer.layout();
                renderer.createPDF(os);

                os.close();
            } catch (IOException ex) {
                throw new CoserTechnicalException("Can't generate log pdf", ex);
            } catch (DocumentException ex) {
                throw new CoserTechnicalException("Can't generate log pdf", ex);
            } finally {
                IOUtils.closeQuietly(os);

                // delete file collection
                for (File file : toDelete) {
                    FileUtils.deleteQuietly(file);
                }
            }
        }
    }
}

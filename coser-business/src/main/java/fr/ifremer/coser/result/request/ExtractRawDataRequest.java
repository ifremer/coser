package fr.ifremer.coser.result.request;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coser.result.CoserRequest;

/**
 * Request to obtain raw data results.
 *
 * Created on 3/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ExtractRawDataRequest implements CoserRequest, CoserRequestFacadeAware, CoserRequestZoneAware {

    private static final long serialVersionUID = 1L;

    protected String facade;

    protected String zone;

    @Override
    public boolean isFilled() {
        return !(facade == null || zone == null);
    }

    @Override
    public String getFacade() {
        return facade;
    }

    @Override
    public void setFacade(String facade) {
        Preconditions.checkNotNull(facade);
        this.facade = facade;
    }

    @Override
    public String getZone() {
        return zone;
    }

    @Override
    public void setZone(String zone) {
        Preconditions.checkNotNull(zone);
        this.zone = zone;
    }
}

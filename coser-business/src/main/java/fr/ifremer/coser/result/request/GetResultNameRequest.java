package fr.ifremer.coser.result.request;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.util.DataType;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Created on 3/18/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetResultNameRequest implements CoserRequest, CoserRequestExtractTypeListAware {

    private static final long serialVersionUID = 1L;

    protected List<DataType> extractTypeList;

    @Override
    public List<DataType> getExtractTypeList() {
        return extractTypeList;
    }

    @Override
    public void setExtractTypeList(List<DataType> extractTypeList) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(extractTypeList));
        this.extractTypeList = extractTypeList;
    }

    @Override
    public boolean isFilled() {
        return CollectionUtils.isNotEmpty(extractTypeList);
    }
}

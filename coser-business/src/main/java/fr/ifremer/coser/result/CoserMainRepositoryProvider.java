package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.repository.ResultRepositoryProvider;
import fr.ifremer.coser.result.repository.echobase.EchoBaseResultRepositoryProvider;
import fr.ifremer.coser.result.repository.legacy.LegacyResultRepositoryProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.Set;

/**
 * Created on 3/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserMainRepositoryProvider {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CoserMainRepositoryProvider.class);

    /**
     * Repositories providers.
     */
    protected final Set<ResultRepositoryProvider<?>> repositoryProviders;

    /**
     * Cache of repositories.
     */
    protected Set<ResultRepository> repositories;

    public static Set<ResultRepositoryProvider<?>> createDefaultRepositoryProviders(CoserBusinessConfig config) {
        Preconditions.checkNotNull(config);
        Set<ResultRepositoryProvider<?>> result = Sets.newHashSet();

        // add legacy map repository
        Preconditions.checkNotNull(config.getWebMapsProjectsDirectory());
        result.add(new LegacyResultRepositoryProvider(config, config.getWebMapsProjectsDirectory(), ResultType.MAP));

        // add legacy indicators repository
        Preconditions.checkNotNull(config.getWebIndicatorsProjectsDirectory());
        result.add(new LegacyResultRepositoryProvider(config, config.getWebIndicatorsProjectsDirectory(), ResultType.INDICATOR));

        // add EchoBase repository
        Preconditions.checkNotNull(config.getWebEchobaseProjectsDirectory());
        result.add(new EchoBaseResultRepositoryProvider(config.getWebEchobaseProjectsDirectory()));
        return result;
    }

    public CoserMainRepositoryProvider(Set<ResultRepositoryProvider<?>> repositoryProviders) {
        Preconditions.checkNotNull(repositoryProviders);
        this.repositoryProviders = Collections.unmodifiableSet(repositoryProviders);
    }

    public Set<ResultRepositoryProvider<?>> getRepositoryProviders() {
        return repositoryProviders;
    }

    public void resetRepositories() {
        repositories = null;
    }

    public Set<ResultRepository> getResultRepositories() {
        if (repositories == null) {
            Set<String> ids = Sets.newHashSet();
            Set<ResultRepository> resultRepositories = Sets.newHashSet();
            for (ResultRepositoryProvider<?> repositoryProvider : repositoryProviders) {

                Set<ResultRepository> repos = loadFromRepositoryProvider(repositoryProvider, ids);
                resultRepositories.addAll(repos);
            }

            repositories = Collections.unmodifiableSet(resultRepositories);
            if (log.isInfoEnabled()) {
                log.info("Found " + repositories.size() + " result repository(ies).");
            }
        }
        return repositories;
    }

    protected Set<ResultRepository> loadFromRepositoryProvider(ResultRepositoryProvider<?> repositoryProvider,
                                                               Set<String> ids) {

        Set<? extends ResultRepository> resultRepositories = repositoryProvider.loadRepositories();

        Set<ResultRepository> result = Sets.newHashSet();

        // check all repository use a unique id
        for (ResultRepository resultRepository : resultRepositories) {
            String id = resultRepository.getId();
            if (log.isDebugEnabled()) {
                log.debug("Try to register result Repository: " + id);
            }
            if (!ids.add(id)) {

                // there is already a repository with this id
                throw new ResultRepositoryInitializationException(repositoryProvider, "Duplicate result repository with id: " + id, null);
            }
            result.add(resultRepository);
        }
        return result;
    }
}

package fr.ifremer.coser.result.repository.legacy.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.CoserCommand;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.CoserRequestContext;
import fr.ifremer.coser.result.repository.legacy.LegacyPredicates;
import fr.ifremer.coser.result.repository.legacy.LegacyResultRepository;
import fr.ifremer.coser.result.result.ExtractRawDataAndResultsResult;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.result.result.MapResult;
import fr.ifremer.coser.result.result.VoidResult;
import fr.ifremer.coser.result.util.Charts;
import fr.ifremer.coser.result.util.Reports;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorageWalker;
import fr.ifremer.coser.storage.DataStorages;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jfree.chart.JFreeChart;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public abstract class AbstractLegacyCommand<R extends CoserRequest> implements CoserCommand<LegacyResultRepository, R> {

    protected CoserRequestContext context;

    protected LegacyResultRepository repository;

    @Override
    public void setRequestContext(CoserRequestContext context) {
        this.context = context;
    }

    @Override
    public void setRepository(LegacyResultRepository repository) {
        this.repository = repository;
    }

    // --------------------------------------------------------------------- //
    // --- Context shortcuts method ---------------------------------------- //
    // --------------------------------------------------------------------- //

    public IndicatorMap getIndicatorsMap() {
        return context.getIndicatorMap();
    }

    public ZoneMap getZonesMap() {
        return context.getZoneMap();
    }

    public Reports getReports() {
        return context.getReports();
    }

    public Charts getCharts() {
        return context.getCharts();
    }

    public CoserBusinessConfig getConfig() {
        return context.getConfig();
    }

    public Locale getLocale() {
        return context.getLocale();
    }

    public void loadSelectionData() {
        try {
            // be sure that data are available for this project
            // or it will fail

            ProjectService projectService = new ProjectService(context.getConfig());

            RSufiResultPath path = repository.getPath();
            projectService.loadSelectionData(repository.getBasedir().getParentFile(), path.getProject(), path.getSelection());
        } catch (CoserBusinessException e) {
            throw new CoserTechnicalException("Could not load project selection", e);
        }
    }

    public File extractRSUfiData(File zoneDirectory) {
        try {
            FileUtils.forceMkdir(zoneDirectory);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not create directory: " + zoneDirectory, e);
        }
        try {
            ProjectService projectService = new ProjectService(context.getConfig());

            RSufiResultPath path = repository.getPath();
            return projectService.extractRSUfiData(path.getProject(), path.getSelection(), zoneDirectory, true);
        } catch (CoserBusinessException e) {
            throw new CoserTechnicalException("Could not extract raw data", e);
        }
    }

    // --------------------------------------------------------------------- //
    // --- To create results ----------------------------------------------- //
    // --------------------------------------------------------------------- //

    protected MapResult newMapResult(Map<String, String> map) {
        MapResult result = new MapResult(repository.getId(), map);
        return result;
    }

    protected FileResult newFileResult(File file) {
        FileResult result = new FileResult(repository.getId(), file);
        return result;
    }

    protected VoidResult newVoidResult() {
        VoidResult result = new VoidResult(repository.getId());
        return result;
    }

    protected ExtractRawDataAndResultsResult newExtractRawDataAndResultsResult(MultiKeyMap<String, File> pdfMaps, MultiKeyMap<String, Pair<File, String>> pdfCharts) {
        ExtractRawDataAndResultsResult result = new ExtractRawDataAndResultsResult(
                repository.getId(), pdfMaps, pdfCharts);
        return result;
    }


    // --------------------------------------------------------------------- //
    // --- Get species lists ----------------------------------------------- //
    // --------------------------------------------------------------------- //

    protected Set<String> getPopulationSpecies() {
        final Set<String> result = Sets.newHashSet();

        DataStorageWalker walker = new DataStorageWalker() {

            @Override
            public void onRow(String... tuple) {
                String speciesCode = tuple[3];
                result.add(speciesCode);
            }
        };
        walkOnPopulation(walker);
        return result;
    }

    protected Map<String, String> getPopulationSpeciesMap() {
        final Set<String> speciesList = getPopulationSpecies();

        Map<String, String> result = repository.getSpeciesMap().getSpeciesSubMap(speciesList);
        return result;
    }

    protected Map<String, String> getCommunitySpeciesMap() {
        final Set<String> speciesList = Sets.newHashSet();

        DataStorageWalker walker = new DataStorageWalker() {

            @Override
            public void onRow(String... tuple) {
                String speciesCode = tuple[3];
                speciesList.add(speciesCode);
            }
        };
        walkOnCommunity(walker);
        Map<String, String> result = repository.getSpeciesMap().getSpeciesSubMap(speciesList);

        return result;
    }

    protected Map<String, String> getMapSpeciesMap() {
        //TODO
        return getPopulationSpeciesMap();
    }

    // --------------------------------------------------------------------- //
    // --- Get indicator lists --------------------------------------------- //
    // --------------------------------------------------------------------- //

    protected Set<String> getCommunityIndicators() {
        final Set<String> result = Sets.newHashSet();
        DataStorageWalker walker = new DataStorageWalker() {

            @Override
            public void onRow(String... tuple) {
                String indicatorCode = tuple[1];
                result.add(indicatorCode);
            }
        };
        walkOnCommunity(walker);
        return result;
    }

    protected Set<String> getPopulationIndicators() {
        final Set<String> result = Sets.newHashSet();
        DataStorageWalker walker = new DataStorageWalker() {

            @Override
            public void onRow(String... tuple) {
                String indicatorCode = tuple[1];
                result.add(indicatorCode);
            }
        };
        walkOnPopulation(walker);
        return result;
    }

    protected Set<String> getPopulationIndicators(String species) {
        final Set<String> result = Sets.newHashSet();
        DataStorageWalker walker = new DataStorageWalker() {

            @Override
            public void onRow(String... tuple) {
                String indicatorCode = tuple[1];
                result.add(indicatorCode);
            }
        };
        walkOnPopulation(LegacyPredicates.populationSpeciesPredicate(species), walker);
        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Walk on indicators ---------------------------------------------- //
    // --------------------------------------------------------------------- //

    protected void walkOnCommunity(DataStorageWalker walker) {
        Preconditions.checkNotNull(walker);

        DataStorage storage = repository.getCommunityIndicatorStorage();
        DataStorages.walk(storage, walker);
    }

    protected void walkOnCommunity(Predicate<String[]> predicate, DataStorageWalker walker) {
        Preconditions.checkNotNull(walker);
        Preconditions.checkNotNull(predicate);

        DataStorage storage = repository.getCommunityIndicatorStorage();
        DataStorages.walk(storage, predicate, walker);
    }

    protected void walkOnPopulation(DataStorageWalker walker) {
        Preconditions.checkNotNull(walker);

        DataStorage storage = repository.getPopulationIndicatorStorage();
        DataStorages.walk(storage, walker);
    }

    protected void walkOnPopulation(Predicate<String[]> predicate, DataStorageWalker walker) {
        Preconditions.checkNotNull(predicate);
        Preconditions.checkNotNull(walker);

        DataStorage storage = repository.getPopulationIndicatorStorage();
        DataStorages.walk(storage, predicate, walker);
    }

    // --------------------------------------------------------------------- //
    // --- Extract indicator data ------------------------------------------ //
    // --------------------------------------------------------------------- //

    protected DataStorage extractCommunity(Predicate<String[]> predicate) {
        Preconditions.checkNotNull(predicate);

        String headerList = l(getLocale(), "coser.business.rsufi.community.header");
        String header[] = headerList.split("\\s*,\\s*");

        DataStorage source = repository.getCommunityIndicatorStorage();
        DataStorage result = DataStorages.sub(source, predicate, header);
        return result;
    }

    protected DataStorage extractPopulation(Predicate<String[]> predicate) {
        Preconditions.checkNotNull(predicate);

        String headerList = l(getLocale(), "coser.business.rsufi.population.header");
        String header[] = headerList.split("\\s*,\\s*");

        DataStorage source = repository.getPopulationIndicatorStorage();
        DataStorage result = DataStorages.sub(source, predicate, header);
        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Generate indicator charts --------------------------------------- //
    // --------------------------------------------------------------------- //

    protected JFreeChart generateCommunityChart(String zone,
                                                String indicator,
                                                String speciesList) {

        Predicate<String[]> predicate = Predicates.and(
                LegacyPredicates.communityIndicatorPredicate(indicator),
                LegacyPredicates.communitySpeciesListPredicate(speciesList));

        Charts.ExtractGraphDataWalker walker = new Charts.ExtractGraphDataWalker() {

            @Override
            protected String getEstimationData(String... tuple) {
                return tuple[5];
            }

            @Override
            protected String getEcartData(String... tuple) {
                return tuple[6];
            }

            @Override
            protected String getYearData(String... tuple) {
                return tuple[4];
            }
        };
        walkOnCommunity(predicate, walker);

        Locale locale = getLocale();

        // get graph title
        String zoneDisplayName = getZonesMap().getZoneFullName(zone);
        String indicatorName = getIndicatorsMap().getIndicatorValue(locale, indicator);
        String speciesListName = repository.getSpeciesListMap().getSpeciesListName(locale, speciesList);
        String chartTitle = zoneDisplayName + " - " + indicatorName + " - " + speciesListName;

        JFreeChart chart = getCharts().generateCommunityChart(locale,
                                                              walker,
                                                              getIndicatorsMap(),
                                                              indicator,
                                                              chartTitle);
        return chart;
    }

    protected JFreeChart generatePopulationChart(String zone,
                                                 String species,
                                                 String indicator) {

        // get data to put in graph
        Predicate<String[]> predicate = Predicates.and(
                LegacyPredicates.populationSpeciesPredicate(species),
                LegacyPredicates.populationIndicatorPredicate(indicator));

        Charts.ExtractGraphDataWalker walker = new Charts.ExtractGraphDataWalker() {

            @Override
            protected String getEstimationData(String... tuple) {
                return tuple[6];
            }

            @Override
            protected String getEcartData(String... tuple) {
                return tuple[7];
            }

            @Override
            protected String getYearData(String... tuple) {
                return tuple[5];
            }
        };
        walkOnPopulation(predicate, walker);

        Locale locale = getLocale();

        // get graph title
        String zoneDisplayName = getZonesMap().getZoneFullName(zone);
        String indicatorName = getIndicatorsMap().getIndicatorValue(locale, indicator);
        String speciesName = repository.getSpeciesMap().getReportDisplayName(species);
        String chartTitle = zoneDisplayName + " - " + indicatorName + " - " + speciesName;

        JFreeChart chart = getCharts().generatePopulationChart(locale,
                                                               walker,
                                                               getIndicatorsMap(),
                                                               indicator,
                                                               chartTitle);
        return chart;
    }

}

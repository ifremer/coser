package fr.ifremer.coser.result.request;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * To delete some results.
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class DeleteResultsRequest implements CoserRequest, CoserRequestZoneListAware, CoserRequestRepositoryTypeAware, CoserRequestRepositoryResultTypeAware {

    private static final long serialVersionUID = 1L;


    /**
     * Id of matching result repository type.
     *
     * @see ResultRepositoryType#getId()
     */
    protected String resultRepositoryType;

    /**
     * Type of matching result.
     */
    protected ResultType resultType;

    /**
     * Ids of matching zone to delete.
     */
    protected List<String> zoneList;

    @Override
    public boolean isFilled() {
        return !(resultRepositoryType == null ||
                 resultType == null ||
                 CollectionUtils.isEmpty(zoneList));
    }

    @Override
    public String getRepositoryType() {
        return resultRepositoryType;
    }

    @Override
    public void setRepositoryType(String resultRepositoryType) {
        Preconditions.checkNotNull(resultRepositoryType);
        this.resultRepositoryType = resultRepositoryType;
    }

    @Override
    public ResultType getResultType() {
        return resultType;
    }

    @Override
    public void setResultType(ResultType resultType) {
        Preconditions.checkNotNull(resultType);
        this.resultType = resultType;
    }

    @Override
    public List<String> getZoneList() {
        return zoneList;
    }

    @Override
    public void setZoneList(List<String> zoneList) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(zoneList));
        this.zoneList = zoneList;
    }
}

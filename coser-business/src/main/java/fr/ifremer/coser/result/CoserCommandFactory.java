package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import org.apache.commons.collections4.map.MultiKeyMap;

import java.util.Map;
import java.util.Set;

/**
 * To create commands.
 * Created on 3/14/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserCommandFactory {

    /**
     * Cache of command types.
     *
     * Keys are (repository type id, request type).
     */
    protected final MultiKeyMap<Object, Class<? extends CoserCommand>> commands;

    public CoserCommandFactory(Set<ResultRepositoryType> resultRepositoryTypes) {
        this.commands = new MultiKeyMap<Object, Class<? extends CoserCommand>>();
        for (ResultRepositoryType resultRepositoryType : resultRepositoryTypes) {

            String resultRepositoryTypeId = resultRepositoryType.getId();

            Map<Class<? extends CoserRequest>, Class<? extends CoserCommand>> commandTypes = resultRepositoryType.getCommandTypes();
            for (Map.Entry<Class<? extends CoserRequest>, Class<? extends CoserCommand>> entry : commandTypes.entrySet()) {
                commands.put(resultRepositoryTypeId, entry.getKey(), entry.getValue());
            }
        }
    }

    public <Repo extends ResultRepository, R extends CoserRequest> CoserCommand<Repo, R> newCommand(CoserRequestContext requestContext,
                                                                                                    Repo repository,
                                                                                                    R request) {

        Class<? extends CoserCommand> commandType = commands.get(repository.getResultRepositoryType().getId(), request.getClass());

        try {
            // instanciate the command to get the request type :(
            CoserCommand<Repo, R> command = commandType.newInstance();
            command.setRepository(repository);
            command.setRequestContext(requestContext);
            return command;
        } catch (InstantiationException e) {
            throw new CoserTechnicalException(e);
        } catch (IllegalAccessException e) {
            throw new CoserTechnicalException(e);
        }
    }
}

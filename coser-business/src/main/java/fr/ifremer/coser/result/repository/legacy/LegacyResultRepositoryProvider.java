package fr.ifremer.coser.result.repository.legacy;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.result.ResultRepositoryInitializationException;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.repository.ResultRepositoryProvider;
import fr.ifremer.coser.services.ProjectService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Set;

/**
 * Created on 3/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class LegacyResultRepositoryProvider implements ResultRepositoryProvider<LegacyResultRepository> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LegacyResultRepositoryProvider.class);

    /**
     * Directory where are stored all legacy results.
     */
    protected final File basedir;

    /**
     * //FIXME Should remove this
     * Application config (used to instanciate some service).
     */
    protected final CoserBusinessConfig config;

    /**
     * Hack to sanity repository {@link LegacyResultRepository#isMapsResult()} or
     * {@link LegacyResultRepository#isIndicatorsResult()} flags.
     *
     * @see #sanityRepository(LegacyResultRepository)
     */
    protected final ResultType resultType;

    public LegacyResultRepositoryProvider(CoserBusinessConfig config,
                                          File basedir,
                                          ResultType resultType) {
        Preconditions.checkNotNull(config);
        Preconditions.checkNotNull(basedir);
        this.config = config;
        this.resultType = resultType;
        this.basedir = basedir;
    }

    @Override
    public LegacyResultRepositoryType getRepositoryType() {
        return LegacyResultRepositoryType.INSTANCE;
    }

    @Override
    public Set<LegacyResultRepository> loadRepositories() {

        if (log.isInfoEnabled()) {
            log.info(String.format("Scan for projects from basedir: %s", basedir));
        }
        Set<LegacyResultRepository> result;
        try {
            result = findAllProjectWithResult();
        } catch (CoserBusinessException e) {
            throw new ResultRepositoryInitializationException(this, "Could not find projects", e);
        }

        if (log.isInfoEnabled()) {
            log.info(String.format("Found %s result repository(ies) from basedir: %s", result.size(), basedir));
        }
        return result;
    }

    /**
     * Retourne tous les projets qui ont des résultats.
     *
     * De la forme d'une liste de de path (à la tree path) :
     * ProjetName/SelectionName/ResultName
     *
     * @return result paths
     * @throws CoserBusinessException
     */
    protected Set<LegacyResultRepository> findAllProjectWithResult() throws CoserBusinessException {

        ProjectService projectService = new ProjectService(config);

        Set<LegacyResultRepository> result = Sets.newHashSet();
        // loop on projets
        File[] projects = basedir.listFiles();
        if (projects != null) {
            for (File existingProject : projects) {
                if (existingProject.isDirectory()) {
                    String projectName = existingProject.getName();
                    // need to open project
                    Project p = projectService.openProject(projectName, basedir);

                    // loop on selections
                    File selectionsDirectory = new File(existingProject, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selections = selectionsDirectory.listFiles();
                    if (selections != null) {
                        for (File existingSelection : selections) {
                            if (existingSelection.isDirectory()) {
                                String selectionName = existingSelection.getName();
                                Selection s = new Selection(selectionName);

                                // loop on result
                                File rsufisDirectory = new File(existingSelection, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] rSufiResults = rsufisDirectory.listFiles();
                                if (rSufiResults != null) {
                                    for (File rSufiResult : rSufiResults) {
                                        if (rSufiResult.isDirectory()) {
                                            RSufiResult r = projectService.getRSufiResult(rSufiResult);
                                            boolean candidate = r.isPubliableResult();
                                            if (candidate) {
                                                RSufiResultPath path = new RSufiResultPath(p, s, r);
                                                if (log.isDebugEnabled()) {
                                                    log.debug("Detected result: " + result);
                                                }
                                                String surveyName = projectService.getProjectSurveyName(rSufiResult, r);

                                                LegacyResultRepository repository = new LegacyResultRepository(existingProject, path, surveyName);
                                                sanityRepository(repository);
                                                if (log.isInfoEnabled()) {
                                                    log.info(String.format("Detected result: %s - %s", path.getProject().getName(), path.getRsufiResult().getName()));
                                                }
                                                result.add(repository);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Sanity the given repository.
     *
     * @param repository repository to sanity
     */
    protected void sanityRepository(LegacyResultRepository repository) {
        if (resultType != null) {

            boolean needSave = false;
            RSufiResult rsufiResult = repository.getPath().getRsufiResult();
            if (resultType == ResultType.MAP) {
                // map repository
                if (repository.isIndicatorsResult()) {
                    // can't be both indicator result and maps result
                    needSave = true;
                    rsufiResult.setIndicatorsResult(false);
                    if (log.isInfoEnabled()) {
                        log.info("Will sanity repository to only map result");
                    }
                }
            }
            if (resultType == ResultType.INDICATOR) {
                {
                    // indicator repository
                    if (repository.isMapsResult()) {
                        // can't be both indicator result and maps result
                        needSave = true;
                        rsufiResult.setMapsResult(false);
                        if (log.isInfoEnabled()) {
                            log.info("Will sanity repository to only indicator result");
                        }
                    }
                }
                if (needSave) {
                    rsufiResult.save(repository.getResultDirectory());
                }
            }
        }
    }
}

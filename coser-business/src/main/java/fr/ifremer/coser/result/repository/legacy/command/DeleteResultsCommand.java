package fr.ifremer.coser.result.repository.legacy.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.CoserResult;
import fr.ifremer.coser.result.request.DeleteResultsRequest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class DeleteResultsCommand extends AbstractLegacyCommand<DeleteResultsRequest> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DeleteResultsCommand.class);

    @Override
    public boolean accept(DeleteResultsRequest request) {
        boolean result = repository.matchRepositoryType(request) &&
                         repository.matchResultType(request) &&
                         repository.isPubliableResult() &&
                         repository.matchZone(request);
        return result;
    }

    @Override
    public CoserResult execute(DeleteResultsRequest request) {
        File basedir = repository.getBasedir();
        if (log.isInfoEnabled()) {
            log.info("Will delete project at: " + basedir);
        }
        try {
            FileUtils.deleteDirectory(basedir);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not delete project at: " + basedir, e);
        }
        return newVoidResult();
    }

}

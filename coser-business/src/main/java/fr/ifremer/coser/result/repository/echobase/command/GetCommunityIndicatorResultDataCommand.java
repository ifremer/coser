package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.repository.echobase.EchoBasePredicates;
import fr.ifremer.coser.result.request.GetCommunityIndicatorResultDataRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorages;
import org.apache.commons.io.FileUtils;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetCommunityIndicatorResultDataCommand extends AbstractEchoBaseCommand<GetCommunityIndicatorResultDataRequest> {

    @Override
    public boolean accept(GetCommunityIndicatorResultDataRequest request) {
        boolean result = repository.matchFacade(request) &&
                         repository.matchZone(request);
        if (result) {
            Predicate<String[]> predicate = createPredicate(request);
            result = repository.matchCommunity(predicate);
        }
        return result;
    }

    @Override
    public FileResult execute(GetCommunityIndicatorResultDataRequest request) {

        Predicate<String[]> predicate = createPredicate(request);

        File file = getCommunityIndicatorDataFile(predicate,
                                                  request.getIndicator());

        FileResult result = newFileResult(file);
        return result;
    }

    protected File getCommunityIndicatorDataFile(Predicate<String[]> predicate,
                                                 String indicator) {

        try {

            File tempDir = FileUtil.createTempDirectory("coser-chart-population-indicator", "-tmp");

            File baseDir = new File(tempDir, repository.getSurveyName());
            FileUtils.forceMkdir(baseDir);

            // ajout du fichier csv avec les indicateurs
            DataStorage dataStorage = extractCommunity(predicate);
            File csvFile = DataStorages.save(dataStorage, "coser-chart-community-indicator", ".csv");

            File csvFileCopied = new File(baseDir, indicator + ".csv");
            FileUtils.copyFile(csvFile, csvFileCopied);
            FileUtils.forceDelete(csvFile);

            // make zip
            File result = File.createTempFile("coser-chart-community-indicator", ".zip");
            result.deleteOnExit();
            ZipUtil.compress(result, baseDir);

            // clean directory
            FileUtils.deleteDirectory(tempDir);
            return result;
        } catch (Exception e) {
            throw new CoserTechnicalException("Can't create zip file", e);
        }
    }

    // --------------------------------------------------------------------- //
    // --- Matchers -------------------------------------------------------- //
    // --------------------------------------------------------------------- //

    protected Predicate<String[]> createPredicate(GetCommunityIndicatorResultDataRequest request) {
        return Predicates.and(
                EchoBasePredicates.communityIndicatorPredicate(request.getIndicator()),
                EchoBasePredicates.communitySpeciesPredicate(request.getSpecies()));
    }
}

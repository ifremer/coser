package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.coser.CoserApplicationContext;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.request.DeleteResultsRequest;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.result.result.MapResult;
import fr.ifremer.coser.result.result.VoidResult;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * To execute request all over coser result repositories.
 *
 * This new API will let us to define any result format to be queried.
 *
 * Created on 3/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserRequestExecutor {

    /**
     * To get repositories.
     */
    protected final CoserMainRepositoryProvider repositoryProvider;

    /**
     * To create commands.
     */
    protected final CoserCommandFactory commandFactory;

    protected FileResult fileResult;

    protected List<CoserResult> multipleResults;

    protected MapResult mapResult;

    protected VoidResult voidResult;

    protected List<ResultRepository> matchingRepositories;

    public CoserRequestExecutor(CoserApplicationContext applicationContext,
                                CoserMainRepositoryProvider repositoryProvider) {
        Preconditions.checkNotNull(applicationContext);
        Preconditions.checkNotNull(repositoryProvider);
        this.commandFactory = applicationContext.getCommandFactory();
        this.repositoryProvider = repositoryProvider;
    }

    // --------------------------------------------------------------------- //
    // --- Execute API ----------------------------------------------------- //
    // --------------------------------------------------------------------- //

    /**
     * Extract some raw data and results from repositories and assembly them as an archive.
     *
     * @param context request context
     * @param request extract request
     * @return the file result containing the archive
     */
    public FileResult executeUnique(CoserRequestContext context,
                                    ExtractRawDataAndResultsRequest request) {

        File extractDirectory = new File(context.getTemporaryDirectory(), "Indicateurs_Ifremer");
        request.setExtractDirectory(extractDirectory);

        try {
            FileUtils.forceMkdir(extractDirectory);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not create directory: " + extractDirectory, e);
        }

        // Get all extracted stuff from matching repositories
        executeAll(context, request);

        File file = context.getExtracts().assemblyExtractResult(context,
                                                                extractDirectory,
                                                                request.getZoneList(),
                                                                (List) multipleResults);
        FileResult result = new FileResult("ALL", file);
        return result;
    }

    /**
     * Execute the given {@code request} to obtain a single result.
     *
     * A unique result repository must match this request, otherwise a {@link DuplicatedResultException} will be thrown.
     *
     * If no result repository matches the request a {@link NoResultRepositoryFoundException} will be thrown
     *
     * @param context request context
     * @param request request to execute
     * @return the executor
     */
    public CoserRequestExecutor executeUnique(CoserRequestContext context, CoserRequest request) {

        Preconditions.checkNotNull(request);
        Preconditions.checkArgument(request.isFilled(), "Request " + request + " is not filled.");

        List<ResultRepository> repositories = getMatchingRepositories(context, request);
        if (CollectionUtils.isEmpty(repositories)) {
            throw new NoResultRepositoryFoundException("No result repository matching request", request);
        }

        if (repositories.size() > 1) {
            throw new DuplicatedResultException(
                    repositories.toArray(new ResultRepository[repositories.size()]));
        }

        // Keep matching repository
        matchingRepositories = ImmutableList.copyOf(repositories);

        // Get first matching repository
        ResultRepository repository = getFirstMatchingRepository();

        // execute
        CoserResult result = execute(context, repository, request);

        // register result
        if (result instanceof FileResult) {
            this.fileResult = (FileResult) result;
        } else if (result instanceof MapResult) {
            this.mapResult = (MapResult) result;
        } else if (result instanceof VoidResult) {
            this.voidResult = (VoidResult) result;
        }
        return this;
    }

    /**
     * Execute the given {@code request} on the first matching result repository.
     *
     * If no result repository matches the request a {@link NoResultRepositoryFoundException} will be thrown.
     *
     * @param context request context
     * @param request request to execute
     * @return the result of the first matching repository.
     * @since 1.5
     */
    public CoserRequestExecutor executeFirst(CoserRequestContext context, CoserRequest request) {

        Preconditions.checkNotNull(request);
        Preconditions.checkArgument(request.isFilled(), "Request " + request + " is not filled.");

        List<ResultRepository> repositories = getMatchingRepositories(context, request);
        if (CollectionUtils.isEmpty(repositories)) {
            throw new NoResultRepositoryFoundException("No result repository matching request", request);
        }

        // Keep matching repositories
        matchingRepositories = ImmutableList.copyOf(repositories);

        // Get first matching repository
        ResultRepository repository = getFirstMatchingRepository();

        // execute
        CoserResult result = execute(context, repository, request);

        // register result
        if (result instanceof FileResult) {
            this.fileResult = (FileResult) result;
        } else if (result instanceof MapResult) {
            this.mapResult = (MapResult) result;
        } else if (result instanceof VoidResult) {
            this.voidResult = (VoidResult) result;
        }
        return this;
    }

    public void deleteResults(CoserRequestContext context,
                              DeleteResultsRequest request) {

        // do delete results (don't care about result)
        executeAll(context, request);

        // reload projects
        repositoryProvider.resetRepositories();
    }

    /**
     * Execute the given {@code request} on any matching result repository.
     *
     * If no result repository matches the request a {@link NoResultRepositoryFoundException} will be thrown.
     *
     * @param context request context
     * @param request request to execute
     * @return the list of result (one by each result repository).
     */
    public CoserRequestExecutor executeAll(CoserRequestContext context, CoserRequest request) {

        Preconditions.checkNotNull(request);
        Preconditions.checkArgument(request.isFilled(), "Request " + request + " is not filled.");

        List<CoserResult> result = Lists.newArrayList();

        List<ResultRepository> repositories = getMatchingRepositories(context, request);

        matchingRepositories = ImmutableList.copyOf(repositories);

        for (ResultRepository repository : repositories) {

            CoserResult repositoryResult = execute(context, repository, request);

            result.add(repositoryResult);
        }
        this.multipleResults = result;

        return this;
    }

    // --------------------------------------------------------------------- //
    // --- Result API ------------------------------------------------------ //
    // --------------------------------------------------------------------- //

    public MapResult toMapResult() {
        if (mapResult == null) {
            throw new CoserTechnicalException("No MapResult found");
        }
        return mapResult;
    }

    public FileResult toFileResult() {
        if (fileResult == null) {
            throw new CoserTechnicalException("No FileResult found");
        }
        return fileResult;
    }

    public <Result extends CoserResult> List<Result> toMultipleResult(Class<Result> resultType) {
        if (multipleResults == null) {
            throw new CoserTechnicalException("No MultipleResult found");
        }
        for (CoserResult coserResult : multipleResults) {
            if (!(resultType.isAssignableFrom(coserResult.getClass()))) {
                throw new CoserTechnicalException(
                        "Result should a " + resultType + ", but was: " + coserResult.getClass());
            }
        }
        return (List<Result>) multipleResults;
    }

    public Map<String, String> toMap() {
        List<MapResult> mapResults = toMultipleResult(MapResult.class);
        Map<String, String> resultAsMap = Maps.newHashMap();
        for (MapResult map : mapResults) {
            resultAsMap.putAll(map.getResult());
        }
        return sortByValue(resultAsMap);
    }

    protected static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map ) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ) {
                return (o1.getValue()).compareTo( o2.getValue() );
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }

    public List<ResultRepository> getMatchingRepositories() {
        return matchingRepositories;
    }

    public ResultRepository getFirstMatchingRepository() {
        return matchingRepositories.get(0);
    }

    // --------------------------------------------------------------------- //
    // --- Internal Methods ------------------------------------------------ //
    // --------------------------------------------------------------------- //

    protected List<ResultRepository> getMatchingRepositories(CoserRequestContext context, CoserRequest request) {

        List<ResultRepository> result = Lists.newArrayList();

        Set<ResultRepository> repositories = repositoryProvider.getResultRepositories();
        for (ResultRepository repository : repositories) {

            if (accept(context, repository, request)) {
                result.add(repository);
            }
        }
        return result;
    }

    protected <Request extends CoserRequest, Repository extends ResultRepository> boolean
    accept(CoserRequestContext context, Repository repository, Request request) {

        CoserCommand<Repository, Request> command = newCommand(context, repository, request);

        boolean result = command.accept(request);
        return result;
    }

    protected <Request extends CoserRequest, Repository extends ResultRepository> CoserResult
    execute(CoserRequestContext context, Repository repository, Request request) {

        CoserCommand<Repository, Request> command = newCommand(context, repository, request);

        CoserResult result = command.execute(request);
        if (result == null) {
            throw new NoResultFoundException(repository.getId(), request);
        }
        return result;
    }

    protected <Request extends CoserRequest, Repository extends ResultRepository> CoserCommand<Repository, Request>
    newCommand(CoserRequestContext context, Repository repository, Request request) {

        CoserCommand<Repository, Request> command =
                commandFactory.newCommand(context, repository, request);
        return command;
    }
}

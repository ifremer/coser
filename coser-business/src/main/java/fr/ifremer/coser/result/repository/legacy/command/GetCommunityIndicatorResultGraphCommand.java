package fr.ifremer.coser.result.repository.legacy.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import fr.ifremer.coser.result.repository.legacy.LegacyPredicates;
import fr.ifremer.coser.result.request.GetCommunityIndicatorResultGraphRequest;
import fr.ifremer.coser.result.result.FileResult;
import org.jfree.chart.JFreeChart;

import java.io.File;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetCommunityIndicatorResultGraphCommand extends AbstractLegacyCommand<GetCommunityIndicatorResultGraphRequest> {

    @Override
    public boolean accept(GetCommunityIndicatorResultGraphRequest request) {
        boolean result = repository.isIndicatorsResult() &&
                         repository.matchFacade(request) &&
                         repository.matchZone(request);
        if (result) {
            Predicate<String[]> predicate = Predicates.and(
                    LegacyPredicates.communityIndicatorPredicate(request.getIndicator()),
                    LegacyPredicates.communitySpeciesListPredicate(request.getSpecies()));
            result = repository.matchCommunity(predicate);
        }
        return result;
    }

    @Override
    public FileResult execute(GetCommunityIndicatorResultGraphRequest request) {

        // generate chart
        JFreeChart chart = generateCommunityChart(request.getZone(),
                                                  request.getIndicator(),
                                                  request.getSpecies());

        // generate file from chart
        File file = getCharts().generateChartFile("coser-chart-community-indicator-",
                                                  chart,
                                                  800,
                                                  400);

        FileResult result = newFileResult(file);
        return result;
    }
}

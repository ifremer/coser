package fr.ifremer.coser.result.repository.legacy.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.request.ExtractRawDataRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.result.util.Reports;
import org.apache.commons.io.FileUtils;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ExtractRawDataCommand extends AbstractLegacyCommand<ExtractRawDataRequest> {

    @Override
    public boolean accept(ExtractRawDataRequest request) {
        return repository.isDataResult() &&
               repository.matchFacade(request) &&
               repository.matchZone(request);
    }

    @Override
    public FileResult execute(ExtractRawDataRequest request) {

        Locale locale = getLocale();

        loadSelectionData();

        File resultZip;

        try {
            File tempDir = FileUtil.createTempDirectory("coser-source-", "-tmp");

            // il ne faut pas les fichiers de selection, mais leurs
            // export rsufi (sans les lignes, et les quotes)
            File archiveDir = extractRSUfiData(tempDir);

            // add decharge file
            String filename = Reports.getDechargeFilename(locale);
            File dechargePDF = new File(archiveDir, filename);

            Date lastDataUpdateDate = getConfig().getLastDataUpdateDate();

            getReports().generateDechargePDF(dechargePDF,
                                             locale,
                                             lastDataUpdateDate,
                                             repository.getSurveyName());

            // ajout du reftax dans le zip
            File reftaxFile = new File(repository.getBasedir(), CoserConstants.Category.REFTAX_SPECIES.getStorageFileName());
            FileUtils.copyFileToDirectory(reftaxFile, archiveDir);

            // make zip
            resultZip = File.createTempFile("coser-source-", ".zip");
            resultZip.deleteOnExit();
            ZipUtil.compress(resultZip, archiveDir);

            // clean directory
            FileUtils.deleteDirectory(tempDir);
        } catch (IOException e) {
            throw new CoserTechnicalException("Can't create zip file", e);
        }

        FileResult result = newFileResult(resultZip);
        return result;
    }

}

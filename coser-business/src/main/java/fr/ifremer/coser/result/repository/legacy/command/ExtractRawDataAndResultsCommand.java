package fr.ifremer.coser.result.repository.legacy.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import fr.ifremer.coser.bean.SpeciesMap;
import fr.ifremer.coser.result.repository.legacy.LegacyPredicates;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.result.ExtractRawDataAndResultsResult;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorageWalker;
import fr.ifremer.coser.storage.DataStorages;
import fr.ifremer.coser.util.DataType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.JFreeChart;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ExtractRawDataAndResultsCommand extends AbstractLegacyCommand<ExtractRawDataAndResultsRequest> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExtractRawDataAndResultsCommand.class);

    @Override
    public boolean accept(ExtractRawDataAndResultsRequest request) {
        boolean result = repository.matchExtractTypeList(request) &&
                         repository.matchZone(request);
        if (result && repository.isMapsResult()) {
            //FIXME Should we do something here? check species list...
        }
        if (result && repository.isIndicatorsResult()) {

            if (request.getExtractTypeList().contains(DataType.POPULATION) &&
                CollectionUtils.isNotEmpty(request.getPopulationIndicatorList())) {

                // match population indicator?
                Predicate<String[]> predicate = Predicates.and(
                        LegacyPredicates.populationIndicatorPredicate(request.getPopulationIndicatorList()),
                        LegacyPredicates.populationSpeciesPredicate(request.getSpeciesList()));
                result = repository.matchPopulation(predicate);
            }

            if (!result) {

                if (request.getExtractTypeList().contains(DataType.COMMUNITY) &&
                    CollectionUtils.isNotEmpty(request.getCommunityIndicatorList())) {

                    // match community indicator?
                    Predicate<String[]> predicate = Predicates.and(
                            LegacyPredicates.communityIndicatorPredicate(request.getCommunityIndicatorList()),
                            LegacyPredicates.communitySpeciesPredicate(request.getSpeciesList()));
                    result = repository.matchCommunity(predicate);
                }
            }
        }
        return result;
    }

    @Override
    public ExtractRawDataAndResultsResult execute(ExtractRawDataAndResultsRequest request) {

        List<DataType> extractTypeList = request.getExtractTypeList();
        List<String> speciesList = request.getSpeciesList();
        List<String> communityIndicatorList = request.getCommunityIndicatorList();
        List<String> populationIndicatorList = request.getPopulationIndicatorList();

        File extractDirectory = request.getExtractDirectory();

        // les sources se retrouve dans le zip a cote du pdf
        if (extractTypeList.contains(DataType.SOURCE) && repository.isDataResult()) {
            if (log.isDebugEnabled()) {
                log.debug("Extracting sources");
            }
            File srcDir = new File(extractDirectory, "sources");
            extractSource(srcDir);
        }

        // les cartes doivent se retrouver dans le pdf
        MultiKeyMap<String, File> pdfMaps = null;
        if (extractTypeList.contains(DataType.MAP) && repository.isMapsResult()) {
            if (log.isDebugEnabled()) {
                log.debug("Extracting maps");
            }
            pdfMaps = extractMaps(speciesList);
        }

        // les graphiques sont également dans le pdf
        MultiKeyMap<String, Pair<File, String>> pdfCharts = new MultiKeyMap<String, Pair<File, String>>();

        if (CollectionUtils.isNotEmpty(communityIndicatorList) && repository.isIndicatorsResult()) {
            if (log.isDebugEnabled()) {
                log.debug("Extracting community charts");
            }

            MultiKeyMap<String, Pair<File, String>> map = extractCommunityResults(communityIndicatorList,
                                                                                  repository.getZone(),
                                                                                  650,
                                                                                  430);
            pdfCharts.putAll(map);
        }
        if (CollectionUtils.isNotEmpty(populationIndicatorList) && repository.isIndicatorsResult()) {
            if (log.isDebugEnabled()) {
                log.debug("Extracting population charts");
            }
            MultiKeyMap<String, Pair<File, String>> map = extractPopulationResults(speciesList,
                                                                                   populationIndicatorList,
                                                                                   repository.getZone(),
                                                                                   650,
                                                                                   430);
            pdfCharts.putAll(map);
        }
        ExtractRawDataAndResultsResult result = newExtractRawDataAndResultsResult(pdfMaps, pdfCharts);
        return result;
    }

    /**
     * Generate raw data after selection in a sub directory (named of the zone) of the given directory.
     *
     * @param directory where to generate file
     */
    protected void extractSource(File directory) {

        loadSelectionData();

        // il ne faut pas les fichiers de selection, mais leurs
        // export rsufi (sans les lignes, et les quotes)
        File zoneDirectory = new File(directory, repository.getZone());

        extractRSUfiData(zoneDirectory);
    }

    /**
     * Extract all maps for matchings species.
     *
     * @param speciesList list of species to extract
     * @return map of map files indexed by zone - speciesName
     */
    protected MultiKeyMap<String, File> extractMaps(List<String> speciesList) {
        MultiKeyMap<String, File> pdfMaps;
        pdfMaps = new MultiKeyMap<String, File>();
        SpeciesMap speciesMap = repository.getSpeciesMap();
        String zone = repository.getZone();
        for (String species : speciesList) {
            File mapFile = repository.getMapSpeciesFile(species);
            String speciesName = speciesMap.getSpeciesName(species);
            pdfMaps.put(zone, speciesName, mapFile);
        }
        return pdfMaps;
    }

    /**
     * Generate community graph and their data for selected indicators.
     *
     * @param selectedIndicators indicators to extract
     * @param zone               zone
     * @param width              graph width
     * @param height             graph height
     * @return maps of generated graph image (temp file) and data indexed by natural key (indicator - species)
     */
    public MultiKeyMap<String, Pair<File, String>> extractCommunityResults(List<String> selectedIndicators,
                                                                           String zone,
                                                                           int width,
                                                                           int height) {

        // Get exact list of indicators usable for this result
        List<String> indicators = Lists.newArrayList(getCommunityIndicators());
        indicators.retainAll(selectedIndicators);

        //FIXME Check if this is correct
        // get for all indicators the first species list found (we only extract this one)
        final Map<String, String> indicatorLists = new HashMap<String, String>();
        walkOnCommunity(LegacyPredicates.communityIndicatorPredicate(indicators), new DataStorageWalker() {

            @Override
            public void onRow(String... row) {
                String indicatorCode = row[1];
                String localList = indicatorLists.get(indicatorCode);
                //FIXME Should only test if indicatorCode is present ?
                if (StringUtils.isBlank(localList)) {
                    String indicatorList = row[2];
                    indicatorLists.put(indicatorCode, indicatorList);
                }
            }
        });

        MultiKeyMap<String, Pair<File, String>> result = new MultiKeyMap<String, Pair<File, String>>();

        for (String indicator : indicators) {

            String speciesList = indicatorLists.get(indicator);

            Predicate<String[]> predicate = Predicates.and(
                    LegacyPredicates.communityIndicatorPredicate(indicator),
                    LegacyPredicates.communitySpeciesListPredicate(speciesList));

            // generate chart
            JFreeChart chart = generateCommunityChart(zone,
                                                      indicator,
                                                      speciesList);

            if (chart != null) {
                // generate chart file
                File chartFile = getCharts().generateChartFile("coser-community-chart-",
                                                               chart,
                                                               width,
                                                               height);

                // extract raw data
                DataStorage storage = extractCommunity(predicate);

                String rawDataText = DataStorages.toString(storage);

                result.put(zone, indicator, Pair.of(chartFile, rawDataText));

            }

        }
        return result;
    }

    /**
     * Generate population graph and their data for selected indicators.
     *
     * @param selectedSpecies    species to extract
     * @param selectedIndicators indicators to extract
     * @param zone               zone
     * @param width              graph width
     * @param height             graph height
     * @return maps of generated graph image (temp file) and data indexed by natural key (indicator - species)
     */
    public MultiKeyMap<String, Pair<File, String>> extractPopulationResults(List<String> selectedSpecies,
                                                                            List<String> selectedIndicators,
                                                                            String zone,
                                                                            int width,
                                                                            int height) {

        // Get exact list of species to use
        List<String> speciesList = Lists.newArrayList(getPopulationSpecies());
        speciesList.retainAll(selectedSpecies);

        MultiKeyMap<String, Pair<File, String>> result = new MultiKeyMap<String, Pair<File, String>>();

        for (String species : speciesList) {

            // Get extact indicators to use
            List<String> indicators = Lists.newArrayList(getPopulationIndicators(species));
            indicators.retainAll(selectedIndicators);

            for (String indicator : indicators) {

                Predicate<String[]> predicate = Predicates.and(
                        LegacyPredicates.populationIndicatorPredicate(indicator),
                        LegacyPredicates.populationSpeciesPredicate(species));

                // generate chart
                JFreeChart chart = generatePopulationChart(zone,
                                                           indicator,
                                                           species);

                if (chart != null) {

                    // generate chart file
                    File chartFile = getCharts().generateChartFile("coser-population-chart-",
                                                                   chart,
                                                                   width,
                                                                   height);

                    // extract raw data
                    DataStorage storage = extractPopulation(predicate);

                    String rawDataText = DataStorages.toString(storage);

                    result.put(zone, indicator + "-" + species, Pair.of(chartFile, rawDataText));

                }

            }
        }
        return result;
    }

}

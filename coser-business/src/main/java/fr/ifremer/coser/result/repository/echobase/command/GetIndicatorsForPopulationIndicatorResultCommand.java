package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.coser.result.repository.echobase.EchoBasePredicates;
import fr.ifremer.coser.result.request.GetIndicatorsForPopulationIndicatorResultRequest;
import fr.ifremer.coser.result.result.MapResult;

import java.util.Map;
import java.util.Set;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetIndicatorsForPopulationIndicatorResultCommand extends AbstractEchoBaseCommand<GetIndicatorsForPopulationIndicatorResultRequest> {

    @Override
    public boolean accept(GetIndicatorsForPopulationIndicatorResultRequest request) {
        boolean result = repository.isIndicatorsResult() &&
                         repository.matchFacade(request) &&
                         repository.matchZone(request);
        if (result) {

            Predicate<String[]> predicate = EchoBasePredicates.populationSpeciesPredicate(request.getSpecies());
            result = repository.matchPopulation(predicate);
        }
        return result;
    }

    @Override
    public MapResult execute(GetIndicatorsForPopulationIndicatorResultRequest request) {

        Set<String> indicatorList = getPopulationIndicators(request.getSpecies());
        Map<String, String> map = getIndicatorsMap().getIndicatorsValues(getLocale(), indicatorList);

        MapResult result = newMapResult(map);
        return result;
    }
}

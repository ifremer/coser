package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.result.request.GetMatchingRepositoryTypeForMapResultRequest;
import fr.ifremer.coser.result.result.VoidResult;

import java.io.File;

/**
 * Created on 6/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetMatchingRepositoryTypeForMapResultCommand extends AbstractEchoBaseCommand<GetMatchingRepositoryTypeForMapResultRequest> {

    @Override
    public boolean accept(GetMatchingRepositoryTypeForMapResultRequest request) {
        boolean result = repository.matchFacade(request) &&
                         repository.matchZone(request);
        if (result) {
            File file = repository.getMapSpeciesFile(request.getSpecies());
            result = file != null;
        }
        return result;
    }

    @Override
    public VoidResult execute(GetMatchingRepositoryTypeForMapResultRequest request) {
        return newVoidResult();
    }

}
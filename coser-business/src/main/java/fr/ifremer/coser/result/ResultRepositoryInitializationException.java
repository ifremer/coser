package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.result.repository.ResultRepositoryProvider;

/**
 * When an eror occurs while init a result repository.
 *
 * Created on 3/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ResultRepositoryInitializationException extends CoserTechnicalException {

    private static final long serialVersionUID = 1L;

    protected final ResultRepositoryProvider source;

    public ResultRepositoryInitializationException(ResultRepositoryProvider source, String message, Throwable cause) {
        super(message, cause);
        this.source = source;
    }
}

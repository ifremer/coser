package fr.ifremer.coser.result.repository.echobase;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.coser.result.CoserCommand;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.repository.echobase.command.CopyRepositoryCommand;
import fr.ifremer.coser.result.repository.echobase.command.DeleteResultsCommand;
import fr.ifremer.coser.result.repository.echobase.command.ExtractRawDataAndResultsCommand;
import fr.ifremer.coser.result.repository.echobase.command.ExtractRawDataCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetAllResultsCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetCommunityIndicatorResultDataCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetCommunityIndicatorResultGraphCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetIndicatorsForCommunityIndicatorResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetIndicatorsForExtractRawDataAndResultsCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetIndicatorsForPopulationIndicatorResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetMapResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetMatchingRepositoryTypeForMapResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetPopulationIndicatorResultDataCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetPopulationIndicatorResultGraphCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetResultNameCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetSpeciesForExtractRawDataAndResultsCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetSpeciesForMapResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetSpeciesForPopulationIndicatorResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetSpeciesListForCommunityIndicatorResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetZonesForCommunityIndicatorResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetZonesForExtractRawDataAndResultsCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetZonesForExtractRawDataCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetZonesForMapResultCommand;
import fr.ifremer.coser.result.repository.echobase.command.GetZonesForPopulationIndicatorResultCommand;
import fr.ifremer.coser.result.request.CopyRepositoryRequest;
import fr.ifremer.coser.result.request.DeleteResultsRequest;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.ExtractRawDataRequest;
import fr.ifremer.coser.result.request.GetAllResultsRequest;
import fr.ifremer.coser.result.request.GetCommunityIndicatorResultDataRequest;
import fr.ifremer.coser.result.request.GetCommunityIndicatorResultGraphRequest;
import fr.ifremer.coser.result.request.GetIndicatorsForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetIndicatorsForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetIndicatorsForPopulationIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetMapResultRequest;
import fr.ifremer.coser.result.request.GetMatchingRepositoryTypeForMapResultRequest;
import fr.ifremer.coser.result.request.GetPopulationIndicatorResultDataRequest;
import fr.ifremer.coser.result.request.GetPopulationIndicatorResultGraphRequest;
import fr.ifremer.coser.result.request.GetResultNameRequest;
import fr.ifremer.coser.result.request.GetSpeciesForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetSpeciesForMapResultRequest;
import fr.ifremer.coser.result.request.GetSpeciesForPopulationIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetSpeciesListForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetZonesForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetZonesForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetZonesForExtractRawDataRequest;
import fr.ifremer.coser.result.request.GetZonesForMapResultRequest;
import fr.ifremer.coser.result.request.GetZonesForPopulationIndicatorResultRequest;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseResultRepositoryType implements ResultRepositoryType {

    /**
     * Id of result source.
     */
    public static final String ID = "echobase";

    protected static final EchoBaseResultRepositoryType INSTANCE = new EchoBaseResultRepositoryType();

    private static final long serialVersionUID = 1L;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getLabelKey() {
        return n("coser.business.result.repository.type.echobase");
    }

    @Override
    public Set<ResultType> getResultTypes() {
        return Sets.immutableEnumSet(ResultType.MAP_AND_INDICATOR);
    }

    @Override
    public Map<Class<? extends CoserRequest>, Class<? extends CoserCommand>> getCommandTypes() {
        Map<Class<? extends CoserRequest>, Class<? extends CoserCommand>> result = Maps.newHashMap();

        // get map result
        result.put(GetZonesForMapResultRequest.class, GetZonesForMapResultCommand.class);
        result.put(GetSpeciesForMapResultRequest.class, GetSpeciesForMapResultCommand.class);
        result.put(GetMapResultRequest.class, GetMapResultCommand.class);

        // get community indicator result
        result.put(GetZonesForCommunityIndicatorResultRequest.class, GetZonesForCommunityIndicatorResultCommand.class);
        result.put(GetIndicatorsForCommunityIndicatorResultRequest.class, GetIndicatorsForCommunityIndicatorResultCommand.class);
        result.put(GetSpeciesListForCommunityIndicatorResultRequest.class, GetSpeciesListForCommunityIndicatorResultCommand.class);
        result.put(GetCommunityIndicatorResultDataRequest.class, GetCommunityIndicatorResultDataCommand.class);
        result.put(GetCommunityIndicatorResultGraphRequest.class, GetCommunityIndicatorResultGraphCommand.class);

        // get population indicator result
        result.put(GetZonesForPopulationIndicatorResultRequest.class, GetZonesForPopulationIndicatorResultCommand.class);
        result.put(GetSpeciesForPopulationIndicatorResultRequest.class, GetSpeciesForPopulationIndicatorResultCommand.class);
        result.put(GetIndicatorsForPopulationIndicatorResultRequest.class, GetIndicatorsForPopulationIndicatorResultCommand.class);
        result.put(GetPopulationIndicatorResultDataRequest.class, GetPopulationIndicatorResultDataCommand.class);
        result.put(GetPopulationIndicatorResultGraphRequest.class, GetPopulationIndicatorResultGraphCommand.class);

        // get all results
        result.put(GetAllResultsRequest.class, GetAllResultsCommand.class);

        // extract raw data
        result.put(GetZonesForExtractRawDataRequest.class, GetZonesForExtractRawDataCommand.class);
        result.put(ExtractRawDataRequest.class, ExtractRawDataCommand.class);

        // extract raw data and results
        result.put(GetZonesForExtractRawDataAndResultsRequest.class, GetZonesForExtractRawDataAndResultsCommand.class);
        result.put(GetSpeciesForExtractRawDataAndResultsRequest.class, GetSpeciesForExtractRawDataAndResultsCommand.class);
        result.put(GetIndicatorsForExtractRawDataAndResultsRequest.class, GetIndicatorsForExtractRawDataAndResultsCommand.class);
        result.put(ExtractRawDataAndResultsRequest.class, ExtractRawDataAndResultsCommand.class);

        // delete results
        result.put(DeleteResultsRequest.class, DeleteResultsCommand.class);

        // add results
        result.put(GetResultNameRequest.class, GetResultNameCommand.class);
        result.put(CopyRepositoryRequest.class, CopyRepositoryCommand.class);
        result.put(GetMatchingRepositoryTypeForMapResultRequest.class, GetMatchingRepositoryTypeForMapResultCommand.class);
        return result;
    }

}

package fr.ifremer.coser.result.util;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.storage.DataStorageWalker;
import org.apache.commons.lang3.StringUtils;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StatisticalLineAndShapeRenderer;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.nuiton.i18n.I18n;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Useful methods to generate charts.
 *
 * Created on 3/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class Charts {

    public File generateChartFile(String prefix,
                                  JFreeChart chart,
                                  int width,
                                  int height) {
        try {
            File file = File.createTempFile(prefix, ".png");
            file.deleteOnExit();
            ChartUtilities.saveChartAsPNG(file, chart, width, height);
            return file;
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't save chart", ex);
        }
    }

    public String getYearChartTitle(Locale locale) {
        Preconditions.checkNotNull(locale);
        String yearAxis = I18n.l(locale, "coser.business.year");
//        String yearAxis = "Year";
//        if ("fr".equals(locale.getLanguage())) {
//            yearAxis = "Ann\u00E9e";
//        } else if ("es".equals(locale.getLanguage())) {
//            yearAxis = "A\u00F1o";
//        }
        return yearAxis;
    }

    public JFreeChart generateCommunityChart(Locale locale,
                                             ExtractGraphDataWalker walker,
                                             IndicatorMap indicatorMap,
                                             String indicator,
                                             String chartTitle) {

        String indicatorName = indicatorMap.getIndicatorValue(locale, indicator);

        int multiplicator = walker.getMultiplicator();
        int minYear = walker.getMinYear();
        int maxYear = walker.getMaxYear();
        Map<Integer, Double[]> graphData = walker.getGraphData();

        // generate dataset with sorted data
        DefaultStatisticalCategoryDataset statisticalDataset = new DefaultStatisticalCategoryDataset();
        for (int indexYear = minYear; indexYear <= maxYear; ++indexYear) {
            Double[] entry = graphData.get(indexYear);
            if (entry != null) {
                Double estimation = entry[0] / multiplicator;
                Double ecart = entry[1] / multiplicator;
                statisticalDataset.add(estimation, ecart, "Serie1", (Comparable) indexYear);
            } else {
                statisticalDataset.add(null, null, "Serie1", indexYear);
            }
        }

        // configure chart
        String yearAxis = getYearChartTitle(locale);
        CategoryAxis categoryAxis = new CategoryAxis(yearAxis);
        categoryAxis.setCategoryMargin(0);
        categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);

        String unit = indicatorMap.getIndicatorUnit(indicator);
        // label horizontaux
        String legendY = indicatorName;
        if (multiplicator != 1) {
            // affiche par exemple : cm * 1000
            legendY += " (" + unit + "*" + multiplicator + ")";
        } else if (StringUtils.isNotEmpty(unit)) {
            legendY += " (" + unit + ")";
        }
        ValueAxis valueAxis = new NumberAxis(legendY);
        valueAxis.setUpperMargin(0.1);

        CategoryItemRenderer renderer = new StatisticalLineAndShapeRenderer(false, true);

        CategoryPlot plot = new CategoryPlot(statisticalDataset, categoryAxis, valueAxis, renderer);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart chart = new JFreeChart(chartTitle, JFreeChart.DEFAULT_TITLE_FONT, plot, true);

        // remove series legend
        chart.removeLegend();
        // white background
        chart.setBackgroundPaint(Color.WHITE);

        return chart;
    }

    public JFreeChart generatePopulationChart(Locale locale,
                                              ExtractGraphDataWalker walker,
                                              IndicatorMap indicatorMap,
                                              String indicator,
                                              String title) {

        if (!walker.isIndicatorFound()) {
            // indicator not found
            return null;
        }

        int multiplicator = walker.getMultiplicator();
        int minYear = walker.getMinYear();
        int maxYear = walker.getMaxYear();
        Map<Integer, Double[]> graphData = walker.getGraphData();

        String indicatorName = indicatorMap.getIndicatorValue(locale, indicator);

        // generate dataset with sorted data
        DefaultStatisticalCategoryDataset statisticalDataset = new DefaultStatisticalCategoryDataset();
        for (int indexYear = minYear; indexYear <= maxYear; ++indexYear) {
            Double[] entry = graphData.get(indexYear);
            if (entry != null) {
                Double estimation = entry[0] / multiplicator;
                Double ecart = entry[1] / multiplicator;
                statisticalDataset.add(estimation, ecart, "Serie1", (Comparable) indexYear);
            } else {
                statisticalDataset.add(null, null, "Serie1", indexYear);
            }
        }

        // configure chart
        String yearAxis = getYearChartTitle(locale);
        CategoryAxis categoryAxis = new CategoryAxis(yearAxis);
        categoryAxis.setCategoryMargin(0);
        categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);

        // label horizontaux
        String unit = indicatorMap.getIndicatorUnit(indicator);
        String legendY = indicatorName;
        if (multiplicator != 1) {
            // affiche par exemple : cm * 1000
            legendY += " (" + unit + "*" + multiplicator + ")";
        } else if (StringUtils.isNotEmpty(unit)) {
            legendY += " (" + unit + ")";
        }
        ValueAxis valueAxis = new NumberAxis(legendY);
        valueAxis.setUpperMargin(0.1);

        CategoryItemRenderer renderer = new StatisticalLineAndShapeRenderer(false, true);

        CategoryPlot plot = new CategoryPlot(statisticalDataset, categoryAxis, valueAxis, renderer);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, true);

        // remove series legend
        chart.removeLegend();
        // white background
        chart.setBackgroundPaint(Color.WHITE);

        return chart;
    }

    /**
     * To extract graph data from a indicator data storage.
     */
    public static abstract class ExtractGraphDataWalker implements DataStorageWalker {

        int multiplicator = 1;

        int minYear = Integer.MAX_VALUE;

        int maxYear = Integer.MIN_VALUE;

        boolean indicatorFound = false;

        Map<Integer, Double[]> graphData = new HashMap<Integer, Double[]>();

        protected abstract String getEstimationData(String... tuple);

        protected abstract String getEcartData(String... tuple);

        protected abstract String getYearData(String... tuple);

        @Override
        public void onRow(String... tuple) {

            indicatorFound = true;

            Double estimation = Double.parseDouble(getEstimationData(tuple));
            Double ecart = Double.parseDouble(getEcartData(tuple));
            int year = Integer.parseInt(getYearData(tuple));

            if (year < minYear) {
                minYear = year;
            }
            if (year > maxYear) {
                maxYear = year;
            }
            graphData.put(year, new Double[]{estimation, ecart*1.96});

            // si les données sont énormes, on affiche les données
            // / multiplicator et on le mentionne dans la légende
            if (estimation > 1e9) {
                multiplicator = 1000000;
            }
            if (estimation > 1e6 && multiplicator < 1000000) {
                multiplicator = 1000;
            }
        }

        public int getMultiplicator() {
            return multiplicator;
        }

        public int getMinYear() {
            return minYear;
        }

        public int getMaxYear() {
            return maxYear;
        }

        public boolean isIndicatorFound() {
            return indicatorFound;
        }

        public Map<Integer, Double[]> getGraphData() {
            return graphData;
        }
    }
}

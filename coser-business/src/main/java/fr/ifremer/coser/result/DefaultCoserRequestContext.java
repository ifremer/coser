package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserApplicationContext;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.util.Charts;
import fr.ifremer.coser.result.util.Extracts;
import fr.ifremer.coser.result.util.Reports;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created on 3/14/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class DefaultCoserRequestContext implements CoserRequestContext {

    protected final CoserApplicationContext applicationContext;

    protected final Locale locale;

    protected File temporaryDirectory;

    public DefaultCoserRequestContext(CoserApplicationContext applicationContext, Locale locale) {
        this.applicationContext = applicationContext;
        this.locale = locale;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public IndicatorMap getIndicatorMap() {
        return applicationContext.getIndicatorMap();
    }

    @Override
    public ZoneMap getZoneMap() {
        return applicationContext.getZoneMap();
    }

    @Override
    public Reports getReports() {
        return applicationContext.getReports();
    }

    @Override
    public Charts getCharts() {
        return applicationContext.getCharts();
    }

    @Override
    public Extracts getExtracts() {
        return applicationContext.getExtracts();
    }

    @Override
    public CoserBusinessConfig getConfig() {
        return applicationContext.getConfig();
    }

    @Override
    public File getTemporaryDirectory() {
        if (temporaryDirectory == null) {
            try {
                temporaryDirectory = FileUtil.createTempDirectory("coser-", "");
            } catch (IOException e) {
                throw new CoserTechnicalException("Could not create temporary directory.", e);
            }
        }
        return temporaryDirectory;
    }

}

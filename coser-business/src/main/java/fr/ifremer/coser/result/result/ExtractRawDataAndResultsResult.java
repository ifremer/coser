package fr.ifremer.coser.result.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.result.CoserResult;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;

/**
 * Created on 3/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ExtractRawDataAndResultsResult implements CoserResult {

    private static final long serialVersionUID = 1L;

    protected final String source;

    protected final MultiKeyMap<String, File> pdfMaps;

    protected final MultiKeyMap<String, Pair<File, String>> pdfCharts;

    public ExtractRawDataAndResultsResult(String source,
                                          MultiKeyMap<String, File> pdfMaps,
                                          MultiKeyMap<String, Pair<File, String>> pdfCharts) {
        this.source = source;
        this.pdfMaps = pdfMaps;
        this.pdfCharts = pdfCharts;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public Pair<MultiKeyMap<String, File>, MultiKeyMap<String, Pair<File, String>>> getResult() {
        return Pair.of(pdfMaps, pdfCharts);
    }
}

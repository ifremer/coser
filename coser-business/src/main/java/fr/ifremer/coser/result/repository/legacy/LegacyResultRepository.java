package fr.ifremer.coser.result.repository.legacy;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.bean.EchoBaseProject;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.bean.SpeciesListMap;
import fr.ifremer.coser.bean.SpeciesMap;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.request.CoserRequestExtractTypeListAware;
import fr.ifremer.coser.result.request.CoserRequestFacadeAware;
import fr.ifremer.coser.result.request.CoserRequestRepositoryResultTypeAware;
import fr.ifremer.coser.result.request.CoserRequestRepositoryTypeAware;
import fr.ifremer.coser.result.request.CoserRequestZoneAware;
import fr.ifremer.coser.result.request.CoserRequestZoneListAware;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorages;
import fr.ifremer.coser.util.DataType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class LegacyResultRepository implements ResultRepository {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LegacyResultRepository.class);

    /**
     * Project basedir.
     */
    protected final File basedir;

    /**
     * Results directory.
     */
    protected final File resultDirectory;

    /**
     * Maps directory.
     */
    protected final File mapsDirectory;

    /**
     * Project definition.
     */
    protected final RSufiResultPath path;

    /**
     * Transform a map result species file to the species code.
     */
    protected final Function<File, String> mapFileToSpeciesCode;

    /**
     * Transform a species code to his map result species filename.
     */
    protected final Function<String, String> speciesCodeToMapFile;

    /**
     * Filter map result species file.
     */
    protected final FilenameFilter mapSpeciesFilenameFilter;

    /**
     * Survey name (used to get maps).
     */
    protected final String surveyName;

    /**
     * Cache of species definition.
     */
    protected SpeciesMap speciesMap;

    /**
     * Cache of species list definition.
     */
    protected SpeciesListMap speciesListMap;

    /**
     * Unique id of the result
     */
    protected final String id;

    public LegacyResultRepository(File basedir,
                                  RSufiResultPath path,
                                  String surveyName) {
        Preconditions.checkNotNull(basedir);
        Preconditions.checkNotNull(path);
        Preconditions.checkNotNull(surveyName);
        this.basedir = basedir;
        this.surveyName = surveyName;
        this.resultDirectory = FileUtils.getFile(
                basedir,
                CoserConstants.STORAGE_SELECTION_DIRECTORY,
                path.getSelection().getName(),
                CoserConstants.STORAGE_RESULTS_DIRECTORY,
                path.getRsufiResult().getName());
        this.mapsDirectory = new File(resultDirectory, CoserConstants.STORAGE_MAPS_DIRECTORY);
        this.path = path;

        this.mapFileToSpeciesCode = EchoBaseProject.newMapFileToSpeciesCode(surveyName);
        this.speciesCodeToMapFile = EchoBaseProject.newSpeciesCodeToMapFileName(surveyName);
        this.mapSpeciesFilenameFilter = EchoBaseProject.newMapSpeciesFilenameFilter(surveyName);

        this.id = String.format("%s::%s", LegacyResultRepositoryType.ID, resultDirectory);

        if (log.isInfoEnabled()) {
            log.info("New result repository: " + id);
        }
    }

    // --------------------------------------------------------------------- //
    // --- Public API ------------------------------------------------------ //
    // --------------------------------------------------------------------- //

    @Override
    public String getId() {
        return id;
    }

    @Override
    public LegacyResultRepositoryType getResultRepositoryType() {
        return LegacyResultRepositoryType.INSTANCE;
    }

    @Override
    public String getSurveyName() {
        return surveyName;
    }

    @Override
    public File getBasedir() {
        return basedir;
    }

    @Override
    public String getProjectName() {
        return path.getProject().getName();
    }

    @Override
    public String getZone() {
        return path.getRsufiResult().getZone();
    }

    @Override
    public boolean isMapsResult() {
        return path.getRsufiResult().isMapsResult();
    }

    @Override
    public boolean isIndicatorsResult() {
        return path.getRsufiResult().isIndicatorsResult();
    }

    @Override
    public boolean isPubliableResult() {
        return path.getRsufiResult().isPubliableResult();
    }

    @Override
    public boolean isDataResult() {
        return path.getRsufiResult().isDataAllowed();
    }

    public File getResultDirectory() {
        return resultDirectory;
    }

    public RSufiResultPath getPath() {
        return path;
    }

    public String getSelectionName() {
        return path.getSelection().getName();
    }

    public String getResultName() {
        return path.getRsufiResult().getName();
    }

    // --------------------------------------------------------------------- //
    // --- Matchers -------------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public boolean matchFacade(CoserRequestFacadeAware request) {
        if (log.isTraceEnabled()) {
            log.trace("No usage of facade in request: " + request);
        }
        return true;
    }

    public boolean matchZone(CoserRequestZoneAware request) {
        return getZone().equals(request.getZone());
    }

    public boolean matchZone(CoserRequestZoneListAware request) {
        List<String> zoneList = request.getZoneList();
        boolean result = zoneList.contains(getZone());
        return result;
    }

    public boolean matchRepositoryType(CoserRequestRepositoryTypeAware request) {
        boolean result = request.getRepositoryType().equals(LegacyResultRepositoryType.ID);
        return result;
    }

    public boolean matchResultType(CoserRequestRepositoryResultTypeAware request) {
        boolean result;
        switch (request.getResultType()) {
            case MAP:
                result = isMapsResult();
                break;
            case INDICATOR:
                result = isIndicatorsResult();
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    public boolean matchExtractTypeList(CoserRequestExtractTypeListAware request) {
        boolean result = false;
        if (isMapsResult()) {
            result = request.getExtractTypeList().contains(DataType.MAP);
        } else if (isIndicatorsResult()) {
            result = request.getExtractTypeList().contains(DataType.POPULATION) ||
                     request.getExtractTypeList().contains(DataType.COMMUNITY);
        }
        if (!result && isDataResult()) {
            result = request.getExtractTypeList().contains(DataType.SOURCE);
        }
        return result;
    }

    public boolean matchCommunity(Predicate<String[]> predicate) {
        Preconditions.checkNotNull(predicate);

        DataStorage storage = getCommunityIndicatorStorage();
        boolean result = DataStorages.match(storage, predicate, true);

        return result;
    }

    public boolean matchPopulation(Predicate<String[]> predicate) {
        Preconditions.checkNotNull(predicate);

        DataStorage storage = getPopulationIndicatorStorage();
        boolean result = DataStorages.match(storage, predicate, true);

        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Get Map result -------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public File getMapSpeciesFile(String species) {
        String fileName = speciesCodeToMapFile.apply(species);
        File file = fileName == null ? null : new File(mapsDirectory, fileName);
        return file;
    }

    public Map<String, String> getMapSpeciesMap() {
        Set<String> speciesList = Sets.newHashSet();
        File[] files = mapsDirectory.listFiles(mapSpeciesFilenameFilter);
        if (files != null) {
            List<String> transform = Lists.transform(Lists.newArrayList(files), mapFileToSpeciesCode);
            speciesList.addAll(transform);
        }
        Map<String, String> result = getSpeciesMap().getSpeciesSubMap(speciesList);
        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Get definition maps --------------------------------------------- //
    // --------------------------------------------------------------------- //

    public SpeciesListMap getSpeciesListMap() {
        if (speciesListMap == null) {
            File file = new File(basedir, CoserConstants.Category.TYPE_ESPECES.getStorageFileName());
            speciesListMap = new SpeciesListMap(file);
        }
        return speciesListMap;
    }

    public SpeciesMap getSpeciesMap() {
        if (speciesMap == null) {

            File file = new File(basedir, CoserConstants.Category.REFTAX_SPECIES.getStorageFileName());
            speciesMap = new SpeciesMap(file);
        }
        return speciesMap;
    }

    // --------------------------------------------------------------------- //
    // --- storage methods ------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public DataStorage getPopulationIndicatorStorage() {
        File file = new File(resultDirectory, path.getRsufiResult().getEstPopIndName());
        // Campagne Indicateur  Liste   Espece  Strate  Annee   Estimation  EcartType   CV
        DataStorage result = DataStorages.load(file, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
        return result;
    }

    public DataStorage getCommunityIndicatorStorage() {
        File file = new File(resultDirectory, path.getRsufiResult().getEstComIndName());
        // Campagne Indicateur  Liste   Espece  Strate  Annee   Estimation  EcartType   CV
        DataStorage result = DataStorages.load(file, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
        return result;
    }
}

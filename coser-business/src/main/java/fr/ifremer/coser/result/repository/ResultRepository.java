package fr.ifremer.coser.result.repository;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * General contract of a repository of results in CoserWeb.
 *
 * Created on 3/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public interface ResultRepository {

    /**
     * @return the unique id of the repository
     */
    String getId();

    /**
     * @return the type of result repository
     */
    ResultRepositoryType getResultRepositoryType();

    /**
     * @return base directory of the result repository
     */
    File getBasedir();

    String getProjectName();

    String getZone();

    String getSurveyName();

    boolean isMapsResult();

    boolean isIndicatorsResult();

    boolean isDataResult();

    boolean isPubliableResult();
}

package fr.ifremer.coser.result.repository.echobase;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.coser.bean.EchoBaseProject;
import fr.ifremer.coser.result.ResultRepositoryInitializationException;
import fr.ifremer.coser.result.repository.ResultRepositoryProvider;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.AndFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Set;

/**
 * Created on 3/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseResultRepositoryProvider implements ResultRepositoryProvider<EchoBaseResultRepository> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseResultRepositoryProvider.class);

    /**
     * Directory where are stored all echobase projects.
     */
    protected final File basedir;

    public EchoBaseResultRepositoryProvider(File basedir) {
        Preconditions.checkNotNull(basedir);
        this.basedir = basedir;
    }

    @Override
    public EchoBaseResultRepositoryType getRepositoryType() {
        return EchoBaseResultRepositoryType.INSTANCE;
    }

    @Override
    public Set<EchoBaseResultRepository> loadRepositories() {

        if (log.isInfoEnabled()) {
            log.info(String.format("Scan for projects from basedir: %s", basedir));
        }
        Set<EchoBaseResultRepository> result = Sets.newHashSet();

        AndFileFilter projectFilter = new AndFileFilter();
        projectFilter.addFileFilter(FileFilterUtils.directoryFileFilter());
        projectFilter.addFileFilter(new AbstractFileFilter() {

            @Override
            public boolean accept(File pathname) {
                return new File(pathname, EchoBaseProject.METADATA_FILE).exists();
            }
        });

        File[] projects = basedir.listFiles((FileFilter) projectFilter);
        if (projects != null) {
            for (File projectDirectory : projects) {

                EchoBaseProject project = new EchoBaseProject(projectDirectory);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Detected result: %s", project.getName()));
                }
                try {
                    project.load();
                } catch (IOException e) {
                    throw new ResultRepositoryInitializationException(this, "Could not load project file", e);
                }
                result.add(new EchoBaseResultRepository(project));
            }
        }
        if (log.isInfoEnabled()) {
            log.info(String.format("Found %s result repository(ies) from basedir: %s", result.size(), basedir));
        }
        return result;
    }
}

package fr.ifremer.coser.result;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.util.Charts;
import fr.ifremer.coser.result.util.Extracts;
import fr.ifremer.coser.result.util.Reports;

import java.io.File;
import java.util.Locale;

/**
 * Useful object used by commands.
 *
 * Created on 3/14/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public interface CoserRequestContext {

    Locale getLocale();

    IndicatorMap getIndicatorMap();

    ZoneMap getZoneMap();

    Reports getReports();

    Charts getCharts();

    Extracts getExtracts();

    CoserBusinessConfig getConfig();

    File getTemporaryDirectory();

}

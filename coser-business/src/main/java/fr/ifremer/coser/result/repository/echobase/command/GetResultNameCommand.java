package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.coser.result.request.GetResultNameRequest;
import fr.ifremer.coser.result.result.MapResult;
import fr.ifremer.coser.util.DataType;

import java.util.List;
import java.util.Map;

/**
 * Created on 3/18/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetResultNameCommand extends AbstractEchoBaseCommand<GetResultNameRequest> {

    @Override
    public boolean accept(GetResultNameRequest request) {
        List<DataType> extractTypeList = request.getExtractTypeList();
        boolean result = false;
        if (extractTypeList.contains(DataType.MAP)) {
            result = repository.isMapsResult();
        }
        if (!result && (extractTypeList.contains(DataType.POPULATION) || extractTypeList.contains(DataType.COMMUNITY))) {
            result = repository.isIndicatorsResult();
        }
        if (!result && extractTypeList.contains(DataType.SOURCE)) {
            result = repository.isDataResult();
        }
        return result;
    }

    @Override
    public MapResult execute(GetResultNameRequest request) {
        String zoneId = repository.getZone();
        String resultname = repository.getProjectName();
        Map<String, String> map = Maps.newHashMap();
        map.put(zoneId, resultname);
        return newMapResult(map);
    }
}

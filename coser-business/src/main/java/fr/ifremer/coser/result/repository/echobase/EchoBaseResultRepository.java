package fr.ifremer.coser.result.repository.echobase;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.coser.bean.EchoBaseProject;
import fr.ifremer.coser.bean.SpeciesMap;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.request.CoserRequestExtractTypeListAware;
import fr.ifremer.coser.result.request.CoserRequestFacadeAware;
import fr.ifremer.coser.result.request.CoserRequestRepositoryResultTypeAware;
import fr.ifremer.coser.result.request.CoserRequestRepositoryTypeAware;
import fr.ifremer.coser.result.request.CoserRequestZoneAware;
import fr.ifremer.coser.result.request.CoserRequestZoneListAware;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorages;
import fr.ifremer.coser.util.DataType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseResultRepository implements ResultRepository {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseResultRepository.class);

    /**
     * Project definition.
     */
    protected final EchoBaseProject project;

    /**
     * Transform a map result species file to the species code.
     */
    protected final Function<File, String> mapFileToSpeciesCode;

    /**
     * Transform a species code to his map result species filename.
     */
    protected final Function<String, String> speciesCodeToMapFile;

    /**
     * Filter map result species file.
     */
    protected final FilenameFilter mapSpeciesFilenameFilter;

    /**
     * Cache of species definition.
     */
    protected SpeciesMap speciesMap;

    /**
     * Unique id of the result repository.
     */
    protected final String id;

    public EchoBaseResultRepository(EchoBaseProject project) {
        Preconditions.checkNotNull(project);
        this.project = project;

        String surveyName = project.getSurveyName();
        this.mapFileToSpeciesCode = EchoBaseProject.newMapFileToSpeciesCode(surveyName);
        this.speciesCodeToMapFile = EchoBaseProject.newSpeciesCodeToMapFileName(surveyName);
        this.mapSpeciesFilenameFilter = EchoBaseProject.newMapSpeciesFilenameFilter(surveyName);
        this.id = EchoBaseResultRepositoryType.ID + "::" + project.getBasedir();

        if (log.isInfoEnabled()) {
            log.info("New result repository: " + id);
        }
    }

    // --------------------------------------------------------------------- //
    // --- Public API ------------------------------------------------------ //
    // --------------------------------------------------------------------- //

    @Override
    public String getId() {
        return id;
    }

    @Override
    public EchoBaseResultRepositoryType getResultRepositoryType() {
        return EchoBaseResultRepositoryType.INSTANCE;
    }

    @Override
    public File getBasedir() {
        return project.getBasedir();
    }

    @Override
    public String getProjectName() {
        return project.getName();
    }

    @Override
    public String getSurveyName() {
        return project.getSurveyName();
    }

    @Override
    public String getZone() {
        return project.getZoneName();
    }

    @Override
    public boolean isMapsResult() {
        return true;
    }

    @Override
    public boolean isIndicatorsResult() {
        return true;
    }

    @Override
    public boolean isDataResult() {
        return true;
    }

    @Override
    public boolean isPubliableResult() {
        return project.isPubliableResult();
    }

    public EchoBaseProject getProject() {
        return project;
    }

    // --------------------------------------------------------------------- //
    // --- Matchers -------------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public boolean matchFacade(CoserRequestFacadeAware request) {
        return getProject().getFacadeName().equals(request.getFacade());
    }

    public boolean matchZone(CoserRequestZoneAware request) {
        return getZone().equals(request.getZone());
    }

    public boolean matchZone(CoserRequestZoneListAware request) {
        List<String> zoneList = request.getZoneList();
        boolean result = zoneList.contains(getZone());
        return result;
    }

    public boolean matchRepositoryType(CoserRequestRepositoryTypeAware request) {
        boolean result = request.getRepositoryType().equals(EchoBaseResultRepositoryType.ID);
        return result;
    }

    public boolean matchResultType(CoserRequestRepositoryResultTypeAware request) {
        ResultType resultType = request.getResultType();
        boolean result = ResultType.MAP_AND_INDICATOR == resultType;
        return result;
    }

    public boolean matchExtractTypeList(CoserRequestExtractTypeListAware request) {
        boolean result = false;
        List<DataType> extractTypeList = request.getExtractTypeList();
        if (extractTypeList.contains(DataType.MAP)) {
            result = isMapsResult();
        }

        if (!result && (extractTypeList.contains(DataType.POPULATION) ||
                        extractTypeList.contains(DataType.COMMUNITY))) {
            result = isIndicatorsResult();
        }
        if (!result && extractTypeList.contains(DataType.SOURCE)) {
            result = isDataResult();
        }
        return result;
    }

    public boolean matchCommunity(Predicate<String[]> predicate) {
        Preconditions.checkNotNull(predicate);

        DataStorage storage = getCommunityIndicatorStorage();
        boolean result = DataStorages.match(storage, predicate, true);

        return result;
    }

    public boolean matchPopulation(Predicate<String[]> predicate) {
        Preconditions.checkNotNull(predicate);

        DataStorage storage = getPopulationIndicatorStorage();
        boolean result = DataStorages.match(storage, predicate, true);

        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Get Map result -------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public File getMapSpeciesFile(String species) {
        String fileName = speciesCodeToMapFile.apply(species);
        File file = fileName == null ? null : new File(project.getMapsDirectory(), fileName);
        return file;
    }

    public Map<String, String> getMapSpecies() {
        Set<String> speciesList = Sets.newHashSet();
        File[] files = project.getMapsDirectory().listFiles(mapSpeciesFilenameFilter);
        if (files != null) {
            List<String> transform = Lists.transform(Lists.newArrayList(files), mapFileToSpeciesCode);
            speciesList.addAll(transform);
        }
        Map<String, String> result = getSpeciesMap().getSpeciesSubMap(speciesList);
        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Get definition maps --------------------------------------------- //
    // --------------------------------------------------------------------- //

    public SpeciesMap getSpeciesMap() {
        if (speciesMap == null) {
            File file = project.getSpeciesDefinitionFile();
            speciesMap = new SpeciesMap(file);
        }
        return speciesMap;
    }

    // --------------------------------------------------------------------- //
    // --- storage methods ------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public DataStorage getPopulationIndicatorStorage() {
        File file = project.getPopulationIndicatorsFile();
        // Campagne;Indicateur;Espece;Strate;Annee;Estimation;EcartType;CV
        DataStorage result = DataStorages.load(file);
        return result;
    }

    public DataStorage getCommunityIndicatorStorage() {
        File file = project.getCommunityIndicatorsFile();
        // Campagne;Indicateur;Espece;Strate;Annee;Estimation;EcartType;CV
        DataStorage result = DataStorages.load(file);
        return result;
    }

}

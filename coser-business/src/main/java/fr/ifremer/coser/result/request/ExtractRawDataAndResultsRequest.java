package fr.ifremer.coser.result.request;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.util.DataType;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.List;

/**
 * Request to extract data.
 *
 * Created on 3/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ExtractRawDataAndResultsRequest implements CoserRequest, CoserRequestZoneListAware, CoserRequestExtractTypeListAware {

    private static final long serialVersionUID = 1L;

    protected List<String> zoneList;

    protected List<String> populationIndicatorList;

    protected List<String> communityIndicatorList;

    protected List<String> speciesList;

    protected List<DataType> extractTypeList;

    /**
     * Where to extract all data and graphs.
     */
    protected File extractDirectory;

    @Override
    public boolean isFilled() {
        boolean filled = CollectionUtils.isNotEmpty(extractTypeList)
                         && CollectionUtils.isNotEmpty(zoneList);

        if (filled) {
            if (DataType.isNeedSpecies(extractTypeList)) {
                filled = CollectionUtils.isNotEmpty(speciesList);
            }
        }

        if (filled) {
            if (extractTypeList.contains(DataType.COMMUNITY)) {
                filled = CollectionUtils.isNotEmpty(communityIndicatorList);
            }
        }
        if (filled) {
            if (extractTypeList.contains(DataType.POPULATION)) {
                filled = CollectionUtils.isNotEmpty(populationIndicatorList);
            }
        }

        return filled;
    }

    @Override
    public List<DataType> getExtractTypeList() {
        return extractTypeList;
    }

    @Override
    public void setExtractTypeList(List<DataType> extractTypeSet) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(extractTypeSet));
        this.extractTypeList = extractTypeSet;
    }

    @Override
    public List<String> getZoneList() {
        return zoneList;
    }

    @Override
    public void setZoneList(List<String> zoneList) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(zoneList));
        this.zoneList = zoneList;
    }

    public List<String> getPopulationIndicatorList() {
        return populationIndicatorList;
    }

    public void setPopulationIndicatorList(List<String> populationIndicatorList) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(populationIndicatorList));
        this.populationIndicatorList = populationIndicatorList;
    }

    public List<String> getCommunityIndicatorList() {
        return communityIndicatorList;
    }

    public void setCommunityIndicatorList(List<String> communityIndicatorList) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(communityIndicatorList));
        this.communityIndicatorList = communityIndicatorList;
    }

    public List<String> getSpeciesList() {
        return speciesList;
    }

    public void setSpeciesList(List<String> speciesList) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(speciesList));
        this.speciesList = speciesList;
    }

    public void setExtractDirectory(File extractDirectory) {
        this.extractDirectory = extractDirectory;
    }

    public File getExtractDirectory() {
        return extractDirectory;
    }
}

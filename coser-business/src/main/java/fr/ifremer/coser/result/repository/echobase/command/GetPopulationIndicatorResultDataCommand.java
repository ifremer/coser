package fr.ifremer.coser.result.repository.echobase.command;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import fr.ifremer.coser.result.repository.echobase.EchoBasePredicates;
import fr.ifremer.coser.result.request.GetPopulationIndicatorResultDataRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.DataStorages;

import java.io.File;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetPopulationIndicatorResultDataCommand extends AbstractEchoBaseCommand<GetPopulationIndicatorResultDataRequest> {

    @Override
    public boolean accept(GetPopulationIndicatorResultDataRequest request) {
        boolean result = repository.isIndicatorsResult() &&
                         repository.matchFacade(request) &&
                         repository.matchZone(request);
        if (result) {
            Predicate<String[]> predicate = createPredicate(request);
            result = repository.matchPopulation(predicate);
        }
        return result;
    }

    @Override
    public FileResult execute(GetPopulationIndicatorResultDataRequest request) {

        Predicate<String[]> predicate = createPredicate(request);

        DataStorage dataStorage = extractPopulation(predicate);
        File file = DataStorages.save(dataStorage, "coser-chart-population-indicator", ".csv");

        FileResult result = newFileResult(file);
        return result;
    }

    protected Predicate<String[]> createPredicate(GetPopulationIndicatorResultDataRequest request) {
        return Predicates.and(
                EchoBasePredicates.populationSpeciesPredicate(request.getSpecies()),
                EchoBasePredicates.populationIndicatorPredicate(request.getIndicator()));
    }

}

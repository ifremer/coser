/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import static org.nuiton.i18n.I18n.n;

/**
 * Coser constants.
 *
 * @author chatellier
 */
public class CoserConstants {

    /** CSV Separator. */
    public static final char CSV_SEPARATOR_CHAR = ';';

    /** CSV Separator for result file (estcomind/estpopind). */
    public static final char CSV_RESULT_SEPARATOR_CHAR = '\t';

    /** CSV File encoding. */
    public static final String CSV_FILE_ENCODING = "UTF-8";

    /** Valeur NA valide si pas de valeur. */
    public static final String VALIDATION_NA = "NA";

    /** Nom du dossier de sauvegarde des fichiers originaux. */
    public static final String STORAGE_ORIGINAL_DIRECTORY = "original";

    /** Nom du dossier de sauvegarde des fichiers apres control. */
    public static final String STORAGE_CONTROL_DIRECTORY = "control";

    /** Nom du dossier de sauvegarde des selections. */
    public static final String STORAGE_SELECTION_DIRECTORY = "selections";

    /** Nom du dossier de stockage des autres fichier de la selection a inclure dans l'export rsufi. */
    public static final String STORAGE_SELECTION_FILES = "others";

    /** Nom du dossier des resultats RSufi. */
    public static final String STORAGE_RESULTS_DIRECTORY = "results";

    /** Nom du dossier de stockage des maps (project AND results). */
    public static final String STORAGE_MAPS_DIRECTORY = "maps";

    /** Nom du dossier de stockage des autres fichier de resultats rsufi. */
    public static final String STORAGE_RESULT_FILES = "others";

    /** Suffix des nom de fichiers data apres control. */
    public static final String STORAGE_CONTROL_SUFFIX = "_co";

    /** Suffix des nom de fichiers data contenant les données supprimées. */
    public static final String STORAGE_DELECTED_SUFFIX = "_del";

    /** Suffix des nom de fichiers data apres selection. */
    public static final String STORAGE_SELECTION_SUFFIX = "_se";

    /** Categories des données manipulées. */
    public enum Category {
        CATCH(n("coser.business.category.catch"), "catch.csv", true),
        STRATA(n("coser.business.category.strata"), "strata.csv", true),
        HAUL(n("coser.business.category.haul"), "haul.csv", true),
        LENGTH(n("coser.business.category.length"), "length.csv", true),
        REFTAX_SPECIES(n("coser.business.category.reftax.species"), "reftaxSpecies.csv", false),
        TYPE_ESPECES(n("coser.business.category.typeEspece"), "codeTypeEspeces.csv", false);

        protected String translationKey;

        protected String storageFileName;

        /** Data category (par opposition reftax n'est pas un fichier de data). */
        protected boolean dataCategory;

        Category(String translationKey, String storageFileName, boolean dataCategory) {
            this.translationKey = translationKey;
            this.storageFileName = storageFileName;
            this.dataCategory = dataCategory;
        }

        public String getTranslationKey() {
            return translationKey;
        }

        /**
         * Nom de stockage interne a coser.
         *
         * N'est pas utilisé pour les categorie de données (nom original utilisé).
         *
         * @return le nom de stockage (pour les fichier de référence)
         */
        public String getStorageFileName() {

            if (dataCategory) {
                throw new IllegalStateException("Can't use storageFileName for data category !");
            }

            return storageFileName;
        }

        /**
         * Data category (par opposition reftax n'est pas un fichier de data).
         *
         * @return {@code true} si la category correspond a des data
         */
        public boolean isDataCategory() {
            return dataCategory;
        }
    }

    /** Différent niveau de message de validation. */
    public enum ValidationLevel {
        INFO("info"),
        WARNING("warning"),
        ERROR("error"),
        FATAL("fatal");

        protected String xworkContext;

        ValidationLevel(String xworkContext) {
            this.xworkContext = xworkContext;
        }

        public String getXWorkContext() {
            return xworkContext;
        }
    }
}

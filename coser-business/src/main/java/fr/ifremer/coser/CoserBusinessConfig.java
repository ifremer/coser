/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import org.apache.commons.io.IOUtils;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import static org.nuiton.i18n.I18n.n;

/**
 * Coser business application config.
 *
 * @author chatellier
 */
public class CoserBusinessConfig extends ApplicationConfig {

    public CoserBusinessConfig() {
        // init configuration with default options
        loadDefaultOptions(CoserBusinessOption.values());
    }

    /**
     * Overridden to not store value when equals to default value.
     */
    @Override
    public void setOption(String key, String value) {
        String defaultValue = line.getProperty(key);
        // replace ${xxx}
        defaultValue = replaceRecursiveOptions(defaultValue);
        if (defaultValue != null && defaultValue.equals(value)) {
            options.remove(key);
        } else {
            super.setOption(key, value);
        }
    }

    public void setDatabaseDirectory(String value) {
        setOption(CoserBusinessOption.DATABASE_DIRECTORY.key, value);
    }

    public File getDatabaseDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.DATABASE_DIRECTORY.key);
        return result;
    }

    public File getProjectsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.PROJECTS_DIRECTORY.key);
        return result;
    }

    public void setProjectsDirectory(String projectDirectory) {
        setOption(CoserBusinessOption.PROJECTS_DIRECTORY.key, projectDirectory);
    }

    public File getRSufiProjectsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.RSUFI_PROJECTS_DIRECTORY.key);
        return result;
    }

    public void setRSufiProjectsDirectory(String projectDirectory) {
        setOption(CoserBusinessOption.RSUFI_PROJECTS_DIRECTORY.key, projectDirectory);
    }

    public File getEchoBaseProjectsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.ECHOBASE_PROJECTS_DIRECTORY.key);
        return result;
    }

    public void setEchoBaseProjectsDirectory(String projectDirectory) {
        setOption(CoserBusinessOption.ECHOBASE_PROJECTS_DIRECTORY.key, projectDirectory);
    }

    public File getValidatorsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.VALIDATOR_DIRECTORY.key);
        return result;
    }

    public void setValidatorsDirectory(String validatorDirectory) {
        setOption(CoserBusinessOption.VALIDATOR_DIRECTORY.key, validatorDirectory);
    }

    public void setSmtpHost(String smtpHost) {
        setOption(CoserBusinessOption.SMTP_HOST.key, smtpHost);
    }

    public String getSmtpHost() {
        String result = getOption(CoserBusinessOption.SMTP_HOST.key);
        return result;
    }

    public Locale getLocale() {
        Locale result = getOption(Locale.class, CoserBusinessOption.LOCALE.key);
        return result;
    }

    public void setLocale(Locale locale) {
        setOption(CoserBusinessOption.LOCALE.key, locale.toString());
    }

    public String getReferenceSpeciesPath() {
        String result = getOption(CoserBusinessOption.REFERENCE_SPECIES.key);
        return result;
    }

    public void setReferenceSpeciesPath(String referenceSpeciesPath) {
        setOption(CoserBusinessOption.REFERENCE_SPECIES.key, referenceSpeciesPath);
    }

    public String getReferenceTypeEspecesPath() {
        String result = getOption(CoserBusinessOption.REFERENCE_TYPE_ESPECES.key);
        return result;
    }

    public void setReferenceTypeEspecesPath(String referenceTypeEspecesPath) {
        setOption(CoserBusinessOption.REFERENCE_TYPE_ESPECES.key, referenceTypeEspecesPath);
    }

    public double getControlNobsmin() {
        double result = getOptionAsDouble(CoserBusinessOption.CONTROL_NOBSMIN.key);
        return result;
    }

    public void setControlNobsmin(double controlNobsmin) {
        setOption(CoserBusinessOption.CONTROL_NOBSMIN.key, String.valueOf(controlNobsmin));
    }

    public double getControlDiffCatchLength() {
        double result = getOptionAsDouble(CoserBusinessOption.CONTROL_DIFF_CATCH_LENGTH.key);
        return result;
    }

    public void setControlDiffCatchLength(double controlDiffCatchLength) {
        setOption(CoserBusinessOption.CONTROL_DIFF_CATCH_LENGTH.key, String.valueOf(controlDiffCatchLength));
    }

    public String getControlTypeFish() {
        String result = getOption(CoserBusinessOption.CONTROL_TYPE_FISH.key);
        return result;
    }

    public void setControlTypeFish(String controlTypeFish) {
        setOption(CoserBusinessOption.CONTROL_TYPE_FISH.key, controlTypeFish);
    }

    public void setStandardDeviationToAverage(int deviationToAverage) {
        setOption(CoserBusinessOption.CONTROL_STANDARD_DEVIATION_TO_AVERAGE.key, String.valueOf(deviationToAverage));
    }

    public int getStandardDeviationToAverage() {
        return getOptionAsInt(CoserBusinessOption.CONTROL_STANDARD_DEVIATION_TO_AVERAGE.key);
    }

    public double getSelectionOccurrenceFilter() {
        double result = getOptionAsDouble(CoserBusinessOption.SELECTION_FILTER_OCCURRENCE.key);
        return result;
    }

    public void setSelectionOccurrenceFilter(double selectionOccurrenceFilter) {
        setOption(CoserBusinessOption.SELECTION_FILTER_OCCURRENCE.key, String.valueOf(selectionOccurrenceFilter));
    }

    public double getSelectionDensityFilter() {
        double result = getOptionAsDouble(CoserBusinessOption.SELECTION_FILTER_DENSITY.key);
        return result;
    }

    public void setSelectionDensityFilter(double selectionDensityFilter) {
        setOption(CoserBusinessOption.SELECTION_FILTER_DENSITY.key, String.valueOf(selectionDensityFilter));
    }

    public String getWebFrontEnd() {
        String result = getOption(CoserBusinessOption.WEB_FRONT_END.key);
        return result;
    }

    public void setWebFrontEnd(String webFrontEnd) {
        setOption(CoserBusinessOption.WEB_FRONT_END.key, webFrontEnd);
    }

    public String getWebUploadURL() {
        String result = getOption(CoserBusinessOption.WEB_UPLOAD_URL.key);
        return result;
    }

    public File getWebPropertiesFile() {
        File result = getOptionAsFile(CoserBusinessOption.WEB_PROPERTIES_FILE.key);
        return result;
    }

    public File getWebIndicatorsProjectsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.WEB_INDICATORS_PROJECTS_DIRECTORY.key);
        return result;
    }

    public File getWebMapsProjectsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.WEB_MAPS_PROJECTS_DIRECTORY.key);
        return result;
    }

    public File getWebEchobaseProjectsDirectory() {
        File result = getOptionAsFile(CoserBusinessOption.WEB_ECHOBASE_PROJECTS_DIRECTORY.key);
        return result;
    }

    public File getWebIndicatorsFile() {
        File result = getOptionAsFile(CoserBusinessOption.WEB_INDICATORS.key);
        return result;
    }

    public void setWebIndicatorsFile(String indicatorsFile) {
        setOption(CoserBusinessOption.WEB_INDICATORS.key, indicatorsFile);
    }

    public File getWebZonesFile() {
        File result = getOptionAsFile(CoserBusinessOption.WEB_ZONES.key);
        return result;
    }

    public void setWebZonesFile(String zoneFile) {
        setOption(CoserBusinessOption.WEB_ZONES.key, zoneFile);
    }

    public List<String> getNewResultNotificationList() {
        // la liste est vide si l'option est null
        List<String> emails = getOptionAsList(CoserBusinessOption.WEB_PUBLICATION_EMAIL.key).getOption();
        return emails;
    }

    /**
     * Retourne la date de dernière mise à jour des données du site web.
     *
     * Retourne une date bidon, si pas de dernière mise à jour.
     *
     * @return last data update date
     * @since 1.5
     */
    public Date getLastDataUpdateDate() {
        Date dataUpdateDate = null;
        File webProperties = getWebPropertiesFile();
        if (webProperties.isFile()) {
            // get update date
            Properties props = new Properties();
            InputStream stream = null;
            try {
                stream = new FileInputStream(webProperties);
                props.load(stream);

                if (props.containsKey("updateDate")) {
                    String date = props.getProperty("updateDate");
                    long time = Long.parseLong(date);
                    dataUpdateDate = new Date(time);
                }
                stream.close();
            } catch (IOException ex) {
                throw new CoserTechnicalException("Can't read properties file", ex);
            } finally {
                IOUtils.closeQuietly(stream);
            }
        }

        if (dataUpdateDate == null) {
            dataUpdateDate = new Date(0);
        }

        return dataUpdateDate;
    }

    /**
     * Met à jour certaines proprietes apres la mise à jour des données.
     *
     * @since 1.5
     */
    public void updateDataProperties() {

        File webProperties = getWebPropertiesFile();

        Properties props = new Properties();
        InputStream iStream = null;
        OutputStream oStream = null;
        try {
            if (webProperties.isFile()) {
                iStream = new FileInputStream(webProperties);
                props.load(iStream);
            }

            props.setProperty("updateDate", String.valueOf(new Date().getTime()));
            oStream = new FileOutputStream(webProperties);
            props.store(oStream, "Update data");
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't save properties file", ex);
        } finally {
            IOUtils.closeQuietly(iStream);
            IOUtils.closeQuietly(oStream);
        }
    }

    public enum CoserBusinessOption implements ConfigOptionDef {

        DATABASE_DIRECTORY("coser.database.directory", n("coser.config.database.directory.description"), File.class, "${user.home}" + File.separator + "coser"),
        PROJECTS_DIRECTORY("coser.projects.directory", n("coser.config.projects.directory.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "projects"),
        RSUFI_PROJECTS_DIRECTORY("coser.projects.rsufi.directory", n("coser.config.projects.rsufi.directory.description"), File.class, "${" + PROJECTS_DIRECTORY.key + "}" + File.separator + "rsufi"),
        ECHOBASE_PROJECTS_DIRECTORY("coser.projects.echobase.directory", n("coser.config.projects.echobase.directory.description"), File.class, "${" + PROJECTS_DIRECTORY.key + "}" + File.separator + "echobase"),
        VALIDATOR_DIRECTORY("coser.validator.directory", n("coser.config.validator.directory.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "validators"),

        SMTP_HOST("coser.smtp.host", n("coser.config.smtp.host.description"), String.class, "smtp"),
        LOCALE("coser.locale", n("coser.config.locale.description"), Locale.class, Locale.FRANCE.toString()),

        REFERENCE_SPECIES("coser.reference.species", n("coser.config.reference.species.description"), String.class, ""),
        REFERENCE_TYPE_ESPECES("coser.reference.typeSpecies", n("coser.config.reference.typeSpecies.description"), String.class, ""),

        CONTROL_NOBSMIN("coser.control.nobsmin", n("coser.config.control.nobsmin.description"), Float.class, "1.0"),
        CONTROL_DIFF_CATCH_LENGTH("coser.control.diffcatchlength", n("coser.config.control.diffcatchlength.description"), Float.class, "5.0"),
        CONTROL_TYPE_FISH("coser.control.typeFish", n("coser.config.control.typeFish.description"), String.class, "Pisces + Agnatha"),
        CONTROL_STANDARD_DEVIATION_TO_AVERAGE("coser.control.standarddeviationtoaverage", n("coser.config.control.standarddeviationtoaverage.description"), Integer.class, "3"),
        SELECTION_FILTER_OCCURRENCE("coser.selection.occurrenceFilter", n("coser.config.selection.occurrenceFilter.description"), Float.class, "5.0"),
        SELECTION_FILTER_DENSITY("coser.selection.densityFilter", n("coser.config.selection.densityFilter.description"), Float.class, "5.0"),

        /** Client side. */
        WEB_FRONT_END("coser.web.frontend", n("coser.config.web.frontend.description"), URL.class, "http://www.ifremer.fr/SIH-indices-campagnes"),
        WEB_UPLOAD_URL("coser.web.uploadurl", n("coser.config.web.uploadurl.description"), URL.class, "${" + WEB_FRONT_END.key + "}/upload-result.action"),

        /** Server side. */
        WEB_PROPERTIES_FILE("coser.web.properties.file", n("coser.config.web.properties.file.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "web.properties"),
        WEB_INDICATORS_PROJECTS_DIRECTORY("coser.web.indicators.projects.directory", n("coser.config.web.indicators.projects.directory.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "webindicatorsprojects"),
        WEB_MAPS_PROJECTS_DIRECTORY("coser.web.maps.projects.directory", n("coser.config.web.maps.projects.directory.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "webmapsprojects"),
        WEB_ECHOBASE_PROJECTS_DIRECTORY("coser.web.echobase.projects.directory", n("coser.config.web.echobase.projects.directory.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "webechobaseprojects"),
        WEB_INDICATORS("coser.web.indicators.file", n("coser.config.web.indicators.file.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "webindicators.csv"),
        WEB_ZONES("coser.web.zones.file", n("coser.config.web.zones.file.description"), File.class, "${" + DATABASE_DIRECTORY.key + "}" + File.separator + "webzones.csv"),
        WEB_PUBLICATION_EMAIL("coser.web.newresult.emails", n("coser.config.web.newresult.emails.description"), String.class, null);

        private final String key;

        private final String description;

        private final String defaultValue;

        private final Class<?> type;

        CoserBusinessOption(String key, String description, Class<?> type, String defaultValue) {
            this.key = key;
            this.description = description;
            this.type = type;
            this.defaultValue = defaultValue;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isTransient() {
            return false;
        }

        @Override
        public boolean isFinal() {
            return false;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            // not used
        }

        @Override
        public void setTransient(boolean isTransient) {
            // not used
        }

        @Override
        public void setFinal(boolean isFinal) {
            // not used
        }

        public String getDescription() {
            return description;
        }

        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }
}

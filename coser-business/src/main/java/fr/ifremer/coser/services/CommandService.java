/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.command.Command;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ListIterator;
import java.util.UUID;

/**
 * Command service.
 *
 * @author chatellier
 */
public class CommandService {

    private static final Log log = LogFactory.getLog(CommandService.class);

    protected CoserBusinessConfig config;

    public CommandService(CoserBusinessConfig config) {
        this.config = config;
    }

    /**
     * Generate new unique command UUID.
     *
     * @return unique command uuid
     */
    public String getUniqueCommandUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Perform command on project. Save action on project history file.
     *
     * @param command   command to perform
     * @param project   project
     * @param container container
     * @throws CoserBusinessException
     */
    public void doAction(Command command, Project project, AbstractDataContainer container) throws CoserBusinessException {
        if (log.isDebugEnabled()) {
            log.debug("Do action " + command);
        }

        // si la commande n'a pas de UUID
        if (command.getCommandUUID() == null) {
            command.setCommandUUID(getUniqueCommandUUID());
        }

        command.doCommand(project, container);
        container.addHistoryCommand(command);
    }

    /**
     * Undo last command on container.
     * Undo all operation with same uuid of first action to undo.
     *
     * Warning, this method doesn't fire historyCommands events.
     *
     * @param project   project
     * @param container container
     * @throws CoserBusinessException
     */
    public void undoAction(Project project, AbstractDataContainer container) throws CoserBusinessException {

        ListIterator<Command> itCommand = container.getHistoryCommands().listIterator(container.getHistoryCommands().size());

        String lastUUID = null;
        while (itCommand.hasPrevious()) {
            Command command = itCommand.previous();

            if (lastUUID != null && !lastUUID.equals(command.getCommandUUID())) {
                break;
            } else {
                command.undoCommand(project, container);
                lastUUID = command.getCommandUUID();
                itCommand.remove();
            }
        }
    }

    /**
     * Undo specified command count on container.
     *
     * @param project       project
     * @param container     container
     * @param commandsCount commands count to undo
     * @throws CoserBusinessException
     */
    public void undoAction(Project project, AbstractDataContainer container, int commandsCount) throws CoserBusinessException {
        for (int i = 0; i < commandsCount; i++) {
            Command command = container.getHistoryCommands().get(container.getHistoryCommands().size() - 1);
            command.undoCommand(project, container);
            container.removeHistoryCommand(command);
        }
    }
}

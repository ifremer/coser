/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.bean.SpeciesFieldType;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.command.DeleteLineCommand;
import fr.ifremer.coser.command.MergeSpeciesCommand;
import fr.ifremer.coser.command.ModifyFieldCommand;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.data.AbstractDataEntity;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Haul;
import fr.ifremer.coser.data.Length;
import fr.ifremer.coser.data.Strata;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.util.Coordinate;
import fr.ifremer.coser.util.ProgressMonitor;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.util.MathUtils;
import org.nuiton.math.matrix.DimensionHelper;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixProvider;

import java.beans.Introspector;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Service business method relative to project.
 *
 * Interessant à savoir, l'ordre pour traiter les fichiers correctement est:
 * {@code Strates > Traits > Captures > Tailles}
 *
 * @author chatellier
 */
public class ProjectService {

    private static final Log log = LogFactory.getLog(ProjectService.class);

    protected CoserBusinessConfig config;

    protected CommonService commonService;

    protected CommandService commandService;

    protected PublicationService publicationService;

    public ProjectService(CoserBusinessConfig config) {
        this.config = config;
        commonService = new CommonService(config);
        commandService = new CommandService(config);
        publicationService = new PublicationService(config);
    }

    /**
     * Return existing project name list sorted by project name.
     *
     * @return projects list
     */
    public List<String> getProjectNames() {
        File projectsDirectory = config.getRSufiProjectsDirectory();
        List<String> result = new ArrayList<String>();
        File[] projects = projectsDirectory.listFiles();
        if (projects != null) {
            for (File existingProject : projects) {
                if (existingProject.isDirectory()) {
                    String projectName = existingProject.getName();
                    result.add(projectName);
                }
            }
        }

        Collections.sort(result);

        return result;
    }

    /**
     * Create new project.
     *
     * Do (ordered):
     * - project existence test
     * - data loading
     * - data copying (after loading check)
     *
     * @param project            project to create
     * @param categoriesAndFiles additional files to load
     * @param maps               maps file
     * @param progress           progress monitor (can be null)
     * @return project with filled data
     * @throws CoserBusinessException if project can't be created
     */
    public Project createProject(Project project, Map<Category, File> categoriesAndFiles, List<File> maps, ProgressMonitor progress) throws CoserBusinessException {
        File projectsDirectory = config.getRSufiProjectsDirectory();
        if (!projectsDirectory.isDirectory()) {
            projectsDirectory.mkdirs();
        }

        // check project existence
        String projectName = project.getName();
        File projectDirectory = new File(projectsDirectory, projectName);
        if (projectDirectory.exists()) {
            throw new CoserBusinessException(t("Project %s already exist", project.getName()));
        }

        // set project date
        project.setCreationDate(new Date());

        // first free memory, clear all data
        project.clearData();

        // get number of total byte to read
        if (progress != null) {
            progress.setCurrent(0);
            int total = 0;
            for (Map.Entry<Category, File> categoryAndFile : categoriesAndFiles.entrySet()) {
                total += categoryAndFile.getValue().length();
            }
            progress.setTotal(total);
        }

        Control control = new Control();

        // load each files
        // loaded before any creation, to check data format
        // useless loading, just to check data
        for (Map.Entry<Category, File> categoryAndFile : categoriesAndFiles.entrySet()) {
            Category category = categoryAndFile.getKey();
            File dataFile = categoryAndFile.getValue();

            // test file existence
            if (!dataFile.exists()) {
                throw new CoserBusinessException(t("Can't read file %s for category %s", dataFile.getAbsolutePath(), t(category.getTranslationKey())));
            }

            DataStorage dataStorage = commonService.loadCSVFile(project, category, dataFile, progress, true);
            addProjectContent(project, control, category, dataStorage, false);

            // init empty deleted collection
            if (category.isDataCategory()) {
                // deleted data
                DataStorage dataStorage2 = commonService.getEmptyStorage(project, category);
                addProjectContent(project, control, category, dataStorage2, true);
            }
        }

        // create new project directory (after data checking, if ok)
        projectDirectory.mkdirs();

        // sauvegarde des fichiers (tracabilité)
        File originalDirectory = new File(projectDirectory, CoserConstants.STORAGE_ORIGINAL_DIRECTORY);
        originalDirectory.mkdirs();
        for (Map.Entry<Category, File> categoryAndFile : categoriesAndFiles.entrySet()) {
            Category category = categoryAndFile.getKey();
            File dataFile = categoryAndFile.getValue();
            try {

                // les fichiers de donnees sont stockes dans un repertoire
                // "original"
                if (category.isDataCategory()) {
                    String storageFileName = commonService.getDataStorageFileName(project, category, null);
                    File storageDataFile = new File(originalDirectory, storageFileName);
                    FileUtils.copyFile(dataFile, storageDataFile);

                    // les fichiers autres (reftax) sont stockes a la base
                } else {
                    File storageDataFile = new File(projectDirectory,
                                                    category.getStorageFileName());
                    FileUtils.copyFile(dataFile, storageDataFile);
                }
            } catch (IOException ex) {
                // clean creates directories
                try {
                    FileUtils.deleteDirectory(projectDirectory);
                } catch (IOException e) {
                    throw new CoserBusinessException(t("Can't create project"), ex);
                }
                throw new CoserBusinessException(t("Can't create project"), ex);
            }
        }

        saveProject(project, maps);

        // init additional structures (load empty control)
        project.setSelections(new HashMap<String, Selection>());
        control.setHistoryCommands(new ArrayList<Command>());
        project.setControl(control);

        // creation de la map de cache des assossiation entre les code especes
        // utilisé, et les noms réels visualisé par l'utilisateur
        LinkedHashMap<String, String> reftaxSpecies = getReftaxSpeciesDisplayFieldMap(project, false);
        project.setRefTaxSpeciesMap(reftaxSpecies);

        return project;
    }

    /**
     * Sauve seulement les informations concernant le projet et les cartes
     * si elle ont été modifiée par rapport à la liste des cartes actuellement
     * dans le projet.
     *
     * @param project project
     * @param maps    maps
     * @throws CoserBusinessException
     */
    public void saveProject(Project project, List<File> maps) throws CoserBusinessException {
        File projectsDirectory = config.getRSufiProjectsDirectory();
        String projectName = project.getName();
        File projectDirectory = new File(projectsDirectory, projectName);

        // manage maps BEFORE saving properties
        Project localProject = updateProjectMaps(project, projectDirectory, maps);

        // save project properties file
        File propertiesFile = new File(projectDirectory, "project.properties");
        Properties props = localProject.toProperties();
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(propertiesFile);
            props.store(outputStream, null);
            outputStream.close();

            if (log.isDebugEnabled()) {
                log.debug("Saving project properties file : " + propertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save project properties file", ex);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }

        // creation de la map de cache des assossiation entre les code especes
        // utilisé, et les noms réels visualisé par l'utilisateur
        LinkedHashMap<String, String> reftaxSpecies = getReftaxSpeciesDisplayFieldMap(project, false);
        project.setRefTaxSpeciesMap(reftaxSpecies);
    }

    /**
     * Update project maps with new maps.
     *
     * Théoriquement, si les deux listes sont identiques, cette methode ne fait rien.
     *
     * @param project          project to update maps
     * @param projectDirectory project directory
     * @param newMaps          new maps to set
     * @throws CoserBusinessException
     */
    protected Project updateProjectMaps(Project project, File projectDirectory, List<File> newMaps) throws CoserBusinessException {

        File mapsDirectory = new File(projectDirectory, CoserConstants.STORAGE_MAPS_DIRECTORY);

        List<File> currentMaps = project.getMaps();
        if (currentMaps == null) {
            currentMaps = new ArrayList<File>();
        }

        // bien faire attention aux instances de liste utilisées
        // le code n'est pas evident
        List<File> mapsList = new ArrayList<File>(currentMaps);

        try {
            // check delete maps from current list (not in new "newMaps")
            Collection<File> removedMaps = CollectionUtils.subtract(currentMaps, newMaps);
            for (File removedMap : removedMaps) {
                // not a problem if can't be delete really
                removedMap.delete();
                if (log.isDebugEnabled()) {
                    log.debug("Deleting map : " + removedMap);
                }
                mapsList.remove(removedMap);
            }

            // add new map
            Collection<File> addedMaps = CollectionUtils.subtract(newMaps, currentMaps);
            if (!addedMaps.isEmpty()) {
                mapsDirectory.mkdirs();
            }
            for (File addedMap : addedMaps) {
                File storageMap = new File(mapsDirectory, addedMap.getName());
                FileUtils.copyFile(addedMap, storageMap);
                mapsList.add(storageMap);
                if (log.isDebugEnabled()) {
                    log.debug("Adding map : " + storageMap);
                }
            }

            project.setMaps(mapsList);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't copy maps", ex);
        }

        return project;
    }

    /**
     * Open project without loading data.
     *
     * Just load non category data (reftax).
     *
     * @param projectName project name to open
     * @return loaded project with data
     * @throws CoserBusinessException
     */
    public Project openProject(String projectName) throws CoserBusinessException {
        return openProject(projectName, null);
    }

    /**
     * Open project without loading data.
     *
     * Just load non category data (reftax).
     *
     * @param projectName     project name to open
     * @param parentDirectory optional custom parent directory (for results)
     * @return loaded project with data
     * @throws CoserBusinessException
     */
    public Project openProject(String projectName, File parentDirectory) throws CoserBusinessException {

        // check project existence
        File projectsDirectory = parentDirectory;
        if (projectsDirectory == null) {
            projectsDirectory = config.getRSufiProjectsDirectory();
        }
        File projectDirectory = new File(projectsDirectory, projectName);
        if (!projectDirectory.isDirectory()) {
            throw new CoserBusinessException(t("Project %s doesn't exists !", projectName));
        }
        Project project = new Project(projectName);

        for (Category category : Category.values()) {
            // load only additional files
            if (!category.isDataCategory()) {
                File inputFile = new File(projectDirectory,
                                          category.getStorageFileName());

                if (inputFile.isFile()) {
                    DataStorage dataStorage = commonService.loadCSVFile(project, category, inputFile);
                    addProjectContent(project, null, category, dataStorage, false);
                } else {
                    // si on arrive ici et qu'un fichier de reference
                    // n'existe pas, c'est grave
                    throw new CoserBusinessException(t("Missing file %s", inputFile));
                }
            }
        }

        // load control (without data and commands)
        File controlDirectory = new File(projectDirectory, CoserConstants.STORAGE_CONTROL_DIRECTORY);
        File controlPropertiesFile = new File(controlDirectory, "control.properties");
        Control control = new Control();
        if (controlPropertiesFile.exists()) { // si on a vraiment pas sauve le control ...
            InputStream inputStream = null;
            try {
                Properties props = new Properties();
                inputStream = new FileInputStream(controlPropertiesFile);
                props.load(inputStream);
                control.fromProperties(props);
                inputStream.close();

                if (log.isDebugEnabled()) {
                    log.debug("Read control properties file : " + controlPropertiesFile);
                }
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't read control properties file", ex);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }
        project.setControl(control);

        // reload selection (without data and commands)
        Map<String, Selection> selections = new HashMap<String, Selection>();
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        if (selectionsDirectory.isDirectory()) {
            File[] selectionDirectories = selectionsDirectory.listFiles();
            for (File selectionDirectory : selectionDirectories) {
                if (selectionDirectory.isDirectory()) {
                    String selectionName = selectionDirectory.getName();
                    Selection selection = new Selection();
                    selection.setName(selectionName);

                    // relecture des informations de la selection (properties)
                    File selectionPropertiesFile = new File(selectionDirectory, selectionName + ".selection");
                    InputStream inputStream = null;
                    try {
                        Properties props = new Properties();
                        inputStream = new FileInputStream(selectionPropertiesFile);
                        props.load(inputStream);
                        selection.fromProperties(props);
                        inputStream.close();

                        if (log.isDebugEnabled()) {
                            log.debug("Read selection properties file : " + selectionPropertiesFile);
                        }
                    } catch (IOException ex) {
                        throw new CoserBusinessException("Can't read selection properties file", ex);
                    } finally {
                        IOUtils.closeQuietly(inputStream);
                    }

                    // other files dir
                    File othersDir = new File(selectionDirectory, CoserConstants.STORAGE_SELECTION_FILES);
                    List<File> otherFiles = new ArrayList<File>();
                    if (othersDir.isDirectory()) {
                        for (File file : othersDir.listFiles()) {
                            otherFiles.add(file);
                        }
                    }
                    selection.setOtherFiles(otherFiles);

                    List<RSufiResult> rsufiResults = loadRSufiResults(selectionDirectory);
                    selection.setRsufiResults(rsufiResults);

                    selections.put(selectionDirectory.getName(), selection);
                }
            }
        }
        project.setSelections(selections);

        // relecture des informations du projet (properties)
        File projectPropertiesFile = new File(projectDirectory, "project.properties");
        File mapsDirectory = new File(projectDirectory, CoserConstants.STORAGE_MAPS_DIRECTORY);
        InputStream inputStream = null;
        try {
            Properties props = new Properties();
            inputStream = new FileInputStream(projectPropertiesFile);
            props.load(inputStream);
            project.fromProperties(props, mapsDirectory);
            inputStream.close();

            if (log.isDebugEnabled()) {
                log.debug("Read project properties file : " + projectPropertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read project properties file", ex);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        // creation de la map de cache des assossiation entre les code especes
        // utilisé, et les noms réels visualisé par l'utilisateur
        LinkedHashMap<String, String> reftaxSpecies = getReftaxSpeciesDisplayFieldMap(project, false);
        project.setRefTaxSpeciesMap(reftaxSpecies);

        return project;
    }

    /**
     * Load rsufi result in specified directory.
     *
     * @param selectionDirectory selection directory
     * @return rsufi results list
     * @throws CoserBusinessException
     */
    protected List<RSufiResult> loadRSufiResults(File selectionDirectory) throws CoserBusinessException {

        List<RSufiResult> results = new ArrayList<RSufiResult>();
        File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
        File[] resultsDirectories = resultsDirectory.listFiles();

        if (resultsDirectories != null) {
            for (File resultDirectory : resultsDirectories) {
                if (resultDirectory.isDirectory()) {
                    RSufiResult rsufiResult = getRSufiResult(resultDirectory);
                    rsufiResult.setName(resultDirectory.getName());
                    results.add(rsufiResult);

                    // maps dir exists
                    File mapsDir = new File(resultDirectory, CoserConstants.STORAGE_MAPS_DIRECTORY);
                    rsufiResult.setMapsAvailable(mapsDir.isDirectory());

                    // other files dir
                    File othersDir = new File(resultDirectory, CoserConstants.STORAGE_RESULT_FILES);
                    List<File> otherFiles = new ArrayList<File>();
                    if (othersDir.isDirectory()) {
                        for (File file : othersDir.listFiles()) {
                            otherFiles.add(file);
                        }
                    }
                    rsufiResult.setOtherFiles(otherFiles);
                }
            }
        }

        return results;
    }

    /**
     * Retourne un object {@code RSufiResult} initialise avec les données
     * du resultat du répertoire demandé.
     *
     * @param resultDirectory result directory (base)
     * @return initialized RSufiResult
     * @throws CoserBusinessException
     */
    public RSufiResult getRSufiResult(File resultDirectory) throws CoserBusinessException {
        RSufiResult rsufiResult = new RSufiResult();
        rsufiResult.setName(resultDirectory.getName());

        // relecture des informations du resultat (properties)
        File resultPropertiesFile = new File(resultDirectory, "result.properties");
        InputStream inputStream = null;
        try {
            Properties props = new Properties();
            inputStream = new FileInputStream(resultPropertiesFile);
            props.load(inputStream);
            rsufiResult.fromProperties(props);
            inputStream.close();

            if (log.isDebugEnabled()) {
                log.debug("Read result properties file : " + resultPropertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read result properties file", ex);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return rsufiResult;
    }

    /**
     * Load control data in an initialized project.
     *
     * @param project project
     * @return project with data
     * @throws CoserBusinessException
     */
    public Project loadControlData(Project project) throws CoserBusinessException {

        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());

        // first free memory, clear all data
        project.clearData();

        Control control = project.getControl();

        // try to load validated data first
        File controlDirectory = new File(projectDirectory, CoserConstants.STORAGE_CONTROL_DIRECTORY);
        int fileLoaded = 0;
        for (Category category : Category.values()) {

            // seulement les category de données ici
            if (category.isDataCategory()) {
                String storageFileName = commonService.getDataStorageFileName(project, category, CoserConstants.STORAGE_CONTROL_SUFFIX);
                File inputFile = new File(controlDirectory, storageFileName);

                if (inputFile.isFile()) {
                    DataStorage dataStorage = commonService.loadCSVFile(project, category, inputFile);
                    addProjectContent(project, control, category, dataStorage, false);
                    fileLoaded++;
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Can't find file " + inputFile);
                    }
                }

                // load deleted file if exists
                storageFileName = commonService.getDataStorageFileName(project, category, CoserConstants.STORAGE_DELECTED_SUFFIX);
                inputFile = new File(controlDirectory, storageFileName);
                DataStorage dataStorage = null;
                if (inputFile.isFile()) {
                    dataStorage = commonService.loadCSVFile(project, category, inputFile);
                } else {
                    dataStorage = commonService.getEmptyStorage(project, category);
                    if (log.isDebugEnabled()) {
                        log.debug("Can't find file " + inputFile);
                    }
                }
                addProjectContent(project, control, category, dataStorage, true);
            }
        }

        // if not loaded reload original data
        if (fileLoaded == 0) {
            File originalDirectory = new File(projectDirectory, CoserConstants.STORAGE_ORIGINAL_DIRECTORY);
            for (Category category : Category.values()) {

                if (category.isDataCategory()) {
                    String storageFileName = commonService.getDataStorageFileName(project, category, null);
                    File storageDataFile = new File(originalDirectory, storageFileName);

                    // main data
                    if (storageDataFile.isFile()) {
                        DataStorage dataStorage = commonService.loadCSVFile(project, category, storageDataFile, null, true);
                        addProjectContent(project, control, category, dataStorage, false);
                    } else {
                        // si on arrive ici et qu'un fichier original
                        // n'existe pas, c'est grave
                        throw new CoserBusinessException(t("Missing file %s", storageDataFile));
                    }

                    // deleted data
                    DataStorage dataStorage = commonService.getEmptyStorage(project, category);
                    addProjectContent(project, control, category, dataStorage, true);
                }
            }
        }

        // load control commands
        File controlPropertiesFile = new File(controlDirectory, "control.properties");
        if (controlPropertiesFile.exists()) { // si on a vraiment pas sauve le control ...
            InputStream inputStream = null;
            try {
                Properties props = new Properties();
                inputStream = new FileInputStream(controlPropertiesFile);
                props.load(inputStream);
                inputStream.close();

                List<Command> commands = getHistoryCommandsFromProperties(props, "control.commands");
                control.setHistoryCommands(commands);

                if (log.isDebugEnabled()) {
                    log.debug("Read control properties file : " + controlPropertiesFile);
                }
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't read control properties file", ex);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        } else {
            // FIX future NPE
            control.setHistoryCommands(new ArrayList<Command>());
        }

        return project;
    }

    /**
     * Load selection data in an initialized project.
     *
     * @param project   project
     * @param selection selection to fill
     * @return project with data
     * @throws CoserBusinessException
     */
    public Project loadSelectionData(Project project, Selection selection) throws CoserBusinessException {
        return loadSelectionData(config.getRSufiProjectsDirectory(), project, selection);
    }

    /**
     * Load selection data in an initialized project form specific directory.
     *
     * @param projectsDirectory directory containing projects
     * @param project           project
     * @param selection         selection to fill
     * @return project with data
     * @throws CoserBusinessException
     */
    public Project loadSelectionData(File projectsDirectory, Project project, Selection selection) throws CoserBusinessException {

        File projectDirectory = new File(projectsDirectory, project.getName());

        // first free memory, clear all data
        project.clearData();

        // try to load validated data first
        String selectionName = selection.getName();
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File selectionDirectory = new File(selectionsDirectory, selectionName);
        for (Category category : Category.values()) {

            // seulement les category de données ici
            if (category.isDataCategory()) {
                String storageFileName = commonService.getDataStorageFileName(project, category, CoserConstants.STORAGE_SELECTION_SUFFIX);
                File inputFile = new File(selectionDirectory, storageFileName);

                if (inputFile.isFile()) {
                    DataStorage dataStorage = commonService.loadCSVFile(project, category, inputFile);
                    addProjectContent(project, selection, category, dataStorage, false);
                } else {
                    throw new CoserBusinessException(t("Missing file %s", inputFile));
                }
            }
        }

        // relecture des commandes de la selection (properties)
        File selectionPropertiesFile = new File(selectionDirectory, selectionName + ".selection");
        InputStream inputStream = null;
        try {
            Properties props = new Properties();
            inputStream = new FileInputStream(selectionPropertiesFile);
            props.load(inputStream);
            inputStream.close();

            List<Command> commands = getHistoryCommandsFromProperties(props, "selection.commands");
            selection.setHistoryCommands(commands);

            if (log.isDebugEnabled()) {
                log.debug("Read selection properties file : " + selectionPropertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read selection properties file", ex);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        return project;
    }

    /**
     * Enregistre les fichiers de données apres validation.
     *
     * @param project project to save
     * @throws CoserBusinessException
     */
    public void saveProjectControl(Project project) throws CoserBusinessException {

        // tout ce qui suit doit exister à ce stade
        File projectsDirectory = config.getRSufiProjectsDirectory();
        String projectName = project.getName();
        File projectDirectory = new File(projectsDirectory, projectName);

        Control control = project.getControl();

        // creation du dossier de control (il peut deja exister)
        File controlDirectory = new File(projectDirectory, CoserConstants.STORAGE_CONTROL_DIRECTORY);
        controlDirectory.mkdirs();
        for (Category category : Category.values()) {
            if (category.isDataCategory()) {

                // save main content
                String storageFileName = commonService.getDataStorageFileName(project, category, CoserConstants.STORAGE_CONTROL_SUFFIX);
                File controlFile = new File(controlDirectory, storageFileName);
                if (log.isDebugEnabled()) {
                    log.debug("Saving control file : " + controlFile);
                }
                DataStorage content = getProjectContent(project, control, category, false);
                commonService.storeData(content, controlFile);

                // save deleted content (if needed)
                DataStorage contentDeleted = getProjectContent(project, control, category, true);
                // if more content than header
                if (contentDeleted.size() > 1) {
                    String deletedFileName = commonService.getDataStorageFileName(project, category, CoserConstants.STORAGE_DELECTED_SUFFIX);
                    File deletedDataFile = new File(controlDirectory, deletedFileName);
                    commonService.storeData(contentDeleted, deletedDataFile);
                }
            }
        }

        // sauvegarde des informations du control (properties)
        File propertiesFile = new File(controlDirectory, "control.properties");
        Properties props = control.toProperties();
        addHistoryCommandsToProperties(props, project.getControl().getHistoryCommands(), "control.commands");
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(propertiesFile);
            props.store(outputStream, null);
            outputStream.close();

            if (log.isDebugEnabled()) {
                log.debug("Saving control properties file : " + propertiesFile);
            }

        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save control properties file", ex);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }

    }

    /**
     * Marque le controle comme valider et sauve le projet.
     *
     * Genère également les PDF des rapports de contrôle.
     *
     * @param project          project to save
     * @param validationErrors controls errors used to fill pdf
     * @throws CoserBusinessException
     * @since 1.3
     */
    public void validControl(Project project, List<ControlError> validationErrors) throws CoserBusinessException {

        Control control = project.getControl();
        control.setValidated(true);

        // sauvegarde
        saveProjectControl(project);

        // generate control file as pdf
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());
        File controlDirectory = new File(projectDirectory, CoserConstants.STORAGE_CONTROL_DIRECTORY);
        File controlReportPdf = new File(controlDirectory, "control.pdf");
        publicationService.extractControlLogAsPDF(project, control, validationErrors, controlReportPdf);
    }

    /**
     * Marque la selection comme validée et sauve le projet.
     *
     * Genère également les PDF des rapports de selection.
     *
     * @param project project to save
     * @throws CoserBusinessException
     * @since 1.3
     */
    public void validSelection(Project project, Selection selection) throws CoserBusinessException {

        selection.setValidated(true);

        // sauvegarde
        saveProjectSelection(project, selection);

        // generate control file as pdf
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File selectionDirectory = new File(selectionsDirectory, selection.getName());
        File selectionReportPdf = new File(selectionDirectory, "selection.pdf");
        publicationService.extractSelectionLogAsPDF(project, selection, selectionReportPdf);
    }

    /**
     * Creer une instance de selection "non sauvegardee" avec les données
     * de control (validée) du projet.
     *
     * @param project project to create selection
     * @return selection
     * @throws CoserBusinessException
     */
    public Selection initProjectSelection(Project project) throws CoserBusinessException {

        // le control doit être validé
        if (!project.getControl().isValidated()) {
            throw new CoserBusinessException(t("coser.business.selection.notValidatedControl"));
        }

        Project localProject = project;
        if (localProject.getControl() == null || !localProject.getControl().isDataLoaded()) {
            localProject = loadControlData(localProject);
        }

        Selection selection = new Selection();
        copyControlDataToSelection(localProject, selection);

        selection.setRsufiResults(new ArrayList<RSufiResult>());

        // init filter values with configuration
        selection.setDensityFilter(config.getSelectionDensityFilter());
        selection.setOccurrenceFilter(config.getSelectionOccurrenceFilter());

        List<String> allYears = getProjectYears(selection);
        selection.setAllYears(allYears);
        selection.setSelectedYears(new ArrayList<String>(allYears));

        return selection;
    }

    /**
     * Initialise une nouvelle selection à partir des données de controle
     * et de la definition de la selection du fichier fournit.
     *
     * @param project       project (containing data in control)
     * @param selectionFile selection file
     * @return initialized selection
     * @throws CoserBusinessException
     */
    public Selection initProjectSelectionFromFile(Project project, File selectionFile) throws CoserBusinessException {

        Selection selection = initProjectSelection(project);
        selection.setName(null); //clear name (can't be used)

        // read input file
        InputStream selectionStream = null;
        try {
            Properties props = new Properties();
            selectionStream = new FileInputStream(selectionFile);
            props.load(selectionStream);
            selection.fromProperties(props);
            selectionStream.close();
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read selection properties file", ex);
        } finally {
            IOUtils.closeQuietly(selectionStream);
        }

        // filter years
        filterDataYearsAndGetStrata(project, selection, selection.getSelectedYears());
        // applying merges commands
        List<Command> commands = new ArrayList<Command>(selection.getHistoryCommands());
        selection.clearHistoryCommands();
        for (Command command : commands) {
            commandService.doAction(command, project, selection);
        }

        return selection;
    }

    /**
     * Copy loaded control data to new created selection.
     *
     * @return selection initialized with
     */
    protected Selection copyControlDataToSelection(Project project, Selection selection) {
        // create new selection
        Control control = project.getControl();
        DataStorage dataCatch = control.getCatch();
        DataStorage dataHaul = control.getHaul();
        DataStorage dataLength = control.getLength();
        DataStorage dataStrata = control.getStrata();

        project.clearData();

        selection.setCatch(dataCatch);
        selection.setHaul(dataHaul);
        selection.setLength(dataLength);
        selection.setStrata(dataStrata);

        // data reloaded from control, so modification are empty
        selection.setHistoryCommands(new ArrayList<Command>());

        return selection;
    }

    /**
     * Create and save project selection.
     *
     * @param project   project to create selection
     * @param selection selection to create
     * @throws CoserBusinessException
     */
    public void createProjectSelection(Project project, Selection selection) throws CoserBusinessException {

        // tout ce qui suit doit exister à ce stade
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());

        // creation du dossier de selections (peut deja exister)
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);

        // creation du dossier de la selection courante
        File selectionDirectory = new File(selectionsDirectory, selection.getName());

        if (selectionDirectory.isDirectory()) {
            throw new CoserBusinessException(t("Selection %s already exists", selection.getName()));
        } else {
            selectionDirectory.mkdirs();
            saveProjectSelection(project, selection);
            project.addSelections(selection);
        }
    }

    /**
     * Save project selection.
     *
     * @param project   project to save selection
     * @param selection selection to save
     * @throws CoserBusinessException
     */
    public void saveProjectSelection(Project project, Selection selection) throws CoserBusinessException {

        // tout ce qui suit doit exister à ce stade
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());

        // creation du dossier de selections (peut deja exister)
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);

        // creation du dossier de la selection courante
        String selectionName = selection.getName();
        File selectionDirectory = new File(selectionsDirectory, selectionName);

        for (Category category : Category.values()) {
            if (category.isDataCategory()) {
                String storageFileName = commonService.getDataStorageFileName(project, category, CoserConstants.STORAGE_SELECTION_SUFFIX);
                File dataFile = new File(selectionDirectory, storageFileName);
                if (log.isDebugEnabled()) {
                    log.debug("Saving selection file : " + dataFile);
                }

                DataStorage content = getProjectContent(project, selection, category, false);
                commonService.storeData(content, dataFile);

                // delete data are not saved here
                // can't delete data in selection
            }
        }

        // sauvegarde des informations de la selection (properties)
        File propertiesFile = new File(selectionDirectory, selectionName + ".selection");
        Properties props = selection.toProperties();
        addHistoryCommandsToProperties(props, selection.getHistoryCommands(), "selection.commands");
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(propertiesFile);
            props.store(outputStream, null);
            outputStream.close();

            if (log.isDebugEnabled()) {
                log.debug("Saving selection properties file : " + propertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save selection properties file", ex);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }

        // fichier en plus (demande client)
        // quand la selection est validé (le fichier list espece (4 listes)
        if (selection.isValidated()) {
            File listSpeciesFile = new File(selectionDirectory, "ListEspeces" + project.getName() + ".txt");
            fillListSpeciesFile(selection, listSpeciesFile);
        }
    }

    /**
     * Create new rsufi result list in directory structure.
     *
     * RSufiResult is added to selection result list by this method.
     *
     * @param project     project
     * @param selection   selection
     * @param rsufiResult new result to save
     * @param othersFiles others files and directory
     * @throws CoserBusinessException
     */
    public void saveRsufiResults(Project project, Selection selection,
                                 RSufiResult rsufiResult, List<File> othersFiles) throws CoserBusinessException {

        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File selectionDirectory = new File(selectionsDirectory, selection.getName());
        File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
        File rsufiResultDirectory = new File(resultsDirectory, rsufiResult.getName());

        // save it
        if (rsufiResultDirectory.exists()) {
            throw new CoserBusinessException(t("coser.business.result.rsufiResultAlreadyExists", rsufiResult.getName()));
        } else {
            rsufiResultDirectory.mkdirs();

            // ensure creation date (modifiable par l'ui)
            if (rsufiResult.getCreationDate() == null) {
                rsufiResult.setCreationDate(new Date());
            }

            try {
                // sauvegarde des 2 fichiers obligatoires
                File estComIndFile = new File(rsufiResult.getEstComIndPath());
                File estPopIndFile = new File(rsufiResult.getEstPopIndPath());
                rsufiResult.setEstComIndName(estComIndFile.getName());
                rsufiResult.setEstPopIndName(estPopIndFile.getName());
                FileUtils.copyFileToDirectory(estComIndFile, rsufiResultDirectory);
                FileUtils.copyFileToDirectory(estPopIndFile, rsufiResultDirectory);
                File resultMapsDirectory = new File(rsufiResultDirectory, CoserConstants.STORAGE_MAPS_DIRECTORY);

                // maps path is optionnal
                if (StringUtils.isNotEmpty(rsufiResult.getMapsPath())) {
                    File mapsDirectory = new File(rsufiResult.getMapsPath());
                    FileUtils.copyDirectory(mapsDirectory, resultMapsDirectory);
                }

                // sauvegarde des fichiers autre
                File otherFilesDirectory = new File(rsufiResultDirectory, CoserConstants.STORAGE_RESULT_FILES);
                for (File othersFile : othersFiles) {
                    if (othersFile.isDirectory()) {
                        FileUtils.copyDirectoryToDirectory(othersFile, otherFilesDirectory);
                    } else {
                        FileUtils.copyFileToDirectory(othersFile, otherFilesDirectory);
                    }
                }

                // property file
                saveRSufiResult(rsufiResultDirectory, rsufiResult);

            } catch (IOException ex) {
                throw new CoserBusinessException("Can't save result properties file", ex);
            }

            // maps dir exists
            File mapsDir = new File(rsufiResultDirectory, CoserConstants.STORAGE_MAPS_DIRECTORY);
            rsufiResult.setMapsAvailable(mapsDir.isDirectory());

            // update other files list
            File othersDir = new File(rsufiResultDirectory, CoserConstants.STORAGE_RESULT_FILES);
            List<File> otherFiles = new ArrayList<File>();
            if (othersDir.isDirectory()) {
                for (File file : othersDir.listFiles()) {
                    otherFiles.add(file);
                }
            }
            rsufiResult.setOtherFiles(otherFiles);

            List<RSufiResult> results = selection.getRsufiResults();
            results.add(rsufiResult);
            // this way to fire change event (do not remove)
            selection.setRsufiResults(results);
        }
    }

    /**
     * Save existing rsufi result.
     *
     * @param project     project
     * @param selection   selection
     * @param rsufiResult new result to save
     * @throws CoserBusinessException
     */
    public void editRsufiResults(Project project, Selection selection, RSufiResult rsufiResult, List<File> othersFile) throws CoserBusinessException {

        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File selectionDirectory = new File(selectionsDirectory, selection.getName());
        File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
        File rsufiResultDirectory = new File(resultsDirectory, rsufiResult.getName());
        File otherFilesDirectory = new File(rsufiResultDirectory, CoserConstants.STORAGE_RESULT_FILES);

        // property file
        try {
            saveRSufiResult(rsufiResultDirectory, rsufiResult);

            // remove some files
            Collection<File> toDeletes = CollectionUtils.subtract(rsufiResult.getOtherFiles(), othersFile);
            for (File toDelete : toDeletes) {
                FileUtils.deleteQuietly(toDelete);
            }
            // copy new file
            for (File otherFile : othersFile) {
                // non modified result, skipping it
                if (!otherFile.getAbsolutePath().startsWith(otherFilesDirectory.getAbsolutePath())) {
                    if (otherFile.isDirectory()) {
                        FileUtils.copyDirectoryToDirectory(otherFile, otherFilesDirectory);
                    } else {
                        FileUtils.copyFileToDirectory(otherFile, otherFilesDirectory);
                    }
                }
            }
            // update other files list
            List<File> otherFiles = new ArrayList<File>();
            if (otherFilesDirectory.isDirectory()) {
                for (File file : otherFilesDirectory.listFiles()) {
                    otherFiles.add(file);
                }
            }
            rsufiResult.setOtherFiles(otherFiles);

            List<RSufiResult> results = selection.getRsufiResults();
            // this way to fire change event (do not remove)
            selection.setRsufiResults(results);

        } catch (IOException ex) {
            throw new CoserBusinessException("Can't edit result", ex);
        }
    }

    /**
     * Save rsufi result (only properties file).
     *
     * @param rsufiResultDirectory rsufiresult directory
     * @param rsufiResult          rsufi result
     * @throws CoserBusinessException
     */
    public void saveRSufiResult(File rsufiResultDirectory, RSufiResult rsufiResult) throws CoserBusinessException {

        OutputStream outputStream = null;
        try {
            // sauvegarde des informations du resultat (properties)
            File propertiesFile = new File(rsufiResultDirectory, "result.properties");
            Properties props = rsufiResult.toProperties();
            outputStream = new FileOutputStream(propertiesFile);
            props.store(outputStream, null);
            outputStream.close();
            if (log.isDebugEnabled()) {
                log.debug("Saving result properties file : " + propertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save result", ex);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }

    /**
     * Delete a rsufi result.
     *
     * @param project     project
     * @param selection   selection
     * @param rsufiResult rsufi result to delete
     * @throws CoserBusinessException
     * @since 1.3
     */
    public void deleteRSufiResult(Project project, Selection selection, RSufiResult rsufiResult) throws CoserBusinessException {

        try {
            File projectsDirectory = config.getRSufiProjectsDirectory();
            File projectDirectory = new File(projectsDirectory, project.getName());
            File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
            File selectionDirectory = new File(selectionsDirectory, selection.getName());
            File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
            File rsufiResultDirectory = new File(resultsDirectory, rsufiResult.getName());

            FileUtils.deleteDirectory(rsufiResultDirectory);

            List<RSufiResult> results = selection.getRsufiResults();
            results.remove(rsufiResult);
            // this way to fire change event (do not remove)
            selection.setRsufiResults(results);

            // s'il n'y a plus de résultat, suppprimer le dossier
            // result
            File[] resultFiles = resultsDirectory.listFiles();
            if (ArrayUtils.isEmpty(resultFiles)) {
                resultsDirectory.delete();
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't delete directory", ex);
        }
    }

    /**
     * Sauve une liste ordonnées de commande dans le fichier specifié.
     *
     * @param props          proparties to add command to
     * @param historyCommand command list to save
     * @param propertyPrefix property prefix
     * @throws CoserBusinessException
     */
    protected void addHistoryCommandsToProperties(Properties props, List<Command> historyCommand,
                                                  String propertyPrefix) throws CoserBusinessException {

        int commandIndex = 0;
        for (Command command : historyCommand) {
            String commandProperty = propertyPrefix + "." + commandIndex;
            String commandValue = convertCommandToLine(command);
            props.setProperty(commandProperty, commandValue);
            commandIndex++;
        }
    }

    /**
     * Load history command list from file.
     *
     * @param props          properties to parse
     * @param propertyPrefix property prefix
     * @return command list (never null)
     * @throws CoserBusinessException
     */
    protected List<Command> getHistoryCommandsFromProperties(Properties props, final String propertyPrefix) throws CoserBusinessException {
        List<Command> historyCommand = new ArrayList<Command>();

        // take care of property name order
        List<String> propertiesNames = new ArrayList<String>(props.stringPropertyNames());
        Collections.sort(propertiesNames, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                int result = -1;
                if (o1.matches(Pattern.quote(propertyPrefix + ".") + "\\d+")) {
                    if (o2.matches(Pattern.quote(propertyPrefix + ".") + "\\d+")) {
                        int int1 = Integer.parseInt(o1.substring(propertyPrefix.length() + 1));
                        int int2 = Integer.parseInt(o2.substring(propertyPrefix.length() + 1));
                        result = int1 - int2;
                    } else {
                        result = o1.compareTo(o2);
                    }
                } else {
                    result = o1.compareTo(o2);
                }
                return result;
            }

        });

        for (String propertyName : propertiesNames) {

            if (propertyName.startsWith(propertyPrefix + ".")) {
                String commandAsString = props.getProperty(propertyName);
                Command command = convertLineToCommand(commandAsString);
                historyCommand.add(command);
            }
        }

        return historyCommand;
    }

    /**
     * Convert a command to string info
     *
     * @param command command to convert
     * @return string command
     * @throws CoserBusinessException
     */
    protected String convertCommandToLine(Command command) {

        List<String> args = new ArrayList<String>();
        args.add(command.getCommandUUID());
        args.add(command.getClass().getSimpleName());
        args.add(command.getComment() == null ? "" : command.getComment());
        args.add(command.toStringRepresentation());
        String result = CoserUtils.convertBracketString(args);

        return result;
    }

    /**
     * Convert a string line to parameterized command.
     *
     * @param line line to convert
     * @return command
     * @throws CoserBusinessException
     */
    protected Command convertLineToCommand(String line) throws CoserBusinessException {

        Command result = null;

        List<String> lineDetail = CoserUtils.convertBracketToList(line);
        String commandUUID = lineDetail.get(0);
        String commandName = lineDetail.get(1);
        String commandComment = lineDetail.get(2);
        String commandArgs = lineDetail.get(3);

        // instancie dynamiquement la command
        // en la charchant dans le meme package que la class "Command"
        String commandFQN = Command.class.getPackage().getName() + "." + commandName;
        if (log.isDebugEnabled()) {
            log.debug("Make new instance of command : " + commandFQN);
        }

        try {
            result = (Command) Class.forName(commandFQN).newInstance();
            result.setCommandUUID(commandUUID);
            result.setComment(commandComment);
            result.fromStringRepresentation(commandArgs);
        } catch (Exception ex) {
            throw new CoserBusinessException("Can't init command intance", ex);
        }

        return result;
    }

    /**
     * Set content into project depending on category type.
     *
     * @param project        project
     * @param category       category
     * @param content        content to set
     * @param deletedContent if content means deleted objects for {@code category}
     */
    protected void addProjectContent(Project project, AbstractDataContainer container,
                                     Category category, DataStorage content, boolean deletedContent) {

        switch (category) {
            case CATCH:
                if (!deletedContent) {
                    container.setCatch(content);
                } else {
                    container.setDeletedCatch(content);
                }
                break;
            case HAUL:
                if (!deletedContent) {
                    container.setHaul(content);
                } else {
                    container.setDeletedHaul(content);
                }
                break;
            case LENGTH:
                if (!deletedContent) {
                    container.setLength(content);
                } else {
                    container.setDeletedLength(content);
                }
                break;
            case STRATA:
                if (!deletedContent) {
                    container.setStrata(content);
                } else {
                    container.setDeletedStrata(content);
                }
                break;
            case REFTAX_SPECIES:
                project.setRefTaxSpecies(content);
                break;
            case TYPE_ESPECES:
                project.setTypeEspeces(content);
                break;
        }
    }

    /**
     * Set content into project depending on category type.
     *
     * @param project        project
     * @param container      data container
     * @param category       category
     * @param deletedContent if content means deleted objects for {@code category}
     */
    protected DataStorage getProjectContent(Project project, AbstractDataContainer container,
                                            Category category, boolean deletedContent) {

        DataStorage content = null;

        switch (category) {
            case CATCH:
                if (!deletedContent) {
                    content = container.getCatch();
                } else {
                    content = container.getDeletedCatch();
                }
                break;
            case HAUL:
                if (!deletedContent) {
                    content = container.getHaul();
                } else {
                    content = container.getDeletedHaul();
                }
                break;
            case LENGTH:
                if (!deletedContent) {
                    content = container.getLength();
                } else {
                    content = container.getDeletedLength();
                }
                break;
            case STRATA:
                if (!deletedContent) {
                    content = container.getStrata();
                } else {
                    content = container.getDeletedStrata();
                }
                break;
        }

        return content;
    }

    /**
     * Supprime une données via son index.
     *
     * Used in control ui.
     *
     * @param project     project
     * @param control     control
     * @param category    category
     * @param index       index to delete
     * @param commandUUID command UUID
     * @throws CoserBusinessException
     */
    public void deleteData(Project project, Control control, Category category, String index, String commandUUID) throws CoserBusinessException {

        // create new delete action
        DeleteLineCommand command = new DeleteLineCommand();
        command.setCategory(category);
        command.setLineNumber(index);
        command.setCommandUUID(commandUUID);
        commandService.doAction(command, project, control);
    }

    /**
     * Supprime une données via son index.
     *
     * Used in control ui.
     *
     * @param project  project
     * @param control  control
     * @param category category
     * @param index    index to delete
     * @throws CoserBusinessException
     */
    public void deleteData(Project project, Control control, Category category, String index) throws CoserBusinessException {
        deleteData(project, control, category, index, null);
    }

    /**
     * Replace une valeur d'un champs si nécéssaire.
     *
     * If {@code currentValue} is not equals to current data value for fieldName,
     * no replacement is performed.
     *
     * @param project       project
     * @param category      category
     * @param headerName    headerName
     * @param searchString  current value
     * @param replaceString new value
     * @param data          current data
     * @param isRegex       is regex
     * @param commandUUID   commandUUID
     * @return modification flag
     * @throws CoserBusinessException
     */
    public boolean replaceFieldValue(Project project, Category category, String headerName,
                                     String searchString, String replaceString, String[] data, boolean isRegex, String commandUUID) throws CoserBusinessException {

        boolean result = false;

        AbstractDataEntity beanData = null;
        switch (category) {
            case CATCH:
                beanData = new Catch();
                break;
            case HAUL:
                beanData = new Haul();
                break;
            case LENGTH:
                beanData = new Length();
                break;
            case STRATA:
                beanData = new Strata();
                break;
        }
        beanData.setData(data);

        // get current data value
        try {
            String fieldName = Introspector.decapitalize(headerName);
            String stringFieldProperty = fieldName + "AsString";
            String currentValue = (String) PropertyUtils.getProperty(beanData, stringFieldProperty);

            String escapeSearchString = searchString;
            if (!isRegex) {
                escapeSearchString = Pattern.quote(escapeSearchString);
            }
            String newValue = currentValue.replaceAll(escapeSearchString, replaceString);

            // si le remplacement a fonctionné, une command doit être créée
            if (!currentValue.equals(newValue)) {
                String lineNumber = data[AbstractDataEntity.INDEX_LINE];
                ModifyFieldCommand command = new ModifyFieldCommand();
                command.setCommandUUID(commandUUID);
                command.setFieldName(headerName);
                command.setLineNumber(lineNumber);
                command.setCurrentValue(currentValue);
                command.setNewValue(newValue);
                command.setCategory(category);

                commandService.doAction(command, project, project.getControl());

                result = true;
            }
        } catch (Exception ex) {
            throw new CoserBusinessException("Can't replace inner field value", ex);
        }

        return result;
    }

    /**
     * Replace all bean data. Compare current and new value, and build command
     * for each diff.
     *
     * @param project  project
     * @param control  control
     * @param category category
     * @param data     new data to set
     * @throws CoserBusinessException
     */
    public void replaceData(Project project, Control control, Category category, String[] data) throws CoserBusinessException {

        // build bean for current and new data
        String lineNumber = data[AbstractDataEntity.INDEX_LINE];
        int lineIndex = -1;
        AbstractDataEntity currentBeanData = null;
        AbstractDataEntity newBeanData = null;
        switch (category) {
            case CATCH:
                currentBeanData = new Catch();
                newBeanData = new Catch();
                lineIndex = control.getCatch().indexOf(lineNumber);
                currentBeanData.setData(control.getCatch().get(lineIndex));
                break;
            case HAUL:
                currentBeanData = new Haul();
                newBeanData = new Haul();
                lineIndex = control.getHaul().indexOf(lineNumber);
                currentBeanData.setData(control.getHaul().get(lineIndex));
                break;
            case LENGTH:
                currentBeanData = new Length();
                newBeanData = new Length();
                lineIndex = control.getLength().indexOf(lineNumber);
                currentBeanData.setData(control.getLength().get(lineIndex));
                break;
            case STRATA:
                currentBeanData = new Strata();
                newBeanData = new Strata();
                lineIndex = control.getStrata().indexOf(lineNumber);
                currentBeanData.setData(control.getStrata().get(lineIndex));
                break;
        }
        newBeanData.setData(data);

        String commandUUID = commandService.getUniqueCommandUUID();

        // inspect beans
        for (String propertyHeader : currentBeanData.getHeaders()) {
            String propertyName = Introspector.decapitalize(propertyHeader);
            String stringFieldProperty = propertyName + "AsString";
            try {
                String currentValue = (String) PropertyUtils.getProperty(currentBeanData, stringFieldProperty);
                String newValue = (String) PropertyUtils.getProperty(newBeanData, stringFieldProperty);

                if (!currentValue.equals(newValue)) {
                    ModifyFieldCommand command = new ModifyFieldCommand();
                    command.setCommandUUID(commandUUID);
                    command.setFieldName(propertyHeader);
                    command.setLineNumber(lineNumber);
                    command.setCurrentValue(currentValue);
                    command.setNewValue(newValue);
                    command.setCategory(category);

                    commandService.doAction(command, project, control);
                }
            } catch (Exception ex) {
                throw new CoserBusinessException("Can't get bean value", ex);
            }
        }
    }

    /**
     * Recharge les données de control et les copies dans la selection.
     *
     * Utilisé lors d'un rechargement utilisateur, ou lors de la mise à jour
     * des années.
     *
     * @param project   project
     * @param selection selection to copy data to
     * @return project
     * @throws CoserBusinessException
     */
    public Project loadControlDataToSelection(Project project, Selection selection) throws CoserBusinessException {
        Project newProject = loadControlData(project);
        copyControlDataToSelection(newProject, selection);
        return newProject;
    }

    /**
     * Find all defined year in model.
     *
     * Le parcours est fait sur le fichier "haul", c'est potentiellement
     * le plus petit.
     *
     * Used in selection ui.
     *
     * @param selection selection to search into
     * @return year list
     */
    public List<String> getProjectYears(Selection selection) {

        SortedSet<String> years = new TreeSet<String>();

        Iterator<String[]> itTuple = selection.getHaul().iterator(true);
        while (itTuple.hasNext()) {
            String[] tuple = itTuple.next();
            String year = tuple[Haul.INDEX_YEAR];
            years.add(year);
        }

        List<String> result = new ArrayList<String>(years);
        return result;
    }

    /**
     * Filter data on selected years and return resulting strata name sorted
     * by names.
     *
     * This methods set {@link Selection#setSelectedYears(List)}, and use
     * it to known if control data need to be reloaded (don't set it yourself).
     *
     * Used in selection ui.
     *
     * @param project       project
     * @param selection     selection
     * @param selectedYears selected years
     * @return zones
     * @throws CoserBusinessException
     */
    public List<String> filterDataYearsAndGetStrata(Project project, Selection selection, List<String> selectedYears) throws CoserBusinessException {

        List<String> currentSelectionYears = selection.getSelectedYears();
        if (currentSelectionYears == null || !currentSelectionYears.containsAll(selectedYears)) {
            // la liste des nouvelles année est plus grande que la liste
            // actuelle des données de la selection
            // les données doivent être rechargée
            project = loadControlDataToSelection(project, selection);
        }

        List<String> result = new ArrayList<String>();

        // manage haul
        Iterator<String[]> itHaul = selection.getHaul().iterator(true);
        while (itHaul.hasNext()) {
            String[] tupleHaul = itHaul.next();
            String year = tupleHaul[Haul.INDEX_YEAR];

            if (!selectedYears.contains(year)) {
                itHaul.remove();
            } else {
                String strates = tupleHaul[Haul.INDEX_STRATUM];
                if (!result.contains(strates)) {
                    result.add(strates);
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Haul data filtered by " + selectedYears);
        }

        // manage catch
        Iterator<String[]> itCatch = selection.getCatch().iterator(true);
        while (itCatch.hasNext()) {
            String[] tupleCatch = itCatch.next();
            String year = tupleCatch[Catch.INDEX_YEAR];

            if (!selectedYears.contains(year)) {
                itCatch.remove();
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Catch data filtered by " + selectedYears);
        }

        // manage length
        Iterator<String[]> itLength = selection.getLength().iterator(true);
        while (itLength.hasNext()) {
            String[] tupleLength = itLength.next();
            String year = tupleLength[Length.INDEX_YEAR];

            if (!selectedYears.contains(year)) {
                itLength.remove();
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Length data filtered by " + selectedYears);
        }

        // the only place to set this value
        selection.setSelectedYears(selectedYears);

        // sort strata by names
        Collections.sort(result);

        return result;
    }

    /**
     * Update data to remove non selected strata.
     *
     * Attention, les identifiants des traits sont année/traits
     * Le traits tout seul serait dupliquée.
     *
     * @param project        project
     * @param selection      selection
     * @param selectedStrata selected strata
     */
    public void filterDataStrata(Project project, Selection selection, List<String> selectedStrata) {

        Collection<String> existentYearsHauls = new HashSet<String>();

        // manage strata file
        Iterator<String[]> itStrata = selection.getStrata().iterator(true);
        while (itStrata.hasNext()) {
            String[] tupleStrata = itStrata.next();
            String stratum = tupleStrata[Strata.INDEX_STRATUM];

            if (!selectedStrata.contains(stratum)) {
                itStrata.remove();
            }
        }

        // manage haul file
        Iterator<String[]> itHaul = selection.getHaul().iterator(true);
        while (itHaul.hasNext()) {
            String[] tupleHaul = itHaul.next();
            String stratum = tupleHaul[Haul.INDEX_STRATUM];

            if (!selectedStrata.contains(stratum)) {
                itHaul.remove();
            } else {
                // sauvegarde les traits qui existent encore
                // apres la suppression des strates
                String year = tupleHaul[Haul.INDEX_YEAR];
                String haul = tupleHaul[Haul.INDEX_HAUL];
                existentYearsHauls.add(year + ";" + haul);
            }
        }

        // manage catch file
        Iterator<String[]> itCatch = selection.getCatch().iterator(true);
        while (itCatch.hasNext()) {
            String[] tupleCatch = itCatch.next();
            String year = tupleCatch[Catch.INDEX_YEAR];
            String haul = tupleCatch[Catch.INDEX_HAUL];

            if (!existentYearsHauls.contains(year + ";" + haul)) {
                itCatch.remove();
            }
        }

        // manage length file
        Iterator<String[]> itLength = selection.getLength().iterator(true);
        while (itLength.hasNext()) {
            String[] tupleLength = itLength.next();
            String year = tupleLength[Length.INDEX_YEAR];
            String haul = tupleLength[Length.INDEX_HAUL];

            if (!existentYearsHauls.contains(year + ";" + haul)) {
                itLength.remove();
            }
        }

        // only place to do it
        selection.setSelectedStrata(selectedStrata);
    }

    /**
     * Retourne la liste des type d'especes definie dans le projet sous forme
     * de map avec leur commentaire (sauf "Tous").
     *
     * @param project project to search into
     * @return project species type
     */
    public Map<String, String> getProjectSpeciesTypes(Project project) {
        SortedMap<String, String> types = new TreeMap<String, String>();

        // "Types","Commentaire","NumSys min","NumSys max","Code"

        Iterator<String[]> itTuples = project.getTypeEspeces().iterator(true);
        while (itTuples.hasNext()) {
            String[] tuple = itTuples.next();

            String type = tuple[0];
            String comment = tuple[1];
            String numSysMin = tuple[2];
            String numSysMax = tuple[3];

            // pour le cas "Tous", min est max sont a 0, donc pas filtrable
            if (!numSysMin.equals(numSysMax)) {
                types.put(type, comment);
            }
        }

        return types;
    }

    /**
     * Update data to remove non selected species.
     *
     * @param project         project
     * @param selection       selection
     * @param selectedSpecies selected species
     */
    public void filterDataSpecies(Project project, Selection selection, List<String> selectedSpecies) {

        // manage catch file
        Iterator<String[]> itCatch = selection.getCatch().iterator(true);
        while (itCatch.hasNext()) {
            String[] tupleCatch = itCatch.next();
            String species = tupleCatch[Catch.INDEX_SPECIES];

            if (!selectedSpecies.contains(species)) {
                itCatch.remove();
            }
        }

        // manage length file
        Iterator<String[]> itLength = selection.getLength().iterator(true);
        while (itLength.hasNext()) {
            String[] tupleLength = itLength.next();
            String species = tupleLength[Length.INDEX_SPECIES];

            if (!selectedSpecies.contains(species)) {
                itLength.remove();
            }
        }

        // only place to do it
        selection.setSelectedSpecies(selectedSpecies);
    }

    /**
     * Touve toutes les especes qui ont des tailles pour toutes les années.
     *
     * @param selection selection to search into
     * @return year list
     */
    public Collection<String> getSpeciesWithSizeAllYears(Selection selection) {

        // get all years as set
        SortedSet<String> years = new TreeSet<String>();
        Iterator<String[]> itTuple = selection.getHaul().iterator(true);
        while (itTuple.hasNext()) {
            String[] tuple = itTuple.next();
            String year = tuple[Haul.INDEX_YEAR];
            years.add(year);
        }

        // get all species with years
        Map<String, Set<String>> yearsForSpecies = new HashMap<String, Set<String>>();
        Iterator<String[]> itLength = selection.getLength().iterator(true);
        while (itLength.hasNext()) {
            String[] tupleLength = itLength.next();
            String species = tupleLength[Length.INDEX_SPECIES];
            String annee = tupleLength[Length.INDEX_YEAR];

            Set<String> speciesYears = yearsForSpecies.get(species);
            if (speciesYears == null) {
                speciesYears = new HashSet<String>();
                yearsForSpecies.put(species, speciesYears);
            }
            speciesYears.add(annee);
        }

        // on retire toutes les especes qui n'ont pas des années
        // completes
        List<String> completeSpecies = new ArrayList<String>();
        for (Map.Entry<String, Set<String>> yearForSpeciesEntry : yearsForSpecies.entrySet()) {
            if (yearForSpeciesEntry.getValue().equals(years)) {
                completeSpecies.add(yearForSpeciesEntry.getKey());
            }
        }

        return completeSpecies;
    }

    /**
     * Recherche les especes qui ont au moins une maturité sur l'ensemble des
     * données.
     *
     * @param selection selection to search into
     * @return year list
     */
    public Collection<String> getSpeciesWithMaturity(Selection selection) {

        Set<String> speciesWithMaturity = new HashSet<String>();

        // get all species with years
        Iterator<String[]> itLength = selection.getLength().iterator(true);
        while (itLength.hasNext()) {
            String[] tupleLength = itLength.next();
            String species = tupleLength[Length.INDEX_SPECIES];
            String maturity = tupleLength[Length.INDEX_MATURITY];

            if ("m".equalsIgnoreCase(maturity)) {
                speciesWithMaturity.add(species);
            }
        }

        return speciesWithMaturity;
    }

    /**
     * Get species name in project with data in [{@code beginYear}-{@code endYear}].
     *
     * Used in selection ui.
     *
     * Cette liste est issue du fichier captures (petit fichier, mais suffisant).
     *
     * Les especes sont determinées :
     * - en lisant les traits pour determiner les traits à partir des zones
     * - en lisant les captures pour determiner les especes à partir des traits
     * - en filtrant la liste des especes
     *
     * @param container       data container
     * @param project         project (can be {@code null}, no filtering)
     * @param filterSpecyType filterSpecyType (can be {@code null}, no filtering)
     * @return species
     */
    public List<String> getProjectSpecies(AbstractDataContainer container, Project project, Collection<String> filterSpecyType) {

        // first get species with trait list
        List<String> result = new ArrayList<String>();
        Iterator<String[]> itCatch = container.getCatch().iterator(true);
        while (itCatch.hasNext()) {
            String[] tuple = itCatch.next();

            // "Campagne","Annee","Trait","Espece","Nombre","Poids"
            String species = tuple[Catch.INDEX_SPECIES];
            if (!result.contains(species)) {
                result.add(species);
            }
        }

        // filtering is optionnal
        if (project != null && filterSpecyType != null) {
            // load map SpecyName > numSys
            Map<String, Integer> speciesNumSys = new HashMap<String, Integer>();
            Iterator<String[]> refTaxIterator = project.getRefTaxSpecies().iterator(true);
            while (refTaxIterator.hasNext()) {
                // "C_Perm";"NumSys";"NivSys";"C_VALIDE";"L_VALIDE";"AA_VALIDE";"C_TxPère";"Taxa"
                String[] tuple = refTaxIterator.next();
                Integer iNumSys = Integer.valueOf(tuple[1]);

                String specyId = null;
                switch (project.getStorageSpeciesType()) {
                    case C_PERM:
                        specyId = tuple[0];
                        break;
                    case C_Valide:
                        specyId = tuple[3];
                        break;
                    case L_Valide:
                        specyId = tuple[4];
                        break;
                }
                speciesNumSys.put(specyId, iNumSys);
            }

            // load specy type map SpecyTypeName > [min, max]
            // iteration sur les type d'especes
            Map<String, Integer[]> mapType = new HashMap<String, Integer[]>();
            Iterator<String[]> itTypeSpecies = project.getTypeEspeces().iterator(true);
            while (itTypeSpecies.hasNext()) {
                // "Types";"Commentaire";"NumSys min";"NumSys max","Code"
                String[] tuple = itTypeSpecies.next();
                String specyType = tuple[0];

                if (filterSpecyType != null && filterSpecyType.contains(specyType)) {
                    Integer iMinNumSys = Integer.valueOf(tuple[2]);
                    Integer iMaxNumSys = Integer.valueOf(tuple[3]);
                    mapType.put(specyType, new Integer[]{iMinNumSys, iMaxNumSys});
                }
            }

            // iteration sur les especes trouvées
            Iterator<String> itSpecies = result.iterator();
            while (itSpecies.hasNext()) {
                String specy = itSpecies.next();
                Integer specyNumSys = speciesNumSys.get(specy);

                if (specyNumSys == null) {
                    // ca ne peut pas arriver, ce cs est valider
                    // par un control
                    if (log.isWarnEnabled()) {
                        log.warn("Can't find specy " + specy + " in reftax");
                    }
                    itSpecies.remove();
                    continue;
                }

                // test si le numsys est dans les bornes d'un
                // des type d'espece demandé
                boolean foundInFilter = false;
                for (Integer[] bornes : mapType.values()) {
                    if (specyNumSys >= bornes[0]
                        && specyNumSys <= bornes[1]) {
                        foundInFilter = true;
                    }
                }

                if (!foundInFilter) {
                    itSpecies.remove();
                }
            }
        }

        Collections.sort(result);
        return result;
    }

    /**
     * Test que le nom de la nouvelle espece existe dans le Reftax actuellement
     * utilisé par le projet.
     *
     * Used in selection ui.
     *
     * @param project        project (avec reftax)
     * @param newSpeciesName species code to test
     * @return {@code true} if specy name exist
     */
    public boolean isSpeciesNameExist(Project project, String newSpeciesName) {

        boolean result = false;

        Iterator<String[]> itTuple = project.getRefTaxSpecies().iterator(true);
        while (itTuple.hasNext() && !result) {
            String[] tuple = itTuple.next();
            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String specyCode = null;
            switch (project.getStorageSpeciesType()) {
                case C_PERM:
                    specyCode = tuple[0];
                    break;
                case C_Valide:
                    specyCode = tuple[3];
                    break;
                case L_Valide:
                    specyCode = tuple[4];
                    break;
            }
            if (specyCode.equals(newSpeciesName)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Fusion d'especes.
     *
     * Used un selection ui.
     *
     * @param project        project
     * @param selection      selection
     * @param newSpeciesName new specy name (after merge)
     * @param comment        comment
     * @param speciesNames   species name to merge
     * @return project
     * @throws CoserBusinessException
     */
    public Project mergeSpecies(Project project, Selection selection, String newSpeciesName, String comment, String... speciesNames) throws CoserBusinessException {

        if (!isSpeciesNameExist(project, newSpeciesName)) {
            throw new CoserBusinessException(t("Species %s doesn't exist in referential",
                                               project.getDisplaySpeciesText(newSpeciesName)));
        }

        // check if new species name is not present in current selection
        // species list (collision)
        List<String> selectionSpecies = getProjectSpecies(selection, project, null);
        if (selectionSpecies.contains(newSpeciesName) &&
            !ArrayUtils.contains(speciesNames, newSpeciesName)) {
            throw new CoserBusinessException(t("Species %s already exists in current selection",
                                               project.getDisplaySpeciesText(newSpeciesName)));
        }

        MergeSpeciesCommand command = new MergeSpeciesCommand();
        command.setNewSpecyName(newSpeciesName);
        command.setSpeciesNames(speciesNames);
        command.setComment(comment);
        commandService.doAction(command, project, selection);

        return project;
    }

    /**
     * Effort d'échantillonnage: nombre de traits par strate par année.
     *
     * lengthunique=function(x){length(unique(x))} # nombre d'éléments unique dans x
     *
     * haulcount=tapply(TRAITS$Trait,list(stratum=TRAITS$Strate,year=TRAITS$Anneer),FUN=lengthunique)
     * haulcount[is.na(haulcount)]=0
     * total=apply(haulcount,2,sum)
     * haulcount=rbind(haulcount,total)
     * if(language==2) tab=cbind(Strate=dimnames(haulcount)[[1]],haulcount)
     * else tab=cbind(Stratum=dimnames(haulcount)[[1]],haulcount)
     *
     * Can return {@code null} if there is not enought data to build matrix.
     *
     * @param project   project
     * @param selection selection
     * @return sampling effort as matrix
     */
    public MatrixND getSamplingEffort(Project project, Selection selection) {

        // map strate > "year,integer"
        Map<String, Map<String, Double>> dynMatrix = new HashMap<String, Map<String, Double>>();
        Set<String> lineNames = new HashSet<String>();
        Set<String> columns = new HashSet<String>();

        Iterator<String[]> itData = selection.getHaul().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();

            String year = tuple[Haul.INDEX_YEAR];
            String strata = tuple[Haul.INDEX_STRATUM];
            lineNames.add(strata);
            columns.add(year);

            Map<String, Double> haulCountForStrata = dynMatrix.get(strata);
            if (haulCountForStrata == null) {
                haulCountForStrata = new HashMap<String, Double>();
                haulCountForStrata.put(year, 1.0);
                dynMatrix.put(strata, haulCountForStrata);
            } else {
                Double haulCountForYear = haulCountForStrata.get(year);
                if (haulCountForYear == null) {
                    haulCountForStrata.put(year, 1.0);
                } else {
                    haulCountForYear++;
                    haulCountForStrata.put(year, haulCountForYear);
                }
            }
        }

        List<String> lineNames2 = new ArrayList<String>(lineNames);
        List<String> columnsNames2 = new ArrayList<String>(columns);
        MatrixND matrix = null;
        if (!lineNames2.isEmpty() && !columnsNames2.isEmpty()) {
            Collections.sort(lineNames2);
            Collections.sort(columnsNames2);
            matrix = MatrixFactory.getInstance().create(n("coser.business.matrix.samplingeffort"), new List<?>[]{
                    lineNames2, columnsNames2});

            for (Map.Entry<String, Map<String, Double>> dynMatrixEntry : dynMatrix.entrySet()) {
                for (Map.Entry<String, Double> haulCountForYearEntry : dynMatrixEntry.getValue().entrySet()) {
                    matrix.setValue(dynMatrixEntry.getKey(), haulCountForYearEntry.getKey(), haulCountForYearEntry.getValue());
                }
            }
        }

        return matrix;
    }

    /**
     * Occurrence : pourcentage des traits dans lesquels une espèce est présentes
     *
     * years=seq(min(CAPTURES$Annee), max(CAPTURES$Annee))
     * nyears=length(years)
     * catch=CAPTURES[CAPTURES$Nombre>0,] #prendre seulement données positives
     *
     * #compter nombre de traits par année où chaque espèce est présent
     * occurence=tapply(catch$Trait,list(species=catch$Espece,year=catch$Annee),lengthunique)
     * occurrence[is.na(occurence)]<-0
     *
     * rapport.func=function(x,v) {return (x/v);}
     *
     * #diviser nombre de traits avec présence par nombre total de traits
     * pct.occur=apply(occurence,1,rapport.func,v=haulcount)# haulcount de 1)
     * pct.occur=t(round(pct.occur*100,2)) # multiplier par 100 et arrondir à 2 décimales
     *
     * colnames(pct.occur)=colnames(haulcount) #affectation noms colonnes matrice
     * rownames(pct.occur)=rownames(occur) #nom des espèces
     *
     * #calculerl l'occurrence moyenne à travers les années
     * provi.oc=round(apply(pct.occur,1,mean),2)
     * pct.occur=cbind(pct.occur,MeanOccurence=provi.oc)
     *
     * @param project
     * @param selection
     * @return occurence as matrix
     */
    public MatrixND getOccurrence(Project project, Selection selection) {

        // map species > "year, hauls"
        Map<String, Map<String, Set<String>>> dynMatrix = new HashMap<String, Map<String, Set<String>>>();
        Set<String> lineNames = new HashSet<String>();
        Set<String> columns = new HashSet<String>();
        Map<String, Set<String>> traitsCountPerYear = new HashMap<String, Set<String>>();

        Iterator<String[]> itData = selection.getCatch().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();

            String year = tuple[Catch.INDEX_YEAR];
            String haul = tuple[Catch.INDEX_HAUL];
            String species = tuple[Catch.INDEX_SPECIES];
            lineNames.add(species);
            columns.add(year);

            // count all traits
            Set<String> traitCountPerYear = traitsCountPerYear.get(year);
            if (traitCountPerYear == null) {
                Set<String> hauls = new HashSet<String>();
                hauls.add(haul);
                traitsCountPerYear.put(year, hauls);
            } else {
                traitCountPerYear.add(haul);
            }

            // count for species and year
            Map<String, Set<String>> haulCountForStrata = dynMatrix.get(species);
            if (haulCountForStrata == null) {
                haulCountForStrata = new HashMap<String, Set<String>>();
                Set<String> hauls = new HashSet<String>();
                hauls.add(haul);
                haulCountForStrata.put(year, hauls);
                dynMatrix.put(species, haulCountForStrata);
            } else {
                Set<String> traitCountPerYear2 = haulCountForStrata.get(year);
                if (traitCountPerYear2 == null) {
                    Set<String> hauls = new HashSet<String>();
                    hauls.add(haul);
                    haulCountForStrata.put(year, hauls);
                } else {
                    traitCountPerYear2.add(haul);
                }
            }
        }

        List<String> lineNames2 = new ArrayList<String>(lineNames);
        Collections.sort(lineNames2);
        List<String> columnsNames2 = new ArrayList<String>(columns);
        Collections.sort(columnsNames2);
        MatrixND matrix = MatrixFactory.getInstance().create(n("coser.business.matrix.occurrence"), new List<?>[]{
                lineNames2, columnsNames2});

        for (Map.Entry<String, Map<String, Set<String>>> dynMatrixEntry : dynMatrix.entrySet()) {
            for (Map.Entry<String, Set<String>> haulCountForYearEntry : dynMatrixEntry.getValue().entrySet()) {
                double occurence = (double) haulCountForYearEntry.getValue().size() /
                                   (double) traitsCountPerYear.get(haulCountForYearEntry.getKey()).size();
                occurence = MathUtils.round(occurence * 100.0, 2);

                matrix.setValue(dynMatrixEntry.getKey(), haulCountForYearEntry.getKey(), occurence);
            }
        }

        return matrix;

    }

    /**
     * Densité: nombre par km2 par espèce par année
     *
     * years=sort(unique(TRAITS$Annee))
     *
     * #surface total pour toutes les strates
     * totsurface=sum(STRATES$Surface)
     *
     * #ordonner table STRATES par nom des strates
     * STRATES=STRATES[order(STRATES$Strate),]
     *
     * #combiner les 3 tables de données dans une seule table tab
     * tab=merge(CAPTURES,STRATES,by.x=c("Annee","Trait"),by.y=c("Annee","Trait"))
     * tab=merge(tab,TRAITS, by.x=c("Annee","Trait"),by.y=c("Annee","Trait"))
     *
     * species=sort(unique(CAPTURES$Espece))
     *
     * #calculer la densité moyenne par année par strate par espèce
     * densparstrate=tapply(tab$Nombre/tab$SurfaceBalayee,list(tab$Espece, tab$Annee, tab$Strate), mean)
     * densparstrate[is.na(densparstrate)]=0 # remplace NA par 0
     *
     * #surface par annee et strate et espèce
     * surfaceparstrate=tapply(tab$Surface, list(tab$Espece, tab$Annee, tab$Strate), unique)
     *
     * #multiplier la densité par la surface de la strate pour obtenir nombre par année-strate
     * nombreparstrate= surfaceparstrate* densparstrate
     *
     * #sommer nombre sur strates par année pour chaque espèce et diviser par surface totale
     * densiteparannee=apply(nombreparstrate,c(1,2),sum)/totsurface
     *
     * #densité moyenne pour la période
     * densitemoyenneespece=apply(densiteparanne,1,mean)
     *
     * @param project   project
     * @param selection selection
     * @return density as matrix
     */
    public MatrixND getDensity(Project project, Selection selection) {

        // map species > "year, all density as double"
        Map<String, Map<String, Map<String, Set<Double>>>> densityPerSpecyStrataYear = new HashMap<String, Map<String, Map<String, Set<Double>>>>();
        Set<String> speciesList = new HashSet<String>();
        Set<String> yearsList = new HashSet<String>();

        // load map strataname > stratasurface
        Map<String, Double> startaAndSurface = new HashMap<String, Double>();
        double totalSourface = 0;
        Iterator<String[]> itStrata = selection.getStrata().iterator(true);
        while (itStrata.hasNext()) {
            String[] tuple = itStrata.next();
            String stataName = tuple[Strata.INDEX_STRATUM];
            String stringSurface = tuple[Strata.INDEX_SURFACE];
            try {
                Double surface = Double.valueOf(stringSurface);
                totalSourface += surface;
                startaAndSurface.put(stataName, surface);
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't convert surface '" + stringSurface + "' as double");
                }
            }
        }

        // load map traitname > stratename
        Map<String, String> haulAndStratas = new HashMap<String, String>();
        Map<String, Double> haulAndSweptSurface = new HashMap<String, Double>();
        Iterator<String[]> itHaul = selection.getHaul().iterator(true);
        while (itHaul.hasNext()) {
            String[] tuple = itHaul.next();
            String haul = tuple[Haul.INDEX_HAUL];
            String haulKey = tuple[Haul.INDEX_YEAR] + ";" + haul;
            String strataName = tuple[Haul.INDEX_STRATUM];
            haulAndStratas.put(haul, strataName);

            String sweptSurfaceAsString = tuple[Haul.INDEX_SWEPT_SURFACE];
            try {
                Double sweptSurface = Double.valueOf(sweptSurfaceAsString);
                haulAndSweptSurface.put(haulKey, sweptSurface);
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't convert swept surface '" + sweptSurfaceAsString + "' as double");
                }
            }
        }

        // on conserve egalement les surfaces associé a chaque species/year
        // pour post traiement
        Map<String, Double> surfacesParStrates = new HashMap<String, Double>();

        // calcul de la densité
        Iterator<String[]> itData = selection.getCatch().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();

            String species = tuple[Catch.INDEX_SPECIES];
            String year = tuple[Catch.INDEX_YEAR];
            String haul = tuple[Catch.INDEX_HAUL];
            String numberAsString = tuple[Catch.INDEX_NUMBER];
            String strata = haulAndStratas.get(haul);

            speciesList.add(species);
            yearsList.add(year);

            // compute density
            // densparstrate=tapply(tab$Nombre/tab$SurfaceBalayee,list(tab$Espece, tab$Annee, tab$Strate), mean)
            try {
                Double number = Double.valueOf(numberAsString);
                Double sweptSurface = haulAndSweptSurface.get(year + ";" + haul);
                Double density = null;
                if (sweptSurface == null) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't find swept surface for " + year + ";" + haul);
                    }
                    density = 0.0;
                } else {
                    density = number / sweptSurface;
                }

                // count for species and year
                Map<String, Map<String, Set<Double>>> specyDensityPerStrataYear = densityPerSpecyStrataYear.get(species);
                if (specyDensityPerStrataYear == null) {
                    specyDensityPerStrataYear = new HashMap<String, Map<String, Set<Double>>>();
                    Map<String, Set<Double>> strataDensityPerYear = new HashMap<String, Set<Double>>();
                    Set<Double> densities = new HashSet<Double>();
                    densities.add(density);
                    strataDensityPerYear.put(strata, densities);
                    specyDensityPerStrataYear.put(year, strataDensityPerYear);
                    densityPerSpecyStrataYear.put(species, specyDensityPerStrataYear);
                } else {
                    Map<String, Set<Double>> strataDensityPerYear = specyDensityPerStrataYear.get(year);
                    if (strataDensityPerYear == null) {
                        strataDensityPerYear = new HashMap<String, Set<Double>>();
                        Set<Double> densities = new HashSet<Double>();
                        densities.add(density);
                        strataDensityPerYear.put(strata, densities);
                        specyDensityPerStrataYear.put(year, strataDensityPerYear);
                    } else {
                        Set<Double> specyYearDensity = strataDensityPerYear.get(strata);
                        if (specyYearDensity == null) {
                            specyYearDensity = new HashSet<Double>();
                            specyYearDensity.add(density);
                            strataDensityPerYear.put(strata, specyYearDensity);
                        } else {
                            specyYearDensity.add(density);
                        }
                    }
                }

                // surfaceparstrate
                Double surface = startaAndSurface.get(strata);

                surfacesParStrates.put(species + ";" + year + ";" + strata, surface);
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't convert number '" + numberAsString + "' as double");
                }
            }
        }

        // convert to matrix
        List<String> sortedSpecies = new ArrayList<String>(speciesList);
        Collections.sort(sortedSpecies);
        List<String> sortedYears = new ArrayList<String>(yearsList);
        Collections.sort(sortedYears);
        MatrixND matrix = MatrixFactory.getInstance().create(n("coser.business.matrix.density"), new List<?>[]{
                sortedSpecies, sortedYears});

        for (Map.Entry<String, Map<String, Map<String, Set<Double>>>> dynMatrixEntry : densityPerSpecyStrataYear.entrySet()) {
            String specy = dynMatrixEntry.getKey();
            for (Map.Entry<String, Map<String, Set<Double>>> haulCountForYearEntry : dynMatrixEntry.getValue().entrySet()) {
                String year = haulCountForYearEntry.getKey();

                // nombreparstrate= surfaceparstrate*densparstrate
                // densiteparannee=apply(nombreparstrate,c(1,2),sum)/totsurface
                double totalNombreparstrate = 0;
                for (Map.Entry<String, Set<Double>> densityForYear : haulCountForYearEntry.getValue().entrySet()) {

                    // maen density
                    double totalDensity = 0;
                    double densityCount = 0;
                    for (Double singleDensity : densityForYear.getValue()) {
                        totalDensity += singleDensity;
                        densityCount++;
                    }
                    double meanDensity = totalDensity / densityCount;

                    String strata = densityForYear.getKey();
                    Double surfaceparstrate = surfacesParStrates.get(specy + ";" + year + ";" + strata);
                    if (surfaceparstrate == null) {
                        if (log.isWarnEnabled()) {
                            log.warn("no surface for " + specy + ";" + year + ";" + strata);
                        }
                        surfaceparstrate = 0.0;
                    }
                    double nombreparstrate = surfaceparstrate * meanDensity;
                    totalNombreparstrate += nombreparstrate;
                }
                double densiteparannee = totalNombreparstrate / totalSourface;

                matrix.setValue(specy, year, densiteparannee);
            }
        }

        return matrix;
    }

    /**
     * Présentation du graphique : histogramme de distribution en tailles par
     * espèce, par strate et par année (une planche par espèce et par année)
     * pour toute la série. Avec la possibilité de voir regroupé l'ensemble des
     * années.
     *
     * Données. TAILLES$Nombres et TAILLES$Longueur
     * Calcul. Sommer TAILLES$Nombres par classe de longueur (TAILLES$Longueur)
     * sur tous les traits d'une année (ou tous les années si regroupé)
     *
     * Seconde version : matrix proxy et provider.
     *
     * Cette methode utilise directement les noms de visualisation des especes
     * au lieu de leurs codes (trop compliqué de faire un rendu basé sur le code).
     *
     * @param project       project
     * @param dataContainer data container
     * @return matrix proxy with 4 dimension
     */
    public MatrixND getLengthStructure(Project project, AbstractDataContainer dataContainer) {

        // load map traitname > stratename
        Map<String, String> haulAndStratas = new HashMap<String, String>();
        Iterator<String[]> itHaul = dataContainer.getHaul().iterator(true);
        while (itHaul.hasNext()) {
            String[] tuple = itHaul.next();
            String haul = tuple[Haul.INDEX_HAUL];
            String strataName = tuple[Haul.INDEX_STRATUM];
            haulAndStratas.put(haul, strataName);
        }

        // dimensions
        Set<Double> lengthSet = new HashSet<Double>();
        Set<String> speciesSet = new HashSet<String>();
        Set<String> strataSet = new HashSet<String>();
        Set<String> yearsSet = new HashSet<String>();

        Iterator<String[]> itData = dataContainer.getLength().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();

            String year = tuple[Length.INDEX_YEAR];
            String haul = tuple[Length.INDEX_HAUL];
            String species = tuple[Length.INDEX_SPECIES];
            String lengthAsString = tuple[Length.INDEX_LENGTH];
            String strata = haulAndStratas.get(haul);

            if (strata == null) {
                if (log.isWarnEnabled()) {
                    log.warn("No strata for haul " + haul);
                }
                continue;
            }

            // remember for matrix spemantics
            String speciesText = project.getDisplaySpeciesText(species);
            speciesSet.add(speciesText);
            strataSet.add(strata);
            yearsSet.add(year);

            // get correct length step
            // plain or half centimeters
            try {
                double length = Double.parseDouble(lengthAsString);
                length = getHalfStepValue(length);
                lengthSet.add(length);
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse length as double : " + lengthAsString);
                }
            }
        }

        // convert map to matrixND
        List<Double> lengthSem = new ArrayList<Double>(lengthSet);
        Collections.sort(lengthSem);
        List<String> speciesSem = new ArrayList<String>(speciesSet);
        Collections.sort(speciesSem);
        List<String> strataSem = new ArrayList<String>(strataSet);
        Collections.sort(strataSem);
        List<String> yearsSem = new ArrayList<String>(yearsSet);
        Collections.sort(yearsSem);

        if (log.isDebugEnabled()) {
            log.debug(t("Creating matrix : %d*%d*%d*%d", lengthSem.size(), speciesSem.size(), strataSem.size(), yearsSem.size()));
        }

        MatrixND matrix = MatrixFactory.getInstance().createProxy(n("coser.business.matrix.lengthstructure"),
                                                                  new List<?>[]{lengthSem, speciesSem, strataSem, yearsSem},
                                                                  new String[]{n("coser.business.common.length"),
                                                                               n("coser.business.common.species"),
                                                                               n("coser.business.common.strata"),
                                                                               n("coser.business.common.years")},
                                                                  new LengthStructureMatrixProvider(project, dataContainer));

        return matrix;
    }

    /**
     * Length structure matrix provider.
     */
    class LengthStructureMatrixProvider implements MatrixProvider {

        protected Project project;

        protected AbstractDataContainer container;

        public LengthStructureMatrixProvider(Project project, AbstractDataContainer container) {
            this.project = project;
            this.container = container;
        }

        /*
         * @see org.nuiton.math.matrix.MatrixProvider#fillValues(org.nuiton.math.matrix.MatrixND)
         */
        @Override
        public void fillValues(MatrixND matrix) {
            fillLengthStructureMatrix(project, container, matrix);
        }
    }

    /**
     * Length structure matrix provider filler part.
     *
     * @param project       project
     * @param dataContainer data container
     * @param matrix        non proxy matrix to fill (inited with 0.0)
     */
    public void fillLengthStructureMatrix(Project project, AbstractDataContainer dataContainer, MatrixND matrix) {

        // load map traitname > stratename
        Map<String, String> haulAndStratas = new HashMap<String, String>();
        Iterator<String[]> itHaul = dataContainer.getHaul().iterator(true);
        while (itHaul.hasNext()) {
            String[] tuple = itHaul.next();
            String haul = tuple[Haul.INDEX_HAUL];
            String strataName = tuple[Haul.INDEX_STRATUM];
            haulAndStratas.put(haul, strataName);
        }
        DimensionHelper dimHelper = new DimensionHelper();

        Iterator<String[]> itData = dataContainer.getLength().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();

            String year = tuple[Length.INDEX_YEAR];
            String haul = tuple[Length.INDEX_HAUL];
            String species = tuple[Length.INDEX_SPECIES];
            String lengthAsString = tuple[Length.INDEX_LENGTH];
            String numberAsString = tuple[Length.INDEX_NUMBER];
            String strata = haulAndStratas.get(haul);

            if (strata == null) {
                if (log.isWarnEnabled()) {
                    log.warn("No strata for haul " + haul);
                }
                continue;
            }

            // quitte l'iteration courante si les dimensions demandée
            // ne sont pas dans la matrice reduite
            String speciesText = project.getDisplaySpeciesText(species);
            if (!matrix.getSemantic(1).contains(speciesText)) {
                continue;
            }
            if (!matrix.getSemantic(2).contains(strata)) {
                continue;
            }
            if (!matrix.getSemantic(3).contains(year)) {
                continue;
            }

            // get correct length step
            // plain or half centimeters
            try {
                double length = Double.parseDouble(lengthAsString);
                length = getHalfStepValue(length);

                if (!matrix.getSemantic(0).contains(length)) {
                    continue;
                }

                try {
                    double number = Double.parseDouble(numberAsString);

                    // dimHelper is more performant
                    double currentNumber = matrix.getValue(dimHelper.get(length, speciesText, strata, year));
                    currentNumber += number;
                    matrix.setValue(dimHelper.get(length, speciesText, strata, year), currentNumber);
                } catch (NumberFormatException ex) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't parse number as double : " + numberAsString);
                    }
                }
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse length as double : " + lengthAsString);
                }
            }
        }
    }

    /**
     * Retourne la valeur au demi pas la plus proche.
     *
     * 38.0 > 38.0
     * 38.1 > 38.0
     * 38.6 > 38.5
     *
     * @param value value to get hasl step
     * @return half step value
     */
    protected double getHalfStepValue(double value) {
        // get length step to use
        double floor = Math.floor(value);
        // ne pas prendre 0.5 car 10.0 serait arrondit à 10.5
        double round = Math.floor(value + 0.5);

        // on prend le pas ou demi pas le plus proche
        if (floor == round) {
            value = floor;
        } else {
            value = floor + 0.5;
        }
        return value;
    }

    /**
     * Extrait les données de la selection pour rSufi.
     *
     * @param project       project
     * @param selection     selection
     * @param directory     directory to extract file to
     * @param onlyDataTable extract only data table without additionnal information
     * @return extracted directory
     * @throws CoserBusinessException
     */
    public File extractRSUfiData(Project project, Selection selection, File directory, boolean onlyDataTable) throws CoserBusinessException {

        // Nommer le dossier RSUFI_DATA_nomduprojet.... 
        File exportDirectory = new File(directory, "RSUFI_DATA_" + project.getName());
        exportDirectory.mkdirs();

        // extract 4 data files
        for (Category category : Category.values()) {
            if (category.isDataCategory()) {
                // originals names without suffix
                String storageFileName = commonService.getDataStorageFileName(project, category, null);
                File dataFile = new File(exportDirectory, storageFileName);
                if (log.isDebugEnabled()) {
                    log.debug("Saving selection file : " + dataFile);
                }

                DataStorage content = getProjectContent(project, selection, category, false);
                Map<String, String> reftaxSpecies = getReftaxSpeciesDisplayFieldMap(project, true);
                commonService.storeDataWhithoutQuote(content, dataFile, reftaxSpecies, category);
            }
        }

        if (!onlyDataTable) {
            // extract additionnal data file (selected species)
            File listSpeciesFile = new File(exportDirectory, "ListEspeces" + project.getName() + ".txt");
            fillListSpeciesFile(selection, listSpeciesFile);

            // extract additionnal project information
            File informationFile = new File(exportDirectory, "project.txt");
            PrintStream out = null;
            try {
                out = new PrintStream(new BufferedOutputStream(new FileOutputStream(informationFile)));

                out.println(t("coser.business.extract.projectName") + project.getName());
                out.println(t("coser.business.extract.projectAuthor") + project.getAuthor());
                out.println(t("coser.business.extract.projectComment") + project.getComment());
                out.println(t("coser.business.extract.selectionName") + selection.getName());

                // date de creation du projet
                DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, config.getLocale());
                out.println(t("coser.business.extract.creationdate") + dateFormat.format(project.getCreationDate()));

                // add selection additional files
                File projectsDirectory = config.getRSufiProjectsDirectory();
                File projectDirectory = new File(projectsDirectory, project.getName());
                File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                File selectionDirectory = new File(selectionsDirectory, selection.getName());
                File otherFilesDirectory = new File(selectionDirectory, CoserConstants.STORAGE_SELECTION_FILES);
                if (otherFilesDirectory.isDirectory()) {
                    FileUtils.copyDirectoryToDirectory(otherFilesDirectory, exportDirectory);
                }

                // add reftax
                File reftaxFile = new File(projectDirectory, CoserConstants.Category.REFTAX_SPECIES.getStorageFileName());
                FileUtils.copyFileToDirectory(reftaxFile, exportDirectory);

            } catch (IOException ex) {
                throw new CoserBusinessException("Can't save project information file", ex);
            } finally {
                IOUtils.closeQuietly(out);
            }
        }

        return exportDirectory;
    }

    /**
     * Ecrit le fichier ListEspeces.txt dans le fichier specifiés.
     *
     * Est utilisé lors de l'extraction des données RSUfi et lors de
     * la sauvegarde d'une selection validée (demande client).
     *
     * @param selection selection
     * @param ouputFile output file
     * @throws CoserBusinessException ection
     */
    protected void fillListSpeciesFile(Selection selection, File ouputFile) throws CoserBusinessException {
        // extract additionnal data file (selected species)
        List<String> allSpecies = selection.getSelectedSpecies();
        List<String> occDensSpecies = selection.getSelectedSpeciesOccDens();
        List<String> sizeAllYearSpecies = selection.getSelectedSpeciesSizeAllYear();
        List<String> maturitySpecies = selection.getSelectedSpeciesMaturity();

        PrintStream out = null;
        try {
            out = new PrintStream(new BufferedOutputStream(new FileOutputStream(ouputFile)));

            // header
            out.println("Espece\tListAll\tListIdent\tListLong\tListMat");

            for (String species : allSpecies) {
                String listIdent = occDensSpecies.contains(species) ? "1" : "0";
                String listLong = sizeAllYearSpecies.contains(species) ? "1" : "0";
                String listMat = maturitySpecies.contains(species) ? "1" : "0";
                out.println(species + "\t1\t" + listIdent + "\t" + listLong + "\t" + listMat);
            }
        } catch (FileNotFoundException ex) {
            throw new CoserBusinessException("Can't extract rsufi files", ex);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Look for project survey name in rsufi result.
     * Read estcomind file to known that.
     * Used in webservice to know map file names.
     *
     * @param resultDirectory result directory
     * @param rSufiResult     rsufi result
     * @return survey name
     * @throws CoserBusinessException
     */
    public String getProjectSurveyName(File resultDirectory, RSufiResult rSufiResult) throws CoserBusinessException {
        String result = null;

        // le fichier estcomind est moins gros que l'autre
        String estComIndName = rSufiResult.getEstComIndName();
        File estComIndFile = new File(resultDirectory, estComIndName);
        DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        Iterator<String[]> itEstComIndData = dataStorage.iterator(true);
        while (StringUtils.isEmpty(result) && itEstComIndData.hasNext()) {
            result = itEstComIndData.next()[0];
        }

        return result;
    }

    /**
     * Get all strata's haul coordinates of given strata.
     *
     * @param selection        project's selection
     * @param strataCollection starta collection to get haul
     * @return haul collection
     * @throws CoserBusinessException
     * @since 1.1
     */
    public List<Coordinate> getStrataHaulCoordinate(Selection selection, Collection<String> strataCollection) throws CoserBusinessException {

        List<Coordinate> hauls = new ArrayList<Coordinate>();

        // serie cache to compute serie index
        List<String> serieCache = new ArrayList<String>();

        Iterator<String[]> itData = selection.getHaul().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();

            String strata = tuple[Haul.INDEX_STRATUM];

            // add it to cache (even not selected to stay same color
            // in different display)
            if (!serieCache.contains(strata)) {
                serieCache.add(strata);
            }

            if (strataCollection.contains(strata)) {
                String stratum = tuple[Haul.INDEX_STRATUM];
                String year = tuple[Haul.INDEX_YEAR];
                String haul = tuple[Haul.INDEX_HAUL];
                String depth = tuple[Haul.INDEX_DEPTH];

                String lat = tuple[Haul.INDEX_LAT];
                String longi = tuple[Haul.INDEX_LONG];

                double dlat = Double.parseDouble(lat);
                double dlong = Double.parseDouble(longi);

                String name = t("coser.business.map.haulname", stratum, year, haul, depth);
                int serieIndex = serieCache.indexOf(strata);
                Coordinate coordinate = new Coordinate(serieIndex, name, dlat, dlong);
                hauls.add(coordinate);
            }
        }

        return hauls;
    }

    /**
     * Retourne une map de toutes les selections par projet dans le but de
     * selectionner une selection pour la rejouer dans l'ui.
     *
     * @return all selections
     * @since 1.2
     */
    public SortedMap<String, List<String>> getSelectionByProject() {
        SortedMap<String, List<String>> selectionByProject = new TreeMap<String, List<String>>();

        // parcours des resultats disponibles
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        String projectName = projectFile.getName();
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {

                                List<String> selections = selectionByProject.get(projectName);
                                if (selections == null) {
                                    selections = new ArrayList<String>();
                                    selectionByProject.put(projectName, selections);
                                }
                                selections.add(selectionFile.getName());
                            }
                        }
                    }
                }
            }
        }

        return selectionByProject;
    }

    /**
     * Open selection (without opening associated project) without associated
     * data (just properties info).
     * Selection history commands are loaded too (for replay).
     *
     * @param projectName   project containing selection
     * @param selectionName selection name to open in project
     * @return opened selection
     * @throws CoserBusinessException
     * @since 1.2
     */
    public Selection openSelection(String projectName, String selectionName) throws CoserBusinessException {
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, projectName);
        if (!projectDirectory.isDirectory()) {
            throw new CoserBusinessException(t("Project %s doesn't exists !", projectName));
        }
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File selectionDirectory = new File(selectionsDirectory, selectionName);
        if (!projectDirectory.isDirectory()) {
            throw new CoserBusinessException(t("Selection %s doesn't exists !", projectName));
        }

        Selection selection = new Selection();
        selection.setName(selectionName);

        // relecture des informations de la selection (properties)
        File selectionPropertiesFile = new File(selectionDirectory, selectionName + ".selection");
        InputStream inputStream = null;
        try {
            Properties props = new Properties();
            inputStream = new FileInputStream(selectionPropertiesFile);
            props.load(inputStream);
            selection.fromProperties(props);

            List<Command> commands = getHistoryCommandsFromProperties(props, "selection.commands");
            selection.setHistoryCommands(commands);

            if (log.isDebugEnabled()) {
                log.debug("Read selection properties file : " + selectionPropertiesFile);
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read selection properties file", ex);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return selection;
    }

    /**
     * Definie dans la selection les listes L2 à L4 avec les 3 listes
     * en parametres.
     * Fait attention a ne pas conserver dans les listes des especes qui
     * ne sont pas dans la liste L1 (liste de toutes les especes).
     *
     * @param selection
     * @param selectedSpeciesOccDens
     * @param selectedSpeciesSizeAllYear
     * @param selectedSpeciesMaturity
     * @since 1.2
     */
    public void fillListsSelection(Selection selection, List<String> selectedSpeciesOccDens,
                                   List<String> selectedSpeciesSizeAllYear, List<String> selectedSpeciesMaturity) {

        List<String> allSpecies = selection.getSelectedSpecies();

        // filters species that doesn't exists anymore
        List<String> localSpeciesOccDens = ListUtils.retainAll(selectedSpeciesOccDens, allSpecies);
        List<String> localSpeciesSizeAllYear = ListUtils.retainAll(selectedSpeciesSizeAllYear, allSpecies);
        List<String> localSpeciesMaturity = ListUtils.retainAll(selectedSpeciesMaturity, allSpecies);

        // save lists
        selection.setSelectedSpeciesOccDens(localSpeciesOccDens);
        selection.setSelectedSpeciesSizeAllYear(localSpeciesSizeAllYear);
        selection.setSelectedSpeciesMaturity(localSpeciesMaturity);
    }

    /**
     * Retourne une map de transcription entre la valeur de stockage de l'espece
     * et la valeur retranscrite à l'utilisateur suivant les préférences qu'il
     * a renseigner dans le projet.
     * La map est triée sur le "VALEUR" et non sur la clé (pour la visualisation
     * soit triée suivant la représentation de l'utilisateur).
     *
     * Les données sont issues du reftax.
     *
     * @param project     le projet pour avoir accès au reftax
     * @param outputField meme algorithm, mais renvoi une map de correspondance
     *                    pour le champ de sortie
     * @since 1.3
     */
    public LinkedHashMap<String, String> getReftaxSpeciesDisplayFieldMap(Project project, boolean outputField) {
        Map<String, String> speciesMap = new HashMap<String, String>();

        // "C_Perm";"NumSys";"NivSys";"C_VALIDE";"L_VALIDE";"AA_VALIDE";"C_TxPère";"Taxa"
        Iterator<String[]> itData = project.getRefTaxSpecies().iterator(true);
        while (itData.hasNext()) {
            String[] tuple = itData.next();
            String key = null;
            String value = null;

            switch (project.getStorageSpeciesType()) {
                case C_Valide:
                    key = tuple[3];
                    break;
                case C_PERM:
                    key = tuple[0];
                    break;
                case L_Valide:
                    key = tuple[4];
                    break;
            }

            SpeciesFieldType valueField = outputField ? project.getOutputSpeciesType() :
                                          project.getDisplaySpeciesType();

            switch (valueField) {
                case C_Valide:
                    value = tuple[3];
                    break;
                case C_PERM:
                    value = tuple[0];
                    break;
                case L_Valide:
                    value = tuple[4];
                    break;
            }

            speciesMap.put(key, value);
        }

        return CoserUtils.sortByValue(speciesMap);
    }

    /**
     * Save existing rsufi result.
     *
     * @param project     project
     * @param selection   selection
     * @param rsufiResult new result to save
     * @throws CoserBusinessException
     * @since 1.4
     */
    public void editSelectionOptions(Project project, Selection selection, List<File> othersFile) throws CoserBusinessException {

        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());
        File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File selectionDirectory = new File(selectionsDirectory, selection.getName());
        File otherFilesDirectory = new File(selectionDirectory, CoserConstants.STORAGE_SELECTION_FILES);

        // property file
        try {
            saveProjectSelection(project, selection);

            // remove some files
            Collection<File> toDeletes = CollectionUtils.subtract(selection.getOtherFiles(), othersFile);
            for (File toDelete : toDeletes) {
                FileUtils.deleteQuietly(toDelete);
            }
            // copy new file
            for (File otherFile : othersFile) {
                // non modified result, skipping it
                if (!otherFile.getAbsolutePath().startsWith(otherFilesDirectory.getAbsolutePath())) {
                    if (otherFile.isDirectory()) {
                        FileUtils.copyDirectoryToDirectory(otherFile, otherFilesDirectory);
                    } else {
                        FileUtils.copyFileToDirectory(otherFile, otherFilesDirectory);
                    }
                }
            }
            // update other files list
            List<File> otherFiles = new ArrayList<File>();
            if (otherFilesDirectory.isDirectory()) {
                for (File file : otherFilesDirectory.listFiles()) {
                    otherFiles.add(file);
                }
            }
            selection.setOtherFiles(otherFiles);

        } catch (IOException ex) {
            throw new CoserBusinessException("Can't edit result", ex);
        }
    }
}

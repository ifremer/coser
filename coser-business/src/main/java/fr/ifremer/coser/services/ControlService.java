/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.FileManagerFactory;
import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.DefaultLocaleProvider;
import com.opensymphony.xwork2.DefaultTextProvider;
import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.config.ConfigurationManager;
import com.opensymphony.xwork2.inject.Container;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.ValueStackFactory;
import com.opensymphony.xwork2.validator.ActionValidatorManager;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.control.DiffCatchLengthControlError;
import fr.ifremer.coser.control.SpeciesControlError;
import fr.ifremer.coser.control.SpeciesLengthControlError;
import fr.ifremer.coser.data.AbstractDataEntity;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Haul;
import fr.ifremer.coser.data.Length;
import fr.ifremer.coser.data.Strata;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.util.ProgressMonitor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.t;

/**
 * Validation service.
 *
 * @author chatellier
 */
public class ControlService {

    private static final Log log = LogFactory.getLog(ControlService.class);

    protected CoserBusinessConfig config;

    protected ValidationAwareSupport validationSupport;

    protected DelegatingValidatorContext validationContext;

    protected ActionValidatorManager validator;

    protected ActionContext context;

    /** Pattern des tailles de poisson (ex : 1.5, 1.50, 2.00). */
    protected static final Pattern FISH_LENGTH_PATTERN = Pattern.compile("^\\d+(\\.[05]0*)?$");

    /**
     * Initialise le context xworks.
     *
     * @param config configuration
     */
    public ControlService(CoserBusinessConfig config) {
        this.config = config;
        validationSupport = new ValidationAwareSupport();
        validationContext = new DelegatingValidatorContext(validationSupport, new DefaultTextProvider(), new DefaultLocaleProvider());

        ConfigurationManager confManager = new ConfigurationManager("xwork");
        Configuration conf = confManager.getConfiguration();
        Container container = conf.getContainer();

        // huge improve cache performance
        FileManagerFactory fileManagerFactory = container.getInstance(FileManagerFactory.class);
        fileManagerFactory.setReloadingConfigs("false");

        ValueStackFactory stackFactory = container.getInstance(ValueStackFactory.class);
        ValueStack vs = stackFactory.createValueStack();

        context = new ActionContext(vs.getContext());
        ActionContext.setContext(context);

        // init validator
        Container container2 = context.getContainer();
        validator = container2.getInstance(ActionValidatorManager.class, "no-annotations");
    }

    /**
     * Valide un seul bean, retourne la liste des erreurs trouvées.
     *
     * @param bean     bean to validate
     * @param category result errors category
     * @return error list
     */
    protected List<ControlError> validate(AbstractDataEntity bean, Category category) {
        List<ControlError> result = new ArrayList<ControlError>();
        try {
            // obligatoire pour les appel dans les thread et swing EDT
            ActionContext.setContext(context);

            for (ValidationLevel validationLevel : ValidationLevel.values()) {

                try {
                    validator.validate(bean, validationLevel.getXWorkContext(), validationContext);

                    if (validationContext.hasErrors()) {
                        Map<String, List<String>> fieldErrors = validationContext.getFieldErrors();

                        for (List<String> errors : fieldErrors.values()) {
                            ControlError error = new ControlError();
                            error.setCategory(category);
                            String messages = StringUtils.join(errors, ", ");
                            error.setMessage(messages);
                            error.setLevel(validationLevel);

                            // add error line
                            error.addLineNumber(bean.getLine());

                            result.add(error);
                        }

                        Collection<String> actionMessages = validationContext.getActionErrors();
                        if (CollectionUtils.isNotEmpty(actionMessages)) {
                            ControlError error = new ControlError();
                            error.setCategory(category);
                            String messages = StringUtils.join(actionMessages, ", ");
                            error.setMessage(messages);
                            error.setLevel(validationLevel);

                            // add error line
                            error.addLineNumber(bean.getLine());

                            result.add(error);
                        }
                    }
                } finally {
                    // vidage par log level
                    validationSupport.clearFieldErrors();
                    validationSupport.clearActionErrors();
                }
            }

        } catch (ValidationException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't validate bean", ex);
            }
        } finally {
            validationSupport.clearFieldErrors();
            validationSupport.clearActionErrors();
        }
        return result;
    }

    /**
     * Valide toutes les données du projet.
     *
     * @param project  project
     * @param control  control a valider
     * @param progress progress monitor (can be null)
     * @return les erreurs de validation
     */
    public List<ControlError> validateData(Project project, Control control, ProgressMonitor progress) {

        if (progress != null) {
            progress.setStepCount(11);
            progress.setStep(0);
        }

        // valide chaque category
        List<ControlError> validationErrors = new ArrayList<ControlError>();
        for (Category category : Category.values()) {
            if (category.isDataCategory()) {
                // validation de la category seule (generique)
                List<ControlError> categoryErrors = validateCategoryXWork(project, control, category, progress);
                if (categoryErrors != null) {
                    validationErrors.addAll(categoryErrors);
                }
                if (progress != null) {
                    progress.nextStep();
                }

                // validation specifique de la category
                List<ControlError> specificErrors = validateCategorySpecific(project, control, category, progress);
                if (specificErrors != null) {
                    validationErrors.addAll(specificErrors);
                }
                if (progress != null) {
                    progress.nextStep();
                }
            }
        }

        // validation entre catch et length (specific)
        List<? extends ControlError> diffCatchLengthErrors = validateDiffCatchLength(project, control, progress);
        if (progress != null) {
            progress.nextStep();
        }
        if (diffCatchLengthErrors != null) {
            validationErrors.addAll(diffCatchLengthErrors);
        }

        // validation par croisement de fichiers
        List<ControlError> crossFileErrors = validationCrossFiles(project, control, progress);
        if (progress != null) {
            progress.nextStep();
        }
        if (crossFileErrors != null) {
            validationErrors.addAll(crossFileErrors);
        }

        // cas particulier, s'il n'y a aucune erreur, on ajout une erreur de
        // type info pour dire qu'il n'y a pas d'erreur
        if (validationErrors.isEmpty()) {
            ControlError noErrorError = new ControlError();
            noErrorError.setLevel(ValidationLevel.INFO);
            noErrorError.setMessage(t("coser.business.control.noerrorfound"));
            validationErrors.add(noErrorError);
        }
        return validationErrors;
    }

    /**
     * Valide une category entière d'un project.
     *
     * @param project  project
     * @param control  control a valider
     * @param category category a valider
     * @param progress progress monitor (can be null)
     * @return les erreurs de validation (not null)
     */
    public List<ControlError> validateCategoryXWork(Project project, Control control,
                                                    Category category, ProgressMonitor progress) {

        // instance des bean utilisé lors de la validation
        Catch beanCatch = new Catch();
        Haul beanHaul = new Haul();
        Length beanLength = new Length();
        Strata beanStrata = new Strata();

        // get content depending on category
        DataStorage dataToCheck = null;
        switch (category) {
            case CATCH:
                dataToCheck = control.getCatch();
                break;
            case HAUL:
                dataToCheck = control.getHaul();
                break;
            case LENGTH:
                dataToCheck = control.getLength();
                break;
            case STRATA:
                dataToCheck = control.getStrata();
                break;
        }

        // update progress
        int total = dataToCheck.size() - 1;
        if (progress != null) {
            progress.setText(t("coser.business.control.step.xworks", t(category.getTranslationKey()), 0));
            progress.setTotal(total);
        }

        Map<String, String> uniqueDataKeys = new HashMap<String, String>();
        Map<String, ControlError> uniqueDataErrors = new HashMap<String, ControlError>();
        List<ControlError> validationErrors = new ArrayList<ControlError>();
        Iterator<String[]> itDataToCheck = dataToCheck.iterator(true);
        int lineIndex = 1; // 1 = skip header
        while (itDataToCheck.hasNext()) {

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.xworks", t(category.getTranslationKey()), progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            // first, do single bean validation
            String[] line = itDataToCheck.next();
            List<ControlError> errors = null;
            switch (category) {
                case CATCH:
                    beanCatch.setData(line);
                    errors = validate(beanCatch, category);
                    break;
                case HAUL:
                    beanHaul.setData(line);
                    errors = validate(beanHaul, category);
                    break;
                case LENGTH:
                    beanLength.setData(line);
                    errors = validate(beanLength, category);
                    break;
                case STRATA:
                    beanStrata.setData(line);
                    errors = validate(beanStrata, category);
                    break;
            }
            if (errors != null) {
                validationErrors.addAll(errors);
            }

            // check for duplicated lines
            String lineNumber = line[AbstractDataEntity.INDEX_LINE];
            String uniqueDataKey = getSignificantData(project, category, line);
            if (uniqueDataKeys.containsKey(uniqueDataKey)) {

                // make a single ControlError instance pour the same
                // duplicated even for more than 2 lines
                ControlError error = null;
                if (uniqueDataErrors.containsKey(uniqueDataKey)) {
                    error = uniqueDataErrors.get(uniqueDataKey);
                } else {
                    error = new ControlError();
                    error.setCategory(category);
                    error.setLevel(ValidationLevel.ERROR);
                    // warning, line number must stay sorted
                    error.addLineNumber(uniqueDataKeys.get(uniqueDataKey));
                    error.setMessage(t("coser.business.control.error.duplicatedLine", uniqueDataKey));
                    error.setDetailMessage(t("coser.business.control.error.duplicatedLineDetails", uniqueDataKey));
                    validationErrors.add(error);
                    uniqueDataErrors.put(uniqueDataKey, error);
                }
                // add current line to error (after : important)
                error.addLineNumber(lineNumber);

                // small hack to slow down control by initializing cache
                // because ui will take a lot of time to do it
                // and user will wait to must time
                // real dummy call, but do not remove it
                dataToCheck.indexOf(lineNumber); // init cache
            } else {
                uniqueDataKeys.put(uniqueDataKey, lineNumber);
            }
        }

        return validationErrors;
    }

    /**
     * Retourne sous forme de clé la partie significative des données.
     * Donc sans le numéro de ligne suivant la catégorie.
     *
     * Utilisé pour la detection des doublons.
     *
     * CAPTURES
     * Vérifier l'unicité sur : "Campagne", "Annee", "Trait", "Espece"
     *
     * STRATES
     * Vérifier l'unicité sur : "Campagne", "Strate"
     *
     * TAILLES
     * Vérifier l'unicité sur : "Campagne", "Annee", "Trait", "Espece", "Sexe", "Maturite", "Longueur"
     *
     * TRAITS
     * Vérifier l'unicité sur : "Campagne", "Annee", "Trait", "Mois"
     *
     * @param project  project
     * @param category category
     * @param data     data
     * @return string key
     */
    protected String getSignificantData(Project project, Category category, String[] data) {
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < data.length; ++index) {
            // never count index line
            if (index != AbstractDataEntity.INDEX_LINE) {
                switch (category) {
                    case CATCH:
                        if (index == Catch.INDEX_SURVEY || index == Catch.INDEX_YEAR || index == Catch.INDEX_HAUL) {
                            sb.append(data[index]).append('|');
                        } else if (index == Catch.INDEX_SPECIES) {
                            sb.append(project.getDisplaySpeciesText(data[index])).append('|');
                        }
                        break;
                    case HAUL:
                        if (index == Haul.INDEX_SURVEY || index == Haul.INDEX_YEAR || index == Haul.INDEX_HAUL || index == Haul.INDEX_MONTH) {
                            sb.append(data[index]).append('|');
                        }
                        break;
                    case LENGTH:
                        if (index == Length.INDEX_SURVEY || index == Length.INDEX_YEAR || index == Length.INDEX_HAUL ||
                            index == Length.INDEX_SEX || index == Length.INDEX_MATURITY || index == Length.INDEX_LENGTH) {
                            sb.append(data[index]).append('|');
                        } else if (index == Length.INDEX_SPECIES) {
                            sb.append(project.getDisplaySpeciesText(data[index])).append('|');
                        }
                        break;
                    case STRATA:
                        if (index == Strata.INDEX_SURVEY || index == Strata.INDEX_STRATUM) {
                            sb.append(data[index]).append('|');
                        }
                        break;
                }

            }

        }
        return sb.toString();
    }

    /**
     * Effectue un calcul global, mais specific a chaque categorie.
     *
     * @param project  project
     * @param control  control
     * @param category category
     * @param progress progress monitor
     * @return error list for category (can be {@code null})
     */
    protected List<ControlError> validateCategorySpecific(Project project, Control control,
                                                          Category category, ProgressMonitor progress) {

        List<ControlError> validationErrors = null;
        switch (category) {
            case CATCH:
                validationErrors = validateCategorySpecificCatch(project, control, progress);
                break;
            case HAUL:
                validationErrors = validateCategorySpecificHaul(project, control, progress);
                break;
            case LENGTH:
                validationErrors = validateCategorySpecificLength(project, control, progress);
                break;
            case STRATA:
                validationErrors = validateCategorySpecificStrata(project, control, progress);
                break;
        }
        return validationErrors;
    }

    /**
     * Alerte si Somme(CAPTURES$Nombre par CAPTURES$Annee|Strate|Espece) < nobsmin.
     *
     * @param project  project
     * @param control  control
     * @param progress progress monitor (can be null)
     * @return error list
     */
    protected List<ControlError> validateCategorySpecificCatch(Project project,
                                                               Control control, ProgressMonitor progress) {

        List<ControlError> validationErrors = new ArrayList<ControlError>();

        int total = control.getCatch().size() - 1;
        if (progress != null) {
            progress.setText(t("coser.business.control.step.observation", t(Category.CATCH.getTranslationKey()), 0));
            progress.setTotal(total);
        }

        // "Campagne","Annee","Trait","Espece","Nombre","Poids"
        // regroupement par campagne/annee/trait
        Map<String, Double> nombreForKey = new HashMap<String, Double>();
        Map<String, String> firstLineForKey = new HashMap<String, String>();

        // parcours des elements
        Iterator<String[]> itTuple = control.getCatch().iterator(true);
        int lineIndex = 1; // skip header
        while (itTuple.hasNext()) {
            String[] tuple = itTuple.next();

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.observation", t(Category.CATCH.getTranslationKey()), progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            // compute key
            StringBuilder sb = new StringBuilder();
            for (int tupleIndex = 0; tupleIndex < tuple.length; ++tupleIndex) {
                if (tupleIndex == Catch.INDEX_YEAR || tupleIndex == Catch.INDEX_HAUL) {
                    sb.append(tuple[tupleIndex]).append(';');
                } else if (tupleIndex == Catch.INDEX_SPECIES) {
                    sb.append(project.getDisplaySpeciesText(tuple[tupleIndex])).append(';');
                }
            }

            // store number sum
            String nombreValue = tuple[Catch.INDEX_NUMBER];
            String lineNumber = tuple[AbstractDataEntity.INDEX_LINE];
            try {
                Double nombre = Double.valueOf(nombreValue);
                String key = sb.toString();
                if (nombreForKey.containsKey(key)) {
                    Double oldValue = nombreForKey.get(key);
                    Double newValue = oldValue + nombre;
                    nombreForKey.put(key, newValue);
                } else {
                    nombreForKey.put(key, nombre);
                    // sauvegarde la premiere ligne qui correspond a un regroupement de clé
                    firstLineForKey.put(key, lineNumber);
                }
            } catch (NumberFormatException ex) {
                // par trop grave, normalement les données deviennent
                // valide au fil de la validation
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse '" + nombreValue + "' as double");
                }
            }
        }

        // now look for invalid data
        for (Map.Entry<String, Double> sumObservation : nombreForKey.entrySet()) {
            String key = sumObservation.getKey();
            Double value = sumObservation.getValue();
            if (value < config.getControlNobsmin()) {

                String lineNumber = firstLineForKey.get(key);

                ControlError error = new ControlError();
                error.setCategory(Category.CATCH);
                error.setLevel(ValidationLevel.WARNING);
                error.addLineNumber(lineNumber);
                error.setMessage(t("coser.business.control.error.minObservationCount"));
                error.setDetailMessage(t("coser.business.control.error.minObservationCountDetail", key, value));
                validationErrors.add(error);
            }
        }

        return validationErrors;
    }

    /**
     * Detecte des différences entre les nombres dans captures
     * et les nombres dans taille.
     *
     * @param project  project
     * @param control  data container
     * @param progress progress (can be null)
     * @return errors
     * @see PublicationService#getCompareCatchLengthGraph(Project, AbstractDataContainer, Collection) for details
     * @see CoserBusinessConfig#getControlDiffCatchLength() for option
     */
    protected List<? extends ControlError> validateDiffCatchLength(Project project,
                                                                   Control control, ProgressMonitor progress) {

        int total = control.getCatch().size() - 1 + control.getLength().size() - 1;
        if (progress != null) {
            progress.setText(t("coser.business.control.step.diffCatchLength", 0));
            progress.setTotal(total);
        }

        List<SpeciesControlError> validationErrors = new ArrayList<SpeciesControlError>();

        // data necessary for compute
        SortedSet<String> setYear = new TreeSet<String>();
        Set<String> missingYearLengthSpecies = new HashSet<String>();

        // look for data (data summed over catch)
        Map<String, Map<String, Double>> catchForSpeciesYears = new HashMap<String, Map<String, Double>>();
        Iterator<String[]> itCatchData = control.getCatch().iterator(true);
        int lineIndex = 1; // skip header
        while (itCatchData.hasNext()) {

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.diffCatchLength", progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] tuple = itCatchData.next();
            String species = tuple[Catch.INDEX_SPECIES];

            Map<String, Double> speciesCatchForYears = catchForSpeciesYears.get(species);
            if (speciesCatchForYears == null) {
                speciesCatchForYears = new HashMap<String, Double>();
                catchForSpeciesYears.put(species, speciesCatchForYears);
            }

            String year = tuple[Catch.INDEX_YEAR];
            setYear.add(year);
            String nombreValue = tuple[Catch.INDEX_NUMBER];
            try {
                Double nombreDouble = Double.valueOf(nombreValue);

                if (speciesCatchForYears.containsKey(year)) {
                    Double oldValue = speciesCatchForYears.get(year);
                    Double newValue = oldValue + nombreDouble;
                    speciesCatchForYears.put(year, newValue);
                } else {
                    speciesCatchForYears.put(year, nombreDouble);
                }
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + nombreValue + " as double");
                }
            }
        }

        // look for data (data summed over length)
        Map<String, Map<String, Double>> lengthForSpeciesYears = new HashMap<String, Map<String, Double>>();
        Iterator<String[]> itLengthData = control.getLength().iterator(true);
        while (itLengthData.hasNext()) {

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.diffCatchLength", progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] tuple = itLengthData.next();
            String species = tuple[Length.INDEX_SPECIES];

            Map<String, Double> speciesLengthForYears = lengthForSpeciesYears.get(species);
            if (speciesLengthForYears == null) {
                speciesLengthForYears = new HashMap<String, Double>();
                lengthForSpeciesYears.put(species, speciesLengthForYears);
            }

            String year = tuple[Length.INDEX_YEAR];
            setYear.add(year);
            String nombreValue = tuple[Length.INDEX_NUMBER];
            try {
                Double nombreDouble = Double.valueOf(nombreValue);

                if (speciesLengthForYears.containsKey(year)) {
                    Double oldValue = speciesLengthForYears.get(year);
                    Double newValue = oldValue + nombreDouble;
                    speciesLengthForYears.put(year, newValue);
                } else {
                    speciesLengthForYears.put(year, nombreDouble);
                }
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + nombreValue + " as double");
                }
            }
        }

        // check for all species and years
        for (Map.Entry<String, Map<String, Double>> catchEntries : catchForSpeciesYears.entrySet()) {
            String species = catchEntries.getKey();
            Map<String, Double> catchNumbers = catchEntries.getValue();
            Map<String, Double> lengthNumbers = lengthForSpeciesYears.get(species);

            if (lengthNumbers == null) {
                // on suppose que cette erreur est détectée par un autre controle
                continue;
            }

            Iterator<String> itYears = setYear.iterator();
            while (itYears.hasNext()) {
                String year = itYears.next();

                Double catchNumber = catchNumbers.get(year);
                Double lengthNumber = lengthNumbers.get(year);
                if (catchNumber == null) {
                    catchNumber = 0.0; // marche pour NA
                }
                if (lengthNumber == null) {
                    lengthNumber = 0.0; // marche pour NA
                }

                // warning si taille = 0 et captures différent de 0
                // nomespece, année(s) avec absence de taille
                if (lengthNumber == 0 && catchNumber > 0 && !missingYearLengthSpecies.contains(species)) {
                    SpeciesControlError error = new SpeciesControlError();
                    error.setSpecies(species);
                    error.setLevel(ValidationLevel.WARNING);
                    error.setMessage(t("coser.business.control.error.diffCatchLengthMissingYear"));
                    error.setDetailMessage(t("coser.business.control.error.diffCatchLengthMissingYearDetail",
                                             project.getDisplaySpeciesText(species)));
                    validationErrors.add(error);
                    missingYearLengthSpecies.add(species);
                } else {
                    // diff entre 4 et 5 = (5-4) * 100 / 5
                    double min = Math.min(catchNumber, lengthNumber);
                    // si c'est 0, ou absence de catures, ce n'est pas une erreur
                    if (min > 0) {
                        double max = Math.max(catchNumber, lengthNumber);
                        double diff = (max - min) * 100 / max;

                        if (diff > config.getControlDiffCatchLength()) {
                            DiffCatchLengthControlError error = new DiffCatchLengthControlError();
                            error.setSpecies(species);
                            error.setLevel(ValidationLevel.WARNING);
                            error.setMessage(t("coser.business.control.error.diffCatchLength"));
                            error.setDetailMessage(t("coser.business.control.error.diffCatchLengthDetail",
                                                     project.getDisplaySpeciesText(species), year));
                            validationErrors.add(error);
                        }
                    }
                }
            }
        }

        // classer les warning sur les controle de différences capture/taille
        // par ordre alphabetique des noms d'especes
        Collections.sort(validationErrors);
        return validationErrors;
    }

    /**
     * Alerte si Somme(TAILLES$Nombre par TAILLES$Annee|Strate|Espece) < nobsmin
     *
     * Warning sur les tailles aberrantes par espèce:
     * <ul>
     * <li>premiere passe pour calculer l'écart type et la moyenne par espèce
     * <li>seconde passe pour détecter les valeurs abérentes (> ecart type)
     * </ul>
     *
     * L'ecart étant : racine( somme (x - moyenne)^2  / n)
     *
     * @param project  project
     * @param control  control
     * @param progress progress (can be null)
     * @return error list
     */
    protected List<ControlError> validateCategorySpecificLength(Project project,
                                                                Control control, ProgressMonitor progress) {
        List<ControlError> validationErrors = new ArrayList<ControlError>();

        int total = control.getLength().size() - 1;
        if (progress != null) {
            progress.setText(t("coser.business.control.step.observation", t(Category.LENGTH.getTranslationKey()), 0));
            progress.setTotal(total);
        }

        // "Campagne","Annee","Trait","Espece","Nombre","Poids"
        // regroupement par campagne/annee/trait
        Map<String, Double> nombreForKey = new HashMap<String, Double>();
        Map<String, String> firstLineForKey = new HashMap<String, String>();

        // Standard deviation
        Map<String, Double> lengthSumForSpecies = new HashMap<String, Double>();
        Map<String, Double> lengthCountForSpecies = new HashMap<String, Double>();

        // parcours des elements
        Iterator<String[]> itTuple = control.getLength().iterator(true);
        int lineIndex = 1; // skip header
        while (itTuple.hasNext()) {
            String[] tuple = itTuple.next();

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.observation", t(Category.LENGTH.getTranslationKey()), progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            // compute key
            StringBuilder sb = new StringBuilder();
            for (int tupleIndex = 0; tupleIndex < tuple.length; ++tupleIndex) {
                if (tupleIndex == Length.INDEX_YEAR || tupleIndex == Length.INDEX_HAUL ||
                    tupleIndex == Length.INDEX_SPECIES) {
                    sb.append(tuple[tupleIndex]).append(';');
                }
            }

            // store number sum
            String nombreValue = tuple[Length.INDEX_NUMBER];
            String lineNumber = tuple[AbstractDataEntity.INDEX_LINE];
            try {
                Double nombre = Double.valueOf(nombreValue);
                String key = sb.toString();
                if (nombreForKey.containsKey(key)) {
                    Double oldValue = nombreForKey.get(key);
                    Double newValue = oldValue + nombre;
                    nombreForKey.put(key, newValue);
                } else {
                    nombreForKey.put(key, nombre);
                    // sauvegarde la premiere ligne qui correspond a un regroupement de clé
                    firstLineForKey.put(key, lineNumber);
                }
            } catch (NumberFormatException ex) {
                // par trop grave, normalement les données deviennent
                // valide au fil de la validation
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + nombreValue + " as double");
                }
            }

            // store lenght for Standard deviation
            String lengthValue = tuple[Length.INDEX_LENGTH];
            try {
                Double nombre = Double.valueOf(lengthValue);
                String key = tuple[Length.INDEX_SPECIES];
                if (lengthSumForSpecies.containsKey(key)) {
                    Double oldValue = lengthSumForSpecies.get(key);
                    Double newValue = oldValue + nombre;
                    lengthSumForSpecies.put(key, newValue);
                    Double count = lengthCountForSpecies.get(key);
                    lengthCountForSpecies.put(key, count + 1);
                } else {
                    lengthSumForSpecies.put(key, nombre);
                    lengthCountForSpecies.put(key, 1d);
                }
            } catch (NumberFormatException ex) {
                // par trop grave, normalement les données deviennent
                // valide au fil de la validation
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + lengthValue + " as double");
                }
            }
        }

        // now look for invalid data (observations)
        for (Map.Entry<String, Double> sumObservation : nombreForKey.entrySet()) {
            String key = sumObservation.getKey();
            Double value = sumObservation.getValue();
            if (value < config.getControlNobsmin()) {

                String lineNumber = firstLineForKey.get(key);

                ControlError error = new ControlError();
                error.setCategory(Category.LENGTH);
                error.setLevel(ValidationLevel.WARNING);
                error.addLineNumber(lineNumber);
                error.setMessage(t("coser.business.control.error.minObservationCount"));
                error.setDetailMessage(t("coser.business.control.error.minObservationCountDetail", key, value));
                validationErrors.add(error);
            }
        }

        // recherche des valeurs abérrantes
        if (progress != null) {
            progress.nextStep();
            progress.setTotal(total * 2);
        }

        // Standard deviation : calcul de la moyenne
        Map<String, Double> avgForSpecies = new HashMap<String, Double>();
        for (Map.Entry<String, Double> entry : lengthSumForSpecies.entrySet()) {
            double avg = entry.getValue() / lengthCountForSpecies.get(entry.getKey());
            avgForSpecies.put(entry.getKey(), avg);
        }

        // Standard deviation : somme des variances au carré
        Map<String, Double> varianceSumForSpecies = new HashMap<String, Double>();
        itTuple = control.getLength().iterator(true);
        lineIndex = 1; // skip header
        while (itTuple.hasNext()) {

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 50.0);
                progress.setText(t("coser.business.control.step.lengthdeviation", t(Category.LENGTH.getTranslationKey()), progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] tuple = itTuple.next();
            // store lenght for Standard deviation
            String lengthValue = tuple[Length.INDEX_LENGTH];
            try {
                String key = tuple[Length.INDEX_SPECIES];
                Double nombre = Double.valueOf(lengthValue);
                Double value = Math.pow(nombre - avgForSpecies.get(key), 2);

                if (varianceSumForSpecies.containsKey(key)) {
                    Double oldValue = varianceSumForSpecies.get(key);
                    Double newValue = oldValue + value;
                    varianceSumForSpecies.put(key, newValue);
                } else {
                    varianceSumForSpecies.put(key, value);
                }
            } catch (NumberFormatException ex) {
                // par trop grave, normalement les données deviennent
                // valide au fil de la validation
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + lengthValue + " as double");
                }
            }
        }

        // Standard deviation : calcul de l'ecart type par espèce
        // racine( somme (x - moyenne)^2  / n)
        Map<String, Double> deviationForSpecies = new HashMap<String, Double>();
        for (Map.Entry<String, Double> entry : varianceSumForSpecies.entrySet()) {
            double avg = Math.sqrt(entry.getValue() / lengthCountForSpecies.get(entry.getKey()));
            deviationForSpecies.put(entry.getKey(), avg);
        }

        // Standard deviation : recherche des valeurs aberantes : 3 fois l'écart type
        itTuple = control.getLength().iterator(true);
        lineIndex = 1; // skip header
        while (itTuple.hasNext()) {

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 50.0 + 50.0);
                progress.setText(t("coser.business.control.step.lengthoutliers", t(Category.LENGTH.getTranslationKey()), progressPercent));
                progress.setCurrent(total + lineIndex);
                ++lineIndex;
            }

            String[] tuple = itTuple.next();
            // store lenght for Standard deviation
            String lengthValue = tuple[Length.INDEX_LENGTH];
            try {
                String species = tuple[Length.INDEX_SPECIES];
                Double nombre = Double.valueOf(lengthValue);
                Double avg = avgForSpecies.get(species);
                Double deviation = deviationForSpecies.get(species);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Species %s, avg=%f, deviation=%f, value=%f", species, avg, deviation, nombre));
                }

                if (Math.abs(nombre - avg) > deviation * config.getStandardDeviationToAverage()) {
                    String lineNumber = tuple[AbstractDataEntity.INDEX_LINE];

                    SpeciesLengthControlError error = new SpeciesLengthControlError();
                    error.setCategory(Category.LENGTH);
                    error.setLevel(ValidationLevel.WARNING);
                    error.addLineNumber(lineNumber);
                    error.setSpecies(species);
                    error.setMessage(t("coser.business.control.error.lengthOutliers", avg, deviation));
                    error.setDetailMessage(t("coser.business.control.error.lengthOutliersDetail",
                                             project.getDisplaySpeciesText(species), avg, deviation, lengthValue));
                    validationErrors.add(error);
                }
            } catch (NumberFormatException ex) {
                // par trop grave, normalement les données deviennent
                // valide au fil de la validation
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + lengthValue + " as double");
                }
            }
        }

        return validationErrors;
    }

    /**
     * Specific validation for Haul category.
     *
     * @param project
     * @param control  control
     * @param progress progress
     * @return {@code null}
     */
    protected List<ControlError> validateCategorySpecificHaul(Project project,
                                                              Control control, ProgressMonitor progress) {
        return null;
    }

    /**
     * Specific validation for Strata category.
     *
     * @param project  project
     * @param control  control
     * @param progress progress
     * @return {@code null}
     */
    protected List<ControlError> validateCategorySpecificStrata(Project project,
                                                                Control control, ProgressMonitor progress) {
        return null;
    }

    /**
     * Validation par croisement de fichiers.
     *
     * Les erreurs relevé ici porte sur plusieurs fichiers.
     *
     * L'ordre de verification de cohérence est :
     * Strates > Traits > Captures > Taille
     *
     * @param control  control
     * @param progress progress
     * @return error list
     */
    protected List<ControlError> validationCrossFiles(Project project, Control control,
                                                      ProgressMonitor progress) {

        int total = control.getCatch().size() + control.getHaul().size()
                    + control.getStrata().size() + control.getLength().size() - 4;
        if (progress != null) {
            progress.setText(t("coser.business.control.step.crossFileChech", 0));
            progress.setTotal(total);
            progress.setCurrent(0);
        }

        List<ControlError> crossFilesErrors = new ArrayList<ControlError>();

        // get all iterators
        Iterator<String[]> itReftax = project.getRefTaxSpecies().iterator(true);
        Iterator<String[]> itCatch = control.getCatch().iterator(true);
        Iterator<String[]> itLength = control.getLength().iterator(true);
        Iterator<String[]> itStrata = control.getStrata().iterator(true);
        Iterator<String[]> itHaul = control.getHaul().iterator(true);

        // declare all necessary data
        // collections filled during exploration and checked at end of method
        Map<String, Integer> refTaxSpecies = new HashMap<String, Integer>();
        Set<String> catchYear = new HashSet<String>(); // annee du fichier catch
        Set<String> lengthYear = new HashSet<String>(); // annee du fichier taille
        Set<String> haulYear = new HashSet<String>(); // annee du fichier trait
        Set<String> surveyNames = new HashSet<String>(); // nom des campagnes (tous fichier)
        Set<String> strataStrataNames = new HashSet<String>(); // strates du fichier strates
        Set<String> haulStrataNames = new HashSet<String>(); // strates du fichier traits
        Set<String> haulHaulNames = new HashSet<String>(); // traits du fichier traits
        Set<String> catchHaulNames = new HashSet<String>(); // traits du fichier capture
        Set<String> lengthHaulNames = new HashSet<String>(); // traits du fichier taille
        Set<String> catchSpeciesNames = new HashSet<String>(); // especes du fichier catpures
        Set<String> lengthSpeciesNames = new HashSet<String>(); // especes du fichier tailles
        Set<String> lengthYearHaulSpecies = new HashSet<String>(); // couple annee/trait/espece du fichier taille
        Set<String> catchYearHaulSpecies = new HashSet<String>(); // couple annee/trait/espece du fichier capture
        Set<String> haulYearHaul = new HashSet<String>(); // couple annee/trait du fichier trait
        Set<String> lengthYearHaul = new HashSet<String>(); // couple annee/trait du fichier taille
        Set<String> catchYearHaul = new HashSet<String>(); // couple annee/trait du fichier capture
        Map<String, Integer[]> specyTypes = new HashMap<String, Integer[]>();

        // parcourt de toutes les données
        while (itReftax.hasNext()) {
            String[] tuple = itReftax.next();
            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String speciesId = null;
            switch (project.getStorageSpeciesType()) {
                case C_PERM:
                    speciesId = tuple[0];
                    break;
                case C_Valide:
                    speciesId = tuple[3];
                    break;
                case L_Valide:
                    speciesId = tuple[4];
                    break;
            }
            Integer iNumSys = Integer.valueOf(tuple[1]);
            refTaxSpecies.put(speciesId, iNumSys);
        }

        // parcourt du fichier des types de données
        Iterator<String[]> itTypeSpecies = project.getTypeEspeces().iterator(true);
        while (itTypeSpecies.hasNext()) {
            // "Types";"Commentaire";"NumSys min";"NumSys max","Code"
            String[] tuple = itTypeSpecies.next();
            String specyType = tuple[0];

            Integer iMinNumSys = Integer.valueOf(tuple[2]);
            Integer iMaxNumSys = Integer.valueOf(tuple[3]);
            specyTypes.put(specyType, new Integer[]{iMinNumSys, iMaxNumSys});
        }

        int lineIndex = 1; // skip header

        // strata
        while (itStrata.hasNext()) {
            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.crossFileChech", progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] strataData = itStrata.next();
            surveyNames.add(strataData[Strata.INDEX_SURVEY]);
            strataStrataNames.add(strataData[Strata.INDEX_STRATUM]);
        }

        // haul
        while (itHaul.hasNext()) {
            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.crossFileChech", progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] haulData = itHaul.next();
            surveyNames.add(haulData[Haul.INDEX_SURVEY]);
            haulYear.add(haulData[Haul.INDEX_YEAR]);
            haulYearHaul.add(haulData[Haul.INDEX_YEAR] + "|" + haulData[Haul.INDEX_HAUL]);
            haulStrataNames.add(haulData[Haul.INDEX_STRATUM]);
            haulHaulNames.add(haulData[Haul.INDEX_HAUL]);
        }

        // catch
        while (itCatch.hasNext()) {

            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.crossFileChech", progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] catchData = itCatch.next();
            String species = catchData[Catch.INDEX_SPECIES];
            surveyNames.add(catchData[Catch.INDEX_SURVEY]);
            catchYear.add(catchData[Catch.INDEX_YEAR]);
            catchYearHaulSpecies.add(catchData[Catch.INDEX_YEAR] + "|" +
                                     catchData[Catch.INDEX_HAUL] + "|" + project.getDisplaySpeciesText(species));
            catchYearHaul.add(catchData[Catch.INDEX_YEAR] + "|" + catchData[Catch.INDEX_HAUL]);
            catchHaulNames.add(catchData[Catch.INDEX_HAUL]);
            catchSpeciesNames.add(species);

            // Contrôle des noms d'espèces dans fichiers CAPTURES et TAILLES qui
            // doivent être dans le référentiel "reftax": CAPTURES$Espece et
            // TAILLES$Espece doivent exister dans REFTAX$C_VALIDE 
            if (!refTaxSpecies.containsKey(catchData[Catch.INDEX_SPECIES])) {
                ControlError error = new ControlError();
                error.setCategory(Category.CATCH);
                error.addLineNumber(catchData[Catch.INDEX_LINE]);
                error.setLevel(ValidationLevel.ERROR);
                error.setMessage(t("coser.business.control.error.nonExistantSpecies"));
                error.setDetailMessage(t("coser.business.control.error.nonExistantSpeciesDetail", catchData[Catch.INDEX_SPECIES]));
                crossFilesErrors.add(error);
            }
        }

        // length
        while (itLength.hasNext()) {
            // update progress
            if (progress != null) {
                int progressPercent = (int) ((double) lineIndex / (double) total * 100.0);
                progress.setText(t("coser.business.control.step.crossFileChech", progressPercent));
                progress.setCurrent(lineIndex);
                ++lineIndex;
            }

            String[] lengthData = itLength.next();
            String species = lengthData[Length.INDEX_SPECIES];
            surveyNames.add(lengthData[Length.INDEX_SURVEY]);
            lengthYear.add(lengthData[Length.INDEX_YEAR]);
            lengthYearHaulSpecies.add(lengthData[Length.INDEX_YEAR] + "|" +
                                      lengthData[Length.INDEX_HAUL] + "|" + project.getDisplaySpeciesText(species));
            lengthYearHaul.add(lengthData[Length.INDEX_YEAR] + "|" + lengthData[Length.INDEX_HAUL]);
            lengthHaulNames.add(lengthData[Length.INDEX_HAUL]);
            lengthSpeciesNames.add(species);

            // Contrôle des noms d'espèces dans fichiers CAPTURES et TAILLES qui
            // doivent être dans le référentiel "reftax": CAPTURES$Espece et
            // TAILLES$Espece doivent exister dans REFTAX$C_VALIDE 
            if (!refTaxSpecies.containsKey(species)) {
                ControlError error = new ControlError();
                error.setCategory(Category.LENGTH);
                error.addLineNumber(lengthData[Length.INDEX_LINE]);
                error.setLevel(ValidationLevel.ERROR);
                error.setMessage(t("coser.business.control.error.nonExistantSpecies"));
                error.setDetailMessage(t("coser.business.control.error.nonExistantSpeciesDetail", lengthData[Length.INDEX_SPECIES]));
                crossFilesErrors.add(error);
            } else {
                // ne doit être fait que si l'escepe existe

                // Contrôle sur le champ TAILLES$Longueur :
                // pour les poissons, vérifier que la première décimale n'est que 0 ou 5
                // Décision : Warning si différent de 0 ou 5 -> demander la vérification
                // de l'unité et/ou du pas.
                // Rappeler que le champ Longueur, quelque soit les espèces, doit être en
                // cm et inviter l'utilisateur à vérifier toutes ces données de longueur

                Integer speciesNumSys = refTaxSpecies.get(species);
                for (Map.Entry<String, Integer[]> speciesTypeEntry : specyTypes.entrySet()) {
                    // seulement pour les poissons
                    String speciesType = speciesTypeEntry.getKey();
                    Integer[] bound = speciesTypeEntry.getValue();
                    // si le type courant et le type de la config
                    // et que l'espece courant est de ce type (bornes)
                    // et que la longueur n'est pas valide
                    // alors c'est une erreur
                    if (speciesType.equals(config.getControlTypeFish()) &&
                        speciesNumSys >= bound[0] && speciesNumSys <= bound[1] &&
                        !isValidFishLength(lengthData[Length.INDEX_LENGTH])) {
                        ControlError error = new ControlError();
                        error.setCategory(Category.LENGTH);
                        error.addLineNumber(lengthData[Length.INDEX_LINE]);
                        error.setLevel(ValidationLevel.WARNING);
                        error.setMessage(t("coser.business.control.error.invalidLengthLengthStep"));
                        error.setDetailMessage(t("coser.business.control.error.invalidLengthLengthStepDetail",
                                                 lengthData[Length.INDEX_LENGTH], project.getDisplaySpeciesText(species)));
                        error.setTipMessage(t("coser.business.control.error.invalidLengthLengthStepTip"));
                        crossFilesErrors.add(error);
                    }
                }
            }
        }

        // Vérifier que les mêmes années sont présentes dans fichiers captures
        // traits et tailles: CAPTURES$Annee, TRAITS$Annee, TAILLES$Annee.
        if (!catchYear.equals(lengthYear) || !haulYear.equals(lengthYear)) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.yearsNotEquals"));
            //error.setDetailMessage(t("coser.business.control.error.yearsNotEquals"));
            crossFilesErrors.add(error);
        }

        // Vérifier que le nom de la campagne est le même dans tous les fichiers *$Campagne.
        if (surveyNames.size() != 1) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.surveyNotEquals"));
            //error.setDetailMessage(t("coser.business.control.error.surveyNotEquals"));
            crossFilesErrors.add(error);
        }

        // Vérifier que pour chaque espèce présent dans un trait d'une année
        // dans le fichier tailles (noté Annee|Trait|Espece) il y a une donnée
        // dans le fichier captures (le contraire n'est pas vraie)
        Collection<String> missingSpeciesCatchTuples = CollectionUtils.subtract(lengthYearHaulSpecies, catchYearHaulSpecies);
        for (String missingSpeciesCatchTuple : missingSpeciesCatchTuples) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.missingYearHaulSpeciesForCatchData"));
            error.setDetailMessage(t("coser.business.control.error.missingYearHaulSpeciesForCatchDataDetail", missingSpeciesCatchTuple));
            error.setTipMessage(t("coser.business.control.error.missingYearHaulSpeciesForCatchDataTip"));
            crossFilesErrors.add(error);
        }

        // Vérifier que pour chaque trait présent une année (noté Annee|Trait)
        // dans les fichiers tailles et capt il y a une donnée dans le fichier
        // traits
        Collection<String> missingHaulLengthTuples = CollectionUtils.subtract(lengthYearHaul, haulYearHaul);
        for (String missingHaulLengthTuple : missingHaulLengthTuples) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.missingYearHaulForLengthData"));
            error.setDetailMessage(t("coser.business.control.error.missingYearHaulForLengthDataDetail", missingHaulLengthTuple));
            error.setTipMessage(t("coser.business.control.error.missingYearHaulForLengthDataTip"));
            crossFilesErrors.add(error);
        }

        // Vérifier que pour chaque trait présent une année (noté Annee|Trait)
        // dans les fichiers tailles et capt il y a une donnée dans le fichier
        // traits
        Collection<String> missingHaulCatchTuples = CollectionUtils.subtract(catchYearHaul, haulYearHaul);
        for (String missingHaulCatchTuple : missingHaulCatchTuples) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.missingYearHaulForCatchData"));
            error.setDetailMessage(t("coser.business.control.error.missingYearHaulForCatchDataDetail", missingHaulCatchTuple));
            error.setTipMessage(t("coser.business.control.error.missingYearHaulForCatchDataTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // strates de traits doivent etre dans strates de strates (fatal)
        Collection<String> missingStrataStrataNames = CollectionUtils.subtract(haulStrataNames, strataStrataNames);
        for (String missingStrataStrataName : missingStrataStrataNames) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.missingStrataStrataFromHaul"));
            error.setDetailMessage(t("coser.business.control.error.missingStrataStrataFromHaulDetail", missingStrataStrataName));
            error.setTipMessage(t("coser.business.control.error.missingStrataStrataFromHaulTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // strates de strates doivent etre dans strates de traits (warning)
        Collection<String> missingHaulStrataStrataNames = CollectionUtils.subtract(strataStrataNames, haulStrataNames);
        for (String missingHaulStrataStrataName : missingHaulStrataStrataNames) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.WARNING);
            error.setMessage(t("coser.business.control.error.missingHaulStrataFromStrata"));
            error.setDetailMessage(t("coser.business.control.error.missingHaulStrataFromStrataDetail", missingHaulStrataStrataName));
            error.setTipMessage(t("coser.business.control.error.missingHaulStrataFromStrataTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // traits de captures doivent etre dans traits de traits (fatal)
        Collection<String> missingHaulHaulNames = CollectionUtils.subtract(catchHaulNames, haulHaulNames);
        for (String missingHaulHaulName : missingHaulHaulNames) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.missingHaulHaulFromCatch"));
            error.setDetailMessage(t("coser.business.control.error.missingHaulHaulFromCatchDetail", missingHaulHaulName));
            error.setTipMessage(t("coser.business.control.error.missingHaulHaulFromCatchTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // traits de traits doivent etre dans traits de captures (warning)
        Collection<String> missingCatchHaulNames = CollectionUtils.subtract(haulHaulNames, catchHaulNames);
        for (String missingCatchHaulName : missingCatchHaulNames) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.WARNING);
            error.setMessage(t("coser.business.control.error.missingCatchHaulFromHaul"));
            error.setDetailMessage(t("coser.business.control.error.missingCatchHaulFromHaulDetail", missingCatchHaulName));
            error.setTipMessage(t("coser.business.control.error.missingCatchHaulFromHaulTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // traits de tailles doivent etre dans traits de captures (fatal)
        Collection<String> missingCatchHaulForLengthNames = CollectionUtils.subtract(lengthHaulNames, catchHaulNames);
        for (String missingCatchHaulForLengthName : missingCatchHaulForLengthNames) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setMessage(t("coser.business.control.error.missingCatchHaulFromLength"));
            error.setDetailMessage(t("coser.business.control.error.missingCatchHaulFromLengthDetail", missingCatchHaulForLengthName));
            error.setTipMessage(t("coser.business.control.error.missingCatchHaulFromLengthTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // traits de captures doivent etre dans traits de tailles (warning)
        Collection<String> missingLengthHaulNames = CollectionUtils.subtract(catchHaulNames, lengthHaulNames);
        for (String missingLengthHaulName : missingLengthHaulNames) {
            ControlError error = new ControlError();
            error.setLevel(ValidationLevel.WARNING);
            error.setMessage(t("coser.business.control.error.missingLengthHaulFromCatch"));
            error.setDetailMessage(t("coser.business.control.error.missingLengthHaulFromCatchDetail", missingLengthHaulName));
            error.setTipMessage(t("coser.business.control.error.missingLengthHaulFromCatchTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // especes de tailles doivent etre dans especes de captures (fatal)
        Collection<String> missingCatchSpeciesNames = CollectionUtils.subtract(lengthSpeciesNames, catchSpeciesNames);
        for (String missingCatchSpeciesName : missingCatchSpeciesNames) {
            SpeciesControlError error = new SpeciesControlError();
            error.setLevel(ValidationLevel.FATAL);
            error.setSpecies(missingCatchSpeciesName);
            error.setMessage(t("coser.business.control.error.missingCatchSpeciesFromLength"));
            error.setDetailMessage(t("coser.business.control.error.missingCatchSpeciesFromLengthDetail",
                                     project.getDisplaySpeciesText(missingCatchSpeciesName)));
            error.setTipMessage(t("coser.business.control.error.missingCatchSpeciesFromLengthTip"));
            crossFilesErrors.add(error);
        }

        // Donc tout ce qui est présent dans TAILLES doit être décrit dans CAPTURES
        // qui doit être décrit dans TRAITS qui doit être décrit dans STRATES.
        // especes de captures doivent etre dans especes de tailles (warning)
        Collection<String> missingLengthSpeciesNames = CollectionUtils.subtract(catchSpeciesNames, lengthSpeciesNames);
        for (String missingLengthSpeciesName : missingLengthSpeciesNames) {
            SpeciesControlError error = new SpeciesControlError();
            error.setLevel(ValidationLevel.WARNING);
            error.setSpecies(missingLengthSpeciesName);
            error.setMessage(t("coser.business.control.error.missingLengthSpeciesFromCatch"));
            error.setDetailMessage(t("coser.business.control.error.missingLengthSpeciesFromCatchDetail",
                                     project.getDisplaySpeciesText(missingLengthSpeciesName)));
            error.setTipMessage(t("coser.business.control.error.missingLengthSpeciesFromCatchTip"));
            crossFilesErrors.add(error);
        }

        return crossFilesErrors;
    }

    /**
     * Return {@code true} if given length is valid length (cm or half cm).
     *
     * @param length lengthto test
     * @return {@code true} if given length is valid length (cm or half cm)
     */
    protected boolean isValidFishLength(String length) {
        Matcher mLength = FISH_LENGTH_PATTERN.matcher(length);
        return mLength.matches();
    }
}
package fr.ifremer.coser.services;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.bean.EchoBaseProject;
import fr.ifremer.coser.bean.GlobalResult;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.result.repository.echobase.EchoBaseResultRepositoryType;
import fr.ifremer.coser.result.repository.legacy.LegacyResultRepositoryType;
import fr.ifremer.coser.util.InputStreamKnownSizeBody;
import fr.ifremer.coser.util.ProgressMonitor;
import fr.ifremer.coser.util.ProgressStream;
import fr.ifremer.coser.util.io.MultipleFileFilter;
import fr.ifremer.coser.util.io.OneEchobaseFileFilter;
import fr.ifremer.coser.util.io.OneRSufiResultFileFilter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Service to be used only by the ui client to extract and publish results.
 *
 * Created on 3/17/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ClientResultService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ClientResultService.class);

    protected CoserBusinessConfig config;

    protected ProjectService projectService;

    public ClientResultService(CoserBusinessConfig config) {
        this.config = config;
        this.projectService = new ProjectService(config);
    }

    // --------------------------------------------------------------------- //
    // --- Public API ------------------------------------------------------ //
    // --------------------------------------------------------------------- //

    /**
     * Retourne tous les projets qui ont des résultats.
     *
     * De la forme d'une liste de de path (à la tree path) :
     * ProjetName/SelectionName/ResultName
     *
     * @param beginDate           begin date (can be null)
     * @param endDate             end date (can be null)
     * @param onlyPubliableResult select only publiable results
     * @return results paths
     * @throws CoserBusinessException
     */
    public List<GlobalResult> findAllProjectWithResult(Date beginDate,
                                                       Date endDate,
                                                       boolean onlyPubliableResult) throws CoserBusinessException {
        List<GlobalResult> results = Lists.newArrayList();

        File rsufiProjectsDirectory = config.getRSufiProjectsDirectory();
        File[] rsufiProjects = rsufiProjectsDirectory.listFiles();
        if (rsufiProjects != null) {
            for (File existingProject : rsufiProjects) {
                if (existingProject.isDirectory()) {

                    Collection<GlobalResult> rsufiResults =
                            getRsufiResults(existingProject,
                                            beginDate,
                                            endDate,
                                            onlyPubliableResult);
                    results.addAll(rsufiResults);
                }
            }
        }

        File echoBaseProjectsDirectory = config.getEchoBaseProjectsDirectory();
        File[] echoBaseProjects = echoBaseProjectsDirectory.listFiles();
        if (echoBaseProjects != null) {
            for (File existingProject : echoBaseProjects) {
                if (existingProject.isDirectory()) {
                    Collection<GlobalResult> echoBaseResults =
                            getEchoBaseResults(existingProject,
                                               beginDate,
                                               endDate,
                                               onlyPubliableResult);
                    results.addAll(echoBaseResults);
                }
            }
        }

        return results;
    }

    /**
     * Upload user selected result to coser web front-end using common http
     * client.
     *
     * TODO remove les 4 listes s'il y a mieux.
     *
     * @param selectedResults    selected result (collection of project/selection/rsufiresult)
     * @param indicatorsResults  results selected as indicator results
     * @param mapResults         results selected as map result
     * @param publishDataResults results selected as results published with data
     * @param login              remote admin login
     * @param password           remote admin password
     * @param progress           progress monitor
     * @return upload error status or {@code null} if no error
     * @throws CoserBusinessException
     */
    public String performResultUpload(Collection<GlobalResult> selectedResults,
                                      Collection<GlobalResult> indicatorsResults,
                                      Collection<GlobalResult> mapResults,
                                      Collection<GlobalResult> publishDataResults,
                                      String login,
                                      String password,
                                      ProgressMonitor progress) throws CoserBusinessException {


        // first copy prepare directory with only necessary data
        // ie project with only selected selections
        // and selection with only selected results

        progress.setCurrent(0);
        progress.setText(t("coser.business.uploadresult.modifyResultOptions"));
        modifyRSufiResults(selectedResults, indicatorsResults, mapResults, publishDataResults);

        progress.setText(t("coser.business.uploadresult.checkcollision"));
        checkDataCollision(selectedResults);

        progress.setText(t("coser.business.uploadresult.preparezip"));

        // default extract to temp directory with data sources
        File prepareZip = performResultExtract(selectedResults, publishDataResults, null);

        progress.setText(t("coser.business.uploadresult.sendzip"));
        progress.setTotal((int) prepareZip.length());

        // then upload zip file to website
        try {
            MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
            multipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            multipartEntityBuilder.setCharset(Consts.UTF_8);

            // login/password param (password encoded)
            multipartEntityBuilder.addBinaryBody("login", login.getBytes());
            String sha1password = StringUtil.encodeSHA1(password);
            multipartEntityBuilder.addBinaryBody("sha1Password", sha1password.getBytes());

            // file param
            ProgressStream stream = new ProgressStream(new FileInputStream(prepareZip), progress);
            InputStreamKnownSizeBody fileBody = new InputStreamKnownSizeBody(stream, prepareZip.length(),
                                                                             "application/zip", prepareZip.getName());
            multipartEntityBuilder.addPart("resultFile", fileBody);

            HttpPost httppost = new HttpPost(config.getWebUploadURL());
            httppost.setEntity(multipartEntityBuilder.build());

            if (log.isInfoEnabled()) {
                log.info("Uploading " + prepareZip + " to " + httppost.getURI());
            }

            HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
            HttpResponse response = httpClientBuilder.build().execute(httppost);

            String uploadStatus = null;

            if (response.getStatusLine().getStatusCode() != 200) {
                uploadStatus = response.getStatusLine().getReasonPhrase();
            }

            FileUtils.forceDelete(prepareZip);

            return uploadStatus;

        } catch (ClientProtocolException ex) {
            throw new CoserBusinessException("Can't upload file", ex);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't upload file", ex);
        }
    }

    // --------------------------------------------------------------------- //
    // --- Internal methods ------------------------------------------------ //
    // --------------------------------------------------------------------- //

    protected Collection<GlobalResult> getEchoBaseResults(File existingProject,
                                                          Date beginDate,
                                                          Date endDate,
                                                          boolean onlyPubliableResult) {
        // echobase project
        EchoBaseProject echoBaseProject = new EchoBaseProject(existingProject);
        try {
            echoBaseProject.load();
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not load echobase project", e);
        }
        GlobalResult result = new GlobalResult(echoBaseProject);
        boolean candidate = isCandidateResult(result, beginDate, endDate, onlyPubliableResult);
        List<GlobalResult> results = Lists.newArrayList();
        if (candidate) {
            results.add(result);
        }
        return results;
    }

    protected Collection<GlobalResult> getRsufiResults(File existingProject,
                                                       Date beginDate,
                                                       Date endDate,
                                                       boolean onlyPubliableResult) {
        List<GlobalResult> results = Lists.newArrayList();

        // rsufi project
        String projectName = existingProject.getName();
        Project p = new Project(projectName);

        // loop on selections
        File selectionsDirectory = new File(existingProject, CoserConstants.STORAGE_SELECTION_DIRECTORY);
        File[] selections = selectionsDirectory.listFiles();
        if (selections != null) {
            for (File existingSelection : selections) {
                if (existingSelection.isDirectory()) {
                    String selectionName = existingSelection.getName();
                    Selection s = new Selection(selectionName);

                    // loop on result
                    File rsufisDirectory = new File(existingSelection, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                    File[] rSufiResults = rsufisDirectory.listFiles();
                    if (rSufiResults != null) {
                        for (File rSufiResult : rSufiResults) {
                            if (rSufiResult.isDirectory()) {
                                RSufiResult r;
                                try {
                                    r = projectService.getRSufiResult(rSufiResult);
                                } catch (CoserBusinessException e) {
                                    throw new CoserTechnicalException("Could not load rsufi result", e);
                                }

                                RSufiResultPath path = new RSufiResultPath(p, s, r);
                                GlobalResult result = new GlobalResult(path);
                                boolean candidate = isCandidateResult(result, beginDate, endDate, onlyPubliableResult);
                                if (candidate) {
                                    results.add(result);
                                }
                            }
                        }
                    }
                }
            }
        }
        return results;
    }

    /**
     * Test if result is valid with filtering.
     *
     * @param rsufiResult         rsufi result to test
     * @param beginDate           begin date (can be null)
     * @param endDate             end date (can be null)
     * @param onlyPubliableResult select only publiable results
     * @return if result is valid candidate
     */
    protected boolean isCandidateResult(GlobalResult rsufiResult, Date beginDate,
                                        Date endDate, boolean onlyPubliableResult) {

        boolean result = true;

        if (beginDate != null) {
            result = rsufiResult.getCreationDate().compareTo(beginDate) >= 0;
        }

        if (endDate != null) {
            result &= rsufiResult.getCreationDate().compareTo(endDate) <= 0;
        }

        if (onlyPubliableResult) {
            result &= rsufiResult.isPubliableResult();
        }

        return result;
    }

    /**
     * Modifie les types et options de certains résultats rsufi (map result,
     * data sources result).
     *
     * @param selectedResults    selected result (collection of project/selection/rsufiresult)
     * @param indicatorsResults  results selected as indicator results
     * @param mapResults         map results
     * @param publishDataResults publish data results
     */
    protected void modifyRSufiResults(Collection<GlobalResult> selectedResults,
                                      Collection<GlobalResult> indicatorsResults,
                                      Collection<GlobalResult> mapResults,
                                      Collection<GlobalResult> publishDataResults) {

        // TODO echatellier 20110117 revoir ce code

        // attention, il faut sauver tout les resultats, sinon, les
        // decochage de type map / publish result ne seront pas pris en compte

        // reset type map and data source for all
        for (GlobalResult selectedResult : selectedResults) {
            selectedResult.setIndicatorsResult(false);
            selectedResult.setMapsResult(false);
            selectedResult.setDataAllowed(false);
        }

        // set map type
        for (GlobalResult indicatorsResult : indicatorsResults) {
            indicatorsResult.setIndicatorsResult(true);
        }

        // set map type
        for (GlobalResult mapResult : mapResults) {
            mapResult.setMapsResult(true);
        }

        // set data type
        for (GlobalResult publishDataResult : publishDataResults) {
            publishDataResult.setDataAllowed(true);
        }

        // save all selected results
        for (GlobalResult selectedGlobalResult : selectedResults) {

            if (selectedGlobalResult.isRsufi()) {

                RSufiResultPath selectedResult = selectedGlobalResult.getRsufiProject();
                Project project = selectedResult.getProject();
                Selection selection = selectedResult.getSelection();
                RSufiResult rsufiResult = selectedResult.getRsufiResult();

                File projectDirectory = new File(config.getRSufiProjectsDirectory(), project.getName());
                File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                File selectionDirectory = new File(selectionsDirectory, selection.getName());
                File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                File resultDirectory = new File(resultsDirectory, rsufiResult.getName());

                try {
                    projectService.saveRSufiResult(resultDirectory, rsufiResult);
                } catch (CoserBusinessException e) {
                    throw new CoserTechnicalException("Could not save rsufi results", e);
                }
            } else {
                EchoBaseProject selectedResult = selectedGlobalResult.getEchobaseProject();
                try {
                    selectedResult.save();
                } catch (IOException e) {
                    throw new CoserTechnicalException("Could not save echobaseProject", e);
                }
            }

        }
    }

    /**
     * Met à jour les fichiers de propriétés des resultats (maps, dataSource)
     * and check for duplicated couple (zoneid/resulttype (map) upload).
     *
     * @param selectedResults result id to check
     */
    protected void checkDataCollision(Collection<GlobalResult> selectedResults) {

        Collection<String> resultZoneTypeIds = Lists.newArrayList();

        for (GlobalResult selectedResult : selectedResults) {

            String resultZoneTypeId;

            if (selectedResult.isRsufi()) {
                RSufiResult rsufiResult = selectedResult.getRsufiProject().getRsufiResult();

                // on creer une clé composé pour l'id du resultat
                resultZoneTypeId = rsufiResult.getZone() + String.valueOf(rsufiResult.isMapsResult());
            } else {
                EchoBaseProject echobaseProject = selectedResult.getEchobaseProject();
                resultZoneTypeId = echobaseProject.getZoneName() + String.valueOf(true);
            }
            if (resultZoneTypeIds.contains(resultZoneTypeId)) {
                throw new CoserTechnicalException(t("coser.business.resultupload.duplicatedresult2",
                                                    selectedResult.getName(),
                                                    selectedResult.getZone()));
            } else {
                resultZoneTypeIds.add(resultZoneTypeId);
            }
        }
    }

    /**
     * Extract directory to custom directory.
     *
     * @param selectedResults    selected result paths
     * @param extractDirectory   extract directory (can be null)
     * @param publishDataResults result paths flaged with results export
     * @return extracted file (no automatically deleted)
     */
    public File performResultExtract(Collection<GlobalResult> selectedResults,
                                     Collection<GlobalResult> publishDataResults,
                                     File extractDirectory) {
        File result;
        try {

            // create zip file name if not random name
            if (extractDirectory == null) {
                result = File.createTempFile("Coserextract-", ".zip");
            } else {
                // extract in a specific directory
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                String zipName = "Coserextract" + dateFormat.format(new Date()) + ".zip";
                result = new File(extractDirectory, zipName);
            }

            // where to extract files to zip
            File tempExtractDirectory = FileUtil.createTempDirectory("CoserExtractTemp", "dummy");

            // copy rsufi results
            performResultExtractForRSufi(tempExtractDirectory,
                                         selectedResults,
                                         publishDataResults);

            // copy echobase results
            performResultExtractForEchoBase(tempExtractDirectory,
                                            selectedResults,
                                            publishDataResults);

            // get all files to compress
            Collection<File> files = FileUtil.getFilteredElements(tempExtractDirectory,
                                                                  FileFilterUtils.trueFileFilter(),
                                                                  true);

            // create zip file
            ZipUtil.compressFiles(result, tempExtractDirectory, files, false);

            // delete temp files
            FileUtils.deleteDirectory(tempExtractDirectory);

        } catch (IOException e) {
            throw new CoserTechnicalException("Can't prepare upload data", e);
        }
        return result;
    }

    /**
     * Extract directory to custom directory.
     *
     * @param extractDirectory   extract directory
     * @param selectedResults    selected result paths
     * @param publishDataResults result paths flaged with results export
     */
    protected void performResultExtractForRSufi(File extractDirectory,
                                                Collection<GlobalResult> selectedResults,
                                                Collection<GlobalResult> publishDataResults) {

        Preconditions.checkNotNull(extractDirectory);
        try {

            File projectsDirectory = config.getRSufiProjectsDirectory();

            File rsufiDirectory = new File(extractDirectory, LegacyResultRepositoryType.ID);
            FileUtils.forceMkdir(rsufiDirectory);

            // copy selectively all data to target directory
            MultipleFileFilter mFileFilters = new MultipleFileFilter();
            for (GlobalResult globalProject : selectedResults) {
                if (globalProject.isRsufi()) {
                    RSufiResultPath path = globalProject.getRsufiProject();
                    // load projet, needed to know source data file name
                    Project project = path.getProject();
                    project = projectService.openProject(project.getName());

                    OneRSufiResultFileFilter oneRFF = new OneRSufiResultFileFilter(
                            projectsDirectory,
                            project,
                            path.getSelection(),
                            path.getRsufiResult(),
                            publishDataResults.contains(globalProject));
                    mFileFilters.add(oneRFF);
                }
            }

            // get all files to compress
            Collection<File> files = FileUtil.getFilteredElements(projectsDirectory,
                                                                  mFileFilters,
                                                                  true);

            // create a temp zip file
            File tempZip = File.createTempFile("Coserextractrsufi-", ".zip");
            ZipUtil.compressFiles(tempZip, projectsDirectory, files, false);
            // unzip it to correct directory
            ZipUtil.uncompress(tempZip, rsufiDirectory);
            // delete temp zip
            FileUtils.forceDelete(tempZip);
        } catch (CoserBusinessException e) {
            throw new CoserTechnicalException("Can't prepare upload data", e);
        } catch (IOException e) {
            throw new CoserTechnicalException("Can't prepare upload data", e);
        }
    }

    /**
     * Extract directory to custom directory.
     *
     * @param selectedResults    selected result paths
     * @param publishDataResults result paths flaged with results export
     */
    protected void performResultExtractForEchoBase(File extractDirectory,
                                                   Collection<GlobalResult> selectedResults,
                                                   Collection<GlobalResult> publishDataResults) {
        Preconditions.checkNotNull(extractDirectory);
        try {

            File projectsDirectory = config.getEchoBaseProjectsDirectory();

            File echobaseDirectory = new File(extractDirectory, EchoBaseResultRepositoryType.ID);
            FileUtils.forceMkdir(echobaseDirectory);

            // creation du filter
            MultipleFileFilter mFileFilters = new MultipleFileFilter();
            for (GlobalResult globalProject : selectedResults) {
                if (globalProject.isEchoBase()) {
                    EchoBaseProject echobaseProject = globalProject.getEchobaseProject();
                    OneEchobaseFileFilter fileFilter =
                            new OneEchobaseFileFilter(echobaseProject,
                                                      publishDataResults.contains(globalProject));
                    mFileFilters.add(fileFilter);
                }
            }

            // copie vers le dossier dedie
            FileUtils.copyDirectory(projectsDirectory, echobaseDirectory, mFileFilters);

        } catch (IOException e) {
            throw new CoserTechnicalException("Can't prepare upload data", e);
        }
    }

}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import com.itextpdf.text.DocumentException;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.command.CategoryLineCommand;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.control.ControlErrorGroup;
import fr.ifremer.coser.control.DiffCatchLengthControlError;
import fr.ifremer.coser.control.SpeciesControlError;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Length;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.MemoryDataStorage;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StatisticalLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.util.ShapeUtilities;
import org.nuiton.util.FileUtil;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.awt.Color;
import java.awt.Shape;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Publication service (charts, reports, export, pdf...)
 *
 * @author chatellier
 */
public class PublicationService {

    private static final Log log = LogFactory.getLog(PublicationService.class);

    protected CoserBusinessConfig config;

    protected CommonService commonService;

    public PublicationService(CoserBusinessConfig config) {
        this.config = config;
        commonService = new CommonService(config);
    }

    /**
     * Retourne les graphiques de comparaison entre le nombre de capture et
     * le nombre dans les tailles pour toutes les especes.
     *
     * @param project           project
     * @param container         data container
     * @param speciesCollection utilisé pour n'avoir qu'une partie des especes (can be null)
     * @return chart
     */
    public Map<String, JFreeChart> getCompareCatchLengthGraph(Project project, AbstractDataContainer container, Collection<String> speciesCollection) {

        int minYear = Integer.MAX_VALUE;
        int maxYear = Integer.MIN_VALUE;

        // look for data (data summed over catch)
        Map<String, Map<String, Double>> catchForSpeciesYears = new HashMap<String, Map<String, Double>>();
        Iterator<String[]> itCatchData = container.getCatch().iterator(true);
        while (itCatchData.hasNext()) {
            String[] tuple = itCatchData.next();
            String species = tuple[Catch.INDEX_SPECIES];

            Map<String, Double> speciesCatchForYears = catchForSpeciesYears.get(species);
            if (speciesCatchForYears == null) {
                speciesCatchForYears = new HashMap<String, Double>();
                catchForSpeciesYears.put(species, speciesCatchForYears);
            }

            String year = tuple[Catch.INDEX_YEAR];
            try {
                int intYear = Integer.parseInt(year);
                minYear = Math.min(minYear, intYear);
                maxYear = Math.max(maxYear, intYear);
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + year + "as int");
                }
            }
            String nombreValue = tuple[Catch.INDEX_NUMBER];
            try {
                Double nombreDouble = Double.valueOf(nombreValue);

                if (speciesCatchForYears.containsKey(year)) {
                    Double oldValue = speciesCatchForYears.get(year);
                    Double newValue = oldValue + nombreDouble;
                    speciesCatchForYears.put(year, newValue);
                } else {
                    speciesCatchForYears.put(year, nombreDouble);
                }
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + nombreValue + " as double");
                }
            }
        }

        // look for data (data summed over length)
        Map<String, Map<String, Double>> lengthForSpeciesYears = new HashMap<String, Map<String, Double>>();
        Iterator<String[]> itLengthData = container.getLength().iterator(true);
        while (itLengthData.hasNext()) {
            String[] tuple = itLengthData.next();
            String species = tuple[Length.INDEX_SPECIES];

            Map<String, Double> speciesLengthForYears = lengthForSpeciesYears.get(species);
            if (speciesLengthForYears == null) {
                speciesLengthForYears = new HashMap<String, Double>();
                lengthForSpeciesYears.put(species, speciesLengthForYears);
            }

            String year = tuple[Length.INDEX_YEAR];
            String nombreValue = tuple[Length.INDEX_NUMBER];
            try {
                Double nombreDouble = Double.valueOf(nombreValue);

                if (speciesLengthForYears.containsKey(year)) {
                    Double oldValue = speciesLengthForYears.get(year);
                    Double newValue = oldValue + nombreDouble;
                    speciesLengthForYears.put(year, newValue);
                } else {
                    speciesLengthForYears.put(year, nombreDouble);
                }
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse " + nombreValue + " as double");
                }
            }
        }

        // generation des graphes pour chaques especes
        Map<String, JFreeChart> charts = new TreeMap<String, JFreeChart>();
        for (Map.Entry<String, Map<String, Double>> catchEntries : catchForSpeciesYears.entrySet()) {

            String species = catchEntries.getKey();

            // filtage des especes
            if (speciesCollection != null && !speciesCollection.contains(species)) {
                continue;
            }

            Map<String, Double> catchNumbers = catchEntries.getValue();
            Map<String, Double> lengthNumbers = lengthForSpeciesYears.get(species);


            //SortedMap<String, Double> catchForYears = new TreeMap<String, Double>();
            //SortedMap<String, Double> lengthForYears = new TreeMap<String, Double>();
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            for (int year = minYear; year <= maxYear; ++year) {
                Double catchNumber = catchNumbers.get(String.valueOf(year));

                // laisse les abscents de captures apparentes
                //if (catchNumber == null) {
                //  catchNumber = 0.0;
                //}
                dataset.setValue(catchNumber, t(Category.CATCH.getTranslationKey()), (Integer) year);

                if (lengthNumbers != null) {
                    Double lengthNumber = lengthNumbers.get(String.valueOf(year));
                    // laisse les abscents de taille apparentes
                    //if (lengthNumber == null) {
                    //  lengthNumber = 0.0;
                    //}
                    dataset.setValue(lengthNumber, t(Category.LENGTH.getTranslationKey()), (Integer) year);
                }
            }

            JFreeChart chart = displayGraph(dataset, t("coser.business.chart.compareCatchLengthNumberTitle",
                                                       project.getDisplaySpeciesText(species)));
            charts.put(species, chart);
        }

        return charts;
    }

    protected JFreeChart displayGraph(CategoryDataset categoryDataSet, String title) {

        CategoryAxis categoryAxis = new CategoryAxis(t("coser.business.common.year"));
        categoryAxis.setCategoryMargin(0);
        // label horizontaux
        //categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
        ValueAxis valueAxis = new NumberAxis(t("coser.business.common.number"));
        valueAxis.setUpperMargin(0.1);

        CategoryItemRenderer renderer = new LineAndShapeRenderer();

        // agrandit la 2eme serie
        Shape firstShape = ShapeUtilities.createDiamond(5F);
        renderer.setSeriesShape(0, firstShape);

        CategoryToolTipGenerator cttg = new StandardCategoryToolTipGenerator("{0}: {2}", NumberFormat.getInstance());
        renderer.setBaseToolTipGenerator(cttg);

        CategoryPlot plot = new CategoryPlot(categoryDataSet, categoryAxis, valueAxis, renderer);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart chart = new JFreeChart(title,
                                          JFreeChart.DEFAULT_TITLE_FONT, plot, true);

        return chart;
    }

    /**
     * Export la liste d'erreurs dans un fichier html avec la liste des graphes
     * si necessaire.
     *
     * Genere les graphes en fin de fichier avec des liens html pour voir les
     * graphes depuis les erreurs.
     *
     * @param project          project
     * @param container        data container
     * @param validationErrors errors list
     * @return export html file
     * @throws CoserBusinessException
     */
    public File exportErrorsAsHTML(Project project, AbstractDataContainer container,
                                   List<ControlError> validationErrors) throws CoserBusinessException {

        File exportHtmlFile = null;
        Writer out = null;
        try {
            exportHtmlFile = File.createTempFile("coser-errors-", ".html");
            exportHtmlFile.deleteOnExit();
            if (log.isInfoEnabled()) {
                log.info("Generating HTML report to " + exportHtmlFile.getAbsolutePath());
            }

            out = new OutputStreamWriter(new FileOutputStream(exportHtmlFile), "utf-8");

            exportErrorsAsHTML(project, container, validationErrors, out, false);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't export errors", ex);
        } finally {
            IOUtils.closeQuietly(out);
        }
        return exportHtmlFile;
    }

    /**
     * Export la liste d'erreurs dans un fichier html avec la liste des graphes
     * si necessaire.
     *
     * Genere les graphes en fin de fichier avec des liens html pour voir les
     * graphes depuis les erreurs.
     *
     * @param project            project
     * @param container          data container
     * @param validationErrors   errors list
     * @param includeProjectInfo add project info to generate persistant pdf
     * @return export html file
     * @throws CoserBusinessException
     * @throws IOException
     */
    protected void exportErrorsAsHTML(Project project, AbstractDataContainer container,
                                      List<ControlError> validationErrors, Writer out, boolean includeProjectInfo) throws CoserBusinessException, IOException {

        List<String> speciesGraph = new ArrayList<String>();
        File imageDirectory = FileUtil.createTempDirectory("coser-images-", "-tmp");
        imageDirectory.deleteOnExit();

        out.write("<html><head>");
        out.write("<title>" + t("coser.business.publication.errorexporttitle", project.getName()) + "</title>");
        out.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
        out.write("</head><body>");

        // convert all errors as tree (code duplicated from ui tree model)
        Map<String, List<ControlErrorGroup>> validationCategoryChild = new HashMap<String, List<ControlErrorGroup>>();
        Map<ControlErrorGroup, List<ControlError>> validationErrorsChilds = new HashMap<ControlErrorGroup, List<ControlError>>();
        for (ControlError validationError : validationErrors) {

            String category = validationError.getCategory() == null ? t("coser.business.control.error.allCategories") : t(validationError.getCategory().getTranslationKey());
            List<ControlErrorGroup> errorGroup = validationCategoryChild.get(category);
            if (errorGroup == null) {
                errorGroup = new ArrayList<ControlErrorGroup>();
                validationCategoryChild.put(category, errorGroup);
            }
            ControlErrorGroup group = new ControlErrorGroup(validationError.getCategory(), validationError.getLevel(), validationError.getMessage());
            List<ControlError> childErrors = validationErrorsChilds.get(group);
            if (childErrors == null) {
                childErrors = new ArrayList<ControlError>();
                validationErrorsChilds.put(group, childErrors);
                errorGroup.add(group);
            }

            if (validationError instanceof SpeciesControlError) {
                SpeciesControlError diffError = (SpeciesControlError) validationError;
                speciesGraph.add(diffError.getSpecies());
            }
            childErrors.add(validationError);
        }

        Map<String, JFreeChart> charts = getCompareCatchLengthGraph(project, container, speciesGraph);

        // render output html
        out.write("<h1 style='text-align:center'>" + t("coser.business.publication.errorexporttitle", project.getName()) + "</h1>");

        if (includeProjectInfo) {
            // partie detail
            extractProjectReport(project, out);
            // partie specific : commandes
            extractHistoryCommandReport(project, container, out);
        }

        for (Map.Entry<String, List<ControlErrorGroup>> categoriesEntry : validationCategoryChild.entrySet()) {
            out.write("<h2>" + categoriesEntry.getKey() + "</h2>");

            out.write("<ul>");
            for (ControlErrorGroup group : categoriesEntry.getValue()) {

                out.write("<li style='margin-bottom:20px'><span style='color:");
                switch (group.getValidationLevel()) {
                    case FATAL:
                        out.write("rgb(88,0,0)");
                        break;
                    case ERROR:
                        out.write("red");
                        break;
                    case WARNING:
                        out.write("orange");
                        break;
                    case INFO:
                        out.write("green");
                        break;
                }
                out.write(";font-weight:bold'>" + t(group.getMessage()));
                out.write("</span>");

                out.write("<ul>");
                for (ControlError error : validationErrorsChilds.get(group)) {
                    out.write("<li type='circle'>");
                    out.write(t(error.getDetailMessage()));
                    List<String> lineNumbers = error.getLineNumbers();
                    if (CollectionUtils.isNotEmpty(lineNumbers)) {
                        out.write(" (" + t("coser.business.publication.errorexportlines") + " : ");
                        Iterator<String> itLineNumbers = lineNumbers.iterator();
                        while (itLineNumbers.hasNext()) {
                            out.write(itLineNumbers.next());
                            if (itLineNumbers.hasNext()) {
                                out.write(", ");
                            }
                        }
                        out.write(")");
                    }

                    // ajout d'un graph si necessaire
                    if (error instanceof DiffCatchLengthControlError) {
                        DiffCatchLengthControlError diffError = (DiffCatchLengthControlError) error;
                        String species = diffError.getSpecies();
                        out.write(" (<a href='#graph" + species + "'>" + t("coser.business.publication.errorgraph") + "</a>)");
                    }

                    String tipMessage = t(error.getTipMessage());
                    if (StringUtils.isNotEmpty(error.getTipMessage())) {
                        out.write("<p style='font-style:italic'>" + tipMessage + "</p>");
                    }

                    out.write("</li>");
                }
                out.write("</ul>");
                out.write("</li>");
            }
            out.write("</ul>");
        }

        out.write("<h2>" + t("coser.business.publication.errorgraphs") + "</h2>");
        out.write("<ul>");
        // generation des graphiques
        for (Map.Entry<String, JFreeChart> chartEntry : charts.entrySet()) {
            JFreeChart chart = chartEntry.getValue();
            File tmpChartImage = File.createTempFile("chart-", ".jpg", imageDirectory);
            tmpChartImage.deleteOnExit();
            ChartUtilities.saveChartAsJPEG(tmpChartImage, chart, 600, 300);
            out.write("<li><p><a name='graph" + chartEntry.getKey() + "'>");
            out.write(project.getDisplaySpeciesText(chartEntry.getKey()) + "</a></p>");
            out.write("<img src='" + imageDirectory.getName() + "/" + tmpChartImage.getName() + "' /></li>");
        }
        out.write("</ul>");

        out.write("</body></html>");
    }

    /**
     * Extrait les logs des modifications faites sur un control au format html.
     *
     * @param project project
     * @param control data container
     * @return extractedFile
     * @throws CoserBusinessException
     * @see AbstractDataContainer#getHistoryCommands()
     */
    public File extractControlLogAsHTML(Project project, Control control) throws CoserBusinessException {
        File exportHtmlFile = null;
        Writer out = null;
        try {
            exportHtmlFile = File.createTempFile("log-", ".html");
            if (log.isInfoEnabled()) {
                log.info("Generating HTML report to " + exportHtmlFile.getAbsolutePath());
            }

            out = new OutputStreamWriter(new FileOutputStream(exportHtmlFile), "utf-8");
            extractControlLogAsHTML(project, control, out);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't export logs", ex);
        } finally {
            IOUtils.closeQuietly(out);
        }
        return exportHtmlFile;
    }

    /**
     * Extrait les logs des modifications faites sur un control au format html
     * dans un flux donné.
     *
     * @param project project
     * @param control data container
     * @param out     output stream
     * @throws CoserBusinessException
     * @throws IOException
     */
    protected void extractControlLogAsHTML(Project project, Control control, Writer out) throws CoserBusinessException, IOException {
        out.write("<html><head>");
        out.write("<title>" + t("coser.business.publication.controllogexporttitle", project.getName()) + "</title>");
        out.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>");
        out.write("</head><body>");

        // header
        out.write("<h1 style='text-align:center'>" + t("coser.business.publication.controllogexporttitle", project.getName()) + "</h1>");

        // partie detail
        extractProjectReport(project, out);

        // partie specific : commandes
        extractHistoryCommandReport(project, control, out);

        out.write("</body></html>");
    }

    /**
     * Extrait les logs des modifications faites sur un control au format pdf.
     *
     * @param project          project
     * @param control          data container
     * @param validationErrors
     * @param pdfFile          output pdf file
     * @return extractedFile
     * @throws CoserBusinessException
     * @sincee 1.3
     * @see AbstractDataContainer#getHistoryCommands()
     */
    public void extractControlLogAsPDF(Project project, Control control,
                                       List<ControlError> validationErrors, File pdfFile) throws CoserBusinessException {

        OutputStream os = null;
        try {

            Writer out = new StringWriter();
            exportErrorsAsHTML(project, control, validationErrors, out, true);
            out.flush();

            // get content as w3c document
            Document document = CoserUtils.parseDocument(out.toString());

            // render template output as pdf
            os = new FileOutputStream(pdfFile);

            ITextRenderer renderer = new ITextRenderer();
            // TODO la base de generation est fixé en dur a java.io.tmpdir
            // car je sais que exportErrorsAsHTML genere les images dedans
            renderer.setDocument(document, "file://" + System.getProperty("java.io.tmpdir") + "/");
            renderer.layout();
            renderer.createPDF(os);

            os.close();
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't generate log pdf", ex);
        } catch (DocumentException ex) {
            throw new CoserBusinessException("Can't generate log pdf", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }
    }

    /**
     * Extrait les logs des modifications faites sur une selection au format html.
     *
     * @param project   project
     * @param selection data container
     * @return extractedFile
     * @throws CoserBusinessException
     * @see AbstractDataContainer#getHistoryCommands()
     */
    public File extractSelectionLogAsHTML(Project project, Selection selection) throws CoserBusinessException {
        File exportHtmlFile = null;
        Writer out = null;
        try {
            exportHtmlFile = File.createTempFile("coser-log-", ".html");
            exportHtmlFile.deleteOnExit();
            if (log.isInfoEnabled()) {
                log.info("Generating HTML report to " + exportHtmlFile.getAbsolutePath());
            }

            out = new OutputStreamWriter(new FileOutputStream(exportHtmlFile), "utf-8");
            extractSelectionLogAsHTML(project, selection, out);

        } catch (IOException ex) {
            throw new CoserBusinessException("Can't export logs", ex);
        } finally {
            IOUtils.closeQuietly(out);
        }
        return exportHtmlFile;
    }

    /**
     * Extrait les logs des modifications faites sur une selection au format html
     * dans le flux donné.
     *
     * @param project   project
     * @param selection data container
     * @param out       output stream
     * @return extractedFile
     * @throws IOException
     * @throws CoserBusinessException
     * @see AbstractDataContainer#getHistoryCommands()
     */
    protected void extractSelectionLogAsHTML(Project project, Selection selection, Writer out) throws IOException, CoserBusinessException {
        out.write("<html><head>");
        out.write("<title>" + t("coser.business.publication.selectionlogexporttitle", selection.getName(), project.getName()) + "</title>");
        out.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>");
        out.write("</head><body>");

        // header
        out.write("<h1 style='text-align:center'>" + t("coser.business.publication.selectionlogexporttitle", selection.getName(), project.getName()) + "</h1>");

        // partie detail
        extractProjectReport(project, out);

        // transcription des code especes
        StringBuilder speciesBuilder = new StringBuilder(256);
        String separator = "";
        for (String speciesCode : selection.getSelectedSpecies()) {
            speciesBuilder.append(separator);
            speciesBuilder.append(project.getDisplaySpeciesText(speciesCode));
            separator = ", ";
        }

        // partie resumé de selection
        out.write("<h2>" + t("coser.business.publication.selectionchoices") + "</h2>");
        out.write("<ul>");
        out.write("<li>" + t("coser.business.common.years") + " : " + StringUtils.join(selection.getSelectedYears(), ", ") + "</li>");
        out.write("<li>" + t("coser.business.common.strata") + " : " + StringUtils.join(selection.getSelectedStrata(), ", ") + "</li>");
        out.write("<li>" + t("coser.business.common.species") + " : " + speciesBuilder + "</li>");
        out.write("</ul>");

        // partie specific : commandes
        extractHistoryCommandReport(project, selection, out);

        out.write("</body></html>");
    }

    /**
     * Extrait la partie "detail projet" vers le flux.
     *
     * @param project project
     * @param out     stream
     * @throws IOException
     */
    protected void extractProjectReport(Project project, Writer out) throws IOException {

        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, config.getLocale());

        // date du rapport
        out.write("<div style='text-align:right;font-style:italic'>" +
                  t("coser.business.publication.date") + " : " +
                  dateFormat.format(new Date()) + "</div>");

        // partie projet
        out.write("<h2>" + t("coser.business.publication.projectdetails") + "</h2>");
        out.write("<ul>");
        out.write("<li>" + t("coser.business.publication.creationdate") + " : " + dateFormat.format(project.getCreationDate()) + "</li>");
        out.write("<li>" + t("coser.business.publication.author") + " : " + project.getAuthor() + "</li>");
        out.write("<li>" + t("coser.business.publication.catchfilename") + " : " + commonService.getDataStorageFileName(project, Category.CATCH, null) + "</li>");
        out.write("<li>" + t("coser.business.publication.lengthfilename") + " : " + commonService.getDataStorageFileName(project, Category.LENGTH, null) + "</li>");
        out.write("<li>" + t("coser.business.publication.haulfilename") + " : " + commonService.getDataStorageFileName(project, Category.HAUL, null) + "</li>");
        out.write("<li>" + t("coser.business.publication.stratafilename") + " : " + commonService.getDataStorageFileName(project, Category.STRATA, null) + "</li>");
        if (StringUtils.isNotEmpty(project.getComment())) {
            out.write("<li>" + t("coser.business.publication.comment") + " : " + project.getComment() + "</li>");
        }
        out.write("</ul>");
    }

    /**
     * Extrait les logs des modifications faites sur une selection au format pdf.
     *
     * @param project   project
     * @param selection data container
     * @return extractedFile
     * @throws CoserBusinessException
     * @see AbstractDataContainer#getHistoryCommands()
     * @since 1.3
     */
    public void extractSelectionLogAsPDF(Project project, Selection selection, File pdfFile) throws CoserBusinessException {

        OutputStream os = null;
        try {

            Writer out = new StringWriter();
            extractSelectionLogAsHTML(project, selection, out);
            out.flush();

            // get content as w3c document
            Document document = CoserUtils.parseDocument(out.toString());

            // render template output as pdf
            os = new FileOutputStream(pdfFile);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(document, null);
            renderer.layout();
            renderer.createPDF(os);

            os.close();
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't generate log pdf", ex);
        } catch (DocumentException ex) {
            throw new CoserBusinessException("Can't generate log pdf", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }
    }

    /**
     * Partie commune aux export qui effectue en 2 passe la recuperation
     * des lignes d'erreur, la recuperation des données correspondant à
     * ces lignes dans les fichiers originaux et la sortie des erreurs.
     *
     * @param project   project
     * @param container data container
     * @param out       output stream
     * @throws CoserBusinessException
     * @throws IOException
     */
    protected void extractHistoryCommandReport(Project project, AbstractDataContainer container, Writer out) throws CoserBusinessException, IOException {

        // first get lines index to get content
        Set<String> catchLines = new HashSet<String>();
        Set<String> lengthLines = new HashSet<String>();
        Set<String> haulLines = new HashSet<String>();
        Set<String> strataLines = new HashSet<String>();

        for (Command command : container.getHistoryCommands()) {
            Category category = null;
            String line = null;
            if (command instanceof CategoryLineCommand) {
                category = ((CategoryLineCommand) command).getCategory();
                line = ((CategoryLineCommand) command).getLineNumber();
            }
            if (category != null && line != null) {
                switch (category) {
                    case CATCH:
                        catchLines.add(line);
                        break;
                    case LENGTH:
                        lengthLines.add(line);
                        break;
                    case HAUL:
                        haulLines.add(line);
                        break;
                    case STRATA:
                        strataLines.add(line);
                        break;
                }
            }
        }

        // second get content for lines
        Map<String, String[]> catchContent = commonService.getOriginalContent(project, Category.CATCH, catchLines);
        Map<String, String[]> lengthContent = commonService.getOriginalContent(project, Category.LENGTH, lengthLines);
        Map<String, String[]> haulContent = commonService.getOriginalContent(project, Category.HAUL, haulLines);
        Map<String, String[]> strataContent = commonService.getOriginalContent(project, Category.STRATA, strataLines);

        // third, generate html report
        out.write("<h2>" + t("coser.business.publication.datamodification") + "</h2>");
        out.write("<ol>");
        for (Command command : container.getHistoryCommands()) {
            Category category = null;
            String line = null;
            if (command instanceof CategoryLineCommand) {
                category = ((CategoryLineCommand) command).getCategory();
                line = ((CategoryLineCommand) command).getLineNumber();
            }
            out.write("<li>");
            if (category != null && line != null) {
                String[] data = null;
                switch (category) {
                    case CATCH:
                        data = catchContent.get(line);
                        break;
                    case LENGTH:
                        data = lengthContent.get(line);
                        break;
                    case HAUL:
                        data = haulContent.get(line);
                        break;
                    case STRATA:
                        data = strataContent.get(line);
                        break;
                }
                out.write(command.getLogString(project, container) + "<br/>");
                out.write("<tt style='color:#5F5A59'>" + StringUtils.join(data, ", ") + "</tt>");
            } else {
                out.write(command.getLogString(project, container));
            }
            // add comment if present
            if (StringUtils.isNotEmpty(command.getComment())) {
                out.write("<br /><i>" + command.getComment() + "</i>");
            }
            out.write("</li>");
        }
        out.write("</ol>");
    }

    /**
     * Generate community graph for selected indicator.
     * Used by web ui.
     *
     * @param project             project
     * @param resultDirectory     result directory
     * @param rsufiResult         rsufiresult
     * @param codeTypeEspecesFile le fichier contenant les code type espece (specifique au projet)
     * @param indicator           indicator
     * @param list                indicator's list (can be {@code null} : none selected)
     * @param zoneDisplayName     zone full name
     * @param indicatorName       indicatorName localized
     * @param unit                data unit
     * @param locale              locale
     * @return generated graph image (temp file)
     * @throws CoserBusinessException
     */
    public File getRsufiResultComChart(Project project, File resultDirectory,
                                       RSufiResult rsufiResult, File codeTypeEspecesFile, String indicator,
                                       String list, String zoneDisplayName, String indicatorName, String unit,
                                       Locale locale) throws CoserBusinessException {

        File result = null;

        // le fichier estcomind
        File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

        if (log.isDebugEnabled()) {
            log.debug("Reading estComIndFile : " + estComIndFile);
        }

        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        // indicator list to take care
        // pour avoir une valeur non nulle si list est null
        // on prend dans ce cas la premiere valeur trouvée
        String localList = list;

        if (log.isDebugEnabled()) {
            log.debug("Searching list for indicator : " + indicator);
        }

        int multiplicator = 1;
        int minYear = Integer.MAX_VALUE;
        int maxYear = Integer.MIN_VALUE;
        boolean indicatorFound = false;
        Map<Integer, Double[]> graphData = new HashMap<Integer, Double[]>();
        Iterator<String[]> estComIndIterator = dataStorage.iterator(true);
        while (estComIndIterator.hasNext()) {
            // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
            String[] tuple = estComIndIterator.next();
            String indicatorCode = tuple[1];
            String indicatorList = tuple[2];

            if (indicatorCode.equals(indicator)) {
                indicatorFound = true;

                // si pas de list selectionnée, on prend la premiere
                if (StringUtils.isBlank(localList)) {
                    localList = indicatorList;
                }

                if (indicatorList.equals(localList)) {
                    Double estimation = Double.parseDouble(tuple[5]);
                    Double ecart = Double.parseDouble(tuple[6]);
                    int year = Integer.parseInt(tuple[4]);

                    if (year < minYear) {
                        minYear = year;
                    }
                    if (year > maxYear) {
                        maxYear = year;
                    }
                    graphData.put(year, new Double[]{estimation, ecart*1.96});

                    // si les données sont énormes, on affiche les données
                    // / multiplicator et on le mentionne dans la légende
                    if (estimation > 1e9) {
                        multiplicator = 1000000;
                    }
                    if (estimation > 1e6 && multiplicator < 1000000) {
                        multiplicator = 1000;
                    }
                }
            }
        }

        // avec l'extraction des données, on peut demander a générer un graphique
        // sur un indicateur qui n'est pas présent dans le projet courant,
        // dans ce cas, on retourne null
        if (indicatorFound) {
            // get graph title
            String chartTitle = zoneDisplayName;
            chartTitle += " - " + indicatorName;

            // ajout de la traduction de la liste d'indicateur
            // les liste sont a1, T1, T2 ...
            String listLetter = String.valueOf(localList.charAt(0));
            DataStorage dataStorageType = commonService.loadCSVFile(codeTypeEspecesFile, CoserConstants.CSV_SEPARATOR_CHAR);
            Iterator<String[]> typeIterator = dataStorageType.iterator(true);
            while (typeIterator.hasNext()) {
                // "Types";"Commentaire";"NumSys min";"NumSys max";"Code"
                String[] tuple = typeIterator.next();
                if (tuple[4].equals(listLetter)) {
                    /// gestion du groupe "Tous"
                    // cas special, c'est la seule valeur du fichier
                    // code type espece qui a besoin d'une traduction
                    if (tuple[4].equalsIgnoreCase("T")) {
                        if ("fr".equals(locale.getLanguage())) {
                            chartTitle += " - " + "Tous Liste " + localList.charAt(1);
                        } else if ("es".equals(locale.getLanguage())) {
                            chartTitle += " - " + "Todo Lista " + localList.charAt(1);
                        } else {
                            chartTitle += " - " + "All List " + localList.charAt(1);
                        }
                    } else {
                        // ajout de la traduction du nom de liste plus le numéro
                        if ("fr".equals(locale.getLanguage())) {
                            chartTitle += " - " + tuple[0] + " Liste " + localList.charAt(1);
                        } else if ("es".equals(locale.getLanguage())) {
                            chartTitle += " - " + tuple[0] + " Lista " + localList.charAt(1);
                        } else {
                            chartTitle += " - " + tuple[0] + " List " + localList.charAt(1);
                        }
                    }
                    break;
                }
            }

            // generate dataset with sorted data
            DefaultStatisticalCategoryDataset statisticalDataset = new DefaultStatisticalCategoryDataset();
            for (int indexYear = minYear; indexYear <= maxYear; ++indexYear) {
                Double[] entry = graphData.get(indexYear);
                if (entry != null) {
                    Double estimation = entry[0] / multiplicator;
                    Double ecart = entry[1] / multiplicator;
                    statisticalDataset.add(estimation, ecart, "Serie1", (Comparable) indexYear);
                } else {
                    statisticalDataset.add(null, null, "Serie1", (Comparable) indexYear);
                }
            }

            // configure chart
            //CategoryAxis categoryAxis = new CategoryAxis(t("coser.business.common.year"));
            // FIXME echatellier 20110414 hack just for year, need a real locale object here
            // to call t(locale, i18nkey)
            String yearAxis = "Year";
            if ("fr".equals(locale.getLanguage())) {
                yearAxis = "Ann\u00E9e";
            } else if ("es".equals(locale.getLanguage())) {
                yearAxis = "A\u00F1o";
            }
            CategoryAxis categoryAxis = new CategoryAxis(yearAxis);
            categoryAxis.setCategoryMargin(0);
            categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
            // label horizontaux
            String legendY = indicatorName;
            if (multiplicator != 1) {
                // affiche par exemple : cm * 1000
                legendY += " (" + unit + "*" + multiplicator + ")";
            } else if (StringUtils.isNotEmpty(unit)) {
                legendY += " (" + unit + ")";
            }
            ValueAxis valueAxis = new NumberAxis(legendY);
            valueAxis.setUpperMargin(0.1);

            CategoryItemRenderer renderer = new StatisticalLineAndShapeRenderer(false, true);

            // n'affiche pas les nombre sur le graphique
            //StandardCategoryItemLabelGenerator itemLabelGenerator = new StandardCategoryItemLabelGenerator();
            //renderer.setBaseItemLabelGenerator(itemLabelGenerator);  
            //renderer.setBaseItemLabelsVisible(true);

            CategoryPlot plot = new CategoryPlot(statisticalDataset, categoryAxis, valueAxis, renderer);
            plot.setOrientation(PlotOrientation.VERTICAL);
            JFreeChart chart = new JFreeChart(chartTitle,
                                              JFreeChart.DEFAULT_TITLE_FONT, plot, true);

            // remove series legend
            chart.removeLegend();
            // white background
            chart.setBackgroundPaint(Color.WHITE);

            try {
                result = File.createTempFile("coser-chart-", ".png");
                result.deleteOnExit();
                ChartUtilities.saveChartAsPNG(result, chart, 800, 400);
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't save chart", ex);
            }
        }

        return result;
    }

    /**
     * Generate population graph for selected species and indicator.
     *
     * @param project         project
     * @param resultDirectory result directory
     * @param rsufiResult     rsufi result
     * @param species         species
     * @param indicator       indicator
     * @param zoneDisplayName zone full name
     * @param indicatorName   indicatorName localized
     * @param unit            data unit
     * @param locale          locale
     * @return generated image file (temp file)
     * @throws CoserBusinessException
     */
    public File getRsufiResultPopChart(Project project, File resultDirectory,
                                       RSufiResult rsufiResult, String species, String indicator,
                                       String zoneDisplayName, String indicatorName, String unit, Locale locale) throws CoserBusinessException {

        File result = null;

        // le fichier estcomind
        File estPopIndFile = new File(resultDirectory, rsufiResult.getEstPopIndName());

        // Campagne Indicateur Liste Species Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estPopIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        int multiplicator = 1;
        int minYear = Integer.MAX_VALUE;
        int maxYear = Integer.MIN_VALUE;
        boolean indicatorFound = false;
        Map<Integer, Double[]> graphData = new HashMap<Integer, Double[]>();
        Iterator<String[]> estPopIndIterator = dataStorage.iterator(true);
        while (estPopIndIterator.hasNext()) {
            String[] tuple = estPopIndIterator.next();

            String speciesCode = tuple[3];
            String indicatorCode = tuple[1];
            if (speciesCode.equals(species) && indicatorCode.equals(indicator)) {
                indicatorFound = true;

                // XXX echatellier, maybe take care of list here ?

                Double estimation = Double.parseDouble(tuple[6]);
                Double ecart = Double.parseDouble(tuple[7]);
                int year = Integer.parseInt(tuple[5]);

                if (year < minYear) {
                    minYear = year;
                }
                if (year > maxYear) {
                    maxYear = year;
                }
                graphData.put(year, new Double[]{estimation, ecart*1.96});

                // si les données sont énormes, on affiche les données
                // / multiplicator et on le mentionne dans la légende
                if (estimation > 1e9) {
                    multiplicator = 1000000;
                }
                if (estimation > 1e6 && multiplicator < 1000000) {
                    multiplicator = 1000;
                }
            }
        }

        // avec l'extraction des données, on peut demander a générer un graphique
        // sur un indicateur qui n'est pas présent dans le projet courant,
        // dans ce cas, on retourne null
        if (indicatorFound) {
            // get graph title
            String title = zoneDisplayName;
            title += " - " + indicatorName;
            title += " - " + commonService.getReportDisplayName(project, species);

            // generate dataset with sorted data
            DefaultStatisticalCategoryDataset statisticalDataset = new DefaultStatisticalCategoryDataset();
            for (int indexYear = minYear; indexYear <= maxYear; ++indexYear) {
                Double[] entry = graphData.get(indexYear);
                if (entry != null) {
                    Double estimation = entry[0] / multiplicator;
                    Double ecart = entry[1] / multiplicator;
                    statisticalDataset.add(estimation, ecart, "Serie1", (Comparable) indexYear);
                } else {
                    statisticalDataset.add(null, null, "Serie1", (Comparable) indexYear);
                }
            }


            // configure chart
            //CategoryAxis categoryAxis = new CategoryAxis(t("coser.business.common.year"));
            // FIXME echatellier 20110414 hack just for year, need a real locale object here
            // to call t(locale, i18nkey)
            String yearAxis = "Year";
            if ("fr".equals(locale.getLanguage())) {
                yearAxis = "Ann\u00E9e";
            } else if ("es".equals(locale.getLanguage())) {
                yearAxis = "Ann\u00E9e";
            }
            CategoryAxis categoryAxis = new CategoryAxis(yearAxis);
            categoryAxis.setCategoryMargin(0);
            categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
            // label horizontaux
            String legendY = indicatorName;
            if (multiplicator != 1) {
                // affiche par exemple : cm * 1000
                legendY += " (" + unit + "*" + multiplicator + ")";
            } else if (StringUtils.isNotEmpty(unit)) {
                legendY += " (" + unit + ")";
            }
            ValueAxis valueAxis = new NumberAxis(legendY);
            valueAxis.setUpperMargin(0.1);

            CategoryItemRenderer renderer = new StatisticalLineAndShapeRenderer(false, true);

            // n'affiche pas les nombre sur le graphique
            //StandardCategoryItemLabelGenerator itemLabelGenerator = new StandardCategoryItemLabelGenerator();
            //renderer.setBaseItemLabelGenerator(itemLabelGenerator);  
            //renderer.setBaseItemLabelsVisible(true);

            CategoryPlot plot = new CategoryPlot(statisticalDataset, categoryAxis, valueAxis, renderer);
            plot.setOrientation(PlotOrientation.VERTICAL);
            JFreeChart chart = new JFreeChart(title,
                                              JFreeChart.DEFAULT_TITLE_FONT, plot, true);

            // remove series legend
            chart.removeLegend();
            // white background
            chart.setBackgroundPaint(Color.WHITE);

            try {
                result = File.createTempFile("coser-chart-", ".png");
                result.deleteOnExit();
                ChartUtilities.saveChartAsPNG(result, chart, 800, 400);
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't save chart", ex);
            }
        }

        return result;
    }

    /**
     * Generate community graph for selected indicators.
     * Used by web ui extraction.
     *
     * @param project             project
     * @param resultDirectory     result directory
     * @param rsufiResult         rsufiresult
     * @param codeTypeEspecesFile le fichier contenant les code type espece (specifique au projet)
     * @param indicator           indicator
     * @param zoneDisplayName     zone full name
     * @param indicatorName       indicatorName localized
     * @param unit                data unit
     * @param locale              locale
     * @param width               graph width
     * @param height              graph height
     * @return generated graph image (temp file) and data
     * @throws CoserBusinessException
     */
    public Map<String, Object[]> getRsufiResultComCharts(Project project, File resultDirectory,
                                                         RSufiResult rsufiResult, File codeTypeEspecesFile, Collection<String> indicators,
                                                         String zoneDisplayName, MultiKeyMap indicatorMap, Locale locale,
                                                         int width, int height) throws CoserBusinessException {

        Map<String, Object[]> result = new HashMap<String, Object[]>();

        // le fichier estcomind
        File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

        if (log.isDebugEnabled()) {
            log.debug("Reading estComIndFile : " + estComIndFile);
        }

        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        int multiplicator = 1;
        int minYear = Integer.MAX_VALUE;
        int maxYear = Integer.MIN_VALUE;

        Map<String, Map<Integer, Double[]>> indicatorGraphData = new HashMap<String, Map<Integer, Double[]>>();
        Map<String, String> indicatorLists = new HashMap<String, String>();
        Map<String, DataStorage> indicatorStorages = new HashMap<String, DataStorage>();
        Iterator<String[]> estComIndIterator = dataStorage.iterator(false);
        String[] headers = estComIndIterator.next();
        while (estComIndIterator.hasNext()) {
            // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
            String[] tuple = estComIndIterator.next();
            String indicatorCode = tuple[1];
            String indicatorList = tuple[2];

            if (indicators.contains(indicatorCode)) {

                // si pas de list selectionnée, on prend la premiere
                String localList = indicatorLists.get(indicatorCode);
                if (StringUtils.isBlank(localList)) {
                    localList = indicatorList;
                    indicatorLists.put(indicatorCode, localList);
                }

                if (indicatorList.equals(localList)) {
                    Double estimation = Double.parseDouble(tuple[5]);
                    Double ecart = Double.parseDouble(tuple[6]);
                    int year = Integer.parseInt(tuple[4]);

                    if (year < minYear) {
                        minYear = year;
                    }
                    if (year > maxYear) {
                        maxYear = year;
                    }
                    Map<Integer, Double[]> graphData = indicatorGraphData.get(indicatorCode);
                    if (graphData == null) {
                        graphData = new HashMap<Integer, Double[]>();
                        indicatorGraphData.put(indicatorCode, graphData);
                    }
                    graphData.put(year, new Double[]{estimation, ecart*1.96});

                    // si les données sont énormes, on affiche les données
                    // / multiplicator et on le mentionne dans la légende
                    if (estimation > 1e9) {
                        multiplicator = 1000000;
                    }
                    if (estimation > 1e6 && multiplicator < 1000000) {
                        multiplicator = 1000;
                    }

                    // for data part
                    DataStorage subDataStorage = indicatorStorages.get(indicatorCode);
                    if (subDataStorage == null) {
                        subDataStorage = new MemoryDataStorage();
                        if ("fr".equals(locale.getLanguage())) {
                            subDataStorage.add(new String[]{"Campagne", "Indice", "Liste", "Strate", "Année", "Estimation", "EcartType", "CV"});
                        } else {
                            subDataStorage.add(new String[]{"Survey", "Index", "List", "Stratum", "Year", "Estimate", "StandardDeviation", "CV"});
                        }
                        indicatorStorages.put(indicatorCode, subDataStorage);
                    }
                    subDataStorage.add(tuple);
                }
            }
        }

        // avec l'extraction des données, on peut demander a générer un graphique
        // sur un indicateur qui n'est pas présent dans le projet courant,
        // dans ce cas, on retourne null
        for (String indicator : indicatorGraphData.keySet()) {
            // get graph title
            String chartTitle = zoneDisplayName;
            String indicatorName = (String) indicatorMap.get(indicator, locale.getLanguage());
            String unit = (String) indicatorMap.get(indicator, "unit");
            chartTitle += " - " + indicatorName;

            // ajout de la traduction de la liste d'indicateur
            // les liste sont a1, T1, T2 ...
            String localList = indicatorLists.get(indicator);
            String listLetter = String.valueOf(localList.charAt(0));
            DataStorage dataStorageType = commonService.loadCSVFile(codeTypeEspecesFile, CoserConstants.CSV_SEPARATOR_CHAR);
            Iterator<String[]> typeIterator = dataStorageType.iterator(true);
            while (typeIterator.hasNext()) {
                // "Types";"Commentaire";"NumSys min";"NumSys max";"Code"
                String[] tuple = typeIterator.next();
                if (tuple[4].equals(listLetter)) {
                    /// gestion du groupe "Tous"
                    // cas special, c'est la seule valeur du fichier
                    // code type espece qui a besoin d'une traduction
                    if (tuple[4].equalsIgnoreCase("T")) {
                        if ("fr".equals(locale.getLanguage())) {
                            chartTitle += " - " + "Tous Liste " + localList.charAt(1);
                        } else if ("es".equals(locale.getLanguage())) {
                            chartTitle += " - " + "Todo Lista " + localList.charAt(1);
                        } else {
                            chartTitle += " - " + "All List " + localList.charAt(1);
                        }
                    } else {
                        // ajout de la traduction du nom de liste plus le numéro
                        if ("fr".equals(locale.getLanguage())) {
                            chartTitle += " - " + tuple[0] + " Liste " + localList.charAt(1);
                        } else if ("es".equals(locale.getLanguage())) {
                            chartTitle += " - " + tuple[0] + " Lista " + localList.charAt(1);
                        } else {
                            chartTitle += " - " + tuple[0] + " List " + localList.charAt(1);
                        }
                    }
                    break;
                }
            }

            // generate dataset with sorted data
            Map<Integer, Double[]> graphData = indicatorGraphData.get(indicator);
            DefaultStatisticalCategoryDataset statisticalDataset = new DefaultStatisticalCategoryDataset();
            for (int indexYear = minYear; indexYear <= maxYear; ++indexYear) {
                Double[] entry = graphData.get(indexYear);
                if (entry != null) {
                    Double estimation = entry[0] / multiplicator;
                    Double ecart = entry[1] / multiplicator;
                    statisticalDataset.add(estimation, ecart, "Serie1", (Comparable) indexYear);
                } else {
                    statisticalDataset.add(null, null, "Serie1", (Comparable) indexYear);
                }
            }

            // configure chart
            //CategoryAxis categoryAxis = new CategoryAxis(t("coser.business.common.year"));
            // FIXME echatellier 20110414 hack just for year, need a real locale object here
            // to call t(locale, i18nkey)
            String yearAxis = "Year";
            if ("fr".equals(locale.getLanguage())) {
                yearAxis = "Ann\u00E9e";
            } else if ("es".equals(locale.getLanguage())) {
                yearAxis = "A\u00F1o";
            }
            CategoryAxis categoryAxis = new CategoryAxis(yearAxis);
            categoryAxis.setCategoryMargin(0);
            categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
            // label horizontaux
            String legendY = indicatorName;
            if (multiplicator != 1) {
                // affiche par exemple : cm * 1000
                legendY += " (" + unit + "*" + multiplicator + ")";
            } else if (StringUtils.isNotEmpty(unit)) {
                legendY += " (" + unit + ")";
            }
            ValueAxis valueAxis = new NumberAxis(legendY);
            valueAxis.setUpperMargin(0.1);

            CategoryItemRenderer renderer = new StatisticalLineAndShapeRenderer(false, true);

            // n'affiche pas les nombre sur le graphique
            //StandardCategoryItemLabelGenerator itemLabelGenerator = new StandardCategoryItemLabelGenerator();
            //renderer.setBaseItemLabelGenerator(itemLabelGenerator);  
            //renderer.setBaseItemLabelsVisible(true);

            CategoryPlot plot = new CategoryPlot(statisticalDataset, categoryAxis, valueAxis, renderer);
            plot.setOrientation(PlotOrientation.VERTICAL);
            JFreeChart chart = new JFreeChart(chartTitle,
                                              JFreeChart.DEFAULT_TITLE_FONT, plot, true);

            // remove series legend
            chart.removeLegend();
            // white background
            chart.setBackgroundPaint(Color.WHITE);

            try {
                File chartFile = File.createTempFile("coser-chart-", ".png");
                chartFile.deleteOnExit();
                ChartUtilities.saveChartAsPNG(chartFile, chart, width, height);
                //ByteArrayOutputStream out = new ByteArrayOutputStream();
                //ChartUtilities.writeChartAsPNG(out, chart, width, height);

                // data extraction
                DataStorage subDataStorage = indicatorStorages.get(indicator);
                StringWriter writer = new StringWriter();
                commonService.storeDataWhithoutQuote(subDataStorage, writer, null, null);

                // add chart file dans chart data in result
                result.put(indicator, new Object[]{chartFile, writer.toString()});
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't save chart", ex);
            }
        }

        return result;
    }

    /**
     * Generate population graph for selected species and indicator.
     *
     * @param project         project
     * @param resultDirectory result directory
     * @param rsufiResult     rsufi result
     * @param species         species
     * @param indicator       indicator
     * @param zoneDisplayName zone full name
     * @param indicatorName   indicatorName localized
     * @param unit            data unit
     * @param locale          locale
     * @param width           graph width
     * @param height          graph height
     * @return generated image file (temp file)
     * @throws CoserBusinessException
     */
    public Map<String, Object[]> getRsufiResultPopCharts(Project project, File resultDirectory,
                                                         RSufiResult rsufiResult, Collection<String> species, Collection<String> indicators,
                                                         String zoneDisplayName, MultiKeyMap indicatorMap, Locale locale,
                                                         int width, int height) throws CoserBusinessException {

        Map<String, Object[]> result = new HashMap<String, Object[]>();

        // le fichier estcomind
        File estPopIndFile = new File(resultDirectory, rsufiResult.getEstPopIndName());

        // Campagne Indicateur Liste Species Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estPopIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        int multiplicator = 1;
        int minYear = Integer.MAX_VALUE;
        int maxYear = Integer.MIN_VALUE;

        MultiKeyMap indicatorGraphData = new MultiKeyMap();
        MultiKeyMap indicatorStorages = new MultiKeyMap();
        Iterator<String[]> estPopIndIterator = dataStorage.iterator(true);
        while (estPopIndIterator.hasNext()) {
            String[] tuple = estPopIndIterator.next();

            String speciesCode = tuple[3];
            String indicatorCode = tuple[1];
            if (species.contains(speciesCode) && indicators.contains(indicatorCode)) {

                // XXX echatellier, maybe take care of list here ?

                Double estimation = Double.parseDouble(tuple[6]);
                Double ecart = Double.parseDouble(tuple[7]);
                int year = Integer.parseInt(tuple[5]);

                if (year < minYear) {
                    minYear = year;
                }
                if (year > maxYear) {
                    maxYear = year;
                }
                Map<Integer, Double[]> graphData = (Map<Integer, Double[]>) indicatorGraphData.get(indicatorCode, speciesCode);
                if (graphData == null) {
                    graphData = new HashMap<Integer, Double[]>();
                    indicatorGraphData.put(indicatorCode, speciesCode, graphData);
                }
                graphData.put(year, new Double[]{estimation, ecart*1.96});

                // si les données sont énormes, on affiche les données
                // / multiplicator et on le mentionne dans la légende
                if (estimation > 1e9) {
                    multiplicator = 1000000;
                }
                if (estimation > 1e6 && multiplicator < 1000000) {
                    multiplicator = 1000;
                }

                // for data part
                DataStorage subDataStorage = (DataStorage) indicatorStorages.get(indicatorCode, speciesCode);
                if (subDataStorage == null) {
                    subDataStorage = new MemoryDataStorage();
                    if ("fr".equals(locale.getLanguage())) {
                        subDataStorage.add(new String[]{"Campagne", "Indice", "Liste", "Espèce", "Strate", "Annee", "Estimation", "EcartType", "CV"});
                    } else {
                        subDataStorage.add(new String[]{"Survey", "Index", "List", "Species", "Stratum", "Year", "Estimate", "StandardDeviation", "CV"});
                    }
                    indicatorStorages.put(indicatorCode, speciesCode, subDataStorage);
                }
                subDataStorage.add(tuple);
            }
        }


        // avec l'extraction des données, on peut demander a générer un graphique
        // sur un indicateur qui n'est pas présent dans le projet courant,
        // dans ce cas, on retourne null
        for (MultiKey indicatorSpecies : (Set<MultiKey>) indicatorGraphData.keySet()) {
            String indicator = (String) indicatorSpecies.getKey(0);
            String aSpecies = (String) indicatorSpecies.getKey(1);
            // get graph title
            String title = zoneDisplayName;
            String indicatorName = (String) indicatorMap.get(indicator, locale.getLanguage());
            String unit = (String) indicatorMap.get(indicator, "unit");
            title += " - " + indicatorName;
            title += " - " + commonService.getReportDisplayName(project, aSpecies);

            // generate dataset with sorted data
            DefaultStatisticalCategoryDataset statisticalDataset = new DefaultStatisticalCategoryDataset();
            Map<Integer, Double[]> graphData = (Map<Integer, Double[]>) indicatorGraphData.get(indicator, aSpecies);
            for (int indexYear = minYear; indexYear <= maxYear; ++indexYear) {
                Double[] entry = graphData.get(indexYear);
                if (entry != null) {
                    Double estimation = entry[0] / multiplicator;
                    Double ecart = entry[1] / multiplicator;
                    statisticalDataset.add(estimation, ecart, "Serie1", (Comparable) indexYear);
                } else {
                    statisticalDataset.add(null, null, "Serie1", (Comparable) indexYear);
                }
            }


            // configure chart
            //CategoryAxis categoryAxis = new CategoryAxis(t("coser.business.common.year"));
            // FIXME echatellier 20110414 hack just for year, need a real locale object here
            // to call t(locale, i18nkey)
            String yearAxis = "Year";
            if ("fr".equals(locale.getLanguage())) {
                yearAxis = "Ann\u00E9e";
            } else if ("es".equals(locale.getLanguage())) {
                yearAxis = "Ann\u00E9e";
            }
            CategoryAxis categoryAxis = new CategoryAxis(yearAxis);
            categoryAxis.setCategoryMargin(0);
            categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
            // label horizontaux
            String legendY = indicatorName;
            if (multiplicator != 1) {
                // affiche par exemple : cm * 1000
                legendY += " (" + unit + "*" + multiplicator + ")";
            } else if (StringUtils.isNotEmpty(unit)) {
                legendY += " (" + unit + ")";
            }
            ValueAxis valueAxis = new NumberAxis(legendY);
            valueAxis.setUpperMargin(0.1);

            CategoryItemRenderer renderer = new StatisticalLineAndShapeRenderer(false, true);

            // n'affiche pas les nombre sur le graphique
            //StandardCategoryItemLabelGenerator itemLabelGenerator = new StandardCategoryItemLabelGenerator();
            //renderer.setBaseItemLabelGenerator(itemLabelGenerator);  
            //renderer.setBaseItemLabelsVisible(true);

            CategoryPlot plot = new CategoryPlot(statisticalDataset, categoryAxis, valueAxis, renderer);
            plot.setOrientation(PlotOrientation.VERTICAL);
            JFreeChart chart = new JFreeChart(title,
                                              JFreeChart.DEFAULT_TITLE_FONT, plot, true);

            // remove series legend
            chart.removeLegend();
            // white background
            chart.setBackgroundPaint(Color.WHITE);

            try {
                File chartFile = File.createTempFile("coser-chart-", ".png");
                chartFile.deleteOnExit();
                ChartUtilities.saveChartAsPNG(chartFile, chart, width, height);
                //ByteArrayOutputStream out = new ByteArrayOutputStream();
                //ChartUtilities.writeChartAsPNG(out, chart, width, height);

                // data extraction
                DataStorage subDataStorage = (DataStorage) indicatorStorages.get(indicator, aSpecies);
                StringWriter writer = new StringWriter();
                commonService.storeDataWhithoutQuote(subDataStorage, writer, null, null);

                // add chart file dans chart data in result
                result.put(indicator + "-" + aSpecies, new Object[]{chartFile, writer.toString()});
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't save chart", ex);
            }
        }

        return result;
    }

    /**
     * Genere un sous fichier CSV (qui contient un sous ensemble de estPopInd)
     * et qui a servit a generer le graphique.
     *
     * @param resultDirectory result directory
     * @param rsufiResult     rsufi result
     * @param indicator       indicator
     * @param list            indicator's list (if {@code null} look for pop indicators or no list selected
     * @return generated csv file (temp file)
     * @throws CoserBusinessException
     */
    public File getRsufiResultComChartData(File resultDirectory,
                                           RSufiResult rsufiResult, String indicator, String list) throws CoserBusinessException {

        File result = null;

        try {
            result = File.createTempFile("coser-chart-", ".csv");
            result.deleteOnExit();

            // le fichier estcomind
            File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

            // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
            DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
            DataStorage subDataStorage = new MemoryDataStorage();
            Iterator<String[]> estComIndIterator = dataStorage.iterator();
            // manage header
            String[] headers = estComIndIterator.next();
            subDataStorage.add(headers);

            // indicator list to take care
            // pour avoir une valeur non nulle si list est null
            // on prend dans ce cas la premiere valeur trouvée
            String localList = list;

            while (estComIndIterator.hasNext()) {
                String[] tuple = estComIndIterator.next();
                String indicatorCode = tuple[1];
                String indicatorList = tuple[2];

                if (indicatorCode.equals(indicator)) {

                    // si pas de list selectionnée, on prend la premiere
                    if (StringUtils.isBlank(localList)) {
                        localList = indicatorList;
                    }

                    if (indicatorList.equals(localList)) {
                        subDataStorage.add(tuple);
                    }
                }
            }

            commonService.storeDataWhithoutQuote(subDataStorage, result, null, null);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save csv file", ex);
        }

        return result;
    }

    /**
     * Genere un sous fichier CSV (qui contient un sous ensemble de estPopInd)
     * et qui a servit a generer le graphique.
     *
     * @param resultDirectory result directory
     * @param rsufiResult     rsufi result
     * @param species         species
     * @param indicator       indicator
     * @return generated csv file (temp file)
     * @throws CoserBusinessException
     */
    public File getRsufiResultPopChartData(File resultDirectory,
                                           RSufiResult rsufiResult, String species, String indicator) throws CoserBusinessException {

        File result = null;

        try {
            result = File.createTempFile("coser-graph-", ".csv");
            result.deleteOnExit();

            // le fichier estpopind
            File estPopIndFile = new File(resultDirectory, rsufiResult.getEstPopIndName());

            // Campagne Indicateur Liste Species Strate Annee Estimation EcartType CV
            DataStorage dataStorage = commonService.loadCSVFile(estPopIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
            DataStorage subDataStorage = new MemoryDataStorage();
            Iterator<String[]> estPopIndIterator = dataStorage.iterator();
            // manage header
            String[] headers = estPopIndIterator.next();
            subDataStorage.add(headers);

            // le fichier estpopind semble pouvoir contenir plusieurs fois
            // les mêmes données dupliqués sur différentes liste (avec
            // les mêmes valeurs dans chaque liste
            // pour ne pas avoir de duplication, on prend la premiere
            // trouvée seulement dans le fichier csv de sortie
            String localList = null;

            while (estPopIndIterator.hasNext()) {
                String[] tuple = estPopIndIterator.next();
                String indicatorCode = tuple[1];
                String indicatorList = tuple[2];
                String speciesCode = tuple[3];

                if (speciesCode.equals(species) && indicatorCode.equals(indicator)) {

                    // si pas de list selectionnée, on prend la premiere
                    // qui correspond a species et indicator
                    if (StringUtils.isBlank(localList)) {
                        localList = indicatorList;
                    }

                    if (indicatorList.equals(localList)) {
                        subDataStorage.add(tuple);
                    }
                }
            }

            commonService.storeDataWhithoutQuote(subDataStorage, result, null, null);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save csv file", ex);
        }

        return result;
    }
}

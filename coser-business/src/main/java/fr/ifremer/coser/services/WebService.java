/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import com.itextpdf.text.DocumentException;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.MemoryDataStorage;
import fr.ifremer.coser.util.DataType;
import fr.ifremer.coser.util.InputStreamKnownSizeBody;
import fr.ifremer.coser.util.ProgressMonitor;
import fr.ifremer.coser.util.ProgressStream;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.util.ZipUtil;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.t;

/**
 * Service specifique à l'interface web de visualisation et à la partie UI
 * qui sert a envoyer les resultats vers l'interface web.
 *
 * Ce service, contrairement aux autres a un état, les indicateurs et zones
 * chargées persistent après chargement, car elle ne peuvent pas changer
 * tant que l'application est en cours d'utilisation.
 *
 * Il faut garder à l'esprit que les projets n'ont pas forcement
 * de données publiées, donc le chargement du projet est possible, mais
 * pas les données de controle et les données de selection (donc plutot
 * travailler sur les fichiers estcomind/estpopind que sur les 4 tables).
 *
 * @author chatellier
 * @deprecated since 1.5, should never be used, code too ugly and *simple* (dummy simple?)
 */
@Deprecated
public class WebService {

    private static final Log log = LogFactory.getLog(WebService.class);

    protected CoserBusinessConfig config;

    protected CommonService commonService;

    protected ProjectService projectService;

    protected PublicationService publicationService;

    /** Indicator map (id, locale > translation, id, "unit" > unit) (etat du service). */
    protected MultiKeyMap indicatorsMap;

    /** Zones map (etat du service). */
    protected DataStorage zonesMap;

    /** Freemarker */
    protected Configuration freemarkerConfiguration;

    public WebService(CoserBusinessConfig config) {
        this.config = config;

        commonService = new CommonService(config);
        projectService = new ProjectService(config);
        publicationService = new PublicationService(config);

        freemarkerConfiguration = new Configuration();

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(WebService.class, "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

        // pour les maps dans les template (entre autre)
        freemarkerConfiguration.setObjectWrapper(new BeansWrapper());
    }

    /**
     * Charge les indicateurs disponible depuis le fichier des indicateurs.
     * (indid, indname)
     *
     * @return indicators map
     * @throws CoserBusinessException
     */
    protected MultiKeyMap getIndicatorsMap() throws CoserBusinessException {

        if (indicatorsMap == null) {
            indicatorsMap = new MultiKeyMap();
            File indicatorsFile = config.getWebIndicatorsFile();
            DataStorage indicatorsStorage = commonService.loadCSVFile(indicatorsFile);
            Iterator<String[]> iteratorInd = indicatorsStorage.iterator(true);
            while (iteratorInd.hasNext()) {
                // "id";"label_fr";"label_en";"label_es";"unit"
                String[] indicator = iteratorInd.next();
                indicatorsMap.put(indicator[0], "fr", indicator[1]);
                indicatorsMap.put(indicator[0], "en", indicator[2]);
                indicatorsMap.put(indicator[0], "es", indicator[3]);
                indicatorsMap.put(indicator[0], "unit", indicator[4]);
            }
        }

        return indicatorsMap;
    }

    /**
     * Get indicator translation by checking correct locale.
     *
     * @param indicator indicator code
     * @param locale    locale
     * @return indicator translation
     */
    protected String getIndicatorValue(String indicator, String localeCode) throws CoserBusinessException {
        String localLocaleCode = localeCode;
        if (!"fr".equals(localLocaleCode) && !"es".equals(localLocaleCode)
            && !"unit".equals(localLocaleCode)) {
            localLocaleCode = "en"; // en by default
        }
        return (String) getIndicatorsMap().get(indicator, localLocaleCode);
    }

    /**
     * Charge les zones disponibles depuis le fichier des zones.
     * (zoneid, zonename)
     *
     * @return zones map
     * @throws CoserBusinessException
     */
    public DataStorage getZonesMap() throws CoserBusinessException {

        if (zonesMap == null) {
            File zoneFile = config.getWebZonesFile();

            // l'operation n'est pas obligatoire pour tous les clients
            // lourd, le fichier peut donc ne pas exister
            if (zoneFile.isFile()) {
                zonesMap = commonService.loadCSVFile(zoneFile);
            } else {
                // fait volontairement un return new, n'affecte pas l'etat du
                // service
                return new MemoryDataStorage();
            }
        }

        return zonesMap;
    }

    /**
     * Retourne les nom d'une zone (avec la facade, l'année et la serie) en
     * fonction de l'id de la zone.
     *
     * @param zoneId zone id
     * @return zone name (or {@code null} if not found)
     * @throws CoserBusinessException
     */
    public String getZoneFullName(String zoneId) throws CoserBusinessException {
        DataStorage localZoneMap = getZonesMap();
        String resultName = null;
        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
        int zoneIndex = localZoneMap.indexOf(zoneId);
        if (zoneIndex != -1) {
            resultName = localZoneMap.get(zoneIndex)[2];
            resultName += " - " + localZoneMap.get(zoneIndex)[3];
            resultName += " - " + localZoneMap.get(zoneIndex)[4];
            resultName += " - " + localZoneMap.get(zoneIndex)[5];
        }
        return resultName;
    }

    /**
     * Retourne tous les projets qui ont des résultats.
     *
     * De la forme d'une liste de de path (à la tree path) :
     * ProjetName/SelectionName/ResultName
     *
     * @param beginDate           begin date (can be null)
     * @param endDate             end date (can be null)
     * @param onlyPubliableResult select only publiable results
     * @return results paths
     * @throws CoserBusinessException
     */
    public List<RSufiResultPath> findAllProjectWithResult(Date beginDate, Date endDate, boolean onlyPubliableResult) throws CoserBusinessException {
        List<RSufiResultPath> results = new ArrayList<RSufiResultPath>();

        // loop on projets
        File projectsDirectory = config.getRSufiProjectsDirectory();
        File[] projects = projectsDirectory.listFiles();
        if (projects != null) {
            for (File existingProject : projects) {
                if (existingProject.isDirectory()) {
                    String projectName = existingProject.getName();
                    Project p = new Project(projectName);

                    // loop on selections
                    File selectionsDirectory = new File(existingProject, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selections = selectionsDirectory.listFiles();
                    if (selections != null) {
                        for (File existingSelection : selections) {
                            if (existingSelection.isDirectory()) {
                                String selectionName = existingSelection.getName();
                                Selection s = new Selection(selectionName);

                                // loop on result
                                File rsufisDirectory = new File(existingSelection, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] rSufiResults = rsufisDirectory.listFiles();
                                if (rSufiResults != null) {
                                    for (File rSufiResult : rSufiResults) {
                                        if (rSufiResult.isDirectory()) {
                                            RSufiResult r = projectService.getRSufiResult(rSufiResult);

                                            boolean candidate = isCandidateResult(r, beginDate, endDate, onlyPubliableResult);
                                            if (candidate) {
                                                RSufiResultPath result = new RSufiResultPath(p, s, r);
                                                results.add(result);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return results;
    }

    /**
     * Test if result is valid with filtering.
     *
     * @param rsufiResult         rsufi result to test
     * @param beginDate           begin date (can be null)
     * @param endDate             end date (can be null)
     * @param onlyPubliableResult select only publiable results
     * @return if result is valid candidate
     */
    protected boolean isCandidateResult(RSufiResult rsufiResult, Date beginDate,
                                        Date endDate, boolean onlyPubliableResult) {

        boolean result = true;

        if (beginDate != null) {
            result &= rsufiResult.getCreationDate().compareTo(beginDate) >= 0;
        }

        if (endDate != null) {
            result &= rsufiResult.getCreationDate().compareTo(endDate) <= 0;
        }

        if (onlyPubliableResult) {
            result &= rsufiResult.isPubliableResult();
        }

        return result;
    }

    /**
     * Extract directory to custom directory.
     *
     * @param selectedResults    selected result paths
     * @param extractDirectory   extract directory (can be null)
     * @param publishDataResults result paths flaged with results export
     * @return extracted file (no automatically deleted)
     * @throws CoserBusinessException
     */
    public File performResultExtract(Collection<RSufiResultPath> selectedResults, Collection<RSufiResultPath> publishDataResults, File extractDirectory) throws CoserBusinessException {
        File prepareZip = null;
        try {

            // create zip file name not random name
            if (extractDirectory == null) {
                prepareZip = File.createTempFile("Coserextract-", ".zip");
            } else {
                // cas extraction vers un répertoire specifique
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                String zipName = "Coserextract" + dateFormat.format(new Date()) + ".zip";
                prepareZip = new File(extractDirectory, zipName);
            }

            // copy selectively all data to target directory
            MultipleFileFilter mFileFilters = new MultipleFileFilter();
            for (RSufiResultPath path : selectedResults) {
                // load projet, needed to known source data file name
                Project project = path.getProject();
                project = projectService.openProject(project.getName());

                OneResultFileFilter oneRFF = new OneResultFileFilter(config.getRSufiProjectsDirectory(),
                                                                     project, path.getSelection(), path.getRsufiResult(), publishDataResults.contains(path));
                mFileFilters.add(oneRFF);
            }

            // create zip temp file
            File projectsDirectory = config.getRSufiProjectsDirectory();

            //ZipUtil.compress(prepareZip, projectsDirectory, mFileFilters);
            // compress les projets à la racines de l'archive
            // sans le dossier "projects"
            Collection<File> files = FileUtil.getFilteredElements(projectsDirectory, mFileFilters, true);
            ZipUtil.compressFiles(prepareZip, projectsDirectory, files, false);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't prepare upload data", ex);
        }

        return prepareZip;
    }

    /**
     * Upload user selected result to coser web front-end using common http
     * client.
     *
     * TODO remove les 4 listes s'il y a mieux.
     *
     * @param selectedResults    selected result (collection of project/selection/rsufiresult)
     * @param indicatorsResults  results selected as indicator results
     * @param mapResults         results selected as map result
     * @param publishDataResults results selected as results published with data
     * @param login              remote admin login
     * @param password           remote admin password
     * @param progress           progress monitor
     * @return upload error status or {@code null} if no error
     * @throws CoserBusinessException
     */
    public String performResultUpload(Collection<RSufiResultPath> selectedResults,
                                      Collection<RSufiResultPath> indicatorsResults, Collection<RSufiResultPath> mapResults,
                                      Collection<RSufiResultPath> publishDataResults, String login,
                                      String password, ProgressMonitor progress) throws CoserBusinessException {

        String uploadStatus = null;

        // first copy prepare directory with only necessary data
        // ie project with only selected selections
        // and selection with only selected results

        progress.setCurrent(0);
        progress.setText(t("coser.business.uploadresult.modifyResultOptions"));
        modifyRSufiResults(selectedResults, indicatorsResults, mapResults, publishDataResults);

        progress.setText(t("coser.business.uploadresult.checkcollision"));
        checkDataCollision(selectedResults);

        progress.setText(t("coser.business.uploadresult.preparezip"));

        // default extract to temp directory with data sources
        File prepareZip = performResultExtract(selectedResults, publishDataResults, null);

        progress.setText(t("coser.business.uploadresult.sendzip"));
        progress.setTotal((int) prepareZip.length());

        // then upload zip file to website
        try {
            HttpClient httpclient = new DefaultHttpClient();
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            // login/password param (password encoded)
            StringBody loginBody = new StringBody(login, Charset.forName("UTF-8"));
            reqEntity.addPart("login", loginBody);
            String sha1password = StringUtil.encodeSHA1(password);
            StringBody passwordBody = new StringBody(sha1password, Charset.forName("UTF-8"));
            reqEntity.addPart("sha1Password", passwordBody);

            // file param
            ProgressStream stream = new ProgressStream(new FileInputStream(prepareZip), progress);
            InputStreamKnownSizeBody fileBody = new InputStreamKnownSizeBody(stream, prepareZip.length(),
                                                                             "application/zip", prepareZip.getName());
            reqEntity.addPart("resultFile", fileBody);

            HttpPost httppost = new HttpPost(config.getWebUploadURL());
            httppost.setEntity(reqEntity);

            if (log.isInfoEnabled()) {
                log.info("Uploading " + prepareZip + " to " + httppost.getURI());
            }

            HttpResponse response = httpclient.execute(httppost);

            if (response.getStatusLine().getStatusCode() != 200) {
                uploadStatus = response.getStatusLine().getReasonPhrase();
            }
        } catch (ClientProtocolException ex) {
            throw new CoserBusinessException("Can't upload file", ex);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't upload file", ex);
        }

        prepareZip.delete();

        return uploadStatus;
    }

    /**
     * Modifie les types et options de certains résultats rsufi (map result,
     * data sources result).
     *
     * @param selectedResults    selected result (collection of project/selection/rsufiresult)
     * @param indicatorsResults  results selected as indicator results
     * @param mapResults         map results
     * @param publishDataResults publish data results
     * @throws CoserBusinessException
     */
    protected void modifyRSufiResults(Collection<RSufiResultPath> selectedResults,
                                      Collection<RSufiResultPath> indicatorsResults, Collection<RSufiResultPath> mapResults,
                                      Collection<RSufiResultPath> publishDataResults) throws CoserBusinessException {

        // TODO echatellier 20110117 revoir ce code

        // attention, il faut sauver tout les resultats, sinon, les
        // decochage de type map / publish result ne seront pas pris en compte

        // reset type map and data source for all
        for (RSufiResultPath selectedResult : selectedResults) {
            RSufiResult rsufiResult = selectedResult.getRsufiResult();
            rsufiResult.setIndicatorsResult(false);
            rsufiResult.setMapsResult(false);
            rsufiResult.setDataAllowed(false);
        }

        // set map type
        for (RSufiResultPath indicatorsResult : indicatorsResults) {
            RSufiResult rsufiResult = indicatorsResult.getRsufiResult();
            rsufiResult.setIndicatorsResult(true);
        }

        // set map type
        for (RSufiResultPath mapResult : mapResults) {
            RSufiResult rsufiResult = mapResult.getRsufiResult();
            rsufiResult.setMapsResult(true);
        }

        // set data type
        for (RSufiResultPath publishDataResult : publishDataResults) {
            RSufiResult rsufiResult = publishDataResult.getRsufiResult();
            rsufiResult.setDataAllowed(true);
        }

        // save all selected results
        for (RSufiResultPath selectedResult : selectedResults) {
            Project project = selectedResult.getProject();
            Selection selection = selectedResult.getSelection();
            RSufiResult rsufiResult = selectedResult.getRsufiResult();

            File projectDirectory = new File(config.getRSufiProjectsDirectory(), project.getName());
            File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
            File selectionDirectory = new File(selectionsDirectory, selection.getName());
            File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
            File resultDirectory = new File(resultsDirectory, rsufiResult.getName());

            projectService.saveRSufiResult(resultDirectory, rsufiResult);
        }
    }

    /**
     * Met à jour les fichiers de propriétés des resultats (maps, dataSource)
     * and check for duplicated couple (zoneid/resulttype (map) upload).
     *
     * @param selectedResults result id to check
     * @throws CoserBusinessException
     */
    protected void checkDataCollision(Collection<RSufiResultPath> selectedResults) throws CoserBusinessException {

        Collection<String> resultZoneTypeIds = new ArrayList<String>();

        for (RSufiResultPath selectedResult : selectedResults) {
            Project project = selectedResult.getProject();
            Selection selection = selectedResult.getSelection();
            RSufiResult rsufiResult = selectedResult.getRsufiResult();

            // on creer une clé composé pour l'id du resultat
            String resultZoneTypeId = rsufiResult.getZone() + String.valueOf(rsufiResult.isMapsResult());
            if (resultZoneTypeIds.contains(resultZoneTypeId)) {
                throw new CoserBusinessException(t("coser.business.resultupload.duplicatedresult",
                                                   project.getName(), selection.getName(), rsufiResult.getName(), rsufiResult.getZone()));
            } else {
                resultZoneTypeIds.add(resultZoneTypeId);
            }
        }
    }

    /**
     * Aggrege plusieurs file filters.
     */
    protected static class MultipleFileFilter implements FileFilter {

        protected Collection<FileFilter> fileFilters = new ArrayList<FileFilter>();

        public void add(FileFilter f) {
            fileFilters.add(f);
        }

        /*
         * @see java.io.FileFilter#accept(java.io.File)
         */
        @Override
        public boolean accept(File pathname) {

            boolean result = false;
            Iterator<FileFilter> it = fileFilters.iterator();
            while (it.hasNext() && !result) {
                result = it.next().accept(pathname);
            }
            return result;
        }
    }

    /**
     * Filter pour un resultat donné.
     *
     * Attention, implémentation que ne doit fonctionner que avec ZipUtil
     * car meme si on refuse en répertoire, il redemande quand même
     * les fils (et il faut qu'il les demande)
     */
    protected class OneResultFileFilter implements FileFilter {

        protected File projectsDirectory;

        /** Doit etre un project chargé avec nom de fichier originaux. */
        protected Project project;

        protected Selection selection;

        protected RSufiResult rsufi;

        protected boolean exportWithData;

        public OneResultFileFilter(File projectsDirectory, Project project, Selection selection, RSufiResult rsufi, boolean exportWithData) {
            this.projectsDirectory = projectsDirectory;
            this.project = project;
            this.selection = selection;
            this.rsufi = rsufi;
            this.exportWithData = exportWithData;
        }

        /*
         * @see java.io.FileFilter#accept(java.io.File)
         */
        @Override
        public boolean accept(File pathname) {

            boolean result = false;

            try {
                String currentPathName = pathname.getCanonicalPath() + File.separator;

                File projectDirectory = new File(projectsDirectory, project.getName());
                File selectionsDirectory = new File(projectDirectory, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                File selectionDirectory = new File(selectionsDirectory, selection.getName());
                File resultsDirectory = new File(selectionDirectory, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                File resultDirectory = new File(resultsDirectory, rsufi.getName());

                String projectPath = projectDirectory.getCanonicalPath() + File.separator;
                String selectionsPath = selectionsDirectory.getCanonicalPath() + File.separator;
                String selectionPath = selectionDirectory.getCanonicalPath() + File.separator;
                String resultsPath = resultsDirectory.getCanonicalPath() + File.separator;
                String resultPath = resultDirectory.getCanonicalPath() + File.separator;

                // on prend
                // - tout ce qu'il y a dans le projet
                // - sauf le répertoire "selections"
                // - ou la selection entierement
                // - sauf le répertoire result
                // - ou le resultat entierrement
                result = (currentPathName.startsWith(projectPath)
                          && !currentPathName.startsWith(selectionsPath))
                         || (currentPathName.startsWith(selectionPath)
                             && !currentPathName.startsWith(resultsPath))
                         || currentPathName.startsWith(resultPath);

                // cas ou les données sources ne doivent pas être exporter
                // condition sur les noms de fichiers ?
                if (!exportWithData) {
                    String fileName = pathname.getName();

                    // on exclu tout les fichiers qui commencent
                    // par les memes noms de fichiers que ceux du projet
                    // (les noms de fichiers sont personnalisables)
                    for (Category category : Category.values()) {
                        if (category.isDataCategory()) {
                            String sourceFileName = commonService.getDataStorageFileName(project, category, null);
                            Matcher matcher = CoserUtils.FILENAME_SUFFIX_PATTERN.matcher(sourceFileName);
                            if (matcher.matches()) {
                                result &= !(fileName.startsWith(matcher.group(1)) && fileName.endsWith(matcher.group(2)));
                            } else {
                                result &= !fileName.startsWith(sourceFileName);
                            }
                        }
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException("Can't get system canonical path");
            }

            return result;
        }
    }

    /**
     * Traite le fichier uploade par l'application client et l'enregistre
     * dans le stockage coté web.
     *
     * Le nouveau fichier uploadé est mergé avec l'ancien, c'est à dire:
     * <ul>
     * <li>dezipage dans un fichier temporaire
     * <li>recuperation des noms de zones des nouveau fichiers (par type, carte/indicateur)
     * <li>suppression dans l'ancien répertoire des resultats deja presents dans le nouveau (pour les conflits zone)
     * <li>suppression des selections vides
     * <li>suppression des projets vides
     * <li>copie (avec ecrasement) des nouveaux fichiers dans l'ancien répertoire
     * mais seulement pour ceux des zones concernés (partie difficile)
     * </ul>
     *
     * TODO chatellier 20110125 l'algorithme n'est pour l'instant pas performant
     * et contient pas mal de code dupliqué, mais pour la v1.0 ca ira.
     *
     * @param login       user login
     * @param archiveFile uploaded file
     * @throws CoserBusinessException
     */
    public void registerNewUploadedResults(String login, File archiveFile) throws CoserBusinessException {

        try {

            // dezipage dans un fichier temporaire
            File tempDirectory = FileUtil.createTempDirectory("coser-upload-", "-tmp");
            ZipUtil.uncompress(archiveFile, tempDirectory);

            File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
            File mapsDirectory = config.getWebMapsProjectsDirectory();

            // recuperer les resultats actuels pour le mail de mise à jour
            Map<String, String> indicatorResults = getZonesIds(projectsDirectory, true, null, null);
            Map<String, String> mapsResults = getZonesIds(mapsDirectory, null, true, null);
            Map<String, String> dataResults = getZonesIds(projectsDirectory, null, null, true);

            // suppression des resultats qui ont été envoyé mais
            // ne sont ni maps result, ni indicator result
            Map<String, String> noIndicatorsResultZoneIds = getZonesIds(tempDirectory, false, null, null);
            cleanCurrentProjectDirectory(projectsDirectory, noIndicatorsResultZoneIds.keySet());
            Map<String, String> noMapsResultZoneIds = getZonesIds(tempDirectory, null, false, null);
            cleanCurrentProjectDirectory(mapsDirectory, noMapsResultZoneIds.keySet());
            Map<String, String> noDataResultZoneIds = getZonesIds(tempDirectory, null, null, false);

            // recuperation des noms zone des nouveau fichiers
            Map<String, String> indicatorsResultZoneIds = getZonesIds(tempDirectory, true, null, null);
            // suppression dans l'ancien répertoire des resultat deja present
            // dans le nouveau (pour les conflits)
            cleanCurrentProjectDirectory(projectsDirectory, indicatorsResultZoneIds.keySet());
            // creation du filter qui copiera juste ce qu'il faut
            FileFilter indicatorsFileFilter = getCopyFileFilter(tempDirectory, false);
            // copie (avec ecrasement) des nouveaux fichiers dans l'ancien répertoire
            customCopyDirectory(tempDirectory, projectsDirectory, indicatorsFileFilter);

            // recuperation des noms zone des nouveau fichiers
            Map<String, String> mapsResultZoneIds = getZonesIds(tempDirectory, null, true, null);
            // suppression dans l'ancien répertoire des resultat deja present
            // dans le nouveau (pour les conflits)
            cleanCurrentProjectDirectory(mapsDirectory, mapsResultZoneIds.keySet());
            // creation du filter qui copiera juste ce qu'il faut
            FileFilter mapsFileFilter = getCopyFileFilter(tempDirectory, true);
            // copie (avec ecrasement) des nouveaux fichiers dans l'ancien répertoire
            customCopyDirectory(tempDirectory, mapsDirectory, mapsFileFilter);

            // recuperation des nom de resultat data
            Map<String, String> dataResultZoneIds = getZonesIds(tempDirectory, null, null, true);

            FileUtils.deleteDirectory(tempDirectory);

            if (log.isInfoEnabled()) {
                log.info("Unzipping file " + archiveFile + " to " + projectsDirectory);
            }

            // generate email content
            StringBuilder content = new StringBuilder();

            int count = 0;
            content.append(t("coser.business.notificationmail.mapsresults") + "\n");
            for (Map.Entry<String, String> noMapsResultZoneId : noMapsResultZoneIds.entrySet()) {
                if (mapsResults.containsValue(noMapsResultZoneId.getValue())) {
                    content.append(" - " + t("coser.business.notificationmail.deleted",
                                             getZoneFullName(noMapsResultZoneId.getKey()),
                                             noMapsResultZoneId.getValue()) + "\n");
                    count++;
                }
            }
            for (Map.Entry<String, String> mapsResultZoneId : mapsResultZoneIds.entrySet()) {
                if (!mapsResults.containsValue(mapsResultZoneId.getValue())) {
                    content.append(" - " + t("coser.business.notificationmail.added",
                                             getZoneFullName(mapsResultZoneId.getKey()),
                                             mapsResultZoneId.getValue()) + "\n");
                    count++;
                }
            }
            content.append("\n");

            content.append(t("coser.business.notificationmail.indicatorsresults") + "\n");
            for (Map.Entry<String, String> noIndicatorsResultZoneId : noIndicatorsResultZoneIds.entrySet()) {
                if (indicatorResults.containsValue(noIndicatorsResultZoneId.getValue())) {
                    content.append(" - " + t("coser.business.notificationmail.deleted",
                                             getZoneFullName(noIndicatorsResultZoneId.getKey()),
                                             noIndicatorsResultZoneId.getValue()) + "\n");
                    count++;
                }

            }
            for (Map.Entry<String, String> indicatorsResultZoneId : indicatorsResultZoneIds.entrySet()) {
                if (!indicatorResults.containsValue(indicatorsResultZoneId.getValue())) {
                    content.append(" - " + t("coser.business.notificationmail.added",
                                             getZoneFullName(indicatorsResultZoneId.getKey()),
                                             indicatorsResultZoneId.getValue()) + "\n");
                    count++;
                }
            }
            content.append("\n");

            content.append(t("coser.business.notificationmail.dataresults") + "\n");
            for (Map.Entry<String, String> noDataResultZoneId : noDataResultZoneIds.entrySet()) {
                if (dataResults.containsValue(noDataResultZoneId.getValue())) {
                    content.append(" - " + t("coser.business.notificationmail.deleted",
                                             getZoneFullName(noDataResultZoneId.getKey()),
                                             noDataResultZoneId.getValue()) + "\n");
                    count++;
                }

            }
            for (Map.Entry<String, String> dataResultZoneId : dataResultZoneIds.entrySet()) {
                if (!dataResults.containsValue(dataResultZoneId.getValue())) {
                    content.append(" - " + t("coser.business.notificationmail.added",
                                             getZoneFullName(dataResultZoneId.getKey()),
                                             dataResultZoneId.getValue()) + "\n");
                    count++;
                }
            }
            content.append("\n");

            // send notification mails
            sendNewResultNotifications(login, count, content.toString());

            // update data date
            updateDataProperties();
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't uncompress file", ex);
        }
    }

    /**
     * N'utilise pas la methode de commons-fileutils, car lorsqu'un répertoire
     * est refusé, il ne descend pas dans les sous répertoire alors que dans
     * notre cas il le faut.
     *
     * @param srcDir               source directory to copy
     * @param destDir              destination directory
     * @param indicatorsFileFilter file filter for file to copy
     * @throws IOException
     */
    protected void customCopyDirectory(File srcDir,
                                       File destDir, FileFilter indicatorsFileFilter) throws IOException {
        List<File> files = FileUtil.getFilteredElements(srcDir, null, true);
        for (File file : files) {
            if (indicatorsFileFilter.accept(file)) {
                String path = file.getPath().substring(srcDir.getPath().length());

                File destFile = new File(destDir, path);
                if (file.isDirectory()) {
                    destFile.mkdirs();
                } else {
                    FileUtils.copyFile(file, destFile);
                }
            }
        }
    }

    /**
     * Met à jour certaines proprietes apres la mise à jour des données.
     *
     * @throws CoserBusinessException
     */
    protected void updateDataProperties() throws CoserBusinessException {

        File webProperties = config.getWebPropertiesFile();

        Properties props = new Properties();
        InputStream iStream = null;
        OutputStream oStream = null;
        try {
            if (webProperties.isFile()) {
                iStream = new FileInputStream(webProperties);
                props.load(iStream);
            }

            props.setProperty("updateDate", String.valueOf(new Date().getTime()));
            oStream = new FileOutputStream(webProperties);
            props.store(oStream, "Update data");
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save properties file", ex);
        } finally {
            IOUtils.closeQuietly(iStream);
            IOUtils.closeQuietly(oStream);
        }
    }

    /**
     * Retourne la date de dernière mise à jour des données du site web.
     *
     * Retourne une date bidon, si pas de dernière mise à jour.
     *
     * @return last data update date
     * @throws CoserBusinessException
     */
    public Date getLastDataUpdateDate() throws CoserBusinessException {
        Date dataUpdateDate = null;
        File webProperties = config.getWebPropertiesFile();
        if (webProperties.isFile()) {
            // get update date
            Properties props = new Properties();
            InputStream stream = null;
            try {
                stream = new FileInputStream(webProperties);
                props.load(stream);

                if (props.containsKey("updateDate")) {
                    String date = props.getProperty("updateDate");
                    long time = Long.parseLong(date);
                    dataUpdateDate = new Date(time);
                }

            } catch (IOException ex) {
                throw new CoserBusinessException("Can't read properties file", ex);
            } finally {
                IOUtils.closeQuietly(stream);
            }
        }

        if (dataUpdateDate == null) {
            dataUpdateDate = new Date(0);
        }

        return dataUpdateDate;
    }

    /**
     * Recupere dans un repertoire donné, les zoneid des resultat avec
     * pour chaque id, le nom du projet associé.
     *
     * Les boolean sont des grands {@code Boolean} car si la valeur
     * est {@code null}, on ne tient pas compte du critere lors de la recherche.
     *
     * @param scanDirectory    le repertoire a scanner
     * @param indicatorResults if true get indicator results
     * @param mapResults       if true get map results
     * @param dataResults      if true get data allowed result
     * @return une map de resultid/nom visuel des projets
     * @throws CoserBusinessException
     */
    protected Map<String, String> getZonesIds(File scanDirectory, Boolean indicatorResults,
                                              Boolean mapResults, Boolean dataResults) throws CoserBusinessException {

        Map<String, String> resultIds = new HashMap<String, String>();
        File[] projectFiles = scanDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {

                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {

                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // return result depending on result type
                                            if ((indicatorResults == null || rsufiResult.isIndicatorsResult() == indicatorResults)
                                                &&
                                                (mapResults == null || rsufiResult.isMapsResult() == mapResults)
                                                &&
                                                (dataResults == null || rsufiResult.isDataAllowed() == dataResults)) {
                                                String resultResultId = rsufiResult.getZone();
                                                if (StringUtils.isNotBlank(resultResultId)) {
                                                    String resultPath = projectFile.getName() + "/" +
                                                                        selectionFile.getName() + "/" +
                                                                        resultFile.getName();
                                                    resultIds.put(resultResultId, resultPath);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return resultIds;
    }

    /**
     * Retourne un file filter qui ne copiera que les dossiers resultat
     * correspondant au type demandé. Pour un resultat, le filtre devra
     * egalement copier les dossiers projet et selection correspondants.
     *
     * @param scanDirectory directory containing result to copy
     * @param mapResults    result type to get
     * @return aggragated file filter
     * @throws CoserBusinessException
     */
    protected FileFilter getCopyFileFilter(File scanDirectory, boolean mapResults) throws CoserBusinessException {

        MultipleFileFilter aggregateFileFilter = new MultipleFileFilter();

        File[] projectFiles = scanDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {

                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {

                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // return result depending on result type
                                            if ((mapResults && rsufiResult.isMapsResult()) ||
                                                (!mapResults && rsufiResult.isIndicatorsResult())) {

                                                Project p = new Project(projectFile.getName());
                                                Selection s = new Selection(selectionFile.getName());

                                                OneResultFileFilter resultFileFilter = new OneResultFileFilter(scanDirectory, p, s, rsufiResult, true);
                                                aggregateFileFilter.add(resultFileFilter);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return aggregateFileFilter;
    }

    /**
     * Fait le menage dans le dossier courant des projets en supprimant
     * tout les resulat qui ont un result id present dans la liste
     * {@code newResultIds}.
     *
     * Supprime egalement les selections qui n'ont plus de résultats et
     * les projets qui n'ont plus de selection.
     *
     * @param projectsDirectory projectsDirectory
     * @param newResultIds      new ids
     * @throws CoserBusinessException
     */
    protected void cleanCurrentProjectDirectory(File projectsDirectory, Collection<String> newResultIds) throws CoserBusinessException {

        try {
            File[] projectFiles = projectsDirectory.listFiles();
            if (projectFiles != null) {
                for (File projectFile : projectFiles) {
                    if (projectFile.isDirectory()) {
                        int projectSelectionCount = 0;

                        File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                        File[] selectionFiles = selectionsDirectory.listFiles();
                        if (selectionFiles != null) {
                            for (File selectionFile : selectionFiles) {
                                if (selectionFile.isDirectory()) {
                                    int selectionResultCount = 0;

                                    File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                    File[] resultFiles = resultsDirectory.listFiles();
                                    if (resultFiles != null) {
                                        for (File resultFile : resultFiles) {
                                            if (resultFile.isDirectory()) {
                                                RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);
                                                String resultResultId = rsufiResult.getZone();
                                                if (newResultIds.contains(resultResultId)) {
                                                    // un nouveau resulat utilsera ce resultid
                                                    FileUtils.deleteDirectory(resultFile);
                                                } else {
                                                    // un resultat valid trouvé, selection non a supprimer
                                                    selectionResultCount++;
                                                }
                                            }
                                        }
                                    }

                                    // si aucun resultat valide, suppression de la seletion
                                    if (selectionResultCount == 0) {
                                        FileUtils.deleteDirectory(selectionFile);
                                    } else {
                                        projectSelectionCount++;
                                    }
                                }
                            }
                        }

                        // si aucune selection avec resultat, suppression du projet
                        if (projectSelectionCount == 0) {
                            FileUtils.deleteDirectory(projectFile);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't delete directory", ex);
        }
    }

    /**
     * Envoi un mail de notification apres la publication des resultat à la
     * liste des adresses email renseignées dans la configuration.
     *
     * @param login  user login
     * @param count  updated data count
     * @param detail body mail detail
     */
    protected void sendNewResultNotifications(String login, int count, String detail) {
        List<String> emails = config.getNewResultNotificationList();

        for (String email : emails) {
            try {
                MultiPartEmail emailPart = new MultiPartEmail();
                emailPart.setHostName(config.getSmtpHost());
                emailPart.addTo(email);
                emailPart.setFrom("noreply-coser@ifremer.fr", "Coser");
                emailPart.setSubject(t("coser.business.notificationmail.subject", count));
                emailPart.setContent(t("coser.business.notificationmail.body", login, detail), "text/plain; charset=ISO-8859-9");

                // send mail
                emailPart.send();
            } catch (EmailException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't send mail", ex);
                }
            }
        }
    }

    /**
     * Get facades list (as facadeid/facadename).
     *
     * @return facades map
     * @throws CoserBusinessException
     */
    public Map<String, String> getFacades() throws CoserBusinessException {
        Map<String, String> facades = new LinkedHashMap<String, String>();
        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
        DataStorage zonesMap = getZonesMap();
        Iterator<String[]> itZone = zonesMap.iterator(true);
        while (itZone.hasNext()) {
            String[] tuple = itZone.next();
            // on a pas d'id pour les facades
            facades.put(tuple[1], tuple[2]);
        }
        return facades;
    }

    /**
     * Pour une zone principale, recupere la liste des couples sous-zone /
     * campagne qui sont disponible dans cette zone principale.
     *
     * @param facade         facade (le nom de la facade principale) (can be {@code null} : don't filter on facade)
     * @param onlyWithSource retourn zone liste with available source data
     * @param forMap         look in map directory
     * @return couple subzoneid/sub
     * @throws CoserBusinessException
     */
    public Map<String, String> getZoneForFacade(String facade, boolean onlyWithSource, boolean forMap) throws CoserBusinessException {
        Map<String, String> zonesForFacade = new HashMap<String, String>();

        // get subzone for main zone
        Collection<String> subZones = new ArrayList<String>();
        Iterator<String[]> itZone = getZonesMap().iterator(true);
        while (itZone.hasNext()) {
            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            String[] tuple = itZone.next();
            if (facade == null || tuple[1].equals(facade)) {
                subZones.add(tuple[0]);
            }
        }

        File projectsDirectory = null;
        if (forMap) {
            projectsDirectory = config.getWebMapsProjectsDirectory();
        } else {
            projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        }

        // get survey names in subZones collection
        File[] projectFiles = projectsDirectory.listFiles();

        // project iteration
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            if (!onlyWithSource || rsufiResult.isDataAllowed()) {
                                                String resultZoneId = rsufiResult.getZone();
                                                if (subZones.contains(resultZoneId)) {
                                                    String zoneid = resultZoneId;
                                                    // get zone name
                                                    // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"map"
                                                    int zoneIndex = getZonesMap().indexOf(zoneid);
                                                    String[] zoneData = getZonesMap().get(zoneIndex);
                                                    String zoneName = zoneData[3] + " - " + zoneData[4] + " - " + zoneData[5];

                                                    zonesForFacade.put(zoneid, zoneName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return zonesForFacade;
    }

    /**
     * Retourne les zones disponible par facade.
     *
     * @return couple facadeid/list<zoneid>
     * @throws CoserBusinessException
     */
    public Map<String, List<String>> getZoneByFacade() throws CoserBusinessException {
        Map<String, List<String>> zonesByFacade = new HashMap<String, List<String>>();

        // get subzone for main zone
        Iterator<String[]> itZone = getZonesMap().iterator(true);
        while (itZone.hasNext()) {
            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            String[] tuple = itZone.next();
            String facadeid = tuple[1];
            String zoneid = tuple[0];
            List<String> zones = zonesByFacade.get(facadeid);
            if (zones == null) {
                zones = new ArrayList<String>();
                zonesByFacade.put(facadeid, zones);
            }
            zones.add(zoneid);
        }

        return zonesByFacade;
    }

    /**
     * Recupere la liste des cartes pour chaque id de zone sous forme de Map.
     *
     * @return zone images map
     * @throws CoserBusinessException
     */
    public Map<String, String> getZonePictures() throws CoserBusinessException {
        Map<String, String> result = new HashMap<String, String>();

        Iterator<String[]> itZone = getZonesMap().iterator(true);
        while (itZone.hasNext()) {
            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            String[] tuple = itZone.next();
            result.put(tuple[0], tuple[9]);
        }

        return result;
    }

    /**
     * Recupere la liste des meta info pour chaque id de zone sous forme de Map.
     *
     * @param locale locale
     * @return zone meta info map
     * @throws CoserBusinessException
     */
    public Map<String, String> getZoneMetaInfo(Locale locale) throws CoserBusinessException {
        Map<String, String> result = new HashMap<String, String>();

        Iterator<String[]> itZone = getZonesMap().iterator(true);
        while (itZone.hasNext()) {
            // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"comment_en";"comment_es";"map"
            String[] tuple = itZone.next();
            if (locale != null && "fr".equals(locale.getLanguage())) {
                result.put(tuple[0], tuple[6]);
            } else if (locale != null && "es".equals(locale.getLanguage())) {
                result.put(tuple[0], tuple[8]);
            } else {
                result.put(tuple[0], tuple[7]);
            }
        }

        return result;
    }

    /**
     * Recuperer la liste des populations pour une zone donnée.
     *
     * @param zone   zone id
     * @param forMap for map directory
     * @return map species nom info>nom officiel
     * @throws CoserBusinessException
     */
    public Map<String, String> getSpecies(String zone, boolean forMap) throws CoserBusinessException {
        return getSpecies(Collections.singleton(zone), forMap);
    }

    /**
     * Recuperer la liste des populations pour un ensemble de zones donnée.
     *
     * @param zones  zones ids
     * @param forMap for map directory
     * @return map species nom info>nom officiel
     * @throws CoserBusinessException
     */
    public Map<String, String> getSpecies(Collection<String> zones, boolean forMap) throws CoserBusinessException {

        Map<String, String> result = new TreeMap<String, String>();

        File projectsDirectory = null;
        if (forMap) {
            projectsDirectory = config.getWebMapsProjectsDirectory();
        } else {
            projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        }

        File[] projectFiles = projectsDirectory.listFiles();

        // parcours des resultats disponibles
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && zones.contains(rsufiResult.getZone())) {

                                                // load project (without data to get reftax data)
                                                Project project = projectService.openProject(projectFile.getName(), projectsDirectory);

                                                Map<String, String> resultSpecies = getRsufiResultSpecies(project, resultFile, rsufiResult);
                                                result.putAll(resultSpecies);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Recupere la liste de toutes les especes nom sci et nom off à partir
     * d'un resultat.
     *
     * @param project         project
     * @param resultDirectory rsufi result directory
     * @param rsufiResult     rsufi result
     * @return map with each species code/species name
     * @throws CoserBusinessException
     */
    protected Map<String, String> getRsufiResultSpecies(Project project, File resultDirectory, RSufiResult rsufiResult) throws CoserBusinessException {
        Map<String, String> result = new HashMap<String, String>();

        // load reftax in memory
        Map<String, String> speciesNames = new HashMap<String, String>();
        Iterator<String[]> reftax = project.getRefTaxSpecies().iterator(true);
        while (reftax.hasNext()) {
            String[] tuple = reftax.next();

            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String speciesCode = tuple[3];
            // nom + auteur (sans ajout de parenthese : important)
            String speciesName = tuple[4] + " " + tuple[5];

            speciesNames.put(speciesCode, speciesName);
        }

        // un peu lourd mais reconstruit le path jusqu'au fichier estcomind
        File estPopIndFile = new File(resultDirectory, rsufiResult.getEstPopIndName());

        // Campagne Indicateur  Liste   Espece  Strate  Annee   Estimation  EcartType   CV
        DataStorage dataStorage = commonService.loadCSVFile(estPopIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        Iterator<String[]> estPopIndIterator = dataStorage.iterator(true);
        while (estPopIndIterator.hasNext()) {
            String[] tuple = estPopIndIterator.next();

            String specyCode = tuple[3];
            String specyName = speciesNames.get(specyCode);

            if (StringUtils.isNotEmpty(specyName)) {
                result.put(specyCode, specyName);
            }
        }

        return result;
    }

    /**
     * Retourne les indicateurs calculés avec leurs traductions scientifique
     * pour la zone et l'especes souhaitées.
     *
     * @param zone    zone id
     * @param species especes (if {@code null} look for com indicators)
     * @param locale  locale
     * @return la liste des indicateurs
     * @throws CoserBusinessException
     */
    public Map<String, String> getIndicators(String zone, String species, Locale locale) throws CoserBusinessException {
        Map<String, String> indicators = new TreeMap<String, String>();

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && rsufiResult.getZone().equals(zone)) {

                                                Map<String, String> resultIndicators = null;

                                                if (species == null) {
                                                    resultIndicators = getRsufiResultComIndicators(resultFile, rsufiResult, locale);
                                                } else {
                                                    resultIndicators = getRsufiResultPopIndicators(resultFile, rsufiResult, species, locale);
                                                }
                                                indicators.putAll(resultIndicators);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return indicators;
    }

    // Pour extraction

    /**
     * Retourne les indicateurs calculés avec leurs traductions scientifique
     * pour les zones souhaitées. Retournes les indicateurs de populations
     * de de communauté à la fois.
     *
     * @param zones     zones id
     * @param dataTypes data type
     * @param locale    locale
     * @return la liste des indicateurs
     * @throws CoserBusinessException
     * @since 1.4
     */
    public Map<String, String> getIndicators(Collection<String> zones, DataType dataType, Locale locale) throws CoserBusinessException {
        Map<String, String> indicators = new TreeMap<String, String>();

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (zones.contains(rsufiResult.getZone())) {

                                                Map<String, String> resultIndicators = null;
                                                if (dataType == DataType.COMMUNITY) {
                                                    resultIndicators = getRsufiResultComIndicators(resultFile, rsufiResult, locale);
                                                    indicators.putAll(resultIndicators);
                                                }
                                                if (dataType == DataType.POPULATION) {
                                                    resultIndicators = getRsufiResultPopIndicators(resultFile, rsufiResult, null, locale);
                                                    indicators.putAll(resultIndicators);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return indicators;
    }

    /**
     * Recupere la liste de toutes les especes nom sci et nom off à partir
     * d'un resultat.
     *
     * @param resultDirectory rsufi result directory
     * @param rsufiResult     result
     * @param species         to get indicator (can be {@code null} to not filter on species)
     * @param locale          locale
     * @return indicator for species
     * @throws CoserBusinessException
     */
    protected Map<String, String> getRsufiResultPopIndicators(File resultDirectory, RSufiResult rsufiResult, String species, Locale locale) throws CoserBusinessException {

        Map<String, String> result = new HashMap<String, String>();

        // le fichier estcomind
        File estPopIndFile = new File(resultDirectory, rsufiResult.getEstPopIndName());

        // Campagne Indicateur  Liste   Espece  Strate  Annee   Estimation  EcartType   CV
        DataStorage dataStorage = commonService.loadCSVFile(estPopIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        Iterator<String[]> estPopIndIterator = dataStorage.iterator(true);
        while (estPopIndIterator.hasNext()) {
            String[] tuple = estPopIndIterator.next();

            String specyCode = tuple[3];
            if (species == null || specyCode.equals(species)) {

                String indicatorCode = tuple[1];
                String translations = getIndicatorValue(indicatorCode, locale.getLanguage());
                if (translations == null) {
                    translations = "##" + indicatorCode + "##" + locale.getLanguage();
                }
                result.put(indicatorCode, translations);
            }
        }

        return result;
    }

    /**
     * Recupere la liste de toutes les especes nom sci et nom off à partir
     * d'un resultat.
     *
     * @param resultDirectory rsufi result directory
     * @param rsufiResult     result
     * @param locale          locale
     * @return indicator for species
     * @throws CoserBusinessException
     */
    protected Map<String, String> getRsufiResultComIndicators(File resultDirectory, RSufiResult rsufiResult, Locale locale) throws CoserBusinessException {

        Map<String, String> result = new HashMap<String, String>();

        // le fichier estcomind
        File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);

        Iterator<String[]> estPopIndIterator = dataStorage.iterator(true);
        while (estPopIndIterator.hasNext()) {
            String[] tuple = estPopIndIterator.next();

            String indicatorCode = tuple[1];
            String translations = getIndicatorValue(indicatorCode, locale.getLanguage());
            if (translations == null) {
                translations = "##" + indicatorCode + "##" + locale.getLanguage();
            }
            result.put(indicatorCode, translations);
        }

        return result;
    }

    /**
     * Retourne les listes sur lequel l'indicateur fournit a ete calculé.
     *
     * @param zone      zone id
     * @param indicator indicator
     * @param locale    locale
     * @return la liste des indicateurs
     * @throws CoserBusinessException
     */
    public Map<String, String> getIndicatorLists(String zone, String indicator, Locale locale) throws CoserBusinessException {
        // linked hash map (doit respecter l'ordre d'insertion)
        Map<String, String> indicators = new LinkedHashMap<String, String>();

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && rsufiResult.getZone().equals(zone)) {

                                                // le fichier contenant le code type espece (utile
                                                // pour les traductions des list d'indicateur)
                                                File codeTypeEspecesFile = new File(projectFile, CoserConstants.Category.TYPE_ESPECES.getStorageFileName());

                                                Map<String, String> resultIndicators = getRsufiResultComIndicatorLists(resultFile,
                                                                                                                       rsufiResult, codeTypeEspecesFile, indicator, locale);
                                                indicators.putAll(resultIndicators);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return indicators;
    }

    /**
     * Recupere les nom des listes sur lesquelle ont été calculé les
     * indicateurs avec leurs traductions.
     *
     * @param resultDirectory rsufi result directory
     * @param rsufiResult     result
     * @param indicator       indicator
     * @param locale          locale
     * @return indicator for species
     * @throws CoserBusinessException
     */
    protected Map<String, String> getRsufiResultComIndicatorLists(File resultDirectory,
                                                                  RSufiResult rsufiResult, File codeTypeEspecesFile, String indicator, Locale locale) throws CoserBusinessException {

        // linked hash map (doit respecter l'ordre d'insertion)
        Map<String, String> result = new LinkedHashMap<String, String>();

        // le fichier estcomind
        File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
        DataStorage dataStorageType = commonService.loadCSVFile(codeTypeEspecesFile, CoserConstants.CSV_SEPARATOR_CHAR);

        Iterator<String[]> estPopIndIterator = dataStorage.iterator(true);
        while (estPopIndIterator.hasNext()) {
            String[] tuple = estPopIndIterator.next();

            String indicatorCode = tuple[1];
            if (indicatorCode.equals(indicator)) {
                String list = tuple[2];

                // recherche de la traduction de l'id de liste
                // les liste sont a1, T1, T2 ...
                String listLetter = String.valueOf(list.charAt(0));
                String translation = "## " + list + " not found ##";
                Iterator<String[]> typeIterator = dataStorageType.iterator(true);
                while (typeIterator.hasNext()) {
                    // "Types";"Commentaire";"NumSys min";"NumSys max";"Code"
                    String[] tupleType = typeIterator.next();
                    if (tupleType[4].equals(listLetter)) {

                        // gestion du groupe "Tous"
                        // cas special, c'est la seule valeur du fichier
                        // code type espece qui a besoin d'une traduction
                        if (tupleType[4].equalsIgnoreCase("T")) {
                            if (locale != null && "fr".equals(locale.getLanguage())) {
                                translation = "Tous Liste " + list.charAt(1);
                            } else if (locale != null && "en".equals(locale.getLanguage())) {
                                translation = "Todo Lista " + list.charAt(1);
                            } else {
                                translation = "All List " + list.charAt(1);
                            }
                        } else {
                            // ajout de la traduction du nom de liste plus le numéro
                            if (locale != null && "fr".equals(locale.getLanguage())) {
                                translation = tupleType[0] + " Liste " + list.charAt(1);
                            } else if (locale != null && "en".equals(locale.getLanguage())) {
                                translation = tupleType[0] + " Lista " + list.charAt(1);
                            } else {
                                translation = tupleType[0] + " List " + list.charAt(1);
                            }
                        }
                        break;
                    }
                }
                result.put(list, translation);
            }
        }

        return result;
    }

    /**
     * Retourne les indicateurs calculés avec leurs traductions scientifique
     * pour la zone et l'especes souhaitées.
     *
     * @param zone      zone id
     * @param species   especes (if {@code null} look for com indicators
     * @param indicator indicator
     * @param list      indicator's list (if {@code null} look for pop indicators or no list selected
     * @param locale    locale
     * @return la liste des indicateurs
     * @throws CoserBusinessException
     */
    public File getChart(String zone, String species, String indicator, String list, Locale locale) throws CoserBusinessException {
        File result = null;

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && rsufiResult.getZone().equals(zone)) {

                                                // load project (without data to get reftax data)
                                                Project project = projectService.openProject(projectFile.getName(), projectsDirectory);
                                                String indicatorName = getIndicatorValue(indicator, locale.getLanguage());
                                                String unit = getIndicatorValue(indicator, "unit");

                                                String zoneDisplayName = getZoneFullName(zone);

                                                if (species == null) {
                                                    // le fichier contenant le code type espece (utile
                                                    // pour les traductions des list d'indicateur)
                                                    File codeTypeEspecesFile = new File(projectFile, CoserConstants.Category.TYPE_ESPECES.getStorageFileName());

                                                    // title = surveyName - indicateur
                                                    result = publicationService.getRsufiResultComChart(project, resultFile,
                                                                                                       rsufiResult, codeTypeEspecesFile, indicator, list,
                                                                                                       zoneDisplayName, indicatorName, unit, locale);
                                                } else {
                                                    // title = surveyName - indicateur - species
                                                    result = publicationService.getRsufiResultPopChart(project, resultFile,
                                                                                                       rsufiResult, species, indicator, zoneDisplayName, indicatorName,
                                                                                                       unit, locale);
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Retourne les indicateurs calculés avec leurs traductions scientifique
     * pour la zone et l'especes souhaitées.
     *
     * @param zone      zone id
     * @param species   especes (if {@code null} look for com indicators
     * @param indicator indicator
     * @param list      indicator's list (if {@code null} look for pop indicators or no list selected
     * @param locale    locale
     * @return la liste des indicateurs
     * @throws CoserBusinessException
     */
    public File getChartData(String zone, String species, String indicator, String list, Locale locale) throws CoserBusinessException {
        File result = null;

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && rsufiResult.getZone().equals(zone)) {
                                                result = getChartDataFile(projectsDirectory, projectFile, selectionFile, resultFile, rsufiResult, species, indicator, list, locale);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Generate chart data (as csv for population and as zip for community).
     *
     * @param projectsDirectory  projects storage directory
     * @param projectDirectory   current project directory
     * @param selectionDirectory selection directory
     * @param resultDirectory    rsufi result directory
     * @param rSufiResult        rsufi result
     * @param species            species (can be null for community)
     * @param indicator          indicator
     * @param list               indicator's list (if {@code null} look for pop indicators or no list selected
     * @param locale             locale
     * @return generated file (auto delete when jvm shutdown)
     * @throws CoserBusinessException
     */
    protected File getChartDataFile(File projectsDirectory, File projectDirectory, File selectionDirectory,
                                    File resultDirectory, RSufiResult rSufiResult, String species, String indicator, String list,
                                    Locale locale) throws CoserBusinessException {

        File result = null;

        try {
            // cas community (zip avec fichier meta)
            if (species == null) {
                File tempDir = FileUtil.createTempDirectory("coser-chartdata-", "-tmp");

                String surveyName = projectService.getProjectSurveyName(resultDirectory, rSufiResult);
                File baseDir = new File(tempDir, surveyName);
                baseDir.mkdirs();

                File csvFile = publicationService.getRsufiResultComChartData(resultDirectory, rSufiResult, indicator, list);
                File csvFileCopied = new File(baseDir, indicator + ".csv");
                FileUtils.copyFile(csvFile, csvFileCopied);
                csvFile.delete();

                // ajout du fichier d'information sur les espèces incluses dans
                // les calculs des indicateurs de communautés
                // load project (without data to get reftax data)
                Project project = projectService.openProject(projectDirectory.getName(), projectsDirectory);
                Selection selection = project.getSelections().get(selectionDirectory.getName());
                File metaFile = generateMetaFilePDF(project, selection, resultDirectory, rSufiResult, indicator, locale);
                File metaFileCopied = new File(baseDir, "Information.pdf");
                FileUtils.copyFile(metaFile, metaFileCopied);

                // make zip
                result = File.createTempFile("coser-chartdata-", ".zip");
                result.deleteOnExit();
                ZipUtil.compress(result, baseDir);

                // clean directory
                FileUtils.deleteDirectory(tempDir);
            } else {
                // cas pop, fichier csv brut
                result = publicationService.getRsufiResultPopChartData(resultDirectory, rSufiResult, species, indicator);
            }

        } catch (IOException ex) {
            throw new CoserBusinessException("Can't generate chart data file", ex);
        }

        return result;
    }

    /**
     * Recupere le fichier image de la carte demandées en fonction de la zone
     * et de l'espece.
     *
     * Retourne également la repartition globale sur la zone si le nom de
     * l'espece est {@code null}.
     *
     * @param zone    zone (zoneid)
     * @param species species or (null to get <survey>_Repartition-stations.png map file)
     * @return map file
     * @throws CoserBusinessException
     */
    public File getMapFile(String zone, String species) throws CoserBusinessException {

        File result = null;

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebMapsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && rsufiResult.getZone().equals(zone) && rsufiResult.isMapsResult()) {
                                                // get survey name (other condition)
                                                String surveyName = projectService.getProjectSurveyName(resultFile, rsufiResult);

                                                String mapName = null;
                                                if (species != null) {
                                                    mapName = surveyName + "_" + species.toUpperCase() + ".png";
                                                } else {
                                                    mapName = surveyName + "_Repartition-stations.png";
                                                }

                                                File mapsDirectory = new File(resultFile, CoserConstants.STORAGE_MAPS_DIRECTORY);
                                                result = new File(mapsDirectory, mapName);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Genere un fichier zip des sources d'un projet.
     *
     * Contient:
     * <ul>
     * <li>les 4 fichiers apres sélection
     * <li>un fichier de décharge (pdf)
     * </ul>
     *
     * @param zone   zone (zoneid-surveyname)
     * @param locale locale
     * @return zip source file (auto delete when jvm shutdown)
     * @throws CoserBusinessException
     */
    public File getSourceZip(String zone, Locale locale) throws CoserBusinessException {

        File result = null;

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.getZone() != null && rsufiResult.getZone().equals(zone)) {

                                                // load project (with data to get original file names)
                                                Project project = projectService.openProject(projectFile.getName(), projectsDirectory);

                                                // load selection data (to do data export rsufi)
                                                Selection selection = project.getSelections().get(selectionFile.getName());

                                                // be sure that data are available for this project
                                                // or it will fail
                                                projectService.loadSelectionData(projectsDirectory, project, selection);

                                                result = generateSourceZip(project, selection, resultFile, rsufiResult, locale);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Generate zip for selection.
     *
     * Be sure that data are available for this project.
     *
     * @param project         project
     * @param selection       selection with loaded data
     * @param resultDirectory rsufi result directory
     * @param rSufiResult     rsufi result
     * @param locale          generated pdf locale
     * @return generated zip file (auto delete when jvm shutdown)
     * @throws CoserBusinessException
     */
    protected File generateSourceZip(Project project, Selection selection, File resultDirectory,
                                     RSufiResult rSufiResult, Locale locale) throws CoserBusinessException {

        if (!rSufiResult.isDataAllowed()) {
            throw new CoserBusinessException("Can't download source for non allowed result");
        }

        File resultZip = null;

        try {
            File tempDir = FileUtil.createTempDirectory("coser-source-", "-tmp");

            // il ne faut pas les fichiers de selection, mais leurs
            // export rsufi (sans les lignes, et les quotes)
            File archiveDir = projectService.extractRSUfiData(project, selection, tempDir, true);

            // add decharge file
            String filename = null;
            if (locale != null && "fr".equals(locale.getLanguage())) {
                filename = "DechargeDonnees.pdf";
            } else if (locale != null && "es".equals(locale.getLanguage())) {
                filename = "DatosDeExencionDeResponsabilidad.pdf";
            } else {
                filename = "DataDisclaimer.pdf";
            }
            File dechargePDF = new File(archiveDir, filename);
            generateDechargePDF(dechargePDF, resultDirectory, rSufiResult, locale);

            // ajout du reftax dans le zip
            File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
            File projectDirectory = new File(projectsDirectory, project.getName());
            File reftaxFile = new File(projectDirectory, CoserConstants.Category.REFTAX_SPECIES.getStorageFileName());
            FileUtils.copyFileToDirectory(reftaxFile, archiveDir);

            // make zip
            resultZip = File.createTempFile("coser-source-", ".zip");
            resultZip.deleteOnExit();
            ZipUtil.compress(resultZip, archiveDir);

            // clean directory
            FileUtils.deleteDirectory(tempDir);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't create zip file", ex);
        }

        return resultZip;
    }

    /**
     * Genere le PDF dynamique de decharge à partir du template freemarker.
     *
     * @param disclamerPdf    pdf file to generate
     * @param resultDirectory rsufi result directory
     * @param rSufiResult     rsufi result
     * @param locale          generated pdf locale
     * @return le fichier généré
     * @throws CoserBusinessException
     */
    public File generateDechargePDF(File disclamerPdf, File resultDirectory, RSufiResult rSufiResult, Locale locale) throws CoserBusinessException {

        File result = null;

        OutputStream os = null;

        try {
            // get some info to put into pdf
            Date updateDate = getLastDataUpdateDate();

            // il  se peut que pour l'extraction un fichier de décharge ne soit
            // pas généré pour une campagne en particulier
            // on passe un nom vide a freemarker
            String surveyName = "";
            if (resultDirectory != null && rSufiResult != null) {
                surveyName = projectService.getProjectSurveyName(resultDirectory, rSufiResult);
            }

            // render freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate("decharge.ftl", locale);

            Map<String, Object> root = new HashMap<String, Object>();
            root.put("updateDate", updateDate);
            root.put("surveyName", surveyName);

            Writer out = new StringWriter();
            mapTemplate.process(root, out);
            out.flush();

            // get content as w3c document
            Document document = CoserUtils.parseDocument(out.toString());

            // render template output as pdf
            os = new FileOutputStream(disclamerPdf);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(document, null);
            renderer.layout();
            renderer.createPDF(os);

            os.close();
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't generate decharge file", ex);
        } catch (TemplateException ex) {
            throw new CoserBusinessException("Can't generate decharge file", ex);
        } catch (DocumentException ex) {
            throw new CoserBusinessException("Can't generate decharge file", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }

        return result;
    }

    /**
     * Genere le fichier PDF d'information sur les espèces incluses dans les
     * calculs des indicateurs de communautés, à jointe à chaque téléchargement.
     *
     * @param project         project
     * @param selection       selection
     * @param resultDirectory result directory
     * @param rsufiResult     rsufi result
     * @param indicator       indicator
     * @param locale          locale
     * @return generated pdf file
     * @throws CoserBusinessException
     */
    protected File generateMetaFilePDF(Project project, Selection selection, File resultDirectory,
                                       RSufiResult rsufiResult, String indicator, Locale locale) throws CoserBusinessException {

        File result = null;

        // chargement du reftax et des code types especes pour connaitre
        // le type des especes de poissons
        // parcourt du fichier des types de données
        Map<String, Integer> refTaxSpeciesNumSys = new HashMap<String, Integer>();
        Map<String, String> refTaxSpeciesName = new HashMap<String, String>();
        Iterator<String[]> itReftax = project.getRefTaxSpecies().iterator(true);
        while (itReftax.hasNext()) {
            String[] tuple = itReftax.next();
            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String speciesCode = tuple[3];
            Integer iNumSys = Integer.valueOf(tuple[1]);
            refTaxSpeciesNumSys.put(speciesCode, iNumSys);

            // fix html entities bug
            String speciesSciName = StringEscapeUtils.escapeXml(tuple[4]);
            String speciesAuthor = StringEscapeUtils.escapeXml(tuple[5]);

            // TODO little hack for italic species
            refTaxSpeciesName.put(speciesCode, "<span style='font-style:italic'>" + speciesSciName + "</span> " + speciesAuthor);
        }

        // code type / especes
        Map<String, Integer[]> specyTypes = new HashMap<String, Integer[]>();
        Iterator<String[]> itTypeSpecies = project.getTypeEspeces().iterator(true);
        while (itTypeSpecies.hasNext()) {
            // "Types";"Commentaire";"NumSys min";"NumSys max","Code"
            String[] tuple = itTypeSpecies.next();
            String specyTypeCode = tuple[4];

            Integer iMinNumSys = Integer.valueOf(tuple[2]);
            Integer iMaxNumSys = Integer.valueOf(tuple[3]);
            specyTypes.put(specyTypeCode, new Integer[]{iMinNumSys, iMaxNumSys});
        }

        // le fichier estpopind
        File estComIndFile = new File(resultDirectory, rsufiResult.getEstComIndName());

        // donnees intermediare (map liste id > liste des indicateurs)
        Map<String, SortedSet<String>> indicatorMap = new HashMap<String, SortedSet<String>>();

        // le resutat sera une map complexe
        // map liste id > liste des especes (nom complet)
        Map<String, SortedSet<String>> speciesMap = new HashMap<String, SortedSet<String>>();

        // Campagne Indicateur Liste Strate Annee Estimation EcartType CV
        DataStorage dataStorage = commonService.loadCSVFile(estComIndFile, CoserConstants.CSV_RESULT_SEPARATOR_CHAR);
        Iterator<String[]> estComIndIterator = dataStorage.iterator(true);
        while (estComIndIterator.hasNext()) {
            String[] tuple = estComIndIterator.next();

            String indicatorCode = tuple[1];
            String listIdCode = tuple[2]; // c1, p2, T3 ...

            String listNumber = listIdCode.substring(1); // 1, 2, 3

            // get indicator list
            SortedSet<String> indicatorList = indicatorMap.get(listNumber);
            if (indicatorList == null) {
                indicatorList = new TreeSet<String>();
                indicatorMap.put(listNumber, indicatorList);
            }

            // get indicator full name
            String indicatorName = getIndicatorValue(indicatorCode, locale.getLanguage());
            // peut arriver pour les indicateurs inconnu par coser
            if (indicatorName != null) {
                indicatorList.add(indicatorName);
            }
        }

        // seconde pass, remplit la map speciesMap avec les listes configurées
        // dans la selection
        for (String listNumber : indicatorMap.keySet()) {
            List<String> selectionSpeciesList = null;
            if ("1".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpecies();
            } else if ("2".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpeciesOccDens();
            } else if ("3".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpeciesSizeAllYear();
            } else if ("4".equals(listNumber)) {
                selectionSpeciesList = selection.getSelectedSpeciesMaturity();
            }

            if (selectionSpeciesList != null) {
                SortedSet<String> speciesList = new TreeSet<String>();

                for (String speciesCode : selectionSpeciesList) {
                    // get species full name
                    String speciesName = refTaxSpeciesName.get(speciesCode);

                    // recupere le code type de l'espece, "m", "c", "p" ...
                    Integer speciesNumSys = refTaxSpeciesNumSys.get(speciesCode);
                    for (Map.Entry<String, Integer[]> speciesTypeEntry : specyTypes.entrySet()) {
                        String speciesTypeCode = speciesTypeEntry.getKey();
                        Integer[] bound = speciesTypeEntry.getValue();

                        if (speciesNumSys >= bound[0] && speciesNumSys <= bound[1]) {
                            speciesName = "(" + speciesTypeCode + ") " + speciesName;
                            break;
                        }
                    }
                    // end code type espece

                    speciesList.add(speciesName);
                }
                speciesMap.put(listNumber, speciesList);
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get species list for list id " + listNumber);
                }
            }
        }

        OutputStream os = null;
        try {
            // render freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate("metainfo.ftl", locale);

            Map<String, Object> root = new HashMap<String, Object>();
            root.put("indicatorsMap", indicatorMap);
            root.put("speciesMap", speciesMap);

            Writer out = new StringWriter();
            mapTemplate.process(root, out);
            out.flush();

            // get content as w3c document
            Document document = CoserUtils.parseDocument(out.toString());

            // render template output as pdf
            result = File.createTempFile("coser-metainfo-", ".pdf");
            result.deleteOnExit();
            os = new FileOutputStream(result);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(document, null);
            renderer.layout();
            renderer.createPDF(os);

            os.close();
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't generate meta info file", ex);
        } catch (TemplateException ex) {
            throw new CoserBusinessException("Can't generate meta info file", ex);
        } catch (DocumentException ex) {
            throw new CoserBusinessException("Can't generate meta info file", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }

        return result;
    }

    /**
     * Recupere dans le repertoire des projets d'indicateur les resultats
     * disponible par zone (il ne peut y en avoir qu'un par zone).
     *
     * @return une map avec par zone, son resultat associé (ProjectName/SelectionName)
     * @throws CoserBusinessException
     */
    public Map<String, String> getIndicatorsResultsPerZone() throws CoserBusinessException {
        return getResultsPerZone(config.getWebIndicatorsProjectsDirectory());
    }

    /**
     * Recupere dans le repertoire des projets d'indicateur les resultats
     * disponible par zone (il ne peut y en avoir qu'un par zone).
     *
     * @return une map avec par zone, son resultat associé (ProjectName/SelectionName)
     * @throws CoserBusinessException
     */
    public Map<String, String> getMapsResultsPerZone() throws CoserBusinessException {
        return getResultsPerZone(config.getWebMapsProjectsDirectory());
    }

    /**
     * Recupere dans le repertoire des projets d'indicateur les resultats
     * disponible par zone (il ne peut y en avoir qu'un par zone).
     *
     * @param scanDirectory le repertoire a scanner
     * @return une map avec par zone, son resultat associé (ProjectName/SelectionName/RSUfiName)
     * @throws CoserBusinessException
     */
    protected Map<String, String> getResultsPerZone(File scanDirectory) throws CoserBusinessException {

        Map<String, String> resultIds = new HashMap<String, String>();
        File[] projectFiles = scanDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {

                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {

                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);
                                            String resultResultId = rsufiResult.getZone();
                                            String name = projectFile.getName() + "/" + selectionFile.getName()
                                                          + "/" + resultFile.getName();
                                            resultIds.put(resultResultId, name);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return resultIds;
    }

    /**
     * Supprime des résultats par leur identifiant de zone de ratachement (car
     * un seul resultat par zone).
     *
     * Utilisé par l'interface d'admin.
     *
     * @param deleteZoneId
     * @throws CoserBusinessException
     */
    public void deleteIndicatorsResult(List<String> deleteZoneId) throws CoserBusinessException {
        cleanCurrentProjectDirectory(config.getWebIndicatorsProjectsDirectory(), deleteZoneId);
    }

    /**
     * Supprime des résultats par leur identifiant de zone de ratachement (car
     * un seul resultat par zone).
     *
     * Utilisé par l'interface d'admin.
     *
     * @param deleteZoneId
     * @throws CoserBusinessException
     */
    public void deleteMapsResult(List<String> deleteZoneId) throws CoserBusinessException {
        cleanCurrentProjectDirectory(config.getWebMapsProjectsDirectory(), deleteZoneId);
    }

    /**
     * Extrait de toutes les données les informations demandées restreintes
     * sur certaines zone, certaines espèces et certains indicateurs.
     *
     * Ajout en plus dans le zip les cartes, les sources... suivant les
     * types selectionnés.
     *
     * @param selectZones   zones
     * @param selectTypes   types
     * @param selectSpecies species
     * @param comIndicators indicators
     * @param popIndicators indicators
     * @return le zip
     * @since 1.4
     */
    public File extractData(List<String> zones, List<DataType> types, List<String> species,
                            List<String> comIndicators, List<String> popIndicators, Locale locale) throws CoserBusinessException {

        File resultZip = null;
        File tempDir = null;
        try {
            tempDir = FileUtil.createTempDirectory("coser-extract-", "-tmp");

            File subDir = new File(tempDir, "Indicateurs_Ifremer");
            subDir.mkdirs();

            // les sources se retrouve dans le zip a cote du pdf
            if (types.contains(DataType.SOURCE)) {
                if (log.isDebugEnabled()) {
                    log.debug("Extracting sources");
                }
                File srcDir = new File(subDir, "sources");
                extractSource(zones, srcDir);
            }

            // les cartes doivent se retrouver dans le pdf
            MultiKeyMap pdfMaps = null;
            if (types.contains(DataType.MAP)) {
                if (log.isDebugEnabled()) {
                    log.debug("Extracting maps");
                }
                pdfMaps = extractDataMap(zones, species);
            }

            // les graphiques sont également dans le pdf
            MultiKeyMap pdfCharts = null;
            if (CollectionUtils.isNotEmpty(comIndicators) || CollectionUtils.isNotEmpty(popIndicators)) {
                if (log.isDebugEnabled()) {
                    log.debug("Extracting charts");
                }
                pdfCharts = extractCharts(zones, species, comIndicators, popIndicators, locale);
            }

            // generate pdf if necessary
            if (MapUtils.isNotEmpty(pdfMaps) || MapUtils.isNotEmpty(pdfCharts)) {
                generateExtractPDF(subDir, zones, pdfMaps, pdfCharts, locale);
            }

            // fichier de décharge en pdf
            String filename = null;
            if (locale != null && "fr".equals(locale.getLanguage())) {
                filename = "DechargeDonnees.pdf";
            } else if (locale != null && "es".equals(locale.getLanguage())) {
                filename = "DatosDeExencionDeResponsabilidad.pdf";
            } else {
                filename = "DataDisclaimer.pdf";
            }
            File dechargePDF = new File(subDir, filename);
            generateDechargePDF(dechargePDF, null, null, locale);

            // make zip
            resultZip = File.createTempFile("coser-extract-", ".zip");
            resultZip.deleteOnExit();
            ZipUtil.compress(resultZip, subDir);

            // clean directory
            FileUtils.deleteDirectory(tempDir);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't create zip file", ex);
        } finally {
            // clean directory
            FileUtils.deleteQuietly(tempDir);
        }
        return resultZip;
    }

    /**
     * Extrait les cartes.
     *
     * @param zones   zones (zoneid)
     * @param species species
     * @return map file (zone/speciesname/mapfile)
     * @throws CoserBusinessException
     * @throws IOException
     * @since 1.4
     */
    protected MultiKeyMap extractDataMap(Collection<String> zones, Collection<String> species) throws CoserBusinessException, IOException {

        MultiKeyMap mapForZoneAndSpecies = new MultiKeyMap();

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebMapsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.isMapsResult() && zones.contains(rsufiResult.getZone())) {

                                                // load project (to get user display species name)
                                                Project project = projectService.openProject(projectFile.getName(), projectsDirectory);

                                                // get survey name (other condition)
                                                String surveyName = projectService.getProjectSurveyName(resultFile, rsufiResult);

                                                File mapsDirectory = new File(resultFile, CoserConstants.STORAGE_MAPS_DIRECTORY);
                                                for (String aSpecies : species) {
                                                    String mapName = surveyName + "_" + aSpecies.toUpperCase() + ".png";
                                                    File mapFile = new File(mapsDirectory, mapName);
                                                    if (mapFile.isFile()) {
                                                        String speciesName = project.getDisplaySpeciesText(aSpecies);
                                                        mapForZoneAndSpecies.put(rsufiResult.getZone(), speciesName, mapFile);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return mapForZoneAndSpecies;
    }

    /**
     * Genere un fichier zip des sources d'un projet.
     *
     * Contient:
     * <ul>
     * <li>les 4 fichiers apres sélection
     * </ul>
     *
     * @param zone   zone (zoneid-surveyname)
     * @param locale locale
     * @return zip source file (auto delete when jvm shutdown)
     * @throws CoserBusinessException
     * @since 1.4
     */
    protected File extractSource(Collection<String> zones, File directory) throws CoserBusinessException {

        File result = null;

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (rsufiResult.isDataAllowed() && zones.contains(rsufiResult.getZone())) {

                                                // load project (with data to get original file names)
                                                Project project = projectService.openProject(projectFile.getName(), projectsDirectory);

                                                // load selection data (to do data export rsufi)
                                                Selection selection = project.getSelections().get(selectionFile.getName());

                                                // be sure that data are available for this project
                                                // or it will fail
                                                projectService.loadSelectionData(projectsDirectory, project, selection);

                                                // il ne faut pas les fichiers de selection, mais leurs
                                                // export rsufi (sans les lignes, et les quotes)
                                                File zoneDirectory = new File(directory, rsufiResult.getZone());
                                                zoneDirectory.mkdirs();
                                                projectService.extractRSUfiData(project, selection, zoneDirectory, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Retourne les indicateurs calculés avec leurs traductions scientifique
     * pour la zone et l'especes souhaitées.
     *
     * @param zone          zone id
     * @param species       especes (if {@code null} look for com indicators
     * @param comIndicator  comIndicator
     * @param popIndicators popIndicators
     * @param locale        locale
     * @return la liste des indicateurs (zone, speciesname, [graphfile, graphdata])
     * @throws CoserBusinessException
     */
    protected MultiKeyMap extractCharts(Collection<String> zones, Collection<String> species,
                                        Collection<String> comIndicators, Collection<String> popIndicators, Locale locale) throws CoserBusinessException {

        MultiKeyMap chartFileAndDatas = new MultiKeyMap();

        // parcours des resultats disponibles
        File projectsDirectory = config.getWebIndicatorsProjectsDirectory();
        File[] projectFiles = projectsDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {
                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();

                    // selection iteration
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {
                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();

                                // result iteration
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // extraction des especes pour le résultat demandé
                                            if (zones.contains(rsufiResult.getZone())) {

                                                if (log.isDebugEnabled()) {
                                                    log.debug("Extracting charts for result " + resultFile);
                                                }

                                                // load project (without data to get reftax data)
                                                Project project = projectService.openProject(projectFile.getName(), projectsDirectory);
                                                String zone = rsufiResult.getZone();
                                                String zoneDisplayName = getZoneFullName(zone);

                                                // le fichier contenant le code type espece (utile
                                                // pour les traductions des list d'indicateur)
                                                File codeTypeEspecesFile = new File(projectFile, CoserConstants.Category.TYPE_ESPECES.getStorageFileName());

                                                if (CollectionUtils.isNotEmpty(comIndicators)) {
                                                    Map<String, Object[]> chartFileAndDataCom = publicationService.getRsufiResultComCharts(project,
                                                                                                                                           resultFile, rsufiResult, codeTypeEspecesFile, comIndicators,
                                                                                                                                           zoneDisplayName, getIndicatorsMap(), locale, 650, 430);
                                                    // put in multimap as zone,speciesname, data
                                                    for (Entry<String, Object[]> entry : chartFileAndDataCom.entrySet()) {
                                                        chartFileAndDatas.put(zone, entry.getKey(), entry.getValue());
                                                    }
                                                }

                                                if (CollectionUtils.isNotEmpty(popIndicators)) {
                                                    Map<String, Object[]> chartFileAndDataPop = publicationService.getRsufiResultPopCharts(project,
                                                                                                                                           resultFile, rsufiResult, species, popIndicators,
                                                                                                                                           zoneDisplayName, getIndicatorsMap(), locale, 650, 430);
                                                    // put in multimap as zone,speciesname, data
                                                    for (Entry<String, Object[]> entry : chartFileAndDataPop.entrySet()) {
                                                        chartFileAndDatas.put(zone, entry.getKey(), entry.getValue());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return chartFileAndDatas;
    }

    /**
     * Generate pdf file filled with maps and charts.
     *
     * @param directory directory to generate pdf to
     * @param zones     zones ids
     * @param pdfMaps   pdf maps (can be {@code null})
     * @param pdfCharts pdf charts (can be {@code null})
     * @throws CoserBusinessException
     */
    protected void generateExtractPDF(File directory, List<String> zones,
                                      MultiKeyMap pdfMaps, MultiKeyMap pdfCharts, Locale locale) throws CoserBusinessException {

        for (String zone : zones) {
            Collection<File> toDelete = new ArrayList<File>();
            OutputStream os = null;

            try {
                StringBuilder htmlContent = new StringBuilder();
                htmlContent.append("<html><head>");
                htmlContent.append("<title>" + l(locale, "coser.business.extract.extracttitle") + "</title>");
                htmlContent.append("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
                htmlContent.append("</head><body>");

                if (pdfMaps != null) {
                    for (Entry<MultiKey, File> mapEntry : (Set<Entry<MultiKey, File>>) pdfMaps.entrySet()) {
                        String zoneId = (String) mapEntry.getKey().getKey(0);
                        if (zoneId.equals(zone)) {
                            String speciesName = (String) mapEntry.getKey().getKey(1);
                            File file = mapEntry.getValue();

                            String zoneName = getZoneFullName(zoneId);
                            htmlContent.append("<div style='page-break-after: always'>");
                            htmlContent.append("<p>" + zoneName + " - " + speciesName + "</p>");
                            htmlContent.append("<img src='file://" + file.getAbsolutePath() + "' />");
                            htmlContent.append("</div>");
                        }
                    }
                }

                if (pdfCharts != null) {
                    for (Entry<MultiKey, Object[]> chartFileAndData : (Set<Entry<MultiKey, Object[]>>) pdfCharts.entrySet()) {
                        String zoneId = (String) chartFileAndData.getKey().getKey(0);
                        if (zoneId.equals(zone)) {
                            File chartFile = (File) chartFileAndData.getValue()[0];
                            String content = (String) chartFileAndData.getValue()[1];

                            htmlContent.append("<div style='page-break-after: always'>");
                            htmlContent.append("<img src='file://" + chartFile.getAbsolutePath() + "' />");
                            htmlContent.append("<br />");
                            htmlContent.append(l(locale, "coser.business.extract.extractdata") + " :");
                            htmlContent.append("<pre>").append(content).append("</pre>");
                            htmlContent.append("</div>");

                            // les graphiques ont été générés, a supprimer donc
                            // a ne surtout pas faire avec les cartes !!!
                            toDelete.add(chartFile);
                        }
                    }
                }

                htmlContent.append("</body></html>");

                // get content as w3c document
                Document document = CoserUtils.parseDocument(htmlContent.toString());

                // render template output as pdf
                // remove accents and strange characters from zone display name
                String zoneDisplay = getZoneFullName(zone);
                zoneDisplay = StringUtils.stripAccents(zoneDisplay);
                zoneDisplay = zoneDisplay.replaceAll("[^\\w- ]", "_");
                File pdfFile = new File(directory, zoneDisplay + ".pdf");
                os = new FileOutputStream(pdfFile);

                ITextRenderer renderer = new ITextRenderer();
                renderer.setDocument(document, null);
                renderer.layout();
                renderer.createPDF(os);

                os.close();
            } catch (IOException ex) {
                throw new CoserBusinessException("Can't generate log pdf", ex);
            } catch (DocumentException ex) {
                throw new CoserBusinessException("Can't generate log pdf", ex);
            } finally {
                IOUtils.closeQuietly(os);

                // delete file collection
                for (File file : toDelete) {
                    file.delete();
                }
            }
        }
    }
}

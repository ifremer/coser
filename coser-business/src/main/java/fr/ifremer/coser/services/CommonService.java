/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.data.AbstractDataEntity;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Haul;
import fr.ifremer.coser.data.Length;
import fr.ifremer.coser.data.Strata;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.storage.MemoryDataStorage;
import fr.ifremer.coser.util.ProgressMonitor;
import fr.ifremer.coser.util.ProgressReader;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.ArrayUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Common methods used by others services only.
 *
 * <ul>
 * <li>csv import/export</li>
 * <li>file name management</li>
 * <li>utilities methods</li>
 * </ul>
 *
 * @author chatellier
 */
public class CommonService {

    protected CoserBusinessConfig config;

    CommonService(CoserBusinessConfig config) {
        this.config = config;
    }

    /**
     * Load file as csv. Check and affect data in project depending on category type.
     *
     * @param project  project
     * @param category category
     * @param file     file to load
     * @return data storage with file content
     * @throws CoserBusinessException
     */
    public DataStorage loadCSVFile(Project project, Category category, File file) throws CoserBusinessException {
        return loadCSVFile(project, category, file, null);
    }

    /**
     * Load file as csv. Check and affect data in project depending on category type.
     *
     * @param project  project
     * @param category category
     * @param file     file to load
     * @param progress progress monitor (can be null)
     * @return data storage with file content
     * @throws CoserBusinessException
     */
    public DataStorage loadCSVFile(Project project, Category category, File file, ProgressMonitor progress) throws CoserBusinessException {
        return loadCSVFile(project, category, file, null, false);
    }

    /**
     * Load file as csv. Check and affect data in project depending on category type.
     *
     * @param project         project
     * @param category        category
     * @param file            file to load
     * @param progress        progress monitor (can be null)
     * @param originalLoading dans le cas d'un reload, la colonne "line" est a prendre en compte
     *                        pour les data (elle est absente dans les jeux de données originaux)
     * @return data storage with file content
     * @throws CoserBusinessException
     */
    public DataStorage loadCSVFile(Project project, Category category, File file, ProgressMonitor progress, boolean originalLoading) throws CoserBusinessException {

        DataStorage content = new MemoryDataStorage();

        CSVReader csvReader = null;
        try {

            InputStream stream = new FileInputStream(file);
            Reader reader = new BufferedReader(new InputStreamReader(stream, CoserConstants.CSV_FILE_ENCODING));
            if (progress != null) {
                reader = new ProgressReader(reader, progress);
            }
            csvReader = new CSVReader(reader, CoserConstants.CSV_SEPARATOR_CHAR);

            // check header
            String[] line = csvReader.readNext();
            if (line == null || line.length <= 1) {
                throw new CoserBusinessException(t("Can't read file '%s'. Check CSV file separator",
                                                   file.getAbsolutePath()));
            } else {
                if (originalLoading) {
                    checkFileHeader(file, category, line);
                    if (category.isDataCategory()) {
                        line = (String[]) ArrayUtil.concat(new String[]{AbstractDataEntity.PROPERTY_LINE}, line);
                    }
                }
                content.add(line);
            }

            // pour alimenter la line index (si non reloading)
            // dans coser, la numérotation des lignes commence à 1
            int lineIndex = 1;
            while ((line = csvReader.readNext()) != null) {
                if (line.length > 1) {
                    if (originalLoading && category.isDataCategory()) {
                        // ajout de la colonne "line" en debut (seulement pour les data)
                        line = (String[]) ArrayUtil.concat(new String[]{String.valueOf(lineIndex)}, line);
                        lineIndex++;
                    }
                    content.add(line);
                }
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read file", ex);
        } finally {
            IOUtils.closeQuietly(csvReader);
        }

        return content;
    }

    /**
     * Check csv file header names and order depending on file category.
     *
     * @param file     file
     * @param category category
     * @param line     header line to ckeck
     * @throws CoserBusinessException
     */
    protected void checkFileHeader(File file, Category category, String[] line) throws CoserBusinessException {

        String[] enHeaders = null;
        String[] frHeaders = null;

        switch (category) {
            case CATCH:
                enHeaders = Catch.EN_HEADERS;
                frHeaders = Catch.FR_HEADERS;
                break;
            case HAUL:
                enHeaders = Haul.EN_HEADERS;
                frHeaders = Haul.FR_HEADERS;
                break;
            case LENGTH:
                enHeaders = Length.EN_HEADERS;
                frHeaders = Length.FR_HEADERS;
                break;
            case STRATA:
                enHeaders = Strata.EN_HEADERS;
                frHeaders = Strata.FR_HEADERS;
                break;
            case REFTAX_SPECIES:
                enHeaders = Project.REFTAX_SPECIES_HEADER;
                break;
            case TYPE_ESPECES:
                enHeaders = Project.TYPE_ESPECE_HEADER;
                break;
        }

        if (frHeaders != null) {
            if (!Arrays.equals(line, enHeaders) && !Arrays.equals(line, frHeaders)) {
                throw new CoserBusinessException(t("Wrong header detected in file %s. Found : %s, expected %s or %s",
                                                   file.getName(),
                                                   StringUtils.join(line, ", "),
                                                   StringUtils.join(frHeaders, ", "),
                                                   StringUtils.join(enHeaders, ", ")));
            }
        } else {
            if (!Arrays.equals(line, enHeaders)) {
                throw new CoserBusinessException(t("Wrong header detected in file %s. Found : %s, expected %s",
                                                   file.getName(),
                                                   StringUtils.join(line, ", "),
                                                   StringUtils.join(enHeaders, ", ")));
            }
        }
    }

    /**
     * Get empty storage for category (filled with just header)
     *
     * @param project  project
     * @param category category
     * @return initialized storage
     */
    public DataStorage getEmptyStorage(Project project, Category category) {

        DataStorage dataStorage = new MemoryDataStorage();
        switch (category) {
            case CATCH:
                dataStorage.add(Catch.EN_HEADERS);
                break;
            case HAUL:
                dataStorage.add(Haul.EN_HEADERS);
                break;
            case LENGTH:
                dataStorage.add(Length.EN_HEADERS);
                break;
            case STRATA:
                dataStorage.add(Strata.EN_HEADERS);
                break;
        }

        return dataStorage;
    }

    /**
     * Store project category data in specified file as csv.
     *
     * @param content content to save
     * @param file    file to save to
     * @throws CoserBusinessException
     */
    public void storeData(DataStorage content, File file) throws CoserBusinessException {

        // save content
        CSVWriter csvWriter = null;
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), CoserConstants.CSV_FILE_ENCODING));

            csvWriter = new CSVWriter(writer, CoserConstants.CSV_SEPARATOR_CHAR);
            Iterator<String[]> itContent = content.iterator();
            while (itContent.hasNext()) {
                csvWriter.writeNext(itContent.next());
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save data", ex);
        } finally {
            IOUtils.closeQuietly(csvWriter);
        }
    }

    /**
     * Store data without quoting every columns with "" (r specific demand).
     *
     * @param content  content to save
     * @param file     file to save to
     * @param project  project used only to get species output field (can be null)
     * @param category category used to replace species output field if needed (can be null)
     * @throws CoserBusinessException
     * @deprecated since 1.4, prefer use of {@link #storeDataWhithoutQuote(DataStorage, Writer, Map, Category)}
     */
    public void storeDataWhithoutQuote(DataStorage content, File file, Map<String, String> refTaxSpecies, Category category) throws CoserBusinessException {

        // save content
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), CoserConstants.CSV_FILE_ENCODING));
            storeDataWhithoutQuote(content, writer, refTaxSpecies, category);
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save data", ex);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    /**
     * Store data without quoting every columns with "" (r specific demand).
     *
     * @param content  content to save
     * @param file     file to save to
     * @param project  project used only to get species output field (can be null)
     * @param category category used to replace species output field if needed (can be null)
     * @throws CoserBusinessException
     * @since 1.4
     */
    public void storeDataWhithoutQuote(DataStorage content, Writer writer, Map<String, String> refTaxSpecies, Category category) throws CoserBusinessException {

        // save content
        try {
            Iterator<String[]> itContent = content.iterator();
            while (itContent.hasNext()) {
                String[] contentDatas = itContent.next();

                // start at 1 to not output "line" column
                for (int i = 1; i < contentDatas.length; i++) {

                    if (i != 1) {
                        writer.write(CoserConstants.CSV_SEPARATOR_CHAR);
                    }

                    String contentData = contentDatas[i];

                    // for some category, need to swap species output field with
                    // user preference (since 1.3)
                    if ((category == Category.CATCH && i == Catch.INDEX_SPECIES) ||
                        (category == Category.LENGTH && i == Length.INDEX_SPECIES)) {
                        // can not exists, stay unchanged
                        if (refTaxSpecies.containsKey(contentData)) {
                            contentData = refTaxSpecies.get(contentData);
                        }
                    }

                    if (contentData.indexOf(CoserConstants.CSV_SEPARATOR_CHAR) > -1) {
                        writer.write("\"" + contentData + "\"");
                    } else {
                        writer.write(contentData);
                    }
                }
                writer.write("\n");
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't save data", ex);
        }
    }

    /**
     * Lit le fichier demandé et sauve dans une map les lignes demandées.
     *
     * @param file  file to read
     * @param lines lines to save content
     * @return saved content
     * @throws CoserBusinessException
     */
    public Map<String, String[]> getOriginalFileContent(File file, Collection<String> lines) throws CoserBusinessException {
        Map<String, String[]> content = new HashMap<String, String[]>();

        CSVReader csvReader = null;
        try {

            Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), CoserConstants.CSV_FILE_ENCODING));
            csvReader = new CSVReader(reader, CoserConstants.CSV_SEPARATOR_CHAR);

            // skip header
            String[] line = csvReader.readNext();

            // pour alimenter la line index (si non reloading)
            // dans coser, la numérotation des lignes commence à 1
            int lineIndex = 1;
            while ((line = csvReader.readNext()) != null) {
                String stringLine = String.valueOf(lineIndex);
                if (lines.contains(stringLine)) {
                    content.put(stringLine, line);
                }
                lineIndex++;
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read file", ex);
        } finally {
            IOUtils.closeQuietly(csvReader);
        }

        return content;
    }

    /**
     * Charge un fichier csv (estcomind/estpopind ...) mais sans verification.
     * C'est a peu pret le meme code qu'au dessus, sauf que le storage est
     * simplement retourné et que les séparateurs ne sont pas les même.
     *
     * @param file file to load
     * @return data storage with file content
     * @throws CoserBusinessException
     */
    public DataStorage loadCSVFile(File file) throws CoserBusinessException {
        return loadCSVFile(file, CoserConstants.CSV_SEPARATOR_CHAR);
    }

    /**
     * Charge un fichier csv (estcomind/estpopind ...) mais sans verification.
     * C'est a peu pret le meme code qu'au dessus, sauf que le storage est
     * simplement retourné et que les séparateurs ne sont pas les même.
     *
     * @param file      file to load
     * @param separator separator to use to load file
     * @return data storage with file content
     * @throws CoserBusinessException
     */
    public DataStorage loadCSVFile(File file, char separator) throws CoserBusinessException {

        DataStorage content = new MemoryDataStorage();

        CSVReader csvReader = null;
        try {

            InputStream stream = new FileInputStream(file);
            Reader reader = new BufferedReader(new InputStreamReader(stream, CoserConstants.CSV_FILE_ENCODING));
            csvReader = new CSVReader(reader, separator);

            // check header
            String[] line = csvReader.readNext();
            if (line == null || line.length <= 1) {
                throw new CoserBusinessException(t("Can't read file '%s'. Check CSV file separator",
                                                   file.getAbsolutePath()));
            } else {
                content.add(line);
            }

            while ((line = csvReader.readNext()) != null) {
                if (line.length > 1) {
                    content.add(line);
                }
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't read file", ex);
        } finally {
            IOUtils.closeQuietly(csvReader);
        }

        return content;
    }

    /**
     * Retourne le nom de stockage d'un fichier de données suivant la categories
     * de fichier demandé. Les fichiers sont stockés avec leurs noms originaux
     * (sauf les fichiers de réference) donc il n'est pas fixe.
     *
     * @param project  project (containing originals file names)
     * @param category category to get file name
     * @param suffix   suffix to add into file name
     * @return storage file name
     * @deprecated since 1.5, use now {@link Project#getDataStorageFileName(Category, String)}
     */
    @Deprecated
    protected String getDataStorageFileName(Project project, Category category, String suffix) {

        String result = project.getDataStorageFileName(category, suffix);
        return result;
    }

    /**
     * Lit tout le fichier associé à la categorie demandé, mais ne retourne
     * que les données correspondant au numéro de lignes demandée.
     *
     * Le contenu retourné ne contient pas le numéro de ligne.
     *
     * @param project  project
     * @param category category
     * @param lines    lines to get content
     * @return content for lines
     * @throws CoserBusinessException
     */
    public Map<String, String[]> getOriginalContent(Project project, Category category, Collection<String> lines) throws CoserBusinessException {

        // ne lit pas un fichier entier pour ne rien chercher
        if (CollectionUtils.isEmpty(lines)) {
            return new HashMap<String, String[]>();
        }

        File projectsDirectory = config.getRSufiProjectsDirectory();
        File projectDirectory = new File(projectsDirectory, project.getName());
        File originalDirectory = new File(projectDirectory, CoserConstants.STORAGE_ORIGINAL_DIRECTORY);
        String storageFileName = getDataStorageFileName(project, category, null);
        File dataFile = new File(originalDirectory, storageFileName);

        Map<String, String[]> contents = getOriginalFileContent(dataFile, lines);

        return contents;
    }

    /**
     * Retourne le nom d'affichage d'une especes pour les rapports.
     *
     * @param project project
     * @param species species code
     * @return species display name
     */
    public String getReportDisplayName(Project project, String species) {
        String displayName = null;

        // load reftax in memory
        Iterator<String[]> reftax = project.getRefTaxSpecies().iterator(true);
        while (reftax.hasNext()) {
            String[] tuple = reftax.next();

            // "C_Perm","NumSys","NivSys","C_VALIDE","L_VALIDE","AA_VALIDE","C_TxP\u00E8re","Taxa"
            String speciesCode = tuple[3];
            if (speciesCode.equals(species)) {
                // nom + auteur (sans ajout de parenthese : important)
                displayName = tuple[4] + " " + tuple[5];
                break;
            }
        }

        return displayName;
    }
}

package fr.ifremer.coser.services;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.coser.CoserApplicationContext;
import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.CoserMainRepositoryProvider;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.CoserRequestBuilder;
import fr.ifremer.coser.result.CoserRequestContext;
import fr.ifremer.coser.result.CoserRequestExecutor;
import fr.ifremer.coser.result.DefaultCoserRequestContext;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.repository.ResultRepositoryProvider;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.repository.echobase.EchoBaseResultRepositoryProvider;
import fr.ifremer.coser.result.repository.echobase.EchoBaseResultRepositoryType;
import fr.ifremer.coser.result.repository.legacy.LegacyResultRepositoryProvider;
import fr.ifremer.coser.result.repository.legacy.LegacyResultRepositoryType;
import fr.ifremer.coser.result.request.CopyRepositoryRequest;
import fr.ifremer.coser.result.request.DeleteResultsRequest;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetResultNameRequest;
import fr.ifremer.coser.result.result.ExtractRawDataAndResultsResult;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.result.util.Extracts;
import fr.ifremer.coser.util.DataType;
import fr.ifremer.coser.util.io.MultipleFileFilter;
import fr.ifremer.coser.util.io.OneRSufiResultFileFilter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Service to be used only by the web server to manage results.
 *
 * Created on 3/18/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class WebResultService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(WebResultService.class);

    protected final CoserApplicationContext applicationContext;

    public WebResultService(CoserApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    // --------------------------------------------------------------------- //
    // --- Public API ------------------------------------------------------ //
    // --------------------------------------------------------------------- //

    public CoserRequestContext newRequestContext(Locale locale) {
        return new DefaultCoserRequestContext(applicationContext, locale);
    }

    public CoserRequestExecutor executeUnique(CoserRequestContext context, CoserRequest request) {
        CoserRequestExecutor executor = executeUnique(getInternalRepositoryProvider(), context, request);
        return executor;
    }

    public CoserRequestExecutor executeFirst(CoserRequestContext context, CoserRequest request) {
        CoserRequestExecutor executor = executeFirst(getInternalRepositoryProvider(), context, request);
        return executor;
    }

    public CoserRequestExecutor executeAll(CoserRequestContext context, CoserRequest request) {
        CoserRequestExecutor executor = executeAll(getInternalRepositoryProvider(), context, request);
        return executor;
    }

    /**
     * Extract some raw data and results from repositories and assembly them as an archive.
     *
     * @param context request context
     * @param request extract request
     * @return the file result containing the archive
     */
    public FileResult extractRawDataAndResults(CoserRequestContext context,
                                               ExtractRawDataAndResultsRequest request) {

        File extractDirectory = new File(context.getTemporaryDirectory(), "Indicateurs_Ifremer");
        request.setExtractDirectory(extractDirectory);

        try {
            FileUtils.forceMkdir(extractDirectory);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not create directory: " + extractDirectory, e);
        }

        // Get all extracted stuff from matching repositories
        List<ExtractRawDataAndResultsResult> multipleResults =
                executeAll(getInternalRepositoryProvider(), context, request).
                        toMultipleResult(ExtractRawDataAndResultsResult.class);

        File file = getExtracts().assemblyExtractResult(context,
                                                        extractDirectory,
                                                        request.getZoneList(),
                                                        multipleResults);
        FileResult result = new FileResult("ALL", file);
        return result;
    }

    public void deleteResults(CoserRequestContext context,
                              DeleteResultsRequest request) {

        // do delete results (don't care about result)
        executeAll(getInternalRepositoryProvider(), context, request);

        // reload projects
        getInternalRepositoryProvider().resetRepositories();
    }

    public void resetRepositories() {
        applicationContext.getRepositoryProvider().resetRepositories();
    }

    public Map<String, ResultRepositoryType> getRepositoryTypes() {
        Map<String, ResultRepositoryType> result = Maps.newHashMap();
        for (ResultRepositoryType resultRepositoryType : applicationContext.getRepositoryTypes()) {
            result.put(resultRepositoryType.getId(), resultRepositoryType);
        }
        return result;
    }

    /**
     * Traite le fichier uploade par l'application client et l'enregistre
     * dans le stockage coté web.
     *
     * Le nouveau fichier uploadé est mergé avec l'ancien, c'est à dire:
     * <ul>
     * <li>dezipage dans un fichier temporaire
     * <li>recuperation des noms de zones des nouveau fichiers (par type, carte/indicateur)
     * <li>suppression dans l'ancien répertoire des resultats deja presents dans le nouveau (pour les conflits zone)
     * <li>suppression des selections vides
     * <li>suppression des projets vides
     * <li>copie (avec ecrasement) des nouveaux fichiers dans l'ancien répertoire
     * mais seulement pour ceux des zones concernés (partie difficile)
     * </ul>
     *
     * TODO chatellier 20110125 l'algorithme n'est pour l'instant pas performant
     * et contient pas mal de code dupliqué, mais pour la v1.0 ca ira.
     *
     * @param login       user login
     * @param archiveFile uploaded file
     */
    public void registerNewUploadedResults(CoserRequestContext context,
                                           String login,
                                           File archiveFile) {
        File tempDirectory;

        // dezipage dans un fichier temporaire
        try {
            tempDirectory = FileUtil.createTempDirectory("coser-upload-", "-tmp");
            ZipUtil.uncompress(archiveFile, tempDirectory);
        } catch (IOException e) {
            throw new CoserTechnicalException("Can't uncompress archive", e);
        }

        NewUploadContext uploadContext = new NewUploadContext(context);

        // recuperer les resultats actuels pour le mail de mise à jour
        GetResultNameRequest indicatorsRequest =
                requestBuilder(context, GetResultNameRequest.class).
                        addExtractTypeList(Lists.newArrayList(DataType.COMMUNITY, DataType.POPULATION)).toRequest();

        GetResultNameRequest mapsRequest =
                requestBuilder(context, GetResultNameRequest.class).
                        addExtractTypeList(Lists.newArrayList(DataType.MAP)).
                        toRequest();

        GetResultNameRequest rawDataRequest =
                requestBuilder(context, GetResultNameRequest.class).
                        addExtractTypeList(Lists.newArrayList(DataType.SOURCE)).
                        toRequest();

        // recuperer les resultats actuels pour le mail de mise à jour
        Map<String, String> indicatorResults = executeAll(getInternalRepositoryProvider(), context, indicatorsRequest).toMap();
        Map<String, String> mapsResults = executeAll(getInternalRepositoryProvider(), context, mapsRequest).toMap();
        Map<String, String> dataResults = executeAll(getInternalRepositoryProvider(), context, rawDataRequest).toMap();

        uploadContext.indicatorResults.putAll(indicatorResults);
        uploadContext.mapsResults.putAll(mapsResults);
        uploadContext.dataResults.putAll(dataResults);

        // merge rsufi results
        File rsufiDirectory = new File(tempDirectory, LegacyResultRepositoryType.ID);
        try {
            registerNewUploadedResultsForRSufi(uploadContext,
                                               rsufiDirectory,
                                               indicatorsRequest,
                                               mapsRequest,
                                               rawDataRequest);
        } catch (CoserBusinessException e) {
            throw new CoserTechnicalException("Could not merge rsufi results", e);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not merge rsufi results", e);
        }

        // merge echobase results
        File echobaseDirectory = new File(tempDirectory, EchoBaseResultRepositoryType.ID);
        registerNewUploadedResultsForEchoBase(uploadContext,
                                              echobaseDirectory,
                                              indicatorsRequest,
                                              mapsRequest,
                                              rawDataRequest);

        // update data date
        getConfig().updateDataProperties();

        // reload projects
        applicationContext.getRepositoryProvider().resetRepositories();

        // send email notification
        generatedAndSendNewResultNotifications(login, uploadContext);

        // delete temp directory
        try {
            FileUtils.deleteDirectory(tempDirectory);
        } catch (IOException e) {
            throw new CoserTechnicalException("Can't delete directory", e);
        }
    }

    // --------------------------------------------------------------------- //
    // --- Internal methods ------------------------------------------------ //
    // --------------------------------------------------------------------- //

    protected void registerNewUploadedResultsForEchoBase(NewUploadContext uploadContext,
                                                         File basedir,
                                                         GetResultNameRequest indicatorsRequest,
                                                         GetResultNameRequest mapsRequest,
                                                         GetResultNameRequest rawDataRequest) {

        // Creation d'un repository provider
        EchoBaseResultRepositoryProvider provider = new EchoBaseResultRepositoryProvider(basedir);
        CoserMainRepositoryProvider mainRepositoryProvider =
                new CoserMainRepositoryProvider(Sets.<ResultRepositoryProvider<?>>newHashSet(provider));

        if (CollectionUtils.isEmpty(mainRepositoryProvider.getResultRepositories())) {
            // no repository found, nothing to do
            return;
        }
        CoserRequestContext context = uploadContext.context;

        // recuperation des noms zone des nouveau fichiers
        Map<String, String> indicatorsResultZoneIds = executeAll(mainRepositoryProvider, context, indicatorsRequest).toMap();
        Map<String, String> mapsResultZoneIds = executeAll(mainRepositoryProvider, context, mapsRequest).toMap();
        Map<String, String> dataResultZoneIds = executeAll(mainRepositoryProvider, context, rawDataRequest).toMap();

        // toutes les zones qui existent
        Set<String> existingZones = Sets.newHashSet();
        existingZones.addAll(uploadContext.indicatorResults.keySet());
        existingZones.addAll(uploadContext.mapsResults.keySet());
        existingZones.addAll(uploadContext.dataResults.keySet());

        // toutes les zones a ajouter
        Set<String> zonesToImport = Sets.newHashSet();
        zonesToImport.addAll(indicatorsResultZoneIds.keySet());
        zonesToImport.addAll(mapsResultZoneIds.keySet());
        zonesToImport.addAll(dataResultZoneIds.keySet());

        // toutes les zones a supprimer (avant ajout)
        List<String> zonesToDelete = Lists.newArrayList(zonesToImport);
        zonesToDelete.retainAll(existingZones);

        if (CollectionUtils.isNotEmpty(zonesToDelete)) {
            // suppression des results deja existants
            DeleteResultsRequest deleteRequest = requestBuilder(context, DeleteResultsRequest.class).
                    addZoneList(zonesToDelete).
                    addResultType(ResultType.MAP_AND_INDICATOR).
                    addRepositoryType(EchoBaseResultRepositoryType.ID).
                    toRequest();
            executeAll(mainRepositoryProvider, context, deleteRequest);
        }

        if (CollectionUtils.isNotEmpty(zonesToImport)) {
            // ajout des nouvelles zones
            CopyRepositoryRequest addRequest = requestBuilder(context, CopyRepositoryRequest.class).
                    addZoneList(Lists.newArrayList(zonesToImport)).
                    addTargetDirectory(getConfig().getWebEchobaseProjectsDirectory()).
                    toRequest();
            executeAll(mainRepositoryProvider, context, addRequest);
        }

        // mise a jour du context
        uploadContext.indicatorsResultZoneIds.putAll(indicatorsResultZoneIds);
        uploadContext.mapsResultZoneIds.putAll(mapsResultZoneIds);
        uploadContext.dataResultZoneIds.putAll(dataResultZoneIds);
    }

    protected void registerNewUploadedResultsForRSufi(NewUploadContext uploadContext,
                                                      File tempDirectory,
                                                      GetResultNameRequest indicatorsRequest,
                                                      GetResultNameRequest mapsRequest,
                                                      GetResultNameRequest rawDataRequest) throws CoserBusinessException, IOException {

        // Creation d'un repository provider
        LegacyResultRepositoryProvider provider = new LegacyResultRepositoryProvider(getConfig(), tempDirectory, null);
        CoserMainRepositoryProvider mainRepositoryProvider = new CoserMainRepositoryProvider(Sets.<ResultRepositoryProvider<?>>newHashSet(provider));

        if (CollectionUtils.isEmpty(mainRepositoryProvider.getResultRepositories())) {
            // no repository found, nothing to do
            return;
        }
        ProjectService projectService = new ProjectService(getConfig());

        File projectsDirectory = getConfig().getWebIndicatorsProjectsDirectory();
        File mapsDirectory = getConfig().getWebMapsProjectsDirectory();

        CoserRequestContext context = uploadContext.context;

        // suppression des resultats qui ont été envoyé mais
        // ne sont ni maps result, ni indicator result
        Map<String, String> noIndicatorsResultZoneIds = getZonesIds(projectService, tempDirectory, false, null, null);
        cleanCurrentProjectDirectory(projectService, projectsDirectory, noIndicatorsResultZoneIds.keySet());
        Map<String, String> noMapsResultZoneIds = getZonesIds(projectService, tempDirectory, null, false, null);
        cleanCurrentProjectDirectory(projectService, mapsDirectory, noMapsResultZoneIds.keySet());
        Map<String, String> noDataResultZoneIds = getZonesIds(projectService, tempDirectory, null, null, false);

        // recuperation des noms zone des nouveau fichiers
        Map<String, String> indicatorsResultZoneIds = executeAll(mainRepositoryProvider, context, indicatorsRequest).toMap();
        Map<String, String> mapsResultZoneIds = executeAll(mainRepositoryProvider, context, mapsRequest).toMap();
        Map<String, String> dataResultZoneIds = executeAll(mainRepositoryProvider, context, rawDataRequest).toMap();

        // suppression des resultats a reimporter
        cleanCurrentProjectDirectory(projectService, projectsDirectory, indicatorsResultZoneIds.keySet());
        cleanCurrentProjectDirectory(projectService, mapsDirectory, mapsResultZoneIds.keySet());

        // import des nouveaux resultats

        FileFilter indicatorsFileFilter = getCopyFileFilter(projectService, tempDirectory, false);
        CoserUtils.customCopyDirectory(tempDirectory, projectsDirectory, indicatorsFileFilter);

        // creation du filter qui copiera juste ce qu'il faut
        FileFilter mapsFileFilter = getCopyFileFilter(projectService, tempDirectory, true);
        CoserUtils.customCopyDirectory(tempDirectory, mapsDirectory, mapsFileFilter);

        // mise a jour du context
        uploadContext.noIndicatorsResultZoneIds.putAll(noIndicatorsResultZoneIds);
        uploadContext.noMapsResultZoneIds.putAll(noMapsResultZoneIds);
        uploadContext.noDataResultZoneIds.putAll(noDataResultZoneIds);
        uploadContext.indicatorsResultZoneIds.putAll(indicatorsResultZoneIds);
        uploadContext.mapsResultZoneIds.putAll(mapsResultZoneIds);
        uploadContext.dataResultZoneIds.putAll(dataResultZoneIds);
    }

    protected void generatedAndSendNewResultNotifications(String login,
                                                          NewUploadContext uploadContext) {

        // generate email content
        StringBuilder content = new StringBuilder();

        ZoneMap zoneMap = getZoneMap();
        int count = 0;
        String endOfLine = "\n";
        content.append(t("coser.business.notificationmail.mapsresults")).append(endOfLine);
        for (Map.Entry<String, String> noMapsResultZoneId : uploadContext.noMapsResultZoneIds.entrySet()) {
            if (uploadContext.mapsResults.containsValue(noMapsResultZoneId.getValue())) {
                content.append(" - ").append(t("coser.business.notificationmail.deleted",
                                               zoneMap.getZoneFullName(noMapsResultZoneId.getKey()),
                                               noMapsResultZoneId.getValue())).append(endOfLine);
                count++;
            }
        }
        for (Map.Entry<String, String> mapsResultZoneId : uploadContext.mapsResultZoneIds.entrySet()) {
            if (!uploadContext.mapsResults.containsValue(mapsResultZoneId.getValue())) {
                content.append(" - ").append(t("coser.business.notificationmail.added",
                                               zoneMap.getZoneFullName(mapsResultZoneId.getKey()),
                                               mapsResultZoneId.getValue())).append(endOfLine);
                count++;
            }
        }
        content.append(endOfLine);

        content.append(t("coser.business.notificationmail.indicatorsresults")).append(endOfLine);
        for (Map.Entry<String, String> noIndicatorsResultZoneId : uploadContext.noIndicatorsResultZoneIds.entrySet()) {
            if (uploadContext.indicatorResults.containsValue(noIndicatorsResultZoneId.getValue())) {
                content.append(" - ").append(t("coser.business.notificationmail.deleted",
                                               zoneMap.getZoneFullName(noIndicatorsResultZoneId.getKey()),
                                               noIndicatorsResultZoneId.getValue())).append(endOfLine);
                count++;
            }

        }
        for (Map.Entry<String, String> indicatorsResultZoneId : uploadContext.indicatorsResultZoneIds.entrySet()) {
            if (!uploadContext.indicatorResults.containsValue(indicatorsResultZoneId.getValue())) {
                content.append(" - ").append(t("coser.business.notificationmail.added",
                                               zoneMap.getZoneFullName(indicatorsResultZoneId.getKey()),
                                               indicatorsResultZoneId.getValue())).append(endOfLine);
                count++;
            }
        }
        content.append(endOfLine);

        content.append(t("coser.business.notificationmail.dataresults")).append(endOfLine);
        for (Map.Entry<String, String> noDataResultZoneId : uploadContext.noDataResultZoneIds.entrySet()) {
            if (uploadContext.dataResults.containsValue(noDataResultZoneId.getValue())) {
                content.append(" - ").append(t("coser.business.notificationmail.deleted",
                                               zoneMap.getZoneFullName(noDataResultZoneId.getKey()),
                                               noDataResultZoneId.getValue())).append(endOfLine);
                count++;
            }
        }

        for (Map.Entry<String, String> dataResultZoneId : uploadContext.dataResultZoneIds.entrySet()) {
            if (!uploadContext.dataResults.containsValue(dataResultZoneId.getValue())) {
                content.append(" - ").append(t("coser.business.notificationmail.added",
                                               zoneMap.getZoneFullName(dataResultZoneId.getKey()),
                                               dataResultZoneId.getValue())).append(endOfLine);
                count++;
            }
        }
        content.append(endOfLine);

        // send notification mails
        sendNewResultNotifications(login, count, content.toString());
    }

    protected CoserRequestExecutor executeUnique(CoserMainRepositoryProvider repositoryProvider,
                                                 CoserRequestContext context,
                                                 CoserRequest request) {
        CoserRequestExecutor executor = new CoserRequestExecutor(applicationContext, repositoryProvider);
        executor.executeUnique(context, request);
        return executor;
    }

    protected CoserRequestExecutor executeFirst(CoserMainRepositoryProvider repositoryProvider,
                                                CoserRequestContext context,
                                                CoserRequest request) {
        CoserRequestExecutor executor = new CoserRequestExecutor(applicationContext, repositoryProvider);
        executor.executeFirst(context, request);
        List<ResultRepository> matchingRepositories = executor.getMatchingRepositories();
        if (matchingRepositories.size() > 1) {
            String message = "Found more than one matching repository: ";
            for (ResultRepository matchingRepository : matchingRepositories) {
                message += "\n\t" + matchingRepository.getProjectName();
            }
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
        }
        return executor;
    }


    protected CoserRequestExecutor executeAll(CoserMainRepositoryProvider repositoryProvider,
                                              CoserRequestContext context,
                                              CoserRequest request) {
        CoserRequestExecutor executor = new CoserRequestExecutor(applicationContext, repositoryProvider);
        executor.executeAll(context, request);
        return executor;
    }

    protected <R extends CoserRequest> CoserRequestBuilder<R> requestBuilder(CoserRequestContext context,
                                                                             Class<R> requestType) {
        return CoserRequestBuilder.newBuilder(context.getLocale(), requestType);
    }

    protected CoserBusinessConfig getConfig() {
        return applicationContext.getConfig();
    }

    protected ZoneMap getZoneMap() {
        return applicationContext.getZoneMap();
    }

    protected Extracts getExtracts() {
        return applicationContext.getExtracts();
    }

    protected CoserMainRepositoryProvider getInternalRepositoryProvider() {
        return applicationContext.getRepositoryProvider();
    }

    class NewUploadContext {

        final CoserRequestContext context;

        // existing zones for indicator results
        Map<String, String> indicatorResults = Maps.newHashMap();

        // existing zones for map results
        Map<String, String> mapsResults = Maps.newHashMap();

        // existing zones for raw data results
        Map<String, String> dataResults = Maps.newHashMap();

        // incoming zones for not indicator results
        Map<String, String> noIndicatorsResultZoneIds = Maps.newHashMap();

        // incoming zones for not map results
        Map<String, String> noMapsResultZoneIds = Maps.newHashMap();

        // incoming zones for not raw data results
        Map<String, String> noDataResultZoneIds = Maps.newHashMap();

        // incoming zones for indicator results
        Map<String, String> indicatorsResultZoneIds = Maps.newHashMap();

        // incoming zones for map results
        Map<String, String> mapsResultZoneIds = Maps.newHashMap();

        // incoming zones for raw data results
        Map<String, String> dataResultZoneIds = Maps.newHashMap();

        NewUploadContext(CoserRequestContext context) {
            this.context = context;
        }
    }

    // --------------------------------------------------------------------- //
    // --- Duplicate code from WebService ---------------------------------- //
    // --------------------------------------------------------------------- //

    /**
     * Envoi un mail de notification apres la publication des resultat à la
     * liste des adresses email renseignées dans la configuration.
     *
     * @param login  user login
     * @param count  updated data count
     * @param detail body mail detail
     */
    protected void sendNewResultNotifications(String login, int count, String detail) {
        List<String> emails = getConfig().getNewResultNotificationList();

        for (String email : emails) {
            try {
                MultiPartEmail emailPart = new MultiPartEmail();
                emailPart.setHostName(getConfig().getSmtpHost());
                emailPart.addTo(email);
                emailPart.setFrom("noreply-coser@ifremer.fr", "Coser");
                emailPart.setSubject(t("coser.business.notificationmail.subject", count));
                emailPart.setContent(t("coser.business.notificationmail.body", login, detail), "text/plain; charset=ISO-8859-9");

                // send mail
                emailPart.send();
            } catch (EmailException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't send mail", ex);
                }
            }
        }
    }

    /**
     * Fait le menage dans le dossier courant des projets en supprimant
     * tout les resulat qui ont un result id present dans la liste
     * {@code newResultIds}.
     *
     * Supprime egalement les selections qui n'ont plus de résultats et
     * les projets qui n'ont plus de selection.
     *
     * @param projectService    service to load project
     * @param projectsDirectory projectsDirectory
     * @param newResultIds      new ids
     * @throws CoserBusinessException
     */
    protected void cleanCurrentProjectDirectory(ProjectService projectService,
                                                File projectsDirectory,
                                                Collection<String> newResultIds) throws CoserBusinessException {

        try {
            File[] projectFiles = projectsDirectory.listFiles();
            if (projectFiles != null) {
                for (File projectFile : projectFiles) {
                    if (projectFile.isDirectory()) {
                        int projectSelectionCount = 0;

                        File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                        File[] selectionFiles = selectionsDirectory.listFiles();
                        if (selectionFiles != null) {
                            for (File selectionFile : selectionFiles) {
                                if (selectionFile.isDirectory()) {
                                    int selectionResultCount = 0;

                                    File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                    File[] resultFiles = resultsDirectory.listFiles();
                                    if (resultFiles != null) {
                                        for (File resultFile : resultFiles) {
                                            if (resultFile.isDirectory()) {
                                                RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);
                                                String resultResultId = rsufiResult.getZone();
                                                if (newResultIds.contains(resultResultId)) {
                                                    // un nouveau resulat utilsera ce resultid
                                                    FileUtils.deleteDirectory(resultFile);
                                                } else {
                                                    // un resultat valid trouvé, selection non a supprimer
                                                    selectionResultCount++;
                                                }
                                            }
                                        }
                                    }

                                    // si aucun resultat valide, suppression de la seletion
                                    if (selectionResultCount == 0) {
                                        FileUtils.deleteDirectory(selectionFile);
                                    } else {
                                        projectSelectionCount++;
                                    }
                                }
                            }
                        }

                        // si aucune selection avec resultat, suppression du projet
                        if (projectSelectionCount == 0) {
                            FileUtils.deleteDirectory(projectFile);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            throw new CoserBusinessException("Can't delete directory", ex);
        }
    }

    /**
     * Retourne un file filter qui ne copiera que les dossiers resultat
     * correspondant au type demandé. Pour un resultat, le filtre devra
     * egalement copier les dossiers projet et selection correspondants.
     *
     * @param projectService service to load project
     * @param scanDirectory  directory containing result to copy
     * @param mapResults     result type to get
     * @return aggragated file filter
     * @throws CoserBusinessException
     */
    protected FileFilter getCopyFileFilter(ProjectService projectService,
                                           File scanDirectory,
                                           boolean mapResults) throws CoserBusinessException {

        MultipleFileFilter aggregateFileFilter = new MultipleFileFilter();

        File[] projectFiles = scanDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {

                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {

                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // return result depending on result type
                                            if ((mapResults && rsufiResult.isMapsResult()) ||
                                                (!mapResults && rsufiResult.isIndicatorsResult())) {

                                                Project p = new Project(projectFile.getName());
                                                Selection s = new Selection(selectionFile.getName());

                                                OneRSufiResultFileFilter resultFileFilter = new OneRSufiResultFileFilter(scanDirectory, p, s, rsufiResult, true);
                                                aggregateFileFilter.add(resultFileFilter);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return aggregateFileFilter;
    }

    /**
     * Recupere dans un repertoire donné, les zoneid des resultat avec
     * pour chaque id, le nom du projet associé.
     *
     * Les boolean sont des grands {@code Boolean} car si la valeur
     * est {@code null}, on ne tient pas compte du critere lors de la recherche.
     *
     * @param projectService   service to load project
     * @param scanDirectory    le repertoire a scanner
     * @param indicatorResults if true get indicator results
     * @param mapResults       if true get map results
     * @param dataResults      if true get data allowed result
     * @return une map de resultid/nom visuel des projets
     * @throws CoserBusinessException
     */
    protected Map<String, String> getZonesIds(ProjectService projectService,
                                              File scanDirectory,
                                              Boolean indicatorResults,
                                              Boolean mapResults,
                                              Boolean dataResults) throws CoserBusinessException {

        Map<String, String> resultIds = new HashMap<String, String>();
        File[] projectFiles = scanDirectory.listFiles();
        if (projectFiles != null) {
            for (File projectFile : projectFiles) {
                if (projectFile.isDirectory()) {

                    File selectionsDirectory = new File(projectFile, CoserConstants.STORAGE_SELECTION_DIRECTORY);
                    File[] selectionFiles = selectionsDirectory.listFiles();
                    if (selectionFiles != null) {
                        for (File selectionFile : selectionFiles) {
                            if (selectionFile.isDirectory()) {

                                File resultsDirectory = new File(selectionFile, CoserConstants.STORAGE_RESULTS_DIRECTORY);
                                File[] resultFiles = resultsDirectory.listFiles();
                                if (resultFiles != null) {
                                    for (File resultFile : resultFiles) {
                                        if (resultFile.isDirectory()) {
                                            RSufiResult rsufiResult = projectService.getRSufiResult(resultFile);

                                            // return result depending on result type
                                            if ((indicatorResults == null || rsufiResult.isIndicatorsResult() == indicatorResults)
                                                &&
                                                (mapResults == null || rsufiResult.isMapsResult() == mapResults)
                                                &&
                                                (dataResults == null || rsufiResult.isDataAllowed() == dataResults)) {
                                                String resultResultId = rsufiResult.getZone();
                                                if (StringUtils.isNotBlank(resultResultId)) {
                                                    String resultPath = projectFile.getName() + "/" +
                                                                        selectionFile.getName() + "/" +
                                                                        resultFile.getName();
                                                    resultIds.put(resultResultId, resultPath);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return resultIds;
    }
}

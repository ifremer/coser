/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Coser class loader.
 *
 * Used to load custom xwork validation file.
 *
 * @author chatellier
 */
public class CoserClassLoader extends ClassLoader {

    private static final Log log = LogFactory.getLog(CoserClassLoader.class);

    protected File validatorsDirectory;

    public CoserClassLoader(ClassLoader parent) {
        super(parent);
    }

    public void setValidatorsDirectory(File validatorsDirectory) {
        this.validatorsDirectory = validatorsDirectory;
    }

    @Override
    public URL getResource(String name) {

        URL result = null;

        if (name.matches("/fr/ifremer/coser/.+\\.xml")) {

            // cherche sur le disque
            File valFile = new File(validatorsDirectory, name);
            if (valFile.isFile()) {

                if (log.isDebugEnabled()) {
                    log.debug("Locating resources " + name + " to " + valFile.getAbsolutePath());
                }

                try {
                    result = valFile.toURI().toURL();
                } catch (MalformedURLException ex) {
                    throw new RuntimeException("Can't get validator url", ex);
                }
            }

            // cherche dans le classpath
            if (result == null) {
                result = super.getResource("validators" + name);
            }
        }

        // sinon, recherche par default
        if (result == null) {
            result = super.getResource(name);
        }
        return result;
    }
}

package fr.ifremer.coser;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.bean.IndicatorMap;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.CoserCommandFactory;
import fr.ifremer.coser.result.CoserMainRepositoryProvider;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.util.Charts;
import fr.ifremer.coser.result.util.Extracts;
import fr.ifremer.coser.result.util.Reports;

import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public interface CoserApplicationContext {

    /**
     * @return application configuration
     */
    CoserBusinessConfig getConfig();

    /**
     * @return all types of {@link ResultRepositoryType} found via the {@link ServiceLoader} mecanism
     */
    Set<ResultRepositoryType> getRepositoryTypes();

    /**
     * @return repository provider
     */
    CoserMainRepositoryProvider getRepositoryProvider();

    /**
     * @return factory of command
     */
    CoserCommandFactory getCommandFactory();

    /**
     * @return indicator map
     */
    IndicatorMap getIndicatorMap();

    /**
     * @return zone map
     */
    ZoneMap getZoneMap();

    /**
     * @return reports helper
     */
    Reports getReports();

    /**
     * @return charts helper
     */
    Charts getCharts();

    /**
     * @return extracts helper
     */
    Extracts getExtracts();
}

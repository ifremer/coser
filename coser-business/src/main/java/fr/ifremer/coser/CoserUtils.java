/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.FileUtil;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Coser utility class.
 *
 * @author chatellier
 */
public class CoserUtils {

    public static final char BRACKET_STRING_SEPARATOR = ';';

    /** Pattern pour ajouter les suffix (probleme de replace sur le 'dernier' .) */
    public static final Pattern FILENAME_SUFFIX_PATTERN = Pattern.compile("^(.+)(\\.[^\\.]+)$");

    /**
     * Converti une collection de string en une string
     * delimité par les () pour pouvoir être facilement relu par
     * {@link #splitWithBrackets(String, char)}
     *
     * @param args
     * @return string
     */
    public static String convertBracketString(List<String> args) {
        StringBuilder buffer = new StringBuilder();

        Iterator<String> itArgs = args.iterator();
        while (itArgs.hasNext()) {
            buffer.append('(');
            String arg = itArgs.next();
            // escape special chars
            // just (, ) and escape char \
            arg = StringUtils.replace(arg, "\\", "\\\\");
            arg = StringUtils.replace(arg, ")", "\\)");
            arg = StringUtils.replace(arg, "(", "\\(");
            buffer.append(arg);
            buffer.append(')');
            if (itArgs.hasNext()) {
                buffer.append(BRACKET_STRING_SEPARATOR);
            }
        }
        return buffer.toString();
    }

    /**
     * Split line counting opened ( and ) and take care about escaped \\).
     *
     * @param str       string to parse
     * @param separator to take care
     * @return string splited
     */
    protected static List<String> splitWithBrackets(String str, char separator) {

        List<String> result = new ArrayList<String>();

        StringBuffer op = new StringBuffer(); // stack of {([< currently open
        int opened = 0;
        boolean escapeNext = false;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (escapeNext) {
                op.append(c);
                escapeNext = false;
            } else if (c == '\\') {
                escapeNext = true;
            } else if (c == '(') {
                op.append(c);
                opened++;
            } else if (c == ')') {
                op.append(c);
                opened--;
            } else if (c == separator) {
                if (opened == 0) {
                    result.add(op.toString());
                    op = new StringBuffer();
                } else {
                    op.append(c);
                }
            } else {
                op.append(c);
            }
        }

        if (op.length() > 0) {
            result.add(op.toString());
        }

        return result;
    }

    /**
     * Parse la string avec les gestions des () en les supprimant.
     *
     * @param argsString string to parse
     * @return parse list
     */
    public static List<String> convertBracketToList(String argsString) {
        List<String> args = splitWithBrackets(argsString, BRACKET_STRING_SEPARATOR);

        List<String> listArgs = new ArrayList<String>();
        for (String arg : args) {
            if (arg.startsWith("(") && arg.endsWith(")")) {
                arg = StringUtils.removeStart(arg, "(");
                arg = StringUtils.removeEnd(arg, ")");

                // unescape () and \
                arg = StringUtils.replace(arg, "\\)", ")");
                arg = StringUtils.replace(arg, "\\(", "(");
                arg = StringUtils.replace(arg, "\\\\", "\\");

                listArgs.add(arg);
            }
        }
        return listArgs;
    }

    /**
     * Dans un nom de fichier, ajoute un suffix dans le nom du fichier
     * juste avant l'extension.
     *
     * Exemple :
     * captures.csv > captures_co.csv (ajout du suffix _co)
     * captures > captures_co (si pas d'extension)
     *
     * @param str    nom du fichier
     * @param suffix suffix a ajouter
     * @return nouveau nom du fichier
     */
    public static String addSuffixBeforeExtension(String str, String suffix) {
        String result = null;
        Matcher matcher = FILENAME_SUFFIX_PATTERN.matcher(str);
        if (matcher.matches()) {
            result = matcher.group(1) + suffix + matcher.group(2);
        } else {
            result = str + suffix;
        }
        return result;
    }

    /**
     * Split string as string list using "," separator.
     *
     * @param str string to split
     * @return string list
     */
    public static List<String> splitAsList(String str) {
        String[] strArray = str.split(",");
        List<String> strList = Arrays.asList(strArray);
        return strList;
    }

    /**
     * Convert string to {@link Document}.
     *
     * @param content content
     * @return document
     * @throws IOException
     */
    public static Document parseDocument(String content) throws IOException {
        Document result = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            result = builder.parse(new InputSource(new StringReader(content)));
        } catch (ParserConfigurationException e) {
            throw new IOException(e);
        } catch (SAXException e) {
            throw new IOException(e);
        }
        return result;
    }

    /**
     * Sort a map by value.
     * See http://stackoverflow.com/a/2581754/1165234
     *
     * @param map map to sort
     * @return sorted map
     */
    public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {

            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        LinkedHashMap<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    /**
     * Sort a collection with order defined by a LinkedHashMap keySet.
     *
     * @param map  map containing order in keys
     * @param list list to sort
     * @return list sorted
     */
    public static <K> List<K> sortCollectionWithMapKeys(LinkedHashMap<K, ?> map, Collection<K> list) {
        List<K> keys = new ArrayList<K>(map.keySet());
        keys.retainAll(list);
        return keys;
    }

    /**
     * N'utilise pas la methode de commons-fileutils, car lorsqu'un répertoire
     * est refusé, il ne descend pas dans les sous répertoire alors que dans
     * notre cas il le faut.
     *
     * @param srcDir     source directory to copy
     * @param destDir    destination directory
     * @param fileFilter file filter for file to copy
     * @throws IOException
     */
    public static void customCopyDirectory(File srcDir,
                                           File destDir,
                                           FileFilter fileFilter) throws IOException {
        List<File> files = FileUtil.getFilteredElements(srcDir, null, true);
        for (File file : files) {
            if (fileFilter.accept(file)) {
                String path = file.getPath().substring(srcDir.getPath().length());

                File destFile = new File(destDir, path);
                if (file.isDirectory()) {
                    FileUtils.forceMkdir(destFile);
                } else {
                    FileUtils.copyFile(file, destFile);
                }
            }
        }
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.command;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.storage.DataStorage;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Command pattern object.
 *
 * @author chatellier
 */
public class DeleteLineCommand extends CategoryLineCommand {

    @Override
    public void doCommand(Project project, AbstractDataContainer container) throws CoserBusinessException {

        DataStorage dataStorage = null;
        DataStorage deletedDataStorage = null;

        // get data storage depending on category
        switch (category) {
            case CATCH:
                dataStorage = container.getCatch();
                deletedDataStorage = container.getDeletedCatch();
                break;
            case HAUL:
                dataStorage = container.getHaul();
                deletedDataStorage = container.getDeletedHaul();
                break;
            case LENGTH:
                dataStorage = container.getLength();
                deletedDataStorage = container.getDeletedLength();
                break;
            case STRATA:
                dataStorage = container.getStrata();
                deletedDataStorage = container.getDeletedStrata();
                break;
        }

        // parcourt des lignes
        int index = dataStorage.indexOf(lineNumber);
        if (index >= 0) {
            String[] data = dataStorage.remove(index);
            deletedDataStorage.add(data);
        } else {
            // if not found, throw business exception
            throw new CoserBusinessException(t("Can't find line %s for deletion", lineNumber));
        }
    }

    @Override
    public void undoCommand(Project project, AbstractDataContainer container) throws CoserBusinessException {

        DataStorage dataStorage = null;
        DataStorage deletedDataStorage = null;

        // get data storage depending on category
        switch (category) {
            case CATCH:
                dataStorage = container.getCatch();
                deletedDataStorage = container.getDeletedCatch();
                break;
            case HAUL:
                dataStorage = container.getHaul();
                deletedDataStorage = container.getDeletedHaul();
                break;
            case LENGTH:
                dataStorage = container.getLength();
                deletedDataStorage = container.getDeletedLength();
                break;
            case STRATA:
                dataStorage = container.getStrata();
                deletedDataStorage = container.getDeletedStrata();
                break;
        }

        int indexDeletedData = deletedDataStorage.indexOf(lineNumber);

        if (indexDeletedData != -1) {
            String[] deletedDataLine = deletedDataStorage.get(indexDeletedData);

            // search for new insert point
            int lineNumberInt = Integer.parseInt(lineNumber);

            // check que la ligne a restaurer n'existe pas deja
            int indexData = dataStorage.indexOf(String.valueOf(lineNumberInt));
            if (indexData != -1) {
                throw new CoserBusinessException("Original line already exists !");
            }

            do {
                lineNumberInt--;
                indexData = dataStorage.indexOf(String.valueOf(lineNumberInt));
            } while (indexData < 0 && lineNumberInt > 0);

            // re add deleted data
            if (indexData >= 0) {
                // +1 car on l'insert apres la ligne qu'on a trouvé
                dataStorage.add(indexData + 1, deletedDataLine);
            } else {
                dataStorage.add(0, deletedDataLine);
            }
            deletedDataStorage.remove(indexDeletedData);
        }

        // if not found, throw business exception
        else {
            throw new CoserBusinessException(t("Can't find line %s for undeletion", lineNumber));
        }
    }

    /*
     * @see fr.ifremer.coser.command.Command#toStringRepresentation()
     */
    @Override
    public String toStringRepresentation() {
        List<String> args = new ArrayList<String>();
        args.add("category=" + category.toString());
        args.add("lineNumber=" + lineNumber);
        return CoserUtils.convertBracketString(args);
    }

    /*
     * @see fr.ifremer.coser.command.Command#fromStringRepresentation(java.lang.String)
     */
    @Override
    public void fromStringRepresentation(String representation) {

        List<String> args = CoserUtils.convertBracketToList(representation);
        for (String arg : args) {
            int indexOfEqual = arg.indexOf('=');
            String argAttribute = arg.substring(0, indexOfEqual);
            String value = arg.substring(indexOfEqual + 1);
            if (argAttribute.equals("category")) {
                category = Category.valueOf(value);
            } else if (argAttribute.equals("lineNumber")) {
                lineNumber = value;
            }
        }
    }

    @Override
    public String getLogString(Project project, AbstractDataContainer container) {
        return t("coser.business.command.deleteline.log", t(category.getTranslationKey()), lineNumber);
    }

    @Override
    public String getDescription(Project project, AbstractDataContainer container) {
        String desc = t("coser.business.command.deleteline.desc", t(category.getTranslationKey()), lineNumber);
        if (StringUtils.isNotBlank(comment)) {
            desc += " (" + comment + ")";
        }
        return desc;
    }

    @Override
    public String toString() {
        return "Delete line " + lineNumber + " on " + category;
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.command;

import fr.ifremer.coser.CoserConstants.Category;

/**
 * Commande faite une une category de fichier a une ligne specifique.
 *
 * @author chatellier
 */
public abstract class CategoryLineCommand extends Command {

    /** Category de données sur lequel porte l'action. */
    protected Category category;

    /** Line index of object to do command to. */
    protected String lineNumber;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

}

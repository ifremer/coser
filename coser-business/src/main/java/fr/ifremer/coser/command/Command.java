/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.command;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;

/**
 * Command interface.
 *
 * @author chatellier
 */
public abstract class Command {

    /**
     * UUID de la commande. Les commandes ayant un UUID commun font partie
     * du même groupe de commandes.
     */
    protected String commandUUID;

    /**
     * Command comment.
     */
    protected String comment;

    /**
     * UUID de la commande. Les commandes ayant un UUID commun font partie
     * du même groupe de commandes.
     *
     * @return command UUID
     * @see java.util.UUID
     */
    public String getCommandUUID() {
        return commandUUID;
    }

    /**
     * UUID de la commande. Les commandes ayant un UUID commun font partie
     * du même groupe de commandes.
     *
     * @param commandUUID new uuid
     * @see java.util.UUID
     */
    public void setCommandUUID(String commandUUID) {
        this.commandUUID = commandUUID;
    }

    /**
     * Command comment.
     *
     * @return command comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Command comment.
     *
     * @param comment command comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Do command on project.
     *
     * @param project   project
     * @param container container
     * @throws CoserBusinessException
     */
    public abstract void doCommand(Project project, AbstractDataContainer container) throws CoserBusinessException;

    /**
     * Undo command on project.
     *
     * @param project   project
     * @param container container
     * @throws CoserBusinessException
     */
    public abstract void undoCommand(Project project, AbstractDataContainer container) throws CoserBusinessException;

    /**
     * Get command argument string representation.
     *
     * @return command argument string representation
     */
    public abstract String toStringRepresentation();

    /**
     * Init command from string representation.
     *
     * @param representation string command representation
     */
    public abstract void fromStringRepresentation(String representation);

    /**
     * Return human readable string for log output.
     *
     * @param project   used to convert species code
     * @param container rarement utile, mais dans certains cas, sert a avoir les
     *                  vrais valeur de champs au lieu des noms techniques
     * @return log representation
     */
    public abstract String getLogString(Project project, AbstractDataContainer container);

    /**
     * Return human readable string for ui display (used in selection replay,
     * and undo redo commands).
     *
     * @param project   used to convert species code
     * @param container rarement utile, mais dans certains cas, sert a avoir les
     *                  vrais valeur de champs au lieu des noms techniques
     * @return string description (i18n)
     */
    public abstract String getDescription(Project project, AbstractDataContainer container);
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.command;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Length;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Merge species command.
 *
 * @author chatellier
 */
public class MergeSpeciesCommand extends Command {

    private static final Log log = LogFactory.getLog(MergeSpeciesCommand.class);

    protected String newSpecyName;

    protected String[] speciesNames;

    public String getNewSpecyName() {
        return newSpecyName;
    }

    public void setNewSpecyName(String newSpecyName) {
        this.newSpecyName = newSpecyName;
    }

    public String[] getSpeciesNames() {
        return speciesNames;
    }

    public void setSpeciesNames(String[] speciesNames) {
        this.speciesNames = speciesNames.clone();
    }

    /*
     * @see fr.ifremer.coser.command.Command#doCommand(fr.ifremer.coser.bean.Project)
     */
    @Override
    public void doCommand(Project project, AbstractDataContainer container) throws CoserBusinessException {

        project = mergeCatch(project, container, newSpecyName, speciesNames);
        project = mergeLength(project, container, newSpecyName, speciesNames);

    }

    /*
     * @see fr.ifremer.coser.command.Command#undoCommand(fr.ifremer.coser.bean.Project)
     */
    @Override
    public void undoCommand(Project project, AbstractDataContainer container) throws CoserBusinessException {
        throw new UnsupportedOperationException("Merge operation can't be undone");
        // can't undo merge
    }

    /**
     * Fusion d'espece dans les données de captures.
     *
     * @param project      project
     * @param container    data container
     * @param newSpecyName new specy name (after merge)
     * @param speciesNames species name to merge
     * @return project
     */
    protected Project mergeLength(Project project, AbstractDataContainer container,
                                  String newSpecyName, String... speciesNames) {

        // Campagne;Annee;Trait;Espece;Sexe;Maturite;Longueur;Nombre;Poids;Age

        // regroupement par Campagne;Annee;Trait;Espece;Sexe;Maturite;Longueur;Age
        Map<String, Integer> firstLineForKey = new HashMap<String, Integer>();

        // parcours des elements
        Iterator<String[]> itTuple = container.getLength().iterator(true);
        int lineIndex = 1; // skip header
        while (itTuple.hasNext()) {
            String[] tuple = itTuple.next();

            // test si l'espece en cours fait partie de celle a merger
            String species = tuple[Length.INDEX_SPECIES];
            boolean specyFound = false;
            for (String specy : speciesNames) {
                if (specy.equals(species)) {
                    specyFound = true;
                }
            }

            // si l'espece est a merger, on se souvient du tuple
            // principale a merge, ou on merge avec le tuple
            // principal si on a deja un tuple principal
            if (specyFound) {

                // compute key (attention a ne pas prendre en compte l'espece : elle change)
                StringBuilder sb = new StringBuilder();
                for (int tupleIndex = 0; tupleIndex < tuple.length; ++tupleIndex) {
                    if (tupleIndex == Length.INDEX_SURVEY || tupleIndex == Length.INDEX_YEAR ||
                        tupleIndex == Length.INDEX_HAUL || tupleIndex == Length.INDEX_SEX ||
                        tupleIndex == Length.INDEX_MATURITY || tupleIndex == Length.INDEX_LENGTH ||
                        tupleIndex == Length.INDEX_AGE) {
                        sb.append(tuple[tupleIndex]).append(';');
                    }
                }

                String key = sb.toString();
                Integer firstLineFound = firstLineForKey.get(key);

                if (firstLineFound == null) {
                    // on modifie le nom de la ligne dans tout les cas
                    tuple[Length.INDEX_SPECIES] = newSpecyName;
                    container.getLength().set(lineIndex, tuple);

                    firstLineForKey.put(key, lineIndex);
                    lineIndex++;
                } else {
                    String[] previousTuple = container.getLength().get(firstLineFound);
                    String[] mergedTuple = mergeLengths(previousTuple, tuple);
                    // et on supprime le tuple
                    // qui a ete merge
                    itTuple.remove();
                    // on update le precedent qui contient maintenant le merge
                    container.getLength().set(firstLineFound, mergedTuple);
                }
            } else {
                ++lineIndex;
            }
        }
        return project;
    }

    /**
     * Merge deux lines des catch.
     *
     * Le resultat est mergé dans {@code tuple1} et retourné.
     *
     * Somme les "Nombre" et "Poids"
     *
     * @param tuple1 first tuple to merge
     * @param tuple2 second tuple to merge
     * @return merged tuples
     */
    protected String[] mergeLengths(String[] tuple1, String[] tuple2) {

        try {
            // number
            if (isNotAvailableData(tuple1[Length.INDEX_NUMBER]) || isNotAvailableData(tuple2[Length.INDEX_NUMBER])) {
                tuple1[Length.INDEX_NUMBER] = CoserConstants.VALIDATION_NA;
            } else {
                double nombre1 = Double.parseDouble(tuple1[Length.INDEX_NUMBER]);
                double nombre2 = Double.parseDouble(tuple2[Length.INDEX_NUMBER]);
                tuple1[Length.INDEX_NUMBER] = String.valueOf(nombre1 + nombre2);
            }

            // weight
            if (isNotAvailableData(tuple1[Length.INDEX_WEIGHT]) || isNotAvailableData(tuple2[Length.INDEX_WEIGHT])) {
                tuple1[Length.INDEX_WEIGHT] = CoserConstants.VALIDATION_NA;
            } else {
                double poids1 = Double.parseDouble(tuple1[Length.INDEX_WEIGHT]);
                double poids2 = Double.parseDouble(tuple2[Length.INDEX_WEIGHT]);
                tuple1[Length.INDEX_WEIGHT] = String.valueOf(poids1 + poids2);
            }
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't convert data as double for merge", ex);
            }
        }
        return tuple1;
    }

    /**
     * Return true if a value
     *
     * @param data data value to check
     * @return {@code true} if value is empty or NA
     */
    protected boolean isNotAvailableData(String data) {
        boolean result = false;
        if (StringUtils.isEmpty(data) || data.equals(CoserConstants.VALIDATION_NA)) {
            result = true;
        }
        return result;
    }

    /**
     * Fusion d'especes dans les données de taille.
     *
     * Peut etre a revoir, car ca fonctionne, mais le traitement
     * est dépendant de l'ordre.
     *
     * @param project      project
     * @param container    data container
     * @param newSpecyName new specy name (after merge)
     * @param speciesNames species name to merge
     * @return project
     */
    protected Project mergeCatch(Project project, AbstractDataContainer container,
                                 String newSpecyName, String... speciesNames) {

        // "Campagne","Annee","Trait","Espece","Nombre","Poids"

        // regroupement par campagne/annee/trait
        Map<String, Integer> firstLineForKey = new HashMap<String, Integer>();

        // parcours des elements
        Iterator<String[]> itTuple = container.getCatch().iterator(true);
        int lineIndex = 1; // header skiped
        while (itTuple.hasNext()) {
            String[] tuple = itTuple.next();

            // test si l'espece en cours fait partie de celle a merger
            String species = tuple[Catch.INDEX_SPECIES];
            boolean specyFound = false;
            for (String specy : speciesNames) {
                if (specy.equals(species)) {
                    specyFound = true;
                }
            }

            // si l'espece est a merger, on se souvient du tuple
            // principale a merge, ou on merge avec le tuple
            // principal si on a deja un tuple principal
            if (specyFound) {

                // compute key (attention a ne pas prendre en compte l'espece : elle change)
                StringBuilder sb = new StringBuilder();
                for (int tupleIndex = 0; tupleIndex < tuple.length; ++tupleIndex) {
                    if (tupleIndex == Catch.INDEX_SURVEY || tupleIndex == Catch.INDEX_YEAR ||
                        tupleIndex == Catch.INDEX_HAUL) {
                        sb.append(tuple[tupleIndex]).append(';');
                    }
                }

                String key = sb.toString();
                Integer firstLineFound = firstLineForKey.get(key);

                if (firstLineFound == null) {
                    // on modifie le nom de la ligne dans tout les cas
                    tuple[Catch.INDEX_SPECIES] = newSpecyName;
                    container.getCatch().set(lineIndex, tuple);

                    firstLineForKey.put(key, lineIndex);
                    lineIndex++;
                } else {
                    String[] previousTuple = container.getCatch().get(firstLineFound);
                    String[] mergeTuple = mergeCatches(previousTuple, tuple);
                    // et on supprime le tuple
                    // qui a ete merge
                    itTuple.remove();
                    // on update le precedent qui contient maintenant le merge
                    container.getCatch().set(firstLineFound, mergeTuple);
                }

            } else {
                lineIndex++;
            }

        }

        return project;
    }

    /**
     * Merge deux lines des catch.
     *
     * Le resultat est mergé dans {@code tuple1} et retourné.
     *
     * Somme les "Nombre" et "Poids"
     *
     * @param tuple1 tuple1
     * @param tuple2 tuple2
     * @return tuple1
     */
    protected String[] mergeCatches(String[] tuple1, String[] tuple2) {

        // "Campagne","Annee","Trait","Espece","Nombre","Poids"

        try {
            double nombre1 = Double.parseDouble(tuple1[Catch.INDEX_NUMBER]);
            double nombre2 = Double.parseDouble(tuple2[Catch.INDEX_NUMBER]);
            double poids1 = Double.parseDouble(tuple1[Catch.INDEX_WEIGHT]);
            double poids2 = Double.parseDouble(tuple2[Catch.INDEX_WEIGHT]);

            tuple1[Catch.INDEX_NUMBER] = String.valueOf(nombre1 + nombre2);
            tuple1[Catch.INDEX_WEIGHT] = String.valueOf(poids1 + poids2);
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't convert data as double for merge", ex);
            }
        }
        return tuple1;
    }

    /*
     * @see fr.ifremer.coser.command.Command#toStringRepresentation()
     */
    @Override
    public String toStringRepresentation() {
        List<String> args = new ArrayList<String>();
        args.add("newSpecyName=" + newSpecyName);
        args.add("speciesNames=" + CoserUtils.convertBracketString(Arrays.asList(speciesNames)));
        return CoserUtils.convertBracketString(args);
    }

    /*
     * @see fr.ifremer.coser.command.Command#fromStringRepresentation(java.lang.String)
     */
    @Override
    public void fromStringRepresentation(String representation) {

        List<String> args = CoserUtils.convertBracketToList(representation);
        for (String arg : args) {
            int indexOfEqual = arg.indexOf('=');
            String argAttribute = arg.substring(0, indexOfEqual);
            String value = arg.substring(indexOfEqual + 1);
            if (argAttribute.equals("newSpecyName")) {
                newSpecyName = value;
            } else if (argAttribute.equals("speciesNames")) {
                List<String> speciesNamesList = CoserUtils.convertBracketToList(value);
                speciesNames = speciesNamesList.toArray(new String[speciesNamesList.size()]);
            }
        }
    }

    @Override
    public String getLogString(Project project, AbstractDataContainer container) {
        StringBuilder speciesAsString = new StringBuilder(256);
        String separator = "";
        for (String speciesName : speciesNames) {
            speciesAsString.append(separator);
            speciesAsString.append(project.getDisplaySpeciesText(speciesName));
            separator = ", ";
        }
        String log = null;
        if (speciesNames.length == 1) {
            log = t("coser.business.command.mergespecies.rename.log",
                    project.getDisplaySpeciesText(newSpecyName), speciesAsString.toString());
        } else {
            log = t("coser.business.command.mergespecies.log",
                    project.getDisplaySpeciesText(newSpecyName), speciesAsString.toString());
        }
        return log;
    }

    @Override
    public String getDescription(Project project, AbstractDataContainer container) {
        StringBuilder speciesAsString = new StringBuilder(256);
        String separator = "";
        for (String speciesName : speciesNames) {
            speciesAsString.append(separator);
            speciesAsString.append(project.getDisplaySpeciesText(speciesName));
            separator = ", ";
        }
        String desc = null;
        if (speciesNames.length == 1) {
            desc = t("coser.business.command.mergespecies.rename.desc",
                     project.getDisplaySpeciesText(newSpecyName), speciesNames.length, speciesAsString.toString());
        } else {
            desc = t("coser.business.command.mergespecies.desc",
                     project.getDisplaySpeciesText(newSpecyName), speciesNames.length, speciesAsString.toString());
        }
        if (StringUtils.isNotBlank(comment)) {
            desc += " (" + comment + ")";
        }
        return desc;
    }

    @Override
    public String toString() {
        return "Merge species to " + newSpecyName;
    }
}

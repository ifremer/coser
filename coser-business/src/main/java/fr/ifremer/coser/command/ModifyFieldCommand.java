/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.command;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.data.AbstractDataEntity;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Haul;
import fr.ifremer.coser.data.Length;
import fr.ifremer.coser.data.Strata;
import fr.ifremer.coser.storage.DataStorage;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Command pattern object.
 *
 * @author chatellier
 */
public class ModifyFieldCommand extends CategoryLineCommand {

    /** Field index to to command. */
    protected String fieldName;

    /** Field actual value. */
    protected String currentValue;

    /** Field new value. */
    protected String newValue;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Object getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public Object getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Override
    public void doCommand(Project project, AbstractDataContainer container) throws CoserBusinessException {

        AbstractDataEntity beanData = null;
        DataStorage dataStorage = null;
        switch (category) {
            case CATCH:
                beanData = new Catch();
                dataStorage = container.getCatch();
                break;
            case HAUL:
                beanData = new Haul();
                dataStorage = container.getHaul();
                break;
            case LENGTH:
                beanData = new Length();
                dataStorage = container.getLength();
                break;
            case STRATA:
                beanData = new Strata();
                dataStorage = container.getStrata();
                break;
        }

        int lineIndex = dataStorage.indexOf(lineNumber);
        String[] data = dataStorage.get(lineIndex);
        beanData.setData(data);
        try {
            String beanFieldName = Introspector.decapitalize(fieldName);
            String stringFieldProperty = beanFieldName + "AsString";
            String dataValue = (String) PropertyUtils.getProperty(beanData, stringFieldProperty);
            if (dataValue.equals(currentValue)) {
                PropertyUtils.setProperty(beanData, stringFieldProperty, newValue);

                dataStorage.set(lineIndex, beanData.getData());
            } else {
                throw new CoserBusinessException(t("Can't replace data value. Expected %s but was %s", currentValue, dataValue));
            }
        } catch (Exception ex) {
            throw new CoserBusinessException("Can't replace data field value", ex);
        }
    }

    @Override
    public void undoCommand(Project project, AbstractDataContainer container) throws CoserBusinessException {
        AbstractDataEntity beanData = null;
        DataStorage dataStorage = null;
        switch (category) {
            case CATCH:
                beanData = new Catch();
                dataStorage = container.getCatch();
                break;
            case HAUL:
                beanData = new Haul();
                dataStorage = container.getHaul();
                break;
            case LENGTH:
                beanData = new Length();
                dataStorage = container.getLength();
                break;
            case STRATA:
                beanData = new Strata();
                dataStorage = container.getStrata();
                break;
        }

        int lineIndex = dataStorage.indexOf(lineNumber);
        String[] data = dataStorage.get(lineIndex);
        beanData.setData(data);
        try {
            String beanFieldName = Introspector.decapitalize(fieldName);
            String stringFieldProperty = beanFieldName + "AsString";
            String dataValue = (String) PropertyUtils.getProperty(beanData, stringFieldProperty);
            if (dataValue.equals(newValue)) {
                PropertyUtils.setProperty(beanData, stringFieldProperty, currentValue);

                dataStorage.set(lineIndex, beanData.getData());
            } else {
                throw new CoserBusinessException(t("Can't replace data value. Expected %s but was %s", newValue, dataValue));
            }
        } catch (Exception ex) {
            throw new CoserBusinessException("Can't replace data field value", ex);
        }
    }

    /*
     * @see fr.ifremer.coser.command.Command#toStringRepresentation()
     */
    @Override
    public String toStringRepresentation() {
        List<String> args = new ArrayList<String>();
        args.add("category=" + category.toString());
        args.add("lineNumber=" + lineNumber);
        args.add("fieldName=" + fieldName);
        args.add("currentValue=" + currentValue);
        args.add("newValue=" + newValue);
        return CoserUtils.convertBracketString(args);
    }

    /*
     * @see fr.ifremer.coser.command.Command#fromStringRepresentation(java.lang.String)
     */
    @Override
    public void fromStringRepresentation(String representation) {

        List<String> args = CoserUtils.convertBracketToList(representation);
        for (String arg : args) {
            int indexOfEqual = arg.indexOf('=');
            String argAttribute = arg.substring(0, indexOfEqual);
            String value = arg.substring(indexOfEqual + 1);
            if (argAttribute.equals("category")) {
                category = Category.valueOf(value);
            } else if (argAttribute.equals("lineNumber")) {
                lineNumber = value;
            } else if (argAttribute.equals("fieldName")) {
                fieldName = value;
            } else if (argAttribute.equals("currentValue")) {
                currentValue = value;
            } else if (argAttribute.equals("newValue")) {
                newValue = value;
            }
        }
    }

    @Override
    public String getLogString(Project project, AbstractDataContainer container) {
        String realFieldName = getRealFieldName(container);
        return t("coser.business.command.modifyfield.log", t(category.getTranslationKey()), lineNumber, realFieldName, currentValue, newValue);
    }

    @Override
    public String getDescription(Project project, AbstractDataContainer container) {
        String realFieldName = getRealFieldName(container);
        String desc = t("coser.business.command.modifyfield.desc", t(category.getTranslationKey()), lineNumber, realFieldName, currentValue, newValue);
        if (StringUtils.isNotBlank(comment)) {
            desc += " (" + comment + ")";
        }
        return desc;
    }

    /**
     * Look for real field name.
     *
     * Les nom de champs utilisé par introspection et sauvegardé sont les
     * nom anglais (toujours). Par contre pour la sortie log, l'utilisateur
     * est plutôt interessé par le nom original du fichier.
     *
     * @param container le container pour lire les headers csv
     * @return le nom original du fichier
     */
    protected String getRealFieldName(AbstractDataContainer container) {
        String[] headers = null;
        DataStorage dataStorage = null;
        switch (category) {
            case CATCH:
                headers = Catch.EN_HEADERS;
                dataStorage = container.getCatch();
                break;
            case HAUL:
                headers = Haul.EN_HEADERS;
                dataStorage = container.getHaul();
                break;
            case LENGTH:
                headers = Length.EN_HEADERS;
                dataStorage = container.getLength();
                break;
            case STRATA:
                headers = Strata.EN_HEADERS;
                dataStorage = container.getStrata();
                break;
        }

        int index = ArrayUtils.indexOf(headers, fieldName);
        // 0 = header
        // +1 = les données contiennent le champs 'line' en plus
        String realFieldName = dataStorage.get(0)[index + 1];
        return realFieldName;
    }

    @Override
    public String toString() {
        return "Modify field " + fieldName + " on line " + lineNumber;
    }
}

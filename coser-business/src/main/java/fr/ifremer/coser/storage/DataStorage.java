/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.storage;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Data storage utiliser pour manipuler dans l'application des
 * tableau de String, mais les stocker en back différement.
 *
 * Cette interface respecte globalement l'interface d'une {@link java.util.List}.
 *
 * @author chatellier
 */
public interface DataStorage extends Iterable<String[]>, Serializable {

    /**
     * Add new data into storage.
     *
     * @param data data to add
     */
    public void add(String[] data);

    /**
     * Add new data into storage.
     *
     * @param index index to insert data
     * @param data  data to add
     */
    public void add(int index, String[] data);

    /**
     * Get data at specified index.
     *
     * @param index index
     * @return data at index
     */
    public String[] get(int index);

    /**
     * Storage size.
     *
     * @return storage size
     */
    public int size();

    /**
     * Replace data at specified index.
     *
     * @param index index
     * @param data  data to set
     * @return previous value
     */
    public String[] set(int index, String[] data);

    /**
     * Remove value at index.
     *
     * @param index index
     * @return removed value
     */
    public String[] remove(int index);

    /**
     * Real storage index of lineNumber.
     *
     * @param lineNumber line number
     * @return storage index
     */
    public int indexOf(String lineNumber);

    /**
     * Return a new iterator, but skip first iterator element (csv header)
     * by calling {@code next()} once.
     *
     * @param skipFirstLine if {@code true}, skip first line
     * @return an Iterator.
     */
    public Iterator<String[]> iterator(boolean skipFirstLine);
}

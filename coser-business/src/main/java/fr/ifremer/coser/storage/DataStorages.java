package fr.ifremer.coser.storage;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import au.com.bytecode.opencsv.CSVReader;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.services.CommonService;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;

import static org.nuiton.i18n.I18n.t;

/**
 * Useful methods around {@link DataStorage}.
 *
 * TODO Should move here all others methods storage-centric from {@link CommonService}.
 *
 * Created on 3/7/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class DataStorages {

    /**
     * Load a csv file using the default csv char separator
     * ({@link CoserConstants#CSV_SEPARATOR_CHAR}).
     *
     * @param file file to load
     * @return data storage with file content
     */
    public static DataStorage load(File file) {
        return load(file, CoserConstants.CSV_SEPARATOR_CHAR);
    }

    /**
     * Load a csv file using the given csv char separator.
     *
     * @param file      file to load
     * @param separator separator to use to load file
     * @return data storage with file content
     */
    public static DataStorage load(File file, char separator) {

        DataStorage content = new MemoryDataStorage();

        CSVReader csvReader = null;
        try {

            InputStream stream = new FileInputStream(file);
            Reader reader = new BufferedReader(new InputStreamReader(stream, CoserConstants.CSV_FILE_ENCODING));
            csvReader = new CSVReader(reader, separator);

            // check header
            String[] line = csvReader.readNext();
            if (line == null || line.length <= 1) {
                throw new CoserTechnicalException(t("Can't read file '%s'. Check CSV file separator",
                                                    file.getAbsolutePath()));
            } else {
                content.add(line);
            }

            while ((line = csvReader.readNext()) != null) {
                if (line.length > 1) {
                    content.add(line);
                }
            }
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't read file", ex);
        } finally {
            IOUtils.closeQuietly(csvReader);
        }

        return content;
    }

    /**
     * Save a datastorage to a file and return its.
     *
     * The file is temporary file (will be deleted at the end life of the jvm),
     * his name is generated then using the {@link File#createTempFile(String, String)} using the given {@code prefix}
     * and {@code suffix}.
     *
     * @param dataStorage the storage to save
     * @param prefix      prefix of generated file name
     * @param suffix      suffix of generated file name
     * @return the file where the storage was saved
     */
    public static File save(DataStorage dataStorage, String prefix, String suffix) throws CoserTechnicalException {

        File file;
        try {
            file = File.createTempFile(prefix, suffix);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not create temp file", e);
        }
        file.deleteOnExit();
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), CoserConstants.CSV_FILE_ENCODING));

            save(dataStorage, writer);
            writer.close();
            writer.close();
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't save data", ex);
        } finally {
            IOUtils.closeQuietly(writer);
        }
        return file;
    }

    public static String toString(DataStorage storage) {
        String rawDataText;
        StringWriter writer = null;
        try {
            writer = new StringWriter();
            DataStorages.save(storage, writer);
            rawDataText = writer.toString();
            writer.close();
        } catch (IOException ex) {
            throw new CoserTechnicalException("Could not close writer", ex);
        } finally {
            IOUtils.closeQuietly(writer);
        }
        return rawDataText;
    }

    /**
     * Save a datastorage to a writer.
     *
     * @param dataStorage the storage to save
     * @param writer      where to save storage
     */
    public static void save(DataStorage dataStorage, Writer writer) throws CoserTechnicalException {

        try {

            for (String[] contentDatas : dataStorage) {
                // start at 1 to not output "line" column
                for (int i = 1; i < contentDatas.length; i++) {

                    if (i != 1) {
                        writer.write(CoserConstants.CSV_SEPARATOR_CHAR);
                    }

                    String contentData = contentDatas[i];

                    if (contentData.indexOf(CoserConstants.CSV_SEPARATOR_CHAR) > -1) {
                        writer.write("\"" + contentData + "\"");
                    } else {
                        writer.write(contentData);
                    }
                }
                writer.write("\n");
            }
            writer.close();
        } catch (IOException ex) {
            throw new CoserTechnicalException("Can't save data", ex);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    /**
     * //TODO Use a walker
     *
     * @param storage    the storage to test
     * @param predicate  predicate to match at least on one row
     * @param skipHeader to skip the header of  storage
     * @param <P>        type of predicate
     * @return {@code true} if a row matchs the predicate
     * @since 1.5
     */
    public static <P extends Predicate<String[]>> boolean match(DataStorage storage,
                                                                P predicate,
                                                                boolean skipHeader) {
        Preconditions.checkNotNull(storage);
        Preconditions.checkNotNull(predicate);

        Iterator<String[]> iterator = storage.iterator(skipHeader);

        boolean result = false;

        while (iterator.hasNext()) {
            String[] tuple = iterator.next();
            if (predicate.apply(tuple)) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Gets a sub storage of all matching rows for the given predicate on the given storage.
     *
     * The result header is the given one in parameter {@code header}.
     *
     * @param storage   storage to read
     * @param predicate predicate to match rows to include in sub storage
     * @param header    new header to use
     * @param <P>       type of predicate
     * @return the sub storage
     */
    public static <P extends Predicate<String[]>> DataStorage sub(DataStorage storage,
                                                                  P predicate,
                                                                  String... header) {

        Preconditions.checkNotNull(storage);
        Preconditions.checkNotNull(predicate);
        Preconditions.checkNotNull(header);

        final DataStorage result = new MemoryDataStorage();
        result.add(header);

        walk(storage, predicate, new DataStorageWalker() {

            @Override
            public void onRow(String... row) {
                result.add(row);
            }
        });
        return result;
    }

    public static <W extends DataStorageWalker> void walk(DataStorage storage,
                                                          W walker) {

        Preconditions.checkNotNull(storage);
        Preconditions.checkNotNull(walker);

        Iterator<String[]> iterator = storage.iterator(true);

        while (iterator.hasNext()) {
            String[] tuple = iterator.next();
            walker.onRow(tuple);
        }
    }

    public static <P extends Predicate<String[]>, W extends DataStorageWalker> void walk(DataStorage storage,
                                                                                         P predicate,
                                                                                         W walker) {

        Preconditions.checkNotNull(storage);
        Preconditions.checkNotNull(predicate);
        Preconditions.checkNotNull(walker);

        Iterator<String[]> iterator = storage.iterator(true);

        while (iterator.hasNext()) {
            String[] tuple = iterator.next();
            if (predicate.apply(tuple)) {
                walker.onRow(tuple);
            }
        }
    }

}

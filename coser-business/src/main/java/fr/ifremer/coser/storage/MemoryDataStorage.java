/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Stockage mémoire des String[] sous forme de String simple.
 *
 * Beaucoup moins couteux en mémoire que les String[] sans pour antant
 * que le temps de parcours soit augmenté.
 *
 * Les String[] sont encodés en CSV et stocké en mémoire en String.
 * Le code CSV a été repris de opencsv, mais sans les ioexception, reader et
 * writer inutiles ici.
 *
 * @author chatellier
 */
public class MemoryDataStorage implements DataStorage {

    private static final long serialVersionUID = 1L;

    /** Stockage des données. */
    protected List<String> listStorage;

    /** Stockage des numero de ligne (String) pour une recherche indexOf plus rapide. */
    protected List<String> lineIndexStorage;

    /** Cache pour les indexOf encore plus rapide (invalidé lors d'un delete). */
    protected Map<String, Integer> lineIndexStorageCache;

    public MemoryDataStorage() {
        listStorage = new ArrayList<String>();
        lineIndexStorage = new ArrayList<String>();
        lineIndexStorageCache = new HashMap<String, Integer>();
    }

    /**
     * Iterator sur les donnnées.
     *
     * {@link #lineIndexIterator} est aussi géré, seulement pour que le
     * remove reste cohérent.
     */
    public class StringListIterator implements Iterator<String[]> {

        protected Iterator<String> listIterator;

        protected Iterator<String> lineIndexIterator;

        public StringListIterator(Iterator<String> internalIterator, Iterator<String> lineIndexIterator) {
            this.listIterator = internalIterator;
            this.lineIndexIterator = lineIndexIterator;
        }

        /*
         * @see java.util.Iterator#hasNext()
         */
        @Override
        public boolean hasNext() {
            return listIterator.hasNext();
        }

        /*
         * @see java.util.Iterator#next()
         */
        @Override
        public String[] next() {

            String nextString = listIterator.next();
            String[] nextArray = stringToArray(nextString);

            lineIndexIterator.next();

            return nextArray;
        }

        /*
         * @see java.util.Iterator#remove()
         */
        @Override
        public void remove() {
            listIterator.remove();
            lineIndexIterator.remove();
        }
    }

    /*
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<String[]> iterator() {
        return iterator(false);
    }

    /**
     * Return a new iterator, but skip first iterator element (csv header)
     * by calling {@code next()} once.
     *
     * @param skipFirstLine if {@code true}, skip first line
     * @return an Iterator.
     */
    @Override
    public Iterator<String[]> iterator(boolean skipFirstLine) {
        Iterator<String[]> iterator = new StringListIterator(listStorage.iterator(), lineIndexStorage.iterator());
        if (skipFirstLine) {
            iterator.next(); // skip header
        }
        return iterator;
    }

    /*
     * @see fr.ifremer.coser.storage.DataStorage#addData(java.lang.String[])
     */
    @Override
    public void add(String[] data) {

        String stringData = arrayToString(data);
        listStorage.add(stringData);
        lineIndexStorage.add(data[0]);
    }

    /*
     * @see fr.ifremer.coser.storage.DataStorage#addData(java.lang.String[])
     */
    @Override
    public void add(int index, String[] data) {

        String stringData = arrayToString(data);
        listStorage.add(index, stringData);
        lineIndexStorage.add(index, data[0]);
        lineIndexStorageCache.clear(); // example (redo commands)
    }

    /*
     * @see fr.ifremer.coser.storage.DataStorage#getData(int)
     */
    @Override
    public String[] get(int index) {
        String stringData = listStorage.get(index);
        String[] dataArray = stringToArray(stringData);
        return dataArray;
    }

    public int indexOf(String lineNumber) {
        Integer indexOf = lineIndexStorageCache.get(lineNumber);
        if (indexOf == null) {
            indexOf = lineIndexStorage.indexOf(lineNumber);
            lineIndexStorageCache.put(lineNumber, indexOf);
        }
        return indexOf;
    }

    public int size() {
        return listStorage.size();
    }

    public String[] set(int index, String[] element) {
        String data = arrayToString(element);
        String old = listStorage.set(index, data);
        String[] oldArray = stringToArray(old);
        lineIndexStorage.set(index, element[0]);
        return oldArray;
    }

    public String[] remove(int index) {
        String old = listStorage.remove(index);
        String[] oldArray = stringToArray(old);
        lineIndexStorage.remove(index);
        lineIndexStorageCache.clear(); // invalidate cache on remove
        return oldArray;
    }

    @Override
    public String toString() {
        return listStorage.toString();
    }

    protected String arrayToString(String[] nextLine) {
        return writeNext(nextLine);
    }

    protected String[] stringToArray(String data) {
        return parseLine(data);
    }

    protected static final char separator = ',';

    protected static final char quotechar = '\"';

    protected static final char escapechar = '\\';

    // Copied from CSVWriter and improved
    protected String writeNext(String[] nextLine) {
        StringBuilder sb = new StringBuilder(128);
        for (int i = 0; i < nextLine.length; i++) {

            if (i != 0) {
                sb.append(separator);
            }

            String nextElement = nextLine[i];
            if (nextElement == null) {
                continue;
            }
            sb.append(quotechar);

            if (nextElement.indexOf(quotechar) != -1 || nextElement.indexOf(escapechar) != -1) {
                sb.append(processLine(nextElement));
            } else {
                sb.append(nextElement);
            }
            sb.append(quotechar);
        }
        return sb.toString();
    }

    // Copied from CSVWriter and improved
    protected StringBuilder processLine(String nextElement) {
        StringBuilder sb = new StringBuilder(128);
        for (int j = 0; j < nextElement.length(); j++) {
            char nextChar = nextElement.charAt(j);
            if (/*escapechar != NO_ESCAPE_CHARACTER && */nextChar == quotechar) {
                sb.append(escapechar).append(nextChar);
            } else if (/*escapechar != NO_ESCAPE_CHARACTER && */nextChar == escapechar) {
                sb.append(escapechar).append(nextChar);
            } else {
                sb.append(nextChar);
            }
        }

        return sb;
    }

    // Copied from CSVParser and improved
    protected String[] parseLine(String nextLine) {

        /*if (!multi && pending != null) {
            pending = null;
        }
        
        if (nextLine == null) {
            if (pending != null) {
                String s = pending;
                pending = null;
                return new String[] {s};
            } else {
                return null;
            }
        }*/

        List<String> tokensOnThisLine = new ArrayList<String>();
        StringBuilder sb = new StringBuilder(128);
        boolean inQuotes = false;
        /*if (pending != null) {
            sb.append(pending);
            pending = null;
            inQuotes = true;
        }*/
        for (int i = 0; i < nextLine.length(); i++) {

            char c = nextLine.charAt(i);
            if (c == escapechar) {
                if (isNextCharacterEscapable(nextLine, inQuotes, i)) {
                    sb.append(nextLine.charAt(i + 1));
                    i++;
                }
            } else if (c == quotechar) {
                if (isNextCharacterEscapedQuote(nextLine, inQuotes, i)) {
                    sb.append(nextLine.charAt(i + 1));
                    i++;
                } else {
                    inQuotes = !inQuotes;
                    // the tricky case of an embedded quote in the middle: a,bc"d"ef,g
                    //if (!strictQuotes) {
                    if (i > 2 //not on the beginning of the line
                        && nextLine.charAt(i - 1) != separator //not at the beginning of an escape sequence
                        && nextLine.length() > (i + 1) &&
                        nextLine.charAt(i + 1) != separator //not at the end of an escape sequence
                            ) {
                        sb.append(c);
                    }
                    //}
                }
            } else if (c == separator && !inQuotes) {
                tokensOnThisLine.add(sb.toString());
                sb = new StringBuilder(128); // start work on next token
            } else {
                //if (!strictQuotes || inQuotes)
                sb.append(c);
            }
        }
        /*// line is done - check status
        if (inQuotes) {
            if (multi) {
                // continuing a quoted section, re-append newline
                sb.append("\n");
                pending = sb.toString();
                sb = null; // this partial content is not to be added to field list yet
            } else {
                throw new IOException("Un-terminated quoted field at end of CSV line");
            }
        }*/
        //if (sb != null) {
        tokensOnThisLine.add(sb.toString());
        //}
        return tokensOnThisLine.toArray(new String[tokensOnThisLine.size()]);
    }

    // Copied from CSVParser
    protected boolean isNextCharacterEscapedQuote(String nextLine, boolean inQuotes, int i) {
        return inQuotes  // we are in quotes, therefore there can be escaped quotes in here.
               && nextLine.length() > (i + 1)  // there is indeed another character to check.
               && nextLine.charAt(i + 1) == quotechar;
    }

    // Copied from CSVParser and improved
    protected boolean isNextCharacterEscapable(String nextLine, boolean inQuotes, int i) {
        return inQuotes  // we are in quotes, therefore there can be escaped quotes in here.
               && nextLine.length() > (i + 1)  // there is indeed another character to check.
               && (nextLine.charAt(i + 1) == quotechar || nextLine.charAt(i + 1) == escapechar);
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.control;

/**
 * Erreur de controle specifique pour les différences des nombres entre
 * les captures et les tailles pour utilisation typée de cette erreur
 * (export html) et information supplémentaires (especes).
 *
 * @author chatellier
 */
public class DiffCatchLengthControlError extends SpeciesControlError {

    /** serialVersionUID */
    private static final long serialVersionUID = 6646855823928956144L;

}

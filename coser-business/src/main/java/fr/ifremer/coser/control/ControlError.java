/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.control;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class regroupant les erreurs de controle d'une entité, son type (erreur,
 * warning...) et les information supplémentaires permettant de la corriger
 * (numéro de ligne).
 *
 * @author chatellier
 */
public class ControlError implements Serializable {


    private static final long serialVersionUID = -1806823191454701123L;

    /** Category de l'erreur. */
    protected Category category;

    /** Niveau de l'erreur (fatal, error, warning, info). */
    protected ValidationLevel level;

    /** Message explicitant l'erreur de validation. */
    protected String message;

    /** Message donnant plus de précision sur l'erreur. */
    protected String detailMessage;

    /** Tip message (can be {@code null}). */
    protected String tipMessage;

    /**
     * Numero des lignes dans le fichier CSV.
     *
     * Peut être vide si l'erreur n'est pas associé à une ligne specifiques.
     */
    protected List<String> lineNumbers;

    public ControlError() {
        // set ArrayList, must be sorted for swing ui to change order in model
        lineNumbers = new ArrayList<String>();
    }

    /**
     * Category de l'erreur.
     *
     * Peut etre null si l'erreur n'est pas associées à une category en particulier.
     *
     * @return category de l'erreur
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Category de l'erreur.
     *
     * @param category category de l'erreur
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * Niveau de l'erreur (fatal, error, warning, info).
     *
     * @return level
     */
    public ValidationLevel getLevel() {
        return level;
    }

    /**
     * Niveau de l'erreur (fatal, error, warning, info).
     *
     * @param level level
     */
    public void setLevel(ValidationLevel level) {
        this.level = level;
    }

    /**
     * Message explicitant l'erreur de validation.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Message explicitant l'erreur de validation.
     *
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Message donnant plus de précision sur l'erreur.
     *
     * Si le message de detail est null, le message est retourné.
     *
     * @return detail message
     */
    public String getDetailMessage() {
        String result = detailMessage;
        if (result == null) {
            result = message;
        }
        return result;
    }

    /**
     * Message donnant plus de précision sur l'erreur.
     *
     * @param detailMessage detail message
     */
    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }

    /**
     * Get tip message (can be {@code null}).
     *
     * @return tip message
     */
    public String getTipMessage() {
        return tipMessage;
    }

    /**
     * Set tip message (can be {@code null}).
     *
     * @param tipMessage tip message
     */
    public void setTipMessage(String tipMessage) {
        this.tipMessage = tipMessage;
    }

    /**
     * Numero des lignes dans le fichier CSV (sorted by implementation).
     *
     * Peut être vide si l'erreur n'est pas associé a une ligne specifiques.
     *
     * @return line number
     */
    public List<String> getLineNumbers() {
        return lineNumbers;
    }

    /**
     * Numero des lignes dans le fichier CSV.
     *
     * Peut être vide si l'erreur n'est pas associé a une ligne specifiques.
     *
     * @param lineNumber line number
     */
    public void addLineNumber(String lineNumber) {
        lineNumbers.add(lineNumber);
    }

    /**
     * Numero des lignes dans le fichier CSV.
     *
     * @param lineNumbers line numbers to add
     */
    public void addAllLineNumber(Collection<String> lineNumbers) {
        this.lineNumbers.addAll(lineNumbers);
    }

    @Override
    public String toString() {
        return "ValidationError [level=" + level + ", message=" + message
               + ", lineNumber=" + lineNumbers + "]";
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.control;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;

/**
 * Global validation group node.
 *
 * @author chatellier
 */
public class ControlErrorGroup implements Comparable<ControlErrorGroup> {

    protected final Category category;

    protected final ValidationLevel validationLevel;

    protected final String message;

    /**
     * @param category
     * @param validationLevel
     * @param message
     */
    public ControlErrorGroup(Category category, ValidationLevel validationLevel, String message) {
        this.category = category;
        this.validationLevel = validationLevel;
        this.message = message;
    }

    public Category getCategory() {
        return category;
    }

    public ValidationLevel getValidationLevel() {
        return validationLevel;
    }

    public String getMessage() {
        return message;
    }

    /*
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(ControlErrorGroup o) {
        int compare = o.getValidationLevel().compareTo(validationLevel);
        if (compare == 0) {
            compare = o.getMessage().compareTo(message);
        }
        return compare;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((category == null) ? 0 : category.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((validationLevel == null) ? 0 : validationLevel.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ControlErrorGroup other = (ControlErrorGroup) obj;
        if (message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!message.equals(other.message)) {
            return false;
        }
        if (validationLevel != other.validationLevel) {
            return false;
        }
        if (category != other.category) {
            return false;
        }
        return true;
    }
}

<#--
 #%L
 Coser :: Business
 %%
 Copyright (C) 2010 - 2012 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
 <body>
  <h2>Thank you</h2>
  <p>
Thank you for downloading data from this web site. If you find any problems in
the data set or would like to make any comments to improve the service, please
send an email to the webmaster at harmonie@ifremer.fr.
</p>

<#if surveyName?length &gt; 0>
<h2>Important</h2>
<p>
The data are derived from the ${surveyName} series.
</p>
<p>
You are cordially invited to consult the relevant information for this survey.
The web site contains information specifying data usage, limitations and known problems.
</p>
<p>
You are required to cite the data source in all products and publications using
this data.  By downloading the data from http://www.ifremer.fr/SIH-indices-campagnes,
you are accepting the terms and conditions described in this document and in all
documents cited therein.
</p>
</#if>

<h2>Contact</h2>
<p>
Please contact the webmaster (harmonie@ifremer.fr) for all questions
concerning the data downloaded from http://www.ifremer.fr/SIH-indices-campagnes.
</p>

<h2>Specific information on the downloaded data</h2>
<p>
The data derived from scientific fisheries surveys are made freely available.
Despite the greatest care taken in preparing these data, various shortcomings
persist. For example</p>

<ul>
<li>Despite the fact that all data are presented in the same format, differences
in sampling protocol preclude the simple combination of data sets from different
surveys. As  catchability varies between species, each sampling device catches a
particular subsample of the community. </li>

<li>A common feature of all surveys is the change in taxonomic skills of the
personnel onboard. This can result in the appearance, disappearance or confusion
of similar species, and consequently non representative time trends.</li>

<li>Within each survey series, changes in survey design, gear used, survey period
or the area covered can influence catches. To avoid any risks of bias the data
might need to be filtered before use.</li>
</ul>

<p>
Users are highly recommended to treat the data with care and interpret the results
with caution. Any user wondering about the validity of the data for a given purpose
is invited to contact the webmaster (harmonie@ifremer.fr).
</p>

<h2 style="page-break-before:always">How to cite the data</h2>

<p>Ifremer ${updateDate?string("yyyy")}. Données des campagnes de surveillance halieutique de l’Ifremer.
http://www.ifremer.fr/SIH-indices-campagnes/ (${updateDate?string("dd MMMM")}).</p>

<p><b>Data from the North Sea (IBTS data series).</b> The data were homogenized
and filtered by Ifremer to allow calculation coherent of indicator time series
based on all data collected by the international partners contributing to the
IBTS survey data stored in the ICES Datras data base (http://datras.ices.dk).<br />
<i>Citation de la source des données au CIEM :</i>
ICES. International Bottom Trawl Survey (1965-2011). available at 
http://datras.ices.dk. Downloaded the 2011-06-27.</p>

<h2>Conditions for using the data</h2>

<p>Ifremer makes the fisheries survey data it collects freely available, under
the condition that 
<ul>
<li>their usage and interpretation are the sole responsibility of the users, and
Ifremer as the data supplier is not involved in any results, conclusions or
recommendations derived from the data.</li>
<li>the data source is duly cited - users must respect all restrictions regarding
use and reproduction of the data including the interdiction of the commercial
use of the data.</li>
<li>users inform Ifremer of any errors found in the data.</li>
</ul>
</p>

<p><b>For North Sea data (IBTS data series).</b>
All users must respect the conditions of use specified by ICES (http://datras.ices.dk).</p>
</body>
</html>

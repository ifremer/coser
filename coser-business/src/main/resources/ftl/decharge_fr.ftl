<#--
 #%L
 Coser :: Business
 %%
 Copyright (C) 2010 - 2012 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
 <body>
  <h2>Réaction</h2>
  <p>
Merci d'avoir téléchargé des données de ce site. Si vous avez identifié des
problèmes dans ces données, ou souhaitez fournir des remarques pour aider à
améliorer le service, vous êtes invités à adresser un courriel à
l'administrateur du site (harmonie@ifremer.fr).
</p>

<#if surveyName?length &gt; 0>
<h2>Important</h2>
<p>
Les données téléchargées sont issues de la série ${surveyName}.
</p>
<p>
Vous êtes invités à consulter les informations relatives à cette série de
campagnes. Ce site comprend des informations spécifiques concernant l'usage de
ces données, les limitations et les problèmes connus.
</p>
<p>
Vous vous engagez à citer la source de ces données dans tout produit et
publication utilisant ces données. En téléchargeant des données à partir du
site http://www.ifremer.fr/SIH-indices-campagnes/, vous acceptez les termes et
conditions d'usage décrites dans ce document et dans tout document cité dans ce
texte.
</p>
</#if>

<h2>Contact</h2>
<p>
Pour toute question concernant les données téléchargées du site
http://www.ifremer.fr/SIH-indices-campagnes/, vous pouvez adresser un courriel à
l'administrateur du site (harmonie@ifremer.fr).
</p>

<h2>Informations particulières sur les données téléchargées</h2>
<p>
Les données des campagnes halieutiques de ce site sont mises à libre disposition
pour téléchargement. En dépit des soins mis à la préparation de ces données, des
défauts inhérents à l'agrégation des informations peuvent persister.
Par exemple :</p>

<ul>
<li>En dépit du fait que toutes les données de toutes les séries de campagnes
  soient présentées selon le même format, des différences
  dans les stratégies d'observation empêchent la combinaison de données de
  différentes campagnes dans une même analyse. Par exemple, la capturabilité
  d'une même espèce varie selon le type d'engin d'échantillonnage utilisé.
  Il en résulte que chaque engin capture un sous-ensemble particulier des
  biocénoses échantillonnées.</li>

<li>Une propriété commune aux séries d'observations à la mer est l'évolution dans
  le temps de la compétence des équipes embarquées pour la détermination des
  espèces. Il peut en résulter des apparitions, des disparitions ou des
  assignations sous un même nom de taxons proches dans les jeux de données, non
  représentatifs de l'évolution des populations concernées dans l'écosystème.</li>

<li>Pour les campagnes d'une même série, des changements dans les procédures
d'échantillonnage, dans les caractéristiques des engins, dans la période de
réalisation de la campagne et la zone couverte peuvent influencer les captures.
Pour prévenir les risques de biais dans les analyses en raison de ces facteurs,
les jeux de données doivent être préalablement filtrés adéquatement.</li>
</ul>

<p>
Il est vivement recommandé aux utilisateurs de données de les traiter avec
précaution. Si des utilisateurs s'interrogent sur la validité de données, ils
sont invités à contacter l'administrateur de la base de données
(harmonie@ifremer.fr).
</p>

<h2 style="page-break-before:always">Citation de l'origine des données</h2>

<p>Ifremer ${updateDate?string("yyyy")}. Données des campagnes de surveillance halieutique de l’Ifremer.
http://www.ifremer.fr/SIH-indices-campagnes/ (${updateDate?string("dd MMMM")}).</p>

<p><b>Pour les données de mer du Nord (série IBTS).</b> Données homogénéisés
et filtrées par l'Ifremer pour répondre aux besoins de calcul d'indices
temporels, à partir des données de base des pays partenaires des campagnes
IBTS déposées dans la base Datras du CIEM (http://datras.ices.dk).<br />
<i>Citation de la source des données au CIEM :</i>
ICES. International Bottom Trawl Survey (1965-2011). disponible en ligne à
http://datras.ices.dk. Téléchargement le 2011-06-27.</p>

<h2>Conditions d'usage des données</h2>

<p>L'Ifremer met les données des campagnes de surveillance halieutique conduites
par l'institut en libre accès, mais :
<ul>
<li>l'interprétation et l'usage approprié de ces données sont de la seule
  responsabilité de leurs utilisateurs,</li>
<li>les utilisateurs des données ne doivent d'aucune façon impliquer l'Ifremer en
  tant que fournisseur de données dans leurs résultats, conclusions et/ou
  recommandations.</li>
<li>la source des données doit être dûment citée,
- les utilisateurs de données doivent respecter toutes les restrictions
  d'usage et de reproduction des données, incluant les restrictions pour un
  usage commercial,</li>
<li>les utilisateurs des données doivent informer l'Ifremer de tout problème
  soupçonné dans les données.</li>
</ul>
</p>

<p><b>Pour les données de mer du Nord (données internationales des campagnes IBTS).</b>
Les utilisateurs doivent se conformer aux modalités d'usage définies par le
CIEM (http://datras.ices.dk).</p>
</body>
</html>

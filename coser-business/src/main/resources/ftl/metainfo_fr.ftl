<#--
 #%L
 Coser :: Business
 %%
 Copyright (C) 2010 - 2011 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
 <body>
  
  <h2>Espèces incluses dans le calcul des indicateurs de communautés</h2>

  <p>Chaque indicateur de communauté est calculé sur une liste d'espèces
  sélectionnées selon différents critères de cohérence et de pertinence.
  Par exemple, les espèces n'ayant pas fait l'objet de relevés tout au long
  de la série sur les paramètres requis sont éliminées. Les espèces incluses
  dans le calcul des indicateurs de communautés de la présente transmission
  sont présentés ci-dessous.</p>
  
  <p>Les espèces sont rattachées selon les types suivants (lettre code devant
  le nom d'espèce ci-dessous) : Cnidaria (n), Annelida (a), Mollusca hors
  Cephalopoda (o), Cephalopoda (m), Crustacea (c), Echinoderma (e), Pisces &amp;
  Agnatha (p). Dans la table des données jointe, le premier caractère du champ
  "Liste" indique le type d'espèces sur lequel est calculé l'indicateur, la
  lettre T indiquant que l'indicateur est calculé pour tous les types d'espèces
  de la liste.</p>
  
  <#list indicatorsMap.keySet() as indicatorId>
  
    <h3>
    <#switch indicatorId>
      <#case "1">
        Liste 1 (liste complète d'espèces)
        <#break>
      <#case "2">
        Liste 2  (espèces filtrées, e.g. sur l'occurrence et la densité, pour
        éliminer les espèces rares)
        <#break>
      <#case "3">
        Liste 3 (uniquement espèces mesurées)
        <#break>
      <#default>
        ## Liste inconnue ? ##
    </#switch>
    </h3>

    <h4>
    <#list indicatorsMap.get(indicatorId) as indicator>
      ${indicator}<#if indicator_has_next>,</#if>
    </#list>
    </h4>
    
    <ul>
    <#list speciesMap.get(indicatorId) as species>
      <li>${species}</li>
    </#list>
    </ul>
  </#list>

</body>
</html>

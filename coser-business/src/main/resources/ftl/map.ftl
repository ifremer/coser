<#--
 #%L
 Coser :: Business
 %%
 Copyright (C) 2010 - 2011 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
 <body><p>L'objectif de cet atlas est de donner un aperçu de la distribution
 spatiale des espèces de poissons et de certains invertébrés marin à partir
 des observations des campagnes de pêche scientifiques.</p>
 <p>
Pour chaque zone un quadrillage systématique a été défini, puis la densité
moyenne par km² dans chaque cellule a été calculée en utilisant les observations
de toute la période. Pour la représentation cartographique, les cellules avec
des densités moyenne correspondant aux quartiles de densité ont reçu la même
couleur : bleu : espèce jamais observée, jaune clair : densité moyenne entre
[0 et 25 %[; jaune foncé : [25-50 %[, orange : [50-75 %[ et rouge : [75-100 %].
Donc, les zones où se trouvent les densités les plus élevées en moyenne sont
représentées en rouge.</p>

<img src="file://${mapFile}" />
</body>
</html>

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2011 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.storage;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.coser.services.CoserTestAbstract;

/**
 * Test for MemoryDataStorage class.
 * 
 * @author chatellier

 * 


 */
public class MemoryDataStorageTest extends CoserTestAbstract {

    /**
     * Test que les format speciaux ne pose pas de problème.
     */
    @Test
    public void testCSVParsing() {
        MemoryDataStorage storage = new MemoryDataStorage();
        storage.add(new String[]{"11", "te\"st", "test, test"});
        
        String[] data = storage.get(0);
        Assert.assertEquals(3, data.length);
        Assert.assertEquals("11", data[0]);
        Assert.assertEquals("te\"st", data[1]);
        Assert.assertEquals("test, test", data[2]);
    }
}

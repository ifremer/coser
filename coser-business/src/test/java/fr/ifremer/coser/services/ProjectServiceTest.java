/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.command.DeleteLineCommand;
import fr.ifremer.coser.command.MergeSpeciesCommand;
import fr.ifremer.coser.command.ModifyFieldCommand;
import fr.ifremer.coser.util.Coordinate;

/**
 * Project service tests.
 * 
 * @author chatellier

 * 


 */
public class ProjectServiceTest extends CoserTestAbstract {

    protected ProjectService projectService;

    protected CommandService commandService;

    @Before
    public void initService() {
        projectService = new ProjectService(config);
        commandService = new CommandService(config);
    }

    /**
     * Test que la creation de deux projets de même nom echoue.
     * 
     * @throws CoserBusinessException 
     */
    @Test(expected=CoserBusinessException.class)
    public void testDuplicatedProject() throws CoserBusinessException {
        Project p = new Project();
        p.setName("project2"); // il existe deja dans src/test/resources
        projectService.createProject(p, new HashMap<CoserConstants.Category, File>(), new ArrayList<File>(), null);
    }

    /**
     * Test que le project est correctement créer (à partir des données
     * dans /src/test/resources.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testCreateProject() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);

        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(), project.getName(),
                "original", "testcatch.csv").exists());
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(), project.getName(),
                "original", "testhaul.csv").exists());
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "original", "teststrata.csv").exists());
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "original", "testlength.csv").exists());
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "reftaxSpecies.csv").exists());
    }

    /**
     * Test que les projets des resources de test sont bien rechargé.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testTestResourcesProjectReloading() throws CoserBusinessException {
        Project projectValidated = openTestProject(projectService, "projectctrvalidated");
        // ce projet a un control validé
        Assert.assertTrue(projectValidated.getControl().isValidated());

        // la selection
        Selection selection = projectValidated.getSelections().get("testselection1");
        Assert.assertNotNull(selection);
        projectService.loadSelectionData(projectValidated, selection);
        Assert.assertEquals(1, selection.getHistoryCommands().size()); // un merge
        Assert.assertEquals("Test selectedSpeciesOccDensComment", selection.getSelectedSpeciesOccDensComment());
        Assert.assertEquals(3.5, selection.getDensityFilter(), 0);

        // un resultat
        RSufiResult rSufiResult = selection.getRsufiResults().get(0);
        Assert.assertEquals("testresult1", rSufiResult.getName());
        Assert.assertEquals("myzone", rSufiResult.getZone());
        Assert.assertTrue(rSufiResult.isPubliableResult());

        // non validé
        Project projectNotValidated = openTestProject(projectService, "project2");
        Assert.assertFalse(projectNotValidated.getControl().isValidated());
    }

    /**
     * Test que le project est correctement créer (à partir des données
     * dans /src/test/resources.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testSaveProject() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        projectService.saveProjectControl(project);

        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "control", "testcatch_co.csv").exists());
    }

    /**
     * Test que la validation du controle fonctionne et produit bien le pdf
     * de rapport.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testValidControl() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        projectService.validControl(project, Collections.EMPTY_LIST);
        
        Assert.assertTrue(project.getControl().isValidated());
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "control", "control.pdf").exists());
    }

    /**
     * Test que les selections sont bien creer.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testValidSelection() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("titi");

        projectService.createProjectSelection(project, selection);
        projectService.validSelection(project, selection);

        Assert.assertTrue(selection.isValidated());
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "selections",
                "titi", "selection.pdf").exists());
    }

    /**
     * Test que les selections sont bien creer.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testCreateSelection() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("titi");
        projectService.createProjectSelection(project, selection);
        
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(),
                project.getName(), "selections",
                "titi", "testcatch_se.csv").exists());
    }
    
    /**
     * Test que les selections sont bien rechargées.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testSelectionReloading() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Assert.assertEquals(25, project.getControl().getCatch().size());
        
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("titi");
        projectService.createProjectSelection(project, selection);
        
        project.clearData();
        projectService.loadSelectionData(project, selection);
        Assert.assertEquals(25, project.getSelections().get("titi").getCatch().size());
    }
    
    /**
     * Test que deux selections de même nom ne peuvent
     * pas être crées.
     * 
     * @throws CoserBusinessException 
     */
    @Test(expected=CoserBusinessException.class)
    public void testCreateSelectionError() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("titi");
        projectService.createProjectSelection(project, selection);
        projectService.createProjectSelection(project, selection);
    }

    /**
     * Test qu'une espece exite bien dans le projet de test.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testSpeciesExistence() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        Assert.assertFalse(projectService.isSpeciesNameExist(project, "Coser_invalid"));
        Assert.assertTrue(projectService.isSpeciesNameExist(project, "COSER_SPECIES1"));
        Assert.assertTrue(projectService.isSpeciesNameExist(project, "COSER_SPECIES2"));
    }

    /**
     * Test l'obtention de la liste des annee d'un projet.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testProjectYear() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);

        List<String> years = projectService.getProjectYears(selection);
        Assert.assertEquals(2, years.size());
        Assert.assertEquals("2010", years.get(0));
        Assert.assertEquals("2011", years.get(1));
    }

    /**
     * Test l'obtention de la liste des strates d'un projet.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testProjectStrata() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);

        List<String> years = new ArrayList<String>();
        years.add("2010");
        years.add("2011");
        List<String> strata = projectService.filterDataYearsAndGetStrata(project, selection, years);
        Assert.assertEquals(6, strata.size());

        List<String> years2 = new ArrayList<String>();
        years2.add("2009");
        strata = projectService.filterDataYearsAndGetStrata(project, selection, years2);
        Assert.assertEquals(0, strata.size());

        List<String> years3 = new ArrayList<String>();
        years3.add("2011");
        // doit recharger les données pour être correct
        strata = projectService.filterDataYearsAndGetStrata(project, selection, years3);
        Assert.assertEquals(3, strata.size());
    }
    
    /**
     * Test l'obtention de la liste des especes d'un projet.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testProjectSpecies() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);

        List<String> years20102011 = new ArrayList<String>();
        years20102011.add("2010");
        years20102011.add("2011");
        List<String> years2011 = new ArrayList<String>();
        years2011.add("2011");
        List<String> years2009 = new ArrayList<String>();
        years2009.add("2009");

        // get all species type
        Set<String> allPpeciesType = projectService.getProjectSpeciesTypes(project).keySet();

        // all data
        List<String> allStrata = projectService.filterDataYearsAndGetStrata(project, selection, years20102011);
        projectService.filterDataStrata(project, selection, allStrata);
        List<String> species = projectService.getProjectSpecies(selection, project, allPpeciesType);
        Assert.assertEquals(4, species.size());

        // data in 2011
        projectService.loadControlDataToSelection(project, selection);
        allStrata = projectService.filterDataYearsAndGetStrata(project, selection, years2011);
        projectService.filterDataStrata(project, selection, allStrata);
        species = projectService.getProjectSpecies(selection, project, allPpeciesType);
        Assert.assertEquals(4, species.size());

        // all strata but no data for years
        projectService.loadControlDataToSelection(project, selection);
        allStrata = projectService.filterDataYearsAndGetStrata(project, selection, years2009);
        projectService.filterDataStrata(project, selection, allStrata);
        species = projectService.getProjectSpecies(selection, project, allPpeciesType);
        Assert.assertEquals(0, species.size());
        
        // test with no strata
        projectService.loadControlDataToSelection(project, selection);
        allStrata = projectService.filterDataYearsAndGetStrata(project, selection, years20102011);
        projectService.filterDataStrata(project, selection, new ArrayList<String>());
        species = projectService.getProjectSpecies(selection, project, allPpeciesType);
        Assert.assertEquals(0, species.size());
        
        // test with only one stratum
        projectService.loadControlDataToSelection(project, selection);
        allStrata = projectService.filterDataYearsAndGetStrata(project, selection, years20102011);
        projectService.filterDataStrata(project, selection, Collections.singletonList("STR6"));
        species = projectService.getProjectSpecies(selection, project, allPpeciesType);
        Assert.assertEquals(4, species.size());
    }

    /**
     * Test le merge de deux especes dans les données.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testMergeSpecies() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("test");

        Assert.assertTrue(projectService.isSpeciesNameExist(project, "COSER_SPECIES_1_3"));
        Assert.assertTrue(projectService.isSpeciesNameExist(project, "COSER_SPECIES_2_4"));
        Assert.assertEquals(25, selection.getCatch().size());
        Assert.assertEquals(30, selection.getLength().size());

        // first merge
        project = projectService.mergeSpecies(project, selection, "COSER_SPECIES_1_3", null, "COSER_SPECIES1", "COSER_SPECIES3");
        Assert.assertEquals(19, selection.getCatch().size());
        Assert.assertEquals(28, selection.getLength().size());
        Assert.assertTrue(projectService.getProjectSpecies(selection, null, null).contains("COSER_SPECIES_1_3"));
        Assert.assertFalse(projectService.getProjectSpecies(selection, null, null).contains("COSER_SPECIES1"));

        // second merge
        project = projectService.mergeSpecies(project, selection, "COSER_SPECIES_2_4", null, "COSER_SPECIES2", "COSER_SPECIES4");
        Assert.assertEquals(13, selection.getCatch().size());
        Assert.assertEquals(26, selection.getLength().size());
        Assert.assertTrue(projectService.getProjectSpecies(selection, null, null).contains("COSER_SPECIES_2_4"));
        Assert.assertFalse(projectService.getProjectSpecies(selection, null, null).contains("COSER_SPECIES4"));

        projectService.createProjectSelection(project, selection);
    }

    /**
     * Test que la sauvegarde des commandes d'un control et le
     * rechargement des commandes fonctionne bien.
     * 
     * @throws CoserBusinessException 
     * @throws IOException 
     */
    @Test
    public void testCommandStoreAndReloading() throws CoserBusinessException, IOException {
        Project project = createTestProject(projectService, false);

        DeleteLineCommand command = new DeleteLineCommand();
        command.setLineNumber("2");
        command.setCategory(Category.LENGTH);

        commandService.doAction(command, project, project.getControl());
        projectService.saveProjectControl(project);

        // clear, reload and undo command after reloading
        project.clearData();
        projectService.loadControlData(project);

        commandService.undoAction(project, project.getControl());
        Assert.assertEquals(30, project.getControl().getLength().size());
    }

    /**
     * Test que les commandes sont bien sauvées et restaurées dans l'ordre
     * d'ajout.
     * @throws CoserBusinessException 
     */
    @Test
    public void testPropertiesCommandOrder() throws CoserBusinessException {
        Properties props = new Properties();
        props.setProperty("test wrong data", "wrong data");
        
        List<Command> commands = new ArrayList<Command>();
        
        // make real commands
        MergeSpeciesCommand msc1 = new MergeSpeciesCommand();
        //msc1.setCategory(Category.CATCH);
        msc1.setSpeciesNames(new String[]{"thuna"});
        MergeSpeciesCommand msc2 = new MergeSpeciesCommand();
        //msc2.setCategory(Category.CATCH);
        msc2.setSpeciesNames(new String[]{"bar"});
        
        DeleteLineCommand dlc1 = new DeleteLineCommand();
        dlc1.setCategory(Category.CATCH);
        dlc1.setLineNumber("10");
        DeleteLineCommand dlc2 = new DeleteLineCommand();
        dlc2.setCategory(Category.STRATA);
        dlc2.setLineNumber("99");

        ModifyFieldCommand mfc1 = new ModifyFieldCommand();
        mfc1.setCategory(Category.CATCH);
        ModifyFieldCommand mfc2 = new ModifyFieldCommand();
        mfc2.setCategory(Category.STRATA);

        commands.add(msc1);
        commands.add(dlc1);
        commands.add(mfc1);
        commands.add(dlc2);
        commands.add(mfc2);
        commands.add(msc2);

        projectService.addHistoryCommandsToProperties(props, commands, "control.commands");

        // try to read it
        commands = projectService.getHistoryCommandsFromProperties(props, "control.commands");

        Assert.assertTrue(commands.get(0) instanceof MergeSpeciesCommand);
        Assert.assertTrue(commands.get(1) instanceof DeleteLineCommand);
        Assert.assertTrue(commands.get(2) instanceof ModifyFieldCommand);
        Assert.assertTrue(commands.get(3) instanceof DeleteLineCommand);
        Assert.assertTrue(commands.get(4) instanceof ModifyFieldCommand);
        Assert.assertTrue(commands.get(5) instanceof MergeSpeciesCommand);
    }

    /**
     * Test de la method sampling effort.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testSamplingEffort() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("test");
        MatrixND matrix = projectService.getSamplingEffort(project, selection);
        Assert.assertNotNull(matrix);
    }

    /**
     * Test de la méthode occurence.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void getOccurence() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("test");
        MatrixND matrix = projectService.getOccurrence(project, selection);
        Assert.assertNotNull(matrix);
    }
    
    /**
     * Test de la méthode densité.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void getDensity() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("test");
        MatrixND matrix = projectService.getDensity(project, selection);
        Assert.assertNotNull(matrix);
    }
    
    /**
     * Test de la méthode de structure en taille.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void getLengthStructure() throws CoserBusinessException {
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setName("test");
        MatrixND matrix = projectService.getLengthStructure(project, selection);
        Assert.assertNotNull(matrix);
    }

    /**
     * Test le calcul du demi pas pour une valeur.
     */
    @Test
    public void testHalfStepValue() {
        Assert.assertEquals(38.0, projectService.getHalfStepValue(38.0), 0.00001);
        Assert.assertEquals(38.0, projectService.getHalfStepValue(38.1), 0.00001);
        Assert.assertEquals(38.0, projectService.getHalfStepValue(38.245), 0.00001);
        Assert.assertEquals(38.5, projectService.getHalfStepValue(38.5), 0.00001);
        Assert.assertEquals(38.5, projectService.getHalfStepValue(38.6), 0.00001);
        Assert.assertEquals(38.5, projectService.getHalfStepValue(38.999), 0.00001);
        Assert.assertEquals(42.5, projectService.getHalfStepValue(42.745), 0.00001);
    }

    /**
     * Test la recuperation des coordonnées des strates.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetStrataHaulCoordinate() throws CoserBusinessException {
        Project projectValidated = openTestProject(projectService, "projectctrvalidated");
        Selection selection = projectValidated.getSelections().get("testselection1");
        projectService.loadSelectionData(projectValidated, selection);

        List<Coordinate> coordinates = projectService.getStrataHaulCoordinate(selection, Collections.EMPTY_LIST);
        Assert.assertTrue(coordinates.isEmpty());
        coordinates = projectService.getStrataHaulCoordinate(selection, Collections.singleton("STR1"));
        Assert.assertEquals(1, coordinates.size());
        coordinates = projectService.getStrataHaulCoordinate(selection, Collections.singleton("STR2"));
        Assert.assertEquals(1, coordinates.size());
        coordinates = projectService.getStrataHaulCoordinate(selection, Arrays.asList(new String[]{"STR1", "STR3", "STR5"}));
        Assert.assertEquals(3, coordinates.size());
    }

    /**
     * Test la recuperation de l'arbre des projets et selection.
     */
    @Test
    public void testSelectionByProject() {
        Map<String, List<String>> projectsAndSelection = projectService.getSelectionByProject();
        
        Assert.assertEquals(2, projectsAndSelection.size());
        Assert.assertEquals(1, projectsAndSelection.get("projectctrvalidated").size());
    }

    /**
     * Test que l'ouverture de selection fonctionne et ne charge pas de données.
     * Test que l'historique est chargé.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testOpenSelection() throws CoserBusinessException {
        Selection selection = projectService.openSelection("projectctrvalidated", "testselection1");

        Assert.assertEquals(1, selection.getHistoryCommands().size());
        Assert.assertNull(selection.getCatch());
        Assert.assertEquals(3.5, selection.getDensityFilter(), 0.0001);
    }

    /**
     * Test que la methode de sauvegarde des selections retire bien les
     * especes qui ne sont plus disponible.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testFillListsSelection() throws CoserBusinessException {
        Selection selection = projectService.openSelection("projectctrvalidated", "testselection1");

        selection.setSelectedSpecies(Arrays.asList(new String[]{"species1", "species2", "species4"}));
        projectService.fillListsSelection(selection,
                Arrays.asList(new String[]{"species1", "species2", "species3"}),
                Arrays.asList(new String[]{"species2", "species3", "species4"}),
                Arrays.asList(new String[]{"species3", "species4", "species5"}));

        Assert.assertEquals(2, selection.getSelectedSpeciesOccDens().size());
        Assert.assertEquals(2, selection.getSelectedSpeciesSizeAllYear().size());
        Assert.assertEquals(1, selection.getSelectedSpeciesMaturity().size());
    }

    /**
     * Test de creation et de suppression de resultat.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testDeleteResult() throws CoserBusinessException {
        Project project = openTestProject(projectService, "projectctrvalidated");
        Selection selection = project.getSelections().get("testselection1");
        
        RSufiResult rsufiResult = selection.getRsufiResults().get(0);
        Assert.assertTrue(getFile(config.getRSufiProjectsDirectory(), project.getName(),
                "selections", "testselection1", "results", rsufiResult.getName(),
                "result.properties").exists());
        
        projectService.deleteRSufiResult(project, selection, rsufiResult);
        Assert.assertFalse(getFile(config.getRSufiProjectsDirectory(), project.getName(),
                "selections", "testselection1", "results", rsufiResult.getName()).exists());
    }
}

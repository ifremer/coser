/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;

import org.apache.commons.io.FileUtils;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.command.DeleteLineCommand;
import fr.ifremer.coser.command.MergeSpeciesCommand;
import fr.ifremer.coser.command.ModifyFieldCommand;
import fr.ifremer.coser.control.DiffCatchLengthControlError;
import fr.ifremer.coser.control.ControlError;

/**
 * Publication service tests
 * 
 * @author chatellier

 * 


 */
public class PublicationServiceTest extends CoserTestAbstract {

    protected ProjectService projectService;
    protected PublicationService publicationService;
    protected CommandService commandService;

    @Before
    public void initService() {
        projectService = new ProjectService(config);
        publicationService = new PublicationService(config);
        commandService = new CommandService(config);
    }

    @Ignore
    public void testCatchChart() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        Map<String, JFreeChart> charts = publicationService.getCompareCatchLengthGraph(project, project.getControl(), null);
        JFreeChart chart = charts.get("COSER_SPECIES1");
        JDialog f = new JDialog();
        f.setModal(true);
        f.add(new ChartPanel(chart));
        f.pack();
        f.setVisible(true);
    }

    /**
     * Test la géneration des rapports html.
     * 
     * @throws CoserBusinessException
     * @throws IOException 
     */
    @Test
    public void testErrorExportHtml() throws CoserBusinessException, IOException {
        Project project = createTestProject(projectService, false);

        List<ControlError> validationErrors = new ArrayList<ControlError>();

        // error 1 :
        ControlError error1 = new ControlError();
        error1.setLevel(ValidationLevel.FATAL);
        error1.setMessage("Test fatal");
        error1.addLineNumber("15");
        error1.setCategory(Category.CATCH);
        validationErrors.add(error1);

        DiffCatchLengthControlError error2 = new DiffCatchLengthControlError();
        error2.setLevel(ValidationLevel.WARNING);
        error2.setMessage("Test warning and graph");
        error2.setSpecies("COSER_SPECIES1");
        error2.setTipMessage("Explication sur l'erreur");
        validationErrors.add(error2);

        ControlError error3 = new ControlError();
        error3.setLevel(ValidationLevel.ERROR);
        error3.setMessage("Test error");
        error3.addLineNumber("12");
        error3.addLineNumber("9999");
        error3.setCategory(Category.CATCH);
        validationErrors.add(error3);

        File htmlExport = publicationService.exportErrorsAsHTML(project, project.getControl(), validationErrors);
        
        // some asserts
        String fileContent = FileUtils.readFileToString(htmlExport);
        Assert.assertTrue(fileContent.indexOf("9999") > 0);
        Assert.assertTrue(fileContent.indexOf("<img src=") > 0); // one chart
        Assert.assertTrue(fileContent.indexOf("Test fatal") > 0);
        Assert.assertTrue(fileContent.indexOf("Test warning and graph") > 0);
        Assert.assertTrue(fileContent.indexOf("Test error") > 0);
        Assert.assertTrue(fileContent.indexOf(t("coser.business.control.error.allCategories")) > 0);

        // clean all
        htmlExport.delete();
    }

    /**
     * Test le rapport de control en html.
     * 
     * @throws CoserBusinessException
     * @throws IOException
     */
    @Test
    public void testControlLogExportHtml() throws CoserBusinessException, IOException {
        
        Project project = createTestProject(projectService, false);

        // delete line 2
        DeleteLineCommand command = new DeleteLineCommand();
        command.setLineNumber("2");
        command.setCategory(Category.LENGTH);
        commandService.doAction(command, project, project.getControl());
        
        ModifyFieldCommand command2 = new ModifyFieldCommand();
        command2.setLineNumber("4");
        command2.setCategory(Category.CATCH);
        command2.setFieldName("Number");
        command2.setCurrentValue("251.86");
        command2.setNewValue("392.98");

        commandService.doAction(command2, project, project.getControl());
        
        File htmlExport = publicationService.extractControlLogAsHTML(project, project.getControl());

        // some asserts
        String fileContent = FileUtils.readFileToString(htmlExport);
        Assert.assertTrue(fileContent.indexOf("line 2") > 0);
        Assert.assertTrue(fileContent.indexOf("line 4") > 0);
        Assert.assertTrue(fileContent.indexOf("from \"251.86\" to \"392.98\"") > 0);

        // clean all
        htmlExport.delete();
    }
    
    /**
     * Test le rapport de selection en html.
     * 
     * @throws CoserBusinessException
     * @throws IOException
     */
    @Test
    public void testSelectionLogExportHtml() throws CoserBusinessException, IOException {
        
        Project project = createTestProject(projectService, true);
        Selection selection = projectService.initProjectSelection(project);
        selection.setSelectedYears(Collections.singletonList("2010"));
        selection.setSelectedStrata(Collections.singletonList("EG34EB"));
        selection.setSelectedSpecies(Collections.singletonList("COSER_SPECIES1"));

        // merge species
        MergeSpeciesCommand command = new MergeSpeciesCommand();
        command.setComment("marged because i wanted to !");
        command.setNewSpecyName("TESTMERGE");
        command.setSpeciesNames(new String[]{"COSER_SPECIES2","COSER_SPECIES3"});
        commandService.doAction(command, project, selection);

        commandService.doAction(command, project, selection);
        
        File htmlExport = publicationService.extractSelectionLogAsHTML(project, selection);

        // some asserts
        String fileContent = FileUtils.readFileToString(htmlExport);
        Assert.assertTrue(fileContent.indexOf("COSER_SPECIES1") > 0);
        Assert.assertTrue(fileContent.indexOf("2010") > 0);
        Assert.assertTrue(fileContent.indexOf("EG34EB") > 0);
        Assert.assertTrue(fileContent.indexOf("TESTMERGE") > 0);
        Assert.assertTrue(fileContent.indexOf("COSER_SPECIES2") > 0);
        Assert.assertTrue(fileContent.indexOf("COSER_SPECIES3") > 0);

        // clean all
        //htmlExport.delete();
    }
}

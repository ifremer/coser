/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import fr.ifremer.coser.CoserApplicationContext;
import fr.ifremer.coser.DefaultCoserApplicationContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.nuiton.i18n.I18n;

import fr.ifremer.coser.CoserBusinessConfig;
import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserClassLoader;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;
import org.nuiton.util.FileUtil;

/**
 * Common coser test code.
 * 
 * @author chatellier

 * 


 */
public abstract class CoserTestAbstract {

    private static final Log log = LogFactory.getLog(CoserTestAbstract.class);
    
    protected static CoserBusinessConfig config;
    protected static CoserApplicationContext serviceContext;

    protected static File testDirectory;

    @BeforeClass
    public static void setUpClassLoader() {
        // declare new classloader because validation files are not
        // in classpath
        ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
        CoserClassLoader coserClassLoader = new CoserClassLoader(currentClassLoader);
        Thread.currentThread().setContextClassLoader(coserClassLoader);
    }

    @BeforeClass
    public static void initConfig() throws IOException {
        String tmpDir = System.getProperty("java.io.tmpdir");
        testDirectory = new File(tmpDir, "coser");
        FileUtil.createDirectoryIfNecessary(testDirectory);
        config = new CoserBusinessConfig();
        config.setDatabaseDirectory(testDirectory.getAbsolutePath());
        
        I18n.init(null, Locale.UK);

        config.setWebIndicatorsFile(CoserTestAbstract.class.getResource("/webindicators.csv").getFile());
        config.setWebZonesFile(CoserTestAbstract.class.getResource("/webzones.csv").getFile());
        serviceContext = new DefaultCoserApplicationContext(config);
    }

    @AfterClass
    public static void cleanDirectory() throws IOException {
        FileUtils.deleteDirectory(testDirectory);
    }

    /**
     * Recopie la base pour chaque test pour a avoir une vrai isolation
     * au niveau des tests.
     * 
     * @throws IOException
     */
    @Before
    public void initProjectDatabase() throws IOException {
        FileUtils.cleanDirectory(config.getDatabaseDirectory());
        copyDirectoryContent("src.test.resources.projects", config.getRSufiProjectsDirectory());
    }

    /**
     * Create an initialized project with csv file in test resources.
     * 
     * Project is filled with control data (not selections).
     * 
     * @param projectService project service
     * @param controlValidated if {@code true} mark loaded control as {@code validated}
     * @return project
     * @throws CoserBusinessException
     */
    protected Project createTestProject(ProjectService projectService, boolean controlValidated) throws CoserBusinessException {
        
        Project project = new Project();
        project.setName("Project-" + System.nanoTime());
        
        File testCatch = new File(CoserTestAbstract.class.getResource("/csv/correct/testcatch.csv").getFile());
        File testHaul = new File(CoserTestAbstract.class.getResource("/csv/correct/testhaul.csv").getFile());
        File testLength = new File(CoserTestAbstract.class.getResource("/csv/correct/testlength.csv").getFile());
        File testStrata = new File(CoserTestAbstract.class.getResource("/csv/correct/teststrata.csv").getFile());
        File testReftax = new File(CoserTestAbstract.class.getResource("/csv/correct/testreftax.csv").getFile());
        File testTypeEspeces = new File(CoserTestAbstract.class.getResource("/csv/correct/testtypeespeces.csv").getFile());

        Map<Category, File> categoriesAndFile = new HashMap<Category, File>();
        categoriesAndFile.put(Category.CATCH, testCatch);
        categoriesAndFile.put(Category.HAUL, testHaul);
        categoriesAndFile.put(Category.LENGTH, testLength);
        categoriesAndFile.put(Category.STRATA, testStrata);
        categoriesAndFile.put(Category.REFTAX_SPECIES, testReftax);
        categoriesAndFile.put(Category.TYPE_ESPECES, testTypeEspeces);

        project.setCatchFileName(testCatch.getName());
        project.setLengthFileName(testLength.getName());
        project.setHaulFileName(testHaul.getName());
        project.setStrataFileName(testStrata.getName());

        project = projectService.createProject(project, categoriesAndFile, new ArrayList<File>(), null);
        project = projectService.loadControlData(project);
        
        // sans la sauvegarde des données de control, on ne pas creer de
        // selection
        projectService.saveProjectControl(project);
        
        project.getControl().setValidated(controlValidated);

        if (log.isDebugEnabled()) {
            log.debug("Created project : " + project.getName());
        }
        return project;
    }

    /**
     * Ouvre un projet present dans le repertoire de test (copié depuis
     * les resources ou créé durant les tests).
     * 
     * @param projectService project service
     * @param projectName project name
     * @return opened project (control data loaded)
     * @throws CoserBusinessException
     */
    protected Project openTestProject(ProjectService projectService, String projectName) throws CoserBusinessException {

        Project project = projectService.openProject(projectName);
        project = projectService.loadControlData(project);
        return project;
    }

    /**
     * Util method used to build path for test.
     * 
     * @param root root dir
     * @param paths path sub component
     * @return file
     */
    protected File getFile(File root, String... paths) {
        File localFile = root;
        for (String path : paths) {
            localFile = new File(localFile, path);
        }
        return localFile;
    }

    protected void copyDirectoryContent(String path, File target) throws IOException {
        File basedir = new File(new File("").getAbsolutePath());
        File source = FileUtil.getFileFromFQN(basedir, path);
        FileFilter fileterWithNoSvn = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                boolean result = true;
                if (pathname.getAbsolutePath().contains(File.separator + ".svn")) {
                    result = false;
                }
                return result;
            }
        };
        FileUtils.copyDirectory(source, target, fileterWithNoSvn);
    }
}

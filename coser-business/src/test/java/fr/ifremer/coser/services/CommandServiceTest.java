/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.command.DeleteLineCommand;
import fr.ifremer.coser.command.ModifyFieldCommand;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Length;

/**
 * CommandService tests
 * 
 * @author chatellier

 * 


 */
public class CommandServiceTest extends CoserTestAbstract {

    protected CommandService commandService;
    protected ProjectService projectService;

    @Before
    public void initService() {
        projectService = new ProjectService(config);
        commandService = new CommandService(config);
    }

    /**
     * Test la suppression correcte d'une ligne.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testDeleteLine() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        Assert.assertEquals(30, project.getControl().getLength().size());

        DeleteLineCommand command = new DeleteLineCommand();
        command.setLineNumber("2");
        command.setCategory(Category.LENGTH);

        commandService.doAction(command, project, project.getControl());
        Assert.assertEquals(29, project.getControl().getLength().size());
    }
    
    /**
     * Test la suppression incorrecte d'une ligne.
     * 
     * @throws CoserBusinessException 
     */
    @Test(expected=CoserBusinessException.class)
    public void testDeleteLineWrong() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        DeleteLineCommand command = new DeleteLineCommand();
        command.setLineNumber("2");
        command.setCategory(Category.LENGTH);
        commandService.doAction(command, project, project.getControl());
        commandService.doAction(command, project, project.getControl());
    }
    
    /**
     * Test la suppression correcte d'une ligne puis le rétablissement
     * de la ligne supprimée.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testDeleteLineUndo() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        Assert.assertEquals("4", project.getControl().getLength().get(4)[Length.INDEX_LINE]);

        DeleteLineCommand command = new DeleteLineCommand();
        command.setLineNumber("4");
        command.setCategory(Category.LENGTH);

        commandService.doAction(command, project, project.getControl());
        Assert.assertEquals("5", project.getControl().getLength().get(4)[Length.INDEX_LINE]);
        commandService.undoAction(project, project.getControl());
        Assert.assertEquals("4", project.getControl().getLength().get(4)[Length.INDEX_LINE]);
    }
    
    /**
     * Test que la double annulation d'un retablissement de ligne
     * lance une exception.
     * 
     * @throws CoserBusinessException 
     */
    @Test(expected=CoserBusinessException.class)
    public void testDeleteLineUndoAlreadyExists() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        DeleteLineCommand command = new DeleteLineCommand();
        command.setLineNumber("3");
        command.setCategory(Category.LENGTH);

        commandService.doAction(command, project, project.getControl());
        commandService.undoAction(project, project.getControl());
        project.getControl().addHistoryCommand(command);
        commandService.undoAction(project, project.getControl());
    }

    /**
     * Test la commande de modification de valeur des champs.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testModifyFieldValue() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        Assert.assertEquals(30, project.getControl().getLength().size());

        ModifyFieldCommand command = new ModifyFieldCommand();
        command.setLineNumber("4");
        command.setCategory(Category.CATCH);
        command.setFieldName("Number");
        command.setCurrentValue("251.86");
        command.setNewValue("392.98");

        commandService.doAction(command, project, project.getControl());
        Assert.assertEquals("392.98", project.getControl().getCatch().get(4)[Catch.INDEX_NUMBER]);
        
        // also test undo
        commandService.undoAction(project, project.getControl());
        Assert.assertEquals("251.86", project.getControl().getCatch().get(4)[Catch.INDEX_NUMBER]);
    }

    /**
     * Test la commande de modification de valeur des champs alors que la valeur
     * attendu avant de remplacer est fausse.
     * 
     * @throws CoserBusinessException
     */
    @Test(expected=CoserBusinessException.class)
    public void testModifyFieldValueError() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        Assert.assertEquals(30, project.getControl().getLength().size());

        ModifyFieldCommand command = new ModifyFieldCommand();
        command.setLineNumber("4");
        command.setCategory(Category.CATCH);
        command.setFieldName("Number");
        command.setCurrentValue("252.86");
        command.setNewValue("392.98");

        commandService.doAction(command, project, project.getControl());
    }
}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import java.io.File;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;

/**
 * Project service tests.
 * 
 * @author chatellier

 * 


 */
public class CommonServiceTest extends CoserTestAbstract {
    
    protected CommonService service;
    
    @Before
    public void initService() {
        service = new CommonService(config);
    }

    @Test(expected=CoserBusinessException.class)
    public void testWrongSeparator() throws CoserBusinessException {
        Project p = new Project();
        
        URL url = CommonServiceTest.class.getResource("/csv/badformat/capturesbadseparator.csv");
        File file = new File(url.getFile());
        service.loadCSVFile(p, Category.CATCH, file, null, true);
    }
    
    @Test(expected=CoserBusinessException.class)
    public void testBadHeaderName() throws CoserBusinessException {
        Project p = new Project();
        
        URL url = CommonServiceTest.class.getResource("/csv/badformat/stratesheadername.csv");
        File file = new File(url.getFile());
        service.loadCSVFile(p, Category.STRATA, file, null, true);
    }
    
    @Test(expected=CoserBusinessException.class)
    public void testBadHeaderOrder() throws CoserBusinessException {
        Project p = new Project();
        
        URL url = CommonServiceTest.class.getResource("/csv/badformat/traitsheaderorder.csv");
        File file = new File(url.getFile());
        service.loadCSVFile(p, Category.HAUL, file, null, true);
    }
}

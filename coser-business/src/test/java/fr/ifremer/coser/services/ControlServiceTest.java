/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Haul;
import fr.ifremer.coser.data.Length;

/**
 * Test about control service (validation...).
 * 
 * @author chatellier
 *
 */
public class ControlServiceTest extends CoserTestAbstract {

    private static final Log log = LogFactory.getLog(ControlServiceTest.class);

    protected ControlService controlService = new ControlService(config);
    protected ProjectService projectService = new ProjectService(config);

    /**
     * Test les validations sur les champs vide.
     */
    @Test
    public void testCatchValidation() {
        
        Catch myCatch = new Catch();

        myCatch.setData(new String[]{"1", "","","","","",""});
        List<ControlError> errors = controlService.validate(myCatch, Category.CATCH);
        Assert.assertNotNull(errors);
        Assert.assertEquals(4, errors.size());

        myCatch.setData(new String[]{"1", "Toto","","","","999",""});
        errors = controlService.validate(myCatch, Category.CATCH);
        Assert.assertNotNull(errors);
        Assert.assertEquals(3, errors.size());
    }

    /**
     * Test la validation des valeurs NA pour les doubles.
     */
    @Test
    public void testDoubleValidation() {
        Catch myCatch = new Catch();
        myCatch.setData(new String[]{"1", "Test survey","1999","Testtrait","Test sp","NA","12"});
        List<ControlError> errors = controlService.validate(myCatch, Category.CATCH);
        log.warn(errors);
        Assert.assertTrue(errors.isEmpty());
    }
    
    /**
     * Test la validation des valeurs NA pour les doubles.
     */
    @Test
    public void testLengthStep() {
        Length myLength = new Length();
        // "Survey","Year","Haul","Species","Sex","Maturity","Length","Number","Weight","Age"
        myLength.setData(new String[]{"1", "Test survey","1999","Testtrait","Test sp","i","m","23.25", "3.00", "44.99", "12"});
        List<ControlError> errors = controlService.validate(myLength, Category.LENGTH);
        log.warn(errors);
        // TODO test a refaire (croisement de fichier)
        //Assert.assertEquals(1, errors.size());
    }

    /**
     * Test que certains double ont 3 ou 5 decimales de définie.
     */
    @Test
    public void test5DecimalValidation() {
        Haul haulBean = new Haul();

        // "Survey","Year","Haul","Month","Stratum","SweptSurface","Lat","Long","Depth"
        
        // erreur , pas assez de décimale
        haulBean.setData(new String[]{"1", "COSER_TEST","2010","TRAIT3","10","STR3","0.06","43.89","1.73","115.00"});
        List<ControlError> errors = controlService.validate(haulBean, Category.HAUL);
        Assert.assertEquals(3, errors.size());

        // pas d'erreur, assez de décimales
        haulBean.setData(new String[]{"1","COSER_TEST","2010","TRAIT3","10","STR3","0.06000","43.89000","1.73234","115.00"});
        errors = controlService.validate(haulBean, Category.HAUL);
        Assert.assertTrue(errors.isEmpty());

        // erreur, exposant incorrect
        haulBean.setData(new String[]{"1","COSER_TEST","2010","TRAIT3","10","STR3","6e-2","4.3e1","1.14564e3","115.00"});
        errors = controlService.validate(haulBean, Category.HAUL);
        Assert.assertEquals(3, errors.size());
        
        // pas d'erreur, exposant correct
        haulBean.setData(new String[]{"1","COSER_TEST","2010","TRAIT3","10","STR3","6e-4","4.3e-3","1.14564e0","115.00"});
        errors = controlService.validate(haulBean, Category.HAUL);
        Assert.assertTrue(errors.isEmpty());
    }

    /**
     * Test si le pas de longueur est valid pour les poissons.
     */
    @Test
    public void testValidFishLength() {
        Assert.assertTrue(controlService.isValidFishLength("4"));
        Assert.assertTrue(controlService.isValidFishLength("4.5"));
        Assert.assertTrue(controlService.isValidFishLength("0"));
        Assert.assertTrue(controlService.isValidFishLength("0.5"));
        Assert.assertTrue(controlService.isValidFishLength("1.50"));
        Assert.assertTrue(controlService.isValidFishLength("1.00"));
        Assert.assertTrue(controlService.isValidFishLength("1.500"));
        Assert.assertFalse(controlService.isValidFishLength(""));
        Assert.assertFalse(controlService.isValidFishLength("1.3"));
        Assert.assertFalse(controlService.isValidFishLength("1.001"));
    }

    /**
     * Test que la validation globales fonctionne.
     * Méthode valid data qui passe tout les contôles.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testValidData() throws CoserBusinessException {
        Project project = createTestProject(projectService, false);
        List<ControlError> errors = controlService.validateData(project, project.getControl(), null);

        // 18 : xxx must contain at least x decimals
        // 1  : Duplicated line for key : COSER_TEST|2010|TRAIT1|COSER_SPECIES2|i|NA|19.60|
        // 8  : Differences between length and catch for species XXX
        // 15 : Missing XXX tuple in catch
        // 4  : Missing strata xxx in haul file
        // 3  : Missing haul xxx in length file
        // Total : 49
        if (log.isInfoEnabled()) {
            for (ControlError error : errors) {
                log.info(error.getLevel() + " " + error.getDetailMessage());
            }
        }

        // il y a beaucoup d'erreurs car les jeux d'essai ne sont
        // pas forcements cohérents
        Assert.assertEquals(49, errors.size());
    }
}

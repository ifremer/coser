/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2011 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.services;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.storage.DataStorage;
import fr.ifremer.coser.util.DataType;

/**
 * Test du service de l'interface web.
 * 
 * @author chatellier

 * 


 */
public class WebServiceTest extends CoserTestAbstract {

    protected WebService webService;

    @Before
    public void initServices() {
        webService = new WebService(config);
    }

    protected void assertFileExists(String filepath) {
        File file = new File(filepath.replace('/', File.separatorChar));
        Assert.assertTrue(file.exists());
    }

    protected void assertFileNotExists(String filepath) {
        File file = new File(filepath.replace('/', File.separatorChar));
        Assert.assertFalse(file.exists());
    }

    /**
     * Simule la publication de nouveaux resultats (uplaod client lourd).
     * 
     * @param path result to upload
     * @throws CoserBusinessException 
     */
    protected void registerUploadedResult(String path) throws CoserBusinessException {
        URL firstUpload = WebServiceTest.class.getResource(path);
        File firstUploadFile = new File(firstUpload.getFile());
        webService.registerNewUploadedResults("admin", firstUploadFile);
    }

    /**
     * Test de lecture du fichier des indicateurs des resources de test.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testGetIndicators() throws CoserBusinessException {
        MultiKeyMap indicators = webService.getIndicatorsMap();
        Assert.assertEquals(200, indicators.size());
    }

    /**
     * Test de lecture du fichier de zones de resources de test.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testGetZones() throws CoserBusinessException {
        DataStorage zones = webService.getZonesMap();
        Assert.assertEquals(25, zones.size());
    }

    /**
     * Test de lecture du fichier de zones de resources de test.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testGetZonePictures() throws CoserBusinessException {
        Map<String, String> zonesAndPictures = webService.getZonePictures();
        Assert.assertEquals("C_GdG-MC.png", zonesAndPictures.get("gdgmc"));
    }
    
    /**
     * Test de lecture du fichier de zones de resources de test.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testGetZoneMetaInfo() throws CoserBusinessException {
        Map<String, String> zonesMetaInfos = webService.getZoneMetaInfo(Locale.FRENCH);
        Assert.assertEquals("Ensemble de la couverture Evhoe", zonesMetaInfos.get("gdgmc"));
        
        zonesMetaInfos = webService.getZoneMetaInfo(Locale.ENGLISH);
        Assert.assertEquals("Area covered by Evhoe survey", zonesMetaInfos.get("gdgmc"));
        
        zonesMetaInfos = webService.getZoneMetaInfo(null);
        Assert.assertEquals("Area covered by Evhoe survey", zonesMetaInfos.get("gdgmc"));
    }

    /**
     * Test que les upload successible merge bien les resultats en fonction
     * des idenfiants des zones auquels ils sont affectés et que les resulats
     * en conflit sont bien supprimé (avec si besoins leurs selections et projets).
     * @throws CoserBusinessException 
     */
    @Test
    public void testUploadDirectoryMerge() throws CoserBusinessException {
        registerUploadedResult("/web/upload1.zip");

        // second upload with merge
        registerUploadedResult("/web/upload2.zip");

        // some tests (from first)
        assertFileNotExists(config.getWebMapsProjectsDirectory() + "/projet1/selections/selection11/results/result111");
        assertFileExists(config.getWebIndicatorsProjectsDirectory() + "/projet1/selections/selection11/results/result112");
        assertFileNotExists(config.getWebIndicatorsProjectsDirectory() + "/projet1/selections/selection12");
        // some tests (from second)
        assertFileExists(config.getWebIndicatorsProjectsDirectory() + "/projet1/selections/selection11/results/result113");
        assertFileExists(config.getWebIndicatorsProjectsDirectory() + "/projet1/selections/selection11/results/result114");
        assertFileExists(config.getWebMapsProjectsDirectory() + "/projet3/selections/selection31/results/result311");
    }

    /**
     * Test du contenu du zip de téléchargement des sources avec erreur
     * can le projet ne le permet pas.
     * 
     * @throws CoserBusinessException 
     * @throws IOException 
     */
    @Test(expected=CoserBusinessException.class)
    public void testSourceZipError() throws CoserBusinessException, IOException {
        registerUploadedResult("/web/upload2.zip");
        
        webService.getSourceZip("ecorse", Locale.ENGLISH);
    }
    
    /**
     * Test du contenu du zip de téléchargement des sources.
     * 
     * Generation du zip, freemarker, et pdf.
     * 
     * @throws CoserBusinessException 
     * @throws IOException 
     */
    @Test
    public void testSourceZip() throws CoserBusinessException, IOException {
        registerUploadedResult("/web/upload2.zip");
        
        File zip = webService.getSourceZip("testzone1", Locale.ENGLISH);
        File tempDir = FileUtil.createTempDirectory("coser-source-", "-tmp");
        ZipUtil.uncompress(zip, tempDir);
        Assert.assertTrue(new File(tempDir, "RSUFI_DATA_projet1" + File.separator + "testcatch.csv").isFile());
        Assert.assertTrue(new File(tempDir, "RSUFI_DATA_projet1" + File.separator + "DataDisclaimer.pdf").isFile());
        FileUtils.deleteDirectory(tempDir);
        
        zip = webService.getSourceZip("testzone1", Locale.FRENCH);
        tempDir = FileUtil.createTempDirectory("coser-source-", "-tmp");
        ZipUtil.uncompress(zip, tempDir);
        Assert.assertTrue(new File(tempDir, "RSUFI_DATA_projet1" + File.separator + "DechargeDonnees.pdf").isFile());
        // test que le reftax est dans le zip
        Assert.assertTrue(new File(tempDir, "RSUFI_DATA_projet1" + File.separator + "reftaxSpecies.csv").isFile());
        FileUtils.deleteDirectory(tempDir);
    }

    /**
     * Test la récupération d'une image parmit les resultat de type "mapReference".
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetMapFile() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");
        File file = webService.getMapFile("ecorse", "SPECIES1");
        Assert.assertTrue(file.isFile());
    }

    /**
     * Test que la generation d'un graph fonctionne.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetChartCom() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");
        File file = webService.getChart("ecorse", null, "Lbcomm", null, Locale.FRENCH);
        Assert.assertTrue(file.isFile());
    }
    
    /**
     * Test que la generation d'un graph fonctionne.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetChartPop() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");
        File file = webService.getChartData("ecorse", "COSER_SPECIES2", "lnN", null, Locale.FRENCH);
        Assert.assertTrue(file.isFile());
    }

    /**
     * Test que la generation d'un sous fichier csv à partir du fichier
     * estcomind fonctionne.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetChartDataCom() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");
        File file = webService.getChartData("ecorse", null, "lnN", null, Locale.FRENCH);
        Assert.assertTrue(file.isFile());
    }
    
    /**
     * Test que la generation d'un sous fichier csv à partir du fichier
     * estcomind fonctionne.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetChartDataPop() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");
        File file = webService.getChartData("ecorse", "COSER_SPECIES2", "lnN", null, Locale.FRENCH);
        Assert.assertTrue(file.isFile());
    }

    /**
     * Test que la recuperation des noms de list pour un indicateur fonctionne.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testGetIndicatorLists() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");
        Map<String, String> lists = webService.getIndicatorLists("ecorse", "Delta", Locale.FRENCH);
        Assert.assertEquals(2, lists.size());
        Assert.assertEquals("Type2 Liste 1", lists.get("m1"));
        Assert.assertEquals("Type2 Liste 2", lists.get("m2"));
        
        lists = webService.getIndicatorLists("ecorse", "Delta", null);
        Assert.assertEquals(2, lists.size());
        Assert.assertEquals("Type2 List 1", lists.get("m1"));
        Assert.assertEquals("Type2 List 2", lists.get("m2"));
    }

    /**
     * Test des modifications de methodes suite à l'intoduction du
     * moteur de recherche pour extraire les données.
     * 
     * @throws CoserBusinessException 
     */
    @Test
    public void testGetAllZoneSpeciesAndIndicators() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");

        Map<String, String> zones = webService.getZoneForFacade(null, false, false);
        List<String> zoneIds = new ArrayList<String>(zones.keySet());
        Assert.assertEquals(Arrays.asList("ecorse"), zoneIds);

        Map<String, String> species = webService.getSpecies(zoneIds, false);
        List<String> speciesIds = new ArrayList<String>(species.keySet());
        Assert.assertEquals(Arrays.asList("COSER_SPECIES1", "COSER_SPECIES2"), speciesIds);

        Map<String, String> indicators = webService.getIndicators(zoneIds, DataType.COMMUNITY, Locale.FRENCH);
        List<String> indicatorIds = new ArrayList<String>(indicators.keySet());
        Assert.assertEquals(Arrays.asList("Delta", "Lbcomm", "biomSmall"), indicatorIds);

        indicators = webService.getIndicators(zoneIds, DataType.POPULATION, Locale.FRENCH);
        indicatorIds = new ArrayList<String>(indicators.keySet());
        Assert.assertEquals(Arrays.asList("Abundance", "Biomass", "Wbar", "lnN"), indicatorIds);
    }

    /**
     * Test une validation de formulaire web pour extraire parmis toutes les
     * données celle choisie sous forme de zip.
     * 
     * @throws CoserBusinessException
     */
    @Test
    public void testExtractDataAsZip() throws CoserBusinessException {
        registerUploadedResult("/web/upload2.zip");

        List<String> zones = Arrays.asList("ecorse");
        List<DataType> types = Arrays.asList(DataType.MAP, DataType.POPULATION, DataType.COMMUNITY, DataType.SOURCE);
        List<String> species = Arrays.asList("COSER_SPECIES1", "COSER_SPECIES2");
        List<String> comIndicators = Arrays.asList("Delta", "Lbcomm", "biomSmall");
        List<String> popIndicators = Arrays.asList("Abundance", "Biomass", "Wbar", "lnN");

        File file = webService.extractData(zones, types, species, comIndicators, popIndicators, Locale.FRENCH);
        Assert.assertTrue(file.length() > 0);
    }
}


package fr.ifremer.coser.result.repository.echobase;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.services.CoserTestAbstract;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Created on 3/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseResultRepositoryProviderTest extends CoserTestAbstract {

    @Override
    public void initProjectDatabase() throws IOException {
        copyDirectoryContent("src.test.resources.web.echobaseprojects", config.getWebEchobaseProjectsDirectory());
    }

    @Test
    public void loadRepositories() throws Exception {

        File basedir = config.getWebEchobaseProjectsDirectory();

        EchoBaseResultRepositoryProvider provider = new EchoBaseResultRepositoryProvider(basedir);

        Set<EchoBaseResultRepository> resultRepositories = provider.loadRepositories();
        Assert.assertNotNull(resultRepositories);
        Assert.assertEquals(2, resultRepositories.size());
    }
}

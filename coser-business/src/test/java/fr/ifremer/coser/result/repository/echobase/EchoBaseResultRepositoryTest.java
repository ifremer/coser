package fr.ifremer.coser.result.repository.echobase;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.result.CoserCommand;
import fr.ifremer.coser.result.CoserCommandFactory;
import fr.ifremer.coser.result.DefaultCoserRequestContext;
import fr.ifremer.coser.result.request.GetZonesForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetZonesForMapResultRequest;
import fr.ifremer.coser.result.request.GetZonesForPopulationIndicatorResultRequest;
import fr.ifremer.coser.services.CoserTestAbstract;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseResultRepositoryTest extends CoserTestAbstract {

    protected EchoBaseResultRepository repository1;

    protected EchoBaseResultRepository repository2;

    @Override
    public void initProjectDatabase() throws IOException {
        File projectsDirectory = config.getWebEchobaseProjectsDirectory();
        copyDirectoryContent("src.test.resources.web.echobaseprojects", projectsDirectory);

        EchoBaseResultRepositoryProvider repositoryProvider = new EchoBaseResultRepositoryProvider(projectsDirectory);

        Set<EchoBaseResultRepository> repositories = repositoryProvider.loadRepositories();

        for (EchoBaseResultRepository repository : repositories) {
            String projectName = repository.project.getName();
            if ("project1".equals(projectName)) {
                repository1 = repository;
            } else if ("project2".equals(projectName)) {
                repository2 = repository;
            }
        }
        Assume.assumeTrue("Could not find repository named *project1*", repository1 != null);
        Assume.assumeTrue("Could not find repository named *project2*", repository2 != null);
    }

    @Test
    public void getAvailableZones() throws Exception {

        CoserCommandFactory commandFactory = serviceContext.getCommandFactory();
        DefaultCoserRequestContext requestContext =
                new DefaultCoserRequestContext(serviceContext, Locale.FRANCE);

        CoserCommand command;

        // Map request

        GetZonesForMapResultRequest mapRequest = new GetZonesForMapResultRequest();
        mapRequest.setFacade("atlantique");

        Map<String, String> availableZones;


        command = commandFactory.newCommand(requestContext, repository1, mapRequest);

        Assert.assertTrue(command.accept(mapRequest));
        availableZones = (Map<String, String>) command.execute(mapRequest).getResult();
        Assert.assertNotNull(availableZones);
        Assert.assertEquals(1, availableZones.size());
        Assert.assertTrue(availableZones.containsKey("gdgciem8"));
        Assert.assertTrue(availableZones.containsValue("2000 > - Pelgas - La limite nord de la zone VIII du CIEM (48 °N) constitue aussi une limite de régions de la DCSMM"));

        // pas de zones pour la facade
        mapRequest.setFacade("mediteranee");
        Assert.assertFalse(command.accept(mapRequest));
//        availableZones = (Map<String, String>) command.execute(repository1, mapRequest).getResult();
//        Assert.assertNull(availableZones);

        // facade inconnue
        mapRequest.setFacade("mediteranee2");
        Assert.assertFalse(command.accept(mapRequest));
//        availableZones = (Map<String, String>) command.execute(repository1, mapRequest).getResult();
//        Assert.assertNull(availableZones);

        // Population request

        GetZonesForPopulationIndicatorResultRequest popRequest = new GetZonesForPopulationIndicatorResultRequest();
        command = commandFactory.newCommand(requestContext, repository1, popRequest);

        popRequest.setFacade("atlantique");

        Assert.assertTrue(command.accept(popRequest));
        availableZones = (Map<String, String>) command.execute(popRequest).getResult();
        Assert.assertNotNull(availableZones);
        Assert.assertEquals(1, availableZones.size());
        Assert.assertTrue(availableZones.containsKey("gdgciem8"));
        Assert.assertTrue(availableZones.containsValue("2000 > - Pelgas - La limite nord de la zone VIII du CIEM (48 °N) constitue aussi une limite de régions de la DCSMM"));

        // pas de zones pour la facade
        popRequest.setFacade("mediteranee");
        Assert.assertFalse(command.accept(popRequest));
//        availableZones = (Map<String, String>) command.execute(repository1, popRequest).getResult();
//        Assert.assertNull(availableZones);

        // facade inconnue
        popRequest.setFacade("mediteranee2");
        Assert.assertFalse(command.accept(popRequest));
//        availableZones = (Map<String, String>) command.execute(repository1, popRequest).getResult();
//        Assert.assertNull(availableZones);

        // Community request

        GetZonesForCommunityIndicatorResultRequest comRequest = new GetZonesForCommunityIndicatorResultRequest();

        command = commandFactory.newCommand(requestContext, repository1, comRequest);
        comRequest.setFacade("atlantique");

        Assert.assertTrue(command.accept(comRequest));
        availableZones = (Map<String, String>) command.execute(comRequest).getResult();
        Assert.assertNotNull(availableZones);
        Assert.assertEquals(1, availableZones.size());
        Assert.assertTrue(availableZones.containsKey("gdgciem8"));
        Assert.assertTrue(availableZones.containsValue("2000 > - Pelgas - La limite nord de la zone VIII du CIEM (48 °N) constitue aussi une limite de régions de la DCSMM"));

        // pas de zones pour la facade
        comRequest.setFacade("mediteranee");
        Assert.assertFalse(command.accept(comRequest));
//        availableZones = (Map<String, String>) command.execute(repository1, comRequest).getResult();
//        Assert.assertNull(availableZones);

        // facade inconnue
        comRequest.setFacade("mediteranee2");
        Assert.assertFalse(command.accept(comRequest));
//        availableZones = (Map<String, String>) command.execute(repository1, comRequest).getResult();
//        Assert.assertNull(availableZones);
    }

}

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * Coser utils tests.
 * 
 * @author chatellier

 * 


 */
public class CoserUtilsTest {

    /**
     * Test l'ajout de suffix dans les noms de fichier.
     */
    @Test
    public void testAddSuffixBeforeExtension() {
        Assert.assertEquals("captures_co.csv", CoserUtils.addSuffixBeforeExtension("captures.csv", CoserConstants.STORAGE_CONTROL_SUFFIX));
        Assert.assertEquals("cap.tu.res_se.csv", CoserUtils.addSuffixBeforeExtension("cap.tu.res.csv", CoserConstants.STORAGE_SELECTION_SUFFIX));
        Assert.assertEquals("captures_co", CoserUtils.addSuffixBeforeExtension("captures", CoserConstants.STORAGE_CONTROL_SUFFIX));
    }

    /**
     * Test le parsing xml.
     * @throws IOException 
     */
    @Test
    public void testParseDocument() throws IOException {
        Assert.assertNotNull(CoserUtils.parseDocument("<html><body>test</body></html>"));
    }

    /**
     * Test le parsing xml avec erreur.
     * @throws IOException 
     */
    @Test(expected=IOException.class)
    public void testParseDocumentError() throws IOException {
        Assert.assertNotNull(CoserUtils.parseDocument("<html>"));
    }
    
    /**
     * Test que la chaine est bien parsée (buggé dans coser 1.0.5).
     */
    @Test
    public void testBracketToList() {
        String data = "(38be0fde-0f55-4dc1-a089-8e589a2623f0);(MergeSpeciesCommand);(Regroupe FMCOTTI, TAURBUB et MYOXSCO en FMCOTTI : PB d'identification avant 2000);((newSpecyName=FMCOTTI);(speciesNames=(FMCOTTI);(MYOXSCO);(TAURBUB)))";
        List<String> dataAsList = CoserUtils.convertBracketToList(data);
        Assert.assertEquals(4, dataAsList.size());
        
        data = "(38be0fde-0f55-4dc1-a089-8e589a2623f0);(test \\\\);(test \\) echappement)";
        dataAsList = CoserUtils.convertBracketToList(data);
        
        Assert.assertEquals(3, dataAsList.size());
        Assert.assertEquals("test \\", dataAsList.get(1));
        Assert.assertEquals("test ) echappement", dataAsList.get(2));
    }

    /**
     * Convertit une liste en une string separé par des () et
     * verifie que les echapements sont pris en compte.
     */
    @Test
    public void testListToBracket() {
        List<String> myList = new ArrayList<String>();
        myList.add("string1");
        myList.add("string2 avec \\");
        myList.add("string3 avec )");
        myList.add("string4 avec (");
        myList.add("string5");
        String asString = CoserUtils.convertBracketString(myList);
        Assert.assertEquals("(string1);(string2 avec \\\\);(string3 avec \\));(string4 avec \\();(string5)", asString);

        myList = CoserUtils.convertBracketToList(asString);
        Assert.assertEquals(5, myList.size());
        Assert.assertEquals("[string1, string2 avec \\, string3 avec ), string4 avec (, string5]", myList.toString());
    }
}

package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.services.CoserTestAbstract;
import fr.ifremer.coser.storage.DataStorage;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 3/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ZoneMapTest extends CoserTestAbstract {

    @Override
    public void initProjectDatabase() {
        // no project to init
    }

    /**
     * Test de lecture du fichier de zones de resources de test.
     */
    @Test
    public void getZones() {
        ZoneMap zoneMap = new ZoneMap(config.getWebZonesFile());
        DataStorage zones = zoneMap.getStorage();
        Assert.assertNotNull(zones);
        Assert.assertEquals(25, zones.size());
    }
}

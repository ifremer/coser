package fr.ifremer.coser.bean;

/*
 * #%L
 * Coser :: Business
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created on 3/17/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class EchoBaseProjectTest {

    @Test
    public void newMapSpeciesFilenameFilter() {

        FilenameFilter filter = EchoBaseProject.newMapSpeciesFilenameFilter("survey");
        Assert.assertFalse(filter.accept(new File(""), "survey"));
        Assert.assertFalse(filter.accept(new File(""), "survey.png"));
        Assert.assertFalse(filter.accept(new File(""), "survey_SPECIES_BAD.png"));
        Assert.assertTrue(filter.accept(new File(""), "survey_SPECIES.png"));
    }
}

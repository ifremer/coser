/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ConfigOptionDef;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import java.awt.Font;

import static org.nuiton.i18n.I18n.n;

/**
 * Coser application configuration.
 *
 * @author chatellier
 */
public class CoserConfig extends CoserBusinessConfig {

    private static final Log log = LogFactory.getLog(CoserConfig.class);

    static {
        //FIXME This in jaxx
        n("numbereditor.cleanAll");
        n("numbereditor.cleanOne");
        n("numbereditor.dot");
        n("numbereditor.sign");
    }

    public CoserConfig() {
        // init configuration with default options
        loadDefaultOptions(CoserOption.values());
    }

    public String getLookAndFeel() {
        String className = getOption(CoserOption.LOOKANDFEEL.key);
        return className;
    }

    public void setLookAndFeel(String lookAndFeel) {
        setOption(CoserOption.LOOKANDFEEL.key, lookAndFeel);
    }

    public String getApplicationVersion() {
        String result = getOption(CoserOption.APPLICATION_VERSION.key);
        return result;
    }

    public void setSupportEmail(String supportEmail) {
        setOption(CoserOption.SUPPORT_EMAIL.key, supportEmail);
    }

    public String getSupportEmail() {
        String result = getOption(CoserOption.SUPPORT_EMAIL.key);
        return result;
    }

    public String getWebsiteURL() {
        String result = getOption(CoserOption.WEBSITE_URL.key);
        return result;
    }

    public int getSwingFontSize() {
        int result = getOptionAsInt(CoserOption.SWING_FONT_SIZE.key);
        return result;
    }

    public void setSwingFontSize(int swingFontSize) {
        int old = getSwingFontSize();
        if (old != swingFontSize) {
            setOption(CoserOption.SWING_FONT_SIZE.key, String.valueOf(swingFontSize));
            updateSwingFont(swingFontSize);
        }
    }

    public enum CoserOption implements ConfigOptionDef {

        CONFIG_FILE(CONFIG_FILE_NAME, n("coser.config.config.file.description"), "coser.properties", String.class, true, true),
        LOOKANDFEEL("coser.lookandfeel", n("coser.config.lookandfeel.description"), null, String.class, false, false),
        APPLICATION_VERSION("coser.application.version", n("coser.config.application.version.description"), null, String.class, false, false),
        SUPPORT_EMAIL("coser.support.email", n("coser.config.support.email.description"), "support@codelutin.com", String.class, false, false),
        WEBSITE_URL("coser.website", n("coser.config.website.description"), "https://coser.codelutin.com", String.class, false, false),
        SWING_FONT_SIZE("coser.swingfontsize", n("coser.config.swingfontsize.description"), "12", Integer.class, false, false);

        private final String key;

        private final String description;

        private final String defaultValue;

        private final Class<?> type;

        private final boolean isTransient;

        private final boolean isFinal;

        CoserOption(String key,
                    String description,
                    String defaultValue,
                    Class<?> type,
                    boolean isTransient,
                    boolean isFinal) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.isTransient = isTransient;
            this.isFinal = isFinal;
        }

        @Override
        public boolean isFinal() {
            return isFinal;
        }

        @Override
        public boolean isTransient() {
            return isTransient;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            // not used
        }

        @Override
        public void setTransient(boolean isTransient) {
            // not used
        }

        @Override
        public void setFinal(boolean isFinal) {
            // not used
        }
    }

    /**
     * Update swing fonts properties.
     */
    public void updateSwingFont() {
        updateSwingFont(getSwingFontSize());
    }

    /**
     * Update swing fonts properties.
     *
     * @param newIncrease increase gap to apply to font
     */
    protected void updateSwingFont(int newIncrease) {
        // update all font properties
        for (Object key : UIManager.getDefaults().keySet()) {
            // Application.useSystemFontSettings exists on windows
            if (key.toString().endsWith("Font") || key.toString().endsWith(".font")) {
                Font font = UIManager.getFont(key);
                if (log.isDebugEnabled()) {
                    log.debug("Update " + key + " to size " + newIncrease);
                }
                font = font.deriveFont((float) newIncrease);
                UIManager.put(key, new FontUIResource(font));
            }
        }
    }
}

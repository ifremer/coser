/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.coser;

import fr.ifremer.coser.ui.CoserFrame;
import jaxx.runtime.context.DefaultApplicationContext;

import javax.swing.SwingUtilities;

/**
 * Coser main class.
 *
 * @author chatellier
 */
public class Coser {

    /**
     * Coser main method.
     *
     * @param args main args
     */
    public static void main(String... args) {

        // launch UI
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                CoserUIApplicationContext applicationContext = CoserUIApplicationContext.init();

                // define unique context global values
                DefaultApplicationContext context = new DefaultApplicationContext();
                context.setContextValue(applicationContext);
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getConfig());
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getSession());
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getProjectService());
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getCommandService());
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getControlService());
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getPublicationService());
                //FIXME Remove this and use directly the application context
                context.setContextValue(applicationContext.getClientResultService());

                // init frame with session reloading
                CoserFrame frame = new CoserFrame(context);
                frame.setLocationRelativeTo(null);
                applicationContext.getSession().add(frame);

                frame.setVisible(true);
            }
        });
    }

}

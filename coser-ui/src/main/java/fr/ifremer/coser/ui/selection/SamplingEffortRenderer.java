/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection;

import org.nuiton.math.matrix.gui.MatrixTableModelND;
import org.nuiton.math.matrix.gui.MatrixTableModelND.MatrixCellRenderer;

import javax.swing.JTable;
import java.awt.Color;
import java.awt.Component;

/**
 * Renderer de nombre entier pour le panel matrix de sampling effort.
 *
 * Surligne en rouge les valeurs nulle.
 *
 * @author chatellier
 */
public class SamplingEffortRenderer extends MatrixCellRenderer {


    private static final long serialVersionUID = -5652185461372011347L;

    /**
     * @param model
     */
    public SamplingEffortRenderer(MatrixTableModelND model) {
        super(model);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {


        Component comp = null;

        if (value instanceof Double) {
            Double number = (Double) value;
            int intNumber = number.intValue();

            comp = super.getTableCellRendererComponent(table, intNumber, isSelected, hasFocus,
                                                       row, column);

            if (intNumber <= 0) {
                comp.setBackground(Color.RED);
            } else {
                if (isSelected) {
                    comp.setBackground(table.getSelectionBackground());
                } else {
                    comp.setBackground(table.getBackground());
                }
            }
        } else {
            comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                                                       row, column);
        }

        return comp;
    }


}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.common;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import java.util.Map;

/**
 * L'editeur remplace la valeur présente dans le fichier csv pour l'especes
 * par un autre nom (latin valide, code...)
 *
 * @author echatellier
 * @since 1.3
 */
public class SpeciesTableCellRenderer extends DefaultTableCellRenderer {


    private static final long serialVersionUID = -1913267872081009935L;

    protected Map<String, String> reftaxSpecies;

    public SpeciesTableCellRenderer(Map<String, String> reftaxSpecies) {
        this.reftaxSpecies = reftaxSpecies;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        String speciesId = (String) value;
        if (reftaxSpecies.containsKey(speciesId)) {
            speciesId = reftaxSpecies.get(speciesId);
        }
        return super.getTableCellRendererComponent(table, speciesId, isSelected, hasFocus,
                                                   row, column);
    }
}

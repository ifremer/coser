/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import java.util.List;

/**
 * Model de selection d'especu utilise sur la dialog de graph pour
 * changer l'espece sur laquelle le graphique porte.
 *
 * @author chatellier
 */
public class SpecyComboModel extends AbstractListModel implements ComboBoxModel {


    private static final long serialVersionUID = -4769109927915812519L;

    protected List<String> species;

    protected Object selectedItem;

    public void setSpecy(List<String> species) {
        this.species = species;
        selectedItem = species.get(0);
        fireContentsChanged(this, 0, species.size());
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        int result = 0;
        if (species != null) {
            result = species.size();
        }
        return result;
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return species.get(index);
    }

    public int getIndexOf(Object o) {
        return species.indexOf(o);
    }

    /*
     * @see javax.swing.ComboBoxModel#setSelectedItem(java.lang.Object)
     */
    @Override
    public void setSelectedItem(Object anItem) {
        this.selectedItem = anItem;
    }

    /*
     * @see javax.swing.ComboBoxModel#getSelectedItem()
     */
    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}

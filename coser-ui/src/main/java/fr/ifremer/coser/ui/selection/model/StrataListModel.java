/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.model;

import fr.ifremer.coser.ui.util.CoserListModel;

import javax.swing.AbstractListModel;
import java.util.List;

/**
 * Model contenant la liste des noms de strates (utilisé dans le detail de selection).
 *
 * @author chatellier
 */
public class StrataListModel extends AbstractListModel implements CoserListModel {


    private static final long serialVersionUID = -8155676616312843132L;

    protected List<String> strata;

    public List<String> getStrata() {
        return strata;
    }

    public void setStrata(List<String> strata) {
        this.strata = strata;
        fireContentsChanged(this, 0, strata.size());
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        int size = 0;
        if (strata != null) {
            size = strata.size();
        }
        return size;
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return strata.get(index);
    }

    /*
     * @see fr.ifremer.coser.ui.util.CoserListModel#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(Object element) {
        return strata.indexOf(element);
    }
}

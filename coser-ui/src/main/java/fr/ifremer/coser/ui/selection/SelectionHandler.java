/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.services.PublicationService;
import fr.ifremer.coser.ui.common.DataHandler;
import fr.ifremer.coser.ui.common.SpeciesListRenderer;
import fr.ifremer.coser.ui.control.ControlGraphFrame;
import fr.ifremer.coser.ui.control.ControlHandler;
import fr.ifremer.coser.ui.result.SelectionAddResultDialog;
import fr.ifremer.coser.ui.result.SelectionEditResultDialog;
import fr.ifremer.coser.ui.selection.model.OccurrenceDensitySpeciesListModel;
import fr.ifremer.coser.ui.util.CoserListSelectionModel;
import fr.ifremer.coser.util.Coordinate;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.swing.session.SwingSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixTableModelND;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Selection handler.
 *
 * @author chatellier
 */
public class SelectionHandler extends DataHandler {

    private static final Log log = LogFactory.getLog(SelectionHandler.class);

    /**
     * Appelé sur un changement d'onglet dans l'interface de sélection.
     *
     * Les listes L1à4 n'ecoutent pas directement les modifications du premier
     * onglet. Opération manuelle, calcul de matrice en plus.
     *
     * @param view  view
     * @param event change event
     */
    public void selectionTabChanged(SelectionView view, ChangeEvent event) {

        try {
            setWaitCursor(view);

            // selected tab is Lists
            Component selectedComponent = view.getSelectedComponent();
            if (selectedComponent instanceof SelectionListsView) {
                SelectionListsView selectionListsView = (SelectionListsView) selectedComponent;

                if (log.isDebugEnabled()) {
                    log.debug("List details tab selected");
                }

                Selection selection = view.getContextValue(Selection.class);

                // si la selection n'est pas la même que que celle de la sélection
                // on met à jour la liste de "Toutes les especes" (L1)

                List<String> currentList = selectionListsView.getSelectionAllSpeciesListModel().getSpecies();
                List<String> selectionSpeciesList = selection.getSelectedSpecies();

                if (!selectionSpeciesList.equals(currentList)) {

                    if (log.isDebugEnabled()) {
                        log.debug("Data changed, fill default selection");
                    }

                    // on calcul également les matrices de d'occurence, densité
                    Project project = view.getContextValue(Project.class);
                    ProjectService projectService = view.getContextValue(ProjectService.class);
                    MatrixND occurrence = projectService.getOccurrence(project, selection);
                    occurrence.meanOverDim(1); // moyenne sur les années
                    MatrixND density = projectService.getDensity(project, selection);
                    density.meanOverDim(1); // moyenne sur les années
                    Collection<String> speciesAllYear = projectService.getSpeciesWithSizeAllYears(selection);
                    Collection<String> speciesWithMaturity = projectService.getSpeciesWithMaturity(selection);

                    // set matrix on list model
                    selectionListsView.getSelectionOccurrenceDensityListModel().setOccurrence(occurrence);
                    selectionListsView.getSelectionOccurrenceDensityListModel().setDensity(density);

                    // useless line but fix a event update bug
                    // can take huge time because somes event are fired
                    selectionListsView.getSelectionAllSpeciesList().clearSelection();

                    // fill all species data (at final)
                    selectionListsView.getSelectionAllSpeciesListModel().setSpecies(selectionSpeciesList);
                    ((CoserListSelectionModel) selectionListsView.getSelectionAllSpeciesList().getSelectionModel()).fillSelection();
                    selectionListsView.getSelectionSizeAllYearListModel().setSizeAllYearSpecies(speciesAllYear);
                    selectionListsView.getSelectionMaturityListModel().setMaturitySpecies(speciesWithMaturity);

                    // rechargement de la selection sauvegardée
                    // si aucune des autres listes n'est vide
                    if (!selection.getSelectedSpeciesOccDens().isEmpty() ||
                        !selection.getSelectedSpeciesSizeAllYear().isEmpty() ||
                        !selection.getSelectedSpeciesMaturity().isEmpty()) {

                        selectionListsView.getSelectionFilterOccurrenceField().setText(String.valueOf(selection.getOccurrenceFilter()));
                        selectionListsView.getSelectionFilterDensityField().setText(String.valueOf(selection.getDensityFilter()));

                        // restauration de la selection sauvegardée
                        ((CoserListSelectionModel) selectionListsView.getSelectionOccurrenceDensityList().getSelectionModel()).setSelectedObjects(selection.getSelectedSpeciesOccDens());
                        ((CoserListSelectionModel) selectionListsView.getSelectionSizeAllYearList().getSelectionModel()).setSelectedObjects(selection.getSelectedSpeciesSizeAllYear());
                        ((CoserListSelectionModel) selectionListsView.getSelectionMaturityList().getSelectionModel()).setSelectedObjects(selection.getSelectedSpeciesMaturity());
                    } else {

                        if (log.isDebugEnabled()) {
                            log.debug("Apply occurrence/density filter");
                        }

                        // select all list (not to do for reload)
                        // init allSize and maturity before (select all with updateOccurrenceDensityFilter)
                        updateOccurrenceDensityFilter(selectionListsView);
                    }
                }
            }
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Initialize certains donnés différement que le ferait le rechargement
     * d'une selection.
     *
     * @param view view
     */
    public void initSelection(SelectionView view) {

        // initialize la liste de toutes les années
        // c'est normalement la seule initialisation a faire
        SelectionDetailsView detailView = view.getSelectionDetailsTab();
        initSelectionYears(detailView);

        view.setCreationState(true);

        // disable tabs
        view.setEnabledAt(1, false); // selection lists
        view.setEnabledAt(2, false); // rsufi
    }

    /**
     * Initialise la listes des années disponible dans les données chargées.
     *
     * @param view view
     */
    public void initSelectionYears(SelectionDetailsView view) {
        Selection selection = view.getContextValue(Selection.class);
        view.getYearsListModel().setYears(selection.getAllYears());

        // tout est selectionné par defaut
        ((CoserListSelectionModel) view.getYearsList().getSelectionModel()).setSelectedObjects(selection.getSelectedYears());

        // clear other lists
        view.getStrataListModel().setStrata(new ArrayList<String>());
        view.getStrataList().clearSelection();
        view.getFilteredSpeciesListModel().setSpecies(new ArrayList<String>());
        view.getFilteredSpeciesList().clearSelection();
        view.getSelectedSpeciesListModel().setSpecies(new ArrayList<String>());
        view.getSelectedSpeciesList().clearSelection();

        // update sampling effort data table
        showSamplingEffort(view);
    }

    /**
     * Recharge la selection en valorisant les différentes listes.
     *
     * Fait ici, car sinon, les evenements ne se déclenchent pas au même
     * moment.
     *
     * @param view view to fill
     */
    public void reloadSelection(SelectionView view) {

        if (log.isDebugEnabled()) {
            log.debug("Reloading selection...");
        }

        Selection selection = view.getSelection();
        SelectionDetailsView detailView = view.getSelectionDetailsTab();

        // details view : fill details view
        detailView.getYearsListModel().setYears(selection.getAllYears());
        List<String> selectedYears = selection.getSelectedYears();
        ((CoserListSelectionModel) detailView.getYearsList().getSelectionModel()).setSelectedObjects(selectedYears);

        // details view : fill strata data and selection
        updateSelectionYearsData(detailView);
        List<String> selectedStrata = selection.getSelectedStrata();
        ((CoserListSelectionModel) detailView.getStrataList().getSelectionModel()).setSelectedObjects(selectedStrata);

        // details view : fill species list and selection
        detailView.getSelectedSpeciesListModel().setSpecies(selection.getSelectedSpecies());
        updateSelectionStrataData(detailView);
        List<String> selectedSpecies = selection.getSelectedSpecies();
        detailView.getSelectedSpeciesListModel().setSpecies(selectedSpecies);

        // disable tabs and other actions
        detailView.setYearsValidated(!selectedYears.isEmpty());
        detailView.setStrataValidated(!selectedStrata.isEmpty());
        detailView.setSpeciesValidated(!selectedSpecies.isEmpty());
        view.setEnabledAt(2, selection.isValidated()); // rsufi

        // update sampling effort data table
        showSamplingEffort(detailView);
    }

    /**
     * Recharge les données de controle sur action utilsateur.
     *
     * Si confirmation:
     * <ul>
     * <li>recharge les données
     * <li>reactive le bouton année
     * <li>affiche l'accordeon année
     * </ul>
     *
     * @param view view
     */
    public void reloadControlData(SelectionDetailsView view) {

        int response = JOptionPane.showConfirmDialog(view, t("coser.ui.selection.detail.confirmcontrolreload"),
                                                     t("coser.ui.selection.selectionTitle"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (response == JOptionPane.YES_OPTION) {
            Project project = view.getContextValue(Project.class);
            Selection selection = view.getContextValue(Selection.class);
            ProjectService projectService = view.getContextValue(ProjectService.class);
            try {
                projectService.loadControlDataToSelection(project, selection);

                // affiche les années
                initSelectionYears(view);
                view.getValidDatesButton().setEnabled(true);
                view.getDetailAccordionPane().setSelected(1);

                JOptionPane.showMessageDialog(view, t("coser.ui.selection.detail.controldatareloaded"),
                                              t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);

            } catch (CoserBusinessException ex) {
                throw new CoserException("Can't reload control data", ex);
            }
        }
    }

    /**
     * Rafraichit la liste des zones suite à la selection des années.
     * Rechargement de selection ou action utilisateur.
     *
     * @param view view
     */
    protected void updateSelectionYearsData(SelectionDetailsView view) {
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        // get selected zones as list
        Object[] selectedDates = view.getYearsList().getSelectedValues();
        List<String> years = new ArrayList<String>(selectedDates.length);
        for (Object selectedDate : selectedDates) {
            years.add((String) selectedDate);
        }

        if (log.isDebugEnabled()) {
            log.debug("Refreshing strata list");
        }

        try {
            // filterDataYearsAndGetStrata do selection.setSelectedYears(years);
            // don't set yourself
            List<String> strata = projectService.filterDataYearsAndGetStrata(project, selection, years);
            view.getStrataListModel().setStrata(strata);

            // auto selectionne les strates par default
            view.getStrataList().addSelectionInterval(0, strata.size() - 1);

            // clear other sublists
            view.getFilteredSpeciesListModel().setSpecies(new ArrayList<String>());
            view.getFilteredSpeciesList().clearSelection();
            view.getSelectedSpeciesListModel().setSpecies(new ArrayList<String>());
            view.getSelectedSpeciesList().clearSelection();

            if (log.isDebugEnabled()) {
                log.debug("Years list refreshed");
            }
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't filters data with specified years", ex);
        }
    }

    /**
     * Rafraichit la liste des zones suite à la selection des années.
     *
     * Action utilisateur.
     *
     * @param view view
     */
    public void validSelectionYearsData(SelectionDetailsView view) {

        try {
            setWaitCursor(view);

            updateSelectionYearsData(view);

            // update sampling effort data table
            showSamplingEffort(view);

            JOptionPane.showMessageDialog(view, t("coser.ui.selection.detail.yearsvalidated"),
                                          t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);

            // auto select strata accordion pane
            view.getDetailAccordionPane().setSelected(2);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Init openmap toolbar and openmap layers.
     *
     * @param view view
     */
    public void initView(SelectionView view) {
        // init specific tab
        initView(view.getSelectionDetailsTab());
        initView(view.getSelectionListsView());
    }

    /**
     * Init detail tab view
     *
     * @param view view
     */
    protected void initView(SelectionDetailsView view) {

        // add layers
        Project project = view.getContextValue(Project.class);
        view.getStrataMap().initMap(project, view.getStataMapInfo());
        view.getStataMapInfo().setMap(view.getStrataMap().getMapBean());
        view.getStataMapInfo().setShowCoordsInfoLine(true);
        view.getStataMapInfo().setShowInfoLine(true);

        // fill species type selection by default
        int count = view.getTypeSpeciesModel().getSize();
        view.getTypeSpeciesList().addSelectionInterval(0, count - 1);

        // init species renderer
        SpeciesListRenderer renderer = new SpeciesListRenderer(project.getRefTaxSpeciesMap());
        view.getFilteredSpeciesList().setCellRenderer(renderer);
        view.getSelectedSpeciesList().setCellRenderer(renderer);
    }

    /**
     * Init selection list tab view.
     *
     * @param view view
     */
    protected void initView(SelectionListsView view) {
        Project project = view.getContextValue(Project.class);
        Map<String, String> reftaxSpecies = project.getRefTaxSpeciesMap();
        SpeciesListRenderer renderer = new SpeciesListRenderer(reftaxSpecies);
        view.getSelectionAllSpeciesList().setCellRenderer(renderer);
        view.getSelectionOccurrenceDensityList().setCellRenderer(new SpeciesListOccDensRenderer(reftaxSpecies));
        view.getSelectionSizeAllYearList().setCellRenderer(renderer);
        view.getSelectionMaturityList().setCellRenderer(renderer);
    }

    /**
     * Appelé lorsque la selection de la liste des strate a changé.
     * Rechargement de selection ou action utilsateur.
     *
     * @param view view
     */
    protected void updateSelectionStrataData(SelectionDetailsView view) {

        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        if (log.isDebugEnabled()) {
            log.debug("Strata list selection changed, updating species list");
        }

        // get selected zones as list
        Object[] selectedStrata = view.getStrataList().getSelectedValues();
        List<String> strata = new ArrayList<String>(selectedStrata.length);
        for (Object selectedStratum : selectedStrata) {
            strata.add((String) selectedStratum);
        }

        // do selection.setSelectedStrata(strata);
        projectService.filterDataStrata(project, selection, strata);
        // remove non selected stata non in data anymore
        // maybe will be improved for v2
        view.getStrataListModel().setStrata(strata);
        ((CoserListSelectionModel) view.getStrataList().getSelectionModel()).setSelectedObjects(strata);

        updateSelectionSpecies(view);
    }

    /**
     * Appelé lorsque la selection de la liste des strate a changé.
     * Action utilsteur seulement.
     *
     * @param view view
     */
    public void validSelectionStrataData(SelectionDetailsView view) {
        try {
            setWaitCursor(view);

            updateSelectionStrataData(view);

            // update sampling effort data table
            showSamplingEffort(view);

            JOptionPane.showMessageDialog(view, t("coser.ui.selection.detail.stratavalidated"),
                                          t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);

            // auto select species accordion pane
            view.getDetailAccordionPane().setSelected(3);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Rafraichit la liste des especes avec les dates sélectionnées
     * et les strates selectionnées ET filtrées par la liste
     * des type d'especes.
     *
     * Appelé lorsque la selection de la liste des zones change.
     *
     * @param view view
     */
    public void updateSelectionSpecies(SelectionDetailsView view) {

        if (log.isDebugEnabled()) {
            log.debug("Updating species list");
        }

        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        // get selected species types
        Object[] selectedSpeciesTypes = view.getTypeSpeciesList().getSelectedValues();
        List<String> speciesTypes = new ArrayList<String>();
        for (Object selectedSpeciesType : selectedSpeciesTypes) {
            speciesTypes.add((String) selectedSpeciesType);
        }

        List<String> filteredSpecies = projectService.getProjectSpecies(selection, project, speciesTypes);

        // code pour que la liste des ids d'especes soit trié
        // comme celle de visualisation de l'utilisateur (vue triée du rendu)
        filteredSpecies = CoserUtils.sortCollectionWithMapKeys(project.getRefTaxSpeciesMap(), filteredSpecies);

        // ne fait pas apparaitre les especes deja selectionnées
        List<String> selectedSpecies = view.getSelectedSpeciesListModel().getSpecies();
        filteredSpecies.removeAll(selectedSpecies);
        view.getFilteredSpeciesListModel().setSpecies(filteredSpecies);
    }

    /**
     * Rafraichit la liste des especes avec les dates sélectionnées
     * et les strates selectionnées ET filtrées par la liste
     * des type d'especes.
     *
     * Appelé lorsque la selection de la liste des zones change.
     *
     * @param view view
     */
    public void validSelectionSpeciesData(SelectionDetailsView view) {

        try {
            setWaitCursor(view);
            Project project = view.getContextValue(Project.class);
            Selection selection = view.getContextValue(Selection.class);
            ProjectService projectService = view.getContextValue(ProjectService.class);

            // get selected species
            List<String> selectedSpecies = view.getSelectedSpeciesListModel().getSpecies();
            projectService.filterDataSpecies(project, selection, selectedSpecies);

            // to remove from view, non available species
            updateSelectionSpecies(view);

            JOptionPane.showMessageDialog(view, t("coser.ui.selection.detail.speciesvalidated"),
                                          t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Affiche un menu contextuel lors du clic (droit) sur la liste des années.
     *
     * @param view  view
     * @param event mouse event
     */
    public void showYearsContextMenu(final SelectionDetailsView view, MouseEvent event) {

        // clic droit
        if (event.getButton() == MouseEvent.BUTTON3) {

            JPopupMenu popupMenu = new JPopupMenu();

            // select all menu
            JMenuItem selectAllMenu = new JMenuItem(t("coser.ui.common.selectAll"));
            selectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int size = view.getYearsList().getModel().getSize();
                    view.getYearsList().getSelectionModel().addSelectionInterval(0, size - 1);
                }
            });
            popupMenu.add(selectAllMenu);

            // unselect all menu
            JMenuItem unselectAllMenu = new JMenuItem(t("coser.ui.common.unselectAll"));
            unselectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    view.getYearsList().getSelectionModel().clearSelection();
                }
            });
            popupMenu.add(unselectAllMenu);

            popupMenu.show(view.getYearsList(), event.getX(), event.getY());
        }
    }

    /**
     * Affiche un menu contextuel lors du clic (droit) sur la liste des strates.
     *
     * @param view  view
     * @param event mouse event
     */
    public void showStrataContextMenu(final SelectionDetailsView view, MouseEvent event) {

        // clic droit
        if (event.getButton() == MouseEvent.BUTTON3) {

            JPopupMenu popupMenu = new JPopupMenu();

            // select all menu
            JMenuItem selectAllMenu = new JMenuItem(t("coser.ui.common.selectAll"));
            selectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int size = view.getStrataList().getModel().getSize();
                    view.getStrataList().getSelectionModel().addSelectionInterval(0, size - 1);
                }
            });
            popupMenu.add(selectAllMenu);

            // unselect all menu
            JMenuItem unselectAllMenu = new JMenuItem(t("coser.ui.common.unselectAll"));
            unselectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    view.getStrataList().getSelectionModel().clearSelection();
                }
            });
            popupMenu.add(unselectAllMenu);

            popupMenu.show(view.getStrataList(), event.getX(), event.getY());
        }
    }

    /**
     * Affiche un menu contextuel lors du clic (droit) sur la liste filtrées des especes.
     *
     * @param view  view
     * @param event mouse event
     */
    public void showFilteredSpeciesContextMenu(final SelectionDetailsView view, MouseEvent event) {

        // clic droit
        if (event.getButton() == MouseEvent.BUTTON3) {

            JPopupMenu popupMenu = new JPopupMenu();

            // affiche le menu contextuel si au moins 2 especes selectionnées
            int[] selectedRows = view.getFilteredSpeciesList().getSelectedIndices();
            if (selectedRows.length > 0) {
                // merge menu
                JMenuItem fusionMenu = new JMenuItem(t("coser.ui.selection.speciesMenuFusion"));
                fusionMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        showSpeciesMergeDialog(view);
                    }
                });
                popupMenu.add(fusionMenu);

                // graph
                JMenuItem graphMenu = new JMenuItem(t("coser.ui.selection.details.displayDiffCatchLengthGraph"));
                graphMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        showSelectedSpeciesGraph(view, view.getFilteredSpeciesList());
                    }
                });
                popupMenu.add(graphMenu);
            }

            // select all menu
            JMenuItem selectAllMenu = new JMenuItem(t("coser.ui.common.selectAll"));
            selectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int size = view.getFilteredSpeciesList().getModel().getSize();
                    view.getFilteredSpeciesList().getSelectionModel().addSelectionInterval(0, size - 1);
                }
            });
            popupMenu.add(selectAllMenu);

            // unselect all menu
            JMenuItem unselectAllMenu = new JMenuItem(t("coser.ui.common.unselectAll"));
            unselectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    view.getFilteredSpeciesList().getSelectionModel().clearSelection();
                }
            });
            popupMenu.add(unselectAllMenu);

            popupMenu.show(view.getFilteredSpeciesList(), event.getX(), event.getY());
        }
    }

    /**
     * Affiche un menu contextuel lors du clic (droit) sur la liste des especes
     * sélectionnées.
     *
     * @param view  view
     * @param event mouse event
     */
    public void showSelectedSpeciesContextMenu(final SelectionDetailsView view, MouseEvent event) {

        // clic droit
        if (event.getButton() == MouseEvent.BUTTON3) {

            JPopupMenu popupMenu = new JPopupMenu();

            int[] selectedRows = view.getSelectedSpeciesList().getSelectedIndices();
            if (selectedRows.length > 0) {
                // graph
                JMenuItem graphMenu = new JMenuItem(t("coser.ui.selection.details.displayDiffCatchLengthGraph"));
                graphMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        showSelectedSpeciesGraph(view, view.getSelectedSpeciesList());
                    }
                });
                popupMenu.add(graphMenu);
            }

            // select all menu
            JMenuItem selectAllMenu = new JMenuItem(t("coser.ui.common.selectAll"));
            selectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int size = view.getSelectedSpeciesList().getModel().getSize();
                    view.getSelectedSpeciesList().getSelectionModel().addSelectionInterval(0, size - 1);
                }
            });
            popupMenu.add(selectAllMenu);

            // unselect all menu
            JMenuItem unselectAllMenu = new JMenuItem(t("coser.ui.common.unselectAll"));
            unselectAllMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    view.getSelectedSpeciesList().getSelectionModel().clearSelection();
                }
            });
            popupMenu.add(unselectAllMenu);

            popupMenu.show(view.getSelectedSpeciesList(), event.getX(), event.getY());
        }
    }

    /**
     * Show species merge dialog. Called from view action or context menu.
     *
     * @param view parent view
     */
    public void showSpeciesMergeDialog(SelectionDetailsView view) {
        Project project = view.getContextValue(Project.class);
        SpeciesFusionDialog speciesFusionDialog = new SpeciesFusionDialog(view);
        speciesFusionDialog.setHandler(SelectionHandler.this);

        // fill species combo box with reftax species list
        Map<String, String> reftaxSpecies = project.getRefTaxSpeciesMap();
        ListComboBoxModel speciesComboModel = new ListComboBoxModel(new ArrayList<String>(reftaxSpecies.keySet()));
        speciesFusionDialog.getSpeciesCombo().setModel(speciesComboModel);
        speciesFusionDialog.getSpeciesCombo().setRenderer(new SpeciesListRenderer(reftaxSpecies));

        // init selected element
        String firstSelected = (String) view.getFilteredSpeciesList().getSelectedValue();
        speciesFusionDialog.getSpeciesCombo().setSelectedItem(firstSelected);

        Object[] selectedValues = view.getFilteredSpeciesList().getSelectedValues();
        if (selectedValues.length == 1) {
            speciesFusionDialog.setTitle(t("coser.ui.selection.fusion.rename.title"));
        } else {
            speciesFusionDialog.setTitle(t("coser.ui.selection.fusion.title"));
        }

        speciesFusionDialog.setLocationRelativeTo(view);
        speciesFusionDialog.setVisible(true);
    }

    /**
     * Clic sur le bouton selectionner tout.
     * Selectionne toutes les espèces dans les 2 listes contrairement au
     * menu contextuel qui ne selectionne que dans une seule liste.
     *
     * @param view parent view
     * @since 1.3
     */
    public void selectAllListSpecies(SelectionDetailsView view) {
        // available
        int size = view.getFilteredSpeciesList().getModel().getSize();
        view.getFilteredSpeciesList().getSelectionModel().addSelectionInterval(0, size - 1);
        // selected
        size = view.getSelectedSpeciesList().getModel().getSize();
        view.getSelectedSpeciesList().getSelectionModel().addSelectionInterval(0, size - 1);
    }

    /**
     * Clic sur le bouton selectionner tout.
     * Selectionne toutes les espèces dans les 2 listes contrairement au
     * menu contextuel qui ne selectionne que dans une seule liste.
     *
     * @param view parent view
     * @since 1.3
     */
    public void unSelectAllListSpecies(SelectionDetailsView view) {
        // available
        view.getFilteredSpeciesList().getSelectionModel().clearSelection();
        // selected
        view.getSelectedSpeciesList().getSelectionModel().clearSelection();
    }

    /**
     * Sauvegarde (ou creer) la sélection (partie details).
     *
     * @param view parent view
     */
    public void saveSelection(SelectionDetailsView view) {
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);
        SelectionView selectionView = view.getParentContainer(SelectionView.class);

        try {
            if (view.isCreationState()) {
                projectService.createProjectSelection(project, selection);
                selectionView.setCreationState(false);

                JOptionPane.showMessageDialog(view, t("coser.ui.selection.selectionCreated"),
                                              t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);
            } else {
                projectService.saveProjectSelection(project, selection);

                JOptionPane.showMessageDialog(view, t("coser.ui.selection.selectionSaved"),
                                              t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (CoserBusinessException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't save selection", ex);
            }
            JOptionPane.showMessageDialog(view, ex.getMessage(), t("coser.ui.selection.saveError"), JOptionPane.ERROR_MESSAGE);
        }

        // enable tabs
        selectionView.setEnabledAt(1, true); // selection lists
    }

    /**
     * Sauve les éléments selectionnés dans la sélection.
     *
     * @param view      view containing jlist
     * @param selection selection to save lists to
     */
    protected void saveSelectionLists(SelectionListsView view, Selection selection) {

        // do not use CoserSelectionModel.getSelectedValues here (not same datas)

        List<String> occDensList = new ArrayList<String>();
        Object[] occDensSpeciesSelection = view.getSelectionOccurrenceDensityList().getSelectedValues();
        for (Object occDensSpecies : occDensSpeciesSelection) {
            occDensList.add((String) occDensSpecies);
        }

        List<String> sizeAllYearList = new ArrayList<String>();
        Object[] sizeAllYearSpeciesSelection = view.getSelectionSizeAllYearList().getSelectedValues();
        for (Object sizeAllYearSpecies : sizeAllYearSpeciesSelection) {
            sizeAllYearList.add((String) sizeAllYearSpecies);
        }

        List<String> maturityList = new ArrayList<String>();
        Object[] maturitySpeciesSelection = view.getSelectionMaturityList().getSelectedValues();
        for (Object maturitySpecies : maturitySpeciesSelection) {
            maturityList.add((String) maturitySpecies);
        }

        selection.setSelectedSpeciesOccDens(occDensList);
        selection.setSelectedSpeciesSizeAllYear(sizeAllYearList);
        selection.setSelectedSpeciesMaturity(maturityList);
    }

    /**
     * Marque la selection comme validée et la sauve ensuite).
     *
     * Test que si une liste a été modifiée par l'utilisateur, le champs
     * commentaire associé est bien renseigné.
     *
     * @param view view
     */
    public void validSelection(SelectionListsView view) {
        Selection selection = view.getContextValue(Selection.class);
        Project project = view.getContextValue(Project.class);
        ProjectService service = view.getContextValue(ProjectService.class);
        saveSelectionLists(view, selection);
        boolean canBeValidated = checkSelectionListComments(view, selection);
        if (canBeValidated) {
            try {
                saveSelectionLists(view, selection);
                service.validSelection(project, selection);
                JOptionPane.showMessageDialog(view, t("coser.ui.selection.selectionValidated"),
                                              t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);

                // enable tabs
                SelectionView selectionView = view.getParentContainer(SelectionView.class);
                selectionView.setEnabledAt(2, true); // selection lists
            } catch (CoserBusinessException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't save selection", ex);
                }
                JOptionPane.showMessageDialog(view, ex.getMessage(), t("coser.ui.selection.saveError"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Test que les selections sont complete ou alors justifiée par un
     * commentaires.
     *
     * @param view view containing jlist
     * @return {@code true} if selection can be saved
     */
    protected boolean checkSelectionListComments(SelectionListsView view, Selection selection) {

        boolean canValidate = true;

        // do not use CoserSelectionModel.getSelectedValues here (not same datas)

        // pour le modele occurence/densite, cela doit être egal a tout
        // se qu'aurait du selectionné le filtre
        Set<String> currentOccDensSelection = new HashSet<String>(selection.getSelectedSpeciesOccDens());
        double occurrence = selection.getOccurrenceFilter();
        double density = selection.getDensityFilter();
        Set<String> originalOccDensSelection = getOccurenceDensityFilteredSpecies(view, occurrence, density);

        if (!currentOccDensSelection.equals(originalOccDensSelection)
            && StringUtils.isBlank(selection.getSelectedSpeciesOccDensComment())) {
            JOptionPane.showMessageDialog(view, t("coser.ui.selection.nonJustifiedOccurenceDensity"),
                                          t("coser.ui.selection.nonJustifiedTitle"), JOptionPane.ERROR_MESSAGE);
            view.getOccurrenceDensityCommentField().requestFocus();
            canValidate = false;
        }

        // pour le modele taille toutes les années : tout doit être selectionné
        if (canValidate) {
            int sizeAllYearSpeciesInModel = view.getSelectionSizeAllYearListModel().getSize();
            int[] sizeAllYearSpeciesSelection = view.getSelectionSizeAllYearList().getSelectedIndices();
            if (sizeAllYearSpeciesSelection.length < sizeAllYearSpeciesInModel
                && StringUtils.isBlank(selection.getSelectedSpeciesSizeAllYearComment())) {
                JOptionPane.showMessageDialog(view, t("coser.ui.selection.nonJustifiedSizeAllYear"),
                                              t("coser.ui.selection.nonJustifiedTitle"), JOptionPane.ERROR_MESSAGE);
                view.getSizeAllYearCommentField().requestFocus();
                canValidate = false;
            }
        }

        // pour le modele maturite : tout doit être selectionné
        if (canValidate) {
            int maturitySpeciesInModel = view.getSelectionMaturityListModel().getSize();
            int[] maturitySpeciesSelection = view.getSelectionMaturityList().getSelectedIndices();
            if (maturitySpeciesSelection.length < maturitySpeciesInModel
                && StringUtils.isBlank(selection.getSelectedSpeciesMaturityComment())) {
                JOptionPane.showMessageDialog(view, t("coser.ui.selection.nonJustifiedMaturity"),
                                              t("coser.ui.selection.nonJustifiedTitle"), JOptionPane.ERROR_MESSAGE);
                view.getMaturityCommentField().requestFocus();
                canValidate = false;
            }
        }

        return canValidate;
    }

    /**
     * Mise à jour du filtre d'ocurrence et densité.
     *
     * Selectionne dans la liste seulement les especes ayant une densité
     * suppérieure à celle indique (idem pour l'occurence).
     *
     * @param view selection list view
     */
    public void updateOccurrenceDensityFilter(SelectionListsView view) {

        Selection selection = view.getContextValue(Selection.class);

        String stringOccurrence = view.getSelectionFilterOccurrenceField().getText().trim();
        String stringDensity = view.getSelectionFilterDensityField().getText().trim();

        try {
            double occurrence = Double.valueOf(stringOccurrence);
            double density = Double.valueOf(stringDensity);

            selection.setOccurrenceFilter(occurrence);
            selection.setDensityFilter(density);

            // construction de la liste de selection des especes
            Set<String> filteredSpeciesSet = getOccurenceDensityFilteredSpecies(view, occurrence, density);
            List<String> filteredSpecies = new ArrayList<String>(filteredSpeciesSet);
            Collections.sort(filteredSpecies);

            // application de la nouvelle selection
            ((CoserListSelectionModel) view.getSelectionOccurrenceDensityList().getSelectionModel()).setSelectedObjects(filteredSpecies);

            // selectionne automatiquement les listes L3 & L4
            ((CoserListSelectionModel) view.getSelectionSizeAllYearList().getSelectionModel()).fillSelection();
            ((CoserListSelectionModel) view.getSelectionMaturityList().getSelectionModel()).fillSelection();
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse occurrence or density as double", ex);
            }
        }
    }

    /**
     * Return l'ensemble des especes qui respece le filtrage par occurrence/densite.
     *
     * @param view       view
     * @param occurrence occurrence
     * @param density    density
     * @return species set
     */
    protected Set<String> getOccurenceDensityFilteredSpecies(SelectionListsView view, double occurrence, double density) {
        // construction de la liste de selection des especes
        OccurrenceDensitySpeciesListModel model = view.getSelectionOccurrenceDensityListModel();
        int speciesCount = model.getSize();
        Set<String> filteredSpecies = new HashSet<String>();
        for (int speciesIndex = 0; speciesIndex < speciesCount; speciesIndex++) {
            String species = (String) model.getElementAt(speciesIndex);
            if (model.getDensity(species) >= density && model.getOccurrence(species) >= occurrence) {
                filteredSpecies.add(species);
            }
        }
        return filteredSpecies;
    }

    /**
     * Check que les paramètres sont correct (nouveau nom existant)
     * et applique la fusion d'espece. Rafraichit la view
     * parente ensuite.
     *
     * @param view
     */
    public void performMergeSpecies(SpeciesFusionDialog view) {

        // TODO echatellier 20101021 attention a ce que la selection
        // ne change pas (la fenetre est modale pour l'instant)

        String newSpeciesCode = (String) view.getSpeciesCombo().getSelectedItem();
        String comment = view.getCommentField().getText();
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        SelectionDetailsView parent = view.getContextValue(SelectionDetailsView.class, JAXXUtil.PARENT);
        Object[] selectedSpecies = parent.getFilteredSpeciesList().getSelectedValues();
        String[] speciesCodes = new String[selectedSpecies.length];
        for (int i = 0; i < selectedSpecies.length; ++i) {
            String singleSelectedSpecies = (String) selectedSpecies[i];
            speciesCodes[i] = singleSelectedSpecies;
        }

        try {
            projectService.mergeSpecies(project, selection, newSpeciesCode, comment, speciesCodes);

            updateSelectionSpecies(parent);
            // supprime la selection apres fusion (demande utilisateur)
            parent.getFilteredSpeciesListSelectionModel().clearSelection();

            if (speciesCodes.length == 1) {
                JOptionPane.showMessageDialog(view, t("coser.ui.selection.speciesRenamed"),
                                              t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(view, t("coser.ui.selection.speciesMerged"),
                                              t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (CoserBusinessException ex) {
            JOptionPane.showMessageDialog(view, ex.getMessage(),
                                          t("coser.ui.selection.fusion.title"), JOptionPane.ERROR_MESSAGE);
        }

        view.dispose();
    }

    /**
     * Show sampling effort data.
     *
     * @param view view
     */
    public void showSamplingEffort(SelectionDetailsView view) {
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);
        MatrixND samplingEffort = projectService.getSamplingEffort(project, selection);

        // can return null if there is not enought data
        if (samplingEffort != null) {
            view.getMatrixPanelEditor().setMatrix(samplingEffort);

            // replace default renderer with a renderer that display
            // null values in red background
            view.getMatrixPanelEditor().getTable().setDefaultRenderer(String.class,
                                                                      new SamplingEffortRenderer((MatrixTableModelND) view.getMatrixPanelEditor().getTable().getModel()));
        }
    }

    /**
     * Display selected strata haul position in map.
     *
     * @param view view
     */
    public void showStataOnMap(SelectionDetailsView view) {
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        // get selected stata
        Object[] selectedStrataArray = view.getStrataList().getSelectedValues();
        Set<String> selectedStata = new HashSet<String>();
        for (Object selectedStrataItem : selectedStrataArray) {
            selectedStata.add((String) selectedStrataItem);
        }

        // display on map
        try {
            List<Coordinate> hauls = projectService.getStrataHaulCoordinate(selection, selectedStata);
            view.getStrataMap().addStataHaulLayer(hauls);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't display stata's hauls on map", ex);
        }
    }

    /**
     * Display data graph, initialized with graph for selected specy
     * selected.
     *
     * TODO chatellier 20110215 : set this code in common with control same code
     *
     * @param view view
     */
    public void displayCompareNumberCatchGraph(SelectionListsView view) {
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        SwingSession session = view.getContextValue(SwingSession.class);

        // get matrix (form context by method #checkData)
        PublicationService publicationService = view.getContextValue(PublicationService.class);
        Map<String, JFreeChart> charts = publicationService.getCompareCatchLengthGraph(project, selection, null);
        view.setContextValue(charts, "CompareCatchLengthGraph");
        List<String> species = view.getSelectionSizeAllYearListModel().getSpecies();

        // close previous opened
        JFrame previousFrame = view.getContextValue(JFrame.class, "comparenumberframe");
        if (previousFrame != null) {
            previousFrame.dispose();
        }

        ControlGraphFrame frame = new ControlGraphFrame(view);
        frame.setHandler(new ControlHandler());
        frame.setContextValue(charts);
        frame.getSpeciesComboModel().setSpecy(species);
        frame.getSpeciesCombo().setRenderer(new SpeciesListRenderer(project.getRefTaxSpeciesMap()));
        frame.pack();
        frame.setLocationRelativeTo(view);
        session.add(frame); // session restore
        frame.toFront();
        frame.setVisible(true);

        // register current frame
        view.setContextValue(frame, "comparenumberframe");
    }

    /**
     * Display lengthStructure matrix in matrixviewerpanel.
     *
     * @param view view
     */
    public void displayLengthStructureGraph(SelectionListsView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        SwingSession session = view.getContextValue(SwingSession.class);
        displayLengthStructureGraph(view, session, projectService, project, selection, null);
    }

    /**
     * Ajout les especes filtrées selectionnées dans la liste des especes
     * selectionnées.
     *
     * @param view view
     */
    public void addSelectedFilteredSpecies(SelectionDetailsView view) {
        Project project = view.getContextValue(Project.class);

        // get selected species
        Object[] selectedFilteredSpecies = view.getFilteredSpeciesList().getSelectedValues();
        Set<String> selectedSpecies = new HashSet<String>();
        if (view.getSelectedSpeciesListModel().getSpecies() != null) {
            selectedSpecies.addAll(view.getSelectedSpeciesListModel().getSpecies());
        }
        for (Object singleSelectedFilteredSpecies : selectedFilteredSpecies) {
            selectedSpecies.add((String) singleSelectedFilteredSpecies);
        }

        // met a jour le liste des especes selectionnées
        List<String> selectedSpeciesList = new ArrayList<String>(selectedSpecies);

        // code pour que la liste des ids d'especes soit trié
        // comme celle de visualisation de l'utilisateur (vue triée du rendu)
        selectedSpeciesList = CoserUtils.sortCollectionWithMapKeys(project.getRefTaxSpeciesMap(), selectedSpeciesList);

        view.getSelectedSpeciesListModel().setSpecies(selectedSpeciesList);

        // met a jour la liste filtrée (retrait des especes selectionnées)
        // ne fait pas apparaitre les especes deja selectionnées
        List<String> filteredSpecies = view.getFilteredSpeciesListModel().getSpecies();
        filteredSpecies.removeAll(selectedSpeciesList);
        view.getFilteredSpeciesListModel().setSpecies(filteredSpecies);
    }

    /**
     * Retire les especes selectionnées de la liste des esepeces sélectionnées.
     *
     * @param view view
     */
    public void removeSelectedSpecies(SelectionDetailsView view) {
        Project project = view.getContextValue(Project.class);

        List<String> filteredSpecies = new ArrayList<String>(view.getFilteredSpeciesListModel().getSpecies());
        List<String> selectedSpecies = new ArrayList<String>(view.getSelectedSpeciesListModel().getSpecies());
        Object[] selectedSelectedSpecies = view.getSelectedSpeciesList().getSelectedValues();
        for (Object singleSelectedSelectedSpecies : selectedSelectedSpecies) {
            selectedSpecies.remove(singleSelectedSelectedSpecies);
            filteredSpecies.add((String) singleSelectedSelectedSpecies);
        }

        // code pour que la liste des ids d'especes soit trié
        // comme celle de visualisation de l'utilisateur (vue triée du rendu)
        filteredSpecies = CoserUtils.sortCollectionWithMapKeys(project.getRefTaxSpeciesMap(), filteredSpecies);

        // met à jour la liste des especes selectionnées
        view.getSelectedSpeciesListModel().setSpecies(selectedSpecies);

        // met a jour la liste filtrée (retrait des especes selectionnées)
        view.getFilteredSpeciesListModel().setSpecies(filteredSpecies);
    }

    /**
     * Affiche les graphes de différence captures/taille dans le panel
     * reféréncé par {@code cardLayoutContraints} suivant la liste
     * sur laquelle porte l'appel.
     *
     * @param view   view
     * @param source source list to choose selected values (can be {@code null}, read both lists)
     */
    public void showSelectedSpeciesGraph(SelectionDetailsView view, JList source) {

        PublicationService publicationService = view.getContextValue(PublicationService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);

        Set<String> graphSpecies = new HashSet<String>();
        // specific list given (context menu)
        if (source != null) {
            Object[] selectedSpecies = source.getSelectedValues();
            for (Object selectedSingleSpecies : selectedSpecies) {
                graphSpecies.add((String) selectedSingleSpecies);
            }
        } else {
            // merge both lists
            Object[] selectedSpecies = view.getFilteredSpeciesList().getSelectedValues();
            for (Object selectedSingleSpecies : selectedSpecies) {
                graphSpecies.add((String) selectedSingleSpecies);
            }
            selectedSpecies = view.getSelectedSpeciesList().getSelectedValues();
            for (Object selectedSingleSpecies : selectedSpecies) {
                graphSpecies.add((String) selectedSingleSpecies);
            }
        }

        Map<String, JFreeChart> chartsMap = publicationService.getCompareCatchLengthGraph(project, selection, graphSpecies);
        Collection<JFreeChart> charts = chartsMap.values();

        // add all this graphes in panel
        JPanel graphPanel = view.getSpeciesGraphPanel();
        graphPanel.removeAll();
        Iterator<JFreeChart> itCharts = charts.iterator();
        int chartIndex = 0;
        while (itCharts.hasNext()) {
            JFreeChart chart = itCharts.next();

            ChartPanel chartPanel = new ChartPanel(chart);
            view.getSpeciesGraphPanel().add(chartPanel,
                                            new GridBagConstraints(0, chartIndex, 1, 1, 1, 1, GridBagConstraints.CENTER,
                                                                   GridBagConstraints.BOTH, new Insets(0, 3, 3, 3), 0, 0));
            chartIndex++;
        }
        view.getDetailDecisionPanelLayout().show(view.getDetailDecisionPanel(), "SPECIESGRAPH");
        view.getDetailDecisionPanel().validate();
        view.getDetailDecisionPanel().repaint();
    }

    /**
     * Display rsufi new result dialog.
     *
     * @param view view
     */
    public void showAddResultDialog(SelectionRsufiView view) {
        SelectionAddResultDialog addResultView = new SelectionAddResultDialog(view);

        // Mettre RSUFI_RESULTS_nomduprojet par defaut
        // dans le nom du resultat.
        Project project = view.getContextValue(Project.class);
        RSufiResult rsufiresult = new RSufiResult();
        rsufiresult.setName("RSUFI_RESULTS_" + project.getName());

        addResultView.setRsufiResult(rsufiresult);
        addResultView.getValidatorRSufiResult().setBean(addResultView.getRsufiResult());
        addResultView.setHandler(this);
        addResultView.pack();
        addResultView.setLocationRelativeTo(view);
        addResultView.setVisible(true);
    }

    /**
     * Save new result after clicking "ok" button on SelectionAddResultDialog
     * opened by {@link #showAddResultDialog(SelectionRsufiView)}.
     *
     * @param view view
     */
    public void performAddResult(SelectionAddResultDialog view) {

        setWaitCursor(view);

        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);
        List<File> otherFiles = view.getOtherDataFileListModel().getFiles();

        RSufiResult newResult = view.getRsufiResult();
        try {
            projectService.saveRsufiResults(project, selection, newResult, otherFiles);
            view.dispose();
        } catch (CoserBusinessException ex) {
            JOptionPane.showMessageDialog(view, ex.getMessage(), t("coser.ui.result.saveError"), JOptionPane.ERROR_MESSAGE);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Show rsufi result edit dialog with selected rsufiresult.
     *
     * @param view view
     */
    public void showEditResultDialog(SelectionRsufiView view) {

        // get selected rsufi result
        int selectedRow = view.getSelectionResultsTable().getSelectedRow();
        RSufiResult result = view.getSelectionResultsTableModel().getValue(selectedRow);

        SelectionEditResultDialog editResultView = new SelectionEditResultDialog(view);
        // TODO chatellier 20101121 il ne faudrait pas que ca soit le
        // meme, sinon meme en annulant il est modifié
        editResultView.setRsufiResult(result);
        editResultView.getOtherDataFileListModel().setFiles(new ArrayList<File>(result.getOtherFiles()));
        editResultView.setHandler(this);
        editResultView.pack();
        editResultView.setLocationRelativeTo(view);
        editResultView.setVisible(true);
    }

    /**
     * Save edited result (only save properties file).
     *
     * @param view view
     */
    public void performEditResult(SelectionEditResultDialog view) {
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        RSufiResult editedResult = view.getRsufiResult();
        try {
            List<File> newOthersFile = view.getOtherDataFileListModel().getFiles();
            projectService.editRsufiResults(project, selection, editedResult, newOthersFile);
        } catch (CoserBusinessException ex) {
            JOptionPane.showMessageDialog(view, ex.getMessage(), t("coser.ui.result.saveError"), JOptionPane.ERROR_MESSAGE);
        }
        view.dispose();
    }

    /**
     * Supprime le résultat après confirmation par l'utilisateur.
     *
     * @param view view
     */
    public void deleteResult(SelectionRsufiView view) {
        // get selected rsufi result
        int selectedRow = view.getSelectionResultsTable().getSelectedRow();
        RSufiResult result = view.getSelectionResultsTableModel().getValue(selectedRow);

        int confirm = JOptionPane.showConfirmDialog(view,
                                                    t("coser.ui.result.confirmDeleteResult", result.getName()),
                                                    t("coser.ui.result.resultTitle"), JOptionPane.YES_NO_OPTION,
                                                    JOptionPane.QUESTION_MESSAGE);
        if (confirm == JOptionPane.YES_OPTION) {
            Project project = view.getContextValue(Project.class);
            Selection selection = view.getContextValue(Selection.class);
            ProjectService projectService = view.getContextValue(ProjectService.class);
            try {
                projectService.deleteRSufiResult(project, selection, result);
                // le model ecoute directement le changement sur la liste
                // de resultat (auto refresh)
            } catch (CoserBusinessException ex) {
                throw new CoserException("Can't delete result", ex);
            }
        }
    }

    /**
     * Selectionne le dossier d'extraction.
     *
     * @param view          view
     * @param textComponent text component to set selected directory
     */
    public void selectOutputDirectory(SelectionRsufiView view, JTextField textComponent) {
        // null, pour l'export, il ne faut pas que ca soit
        // dans le dossier des projets
        JFileChooser selectFileChooser = getFileChooserInstance(null);
        selectFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }

    /**
     * Extract data as RSufi format.
     *
     * @param view view
     */
    public void extractRSufiData(SelectionRsufiView view) {

        try {
            setWaitCursor(view);
            ProjectService projectService = view.getContextValue(ProjectService.class);
            Project project = view.getContextValue(Project.class);
            Selection selection = view.getContextValue(Selection.class);

            String directoryPath = view.getResultExtractDataField().getText();
            File directory = new File(directoryPath);

            projectService.extractRSUfiData(project, selection, directory, false);

            JOptionPane.showMessageDialog(view, t("coser.ui.selection.rsufidataextracted"),
                                          t("coser.ui.selection.selectionTitle"), JOptionPane.INFORMATION_MESSAGE);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't extract rsufi files", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Select result file (file only).
     *
     * @param view          view
     * @param textComponent text component to set selected file
     */
    public void selectResultFile(SelectionAddResultDialog view, JTextField textComponent) {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }

    /**
     * Select maps directory (directory only).
     *
     * @param view          view
     * @param textComponent text component to set selected directory
     */
    public void selectMapsDirectory(SelectionAddResultDialog view, JTextField textComponent) {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }

    /**
     * Select other files (file or directory).
     *
     * @param view          view
     * @param listComponent list component to set selected file
     */
    public void selectResultFileOrDirectory(SelectionAddResultDialog view, JList listComponent) {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            view.getOtherDataFileListModel().add(selectedFile);
        }
    }

    /**
     * Select other files (file or directory).
     *
     * @param view          view
     * @param listComponent list component to set selected file
     */
    public void selectResultFileOrDirectory(SelectionEditResultDialog view, JList listComponent) {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            view.getOtherDataFileListModel().add(selectedFile);
        }
    }

    /**
     * Select other files (file or directory).
     *
     * @param view          view
     * @param listComponent list component to set selected file
     */
    public void selectResultFileOrDirectory(SelectionFilesView view, JList listComponent) {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            view.getSelectionFilesListModel().add(selectedFile);
        }
    }

    /**
     * Remove selected file in other file list.
     *
     * @param view
     * @since 1.3
     */
    public void removeSelectedFile(SelectionEditResultDialog view) {
        File selectedFile = (File) view.getOtherDataFileList().getSelectedValue();
        view.getOtherDataFileListModel().remove(selectedFile);
    }

    /**
     * Remove selected file in other file list.
     *
     * @param view
     * @since 1.4
     */
    public void removeSelectedFile(SelectionFilesView view) {
        File selectedFile = (File) view.getSelectionFilesList().getSelectedValue();
        view.getSelectionFilesListModel().remove(selectedFile);
    }

    /**
     * Genere le log des modifications faites lors du control en HTML
     * et l'ouvre dans le navigateur systeme.
     *
     * @param view
     */
    public void displayLogReport(SelectionDetailsView view) {
        PublicationService publicationService = view.getContextValue(PublicationService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);

        File htmlFile = null;
        try {
            htmlFile = publicationService.extractSelectionLogAsHTML(project, selection);
            browseFile(view, htmlFile);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't generate html report", ex);
        }
    }

    /**
     * Affiche la dialog de modification des fichiers d'une selection.
     * (appelée depuis l'onglet rsufi)
     *
     * @param view parent view
     * @since 1.4
     */
    public void showSelectionFilesEdit(SelectionRsufiView view) {
        Selection selection = view.getContextValue(Selection.class);

        SelectionFilesView selectionFilesView = new SelectionFilesView(view);
        selectionFilesView.getSelectionFilesListModel().setFiles(new ArrayList<File>(selection.getOtherFiles()));
        selectionFilesView.setHandler(this);
        selectionFilesView.pack();
        selectionFilesView.setLocationRelativeTo(view);
        selectionFilesView.setVisible(true);
    }

    /**
     * Sauvegarde des options de la selection.
     *
     * @param view view containing option
     */
    public void performSaveSelectionFiles(SelectionFilesView view) {
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getContextValue(Selection.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);

        try {
            List<File> newOthersFile = view.getSelectionFilesListModel().getFiles();
            projectService.editSelectionOptions(project, selection, newOthersFile);
        } catch (CoserBusinessException ex) {
            JOptionPane.showMessageDialog(view, ex.getMessage(), t("coser.ui.result.saveError"), JOptionPane.ERROR_MESSAGE);
        }
        view.dispose();
    }
}

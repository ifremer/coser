/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.control.ControlErrorGroup;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.jdesktop.swingx.treetable.TreeTableNode;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Renderer pour le table des erreurs globales.
 *
 * @author chatellier
 */
public class ControlErrorTreeRenderer extends DefaultTreeCellRenderer {


    private static final long serialVersionUID = -6423364126451874968L;

    protected ImageIcon fatalIcon = null;

    protected ImageIcon errorIcon = null;

    protected ImageIcon warningIcon = null;

    protected ImageIcon infoIcon = null;

    public ControlErrorTreeRenderer() {
        // use icon in jaxx
        fatalIcon = SwingValidatorUtil.getFatalIcon();
        errorIcon = SwingValidatorUtil.getErrorIcon();
        warningIcon = SwingValidatorUtil.getWarningIcon();
        infoIcon = SwingValidatorUtil.getInfoIcon();
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean sel, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        GlobalControlErrorModel model = (GlobalControlErrorModel) tree.getModel();
        JLabel component = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        Object userObject = ((TreeTableNode) value).getUserObject();
        ImageIcon icon = null;
        String text = null;
        String tooltipText = null;

        if (userObject instanceof String) {
            text = t((String) userObject) + " (" + model.getChildCount(value) + ")";
        } else if (userObject instanceof Category) {
            text = t(((Category) userObject).getTranslationKey()) + " (" + model.getChildCount(value) + ")";
        } else if (userObject instanceof ControlErrorGroup) {
            ControlErrorGroup validationErrorGroup = (ControlErrorGroup) userObject;
            ValidationLevel level = validationErrorGroup.getValidationLevel();
            switch (level) {
                case FATAL:
                    icon = fatalIcon;
                    break;
                case ERROR:
                    icon = errorIcon;
                    break;
                case WARNING:
                    icon = warningIcon;
                    break;
                case INFO:
                    icon = infoIcon;
                    break;
            }

            String message = validationErrorGroup.getMessage();
            text = t(message) + " (" + model.getChildCount(value) + ")";
        } else if (userObject instanceof ControlError) {
            ControlError validationError = (ControlError) userObject;
            ValidationLevel level = validationError.getLevel();
            switch (level) {
                case FATAL:
                    icon = fatalIcon;
                    break;
                case ERROR:
                    icon = errorIcon;
                    break;
                case WARNING:
                    icon = warningIcon;
                    break;
                case INFO:
                    icon = infoIcon;
                    break;
            }

            String message = validationError.getDetailMessage();
            if (message == null) {
                message = validationError.getMessage();
            }
            text = t(message);
            tooltipText = validationError.getTipMessage();
        }

        component.setText(text);
        component.setIcon(icon);
        component.setToolTipText(tooltipText);

        return component;
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui;

import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.ui.control.ControlView;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Redo menu action.
 *
 * Currently working only for control view because in selection merge commands
 * are not undoable.
 *
 * Listen for action undone by {@link ContainerUndoMenu} to redo them. When
 * new history commands are done on container, local modification list is
 * cleared (can't be undone).
 *
 * @author chatellier
 * @since 1.2
 */
public class ContainerRedoMenu extends JMenu implements ActionListener, PropertyChangeListener {

    /** serialVersionUID */
    private static final long serialVersionUID = 2104672856535432709L;

    private static final Log log = LogFactory.getLog(ContainerRedoMenu.class);

    protected ControlView controlView;

    /** Ordered from older to newer (must be undo from last to first). */
    protected List<Command> redoableCommands = new ArrayList<Command>();

    public ContainerRedoMenu() {
        setEnabled(false);
    }

    public void setControlView(ControlView controlView) {
        if (this.controlView != null && this.controlView.getControl() != null) {
            this.controlView.getControl().removePropertyChangeListener(AbstractDataContainer.PROPERTY_HISTORY_COMMANDS, this);
        }
        this.controlView = controlView;
        if (this.controlView != null && this.controlView.getControl() != null) {
            this.controlView.getControl().addPropertyChangeListener(AbstractDataContainer.PROPERTY_HISTORY_COMMANDS, this);
        }
        updateSubMenuItems();
    }

    public List<Command> getCommands() {
        return redoableCommands;
    }

    public void setCommands(List<Command> redoableCommands) {
        this.redoableCommands = redoableCommands;
        updateSubMenuItems();
    }

    /**
     * Update submenu items.
     */
    protected void updateSubMenuItems() {

        if (log.isDebugEnabled()) {
            log.debug("Refresh redo menu items");
        }

        removeAll();
        boolean menuEnabled = false;

        if (controlView != null && controlView.getControl() != null) {
            Control control = controlView.getControl();
            Project project = controlView.getContextValue(Project.class);
            menuEnabled = !redoableCommands.isEmpty();

            // command in reverse order (only 10 last)
            for (int i = redoableCommands.size() - 1; i >= 0 && i > redoableCommands.size() - 10; i--) {
                Command command = redoableCommands.get(i);
                JMenuItem commandMenu = new JMenuItem(command.getDescription(project, control));
                commandMenu.setActionCommand(String.valueOf(i));
                commandMenu.addActionListener(this);
                add(commandMenu);
            }
        }

        setEnabled(menuEnabled);
    }

    /*
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        redoableCommands.clear();
        updateSubMenuItems();
    }

    /*
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        int commandIndex = Integer.parseInt(actionCommand);
        if (log.isDebugEnabled()) {
            log.debug("Redo command index " + commandIndex);
        }

        List<Command> commands = new ArrayList<Command>();
        List<Command> newRedoCommands = new ArrayList<Command>(redoableCommands);
        for (int i = redoableCommands.size() - 1; i >= commandIndex; i--) {
            Command command = redoableCommands.get(i);
            commands.add(command);
            newRedoCommands.remove(command);
        }
        controlView.getHandler().redoCommands(controlView, commands);

        // update redoable command list
        // a gerer completement sinon, avec les event du control, on perd tout.
        setCommands(newRedoCommands);
    }
}

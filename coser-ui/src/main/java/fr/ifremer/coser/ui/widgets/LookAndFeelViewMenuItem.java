/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.widgets;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Look and feel {@link JMenu}.
 *
 * External listener can listen for {@link #PROPERTY_LOOK_AND_FEEL} property
 * modification.
 *
 * Code copied form SwingSet3.
 *
 * @author chatellier
 */
public class LookAndFeelViewMenuItem extends JMenu implements ActionListener {


    private static final long serialVersionUID = 2462162272156484217L;

    public static final String PROPERTY_LOOK_AND_FEEL = "lookAndFeel";

    protected String currentLookAndFeel;

    public LookAndFeelViewMenuItem() {
        // Look for toolkit look and feels first
        UIManager.LookAndFeelInfo lookAndFeelInfos[] = UIManager.getInstalledLookAndFeels();
        currentLookAndFeel = UIManager.getLookAndFeel().getClass().getName();
        ButtonGroup lookAndFeelRadioGroup = new ButtonGroup();
        for (UIManager.LookAndFeelInfo lafInfo : lookAndFeelInfos) {
            JRadioButtonMenuItem lafItem = new JRadioButtonMenuItem();
            lafItem.setSelected(lafInfo.getClassName().equals(currentLookAndFeel));
            lafItem.setHideActionText(true);
            lafItem.addActionListener(this);
            lafItem.setText(lafInfo.getName());
            lafItem.setActionCommand(lafInfo.getClassName());
            lookAndFeelRadioGroup.add(lafItem);
            add(lafItem);
        }
    }

    /*
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        String lookAndFeel = event.getActionCommand();
        try {
            String oldLookAndFeel = this.currentLookAndFeel;
            if (!oldLookAndFeel.equals(lookAndFeel)) {
                UIManager.setLookAndFeel(lookAndFeel);
                this.currentLookAndFeel = lookAndFeel;
                updateLookAndFeel();
                // for external listener to listen for modification
                firePropertyChange(PROPERTY_LOOK_AND_FEEL, oldLookAndFeel, lookAndFeel);
            }
        } catch (Exception ex) {
            throw new RuntimeException("Can't install selected look and feel : " + lookAndFeel, ex);
        }
    }

    /**
     * Update all UIs.
     */
    protected void updateLookAndFeel() {
        Window windows[] = Frame.getWindows();

        for (Window window : windows) {
            SwingUtilities.updateComponentTreeUI(window);
        }
    }
}

/* %%Ignore-License
 * Copyright (C) 2007 Craig Knudsen
 *
 * AccordionPane is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License can be found at www.gnu.org. 
 * To receive a hard copy, you can write to:
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307 USA.
 */

package fr.ifremer.coser.ui.widgets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class AccordionPaneSubPanel extends JPanel {


    private static final long serialVersionUID = 4836598811061630517L;

    protected AccordionPane accordionPane;

    protected JPanel titlePanel;

    protected JLabel titleLabel;

    protected JComponent child;

    protected int index;

    protected Cursor handCursor = null;

    protected int mousePressedInd = -1;

    public AccordionPaneSubPanel() {
        this.setBorder(BorderFactory.createEtchedBorder());
        if (handCursor == null) {
            handCursor = new Cursor(Cursor.HAND_CURSOR);
        }

        this.setLayout(new BorderLayout());
    }

    public void setAccordionPane(AccordionPane accordionPane) {
        this.accordionPane = accordionPane;

        if (titlePanel != null) {
            this.titlePanel.setBackground(accordionPane.paneBackgroundColor);
        }
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public JPanel getTitlePanel() {
        return titlePanel;
    }

    public JLabel getTitleLabel() {
        return titleLabel;
    }

    public void setTitle(String title) {
        this.titleLabel = new JLabel(title);
        this.titlePanel = new JPanel();
        //this.titlePanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 3));
        this.titlePanel.setBorder(BorderFactory.createRaisedBevelBorder());
        this.titlePanel.setLayout(new BorderLayout());
        this.titlePanel.add(this.titleLabel, BorderLayout.WEST);
        if (accordionPane != null) {
            this.titlePanel.setBackground(accordionPane.paneBackgroundColor);
        }
        // Set cursor to hand cursor to indicate the user can select the title area
        titlePanel.setCursor(handCursor);
        this.add(titlePanel, BorderLayout.NORTH);

        // Add mouse listener for titlePanel so we can change the cursor and also
        // listen for mouse click.
        this.titlePanel.addMouseListener(new MouseListener() {

            public void mouseEntered(MouseEvent me) {
            }

            public void mouseExited(MouseEvent me) {
            }

            public void mouseClicked(MouseEvent me) {
                if (accordionPane.selected != index) {
                    accordionPane.setSelected(index);
                }
            }

            public void mousePressed(MouseEvent me) {
                mousePressedInd = index;
            }

            public void mouseReleased(MouseEvent me) {
                if (mousePressedInd == index) {
                    if (accordionPane.selected != index) {
                        accordionPane.setSelected(index);
                    }
                }
                mousePressedInd = -1;
            }
        });

    }

    public Component add(Component child) {
        // child.setVisible ( false );
        this.add(child, BorderLayout.CENTER);
        //validate();
        //repaint();
        return child;
    }
}
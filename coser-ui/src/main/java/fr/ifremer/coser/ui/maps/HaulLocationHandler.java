/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.maps;

import com.bbn.openmap.layer.location.AbstractLocationHandler;
import com.bbn.openmap.layer.location.BasicLocation;
import com.bbn.openmap.layer.location.Location;
import com.bbn.openmap.layer.location.LocationLayer;
import com.bbn.openmap.omGraphics.OMGraphicList;
import fr.ifremer.coser.util.Coordinate;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

/**
 * Handler used by {@link LocationLayer} to provide haul's locations.
 *
 * @author chatellier
 */
public class HaulLocationHandler extends AbstractLocationHandler {

    protected static Color[] markerColors = new Color[]{
            Color.RED,
            Color.BLUE,
            Color.CYAN,
            Color.GRAY,
            Color.BLACK,
            Color.GREEN,
            Color.GREEN.darker(),
            Color.MAGENTA,
            Color.PINK,
            Color.YELLOW,
            new Color(165, 3, 63) // purple
    };

    protected List<Coordinate> coordinates;

    public HaulLocationHandler(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
        setShowLocations(true);
    }

    /*
     * @see com.bbn.openmap.layer.location.LocationHandler#get(float, float, float, float, com.bbn.openmap.omGraphics.OMGraphicList)
     */
    @Override
    public OMGraphicList get(float nwLat, float nwLon, float seLat, float seLon, OMGraphicList graphicList) {

        for (Coordinate coordinate : coordinates) {

            // get color index in color array depending on serie
            int serie = coordinate.getSerie();
            serie = serie % markerColors.length;

            // use html for multiple line tooltip
            String htmlName = "<html>" + coordinate.getName().replaceAll("\n", "<br>") + "</html>";
            Location location = new BasicLocation(coordinate.getLatitude(), coordinate.getLongitude(), htmlName, null);
            location.setLocationHandler(this);
            location.getLocationMarker().setLinePaint(markerColors[serie]);
            graphicList.add(location);
        }

        return graphicList;
    }

    /*
     * @see com.bbn.openmap.layer.location.LocationHandler#getItemsForPopupMenu(com.bbn.openmap.layer.location.Location)
     */
    @Override
    public List<Component> getItemsForPopupMenu(Location arg0) {
        return null;
    }

    /*
     * @see com.bbn.openmap.layer.location.LocationHandler#reloadData()
     */
    @Override
    public void reloadData() {

    }
}

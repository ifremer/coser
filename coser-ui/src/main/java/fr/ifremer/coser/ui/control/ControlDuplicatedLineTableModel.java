/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.control.ControlError;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Table model that change line order to display duplicated line side by side.
 *
 * L'implementation reside sur la construction d'un tableau associatif qui
 * associe les numeros demandé par la vue à d'autre numero différents qui
 * correspondent au données du modèle réel.
 * La précédente implémentation utilisait des ArrayList, mais n'était pas
 * performante vis à vis de la construction de la liste.
 *
 * @author chatellier
 */
public class ControlDuplicatedLineTableModel extends ControlTableModel {


    private static final long serialVersionUID = 8138379018849263395L;

    private static final Log log = LogFactory.getLog(ControlDuplicatedLineTableModel.class);

    protected ControlTableModel dataTableModel;

    /** List des erreurs de validation (non null seulement apres validation). */
    protected List<ControlError> controlErrors;

    protected int[] linesIndex = null;

    protected int[] reverseLinesIndex = null;

    @Override
    public Category getCategory() {
        return dataTableModel.getCategory();
    }

    public ControlDuplicatedLineTableModel(ControlTableModel dataTableModel) {
        this.dataTableModel = dataTableModel;
    }

    public void setControlErrors(List<ControlError> controlErrors) {
        this.controlErrors = controlErrors;
        computeLineIndices();
        fireTableDataChanged();
    }

    /**
     * Compute line indices.
     *
     * This algorithm has been optimized three times.
     * Be carefull of performances when modify it, can take looooong times
     * without optimisation.
     */
    protected void computeLineIndices() {

        long timeBefore = System.currentTimeMillis();
        int dataCount = getRowCount();

        // we need to have a smaller list for performance reason
        // next loop is while into another while
        List<ControlError> localControlErrors = getCurrentModelErrors();

        // build indexed array
        linesIndex = new int[dataCount];
        int i = 0;
        int j = 0;
        Set<Integer> alreadyDone = new HashSet<Integer>();

        // look for first error line to manage in main loop
        Iterator<ControlError> itControlErrors = localControlErrors.iterator();
        int nextErrorFirstLine = -1;
        ControlError controlError = null;
        if (itControlErrors.hasNext()) {
            controlError = itControlErrors.next();
            // can return -1 here, after control cannot contains deleted lines
            nextErrorFirstLine = dataTableModel.getRealIndexOfLine(controlError.getLineNumbers().get(0));
        }

        // main loop
        while (i < dataCount) {
            while (alreadyDone.contains(j)) {
                j++;
            }
            linesIndex[i++] = j;
            if (j == nextErrorFirstLine) {
                for (int errorIndex = 1; errorIndex < controlError.getLineNumbers().size(); ++errorIndex) {
                    // can return -1 here, after control cannot contains deleted lines
                    int delegateIndex = dataTableModel.getRealIndexOfLine(controlError.getLineNumbers().get(errorIndex));
                    linesIndex[i++] = delegateIndex;
                    alreadyDone.add(delegateIndex);
                }

                // remove it, useless for next iteration
                itControlErrors.remove();

                // look for next error line to manage in main loop
                if (itControlErrors.hasNext()) {
                    controlError = itControlErrors.next();
                    // can return -1 here, after control cannot contains deleted lines
                    nextErrorFirstLine = dataTableModel.getRealIndexOfLine(controlError.getLineNumbers().get(0));
                } // no else j == nextErrorFirstLine will become false
            }
            j++;
        }

        // build reverse array (for method getRealIndexOfLine)
        reverseLinesIndex = new int[dataCount];
        for (int reversei = 0; reversei < dataCount; ++reversei) {
            reverseLinesIndex[linesIndex[reversei]] = reversei;
        }

        if (log.isDebugEnabled()) {
            long timeAfter = System.currentTimeMillis();
            log.debug("Build new order array took " + (timeAfter - timeBefore) + " ms");
        }
    }

    /**
     * Parse input control errors list, an build a new list with only errors
     * that will be used by this model.
     *
     * @return new error list for model only
     */
    protected List<ControlError> getCurrentModelErrors() {
        Category category = getCategory();
        // linked list because no access with index
        List<ControlError> resultControlErrors = new LinkedList<ControlError>();
        for (ControlError controlError : controlErrors) {
            if (CollectionUtils.size(controlError.getLineNumbers()) >= 2 && category.equals(controlError.getCategory())) {
                resultControlErrors.add(controlError);
            }
        }
        return resultControlErrors;
    }

    /*
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return dataTableModel.getRowCount();
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return dataTableModel.getColumnCount();
    }

    /*
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(int column) {
        return dataTableModel.getColumnName(column);
    }

    /**
     * Look for modified order row index.
     *
     * @param rowIndex initial row index
     * @return modified row index
     */
    protected int getModifiedRowIndex(int rowIndex) {
        int dupliRowIndex = rowIndex;
        if (linesIndex != null) {
            dupliRowIndex = linesIndex[rowIndex];
        }
        return dupliRowIndex;
    }

    /*
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return dataTableModel.getValueAt(getModifiedRowIndex(rowIndex), columnIndex);
    }

    /*
     * @see fr.ifremer.coser.ui.control.ControlTableModel#getDataAt(int)
     */
    @Override
    public String[] getDataAt(int rowIndex) {
        return dataTableModel.getDataAt(getModifiedRowIndex(rowIndex));
    }

    /**
     * Retourne l'index réel de la ligne dans la vue.
     *
     * Ne doit être utilisé que pour manipuler le modele de selection
     * et non appeler derriere getValueAt ou getDataAt (pas de sens).
     */
    @Override
    public int getRealIndexOfLine(String lineNumber) {
        int rowIndex = dataTableModel.getRealIndexOfLine(lineNumber);
        // can return -2 if line has been deleted
        if (rowIndex >= 0 && reverseLinesIndex != null) {
            rowIndex = reverseLinesIndex[rowIndex];
        }
        return rowIndex;
    }

    /**
     * Update linesIndex array to remove an index of a line.
     * This method WONT work for addition, just deletion.
     *
     * @param lineIndex index of line to delete
     */
    protected void deleteLine(int lineIndex) {
        int dataCount = getRowCount();
        int currentVal = linesIndex[lineIndex];

        // before index
        for (int bIndex = 0; bIndex < lineIndex; ++bIndex) {
            if (linesIndex[bIndex] > currentVal) {
                linesIndex[bIndex] = linesIndex[bIndex] - 1;
            }
        }
        // after index
        // attention, il faut bien le <= ici, pour aller au dela
        // du tableau et que l'element soit bien déplacé
        // via aIndex - 1 dans le bon tableau
        for (int aIndex = lineIndex + 1; aIndex <= dataCount; ++aIndex) {
            if (linesIndex[aIndex] > currentVal) {
                linesIndex[aIndex - 1] = linesIndex[aIndex] - 1;
            } else {
                linesIndex[aIndex - 1] = linesIndex[aIndex];
            }
        }

        // update reverse array indices
        for (int reversei = 0; reversei < dataCount; ++reversei) {
            reverseLinesIndex[linesIndex[reversei]] = reversei;
        }
    }

    @Override
    public int findColumn(String columnName) {
        return dataTableModel.findColumn(columnName);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return dataTableModel.getColumnClass(columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return dataTableModel.isCellEditable(getModifiedRowIndex(rowIndex), columnIndex);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        dataTableModel.setValueAt(aValue, getModifiedRowIndex(rowIndex), columnIndex);
    }

    @Override
    public void fireTableDataChanged() {
        dataTableModel.fireTableDataChanged();
    }

    @Override
    public void fireTableStructureChanged() {
        dataTableModel.fireTableStructureChanged();
    }

    @Override
    public void fireTableRowsInserted(int firstRow, int lastRow) {
        dataTableModel.fireTableRowsUpdated(firstRow, lastRow);
    }

    @Override
    public void fireTableRowsUpdated(int firstRow, int lastRow) {
        dataTableModel.fireTableRowsUpdated(firstRow, lastRow);
    }

    @Override
    public void fireTableRowsDeleted(int firstRow, int lastRow) {
        // fire before to get row count return correct value
        dataTableModel.fireTableRowsDeleted(firstRow, lastRow);

        // then delete line in current model
        if (linesIndex != null) {
            for (int lineIndex = firstRow; lineIndex <= lastRow; ++lineIndex) {
                deleteLine(lineIndex);
            }
        }
    }

    @Override
    public void fireTableCellUpdated(int row, int column) {
        dataTableModel.fireTableCellUpdated(row, column);
    }

    @Override
    public void fireTableChanged(TableModelEvent e) {
        dataTableModel.fireTableChanged(e);
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        dataTableModel.addTableModelListener(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        dataTableModel.removeTableModelListener(l);
    }

    @Override
    public TableModelListener[] getTableModelListeners() {
        return dataTableModel.getTableModelListeners();
    }

    @Override
    public <T extends EventListener> T[] getListeners(Class<T> listenerType) {
        return dataTableModel.getListeners(listenerType);
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.common;

import fr.ifremer.coser.ui.control.ControlHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Common application handler.
 *
 * @author chatellier
 */
public class CommonHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ControlHandler.class);

    /** File chooser instance. */
    protected JFileChooser fileChooserInstance;

    /**
     * Retourne une unique instance du file chooser pour conserver
     * le répertoire de sélection d'un appel sur l'autre.
     *
     * @param currentDirectory current directory
     * @return file chooser
     */
    protected JFileChooser getFileChooserInstance(File currentDirectory) {
        if (fileChooserInstance == null) {
            fileChooserInstance = new JFileChooser();
            fileChooserInstance.setCurrentDirectory(currentDirectory);
        }
        return fileChooserInstance;
    }

    /**
     * Install le curseur sablier.
     *
     * @param comp component to set cursor
     */
    public void setWaitCursor(Component comp) {
        comp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    /**
     * Repositionne le curseur a sa valeur par defaut.
     *
     * @param comp component to set cursor
     */
    public void setDefaultCursor(Component comp) {
        comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    /**
     * Ouvre un fichier dans le navigateur systeme.
     *
     * @param parentComponent parent component
     * @param report          report to open
     */
    protected void browseFile(Component parentComponent, File report) {
        try {
            Desktop.getDesktop().browse(report.toURI());
        } catch (IOException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't open generated report at " + report.getAbsolutePath(), ex);
            }
            JOptionPane.showMessageDialog(parentComponent, t("coser.ui.error.htmlReportCantBeOpened", report.getAbsolutePath()),
                                          t("coser.ui.error.htmlReportError"), JOptionPane.WARNING_MESSAGE);
        }
    }
}

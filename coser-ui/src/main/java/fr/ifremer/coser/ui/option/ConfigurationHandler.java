/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.option;

import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.ui.common.CommonHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import java.awt.Frame;
import java.awt.Window;
import java.io.File;

/**
 * Handler for {@link ConfigurationView}.
 *
 * @author chatellier
 */
public class ConfigurationHandler extends CommonHandler {

    private static final Log log = LogFactory.getLog(ConfigurationHandler.class);

    /**
     * Display user directory selection dialog and fill given text component
     * with given file.
     *
     * @param configurationView config view
     * @param textComponent     text component to fill
     */
    public void selectInputDirectory(ConfigurationView configurationView, JTextField textComponent) {
        CoserConfig config = configurationView.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getDatabaseDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = selectFileChooser.showOpenDialog(configurationView);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }

    /**
     * Display user file selection dialog and fill given text component
     * with given file.
     *
     * @param configurationView config view
     * @param textComponent     text component to fill
     */
    public void selectInputFile(ConfigurationView configurationView, JTextField textComponent) {
        CoserConfig config = configurationView.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getDatabaseDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int result = selectFileChooser.showOpenDialog(configurationView);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }

    /**
     * Save configuration.
     *
     * @param configurationView
     */
    public void saveConfiguration(ConfigurationView configurationView) {
        CoserConfig config = configurationView.getContextValue(CoserConfig.class);

        // path
        config.setProjectsDirectory(configurationView.getProjectDirectoryField().getText());
        config.setValidatorsDirectory(configurationView.getValidatorDirectoryField().getText());
        config.setReferenceSpeciesPath(configurationView.getReferenceSpeciesField().getText());
        config.setReferenceTypeEspecesPath(configurationView.getReferenceTypeEspecesField().getText());
        config.setWebZonesFile(configurationView.getWebZonesField().getText());
        config.setWebFrontEnd(configurationView.getWebFrontEndField().getText());

        // control / selection
        try {
            config.setControlNobsmin(Double.parseDouble(configurationView.getControlNobsMinField().getText()));
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse number", ex);
            }
        }
        try {
            config.setControlDiffCatchLength(Double.parseDouble(configurationView.getControlDiffCatchLengthField().getText()));
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse number", ex);
            }
        }
        config.setControlTypeFish(configurationView.getControlTypeFishField().getText());
        try {
            config.setStandardDeviationToAverage(Integer.parseInt(configurationView.getControlStandardDeviationField().getText()));
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse number", ex);
            }
        }
        try {
            config.setSelectionDensityFilter(Double.parseDouble(configurationView.getSelectionFilterDensityField().getText()));
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse number", ex);
            }
        }
        try {
            config.setSelectionOccurrenceFilter(Double.parseDouble(configurationView.getSelectionFilterOccurrenceField().getText()));
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse number", ex);
            }
        }

        // misc
        config.setSmtpHost(configurationView.getSmtpHostField().getText());
        config.setSupportEmail(configurationView.getSupportEmailField().getText());
        try {
            config.setSwingFontSize(Integer.parseInt((String) configurationView.getSwingFontSizeField().getSelectedItem()));
        } catch (NumberFormatException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse number", ex);
            }
        }

        // save
        config.saveForUser();

        // close
        configurationView.dispose();

        // update font size (font size)
        Window windows[] = Frame.getWindows();
        for (Window window : windows) {
            SwingUtilities.updateComponentTreeUI(window);
        }
    }
}

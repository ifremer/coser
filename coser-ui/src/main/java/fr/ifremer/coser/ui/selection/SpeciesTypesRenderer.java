/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection;

import fr.ifremer.coser.ui.selection.model.SpeciesTypesListModel;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.renderer.DefaultListRenderer;

import javax.swing.JList;
import java.awt.Component;

/**
 * Specy type renderer (with comment display when available).
 *
 * @author chatellier
 */
public class SpeciesTypesRenderer extends DefaultListRenderer {


    private static final long serialVersionUID = -6218097608544949959L;

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {

        String species = (String) value;
        SpeciesTypesListModel model = (SpeciesTypesListModel) list.getModel();

        String comment = model.getComment(species);
        String stringValue = null;
        if (StringUtils.isEmpty(comment)) {
            stringValue = species;
        } else {
            stringValue = species + " (" + comment + ")";
        }

        return super.getListCellRendererComponent(list, stringValue, index, isSelected, cellHasFocus);
    }

}

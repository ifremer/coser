/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui;

import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.Selection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Selection list menu item. Display dynamic entries for project selection.
 *
 * @author chatellier
 */
public class SelectionsListMenu extends JMenu implements ActionListener, PropertyChangeListener {

    private static final long serialVersionUID = -3528302058982208907L;

    private static final Log log = LogFactory.getLog(SelectionsListMenu.class);

    protected CoserFrame view;

    protected Project project;

    public SelectionsListMenu(CoserFrame view) {
        this.view = view;
        updateMenuContent();
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        if (this.project != null) {
            this.project.removePropertyChangeListener(Project.PROPERTY_SELECTIONS, this);
            this.project.getControl().removePropertyChangeListener(Control.PROPERTY_VALIDATED, this);
        }
        this.project = project;
        if (this.project != null) {
            this.project.addPropertyChangeListener(Project.PROPERTY_SELECTIONS, this);
            this.project.getControl().addPropertyChangeListener(Control.PROPERTY_VALIDATED, this);
        }
        updateMenuContent();
    }

    protected void updateMenuContent() {
        removeAll();

        if (log.isDebugEnabled()) {
            log.debug("Refresh selection menu items");
        }

        if (project != null) {

            if (!project.getControl().isValidated()) {
                JMenuItem menuItem = new JMenuItem(t("coser.ui.mainframe.menu.data.noValidation"));
                menuItem.setFont(menuItem.getFont().deriveFont(Font.ITALIC));
                menuItem.setEnabled(false);
                add(menuItem);
            } else {
                Map<String, Selection> selections = project.getSelections();

                if (selections == null || selections.isEmpty()) {
                    JMenuItem menuItem = new JMenuItem(t("coser.ui.mainframe.menu.data.noSelection"));
                    menuItem.setFont(menuItem.getFont().deriveFont(Font.ITALIC));
                    menuItem.setEnabled(false);
                    add(menuItem);
                } else {
                    for (String selectionName : selections.keySet()) {
                        // new selection
                        JMenuItem menuItem = new JMenuItem(selectionName);
                        menuItem.setActionCommand(selectionName);
                        menuItem.addActionListener(this);
                        add(menuItem);
                    }
                }

                // seperator
                add(new JSeparator());

                // new selection
                JMenuItem newMenuItem = new JMenuItem(t("coser.ui.mainframe.menu.data.newSelection"));
                // les chaines ne doivent pas poser pb, on ne peut pas
                // avoir de selection avec espace
                newMenuItem.setActionCommand("$new selection$");
                newMenuItem.addActionListener(this);
                add(newMenuItem);

                // replay selection
                JMenuItem replayMenuItem = new JMenuItem(t("coser.ui.mainframe.menu.data.replaySelection"));
                // les chaines ne doivent pas poser pb, on ne peut pas
                // avoir de selection avec espace
                replayMenuItem.setActionCommand("$replay selection$");
                replayMenuItem.addActionListener(this);
                add(replayMenuItem);
            }
        }
    }

    /*
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        // new selection case
        if ("$new selection$".equals(actionCommand)) {
            view.getHandler().showSelectionView();
        } else if ("$replay selection$".equals(actionCommand)) {
            view.getHandler().replaySelection();
        } else {
            view.getHandler().showSelectionView(actionCommand);
        }
    }

    /*
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        // quand la liste de selection change
        // ou la validation du controle
        updateMenuContent();
    }
}

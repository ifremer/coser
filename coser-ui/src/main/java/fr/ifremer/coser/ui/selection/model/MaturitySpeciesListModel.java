/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.model;

import fr.ifremer.coser.ui.util.CoserListModel;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Specy list model with size all year.
 *
 * @author chatellier
 */
public class MaturitySpeciesListModel extends AbstractListModel implements ListSelectionListener, CoserListModel {


    private static final long serialVersionUID = -4769109927915812519L;

    protected List<String> species = new ArrayList<String>();

    protected Collection<String> maturitySpecies;

    public List<String> getSpecies() {
        return species;
    }

    public void setSpecies(List<String> species) {
        this.species = new ArrayList<String>(species);
        this.species.retainAll(maturitySpecies);
        fireContentsChanged(this, 0, this.species.size());
    }

    public void setMaturitySpecies(Collection<String> maturitySpecies) {
        this.maturitySpecies = maturitySpecies;
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        int result = 0;
        if (species != null) {
            result = species.size();
        }
        return result;
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return species.get(index);
    }


    /*
     * @see fr.ifremer.coser.ui.util.CoserListModel#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(Object o) {
        return species.indexOf(o);
    }

    /*
     * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
     */
    @Override
    public void valueChanged(ListSelectionEvent event) {

        if (!event.getValueIsAdjusting()) {
            JList source = (JList) event.getSource();
            Object[] selectedValues = source.getSelectedValues();
            species.clear();
            for (Object selectedValue : selectedValues) {
                String specy = (String) selectedValue;
                if (maturitySpecies.contains(specy)) {
                    species.add(specy);
                }
            }
            fireContentsChanged(this, 0, species.size() - 1);
        }
    }
}

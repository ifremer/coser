/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.util;

import javax.swing.ListModel;

/**
 * Interface que doivent implementer les modeles de List de coser.
 * (à ne pas confondre avec les modeles de selection des List).
 *
 * @author chatellier
 */
public interface CoserListModel extends ListModel {

    /**
     * Return index of element.
     *
     * @param element element to get index
     * @return index of element
     */
    int indexOf(Object element);
}

package fr.ifremer.coser.ui.publication;

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coser.bean.GlobalResult;
import fr.ifremer.coser.bean.RSufiResultPath;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;

/**
 * Created on 3/17/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GlobalResultRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {

        Object localValue = value;
        if (value instanceof GlobalResult) {
            GlobalResult g = (GlobalResult) value;
            if (g.isRsufi()) {
                RSufiResultPath rsufiResultPath = g.getRsufiProject();
                localValue = rsufiResultPath.getProject().getName() + "/" +
                             rsufiResultPath.getSelection().getName() + "/" +
                             rsufiResultPath.getRsufiResult().getName();
            } else {
                localValue = g.getEchobaseProject().getName();
            }
        }
        return super.getTableCellRendererComponent(table, localValue, isSelected, hasFocus,
                                                   row, column);
    }
}

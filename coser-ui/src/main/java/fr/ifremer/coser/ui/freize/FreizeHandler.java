/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.coser.ui.freize;

import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.ui.CoserFrame;
import fr.ifremer.coser.ui.SelectionsListMenu;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.nuiton.i18n.I18n.t;


/**
 * Panel affichant l'état d'avancement du dans la definition sous forme de frise.
 *
 * @author echatellier
 */
public class FreizeHandler implements PropertyChangeListener {

    private static final Log log = LogFactory.getLog(FreizeHandler.class);

    protected static final Icon OK_ICON = new ImageIcon(FreizeHandler.class.getResource("/icons/button_ok.png"));

    protected static final Icon LOCKED_ICON = new ImageIcon(FreizeHandler.class.getResource("/icons/stock_lock.png"));

    protected static final Icon BAD_ICON = new ImageIcon(FreizeHandler.class.getResource("/icons/agt_action_fail.png"));

    protected static final Color COMPLETE_COLOR = Color.GREEN;

    protected static final Color PENDING_COLOR = Color.YELLOW;

    protected static final Color DISABLED_COLOR = null;

    protected FreizeModel model;

    protected Freize view;

    public FreizeHandler(Freize view) {
        this.view = view;
    }

    public void setModel(FreizeModel model) {
        if (this.model != null) {
            this.model.removePropertyChangeListener(this);
        }
        this.model = model;
        if (this.model != null) {
            this.model.addPropertyChangeListener(this);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();

        if (log.isDebugEnabled()) {
            log.debug("Update freize : " + propertyName);
        }

        if (FreizeModel.PROPERTY_PROJECT.equals(propertyName)) {
            Project oldProject = (Project) evt.getOldValue();
            if (oldProject != null) {
                oldProject.getControl().removePropertyChangeListener(this);
            }
            Project newProject = (Project) evt.getNewValue();
            if (newProject != null) {
                newProject.getControl().addPropertyChangeListener(this);
            }
            updateUi();
        } else if (FreizeModel.PROPERTY_SELECTION.equals(propertyName)) {
            Selection oldSelection = (Selection) evt.getOldValue();
            if (oldSelection != null) {
                oldSelection.removePropertyChangeListener(this);
            }
            Selection newSelection = (Selection) evt.getNewValue();
            if (newSelection != null) {
                newSelection.addPropertyChangeListener(this);
            }
            updateUi();
        } else if (Selection.PROPERTY_RSUFI_RESULTS.equals(propertyName)) {
            updateUi();
        } else if (Control.PROPERTY_VALIDATED.equals(propertyName) ||
                   Selection.PROPERTY_VALIDATED.equals(propertyName)) {
            // c'est les même propriétés, mais ca ne pose pas de pbs
            updateUi();
        }
    }

    /**
     * Met à jour le visuel de la frise.
     */
    protected void updateUi() {

        // init with default values
        model.setStep1Enabled(false);
        model.setStep2Enabled(false);
        model.setStep3Enabled(false);

        view.getStep0Panel().setBackground(DISABLED_COLOR);
        view.getStep1Panel().setBackground(DISABLED_COLOR);
        view.getStep2Panel().setBackground(DISABLED_COLOR);
        view.getStep3Panel().setBackground(DISABLED_COLOR);
        view.getStep4Panel().setBackground(DISABLED_COLOR);

        Project project = model.getProject();
        // step 0
        if (project != null) {
            view.getStep0Panel().setBackground(COMPLETE_COLOR);

            // step 1
            model.setStep1Enabled(true);
            if (project.getControl().isValidated()) {
                view.getStep1Panel().setBackground(COMPLETE_COLOR);

                // step 2
                model.setStep2Enabled(true);
                if (!project.getSelections().isEmpty()) {
                    view.getControlStatus().setText(t("coser.ui.freize.locked"));
                    view.getControlStatus().setToolTipText(t("coser.ui.freize.controllockedtip"));
                    view.getControlStatus().setIcon(LOCKED_ICON);
                } else {
                    view.getControlStatus().setText(t("coser.ui.freize.validated"));
                    view.getControlStatus().setIcon(OK_ICON);
                }

                Selection selection = model.getSelection();
                if (selection != null && selection.isValidated()) {
                    view.getSelectionStatus().setIcon(OK_ICON);
                    view.getSelectionStatus().setText(t("coser.ui.freize.validated"));
                    view.getStep2Panel().setBackground(COMPLETE_COLOR);

                    // step 3
                    model.setStep3Enabled(true);
                    view.getStep3Panel().setBackground(COMPLETE_COLOR);

                    // step 4
                    if (selection.getRsufiResults().isEmpty()) {
                        view.getStep4Panel().setBackground(PENDING_COLOR);
                    } else {
                        view.getStep4Panel().setBackground(COMPLETE_COLOR);
                    }

                } else {
                    view.getStep2Panel().setBackground(PENDING_COLOR);
                    view.getSelectionStatus().setIcon(BAD_ICON);
                    view.getSelectionStatus().setText(t("coser.ui.freize.unvalidated"));
                }
            } else {
                view.getStep1Panel().setBackground(PENDING_COLOR);
                view.getControlStatus().setIcon(BAD_ICON);
                view.getControlStatus().setText(t("coser.ui.freize.unvalidated"));
            }
        } else {
            view.getStep0Panel().setBackground(PENDING_COLOR);
        }
    }

    /**
     * Affiche en popup l'equivalent du menu "Données" > "Selection".
     *
     * @param view view
     */
    public void showSelectionDropDownMenu(Freize view, MouseEvent event) {
        JPopupMenu menu = new JPopupMenu();

        SelectionsListMenu oldMenu = new SelectionsListMenu(view.getParentContainer(CoserFrame.class));
        oldMenu.setProject(view.getModel().getProject());

        // lorsqu'on ajoute un element a menu, il est supprimé
        // de oldMenu, donc le parcourt doit se faire en sens inverse
        for (int i = oldMenu.getItemCount() - 1; i >= 0; i--) {
            menu.insert(oldMenu.getMenuComponent(i), 0);
        }

        menu.show((Component) event.getSource(), event.getX(), event.getY());
    }
}

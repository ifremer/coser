/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserConstants.Category;

import javax.swing.table.AbstractTableModel;

/**
 * Common interface for all data table model used in control view.
 *
 * @author chatellier
 */
public abstract class ControlTableModel extends AbstractTableModel {


    private static final long serialVersionUID = -2010447483214407634L;

    /**
     * Return category managed by model.
     *
     * @return model category
     */
    public abstract Category getCategory();

    /**
     * Retourne la donnée (toutes le String[]) à la ligne demandée.
     *
     * @param rowIndex
     * @return String[]
     */
    public abstract String[] getDataAt(int rowIndex);

    /**
     * Retourne l'index dans la liste des données du numero de ligne
     * demandé.
     *
     * @param lineNumber le numero de données
     * @return l'index
     */
    public abstract int getRealIndexOfLine(String lineNumber);
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.project;

import fr.ifremer.coser.bean.SpeciesFieldType;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * List renderer used to render {@link SpeciesFieldType} enum values with
 * additionnal comment.
 *
 * @author echatellier
 * @since 1.3
 */
public class SpeciesFieldTypeListRenderer extends DefaultListCellRenderer {


    private static final long serialVersionUID = 6335214555392070266L;

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {

        SpeciesFieldType type = (SpeciesFieldType) value;
        String text = type.getReftaxField();

        // add another human comment
        switch (type) {
            case C_Valide:
                text += " (" + t("coser.ui.project.cValideSpeciesTypeDesc") + ")";
                break;
            case C_PERM:
                text += " (" + t("coser.ui.project.cPermSpeciesTypeDesc") + ")";
                break;
            case L_Valide:
                text += " (" + t("coser.ui.project.lValideSpeciesTypeDesc") + ")";
                break;
        }

        return super.getListCellRendererComponent(list, text, index, isSelected,
                                                  cellHasFocus);
    }

}

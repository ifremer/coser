/* %%Ignore-License
 * MySwing: Advanced Swing Utilites 
 * Copyright (C) 2005  Santhosh Kumar T 
 *
 * This library is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU Lesser General Public 
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version. 
 *
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details. 
 */

package fr.ifremer.coser.ui.widgets;

import javax.swing.JComponent;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ComponentTitledBorder implements Border, MouseListener, SwingConstants {

    protected int offset = 5;

    protected Component titleComponent;

    protected JComponent container;

    protected Rectangle rect;

    protected Border border;

    public ComponentTitledBorder(Component titleComponent, JComponent container, Border border) {
        super();
        this.titleComponent = titleComponent;
        this.container = container;
        this.border = border;
        container.addMouseListener(this);
    }

    public boolean isBorderOpaque() {
        return true;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width,
                            int height) {
        Insets borderInsets = border.getBorderInsets(c);
        Insets insets = getBorderInsets(c);
        int temp = (insets.top - borderInsets.top) / 2;
        border.paintBorder(c, g, x, y + temp, width, height - temp);
        Dimension size = titleComponent.getPreferredSize();
        rect = new Rectangle(offset, 0, size.width, size.height);
        SwingUtilities.paintComponent(g, titleComponent, (Container) c, rect);
    }

    public Insets getBorderInsets(Component c) {
        Dimension size = titleComponent.getPreferredSize();
        Insets insets = border.getBorderInsets(c);
        insets.top = Math.max(insets.top, size.height);
        return insets;
    }

    private void dispatchEvent(MouseEvent me) {
        if (rect != null && rect.contains(me.getX(), me.getY())) {
            Point pt = me.getPoint();
            pt.translate(-offset, 0);
            titleComponent.setBounds(rect);
            titleComponent.dispatchEvent(new MouseEvent(titleComponent, me.getID(), me.getWhen(),
                                                        me.getModifiers(), pt.x, pt.y, me.getClickCount(), me
                                                                .isPopupTrigger(), me.getButton()));
            if (!titleComponent.isValid()) {
                container.repaint();
            }
        }
    }

    public void mouseClicked(MouseEvent me) {
        dispatchEvent(me);
    }

    public void mouseEntered(MouseEvent me) {
        dispatchEvent(me);
    }

    public void mouseExited(MouseEvent me) {
        dispatchEvent(me);
    }

    public void mousePressed(MouseEvent me) {
        dispatchEvent(me);
    }

    public void mouseReleased(MouseEvent me) {
        dispatchEvent(me);
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.result;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserException;
import fr.ifremer.coser.bean.RSufiResultPath;
import fr.ifremer.coser.services.WebService;
import fr.ifremer.coser.ui.common.CommonHandler;
import fr.ifremer.coser.ui.util.CoserProgressBar;
import fr.ifremer.coser.ui.util.ErrorHelper;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for rsufi result management.
 *
 * @author chatellier
 */
public class ResultHandler extends CommonHandler {

    /**
     * Initialise la vue (principalement en recuperant les données.
     *
     * @param view view
     */
    public void init(SelectUploadResultView view) {

        //SwingUtil.fixTableColumnWidth(view.getSelectedProjectTable(), 1, 25);
        //SwingUtil.fixTableColumnWidth(view.getAvailableProjectTable(), 1, 25);
        view.getAvailableResultTable().setDefaultRenderer(String[].class, new RsufiResultRenderer());
        view.getSelectedResultTable().setDefaultRenderer(String[].class, new RsufiResultRenderer());
        view.getAvailableResultTable().setDefaultRenderer(String.class, new RsufiResultZoneRenderer(view));
        view.getSelectedResultTable().setDefaultRenderer(String.class, new RsufiResultZoneRenderer(view));

        // initialise les données avec les filtres par default
        updateAvailableResultsFilter(view);
        view.getSelectedResultTableModel().setResultPaths(new ArrayList<RSufiResultPath>());

    }

    /**
     * Met à jour les données de la table apres la modification d'un
     * ou plusieurs filtre.
     * <strong>Important:</strong> does not work any longer
     *
     * @param view view
     */
    @Deprecated
    public void updateAvailableResultsFilter(SelectUploadResultView view) {

        // get filter
        Date beginDate = view.getFilterBeginDate().getDate();
        Date endDate = view.getFilterEndDate().getDate();
        boolean onlyPubliable = view.getPubliableResults().isSelected();

        // get result
        WebService webService = view.getContextValue(WebService.class);

        try {
            List<RSufiResultPath> results = webService.findAllProjectWithResult(beginDate, endDate, onlyPubliable);
            view.getAvailableResultTableModel().setResultPaths(results);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't get results", ex);
        }
    }

    /**
     * Add selected result in available table to selected table.
     *
     * @param view view
     */
    public void addAvailableResult(SelectUploadResultView view) {

        // get new result to add
        List<RSufiResultPath> currentResult = view.getSelectedResultTableModel().getResultPaths();
        int[] selectedAvailableRows = view.getAvailableResultTable().getSelectedRows();
        for (int selectedAvailableRow : selectedAvailableRows) {
            RSufiResultPath resultData = view.getAvailableResultTableModel().getResultPaths().get(selectedAvailableRow);
            if (!currentResult.contains(resultData)) {
                currentResult.add(resultData);

                // indicator results are auto selected
                // can be done only here
                view.getSelectedResultTableModel().getIndicatorResults().add(resultData);
            }
        }

        // les collisions ne peuvent pas être détecté a ce moment.
        // seulement lors du clic sur le bouton export/upload

        view.getSelectedResultTableModel().setResultPaths(currentResult);
    }

    /**
     * Remove selected result from selected list.
     *
     * @param view view
     */
    public void removeSelectedResult(SelectUploadResultView view) {
        List<RSufiResultPath> currentResult = view.getSelectedResultTableModel().getResultPaths();
        int[] selectedSelectedRows = view.getSelectedResultTable().getSelectedRows();
        // need to remove reverse order
        for (int index = selectedSelectedRows.length - 1; index >= 0; --index) {
            int selectedSelectedRow = selectedSelectedRows[index];
            currentResult.remove(selectedSelectedRow);
        }
        view.getSelectedResultTableModel().setResultPaths(currentResult);
    }

    /**
     * Perform file upload to coser server after selection by user.
     * <strong>Important:</strong> does not work any longer
     *
     * @param view view
     */
    @Deprecated
    public void performUploadResult(final ExportUploadDialog view) {

        // get authen options
        final String login = view.getUploadLogintextField().getText();
        final String password = new String(view.getUploadPasswordtextField().getPassword());

        // get result selected by user
        SelectUploadResultView parentView = view.getContextValue(SelectUploadResultView.class, JAXXUtil.PARENT);
        final Collection<RSufiResultPath> selectedResults = parentView.getSelectedResultTableModel().getResultPaths();
        final Collection<RSufiResultPath> indicatorResults = parentView.getSelectedResultTableModel().getIndicatorResults();
        final Collection<RSufiResultPath> mapResults = parentView.getSelectedResultTableModel().getMapResults();
        final Collection<RSufiResultPath> publishDataResults = parentView.getSelectedResultTableModel().getPublishDataResults();
        if (CollectionUtils.isNotEmpty(selectedResults)) {

            SwingWorker<String, Void> task = new SwingWorker<String, Void>() {

                @Override
                protected String doInBackground() {
                    try {
                        setWaitCursor(view);

                        // get progress bar
                        CoserProgressBar progressBar = view.getUploadProgressBar();
                        WebService webService = view.getContextValue(WebService.class);
                        String status = webService.performResultUpload(selectedResults, indicatorResults, mapResults, publishDataResults, login, password, progressBar);
                        return status;
                    } catch (CoserBusinessException ex) {
                        throw new CoserException(ex.getMessage(), ex);
                    }
                }

                @Override
                protected void done() {

                    // laisser cet appel, sinon les exceptions sont silencieuse
                    try {
                        String status = get();

                        if (StringUtils.isNotEmpty(status)) {
                            JOptionPane.showMessageDialog(view, t("coser.ui.uploadresult.resultsuploaderror", status),
                                                          t("coser.ui.uploadresult.title"), JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(view, t("coser.ui.uploadresult.resultsuploaded"),
                                                          t("coser.ui.uploadresult.title"), JOptionPane.INFORMATION_MESSAGE);
                        }

                        view.dispose();
                    } catch (Exception ex) {
                        //throw new CoserException("Can't get upload status", ex);
                        // FIXME chatellier 20110126 le dispatch global marche pas ? :(
                        ErrorHelper errorHelper = new ErrorHelper();
                        errorHelper.showErrorDialog(view, ex.getMessage(), ex);
                    } finally {
                        setDefaultCursor(view);
                    }
                }
            };
            task.execute();
        }
    }

    /**
     * Perform file upload to coser server after selection by user.
     * <strong>Important:</strong> does not work any longer
     *
     * @param view view
     */
    @Deprecated
    public void performExtractResult(ExportUploadDialog view) {

        // get extract directory
        String extractPath = view.getExtractToTextField().getText();
        File extractDirectory = new File(extractPath);

        SelectUploadResultView parentView = view.getContextValue(SelectUploadResultView.class, JAXXUtil.PARENT);
        Collection<RSufiResultPath> selectedResult = parentView.getSelectedResultTableModel().getResultPaths();
        Collection<RSufiResultPath> publishDataResults = parentView.getSelectedResultTableModel().getPublishDataResults();
        WebService webService = view.getContextValue(WebService.class);
        try {
            setWaitCursor(view);

            webService.performResultExtract(selectedResult, publishDataResults, extractDirectory);
            JOptionPane.showMessageDialog(view, t("coser.ui.uploadresult.resultsextracted"),
                                          t("coser.ui.uploadresult.title"), JOptionPane.INFORMATION_MESSAGE);
            view.dispose();
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't upload results", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Just display export / upload dialog.
     *
     * @param view parent view
     */
    public void showExportUploadDialog(SelectUploadResultView view) {
        ExportUploadDialog dialog = new ExportUploadDialog(view);
        dialog.setHandler(this);
        dialog.setLocationRelativeTo(view);
        dialog.setVisible(true);
    }

    /**
     * Select result file (directory only).
     *
     * @param view          view
     * @param textComponent text component to set selected file
     */
    public void selectExportDirectory(ExportUploadDialog view, JTextField textComponent) {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = selectFileChooser.showOpenDialog(view);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }
}

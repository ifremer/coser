/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.common;

import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.Selection;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Filtre qui ajoute les trou dans les données (valeur intermédiares absentes)
 * et qui supprime les bornes sans valeures ensuite.
 *
 * Modifie egalement le titre de la matrice.
 *
 * @author chatellier
 */
public class LengthStructureMatrixFilter implements MatrixFilter {

    protected Project project;

    protected AbstractDataContainer container;

    public LengthStructureMatrixFilter(Project project, AbstractDataContainer container) {
        this.project = project;
        this.container = container;
    }

    /*
     * @see org.nuiton.math.matrix.viewer.MatrixFilter#filter(org.nuiton.math.matrix.MatrixND)
     */
    @Override
    public MatrixND filter(MatrixND matrix) {

        // modifie le titre de la matrice
        String title = "Coser: " + project.getName();
        if (container instanceof Selection) {
            title += ": " + ((Selection) container).getName();
        }
        matrix.setName(title);

        MatrixND filteredMatrix = matrix;

        String semantic0Name = matrix.getDimensionName(0);
        if ("coser.business.common.length".equals(semantic0Name)) {

            List<?> semantic0 = matrix.getSemantic(0);
            double first = Double.MAX_VALUE;
            double last = Double.MIN_VALUE;
            boolean atLeastOneAdujments = false;

            // on cherche les bornes qui ont des valeurs sup à 0.0
            // sinon, ya du vide autour des choses demandées
            // mais le vide entre est requis (trou de données)
            for (Object categorySem : matrix.getSemantic(0)) {
                for (Object serieSem : matrix.getSemantic(1)) {
                    Double value = matrix.getValue(new Object[]{categorySem, serieSem});
                    if (value > 0.0) {
                        double category = (Double) categorySem;
                        if (first > category) {
                            first = category;
                        }
                        if (last < category) {
                            last = category;
                        }
                        atLeastOneAdujments = true;
                    }
                }
            }

            // cas ou toutes les valeurs sont a zero et les
            // bornes ne sont pas ajustées
            // chose incohérente...
            if (!atLeastOneAdujments) {
                first = (Double) matrix.getSemantic(0).get(0);
                last = (Double) matrix.getSemantic(0).get(matrix.getSemantic(0).size() - 1);
            }

            // on verifie toutes les valeurs pour savoir si c'est un
            // pas entier ou un demi pas
            boolean haltStep = false;
            for (Object number : semantic0) {
                double dNumber = (Double) number;
                if (dNumber - Math.floor(dNumber) > 0) {
                    haltStep = true;
                    break;
                }
            }

            List<Double> newLengthSemantic = new ArrayList<Double>();
            for (double index = first; index <= last; index += (haltStep) ? 0.5 : 1) {
                newLengthSemantic.add(index);
            }

            // nouvelle matrix remplit avec des zero
            // avec la nouvelle semantique pour les tailles
            List<?>[] semantics = new List<?>[matrix.getSemantics().length];
            semantics[0] = newLengthSemantic;
            System.arraycopy(matrix.getSemantics(), 1, semantics, 1, matrix.getSemantics().length - 1);
            filteredMatrix = MatrixFactory.getInstance().create(matrix.getName(), semantics);

            // copy des elements en fonction des semantiques
            filteredMatrix.pasteSemantics(matrix);
        }

        return filteredMatrix;
    }

    /*
     * @see org.nuiton.math.matrix.viewer.MatrixFilter#filterDimension(org.nuiton.math.matrix.MatrixND, int, int)
     */
    @Override
    public MatrixND filterDimension(MatrixND matrix, int dim, int sumStep) {
        return matrix;
    }
}

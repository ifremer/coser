/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.result;

import fr.ifremer.coser.CoserApplicationContext;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.ui.selection.SelectionRsufiView;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Selection result table renderer.
 *
 * @author chatellier
 *
 *         <p/>
 */
public class ResultTableRenderer extends DefaultTableCellRenderer {


    private static final long serialVersionUID = -9030155088814184637L;

//    protected SelectionRsufiView view;

    protected ZoneMap zoneMap;

    public ResultTableRenderer(SelectionRsufiView view) {
//        this.view = view;
        this.zoneMap = view.getContextValue(CoserApplicationContext.class).getZoneMap();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        Object localValue = value;

        switch (column) {
            case 2:
                if (value != null) {
                    localValue = zoneMap.getZoneFullName((String) value);
//                    // get web service
//                    try {
//                        WebService webService = view.getContextValue(WebService.class);
//                        localValue = webService.getZoneFullName((String)value);
//                    } catch (CoserBusinessException ex) {
//                        throw new CoserException("Can't get zone name", ex);
//                    }
                }
                break;
            case 5:
            case 6:
                boolean availale = (Boolean) value;
                if (availale) {
                    localValue = t("coser.ui.common.yes");
                } else {
                    localValue = t("coser.ui.common.no");
                }
                break;
        }
        return super.getTableCellRendererComponent(table, localValue, isSelected, hasFocus,
                                                   row, column);
    }
}

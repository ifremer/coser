/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.model;

import fr.ifremer.coser.ui.util.CoserListModel;

import javax.swing.AbstractListModel;
import java.util.List;

/**
 * Model pour list contenant la liste des années définies dans le projet et
 * en selection la liste des années définie pour la selection.
 *
 * @author chatellier
 */
public class YearListModel extends AbstractListModel implements CoserListModel {


    private static final long serialVersionUID = 9172638630862188715L;

    protected List<String> years;

    public List<String> getYears() {
        return years;
    }

    public void setYears(List<String> years) {
        this.years = years;
        fireContentsChanged(this, 0, years.size());
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        int result = 0;
        if (getYears() != null) {
            result = getYears().size();
        }
        return result;
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return getYears().get(index);
    }

    /*
     * @see fr.ifremer.coser.ui.util.CoserListModel#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(Object element) {
        return getYears().indexOf(element);
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection;

import fr.ifremer.coser.ui.selection.model.OccurrenceDensitySpeciesListModel;
import org.jdesktop.swingx.renderer.DefaultListRenderer;

import javax.swing.JList;
import java.awt.Component;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Specy renderer with occurrence and density display.
 *
 * @author chatellier
 */
public class SpeciesListOccDensRenderer extends DefaultListRenderer {


    private static final long serialVersionUID = -5404111064519251687L;

    protected Map<String, String> reftaxSpecies;

    public SpeciesListOccDensRenderer(Map<String, String> reftaxSpecies) {
        this.reftaxSpecies = reftaxSpecies;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {

        String species = (String) value;
        OccurrenceDensitySpeciesListModel model = (OccurrenceDensitySpeciesListModel) list.getModel();

        double occurrence = model.getOccurrence(species);
        double density = model.getDensity(species);

        String speciesText = species;
        if (reftaxSpecies.containsKey(species)) {
            speciesText = reftaxSpecies.get(species);
        }
        String stringValue = t("coser.ui.selection.occurrencedensityrenderer", speciesText, occurrence, density);

        return super.getListCellRendererComponent(list, stringValue, index, isSelected, cellHasFocus);
    }

}

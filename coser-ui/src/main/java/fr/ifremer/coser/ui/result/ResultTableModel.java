/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.result;

import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.ui.selection.SelectionRsufiView;

import javax.swing.table.AbstractTableModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.nuiton.i18n.I18n.t;

/**
 * Modele de la table des résultats RSufi.
 *
 * @author chatellier
 */
public class ResultTableModel extends AbstractTableModel implements PropertyChangeListener {


    private static final long serialVersionUID = -1192463259386773117L;

    protected Selection selection;

    public ResultTableModel(SelectionRsufiView view) {
        selection = view.getContextValue(Selection.class);

        selection.addPropertyChangeListener(Selection.PROPERTY_RSUFI_RESULTS, this);
    }

    /*
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return selection.getRsufiResults().size();
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 7;
    }


    @Override
    public String getColumnName(int column) {
        String name = null;

        switch (column) {
            case 0:
                name = t("coser.ui.result.table.resultName");
                break;
            case 1:
                name = t("coser.ui.result.table.rsufiVersion");
                break;
            case 2:
                name = t("coser.ui.result.table.zone");
                break;
            case 3:
                name = t("coser.ui.result.table.estComIndFile");
                break;
            case 4:
                name = t("coser.ui.result.table.estPopIndFile");
                break;
            case 5:
                name = t("coser.ui.result.table.maps");
                break;
            case 6:
                name = t("coser.ui.result.table.otherfiles");
                break;
        }
        return name;
    }

    /*
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result = null;
        RSufiResult rsufiResult = getValue(rowIndex);
        switch (columnIndex) {
            case 0:
                result = rsufiResult.getName();
                break;
            case 1:
                result = rsufiResult.getRsufiVersion();
                break;
            case 2:
                result = rsufiResult.getZone();
                break;
            case 3:
                result = rsufiResult.getEstComIndName();
                break;
            case 4:
                result = rsufiResult.getEstPopIndName();
                break;
            case 5:
                result = rsufiResult.isMapsAvailable();
                break;
            case 6:
                result = !rsufiResult.getOtherFiles().isEmpty();
                break;
        }

        return result;
    }

    /**
     * Get value for wall row.
     *
     * @param rowIndex row index
     * @return rsufi result at row index
     */
    public RSufiResult getValue(int rowIndex) {
        RSufiResult result = selection.getRsufiResults().get(rowIndex);
        return result;
    }

    /*
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        fireTableDataChanged();
    }
}

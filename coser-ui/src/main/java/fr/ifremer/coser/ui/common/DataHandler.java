/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.common;

import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.services.ProjectService;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.session.SwingSession;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixViewerPanel;
import org.nuiton.math.matrix.viewer.renderer.MatrixChartRenderer;
import org.nuiton.math.matrix.viewer.renderer.MatrixPanelRenderer;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Data application handler.
 *
 * @author chatellier
 */
public class DataHandler extends CommonHandler {

    /**
     * Display lengthStructure matrix in matrixviewerpanel.
     *
     * @param parent         parent component
     * @param session        session to restore frame state
     * @param projectService projectService
     * @param project        project
     * @param container      data container
     * @param species        selected species (can be null)
     */
    public void displayLengthStructureGraph(final Component parent, final SwingSession session,
                                            final ProjectService projectService, final Project project, final AbstractDataContainer container,
                                            final String species) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                try {
                    setWaitCursor(parent);

                    // get matrix
                    MatrixND matrix = projectService.getLengthStructure(project, container);

                    // close previous opened
                    JFrame previousFrame = ((JAXXContext) parent).getContextValue(JFrame.class, "lengthstructureframe");
                    if (previousFrame != null) {
                        previousFrame.dispose();
                    }

                    // display matrix viewer component
                    JFrame matrixViewerFrame = new JFrame(t("coser.ui.graph.lengthStructure"));
                    matrixViewerFrame.setName("lengthstructureframe");
                    MatrixViewerPanel panel = new MatrixViewerPanel();
                    panel.addMatrixRenderer(new MatrixChartRenderer());
                    panel.addMatrixRenderer(new MatrixPanelRenderer());
                    panel.addMatrixFilter(new LengthStructureMatrixFilter(project, container));
                    panel.setMatrix(matrix);
                    // init default selection and rendering
                    String defaultSpecies = species;
                    if (defaultSpecies != null) {
                        // il faut quand même la traduire car les données
                        // contiennent les noms déjà traduits
                        defaultSpecies = project.getDisplaySpeciesText(defaultSpecies);
                    } else {
                        defaultSpecies = (String) matrix.getSemantic(1).get(0);
                    }
                    panel.initRenderering(new List[]{
                            matrix.getSemantic(0), // select all length
                            Collections.singletonList(defaultSpecies) // select first species
                    }, new int[]{-1, -1, 0, 0}); // select dim 3 et 4 sum action
                    matrixViewerFrame.add(panel);
                    matrixViewerFrame.pack();
                    matrixViewerFrame.setLocationRelativeTo(parent);
                    session.add(matrixViewerFrame); // session restore
                    matrixViewerFrame.toFront();
                    matrixViewerFrame.setVisible(true);

                    // register current frame
                    ((JAXXContext) parent).setContextValue(matrixViewerFrame, "lengthstructureframe");
                } finally {
                    setDefaultCursor(parent);
                }
            }
        });
    }
}

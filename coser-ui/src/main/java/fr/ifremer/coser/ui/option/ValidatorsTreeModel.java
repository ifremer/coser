/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.option;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;
import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.swingx.tree.TreeModelSupport;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Validators tree model.
 *
 * @author chatellier
 */
public class ValidatorsTreeModel implements TreeModel {

    protected TreeModelSupport support;

    protected Category[] dataCategory;

    public ValidatorsTreeModel() {
        support = new TreeModelSupport(this);

        // les categories contiennent des données non liés aux données
        List<Category> dataCategories = new ArrayList<Category>();
        for (Category category : Category.values()) {
            if (category.isDataCategory()) {
                dataCategories.add(category);
            }
        }
        dataCategory = dataCategories.toArray(new Category[dataCategories.size()]);
    }

    /*
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    @Override
    public Object getRoot() {
        return 1L;
    }

    /*
     * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
     */
    @Override
    public Object getChild(Object parent, int index) {

        Object child = null;

        if (parent == getRoot()) {
            child = dataCategory[index];
        } else if (parent instanceof Category) {
            child = ValidationLevel.values()[index];
        }

        return child;
    }

    /*
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    @Override
    public int getChildCount(Object parent) {

        int result = 0;

        if (parent == getRoot()) {
            result = dataCategory.length;
        } else if (parent instanceof Category) {
            result = ValidationLevel.values().length;
        }

        return result;
    }

    /*
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    /*
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    /*
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int result = -1;

        if (parent == getRoot()) {
            result = ArrayUtils.indexOf(dataCategory, child);
        } else if (parent instanceof Category) {
            result = ArrayUtils.indexOf(ValidationLevel.values(), child);
        }

        return result;
    }

    /*
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        support.addTreeModelListener(l);
    }

    /*
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        support.addTreeModelListener(l);
    }
}

/* %%Ignore-License
 * Copyright (C) 2007 Craig Knudsen
 *
 * AccordionPane is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License can be found at www.gnu.org. 
 * To receive a hard copy, you can write to:
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307 USA.
 */

package fr.ifremer.coser.ui.widgets;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * The AccordionPane class implements a vertical accordion container, similar to
 * those seen in many AJAX/DHTML frameworks. Multiple child panels are added,
 * but only one of those will be visible at a time. The user can switch between
 * visible panels by clicking on the title area of one of the inactive panels.
 *
 * @author Craig Knudsen, craig@k5n.us
 */
// TODO: add/remove ChangeListener support
// TODO: add support for setting icons
// TODO: tooltip text for titles
// TODO: enable/disable panels
public class AccordionPane extends JPanel {


    private static final long serialVersionUID = -4266521429712806753L;

    protected List<AccordionPaneSubPanel> children;

    protected int selected = -1;

    protected int previouslySelected = -1; // used in animation transition

    // background color for pane title
    protected Color paneBackgroundColor;

    // background color for current pane title
    protected Color activePaneBackgroundColor;

    protected Timer timer;

    protected int transitionStep = 0;

    protected int remainder = -1;

    /**
     * Create a new AccordionPane object. After creating the AccordionPane, you
     * will need to call addPanel to add UI components to it.
     */
    public AccordionPane() {
        this.children = new ArrayList<AccordionPaneSubPanel>();
        this.selected = -1;
        this.setLayout(new AccordionLayout(this));
        this.paneBackgroundColor = super.getBackground();
        this.activePaneBackgroundColor = new Color(255, 255, 200);
    }

    /**
     * Set the background color for the active and inactive pane title areas.
     *
     * @param normalColor The color to use for the non-active panels
     * @param activeColor The color to use for the currently selected panel
     */
    public void setTitleBackgroundColors(Color normalColor, Color activeColor) {
        this.paneBackgroundColor = normalColor;
        this.activePaneBackgroundColor = activeColor;
        repaint();
    }

    public void add(AccordionPaneSubPanel subPanel) {
        subPanel.setAccordionPane(this);
        subPanel.setIndex(this.children.size());
        this.children.add(subPanel);
        super.add(subPanel);
    }

    public void paint(Graphics g) {
        // Do some initializing on the first paint call
        if (this.selected < 0 && this.children.size() > 0) {
            this.setSelected(0, false);
        }
        if (this.transitionStep >= this.remainder && this.remainder > 0
            && timer != null) {
            this.timer.stop();
            this.timer = null;
            System.out.println("Killed timer.");
        }
        super.paint(g);
    }

    /**
     * Set the currently active/selected pane.
     *
     * @param num The pane number to select (0 is first)
     */
    public void setSelected(int num) {
        setSelected(num, false);
    }

    /**
     * Set the currently active/selected pane.
     *
     * @param num           The pane number to select (0 is first)
     * @param showAnimation Show the animation transition from the previously selected pane to
     *                      the newly selected pane.
     */
    public void setSelected(int num, boolean showAnimation) {
        if (num == selected)
            return;
        if (!showAnimation) {
            this.previouslySelected = -1;
        } else {
            this.previouslySelected = selected;
        }
        this.selected = num;
        for (int i = 0; i < children.size(); i++) {
            AccordionPaneSubPanel subPanel = children
                    .get(i);
            // subPanel.child.setVisible ( i == num
            // || ( i == this.previouslySelected && showAnimation ) );
            subPanel.titlePanel
                    .setBackground(i == num ? this.activePaneBackgroundColor
                                            : this.paneBackgroundColor);
        }
        if (this.previouslySelected >= 0 && showAnimation) {
            ActionListener a = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // Add another step in the animation transition
                    if (remainder - transitionStep < 10) {
                        transitionStep++;
                    } else if (remainder - transitionStep < 25) {
                        transitionStep += 3;
                    } else {
                        transitionStep += 10;
                    }
                    // System.out.println ( "transitionStep=" + transitionStep );
                    if (transitionStep >= remainder) {
                        // We're done with the animation
                        transitionStep = -1;
                        timer.stop();
                    } else {
                        timer.setInitialDelay(1);
                        timer.restart();
                    }
                    doLayout();
                    validate();
                    repaint();
                }
            };

            if (timer != null) {
                timer.stop();
                timer = null;
            }

            // Animate the transition from one panel to another panel.
            this.transitionStep = 0;
            timer = new Timer(1, a);
            timer.start();
        }
        doLayout(); // ec-20101202 : par moment ca fonctionne mal
        validate();
        repaint();
    }

    /**
     * Get the index of the currently selected pane.
     *
     * @return the index of the currently selected pane
     */
    public int getSelectedIndex() {
        return this.selected;
    }

    /**
     * Get the number of panes in the AccordionPane.
     *
     * @return the number of panes
     */
    public int getPaneCount() {
        return this.children.size();
    }

    /**
     * Set the text title at the specified location
     *
     * @param index    the index number of the pane
     * @param newTitle the new title for the specified pane.
     */
    public void setTitleAt(int index, String newTitle) {
        AccordionPaneSubPanel subPanel = this.children
                .get(index);
        subPanel.titleLabel.setText(newTitle);
    }

    /**
     * Get the text title at the specified location
     *
     * @param index the index number of the pane
     * @return the text title of the specified pane
     */
    public String getTitleAt(int index) {
        AccordionPaneSubPanel subPanel = this.children
                .get(index);
        return subPanel.titleLabel.getText();
    }

    /**
     * Set the icon for the specified location.
     *
     * @param index
     * @param icon
     */
    public void setIconAt(int index, Icon icon) {
        AccordionPaneSubPanel subPanel = this.children
                .get(index);
        subPanel.titleLabel.setIcon(icon);
    }

    /**
     * Returns the component at index.
     *
     * @param index the index of the item being queried
     * @return component
     */
    public Component getComponentAt(int index) {
        AccordionPaneSubPanel subPanel = this.children
                .get(index);
        return subPanel.child;
    }

    /**
     * Returns the currently selected component for this AccordionPane.
     *
     * @return the currently selected component
     */
    public Component getSelectedComponent() {
        AccordionPaneSubPanel subPanel = this.children
                .get(this.selected);
        return subPanel.child;
    }

}
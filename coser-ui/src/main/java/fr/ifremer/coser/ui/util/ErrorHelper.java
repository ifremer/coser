/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.util;

import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserUIApplicationContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.error.ErrorReporter;

import javax.swing.JOptionPane;
import java.awt.Component;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Error helper.
 *
 * Used to easily switch real exception interface. Currently used : swingx.
 *
 * @author chatellier
 */
public class ErrorHelper implements ErrorReporter {

    /** Log. */
    private static final Log log = LogFactory.getLog(ErrorHelper.class);

    protected CoserConfig coserConfig;

    public ErrorHelper() {
        this.coserConfig = CoserUIApplicationContext.get().getConfig();
    }

    /**
     * Display a user friendly error frame.
     *
     * @param parent  parent component
     * @param message message for user
     * @param cause   exception cause
     */
    public void showErrorDialog(Component parent, String message,
                                Throwable cause) {
        JXErrorPane pane = new JXErrorPane();
        ErrorInfo info = new ErrorInfo(t("coser.ui.error.title"),
                                       t("coser.ui.error.htmlmessage", message), null, null,
                                       cause, null, null);
        pane.setErrorInfo(info);
        pane.setErrorReporter(this);
        JXErrorPane.showDialog(parent, pane);
    }

    /**
     * Display a user friendly error frame.
     *
     * @param message message for user
     */
    public void showErrorDialog(String message) {
        showErrorDialog(message, null);
    }

    /**
     * Display a user friendly error frame.
     *
     * @param message message for user
     * @param cause   exception cause
     */
    public void showErrorDialog(String message, Throwable cause) {
        showErrorDialog(null, message, cause);
    }

    /*
     * @see org.jdesktop.swingx.error.ErrorReporter#reportError(org.jdesktop.swingx.error.ErrorInfo)
     */
    @Override
    public void reportError(ErrorInfo errorInfo) {

        try {
            String emailTo = coserConfig.getSupportEmail();

            MultiPartEmail email = new MultiPartEmail();
            // smtp
            email.setHostName(coserConfig.getSmtpHost());
            // to
            email.addTo(emailTo, "Support");
            // from
            email.setFrom("no-reply@forge.codelutin.com", "Coser");
            // subject
            email.setSubject("Project error notification : Coser");
            // add reply-to for spam
            email.addReplyTo("no-reply@forge.codelutin.com");

            // message description
            StringBuilder message = new StringBuilder();
            message.append(formatMessage("Project", "Coser " + coserConfig.getApplicationVersion()));
            message.append(formatMessage("Date", new Date().toString()));
            message.append(formatMessage("Title", errorInfo.getTitle()));
            message.append(formatMessage("Description", errorInfo.getBasicErrorMessage().replaceAll("<[^>]+>", "")));

            // message configuration
            message.append(formatMessage("Configuration", null));
            List<String> propertiesNames = new ArrayList<String>(coserConfig.getOptions().stringPropertyNames());
            Collections.sort(propertiesNames);
            for (String propertyName : propertiesNames) {
                // security, don't send string containing password :
                if (!propertyName.contains("pass")) {
                    message.append("\t");
                    message.append(propertyName);
                    message.append(" : ");
                    message.append(coserConfig.getOptions().getProperty(propertyName));
                    message.append("\n");
                }
            }

            // message exception
            StringWriter out = new StringWriter();
            PrintWriter writer = new PrintWriter(out);
            errorInfo.getErrorException().printStackTrace(writer);
            message.append(formatMessage("Exception", out.toString()));

            // TODO EC-20101005 i18n files are iso encoded ?
            email.setContent(message.toString(), "text/plain; charset=ISO-8859-9");

            // send mail
            email.send();

            JOptionPane.showMessageDialog(null, t("coser.ui.error.reportSendTo", emailTo));
        } catch (EmailException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't send report email", ex);
            }
        }
    }

    protected String formatMessage(String category, String content) {
        String formatted = category + " :\n";
        if (StringUtils.isNotEmpty(content)) {
            formatted += "\t" + content + "\n";
        }
        return formatted;
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.maps;

import com.bbn.openmap.InformationDelegator;
import com.bbn.openmap.Layer;
import com.bbn.openmap.LayerHandler;
import com.bbn.openmap.MapBean;
import com.bbn.openmap.MouseDelegator;
import com.bbn.openmap.PropertyHandler;
import com.bbn.openmap.event.OMMouseMode;
import com.bbn.openmap.gui.OverlayMapPanel;
import com.bbn.openmap.layer.GraticuleLayer;
import com.bbn.openmap.layer.location.LocationHandler;
import com.bbn.openmap.layer.location.LocationLayer;
import com.bbn.openmap.layer.shape.ShapeLayer;
import com.bbn.openmap.omGraphics.DrawingAttributes;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.util.Coordinate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Color;
import java.io.File;
import java.util.List;
import java.util.Properties;

/**
 * Coser map based open openmap.
 *
 * @author chatellier
 */
public class CoserMap extends OverlayMapPanel {


    private static final long serialVersionUID = 3134624721243512358L;

    private static final Log log = LogFactory.getLog(CoserMap.class);

    protected InformationDelegator informationDelegator;

    protected LocationLayer haulLocationLayer;

    protected static final float SCALE = 9500000f;

    public CoserMap() {
        super(new PropertyHandler(new Properties()), true);
        create();

        MapBean mapBean = getMapBean();
        mapBean.setScale(SCALE);

        addMapComponent(new LayerHandler());
        addMapComponent(new MouseDelegator(mapBean));

        getMapBean().setBackgroundColor(new Color(0x99b3cc));
    }

    public void initMap(Project project, InformationDelegator informationDelegator) {

        this.informationDelegator = informationDelegator;
        OMMouseMode mouseMode = new OMMouseMode();
        mouseMode.setInfoDelegator(informationDelegator);
        addMapComponent(mouseMode);

        MapBean mapBean = getMapBean();

        // centrée sur la france
        mapBean.setCenter(50f, 0f);
        mapBean.setScale(16000000f);

        // graticule layer
        addGraticuleLayer();

        // world layer, affiche si la liste des maps utilsateur
        // ne contient pas une carte nommée cntry00.shp (carte du monde
        // sans fuseau horaire)
        boolean cntrOOShapeFound = false;
        if (project.getMaps() != null) {
            for (File map : project.getMaps()) {
                if (map.getName().equals("cntry00.shp")) {
                    cntrOOShapeFound = true;
                }
            }
        }

        // avant du world layer avant les autres
        if (!cntrOOShapeFound) {
            addLayer("maps/vmap_area_thin.shp", "maps/vmap_area_thin.shp", "ff000000", "ffbdde83");
        }

        // project layer
        if (project.getMaps() != null) {
            for (File map : project.getMaps()) {
                // le fond de carte a une couleur vert/contour noir
                if (map.getName().equals("cntry00.shp")) {
                    addLayer(map.getAbsolutePath(), map.getAbsolutePath(), "ff000000", "ffbdde83");
                } else {
                    // les autres rouge (contour noir)
                    addLayer(map.getAbsolutePath(), map.getAbsolutePath(), "ff000000", "ffFDA908");
                }
            }
        }
    }

    protected void addGraticuleLayer() {
        GraticuleLayer layer = new GraticuleLayer();
        Properties p = new Properties();
        // Show lat / lon spacing labels
        p.setProperty("." + GraticuleLayer.ShowRulerProperty, "true");
        p.setProperty("." + GraticuleLayer.ShowOneAndFiveProperty, "true");
        // Controls when the five degree lines and one degree lines kick in
        // - when there is less than the threshold of ten degree lat or lon
        // lines, five degree lines are drawn.  The same relationship is there
        // for one to five degree lines.
        p.setProperty("." + GraticuleLayer.ThresholdProperty, "5");
        // the color of 10 degree spacing lines (Hex ARGB)
        p.setProperty("." + GraticuleLayer.TenDegreeColorProperty, "FF000000");
        // the color of 5 degree spacing lines (Hex ARGB)
        p.setProperty("." + GraticuleLayer.FiveDegreeColorProperty, "C7009900");
        // the color of 1 degree spacing lines (ARGB)
        p.setProperty("." + GraticuleLayer.OneDegreeColorProperty, "FF003300");
        // the color of the equator (ARGB)
        p.setProperty("." + GraticuleLayer.EquatorColorProperty, "FFFF0000");
        // the color of the international dateline (ARGB)
        p.setProperty("." + GraticuleLayer.DateLineColorProperty, "FF000099");
        // the color of the special lines (ARGB) (Tropic of Cancer, Capricorn)
        p.setProperty("." + GraticuleLayer.SpecialLineColorProperty, "FF000000");
        // the color of the labels (ARGB)
        p.setProperty("." + GraticuleLayer.TextColorProperty, "FF000000");

        layer.setProperties("", p);
        addMapComponent(layer);
    }

    /**
     * Display stata's haul position in an openmap layer
     *
     * @param hauls coordinate to display on map
     */
    public void addStataHaulLayer(List<Coordinate> hauls) {

        if (log.isDebugEnabled()) {
            log.debug("Adding strata haul layer (" + hauls.size() + " coordinates)");
        }

        if (haulLocationLayer != null) {
            removeMapComponent(haulLocationLayer);
        }
        HaulLocationHandler locationHandler = new HaulLocationHandler(hauls);
        haulLocationLayer = new LocationLayer();
        haulLocationLayer.setLocationHandlers(new LocationHandler[]{locationHandler});
        haulLocationLayer.addInfoDisplayListener(informationDelegator);
        addMapComponent(haulLocationLayer);
    }

    /**
     * Add a new layer to the map depending on mapFile extension.
     *
     * @param layerId   layer id
     * @param mapFile   absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     */
    protected void addLayer(String layerId, String mapFile, String lineColor,
                            String fillColor) {

        Layer layer = null;
        // get layer depending on type
        if (mapFile.endsWith(".shp")) {
            layer = getShapeLayer(layerId, mapFile, lineColor, fillColor);
        } else {
            if (log.isErrorEnabled()) {
                log.error("Can't find layer for " + mapFile + " (unknown type)");
            }
        }

        // display layer
        if (layer != null) {
            if (log.isDebugEnabled()) {
                log.debug("Add layer " + layer);
            }
            addMapComponent(layer);
        }
    }

    /**
     * Manage shp layer display.
     *
     * @param layerId   layer id
     * @param mapFile   absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * @return shape layer
     * @see ShapeLayer
     */
    protected Layer getShapeLayer(String layerId, String mapFile, String lineColor,
                                  String fillColor) {

        if (log.isDebugEnabled()) {
            log.debug("Get ShapeLayer with filename : " + mapFile);
        }

        ShapeLayer shapeLayer = new ShapeLayer();
        Properties p = new Properties();
        p.setProperty(layerId + "." + ShapeLayer.shapeFileProperty, mapFile);
        p.setProperty(layerId + "." + DrawingAttributes.linePaintProperty, lineColor);
        p.setProperty(layerId + "." + DrawingAttributes.fillPaintProperty, fillColor);
        shapeLayer.setProperties(layerId, p);
        shapeLayer.setName(layerId);

        return shapeLayer;
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.common;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;
import java.util.Map;

/**
 * L'editeur remplace la valeur présente dans le fichier csv pour l'especes
 * par un autre nom (latin valide, code...)
 *
 * @author echatellier
 * @since 1.3
 */
public class SpeciesListRenderer extends DefaultListCellRenderer {


    private static final long serialVersionUID = 3747535342745177615L;

    protected Map<String, String> reftaxSpecies;

    public SpeciesListRenderer(Map<String, String> reftaxSpecies) {
        this.reftaxSpecies = reftaxSpecies;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {
        String speciesText = (String) value;
        if (reftaxSpecies.containsKey(speciesText)) {
            speciesText = reftaxSpecies.get(speciesText);
        }

        return super.getListCellRendererComponent(list, speciesText, index, isSelected,
                                                  cellHasFocus);
    }
}

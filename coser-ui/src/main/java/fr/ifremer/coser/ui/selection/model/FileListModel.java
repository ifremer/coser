/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.model;

import javax.swing.AbstractListModel;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Modele de list pour la list des fichiers et répertoire des fichiers
 * additionnels.
 *
 * @author chatellier
 */
public class FileListModel extends AbstractListModel {


    private static final long serialVersionUID = -8652851018076968539L;

    protected List<File> files;

    public FileListModel() {
        files = new ArrayList<File>();
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
        fireContentsChanged(this, 0, files.size() - 1);
    }

    public void add(File file) {
        files.add(file);
        fireIntervalAdded(this, files.size() - 1, files.size() - 1);
    }

    public void remove(File file) {
        int index = files.indexOf(file);
        if (files.remove(file)) {
            fireIntervalRemoved(this, index, index);
        }
    }

    @Override
    public int getSize() {
        return files.size();
    }

    @Override
    public Object getElementAt(int index) {
        return files.get(index);
    }
}

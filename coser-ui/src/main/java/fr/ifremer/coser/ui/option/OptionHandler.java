/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.option;

import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.ui.common.CommonHandler;
import jaxx.runtime.JAXXContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

import javax.swing.JOptionPane;
import javax.swing.tree.TreePath;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for options.
 *
 * @author chatellier
 */
public class OptionHandler extends CommonHandler {

    private static final Log log = LogFactory.getLog(OptionHandler.class);

    /**
     * Restaure les validateurs par defaut en supprimant ceux
     * ceux du répertoire et en utilsant le class path.
     *
     * @param view view
     */
    public void restoreDefaults(ValidatorDialog view) {

        int answer = JOptionPane.showConfirmDialog(view, t("coser.ui.validators.confirmRestore"),
                                                   t("coser.ui.validators.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (answer == JOptionPane.YES_OPTION) {
            CoserConfig config = view.getContextValue(CoserConfig.class);
            File validatorsDir = config.getValidatorsDirectory();

            try {
                FileUtils.deleteDirectory(validatorsDir);
                view.dispose();
            } catch (IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't delete directory", ex);
                }
            }
        }
    }

    /**
     * Appelé par le layer pour copier les validateurs par defaut sur
     * le disque de l'utilisateur. Les copies depuis le classpath (dossier
     * "validators" vers le dossier défini par l'utilisateur.
     *
     * @param view view
     */
    public void copyDefaultValidators(ValidatorDialog view) {

        int answer = JOptionPane.showConfirmDialog(view, t("coser.ui.validators.confirmCopyToDisk"),
                                                   t("coser.ui.validators.title"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

        if (answer != JOptionPane.YES_OPTION) {
            view.dispose();
            return;
        }

        CoserConfig config = view.getContextValue(CoserConfig.class);
        File validatorsDir = config.getValidatorsDirectory();
        validatorsDir.mkdirs();

        // ne match que dans coser
        try {
            List<URL> resources = Resource.getResources("validators/fr/ifremer/coser/data/.*\\.xml",
                                                        Thread.currentThread().getContextClassLoader());
            if (log.isDebugEnabled()) {
                log.debug("XML resources found : " + resources.size() + " files");
            }
            for (URL resource : resources) {
                String path = resource.getPath();
                String lastPath = path.substring(path.indexOf("/validators/") + "/validators/".length());
                String folderPath = lastPath.substring(0, lastPath.lastIndexOf('/'));
                String name = lastPath.substring(lastPath.lastIndexOf('/') + 1);

                File validatorDirectory = new File(validatorsDir, folderPath);
                validatorDirectory.mkdirs();
                File validatorFile = new File(validatorDirectory, name);

                if (log.isDebugEnabled()) {
                    log.debug("Coping resources " + resource + " to " + validatorFile.getAbsolutePath());
                }

                FileUtils.copyURLToFile(resource, validatorFile);
            }
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't copy validators to file system", ex);
            }
        }

        // remove bloking layer
        //view.getNoCopiedToDiskLayer().setUI(null);
        // FIXME gros hack, j'arrive pas a enlever le layer
        view.dispose();
        ValidatorDialog newView = new ValidatorDialog((JAXXContext) view);
        newView.setHandler(this);
        newView.setLocationRelativeTo(view);
        newView.setVisible(true);
    }

    /**
     * Retourne le File qui correspond (ou devrait) a un fichier de validateur
     * sur le disque.
     *
     * @param view view
     * @return un file (existant... ou pas)
     */
    protected File getValidatorFile(ValidatorDialog view) {
        File validatorFile = null;

        TreePath selectedPath = view.getValidatorsTree().getSelectionPath();
        if (selectedPath != null) {
            Object[] path = selectedPath.getPath();

            CoserConfig config = view.getContextValue(CoserConfig.class);
            File validatorsDir = config.getValidatorsDirectory();

            // si la selection est un sous context d'une categorie
            if (path.length == 3) {
                Category category = (Category) path[1];
                ValidationLevel level = (ValidationLevel) path[2];

                String sPackage = Catch.class.getPackage().getName();

                String validatorResource = sPackage.replace('.', '/') + "/";
                // c'est le meme nom actuellement
                // au lower case et capitalize pret ;)
                // CATCH = Catch
                validatorResource += StringUtils.capitalize(category.toString().toLowerCase());
                validatorResource += "-" + level.getXWorkContext() + "-validation.xml";

                validatorFile = new File(validatorsDir, validatorResource);
            }
        }

        return validatorFile;
    }

    /**
     * Control selected in tree.
     *
     * Dans ce cas, les fichiers sur disque existent, et on ne doit
     * editer que ceux là, sinon enregistrer un fichier dans le classpath
     * ca risque pas de fonctionner.
     *
     * @param view view
     */
    public void selectValidator(ValidatorDialog view) {

        view.getCreateNewButton().setEnabled(false);
        view.getDeleteButton().setEnabled(false);
        view.getSaveButton().setEnabled(false);

        TreePath selectedPath = view.getValidatorsTree().getSelectionPath();
        if (selectedPath != null) {

            File validatorFile = getValidatorFile(view);

            if (validatorFile != null) {

                // look for resources in class path
                if (log.isDebugEnabled()) {
                    log.debug("Looking for validator : " + validatorFile.getAbsoluteFile());
                }

                if (validatorFile.isFile()) {
                    view.getValidationEditor().open(validatorFile);
                    // selection, enable delete
                    view.getDeleteButton().setEnabled(true);
                    view.getSaveButton().setEnabled(true);
                } else {
                    view.getValidationEditor().close();
                    view.getCreateNewButton().setEnabled(true);
                }
                view.getValidationEditor().validate();
                view.getValidationEditor().repaint();
            }
        }
    }

    /**
     * Save current file.
     *
     * @param view view
     */
    public void saveValidator(ValidatorDialog view) {
        view.getValidationEditor().save();
    }

    /**
     * Create new file.
     *
     * @param view view
     */
    public void createNew(ValidatorDialog view) {

        File validatorFile = getValidatorFile(view);

        try {
            FileUtils.writeStringToFile(validatorFile, "");
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't copy validators to file system", ex);
            }
        }

        selectValidator(view);
    }

    /**
     * Delete a single validator.
     *
     * @param view view
     */
    public void deleteValidator(ValidatorDialog view) {

        File validatorFile = getValidatorFile(view);
        validatorFile.delete();
        selectValidator(view);
    }

}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.option;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Renderer pour l'arbre des navigateurs.
 *
 * Affiche les noms explicites à la place des Category et ValidationLevel.
 *
 * @author chatellier
 */
public class ValidatorsTreeRenderer extends DefaultTreeCellRenderer {


    private static final long serialVersionUID = -2211918491839391988L;

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean sel, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        Object stringValue = value;

        if (value instanceof Category) {
            stringValue = t(((Category) value).getTranslationKey());
        } else if (value instanceof ValidationLevel) {
            // en attendant mieux
            stringValue = t(((ValidationLevel) value).getXWorkContext());
        }
        return super.getTreeCellRendererComponent(tree, stringValue, sel, expanded, leaf,
                                                  row, hasFocus);
    }
}

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.util;

import fr.ifremer.coser.util.ProgressMonitor;

import javax.swing.JProgressBar;

import static org.nuiton.i18n.I18n.t;

/**
 * Control progress bar.
 *
 * @author chatellier
 */
public class CoserProgressBar extends JProgressBar implements ProgressMonitor {


    private static final long serialVersionUID = 3383288562693714316L;

    /** Default to 1 (= no step). */
    protected int stepCount = 1;

    protected int step;

    /*
     * @see fr.ifremer.coser.control.ProgressMonitor#setStepCount(int)
     */
    @Override
    public void setStepCount(int stepCount) {
        this.stepCount = stepCount;
    }

    /*
     * @see fr.ifremer.coser.control.ProgressMonitor#setStep(int)
     */
    @Override
    public void setStep(int step) {
        this.step = step;
    }

    /*
     * @see fr.ifremer.coser.control.ProgressMonitor#nextStep()
     */
    @Override
    public void nextStep() {
        step++;
    }

    /*
     * @see fr.ifremer.coser.control.ProgressMonitor#setCurrent(double)
     */
    @Override
    public void setCurrent(int current) {
        setValue(current);
    }

    /*
     * @see fr.ifremer.coser.control.ProgressMonitor#setTotal(double)
     */
    @Override
    public void setTotal(int total) {
        setMaximum(total);
    }

    @Override
    public void setText(String text) {
        if (stepCount <= 1) {
            setString(text);
        } else {
            setString(t("coser.ui.control.progressStep", step + 1, stepCount, text));
        }
    }

    /*
     * @see fr.ifremer.coser.util.ProgressMonitor#addCurrent(int)
     */
    @Override
    public void addCurrent(int addition) {
        int current = getValue();
        current += addition;
        setValue(current);
    }
}

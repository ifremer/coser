/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.result;

import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.RSufiResultPath;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Modele respresant une liste de resultat avec details sur les resulats
 * affichés et options de selection.
 *
 * @author chatellier
 */
@Deprecated
public class RsufiResultTableModel extends AbstractTableModel {


    private static final long serialVersionUID = 6404018386062830677L;

    /** Les données de la table. */
    protected List<RSufiResultPath> resultPaths;

    /** Les résultats marqués comme etant des données d'indicateurs. */
    protected Set<RSufiResultPath> indicatorResults;

    /** Les résultats marqué comme étant des données de map. */
    protected Set<RSufiResultPath> mapResults;

    /** Les résultat dont la publication des données est autorisée. */
    protected Set<RSufiResultPath> publishDataResults;

    /** Selected table tablemodel (do not show all columns). */
    protected boolean selected;

    public RsufiResultTableModel(boolean selected) {
        this.selected = selected;

        // les selections ne sont jamais supprimé
        // mais vu l'equivalence equals/hascode
        // ca ne doit pas poser de problemes
        indicatorResults = new HashSet<RSufiResultPath>();
        mapResults = new HashSet<RSufiResultPath>();
        publishDataResults = new HashSet<RSufiResultPath>();
    }

    public void setResultPaths(List<RSufiResultPath> resultPaths) {
        this.resultPaths = resultPaths;
        fireTableDataChanged();
    }

    public List<RSufiResultPath> getResultPaths() {
        return resultPaths;
    }

    public Set<RSufiResultPath> getIndicatorResults() {
        return indicatorResults;
    }

    public Set<RSufiResultPath> getMapResults() {
        return mapResults;
    }

    public Set<RSufiResultPath> getPublishDataResults() {
        return publishDataResults;
    }

    /*
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        int result = 0;
        if (resultPaths != null) {
            result = resultPaths.size();
        }
        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result = null;
        switch (column) {
            case 0:
                result = t("coser.ui.uploadresult.creationDate");
                break;
            case 1:
                result = t("coser.ui.uploadresult.path");
                break;
            case 2:
                result = t("coser.ui.uploadresult.zone");
                break;
            case 3:
                result = t("coser.ui.uploadresult.indicatorResult");
                break;
            case 4:
                result = t("coser.ui.uploadresult.mapResult");
                break;
            case 5:
                result = t("coser.ui.uploadresult.publishData");
                break;
        }
        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result = null;
        switch (columnIndex) {
            case 0:
                result = Date.class;
                break;
            case 1:
                result = String[].class;
                break;
            case 2:
                result = String.class;
                break;
            case 3:
                result = Boolean.class;
                break;
            case 4:
                result = Boolean.class;
                break;
            case 5:
                result = Boolean.class;
                break;
        }
        return result;
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        int result = 3;
        if (selected) {
            result = 6;
        }
        return result;
    }

    /*
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result = null;

        RSufiResultPath data = resultPaths.get(rowIndex);
        RSufiResult rsufiResult = data.getRsufiResult();
        switch (columnIndex) {
            case 0:
                result = rsufiResult.getCreationDate();
                break;
            case 1:
                result = data;
                break;
            case 2:
                result = rsufiResult.getZone();
                break;
            case 3:
                result = indicatorResults.contains(data);
                break;
            case 4:
                result = mapResults.contains(data);
                break;
            case 5:
                result = publishDataResults.contains(data);
                break;
        }

        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex >= 3;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        RSufiResultPath data = resultPaths.get(rowIndex);

        if (columnIndex == 3) {
            Boolean bValue = (Boolean) aValue;
            if (bValue.booleanValue()) {
                indicatorResults.add(data);
            } else {
                indicatorResults.remove(data);
            }
        } else if (columnIndex == 4) {
            Boolean bValue = (Boolean) aValue;
            if (bValue.booleanValue()) {
                mapResults.add(data);
            } else {
                mapResults.remove(data);
            }
        } else if (columnIndex == 5) {
            Boolean bValue = (Boolean) aValue;
            if (bValue.booleanValue()) {
                publishDataResults.add(data);
            } else {
                publishDataResults.remove(data);
            }
        }
    }
}

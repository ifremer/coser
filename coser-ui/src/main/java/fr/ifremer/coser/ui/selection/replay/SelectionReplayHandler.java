/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.replay;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.services.CommandService;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.ui.CoserFrame;
import fr.ifremer.coser.ui.common.CommonHandler;
import fr.ifremer.coser.ui.common.SpeciesListRenderer;
import fr.ifremer.coser.ui.util.CoserListSelectionModel;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.SortedMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Specific handler for selection replay view.
 *
 * @author chatellier
 */
public class SelectionReplayHandler extends CommonHandler {

    private static final Log log = LogFactory.getLog(SelectionReplayHandler.class);

    /**
     * Init view by displaying all selection available in all project.
     *
     * @param view view to init
     */
    public void initReplayView(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        SortedMap<String, List<String>> selectionsByProject = projectService.getSelectionByProject();

        SelectionByProjectTreeModel treeModel = new SelectionByProjectTreeModel(selectionsByProject);
        view.getSelectionByProjectTree().setModel(treeModel);

        // set list renderer with project to replace species code
        view.getCommandList().setCellRenderer(new CommandListRenderer(project));
    }

    /**
     * Validation de la selection a rejouer (etape 1).
     *
     * @param view view
     */
    public void validSelectionChoice(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        TreePath selectedSelection = view.getSelectionByProjectTree().getSelectionPath();

        try {
            setWaitCursor(view);

            String selectionName = (String) selectedSelection.getLastPathComponent();
            String projectName = (String) selectedSelection.getParentPath().getLastPathComponent();

            // load selection
            Selection replayedSelection = projectService.openSelection(projectName, selectionName);
            view.setReplayedSelection(replayedSelection);

            // load current new selection
            Selection selection = projectService.initProjectSelection(project);
            view.setSelection(selection);
            // hack, binding won't work
            view.getValidatorSelection().setBean(selection);

            // init next step
            view.getYearsListModel().setYears(selection.getAllYears());
            ((CoserListSelectionModel) view.getYearsList().getSelectionModel()).setSelectedObjects(replayedSelection.getSelectedYears());

            // affichage d'un message si la selection de liste n'est pas
            // cohérente
            Collection<String> remaindYear = CollectionUtils.subtract(replayedSelection.getSelectedYears(), selection.getAllYears());
            if (!remaindYear.isEmpty()) {
                String yearsAsString = StringUtils.join(remaindYear, ", ");
                String message = t("coser.ui.selection.replay.missingyears", yearsAsString);
                view.getMessageArea2().setText(message);
            }

            view.getWizardLayout().show(view.getWizardPanel(), "step2");
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't load selection properties", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Validation des années sélectionnées (etape 2)
     *
     * @param view view
     */
    public void validSelectionYears(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getSelection();
        Selection replayedSelection = view.getReplayedSelection();

        try {
            setWaitCursor(view);

            // get selected zones as list
            Object[] selectedDates = view.getYearsList().getSelectedValues();
            List<String> years = new ArrayList<String>(selectedDates.length);
            for (Object selectedDate : selectedDates) {
                years.add((String) selectedDate);
            }

            if (log.isDebugEnabled()) {
                log.debug("Refreshing strata list");
            }

            // filterDataYearsAndGetStrata do selection.setSelectedYears(years);
            // don't set yourself
            List<String> strata = projectService.filterDataYearsAndGetStrata(project, selection, years);
            view.getStrataListModel().setStrata(strata);

            ((CoserListSelectionModel) view.getStrataList().getSelectionModel()).setSelectedObjects(replayedSelection.getSelectedStrata());

            if (log.isDebugEnabled()) {
                log.debug("Strata list refreshed");
            }

            // affichage d'un message si la selection de liste n'est pas
            // cohérente
            Collection<String> remaindStrata = CollectionUtils.subtract(replayedSelection.getSelectedStrata(), strata);
            if (!remaindStrata.isEmpty()) {
                String strataAsString = StringUtils.join(remaindStrata, ", ");
                String message = t("coser.ui.selection.replay.missingstrata", strataAsString);
                view.getMessageArea3().setText(message);
            }

            view.getWizardLayout().show(view.getWizardPanel(), "step3");

        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't save selected years", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Validation des strates (étape 3).
     *
     * @param view view
     */
    public void validSelectionStrata(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getSelection();
        Selection replayedSelection = view.getReplayedSelection();

        try {
            setWaitCursor(view);

            if (log.isDebugEnabled()) {
                log.debug("Strata list selection changed, updating species list");
            }

            // get selected zones as list
            Object[] selectedStrata = view.getStrataList().getSelectedValues();
            List<String> strata = new ArrayList<String>(selectedStrata.length);
            for (Object selectedStratum : selectedStrata) {
                strata.add((String) selectedStratum);
            }

            // do selection.setSelectedStrata(strata);
            projectService.filterDataStrata(project, selection, strata);

            // init next step
            List<Command> commands = replayedSelection.getHistoryCommands();
            if (CollectionUtils.isNotEmpty(commands)) {
                view.getCommandListModel().setCommands(commands);
                view.getWizardLayout().show(view.getWizardPanel(), "step4");
            } else {
                // appel de la prochaine etape pour initialiser la liste
                validSelectionCommand(view);
            }

        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Validation des commands (fusion, etc...).
     *
     * @param view view
     */
    public void validSelectionCommand(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        CommandService commandService = view.getContextValue(CommandService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getSelection();
        Selection replayedSelection = view.getReplayedSelection();

        try {
            setWaitCursor(view);

            // replay all commands
            List<Command> commands = view.getCommandListModel().getCommands();
            if (commands != null) { // can be
                for (Command command : commands) {
                    if (log.isDebugEnabled()) {
                        log.debug("Replay command " + command);
                    }
                    commandService.doAction(command, project, selection);
                }
            }

            // init next step
            LinkedHashMap<String, String> reftaxSpecies = project.getRefTaxSpeciesMap();
            List<String> currentSpecies = projectService.getProjectSpecies(selection, project, null);
            currentSpecies = CoserUtils.sortCollectionWithMapKeys(reftaxSpecies, currentSpecies);

            view.getSelectedSpeciesListModel().setSpecies(currentSpecies);
            view.getSelectedSpeciesList().setCellRenderer(new SpeciesListRenderer(reftaxSpecies));
            ((CoserListSelectionModel) view.getSelectedSpeciesList().getSelectionModel()).setSelectedObjects(replayedSelection.getSelectedSpecies());

            // affichage d'un message si la selection de liste n'est pas
            // cohérente
            Collection<String> remaindSpecies = CollectionUtils.subtract(replayedSelection.getSelectedSpecies(), currentSpecies);
            if (!remaindSpecies.isEmpty()) {
                String separator = "";
                StringBuilder speciesBuilder = new StringBuilder(256);
                for (String species : remaindSpecies) {
                    speciesBuilder.append(separator);
                    speciesBuilder.append(project.getDisplaySpeciesText(species));
                    separator = ", ";
                }
                String message = t("coser.ui.selection.replay.missingspecies", speciesBuilder.toString());
                view.getMessageArea5().setText(message);
            }

            view.getWizardLayout().show(view.getWizardPanel(), "step5");

        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't replay species merge command", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Validation des strates.
     *
     * @param view view
     */
    public void validSelectionSpecies(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getSelection();
        Selection replayedSelection = view.getReplayedSelection();

        try {
            setWaitCursor(view);

            // get selected species
            // get selected zones as list
            Object[] selectedSpeciesArr = view.getSelectedSpeciesList().getSelectedValues();
            List<String> selectedSpecies = new ArrayList<String>(selectedSpeciesArr.length);
            for (Object selectedSingleSpecies : selectedSpeciesArr) {
                selectedSpecies.add((String) selectedSingleSpecies);
            }
            projectService.filterDataSpecies(project, selection, selectedSpecies);

            // auto selection des listes L2 à L4
            projectService.fillListsSelection(selection,
                                              replayedSelection.getSelectedSpeciesOccDens(),
                                              replayedSelection.getSelectedSpeciesSizeAllYear(),
                                              replayedSelection.getSelectedSpeciesMaturity());

            // init next step
            view.getWizardLayout().show(view.getWizardPanel(), "step6");

        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Validation finale de la selection.
     *
     * @param view view
     */
    public void validSelectionInfos(SelectionReplayView view) {
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = view.getSelection();

        try {
            setWaitCursor(view);

            projectService.createProjectSelection(project, selection);

            // autochargement de la selection dans la fenetre parente
            CoserFrame parent = view.getContextValue(CoserFrame.class, JAXXUtil.PARENT);
            // autoselection de l'onglet "listes"
            parent.getHandler().showSelectionView(selection, 1);

            JOptionPane.showMessageDialog(view, t("coser.ui.selection.selectionCreated"),
                                          t("coser.ui.selection.replay.replayTitle"), JOptionPane.INFORMATION_MESSAGE);

            // hide current frame
            view.dispose();

        } catch (CoserBusinessException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't save selection", ex);
            }
            JOptionPane.showMessageDialog(view, ex.getMessage(), t("coser.ui.selection.saveError"),
                                          JOptionPane.ERROR_MESSAGE);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Validation des étapes 1 a la dernière.
     *
     * @param view view
     */
    public void finishSelection1toFinal(SelectionReplayView view) {
        validSelectionChoice(view);
        finishSelection2toFinal(view);
    }

    /**
     * Validation des étapes 2 a la dernière.
     *
     * @param view view
     */
    public void finishSelection2toFinal(SelectionReplayView view) {
        validSelectionYears(view);
        finishSelection3toFinal(view);
    }

    /**
     * Validation des étapes 3 a la dernière.
     *
     * @param view view
     */
    public void finishSelection3toFinal(SelectionReplayView view) {
        validSelectionStrata(view);
        finishSelection4toFinal(view);
    }

    /**
     * Validation des étapes 4 a la dernière.
     *
     * @param view view
     */
    public void finishSelection4toFinal(SelectionReplayView view) {
        validSelectionCommand(view);
        finishSelection5toFinal(view);
    }

    /**
     * Validation des étapes 5 a la dernière.
     * (ne valide pas la dernière : infos de selection obligatoires)
     *
     * @param view view
     */
    public void finishSelection5toFinal(SelectionReplayView view) {
        validSelectionSpecies(view);
    }
}

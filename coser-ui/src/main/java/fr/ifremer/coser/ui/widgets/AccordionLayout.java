/* %%Ignore-License
 * Copyright (C) 2007 Craig Knudsen
 *
 * AccordionPane is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License can be found at www.gnu.org. 
 * To receive a hard copy, you can write to:
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307 USA.
 */

package fr.ifremer.coser.ui.widgets;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;

public class AccordionLayout implements LayoutManager {

    protected AccordionPane accordionPane;

    /**
     * Constructs a AccordionLayout.
     *
     * @param accordionPane accordionPane
     */
    public AccordionLayout(AccordionPane accordionPane) {
        this.accordionPane = accordionPane;
    }

    protected Dimension layoutSize(Container parent, boolean minimum) {
        Dimension dim = new Dimension(0, 0);
        synchronized (parent.getTreeLock()) {
            int n = parent.getComponentCount();
            int cnt = 0;
            for (int i = 0; i < n; i++) {
                Component c = parent.getComponent(i);
                if (c.isVisible()) {
                    Dimension d = (minimum) ? c.getMinimumSize() : c
                            .getPreferredSize();
                    dim.height += d.height;
                    if (d.width > dim.width) {
                        dim.width = d.width;
                    }
                }
                cnt++;
                if (cnt == accordionPane.children.size()) {
                    break;
                }
            }
        }
        Insets insets = parent.getInsets();
        dim.width += insets.left + insets.right;
        dim.height += insets.top + insets.bottom;
        return dim;
    }

    /**
     * Lays out the container.
     */
    @Override
    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();
        synchronized (parent.getTreeLock()) {
            int n = parent.getComponentCount();
            Dimension pd = parent.getSize();
            // do layout
            int cnt = 0;
            int totalhei = pd.height - insets.top - insets.bottom;
            int x = insets.left;
            int y = insets.top;
            int selInd = accordionPane.selected;
            int numPanes = accordionPane.children.size();
            // Calculate the remainder and set it in the AccordionPane in case the
            // JPanel was resized.
            AccordionPaneSubPanel firstSubPanel = (AccordionPaneSubPanel) parent
                    .getComponent(0);
            // Get height of title area
            int remainder = totalhei
                            - (firstSubPanel.titlePanel.getHeight() * numPanes);
            accordionPane.remainder = remainder;
            for (int i = 0; i < n; i++) {
                Component c = parent.getComponent(i);
                AccordionPaneSubPanel subPanel = (AccordionPaneSubPanel) c;
                // Get height of title area
                int titleH = subPanel.titlePanel.getHeight();
                if (accordionPane.transitionStep > 0) {
                    // We're in the middle of the animation
                    if (subPanel.index == selInd) {
                        int hei = titleH + accordionPane.transitionStep;
                        c.setBounds(x, y,
                                    pd.width - insets.left - insets.right, hei);
                        y += hei;
                    } else if (subPanel.index == accordionPane.previouslySelected) {
                        int hei = titleH
                                  + (remainder - accordionPane.transitionStep);
                        c.setBounds(x, y,
                                    pd.width - insets.left - insets.right, hei);
                        y += hei;
                    } else {
                        int hei = titleH;
                        c.setBounds(x, y,
                                    pd.width - insets.left - insets.right, hei);
                        y += hei;
                    }
                } else {
                    if (subPanel.index == selInd) {
                        int hei = titleH + remainder;
                        c.setBounds(x, y,
                                    pd.width - insets.left - insets.right, hei);
                        y += hei;
                    } else {
                        int hei = titleH;
                        c.setBounds(x, y,
                                    pd.width - insets.left - insets.right, hei);
                        y += hei;
                    }
                }
                cnt++;
                if (cnt == numPanes) {
                    break;
                }
            }
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return layoutSize(parent, false);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return layoutSize(parent, false);
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {

    }

    @Override
    public void removeLayoutComponent(Component comp) {

    }
}
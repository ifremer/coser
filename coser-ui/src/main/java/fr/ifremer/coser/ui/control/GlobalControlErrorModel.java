/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserConstants.ValidationLevel;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.control.ControlErrorGroup;
import fr.ifremer.coser.control.DiffCatchLengthControlError;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Global validation table model.
 *
 * @author chatellier
 */
public class GlobalControlErrorModel extends DefaultTreeTableModel {

    protected List<ControlError> controlErrors;

    protected Set<Object> checkedControlErrors;

    public GlobalControlErrorModel() {
        super(null);
    }

    public void setControlErrors(Project project, List<ControlError> controlErrors) {
        this.controlErrors = controlErrors;
        buildGlobalControlErrorTreeModel(project, controlErrors);
        checkedControlErrors = new HashSet<Object>();
    }

    public List<ControlError> getControlErrors() {
        return controlErrors;
    }

    /**
     * Utilise une simple liste d'erreur issue du contôle pour en faire
     * une représentation sous forme d'arbre pour l'utilisateur.
     *
     * @param project          project (to get species real names)
     * @param validationErrors validation errors list to render
     */
    protected void buildGlobalControlErrorTreeModel(Project project, List<ControlError> validationErrors) {

        Map<Object, DefaultMutableTreeTableNode> nodeCache = new HashMap<Object, DefaultMutableTreeTableNode>();
        DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode(null);

        for (ControlError validationError : validationErrors) {
            Object category = validationError.getCategory() == null ?
                              n("coser.ui.control.error.allCategories") : validationError.getCategory();

            // definition du noeud categorie
            DefaultMutableTreeTableNode categoryNode = nodeCache.get(category);
            if (categoryNode == null) {
                categoryNode = new DefaultMutableTreeTableNode(category);
                root.add(categoryNode);
                nodeCache.put(category, categoryNode);
            }

            // definition du noeud de regroupement
            // des erreurs de categories communes
            // (utilisation de category + validationError.getMessage() car les messages peuvent être similaires
            //  dans plusieurs catégories)
            DefaultMutableTreeTableNode parentNode = nodeCache.get(category + validationError.getMessage());
            if (parentNode == null) {
                ControlErrorGroup group = new ControlErrorGroup(validationError.getCategory(), validationError.getLevel(), validationError.getMessage());
                parentNode = new DefaultMutableTreeTableNode(group);
                categoryNode.add(parentNode);
                nodeCache.put(category + validationError.getMessage(), parentNode);
            }

            // cas special pour les erreurs DiffCatchLengthControlError
            // regroupement supplémentaire par espèce
            if (validationError instanceof DiffCatchLengthControlError) {
                DiffCatchLengthControlError diffCatchError = (DiffCatchLengthControlError) validationError;
                String species = diffCatchError.getSpecies();
                String speciesCategory = t("coser.ui.control.error.diffcatchlenghtspecies",
                                           project.getDisplaySpeciesText(species));

                ControlErrorGroup group = new ControlErrorGroup(validationError.getCategory(), validationError.getLevel(), speciesCategory);
                DefaultMutableTreeTableNode newParent = nodeCache.get(speciesCategory);
                if (newParent == null) {
                    newParent = new DefaultMutableTreeTableNode(group);
                    parentNode.add(newParent);
                    nodeCache.put(speciesCategory, newParent);
                }
                parentNode = newParent;
            }

            // ajout de l'erreur dans sa categories
            DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(validationError);
            parentNode.add(node);
        }

        setRoot(root);
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 2;
    }


    @Override
    public String getColumnName(int columnIndex) {

        String result = null;

        switch (columnIndex) {
            case 0:
                result = t("coser.ui.control.global.message");
                break;
        }
        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result = null;

        switch (columnIndex) {
            case 0:
                result = ValidationLevel.class;
                break;
            case 1:
                result = Boolean.class;
                break;
        }
        return result;
    }

    /*
     * @see org.jdesktop.swingx.treetable.TreeTableModel#getValueAt(java.lang.Object, int)
     */
    @Override
    public Object getValueAt(Object node, int column) {

        Object result = null;

        switch (column) {
            case 0:
                result = node;
                break;
            default:
                result = checkedControlErrors.contains(node);
                break;
        }

        return result;
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        boolean result = false;
        if (column == 1) {
            result = true;
        }
        return result;
    }

    @Override
    public void setValueAt(Object value, Object node, int column) {

        Boolean booleanValue = (Boolean) value;
        if (booleanValue.booleanValue()) {
            checkedControlErrors.add(node);
        } else {
            checkedControlErrors.remove(node);
        }

        // recursive check of sub errors
        int childCount = getChildCount(node);
        for (int i = 0; i < childCount; ++i) {
            Object child = getChild(node, i);
            setValueAt(value, child, column);
        }
    }
}

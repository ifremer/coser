/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserConstants.ValidationLevel;
import fr.ifremer.coser.CoserException;
import fr.ifremer.coser.CoserUtils;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.control.ControlError;
import fr.ifremer.coser.control.ControlErrorGroup;
import fr.ifremer.coser.control.SpeciesControlError;
import fr.ifremer.coser.control.SpeciesLengthControlError;
import fr.ifremer.coser.data.AbstractDataEntity;
import fr.ifremer.coser.data.Catch;
import fr.ifremer.coser.data.Haul;
import fr.ifremer.coser.data.Length;
import fr.ifremer.coser.data.Strata;
import fr.ifremer.coser.services.CommandService;
import fr.ifremer.coser.services.ControlService;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.services.PublicationService;
import fr.ifremer.coser.ui.common.DataHandler;
import fr.ifremer.coser.ui.common.SpeciesListRenderer;
import fr.ifremer.coser.ui.common.SpeciesTableCellRenderer;
import fr.ifremer.coser.ui.util.CoserProgressBar;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.session.SwingSession;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.beans.Introspector;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.nuiton.i18n.I18n.t;

/**
 * Control handler for control view.
 *
 * @author chatellier
 */
public class ControlHandler extends DataHandler {

    private static final Log log = LogFactory.getLog(ControlHandler.class);

    /**
     * Init control view.
     *
     * @param controlView view to init
     */
    public void init(ControlView controlView) {
        CoserConfig config = controlView.getContextValue(CoserConfig.class);
        boolean localUsed = config.getValidatorsDirectory().isDirectory();
        controlView.getLocalControlUsedLabel().setVisible(localUsed);

        // fixe les editeurs qui affiche les noms d'espèces différement
        // de leur nom de stockage
        Project project = controlView.getContextValue(Project.class);
        SpeciesTableCellRenderer renderer = new SpeciesTableCellRenderer(project.getRefTaxSpeciesMap());
        controlView.getControlDataTableCatch().getColumnModel().getColumn(4).setCellRenderer(renderer);
        controlView.getControlDataTableLength().getColumnModel().getColumn(4).setCellRenderer(renderer);
    }

    /**
     * Return l'intance de la table qui est selectionnée.
     *
     * Les 4 tables sont dupliquées juste pour conserver l'etat des colonnes.
     *
     * @param controlView view
     * @return la table affichée (suivante la categorie)
     */
    protected JTable getControlDataTable(ControlView controlView) {
        Category category = (Category) controlView.getCategoryComboBox().getSelectedItem();
        JTable controlDataTable = null;

        switch (category) {
            case CATCH:
                controlDataTable = controlView.getControlDataTableCatch();
                break;
            case HAUL:
                controlDataTable = controlView.getControlDataTableHaul();
                break;
            case LENGTH:
                controlDataTable = controlView.getControlDataTableLength();
                break;
            case STRATA:
                controlDataTable = controlView.getControlDataTableStrata();
                break;
        }
        return controlDataTable;
    }

    /**
     * Selection d'une nouvelle categories dans la liste des categories.
     *
     * @param controlView controlView
     * @param event       event
     */
    public void categoryChanged(ControlView controlView, ActionEvent event) {
        Category category = (Category) controlView.getCategoryComboBox().getSelectedItem();
        controlView.getControlTablesLayout().show(controlView.getControlTablesPanel(), category.toString());
        clearAllSelection(controlView);
    }

    /**
     * Vide la selection des 4 tables pour que le bean en cours d'edition
     * soit vidé et que le changement de categories n'arrive pas
     * dans un etat de selection incoherent.
     *
     * @param controlView controlView
     */
    protected void clearAllSelection(ControlView controlView) {
        controlView.getControlDataTableSelectionModelCatch().clearSelection();
        controlView.getControlDataTableSelectionModelHaul().clearSelection();
        controlView.getControlDataTableSelectionModelLength().clearSelection();
        controlView.getControlDataTableSelectionModelStrata().clearSelection();
    }

    /**
     * Affiche le menu contextuel de la table qui contient les données.
     *
     * @param controlView view
     * @param event       mouse event
     */
    public void showDataTableContextMenu(final ControlView controlView, MouseEvent event) {

        // clic contextuel
        if (event.getButton() == MouseEvent.BUTTON3) {
            JTable displayedTable = getControlDataTable(controlView);
            int[] dataSelectedRows = displayedTable.getSelectedRows();
            final int columnIndex = displayedTable.getColumnModel().getColumnIndexAtX(event.getX());

            JPopupMenu popupMenu = new JPopupMenu(t("coser.ui.control.dataMenuLabel"));

            // plusieurs lignes selectionnées et pas la premiere colonne (Line index)
            if (dataSelectedRows.length > 0 && columnIndex > 0) {
                ControlTableModel controlDataModel = (ControlTableModel) displayedTable.getModel();
                final String columnName = displayedTable.getColumnName(columnIndex);
                final String firstValue = (String) controlDataModel.getValueAt(dataSelectedRows[0], columnIndex);

                // replace in selection
                JMenuItem replaceMenu = new JMenuItem(t("coser.ui.control.dataMenuReplace", columnName));
                replaceMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ControlFindReplaceDialog viewDialog = new ControlFindReplaceDialog(controlView);
                        viewDialog.setHandler(ControlHandler.this);
                        viewDialog.setColumnIndex(columnIndex);
                        viewDialog.setReplaceInSelection(true);
                        viewDialog.getReplaceFieldNameLabel().setText(columnName);
                        viewDialog.getReplaceFindField().setText(firstValue);
                        viewDialog.setLocationRelativeTo(controlView);
                        viewDialog.setVisible(true);
                    }
                });
                popupMenu.add(replaceMenu);
            }

            // replace in everywhere
            if (columnIndex > 0) {
                final String columnName = displayedTable.getColumnName(columnIndex);
                JMenuItem replaceAllMenu = new JMenuItem(t("coser.ui.control.dataMenuReplaceAll", columnName));
                replaceAllMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ControlFindReplaceDialog viewDialog = new ControlFindReplaceDialog(controlView);
                        viewDialog.setHandler(ControlHandler.this);
                        viewDialog.setColumnIndex(columnIndex);
                        viewDialog.getReplaceFieldNameLabel().setText(columnName);
                        viewDialog.setLocationRelativeTo(controlView);
                        viewDialog.setVisible(true);
                    }
                });
                popupMenu.add(replaceAllMenu);
            }

            // delete selected
            if (dataSelectedRows.length > 0) {

                JMenuItem deleteSelectedMenu = new JMenuItem(t("coser.ui.control.dataMenuDeleteSelected"));
                deleteSelectedMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        deletedSelectedDataLines(controlView);
                    }
                });
                popupMenu.add(deleteSelectedMenu);
            }

            popupMenu.show(displayedTable, event.getX(), event.getY());
        }
    }

    /**
     * Delete selected line in data table (called from context menu).
     *
     * @param controlView view
     */
    protected void deletedSelectedDataLines(ControlView controlView) {

        JTable controlDataTable = getControlDataTable(controlView);
        int[] selectedLines = controlDataTable.getSelectedRows();
        if (ArrayUtils.isNotEmpty(selectedLines)) {

            int response = JOptionPane.showConfirmDialog(controlView, t("coser.ui.control.confirmDeletionsMessage", selectedLines.length),
                                                         t("coser.ui.control.confirmDeletionTitle"),
                                                         JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {

                // disableValidAction
                disableValidAction(controlView);

                Project project = controlView.getContextValue(Project.class);
                ProjectService projectService = controlView.getContextValue(ProjectService.class);
                Category category = (Category) controlView.getCategoryComboBox().getSelectedItem();

                ControlTableModel model = (ControlTableModel) controlDataTable.getModel();
                String commandUUID = UUID.randomUUID().toString();

                // il faut le faire à l'envers, sinon, les index de
                // selection change en cours de route
                for (int indexSelected = selectedLines.length - 1; indexSelected >= 0; --indexSelected) {
                    int selectedLine = selectedLines[indexSelected];
                    String[] data = model.getDataAt(selectedLine);
                    try {
                        projectService.deleteData(project, project.getControl(), category, data[AbstractDataEntity.INDEX_LINE], commandUUID);
                        model.fireTableRowsDeleted(selectedLine, selectedLine);
                    } catch (CoserBusinessException ex) {
                        throw new CoserException("Can't delete data", ex);
                    }
                }
            }
        }
    }

    /**
     * Perform find and replace.
     *
     * @param replaceView view
     */
    public void performFindAndReplace(ControlFindReplaceDialog replaceView) {

        ControlView controlView = replaceView.getContextValue(ControlView.class, JAXXUtil.PARENT);
        Category category = (Category) controlView.getCategoryComboBox().getSelectedItem();
        JTable displayedTable = getControlDataTable(controlView);
        ControlTableModel controlDataModel = (ControlTableModel) displayedTable.getModel();
        ProjectService projectService = replaceView.getContextValue(ProjectService.class);
        Project project = replaceView.getContextValue(Project.class);

        // find en field name
        String[] enHeaders = null;
        switch (category) {
            case CATCH:
                enHeaders = Catch.EN_HEADERS;
                break;
            case HAUL:
                enHeaders = Haul.EN_HEADERS;
                break;
            case LENGTH:
                enHeaders = Length.EN_HEADERS;
                break;
            case STRATA:
                enHeaders = Strata.EN_HEADERS;
                break;
        }

        // -1 because table model contains line number
        String field = enHeaders[replaceView.getColumnIndex() - 1];
        String find = replaceView.getReplaceFindField().getText();
        String replace = replaceView.getReplaceReplaceField().getText();
        if (log.isWarnEnabled()) {
            log.warn("Perform : " + find + " replace with " + replace);
        }

        String commandUUID = UUID.randomUUID().toString();

        try {

            int replaceCount = 0;

            if (replaceView.isReplaceInSelection()) {
                // on suppose que la fenetre est modale, et
                // que la selection ne peut pas avoir changée
                int[] dataSelectedRows = displayedTable.getSelectedRows();
                for (int dataSelectedRow : dataSelectedRows) {
                    String[] data = controlDataModel.getDataAt(dataSelectedRow);
                    boolean replaced = projectService.replaceFieldValue(project, category, field, find, replace, data, false, commandUUID);

                    if (replaced) {
                        controlDataModel.fireTableRowsUpdated(dataSelectedRow, dataSelectedRow);
                        replaceCount++;
                    }
                }
            } else {
                int rowCount = controlDataModel.getRowCount();
                for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
                    String[] data = controlDataModel.getDataAt(rowIndex);
                    boolean replaced = projectService.replaceFieldValue(project, category, field, find, replace, data, false, commandUUID);

                    if (replaced) {
                        controlDataModel.fireTableRowsUpdated(rowIndex, rowIndex);
                        replaceCount++;
                    }
                }
            }

            // message de confirmation
            JOptionPane.showMessageDialog(replaceView, t("coser.ui.control.replace.replacedCount", replaceCount),
                                          t("coser.ui.control.replace.title"), JOptionPane.INFORMATION_MESSAGE);

        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't replace field value", ex);
        }
        replaceView.dispose();

        // bufix
        // si le remplacement de masse a modifié la ligne courante, il
        // faut la rafraichir
        controlDataTableSelectionChanged(controlView);
    }

    /**
     * Check project.
     *
     * @param view view
     */
    public void checkData(final ControlView view) {

        // clear selection and error message when check data start
        clearAllSelection(view);

        final ControlService validationService = view.getContextValue(ControlService.class);
        final Project project = view.getContextValue(Project.class);
        final CoserProgressBar progressBar = view.getCheckProgressBar();

        SwingWorker<List<ControlError>, Void> task = new SwingWorker<List<ControlError>, Void>() {

            protected long before = System.currentTimeMillis();

            @Override
            protected List<ControlError> doInBackground() {

                List<ControlError> validationErrors = validationService.validateData(project, project.getControl(), progressBar);
                return validationErrors;
            }

            @Override
            protected void done() {

                if (log.isInfoEnabled()) {
                    long after = System.currentTimeMillis();
                    log.info("Validation file checked in " + (after - before) + "ms");
                }

                try {

                    // genere la liste des graphes a se moment
                    PublicationService publicationService = view.getContextValue(PublicationService.class);
                    Map<String, JFreeChart> charts = publicationService.getCompareCatchLengthGraph(project, project.getControl(), null);
                    view.setContextValue(charts, "CompareCatchLengthGraph");
                    view.getCompareNumberCatchLengthButton().setEnabled(true);

                    // hack parce que impossible à faire
                    // sans que les colonnes soit redimentionnées
                    view.getGlobalControlErrorTable().setAutoCreateColumnsFromModel(false);

                    List<ControlError> errors = get();
                    view.getGlobalControlErrorModel().setControlErrors(project, errors);

                    // set errors list in each table model
                    ControlDuplicatedLineTableModel catchModel = (ControlDuplicatedLineTableModel) view.getControlDataTableCatch().getModel();
                    catchModel.setControlErrors(errors);
                    catchModel = (ControlDuplicatedLineTableModel) view.getControlDataTableStrata().getModel();
                    catchModel.setControlErrors(errors);
                    catchModel = (ControlDuplicatedLineTableModel) view.getControlDataTableHaul().getModel();
                    catchModel.setControlErrors(errors);
                    catchModel = (ControlDuplicatedLineTableModel) view.getControlDataTableLength().getModel();
                    catchModel.setControlErrors(errors);

                    // active le bouton de sauvegarde si la liste d'erreur
                    // ne contient pas de message d'erreur
                    // warning, c'est ok
                    boolean errorFound = false;
                    Iterator<ControlError> itError = errors.iterator();
                    while (itError.hasNext() && !errorFound) {
                        ControlError error = itError.next();
                        if (error.getLevel() == ValidationLevel.ERROR || error.getLevel() == ValidationLevel.FATAL) {
                            errorFound = true;
                        }
                    }
                    view.setCanValidControl(!errorFound);
                } catch (Exception ex) {
                    throw new CoserException("Can't validate data", ex);
                }
            }


        };
        task.execute();
    }

    /**
     * Called when selection change to display selected bean
     * in edition panel.
     *
     * @param view view
     */
    public void controlDataTableSelectionChanged(ControlView view) {

        JTable table = getControlDataTable(view);
        ControlTableModel model = (ControlTableModel) table.getModel();
        int[] selectedRows = table.getSelectedRows();
        if (selectedRows.length == 1) {
            int selectedRow = selectedRows[0];
            Project project = view.getContextValue(Project.class);
            Category category = (Category) view.getCategoryComboBox().getSelectedItem();
            String[] header = null;
            String[] line = null;
            switch (category) {
                case CATCH:
                    // warning, skip header
                    header = project.getControl().getCatch().get(0);
                    //project.getControl().getCatch().get(selectedRow + 1);
                    break;
                case HAUL:
                    header = project.getControl().getHaul().get(0);
                    //line = project.getControl().getHaul().get(selectedRow + 1);
                    break;
                case LENGTH:
                    header = project.getControl().getLength().get(0);
                    //line = project.getControl().getLength().get(selectedRow + 1);
                    break;
                case STRATA:
                    header = project.getControl().getStrata().get(0);
                    //line = project.getControl().getStrata().get(selectedRow + 1);
                    break;
            }

            // get data from model (because of line order modification)
            line = model.getDataAt(selectedRow);

            // convertion
            updateEditionPanel(view, category, header, line);
        } else {
            // clear edition panel
            updateEditionPanel(view, null, null, null);
        }
    }

    /**
     * Selectionne, dans la table des données, la ligne correspondant
     * a l'erreur selectionnée dans la table des erreurs.
     *
     * @param view  view
     * @param event selection event
     */
    public void showSelectedError(ControlView view, TreeSelectionEvent event) {
        TreePath selectedError = view.getGlobalControlErrorTable().getTreeSelectionModel().getSelectionPath();
        if (selectedError != null && selectedError.getPathCount() >= 3) {
            TreeTableNode cateNode = (TreeTableNode) selectedError.getPathComponent(1);
            Object cateObject = cateNode.getUserObject();
            TreeTableNode lastNode = (TreeTableNode) selectedError.getLastPathComponent();
            Object userObject = lastNode.getUserObject();

            if (cateObject instanceof Category && userObject instanceof ControlError) {
                Category category = (Category) cateObject;

                // swap category
                view.getCategoryComboBoxModel().setSelectedItem(category);

                ControlError error = (ControlError) userObject;
                List<String> errorLineNumbers = error.getLineNumbers();
                // peut être vide, si l'erreur ne porte pas sur un bean en particulier
                if (errorLineNumbers != null) {
                    JTable displayedTable = getControlDataTable(view);
                    ControlTableModel tableModel = (ControlTableModel) displayedTable.getModel();
                    ListSelectionModel selectionModel = displayedTable.getSelectionModel();
                    selectionModel.clearSelection();

                    boolean first = true;
                    for (String errorLineNumber : errorLineNumbers) {
                        int errorLineIndex = tableModel.getRealIndexOfLine(errorLineNumber);

                        // ca peut arriver si la ligne a été supprimée
                        if (errorLineIndex >= 0) {
                            selectionModel.addSelectionInterval(errorLineIndex, errorLineIndex);
                            if (first) {
                                scrollToVisible(displayedTable, errorLineIndex, 0);
                                first = false;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Affiche le menu contextuel de l'arbre qui contient les erreurs de
     * validation globales ou les graphes de comparaison captures/taille
     * dans le cas d'un double clic sur une {@link SpeciesControlError}.
     *
     * @param controlView view
     * @param event       mouse event
     * @see SpeciesControlError
     */
    public void globalErrorTableMouseClicked(final ControlView controlView, MouseEvent event) {

        int eventButton = event.getButton();

        // clic contextuel (clic droit)
        if (eventButton == MouseEvent.BUTTON3) {
            JXTreeTable errorTable = controlView.getGlobalControlErrorTable();
            TreePath selectedError = errorTable.getTreeSelectionModel().getSelectionPath();

            JPopupMenu popupMenu = new JPopupMenu();

            // plusieurs lignes selectionnées et pas la premiere colonne (Line index)
            if (selectedError != null && selectedError.getPathCount() >= 3) {

                TreeTableNode cateNode = (TreeTableNode) selectedError.getPathComponent(1);
                Object cateObject = cateNode.getUserObject();

                if (cateObject instanceof Category) { // ca peut être 'tout' en String
                    final Category category = (Category) cateObject;
                    final TreeTableNode lastNode = (TreeTableNode) selectedError.getLastPathComponent();

                    // selectall n'a d'interet que pour les groupes d'erreurs
                    if (lastNode.getChildCount() > 0) {
                        JMenuItem replaceMenu = new JMenuItem(t("coser.ui.control.globalErrorMenuSelectAll"));
                        replaceMenu.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                selectAllErrorGroupLines(controlView, category, lastNode);
                            }
                        });
                        popupMenu.add(replaceMenu);
                        popupMenu.add(new JSeparator());
                    }
                }
            }

            // fix potentail NPE
            if (errorTable.getRowCount() > 0) {
                JMenuItem generateHtmlMenu = new JMenuItem(t("coser.ui.control.globalErrorMenuGenerateHTML"));
                generateHtmlMenu.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        generateHTMLReport(controlView);
                    }
                });
                popupMenu.add(generateHtmlMenu);
            }

            popupMenu.show(controlView.getGlobalControlErrorTable(), event.getX(), event.getY());
        }

        // double clic (bouton normal)
        else if (eventButton == MouseEvent.BUTTON1 && event.getClickCount() == 2) {
            TreePath selectedError = controlView.getGlobalControlErrorTable().getTreeSelectionModel().getSelectionPath();

            // si la selection est une instance de DiffCatchLengthControlError
            // affiche le graphe avec l'espece correspondante
            if (selectedError != null) {
                TreeTableNode lastComponent = (TreeTableNode) selectedError.getLastPathComponent();
                Object userObject = lastComponent.getUserObject();
                if (userObject instanceof SpeciesControlError) {
                    SpeciesControlError error = (SpeciesControlError) userObject;
                    String species = error.getSpecies();
                    if (userObject instanceof SpeciesLengthControlError) {
                        displayLengthStructureGraph(controlView, species);
                    } else {
                        displayCompareNumberCatchGraph(controlView, species);
                    }
                }
            }
        }
    }

    /**
     * Selectionne toutes les lignes associés au erreur d'un groupe d'erreur.
     *
     * @param controlView controlView
     * @param category    la categorie pour switcher les tables
     * @param lastNode    validationGroup
     */
    protected void selectAllErrorGroupLines(ControlView controlView, Category category, TreeTableNode lastNode) {
        // swap category
        controlView.getCategoryComboBoxModel().setSelectedItem(category);

        // select all lines
        JTable displayedTable = getControlDataTable(controlView);
        ListSelectionModel selectionModel = displayedTable.getSelectionModel();
        selectionModel.clearSelection();
        selectAllErrorGroupLines(controlView, lastNode, true);
    }

    /**
     * Methode recursive qui selection toutes les lignes correspondant à un
     * groupe d'erreurs.
     *
     * @param controlView
     * @param lastNode
     * @param scrollToVisible allow to scroll to visible
     */
    protected void selectAllErrorGroupLines(ControlView controlView, TreeTableNode lastNode, boolean scrollToVisible) {
        GlobalControlErrorModel model = controlView.getGlobalControlErrorModel();
        JTable displayedTable = getControlDataTable(controlView);
        ControlTableModel tableModel = (ControlTableModel) displayedTable.getModel();
        ListSelectionModel selectionModel = displayedTable.getSelectionModel();

        boolean first = scrollToVisible;
        int childCount = model.getChildCount(lastNode);
        for (int indexChild = 0; indexChild < childCount; ++indexChild) {
            TreeTableNode childNode = (TreeTableNode) model.getChild(lastNode, indexChild);
            Object userObject = childNode.getUserObject();

            if (userObject instanceof ControlErrorGroup) { // recursive
                selectAllErrorGroupLines(controlView, childNode, first);
                first = false;
            } else if (userObject instanceof ControlError) {
                ControlError validationError = (ControlError) userObject;
                List<String> errorLineNumbers = validationError.getLineNumbers();

                for (String errorLineNumber : errorLineNumbers) {
                    int errorLineIndex = tableModel.getRealIndexOfLine(errorLineNumber);

                    // ca peut arriver si la ligne a été supprimée
                    if (errorLineIndex >= 0) {
                        selectionModel.addSelectionInterval(errorLineIndex, errorLineIndex);
                        if (first) {
                            scrollToVisible(displayedTable, errorLineIndex, 0);
                            first = false;
                        }
                    }
                }
            }
        }
    }


    /**
     * Scroll le viewport de la table à la ligne demandée.
     *
     * @param table     table
     * @param rowIndex  ligne
     * @param vColIndex colonne
     */
    protected void scrollToVisible(JTable table, int rowIndex, int vColIndex) {
        if (!(table.getParent() instanceof JViewport)) {
            return;
        }
        JViewport viewport = (JViewport) table.getParent();
        // This rectangle is relative to the table where the
        // northwest corner of cell (0,0) is always (0,0).
        Rectangle rect = table.getCellRect(rowIndex, vColIndex, true);
        // The location of the viewport relative to the table
        Point pt = viewport.getViewPosition();
        // Translate the cell location so that it is relative
        // to the view, assuming the northwest corner of the
        // view is (0,0)
        rect.setLocation(rect.x - pt.x, rect.y - pt.y);
        // Scroll the area into view
        viewport.scrollRectToVisible(rect);
    }

    /**
     * Generate global error table model data as html report.
     *
     * @param controlView
     */
    protected void generateHTMLReport(ControlView controlView) {

        PublicationService publicationService = controlView.getContextValue(PublicationService.class);
        Project project = controlView.getContextValue(Project.class);

        // get error list from table model
        List<ControlError> controlErrors = controlView.getGlobalControlErrorModel().getControlErrors();
        File htmlFile = null;
        try {
            htmlFile = publicationService.exportErrorsAsHTML(project, project.getControl(), controlErrors);
            browseFile(controlView, htmlFile);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't generate html report", ex);
        }
    }

    /**
     * Contruit le panel d'edition du bean.
     *
     * Vide le panel si line est {@code null}.
     *
     * @param view     view
     * @param category category
     * @param header   header
     * @param line     line to edit
     */
    protected void updateEditionPanel(ControlView view, Category category, String[] header, String[] line) {

        JPanel panel = view.getEditionPanel();
        panel.removeAll();

        // cas non rentrant pour rafraichir le panel et le vider
        if (category != null && header != null && line != null) {

            Catch beanCatch = null;
            Haul beanHaul = null;
            Length beanLength = null;
            Strata beanStrata = null;
            AbstractDataEntity bean = null;
            SwingValidator<?> validator = null;
            switch (category) {
                case CATCH:
                    beanCatch = new Catch();
                    beanCatch.setData(line);
                    bean = beanCatch;
                    validator = view.getValidatorCatch();
                    break;
                case HAUL:
                    beanHaul = new Haul();
                    beanHaul.setData(line);
                    bean = beanHaul;
                    validator = view.getValidatorHaul();
                    break;
                case LENGTH:
                    beanLength = new Length();
                    beanLength.setData(line);
                    bean = beanLength;
                    validator = view.getValidatorLength();
                    break;
                case STRATA:
                    beanStrata = new Strata();
                    beanStrata.setData(line);
                    bean = beanStrata;
                    validator = view.getValidatorStrata();
                    break;
            }

            // set to all validators to clear non concerned
            view.getValidatorCatch().setBean(beanCatch);
            view.getValidatorHaul().setBean(beanHaul);
            view.getValidatorLength().setBean(beanLength);
            view.getValidatorStrata().setBean(beanStrata);

            final AbstractDataEntity finalBean = bean;
            // add to panel
            // start at 1 (0 = line index)
            for (int fieldIndex = 1; fieldIndex < header.length; fieldIndex++) {
                String headerValue = header[fieldIndex];
                String fieldValue = line[fieldIndex];

                String beanFieldName = Introspector.decapitalize(bean.getHeaders()[fieldIndex - 1]);

                // l'ui ne peut utiliser les getter/setter que sur les champs
                // xxxAsString
                final String stringBeanFieldName = beanFieldName + "AsString";

                JLabel label = new JLabel(headerValue + "\u2009:");
                panel.add(label, new GridBagConstraints(0, fieldIndex, 1, 1, 0, 0,
                                                        GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
                        1, 1, 1, 1), 0, 0));

                // combo box used to choose for species in list
                if ((category == Category.CATCH && fieldIndex == Catch.INDEX_SPECIES) ||
                    (category == Category.LENGTH && fieldIndex == Length.INDEX_SPECIES)) {
                    Project project = view.getContextValue(Project.class);
                    Map<String, String> reftaxSpecies = project.getRefTaxSpeciesMap();
                    List<String> domain = new ArrayList<String>(reftaxSpecies.keySet());
                    // ajout de la valeur pour que même si elle n'existe pas
                    // elle soit sélectionnée
                    if (!reftaxSpecies.containsKey(fieldValue)) {
                        domain.add(0, fieldValue);
                    }
                    ListComboBoxModel speciesComboModel = new ListComboBoxModel(domain);
                    JComboBox speciesCombo = new JComboBox(speciesComboModel);
                    speciesCombo.setRenderer(new SpeciesListRenderer(reftaxSpecies));
                    speciesCombo.setSelectedItem(fieldValue);
                    speciesCombo.addItemListener(new ItemListener() {

                        @Override
                        public void itemStateChanged(ItemEvent e) {
                            if (e.getStateChange() == ItemEvent.SELECTED) {
                                String value = (String) e.getItem();
                                try {
                                    PropertyUtils.setProperty(finalBean, stringBeanFieldName, value);
                                } catch (Exception ex) {
                                    if (log.isErrorEnabled()) {
                                        log.error("Can't set property value (" + stringBeanFieldName + ")", ex);
                                    }
                                }
                            }
                        }
                    });
                    panel.add(SwingUtil.boxComponentWithJxLayer(speciesCombo), new GridBagConstraints(1, fieldIndex, 1, 1, 1, 0,
                                                                                                      GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
                            1, 1, 1, 1), 0, 0));
                    // permet de dire a jaxx que les erreurs sur les
                    // champs d'origine, et les champs string pointent sur le
                    // même composant
                    validator.setFieldRepresentation(beanFieldName, speciesCombo);
                    validator.setFieldRepresentation(stringBeanFieldName, speciesCombo);
                } else {
                    final JTextField fieldTextField = new JTextField(fieldValue);
                    fieldTextField.getDocument().addDocumentListener(new DocumentListener() {

                        @Override
                        public void insertUpdate(DocumentEvent event) {
                            valueChanged(event);
                        }

                        @Override
                        public void removeUpdate(DocumentEvent event) {
                            valueChanged(event);
                        }

                        @Override
                        public void changedUpdate(DocumentEvent event) {
                            valueChanged(event);
                        }

                        protected void valueChanged(DocumentEvent event) {
                            try {
                                PropertyUtils.setProperty(finalBean, stringBeanFieldName, fieldTextField.getText());
                            } catch (Exception ex) {
                                if (log.isErrorEnabled()) {
                                    log.error("Can't set property value (" + stringBeanFieldName + ")", ex);
                                }
                            }
                        }
                    });
                    panel.add(SwingUtil.boxComponentWithJxLayer(fieldTextField), new GridBagConstraints(1, fieldIndex, 1, 1, 1, 0,
                                                                                                        GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
                            1, 1, 1, 1), 0, 0));

                    // permet de dire a jaxx que les erreurs sur les
                    // champs d'origine, et les champs string pointent sur le
                    // même composant
                    validator.setFieldRepresentation(beanFieldName, fieldTextField);
                    validator.setFieldRepresentation(stringBeanFieldName, fieldTextField);
                }
            }

            validator.installUIs();
            validator.reloadBean();

            view.getControlDataValidButton().setEnabled(true);
            view.getControlDataCancelButton().setEnabled(true);
            view.getControlDataDeleteButton().setEnabled(true);
        } else {

            // set to all validators to clear non concerned
            view.getValidatorCatch().setBean(null);
            view.getValidatorHaul().setBean(null);
            view.getValidatorLength().setBean(null);
            view.getValidatorStrata().setBean(null);

            view.getControlDataValidButton().setEnabled(false);
            view.getControlDataCancelButton().setEnabled(false);
            view.getControlDataDeleteButton().setEnabled(false);
        }
        view.getEditionScrollPane().repaint();
        view.getEditionScrollPane().validate();
    }

    /**
     * Save project after control and display message.
     *
     * Can't save if :
     * - validation contains error
     * - validation contains non checked warning
     *
     * @param view view
     */
    public void saveControl(ControlView view) {
        saveProjectControl(view);
        JOptionPane.showMessageDialog(view, t("coser.ui.control.controlSaved"),
                                      t("coser.ui.control.controlTitle"), JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Save project after control.
     *
     * @param view view
     */
    protected void saveProjectControl(ControlView view) {
        Project project = view.getContextValue(Project.class);
        ProjectService service = view.getContextValue(ProjectService.class);

        try {
            long before = System.currentTimeMillis();
            service.saveProjectControl(project);
            if (log.isDebugEnabled()) {
                long after = System.currentTimeMillis();
                log.debug("Control files saved in " + (after - before) + "ms");
            }
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't save project control", ex);
        }
    }

    /**
     * Display data graph, initialized with graph for first specy
     * selected in specyComboModel.
     *
     * @param view view
     */
    public void displayCompareNumberCatchGraph(ControlView view) {
        displayCompareNumberCatchGraph(view, null);
    }

    /**
     * Display data graph, initialized with graph for selected specy
     * selected.
     *
     * @param view            view
     * @param selectedSpecies if not null (auto select species in combo box)
     */
    public void displayCompareNumberCatchGraph(ControlView view, String selectedSpecies) {
        SwingSession session = view.getContextValue(SwingSession.class);

        // get matrix (form context by method #checkData)
        Project project = view.getContextValue(Project.class);
        Map<String, JFreeChart> charts = view.getContextValue(Map.class, "CompareCatchLengthGraph");
        List<String> species = CoserUtils.sortCollectionWithMapKeys(project.getRefTaxSpeciesMap(), charts.keySet());

        // close previous opened
        JFrame previousFrame = view.getContextValue(JFrame.class, "comparenumberframe");
        if (previousFrame != null) {
            previousFrame.dispose();
        }

        ControlGraphFrame frame = new ControlGraphFrame(view);
        frame.setHandler(ControlHandler.this);
        frame.setContextValue(charts);
        frame.getSpeciesComboModel().setSpecy(species);
        if (selectedSpecies != null) {
            frame.getSpeciesComboModel().setSelectedItem(selectedSpecies);
            updateCompareNumberCatchGraph(frame);
        }
        frame.getSpeciesCombo().setRenderer(new SpeciesListRenderer(project.getRefTaxSpeciesMap()));
        frame.pack();
        frame.setLocationRelativeTo(view);
        session.add(frame); // session restore
        frame.toFront();
        frame.setVisible(true);

        // register current frame
        view.setContextValue(frame, "comparenumberframe");
    }

    /**
     * Display lengthStructure matrix in matrixviewerpanel.
     *
     * @param view view
     */
    public void displayLengthStructureGraph(ControlView view) {
        displayLengthStructureGraph(view, null);
    }

    /**
     * Display lengthStructure matrix in matrixviewerpanel.
     *
     * @param view    view
     * @param species selected species (can be null)
     */
    public void displayLengthStructureGraph(ControlView view, String species) {
        Project project = view.getContextValue(Project.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);
        SwingSession session = view.getContextValue(SwingSession.class);
        displayLengthStructureGraph(view, session, projectService, project, project.getControl(), species);
    }

    /**
     * Met a jour le graphique lorsque la selection de l'espece change.
     *
     * @param view view
     */
    public void updateCompareNumberCatchGraph(ControlGraphFrame view) {
        String specyName = (String) view.getSpeciesCombo().getSelectedItem();
        Map<String, JFreeChart> charts = view.getContextValue(Map.class, "CompareCatchLengthGraph");
        JFreeChart chart = charts.get(specyName);
        view.getControlGraphPanel().removeAll();
        ChartPanel chartPanel = new ChartPanel(chart);
        view.getControlGraphPanel().add(chartPanel, BorderLayout.CENTER);
        view.getControlGraphPanel().validate();
    }

    /**
     * Valide les modifications faites sur le bean actuellement edité.
     *
     * @param view view
     */
    public void validDataModification(ControlView view) {

        // disable valid button
        disableValidAction(view);

        Project project = view.getContextValue(Project.class);
        ProjectService projectService = view.getContextValue(ProjectService.class);
        JTable controlDataTable = getControlDataTable(view);
        ControlTableModel model = (ControlTableModel) controlDataTable.getModel();
        int selectedLine = controlDataTable.getSelectedRow();
        Category category = (Category) view.getCategoryComboBox().getSelectedItem();

        // get data
        String[] newData = null;
        switch (category) {
            case CATCH:
                Catch beanCatch = view.getValidatorCatch().getBean();
                newData = beanCatch.getData();
                break;
            case HAUL:
                Haul beanHaul = view.getValidatorHaul().getBean();
                newData = beanHaul.getData();
                break;
            case LENGTH:
                Length beanLength = view.getValidatorLength().getBean();
                newData = beanLength.getData();
                break;
            case STRATA:
                Strata beanStrata = view.getValidatorStrata().getBean();
                newData = beanStrata.getData();
                break;
        }

        // remplace les nouvelle données (utilise le numero de ligne pour trouver les anciennes)
        try {
            projectService.replaceData(project, project.getControl(), category, newData);
            model.fireTableRowsUpdated(selectedLine, selectedLine);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't replace data", ex);
        }
    }

    /**
     * Annule les modifications en cours reset les données data, et
     * reload le validator.
     *
     * @param view view
     */
    public void cancelDataModification(ControlView view) {
        controlDataTableSelectionChanged(view);
    }

    /**
     * Supprime la ligne en cours d'edition (apres confirmation).
     *
     * @param view view
     */
    public void deleteData(ControlView view) {
        int response = JOptionPane.showConfirmDialog(view, t("coser.ui.control.confirmDeletionMessage"),
                                                     t("coser.ui.control.confirmDeletionTitle"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

        if (response == JOptionPane.YES_OPTION) {
            // disableValidAction
            disableValidAction(view);

            Project project = view.getContextValue(Project.class);
            ProjectService projectService = view.getContextValue(ProjectService.class);

            // get selected row, and selected csv line index
            JTable controlDataTable = getControlDataTable(view);
            ControlTableModel model = (ControlTableModel) controlDataTable.getModel();
            int selectedLine = controlDataTable.getSelectedRow();
            Category category = (Category) view.getCategoryComboBox().getSelectedItem();
            String[] data = model.getDataAt(selectedLine);

            try {
                projectService.deleteData(project, project.getControl(), category, data[AbstractDataEntity.INDEX_LINE]);
                model.fireTableRowsDeleted(selectedLine, selectedLine);
            } catch (CoserBusinessException ex) {
                throw new CoserException("Can't delete data", ex);
            }
        }
    }

    /**
     * Desactive le bouton de validation du control. Positionne également
     * le flag "validated" du control a false, pour cohérence, binding et
     * en cas de sauvegarde par l'utilisateur.
     */
    protected void disableValidAction(ControlView view) {
        Project project = view.getContextValue(Project.class);
        project.getControl().setValidated(false);
        view.setCanValidControl(false);
    }

    /**
     * Set validated control flag to true and save control.
     *
     * @param view view
     */
    public void validControl(ControlView view) {
        Project project = view.getContextValue(Project.class);
        ProjectService service = view.getContextValue(ProjectService.class);

        try {
            List<ControlError> controlErrors = view.getGlobalControlErrorModel().getControlErrors();
            service.validControl(project, controlErrors);
            JOptionPane.showMessageDialog(view, t("coser.ui.control.controlValidated"),
                                          t("coser.ui.control.controlTitle"), JOptionPane.INFORMATION_MESSAGE);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't generate html report", ex);
        }
    }

    /**
     * Genere le log des modifications faites lors du control en HTML
     * et l'ouvre dans le navigateur systeme.
     *
     * @param controlView
     */
    public void displayLogReport(ControlView controlView) {
        PublicationService publicationService = controlView.getContextValue(PublicationService.class);
        Project project = controlView.getContextValue(Project.class);

        File htmlFile = null;
        try {
            htmlFile = publicationService.extractControlLogAsHTML(project, project.getControl());
            browseFile(controlView, htmlFile);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't generate html report", ex);
        }
    }

    /**
     * Undo commands.
     *
     * @param controlView   view
     * @param commandsCount commands count to undo
     * @since 1.2
     */
    public void undoCommands(ControlView controlView, int commandsCount) {
        CommandService commandeService = controlView.getContextValue(CommandService.class);
        Project project = controlView.getContextValue(Project.class);

        try {
            commandeService.undoAction(project, project.getControl(), commandsCount);

            // after undo, refresh table, edition zone
            JTable controlDataTable = getControlDataTable(controlView);
            ControlTableModel model = (ControlTableModel) controlDataTable.getModel();
            model.fireTableDataChanged();
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't undo selected commands", ex);
        }
    }

    /**
     * Redo commands.
     *
     * @param controlView view
     * @param commands    commands to redo
     * @since 1.2
     */
    public void redoCommands(ControlView controlView, List<Command> commands) {
        CommandService commandeService = controlView.getContextValue(CommandService.class);
        Project project = controlView.getContextValue(Project.class);

        try {
            for (Command command : commands) {
                commandeService.doAction(command, project, project.getControl());
            }
            // after redo, refresh table, edition zone
            JTable controlDataTable = getControlDataTable(controlView);
            ControlTableModel model = (ControlTableModel) controlDataTable.getModel();
            model.fireTableDataChanged();
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't redo selected commands", ex);
        }
    }
}

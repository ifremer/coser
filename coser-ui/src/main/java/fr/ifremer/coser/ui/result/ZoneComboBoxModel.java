/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.result;

import fr.ifremer.coser.CoserApplicationContext;
import fr.ifremer.coser.storage.DataStorage;
import jaxx.runtime.JAXXObject;

import javax.swing.DefaultComboBoxModel;

/**
 * Zone combo box model.
 * Contains zone list defined in "matchzone" file common to web side and client
 * side.
 *
 * @author chatellier
 *
 *         <p/>
 */
public class ZoneComboBoxModel extends DefaultComboBoxModel {


    private static final long serialVersionUID = 4452070553401468762L;

    protected DataStorage zonesMap;

    public static ZoneComboBoxModel newModel(JAXXObject ui) {
        CoserApplicationContext contextValue = ui.getContextValue(CoserApplicationContext.class);
        DataStorage storage = contextValue.getZoneMap().getStorage();
        return new ZoneComboBoxModel(storage);
    }

    public ZoneComboBoxModel(DataStorage zonesMap) {
        this.zonesMap = zonesMap;
    }

//    public ZoneComboBoxModel(WebService webService) {
//        try {
//            zonesMap = webService.getZonesMap();
//        } catch (CoserBusinessException ex) {
//            throw new CoserException("Can't get zone list", ex);
//        }
//    }

    @Override
    public int getSize() {
        // -1 for header
        return zonesMap.size() - 1;
    }

    @Override
    public Object getElementAt(int index) {
        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"map"
        // +1 for header
        return zonesMap.get(index + 1)[0];
    }

    public String[] getZone(String zoneId) {
        int index = zonesMap.indexOf(zoneId);
        String[] result = null;
        if (index != -1) {
            result = zonesMap.get(index);
        }
        return result;
    }
}

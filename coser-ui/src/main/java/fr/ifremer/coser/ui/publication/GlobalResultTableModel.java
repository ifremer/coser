package fr.ifremer.coser.ui.publication;

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.coser.bean.GlobalResult;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/17/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GlobalResultTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    /** Les données de la table. */
    protected List<GlobalResult> resultPaths;

    /** Les résultats marqués comme etant des données d'indicateurs. */
    protected Set<GlobalResult> indicatorResults;

    /** Les résultats marqué comme étant des données de map. */
    protected Set<GlobalResult> mapResults;

    /** Les résultat dont la publication des données est autorisée. */
    protected Set<GlobalResult> publishDataResults;

    /** Selected table tablemodel (do not show all columns). */
    protected boolean selected;

    public GlobalResultTableModel(boolean selected) {
        this.selected = selected;

        // les selections ne sont jamais supprimé
        // mais vu l'equivalence equals/hascode
        // ca ne doit pas poser de problemes
        indicatorResults = Sets.newHashSet();
        mapResults = Sets.newHashSet();
        publishDataResults = Sets.newHashSet();
    }

    public void setResultPaths(List<GlobalResult> resultPaths) {
        this.resultPaths = resultPaths;
        fireTableDataChanged();
    }

    public List<GlobalResult> getResultPaths() {
        return resultPaths;
    }

    public Set<GlobalResult> getIndicatorResults() {
        return indicatorResults;
    }

    public Set<GlobalResult> getMapResults() {
        return mapResults;
    }

    public Set<GlobalResult> getPublishDataResults() {
        return publishDataResults;
    }

    @Override
    public int getRowCount() {
        int result = 0;
        if (resultPaths != null) {
            result = resultPaths.size();
        }
        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result = null;
        switch (column) {
            case 0:
                result = t("coser.ui.uploadresult.resultType");
                break;
            case 1:
                result = t("coser.ui.uploadresult.creationDate");
                break;
            case 2:
                result = t("coser.ui.uploadresult.path");
                break;
            case 3:
                result = t("coser.ui.uploadresult.zone");
                break;
            case 4:
                result = t("coser.ui.uploadresult.indicatorResult");
                break;
            case 5:
                result = t("coser.ui.uploadresult.mapResult");
                break;
            case 6:
                result = t("coser.ui.uploadresult.publishData");
                break;
        }
        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result = null;
        switch (columnIndex) {
            case 0:
                result = String.class;
                break;
            case 1:
                result = Date.class;
                break;
            case 2:
                result = String[].class;
                break;
            case 3:
                result = String.class;
                break;
            case 4:
                result = Boolean.class;
                break;
            case 5:
                result = Boolean.class;
                break;
            case 6:
                result = Boolean.class;
                break;
        }
        return result;
    }

    @Override
    public int getColumnCount() {
        int result = 4;
        if (selected) {
            result = 7;
        }
        return result;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result = null;

        GlobalResult data = resultPaths.get(rowIndex);
        switch (columnIndex) {
            case 0:
                result = data.isRsufi() ? "RSufi" : "EchoBase";
                break;
            case 1:
                result = data.getCreationDate();
                break;
            case 2:
                result = data;
                break;
            case 3:
                result = data.getZone();
                break;
            case 4:
                result = indicatorResults.contains(data);
                break;
            case 5:
                result = mapResults.contains(data);
                break;
            case 6:
                result = publishDataResults.contains(data);
                break;
        }

        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {

        GlobalResult data = resultPaths.get(rowIndex);

        boolean editable = !data.isEchoBase() && columnIndex >= 4;
        return editable;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        GlobalResult data = resultPaths.get(rowIndex);

        if (columnIndex == 4) {
            Boolean bValue = (Boolean) aValue;
            if (bValue) {
                indicatorResults.add(data);
            } else {
                indicatorResults.remove(data);
            }
        } else if (columnIndex == 5) {
            Boolean bValue = (Boolean) aValue;
            if (bValue) {
                mapResults.add(data);
            } else {
                mapResults.remove(data);
            }
        } else if (columnIndex == 6) {
            Boolean bValue = (Boolean) aValue;
            if (bValue) {
                publishDataResults.add(data);
            } else {
                publishDataResults.remove(data);
            }
        }
    }
}

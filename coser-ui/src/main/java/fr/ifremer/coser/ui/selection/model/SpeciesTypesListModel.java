/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.model;

import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.ui.selection.SelectionDetailsView;
import fr.ifremer.coser.ui.util.CoserListModel;

import javax.swing.AbstractListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Affiche la liste des types d'especes définie dans le projet.
 *
 * @author chatellier
 */
public class SpeciesTypesListModel extends AbstractListModel implements CoserListModel {


    private static final long serialVersionUID = 441910182067909029L;

    protected SelectionDetailsView view;

    protected List<String> types;

    protected Map<String, String> typesComments;

    public SpeciesTypesListModel(SelectionDetailsView view) {
        this.view = view;
    }

    protected List<String> getTypes() {
        if (types == null) {
            Project project = view.getContextValue(Project.class);
            ProjectService service = view.getContextValue(ProjectService.class);
            typesComments = service.getProjectSpeciesTypes(project);
            types = new ArrayList<String>(typesComments.keySet());
        }
        return types;
    }

    /**
     * Get species comment (used in renderer).
     *
     * @param species species
     * @return species comment
     */
    public String getComment(String species) {
        return typesComments.get(species);
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        return getTypes().size();
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return getTypes().get(index);
    }

    /*
     * @see fr.ifremer.coser.ui.util.CoserListModel#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(Object element) {
        return getTypes().indexOf(element);
    }
}

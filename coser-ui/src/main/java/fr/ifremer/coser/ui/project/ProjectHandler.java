/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.project;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.CoserException;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.ui.CoserFrame;
import fr.ifremer.coser.ui.common.CommonHandler;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for project related ui.
 *
 * @author chatellier
 */
public class ProjectHandler extends CommonHandler {

    private static final Log log = LogFactory.getLog(ProjectHandler.class);

    protected JFileChooser mapFileChooser;

    /**
     * Retourne une unique instance du file chooser pour conserver
     * le répertoire de sélection d'un appel sur l'autre.
     *
     * @return l'unique instance de file chooser
     */
    protected JFileChooser getMapFileChooser() {
        if (mapFileChooser == null) {
            mapFileChooser = new JFileChooser();
            mapFileChooser.addChoosableFileFilter(new FileFilter() {

                @Override
                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().matches(".*\\.shp");
                }

                @Override
                public String getDescription() {
                    return "ESRI Shapefiles (*.shp)";
                }
            });
        }
        return mapFileChooser;
    }

    /**
     * Display user file selection dialog and fill given text component
     * with given file.
     *
     * @param projectView   project view
     * @param textComponent text component to fill
     */
    public void selectInputFile(ProjectCreationView projectView, JTextField textComponent) {
        CoserConfig config = projectView.getContextValue(CoserConfig.class);
        JFileChooser selectFileChooser = getFileChooserInstance(config.getRSufiProjectsDirectory());
        selectFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int result = selectFileChooser.showOpenDialog(projectView);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            textComponent.setText(selectedFile.getAbsolutePath());
        }
    }

    /**
     * Display user file selection dialog and fill maps list.
     *
     * @param projectView project view
     */
    public void selectMapFiles(ProjectCreationView projectView) {
        JFileChooser selectFileChooser = getMapFileChooser();

        int result = selectFileChooser.showOpenDialog(projectView);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            projectView.getProjectMapsModel().addMap(selectedFile);
        }
    }

    /**
     * Display user file selection dialog and fill given text component
     * with given file.
     *
     * @param projectView project view
     */
    public void removeSelectedMapFiles(ProjectCreationView projectView) {
        Object[] values = projectView.getProjectMaps().getSelectedValues();
        for (Object value : values) {
            projectView.getProjectMapsModel().removeMap(value);
        }
    }

    /**
     * Display user file selection dialog and fill maps list.
     *
     * @param projectView project view
     */
    public void selectMapFiles(ProjectEditView projectView) {
        JFileChooser selectFileChooser = getMapFileChooser();

        int result = selectFileChooser.showOpenDialog(projectView);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = selectFileChooser.getSelectedFile();
            projectView.getProjectMapsModel().addMap(selectedFile);
        }
    }

    /**
     * Display user file selection dialog and fill given text component
     * with given file.
     *
     * @param projectView project view
     */
    public void removeSelectedMapFiles(ProjectEditView projectView) {
        Object[] values = projectView.getProjectMaps().getSelectedValues();
        for (Object value : values) {
            projectView.getProjectMapsModel().removeMap(value);
        }
    }

    /**
     * Create project.
     *
     * @param projectView project view
     */
    public void createProject(final ProjectCreationView projectView) {
        final CoserFrame parent = projectView.getContextValue(CoserFrame.class, JAXXUtil.PARENT);
        CoserConfig config = projectView.getContextValue(CoserConfig.class);
        final Project project = projectView.getProject();

        // get correct reftax to use depending on project configuration
        String reftaxSpeciesPath = null;
        if (projectView.getCustomReferenceCheckBox().isSelected()) {
            reftaxSpeciesPath = projectView.getCustomReferenceSpeciesFileTextField().getText().trim();
        } else {
            reftaxSpeciesPath = config.getReferenceSpeciesPath();
        }

        // get correct codeTypeEspece file to use
        String codeTypeEspecePath = config.getReferenceTypeEspecesPath();

        // convert to file
        File capturesFile = new File(project.getCatchFile());
        File stratesFile = new File(project.getStrataFile());
        File traitsFile = new File(project.getHaulFile());
        File taillesFile = new File(project.getLengthFile());
        File reftaxSpeciesFile = new File(reftaxSpeciesPath);
        File codeTypeEspeceFile = new File(codeTypeEspecePath);

        // remember original file names
        project.setCatchFileName(capturesFile.getName());
        project.setLengthFileName(taillesFile.getName());
        project.setHaulFileName(traitsFile.getName());
        project.setStrataFileName(stratesFile.getName());

        // get map list
        final List<File> newMaps = projectView.getProjectMapsModel().getMaps();

        // check some files existences
        if (!reftaxSpeciesFile.isFile()) {
            JOptionPane.showMessageDialog(projectView, t("coser.ui.project.createProjectMissingReftax"),
                                          t("coser.ui.project.missingFile"), JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (!codeTypeEspeceFile.isFile()) {
            JOptionPane.showMessageDialog(projectView, t("coser.ui.project.createProjectMissingCodeTypeSpecies"),
                                          t("coser.ui.project.missingFile"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        // package in map
        final Map<Category, File> fileToLoad = new HashMap<Category, File>();
        fileToLoad.put(Category.CATCH, capturesFile);
        fileToLoad.put(Category.HAUL, traitsFile);
        fileToLoad.put(Category.LENGTH, taillesFile);
        fileToLoad.put(Category.STRATA, stratesFile);
        fileToLoad.put(Category.REFTAX_SPECIES, reftaxSpeciesFile);
        fileToLoad.put(Category.TYPE_ESPECES, codeTypeEspeceFile);

        // disable create button
        projectView.getCreateProjectButton().setEnabled(false);

        setWaitCursor(projectView);
        SwingWorker<Project, Void> task = new SwingWorker<Project, Void>() {

            protected long before = System.currentTimeMillis();

            @Override
            protected Project doInBackground() {
                ProjectService projectService = projectView.getContextValue(ProjectService.class);
                Project resultProject = null;
                try {
                    resultProject = projectService.createProject(project, fileToLoad, newMaps, projectView.getLoadProgressBar());
                } catch (CoserBusinessException ex) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't create project", ex);
                    }
                    projectView.getCreateProjectButton().setEnabled(true);
                    // demande client, remettre la barre a 0
                    projectView.getLoadProgressBar().setValue(0);
                    // let it go, too many potential cause
                    // conserve le meme mesage que la cause
                    throw new CoserException(ex.getMessage(), ex);
                }
                return resultProject;
            }

            @Override
            protected void done() {
                if (log.isInfoEnabled()) {
                    long after = System.currentTimeMillis();
                    log.info("4 import take " + (after - before) + "ms");
                }
                try {
                    Project resultProject = get();
                    if (resultProject != null) {
                        parent.getHandler().projectLoaded(resultProject);
                        parent.getHandler().showControlView(false);
                    }
                } catch (Exception ex) {
                    // conserve le meme mesage que la cause
                    throw new CoserException(ex.getMessage(), ex);
                } finally {
                    setDefaultCursor(projectView);
                }
            }
        };
        task.execute();
    }

    /**
     * Sauve le projet apres edition.
     *
     * @param projectView project edit view
     */
    public void saveProject(ProjectEditView projectView) {
        Project project = projectView.getProject();
        ProjectService projectService = projectView.getContextValue(ProjectService.class);
        CoserFrame parent = projectView.getContextValue(CoserFrame.class, JAXXUtil.PARENT);

        setWaitCursor(projectView);
        try {

            // get map list
            List<File> newMaps = projectView.getProjectMapsModel().getMaps();

            projectService.saveProject(project, newMaps);
            parent.getHandler().showSummaryView();
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't save project", ex);
        } finally {
            setDefaultCursor(projectView);
        }
    }

    /**
     * Reload project (name selected in ui).
     *
     * @param projectView view
     */
    public void loadProject(ProjectOpenView projectView) {

        int selectedIndex = projectView.getProjectsList().getSelectedIndex();
        String projectName = (String) projectView.getProjectsList().getModel().getElementAt(selectedIndex);
        ProjectService projectService = projectView.getContextValue(ProjectService.class);
        CoserFrame parent = projectView.getContextValue(CoserFrame.class, JAXXUtil.PARENT);

        try {
            setWaitCursor(projectView);
            Project project = projectService.openProject(projectName);
            parent.getHandler().projectLoaded(project);
            parent.getHandler().showSummaryView();
        } catch (CoserBusinessException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't open project", ex);
            }
            JOptionPane.showMessageDialog(projectView, ex.getMessage(), t("coser.ui.project.openError"),
                                          JOptionPane.ERROR_MESSAGE);
        } finally {
            setDefaultCursor(projectView);
        }
    }

    /**
     * Reload project (name selected in ui).
     *
     * @param projectView view
     * @param event       mouse event
     */
    public void loadProjectOnDoubleClick(ProjectOpenView projectView, MouseEvent event) {

        if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 2) {
            int selectedIndex = projectView.getProjectsList().getSelectedIndex();
            if (selectedIndex >= 0) {
                loadProject(projectView);
            }
        }
    }
}

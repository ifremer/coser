/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.replay;

import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.command.Command;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

/**
 * List cell renderer for command (display command description).
 *
 * @author chatellier
 */
public class CommandListRenderer extends DefaultListCellRenderer {


    private static final long serialVersionUID = 6780656602646606040L;

    protected Project project;

    public CommandListRenderer(Project project) {
        this.project = project;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {

        Command command = (Command) value;

        // can be null here. Renderer is currently used only un selection
        // replay where merge command don't use container to get desc
        String commandDesc = command.getDescription(project, null);

        // is selected is forced to "true", because user can't disable
        // command in replay selection as of coser version 1.2.x
        // may change in v 2.0
        return super.getListCellRendererComponent(list, commandDesc, index,
                                                  true, cellHasFocus);
    }
}

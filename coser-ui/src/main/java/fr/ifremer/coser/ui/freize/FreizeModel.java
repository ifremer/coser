/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.freize;

import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.RSufiResult;
import fr.ifremer.coser.bean.Selection;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class FreizeModel {

    public static final String PROPERTY_PROJECT = "project";

    public static final String PROPERTY_SELECTION = "selection";

    public static final String PROPERTY_STEP0_ENABLED = "step0Enabled";

    public static final String PROPERTY_STEP1_ENABLED = "step1Enabled";

    public static final String PROPERTY_STEP2_ENABLED = "step2Enabled";

    public static final String PROPERTY_STEP3_ENABLED = "step3Enabled";

    protected Project project;

    protected Selection selection;

    protected RSufiResult rSufiResult;

    protected boolean step0Enabled;

    protected boolean step1Enabled;

    protected boolean step2Enabled;

    protected boolean step3Enabled;

    protected transient PropertyChangeSupport pcs;

    public FreizeModel() {
        pcs = new PropertyChangeSupport(this);
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        Project oldValue = this.project;
        this.project = project;
        pcs.firePropertyChange(PROPERTY_PROJECT, oldValue, project);
    }

    public Selection getSelection() {
        return selection;
    }

    public void setSelection(Selection selection) {
        Selection oldValue = this.selection;
        this.selection = selection;
        pcs.firePropertyChange(PROPERTY_SELECTION, oldValue, selection);
    }

    public boolean isStep0Enabled() {
        return step0Enabled;
    }

    public void setStep0Enabled(boolean step0Enabled) {
        boolean oldValue = this.step0Enabled;
        this.step0Enabled = step0Enabled;
        pcs.firePropertyChange(PROPERTY_STEP0_ENABLED, oldValue, step0Enabled);
    }

    public boolean isStep1Enabled() {
        return step1Enabled;
    }

    public void setStep1Enabled(boolean step1Enabled) {
        boolean oldValue = this.step1Enabled;
        this.step1Enabled = step1Enabled;
        pcs.firePropertyChange(PROPERTY_STEP1_ENABLED, oldValue, step1Enabled);
    }

    public boolean isStep2Enabled() {
        return step2Enabled;
    }

    public void setStep2Enabled(boolean step2Enabled) {
        boolean oldValue = this.step2Enabled;
        this.step2Enabled = step2Enabled;
        pcs.firePropertyChange(PROPERTY_STEP2_ENABLED, oldValue, step2Enabled);
    }

    public boolean isStep3Enabled() {
        return step3Enabled;
    }

    public void setStep3Enabled(boolean step3Enabled) {
        boolean oldValue = this.step3Enabled;
        this.step3Enabled = step3Enabled;
        pcs.firePropertyChange(PROPERTY_STEP3_ENABLED, oldValue, step3Enabled);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }
}

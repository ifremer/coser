/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserConstants.Category;

import javax.swing.DefaultComboBoxModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Control data categories list model.
 *
 * @author chatellier
 */
public class ControlCategoryListModel extends DefaultComboBoxModel {


    private static final long serialVersionUID = 6700971928409164642L;

    protected List<Category> categories;

    public ControlCategoryListModel() {
        categories = new ArrayList<Category>();
        for (Category category : Category.values()) {
            if (category.isDataCategory()) {
                categories.add(category);
            }
        }
        setSelectedItem(categories.get(0));
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        return categories.size();
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return categories.get(index);
    }
}

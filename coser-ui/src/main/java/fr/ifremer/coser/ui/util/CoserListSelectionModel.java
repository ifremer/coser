/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.util;

import jaxx.runtime.swing.OneClicListSelectionModel;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Model de selection fonctionnant avec les intances des objects selectionnée
 * et non par indices.
 * Etend {@link OneClicListSelectionModel} pour sélectionner par simple clic.
 *
 * Implemente {@link ListDataListener} pour que la selection apres changement
 * des données reste la même au niveau des objets (sinon, les indices
 * ne correspondent pas au données précédent le changement.
 *
 * ATTENTION : {@link #selectedObjects} ne reflete pas la selection
 * de la liste. Certains de ces élements peuvent être mémorisé selectionné,
 * mais non présente dans le modele de la liste, donc hors selection.
 *
 * @author chatellier
 */
public class CoserListSelectionModel extends OneClicListSelectionModel implements ListDataListener {

    /** Selection objects. */
    protected Set<Object> selectedObjects;

    /** List model. */
    protected CoserListModel coserListModel;

    /**
     * Constuctor.
     *
     * @param delegate
     * @param coserListModel
     */
    public CoserListSelectionModel(ListSelectionModel delegate, CoserListModel coserListModel) {
        super(delegate, coserListModel);
        this.coserListModel = coserListModel;
        selectedObjects = new HashSet<Object>();
    }

    /**
     * Replace selection.
     *
     * @param objects new selected objects
     */
    public void setSelectedObjects(Collection<?> objects) {
        selectedObjects.clear();
        if (objects != null) {
            selectedObjects.addAll(objects);
        }
        setValueIsAdjusting(true);
        try {
            contentsChanged(null);
        } finally {
            setValueIsAdjusting(false);
        }
    }

    /*
     * @see javax.swing.event.ListDataListener#intervalAdded(javax.swing.event.ListDataEvent)
     */
    @Override
    public void intervalAdded(ListDataEvent e) {
        throw new RuntimeException("Not yet implemented");
    }

    /*
     * @see javax.swing.event.ListDataListener#intervalRemoved(javax.swing.event.ListDataEvent)
     */
    @Override
    public void intervalRemoved(ListDataEvent e) {
        throw new RuntimeException("Not yet implemented");
    }

    /*
     * @see javax.swing.event.ListDataListener#contentsChanged(javax.swing.event.ListDataEvent)
     */
    @Override
    public void contentsChanged(ListDataEvent e) {
        // super is mandatory here
        super.clearSelection();

        Iterator<Object> itO = new HashSet<Object>(selectedObjects).iterator();
        while (itO.hasNext()) {
            Object o = itO.next();
            int index = coserListModel.indexOf(o);

            if (index >= 0) {
                addSelectionInterval(index, index);
            }

            // a voir, mais si on le remove, avec les evenements multiples
            // on pert toutes la selection
            /*else {
                itO.remove();
            }*/
        }
    }

    @Override
    public void setSelectionInterval(int index0, int index1) {
        super.setSelectionInterval(index0, index1);
        registerSelection(index0, index1);
    }

    @Override
    public void addSelectionInterval(int index0, int index1) {
        super.addSelectionInterval(index0, index1);
        registerSelection(index0, index1);
    }

    protected void registerSelection(int index0, int index1) {

        for (int i = index0; i <= index1; ++i) {
            Object o = coserListModel.getElementAt(i);
            if (isSelectedIndex(i)) {
                selectedObjects.add(o);
            } else {
                selectedObjects.remove(o);
            }
        }
    }

    /**
     * Fill selection.
     */
    public void fillSelection() {
        addSelectionInterval(0, coserListModel.getSize() - 1);
    }

    @Override
    public void clearSelection() {
        super.clearSelection();
        //ne peut pas retirer des elements qui ne sont plus dans le model...
        //registerSelection(0, coserListModel.getSize() - 1);
        selectedObjects.clear();
    }
}

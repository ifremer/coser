/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui;

import fr.ifremer.coser.CoserBusinessException;
import fr.ifremer.coser.CoserConfig;
import fr.ifremer.coser.CoserException;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.bean.Selection;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.ui.common.CommonHandler;
import fr.ifremer.coser.ui.control.ControlHandler;
import fr.ifremer.coser.ui.control.ControlView;
import fr.ifremer.coser.ui.option.ConfigurationView;
import fr.ifremer.coser.ui.option.NoCopiedLayerUI;
import fr.ifremer.coser.ui.option.OptionHandler;
import fr.ifremer.coser.ui.option.ValidatorDialog;
import fr.ifremer.coser.ui.project.ProjectCreationView;
import fr.ifremer.coser.ui.project.ProjectEditView;
import fr.ifremer.coser.ui.project.ProjectHandler;
import fr.ifremer.coser.ui.project.ProjectOpenView;
import fr.ifremer.coser.ui.project.ProjectSummaryView;
import fr.ifremer.coser.ui.publication.SelectUploadResultView;
import fr.ifremer.coser.ui.selection.SelectionHandler;
import fr.ifremer.coser.ui.selection.SelectionView;
import fr.ifremer.coser.ui.selection.replay.SelectionReplayHandler;
import fr.ifremer.coser.ui.selection.replay.SelectionReplayView;
import fr.ifremer.coser.ui.widgets.LookAndFeelViewMenuItem;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.session.SwingSession;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.widgets.extra.AboutFrame;
import org.nuiton.util.Resource;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * Coser handler for main frame.
 *
 * @author chatellier
 */
public class CoserFrameHandler extends CommonHandler {

    private static final Log log = LogFactory.getLog(CoserFrameHandler.class);

    protected CoserFrame view;

    public CoserFrameHandler(CoserFrame view) {
        this.view = view;
    }

    /**
     * Switch application locale.
     *
     * @param frame     frame
     * @param newLocale new locale
     */
    public void switchLanguage(CoserFrame frame, Locale newLocale) {
        CoserConfig config = frame.getContextValue(CoserConfig.class);
        config.setLocale(newLocale);
        config.saveForUser();
        JOptionPane.showMessageDialog(frame, t("coser.ui.locale.mustRestart"),
                                      t("coser.ui.locale.title"), JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Show home view.
     */
    public void showHomeView() {
        HomeView homeView = new HomeView();
        homeView.setHandler(this);
        setMainComponent(homeView);
    }

    /**
     * Display new creation view in main view.
     */
    public void showProjectCreationView() {
        ProjectCreationView projectCreationView = new ProjectCreationView(view);
        projectCreationView.setHandler(new ProjectHandler());
        setMainComponent(projectCreationView);
    }

    /**
     * Display new open view in main view.
     */
    public void showProjectOpenView() {
        ProjectOpenView projectOpenView = new ProjectOpenView(view);
        projectOpenView.setHandler(new ProjectHandler());
        setMainComponent(projectOpenView);
    }

    /**
     * Display new open view in main view.
     */
    public void showProjectEditView() {
        Project project = view.getProject();
        ProjectEditView projectEditView = new ProjectEditView(view);
        projectEditView.setHandler(new ProjectHandler());
        projectEditView.setProject(project);
        setMainComponent(projectEditView);
    }

    /**
     * Replace window main component.
     *
     * @param component new component
     */
    protected void setMainComponent(Component component) {
        view.getMainViewContent().removeAll();
        if (component != null) {
            view.getMainViewContent().add(component, BorderLayout.CENTER);
        }

        // centralisation of undo/redo menu manipulation
        if (component instanceof ControlView) {
            ControlView controlView = (ControlView) component;
            view.getMenuWindowSelectionUndo().setControlView(controlView);
            view.getMenuWindowSelectionRedo().setControlView(controlView);
        } else {
            view.getMenuWindowSelectionUndo().setControlView(null);
            view.getMenuWindowSelectionRedo().setControlView(null);
        }

        view.getMainViewContent().repaint();
        view.getMainViewContent().validate();
    }

    /**
     * Exit application.
     * Save swing session before exit.
     */
    public void quit() {
        SwingSession session = view.getContextValue(SwingSession.class);
        try {
            session.save();
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not save user ui session", e);
            }
        }
        System.exit(0);
    }

    /**
     * Show coser configuration.
     */
    public void showCoserConfiguration() {
        ConfigurationView configView = new ConfigurationView((JAXXContext) view);
        configView.setLocationRelativeTo(view);
        configView.setVisible(true);
    }

    /**
     * Open default system browser to coser website.
     */
    public void showCoserWebsite() {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        String websiteUrl = config.getWebsiteURL();

        // little hack, in en locale, add /en at end of url
        if (Locale.UK.equals(config.getLocale())) {
            websiteUrl += "en/";
        }

        URI uri = URI.create(websiteUrl);
        try {
            Desktop.getDesktop().browse(uri);
        } catch (IOException ex) {
            throw new CoserException("Can't open system browser", ex);
        }
    }

    /**
     * Open default system browser to coser website.
     */
    public void showCoserWebsiteSIH() {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        URI uri = URI.create(config.getWebFrontEnd());
        try {
            Desktop.getDesktop().browse(uri);
        } catch (IOException ex) {
            throw new CoserException("Can't open system browser", ex);
        }
    }

    /**
     * Display coser about view.
     */
    public void showAboutView() {
        CoserConfig config = view.getContextValue(CoserConfig.class);

        /* Logo doesn't not display with that version
        AboutPanel aboutPanel = new AboutPanel(view);
        aboutPanel.setIconPath("/icons/logo300.png");
        aboutPanel.setTitle(t("coser.ui.about.title"));
        aboutPanel.setAboutText(t("coser.ui.about.about", config.getApplicationVersion()));
        aboutPanel.setLicenseFile("META-INF/coser-ui-LICENSE.txt");
        aboutPanel.setThirdpartyFile("META-INF/coser-ui-THIRD-PARTY.txt");
        aboutPanel.init();
        aboutPanel.showInDialog(view, true);*/

        AboutFrame aboutFrame = new AboutFrame() {

            @Override
            protected Component getLicenseTab() {
                JTextArea textArea = (JTextArea) super.getLicenseTab();
                textArea.setCaretPosition(0);
                return new JScrollPane(textArea);
            }
        };
        aboutFrame.setTitle(t("coser.ui.about.title"));
        aboutFrame.setAboutHtmlText(t("coser.ui.about.about", config.getApplicationVersion()));
        aboutFrame.setIconPath("/icons/logo300.png");
        aboutFrame.setIconImage(Resource.getIcon("/icons/logo300.png").getImage());
        InputStream licenseStream = getClass().getResourceAsStream("/META-INF/coser-ui-LICENSE.txt");
        try {
            if (licenseStream != null) {
                aboutFrame.setLicenseText(IOUtils.toString(licenseStream));
            } else {
                aboutFrame.setLicenseText("No license file found");
            }
        } catch (IOException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't read licence file", ex);
            }
        } finally {
            IOUtils.closeQuietly(licenseStream);
        }
        aboutFrame.setBackgroundColor(Color.WHITE);
        aboutFrame.setSize(600, 600);
        aboutFrame.setLocationRelativeTo(view);
        aboutFrame.setVisible(true);
    }

    /**
     * Affiche la fenetre de configuration des validateurs personnel de
     * l'utilisateur.
     */
    public void showValidatorsConfiguration() {
        CoserConfig config = view.getContextValue(CoserConfig.class);
        ValidatorDialog validatorDialog = new ValidatorDialog((JAXXContext) view);
        validatorDialog.setHandler(new OptionHandler());

        // layer si non copié
        if (!config.getValidatorsDirectory().isDirectory()) {
            validatorDialog.getNoCopiedToDiskLayer().setUI(new NoCopiedLayerUI(validatorDialog));
        }

        validatorDialog.setLocationRelativeTo(view);

        // restore session size
        SwingSession session = view.getContextValue(SwingSession.class);
        session.add(validatorDialog);

        validatorDialog.setVisible(true);
    }

    /**
     * Ferme le projet.
     */
    public void closeProject() {
        projectLoaded(null);
        view.setTitle(t("coser.ui.mainview.titleempty"));
        showHomeView();
    }

    /**
     * Do some operation when a new project is loaded into application.
     *
     * @param project loaded project (can be null)
     */
    public void projectLoaded(Project project) {
        view.setProject(project);
        if (project != null) {
            view.setContextValue(project);
            view.setTitle(t("coser.ui.mainview.titleproject", project.getName()));
        } else {
            view.removeContextValue(Project.class);
        }
        selectionLoaded(null); // to update freize
    }

    /**
     * Do some operation when a new selection is loaded into application.
     *
     * @param selection loaded selection (can be null)
     */
    public void selectionLoaded(Selection selection) {
        view.setSelection(selection); // to update freize
        if (selection != null) {
            view.setContextValue(selection);
        } else {
            view.removeContextValue(Selection.class);
        }
    }

    /**
     * Affiche la fenêtre de résumé d'un projet.
     */
    public void showSummaryView() {
        Project project = view.getContextValue(Project.class);

        ProjectSummaryView projectSummaryView = new ProjectSummaryView(view);
        projectSummaryView.setHandler(new ProjectHandler());
        projectSummaryView.setProject(project);

        int selectionCount = 0;
        int resultCount = 0;
        for (Selection selection : project.getSelections().values()) {
            selectionCount++;
            resultCount += selection.getRsufiResults().size();
        }
        projectSummaryView.getProjectSelectionCount().setText(String.valueOf(selectionCount));
        projectSummaryView.getProjectResultCount().setText(String.valueOf(resultCount));

        setMainComponent(projectSummaryView);
    }

    /**
     * Charge les données de control et affiche la vue de control.
     */
    public void showControlView() {
        showControlView(true);
    }

    /**
     * Charge les données de control et affiche la vue de control.
     *
     * @param reloadData reload data (a {@code false} dans le cas d'une creation,
     *                   les données sont deja chargées)
     */
    public void showControlView(boolean reloadData) {
        // load control
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);

        // #667 : Empecher l'accès à l'étape de contrôle si des sélections existent
        if (!project.getSelections().isEmpty()) {
            JOptionPane.showMessageDialog(view, t("coser.ui.mainview.accesswontrolwithselection"),
                                          t("coser.ui.control.controlTitle"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        setWaitCursor(view);
        try {

            if (reloadData) {
                project = projectService.loadControlData(project);
            }

            ControlView controlView = new ControlView(view);
            ControlHandler controlHandler = new ControlHandler();
            controlView.setHandler(controlHandler);
            controlView.setControl(project.getControl());
            controlHandler.init(controlView);

            // restore session size
            SwingSession session = view.getContextValue(SwingSession.class);
            session.add(controlView);

            setMainComponent(controlView);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't load control data", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Show selection view to create new selection.
     *
     * TODO echatellier 20101217 revoir les 3 méthodes de selection (code dupliqués)
     */
    public void showSelectionView() {

        // create new selection
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);

        setWaitCursor(view);
        try {
            Selection selection = projectService.initProjectSelection(project);
            selectionLoaded(selection);

            SelectionView selectionView = new SelectionView(view);
            selectionView.setSelection(selection);
            // fix, binding not working ?
            selectionView.getSelectionDetailsTab().getValidatorSelection().setBean(selection);

            SelectionHandler handler = new SelectionHandler();
            selectionView.setHandler(handler);
            handler.initView(selectionView);
            handler.initSelection(selectionView);

            // restore session size
            SwingSession session = view.getContextValue(SwingSession.class);
            session.add(selectionView);

            // fix la restauration de la selection
            // seule le 1er onglet doit etre selectionné
            selectionView.setSelectedIndex(0);

            setMainComponent(selectionView);
        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't create new selection", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Show selection view to open selection.
     *
     * @param selectionName selection name to open
     */
    public void showSelectionView(String selectionName) {
        // get selected selection
        ProjectService projectService = view.getContextValue(ProjectService.class);
        Project project = view.getContextValue(Project.class);
        Selection selection = project.getSelections().get(selectionName);

        setWaitCursor(view);
        try {
            project = projectService.loadSelectionData(project, selection);

            // fix la restauration de la selection
            // seule le 1er onglet doit etre selectionné
            // sinon, le SwingSession remettra celui à la fermeture
            showSelectionView(selection, 0);

        } catch (CoserBusinessException ex) {
            throw new CoserException("Can't reload selection data", ex);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Show selection view to open selection.
     *
     * Used in selection replay. Selection already exists and is already loaded
     * into memory.
     *
     * @param selection   selection to open
     * @param selectedTab onglet a selectionner apres l'ouverture
     */
    public void showSelectionView(Selection selection, int selectedTab) {

        setWaitCursor(view);
        try {
            selectionLoaded(selection);

            SelectionView selectionView = new SelectionView(view);
            selectionView.setSelection(selection);

            // fix, binding not working ?
            selectionView.getSelectionDetailsTab().getValidatorSelection().setBean(selection);

            SelectionHandler handler = new SelectionHandler();
            selectionView.setHandler(handler);
            handler.initView(selectionView);
            handler.reloadSelection(selectionView);

            // restore session size
            SwingSession session = view.getContextValue(SwingSession.class);
            session.add(selectionView);

            // selection du bon onglet
            selectionView.setSelectedIndex(selectedTab);

            setMainComponent(selectionView);
        } finally {
            setDefaultCursor(view);
        }
    }

    /**
     * Selection file filtrer for selection file chooser.
     */
    public static class SelectionFileFilter extends FileFilter {

        @Override
        public boolean accept(File f) {
            return f.getName().endsWith(".selection");
        }

        @Override
        public String getDescription() {
            return t("coser.ui.selection.selectionFilterDescription");
        }

    }

    /**
     * Replay selection.
     *
     * Open browse selection box to select selection file.
     * Inform user to selection information before applying it.
     * Apply it, and display report to user.
     */
    public void replaySelection() {

        // les données vont être déchargées pour creer la nouvelle selection
        // il faut surtout pas conserver d'affichage sinon, ca devient
        // incohérent
        showSummaryView();

        SelectionReplayView replayView = new SelectionReplayView(view, view);
        SelectionReplayHandler handler = replayView.getHandler();
        handler.initReplayView(replayView);
        replayView.setLocationRelativeTo(view);
        replayView.setVisible(true);
    }

    /**
     * Called by {@link LookAndFeelViewMenuItem} when look and feel selection
     * change.
     *
     * @param event change event
     */
    public void saveLookAndFeelConfiguration(PropertyChangeEvent event) {
        if (event.getPropertyName().equals(LookAndFeelViewMenuItem.PROPERTY_LOOK_AND_FEEL)) {
            CoserConfig config = view.getContextValue(CoserConfig.class);
            config.setLookAndFeel((String) event.getNewValue());
            config.saveForUser();
            if (log.isDebugEnabled()) {
                log.debug("Look and feel saved to " + config.getLookAndFeel());
            }
        }
    }

    /**
     * Display ui to select result to upload to website or to export as archive.
     */
    public void showPublishResultView() {
        SelectUploadResultView selectUploadResultView = new SelectUploadResultView(view);
        selectUploadResultView.getHandler().init(selectUploadResultView);

        // restore session size
        SwingSession session = view.getContextValue(SwingSession.class);
        session.add(selectUploadResultView);

        setMainComponent(selectUploadResultView);
    }
}

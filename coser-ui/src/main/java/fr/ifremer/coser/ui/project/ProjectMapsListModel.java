/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.project;

import javax.swing.DefaultListModel;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Project maps files list model.
 *
 * @author chatellier
 */
public class ProjectMapsListModel extends DefaultListModel {


    private static final long serialVersionUID = 7354496943155548103L;

    protected List<File> maps;

    public ProjectMapsListModel() {
        maps = new ArrayList<File>();
    }

    public void setMaps(List<File> maps) {
        if (maps != null) {
            this.maps = new ArrayList<File>(maps);
        } else {
            this.maps = new ArrayList<File>();
        }
        fireContentsChanged(this, 0, this.maps.size() - 1);
    }

    public List<File> getMaps() {
        return maps;
    }

    public void addMap(File map) {
        maps.add(map);
        fireContentsChanged(this, 0, maps.size() - 1);
    }

    public void removeMap(Object map) {
        maps.remove(map);
        fireContentsChanged(this, 0, maps.size() - 1);
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        return maps.size();
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return maps.get(index);
    }
}

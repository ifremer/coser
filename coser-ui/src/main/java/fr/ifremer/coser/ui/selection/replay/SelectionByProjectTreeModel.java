/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.replay;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

/**
 * TreeModel that display all selections in all projects.
 *
 * @author chatellier
 */
public class SelectionByProjectTreeModel implements TreeModel {

    protected SortedMap<String, List<String>> selectionsByProject;

    protected List<String> projects;

    public SelectionByProjectTreeModel(SortedMap<String, List<String>> selectionsByProject) {
        this.selectionsByProject = selectionsByProject;
        this.projects = new ArrayList<String>(selectionsByProject.keySet());
    }

    @Override
    public Object getRoot() {
        return "root";
    }

    @Override
    public Object getChild(Object parent, int index) {

        Object child = null;

        if (parent == getRoot()) {
            child = projects.get(index);
        } else if (parent instanceof String) {
            if (projects.contains(parent)) {
                child = selectionsByProject.get(parent).get(index);
            }
        }

        return child;
    }

    @Override
    public int getChildCount(Object parent) {

        int count = 0;

        if (parent == getRoot()) {
            count = projects.size();
        } else if (parent instanceof String) {
            if (projects.contains(parent)) {
                count = selectionsByProject.get(parent).size();
            }
        }

        return count;
    }

    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOf = -1;
        if (parent == getRoot()) {
            indexOf = projects.indexOf(child);
        } else if (parent instanceof String) {
            if (projects.contains(parent)) {
                indexOf = selectionsByProject.get(parent).indexOf(child);
            }
        }
        return indexOf;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}

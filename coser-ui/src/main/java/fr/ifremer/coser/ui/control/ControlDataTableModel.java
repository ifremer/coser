/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.control;

import fr.ifremer.coser.CoserConstants.Category;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.storage.DataStorage;

import static org.nuiton.i18n.I18n.t;

/**
 * Table model that display csv content loaded into memory and stored in
 * project storage.
 *
 * @author chatellier
 */
public class ControlDataTableModel extends ControlTableModel {


    private static final long serialVersionUID = -1192463259386773117L;

    protected DataStorage data;

    protected Category category;

    protected String[] header;

    public ControlDataTableModel(ControlView controlView, Category category) {
        this.category = category;
        updateData(controlView);
    }

    @Override
    public Category getCategory() {
        return category;
    }

    protected void updateData(ControlView controlView) {
        Project project = controlView.getContextValue(Project.class);

        switch (category) {
            case CATCH:
                data = project.getControl().getCatch();
                break;
            case HAUL:
                data = project.getControl().getHaul();
                break;
            case LENGTH:
                data = project.getControl().getLength();
                break;
            case STRATA:
                data = project.getControl().getStrata();
                break;
        }
        header = data.get(0);
        fireTableStructureChanged();
    }

    /*
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        // skip header
        return data.size() - 1;
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public String getColumnName(int column) {
        String name = header[column];
        // only for column 0 (line index)
        if (column == 0) {
            name = t(name);
        }
        return name;
    }

    /*
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object result = getDataAt(rowIndex)[columnIndex];
        return result;
    }

    /**
     * Retourne la donnée (toutes le String[]) à la ligne demandée.
     *
     * @param rowIndex
     * @return String[]
     */
    @Override
    public String[] getDataAt(int rowIndex) {
        return data.get(rowIndex + 1);
    }

    /**
     * Retourne l'index dans la liste des données du numero de ligne demandée.
     *
     * @param lineNumber le numero de données
     * @return l'index
     */
    @Override
    public int getRealIndexOfLine(String lineNumber) {
        // -1 because header is table header
        int result = data.indexOf(lineNumber) - 1;
        return result;
    }
}

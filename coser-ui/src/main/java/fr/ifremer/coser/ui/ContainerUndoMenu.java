/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui;

import fr.ifremer.coser.bean.AbstractDataContainer;
import fr.ifremer.coser.bean.Control;
import fr.ifremer.coser.bean.Project;
import fr.ifremer.coser.command.Command;
import fr.ifremer.coser.ui.control.ControlView;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Undo menu action.
 *
 * Currently working only for control view because in selection merge commands
 * are not undoable.
 *
 * @author chatellier
 * @since 1.2
 */
public class ContainerUndoMenu extends JMenu implements ActionListener, PropertyChangeListener {

    private static final long serialVersionUID = 2104672856535432709L;

    private static final Log log = LogFactory.getLog(ContainerUndoMenu.class);

    protected ControlView controlView;

    protected ContainerRedoMenu redoMenu;

    public ContainerUndoMenu() {
        setEnabled(false);
    }

    public void setControlView(ControlView controlView) {
        if (this.controlView != null && this.controlView.getControl() != null) {
            this.controlView.getControl().removePropertyChangeListener(AbstractDataContainer.PROPERTY_HISTORY_COMMANDS, this);
        }
        this.controlView = controlView;
        if (this.controlView != null && this.controlView.getControl() != null) {
            this.controlView.getControl().addPropertyChangeListener(AbstractDataContainer.PROPERTY_HISTORY_COMMANDS, this);
        }
        updateSubMenuItems();
    }

    public void setRedoMenu(ContainerRedoMenu redoMenu) {
        this.redoMenu = redoMenu;
    }

    /**
     * Update submenu items.
     */
    protected void updateSubMenuItems() {

        if (log.isDebugEnabled()) {
            log.debug("Refresh undo menu items");
        }

        removeAll();
        boolean menuEnabled = false;

        if (controlView != null && controlView.getControl() != null) {
            Control control = controlView.getControl();
            Project project = controlView.getContextValue(Project.class);
            List<Command> commands = controlView.getControl().getHistoryCommands();
            menuEnabled = !commands.isEmpty();

            // command in reverse order (only 10 last)
            for (int i = commands.size() - 1; i >= 0 && i > commands.size() - 10; i--) {
                Command command = commands.get(i);
                JMenuItem commandMenu = new JMenuItem(command.getDescription(project, control));
                commandMenu.setActionCommand(String.valueOf(i));
                commandMenu.addActionListener(this);
                add(commandMenu);
            }
        }

        setEnabled(menuEnabled);
    }

    /*
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        updateSubMenuItems();
    }

    /*
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        Control control = controlView.getControl();
        int commandIndex = Integer.parseInt(actionCommand);
        if (log.isDebugEnabled()) {
            log.debug("Undo command index " + commandIndex);
        }

        // le menu redo est géré directement par le menu undo
        // c'est mal fait, mais tellement plus simple
        // attention copie : la list va changer avec les fire
        List<Command> redoCommands = new ArrayList<Command>(redoMenu.getCommands());
        List<Command> commands = controlView.getControl().getHistoryCommands();
        for (int i = commands.size() - 1; i >= commandIndex; i--) {
            redoCommands.add(commands.get(i));
        }

        // undo commands
        int count = control.getHistoryCommands().size() - commandIndex;
        controlView.getHandler().undoCommands(controlView, count);

        // update redo menu
        redoMenu.setCommands(redoCommands);
    }
}

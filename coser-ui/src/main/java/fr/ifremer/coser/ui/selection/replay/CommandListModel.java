/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.selection.replay;

import fr.ifremer.coser.command.Command;

import javax.swing.AbstractListModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Command model.
 *
 * @author chatellier
 */
public class CommandListModel extends AbstractListModel {


    private static final long serialVersionUID = -4769109927915812519L;

    protected List<Command> commands = new ArrayList<Command>();

    public List<Command> getCommands() {
        return commands;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
        fireContentsChanged(this, 0, commands.size());
    }

    /*
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        int result = 0;
        if (commands != null) {
            result = commands.size();
        }
        return result;
    }

    /*
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return commands.get(index);
    }
}

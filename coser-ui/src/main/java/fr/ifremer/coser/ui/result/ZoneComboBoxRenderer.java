/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.result;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

/**
 * Zone list combo renderer (display zone name for zone id).
 *
 * @author chatellier
 */
public class ZoneComboBoxRenderer extends DefaultListCellRenderer {


    private static final long serialVersionUID = -8231189755539976714L;

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {

        String zoneId = (String) value;
        ZoneComboBoxModel model = (ZoneComboBoxModel) list.getModel();

        // "id";"facadeid";"facade";"zone";"periode";"serie";"comment";"map"
        String[] zoneData = model.getZone(zoneId);
        String zoneName = null;
        if (zoneData != null) {
            // le surveyName ne sert à rien
            // mais est la a titre d'information par rapport
            // à l'affichage cote web
            zoneName = zoneData[2] + " - " + zoneData[3] + " - " + zoneData[4] + " - " + zoneData[5];
        } else {
            zoneName = zoneId;
        }
        return super.getListCellRendererComponent(list, zoneName, index, isSelected,
                                                  cellHasFocus);
    }
}

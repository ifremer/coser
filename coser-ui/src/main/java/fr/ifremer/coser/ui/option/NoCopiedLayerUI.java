/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser.ui.option;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;

import javax.swing.JButton;
import javax.swing.JComponent;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.nuiton.i18n.I18n.t;

/**
 * Layer transparent.
 *
 * @author chatellier
 */
public class NoCopiedLayerUI extends LockableUI implements ActionListener {


    private static final long serialVersionUID = -616755266651462912L;

    protected ValidatorDialog view;

    protected JButton copyToDiskButton;

    public NoCopiedLayerUI(ValidatorDialog view) {
        this.view = view;
        setLocked(true);
        copyToDiskButton = new JButton(t("coser.ui.validators.copyToDisk"));
        copyToDiskButton.addActionListener(this);
    }

    /*
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        view.getHandler().copyDefaultValidators(view);
    }

    @Override
    protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
        super.paintLayer(g2, l);
        g2.setColor(new Color(150, 150, 150, 128));
        g2.fillRect(0, 0, l.getWidth(), l.getHeight());
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        JXLayer<JComponent> l = (JXLayer<JComponent>) c;
        l.getGlassPane().setLayout(new GridBagLayout());
        l.getGlassPane().add(copyToDiskButton);
        //copyToDiskButton.setCursor(Cursor.getDefaultCursor());
    }

    @Override
    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
        JXLayer<JComponent> l = (JXLayer<JComponent>) c;
        l.getGlassPane().setLayout(new FlowLayout());
        l.getGlassPane().remove(copyToDiskButton);
    }
}

package fr.ifremer.coser;

/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.bbn.openmap.MapBean;
import com.google.common.base.Preconditions;
import fr.ifremer.coser.services.ClientResultService;
import fr.ifremer.coser.services.CommandService;
import fr.ifremer.coser.services.ControlService;
import fr.ifremer.coser.services.ProjectService;
import fr.ifremer.coser.services.PublicationService;
import jaxx.runtime.swing.session.SwingSession;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import javax.swing.UIManager;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;

/**
 * Coser UI Application context.
 *
 * Created on 3/23/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserUIApplicationContext extends DefaultCoserApplicationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CoserUIApplicationContext.class);

    /**
     * Shared application context.
     */
    protected static CoserUIApplicationContext context;

    public static CoserUIApplicationContext get() {
        Preconditions.checkState(context != null, "Application was not initialized!");
        return context;
    }

    public static CoserUIApplicationContext init() {

        Preconditions.checkState(EventQueue.isDispatchThread(), "Must init application context from Swing EDT Thread");

        context = new CoserUIApplicationContext();

        CoserConfig config = context.getConfig();

        // to work in java webstart
        try {
            I18n.init(new DefaultI18nInitializer("coser-i18n"), config.getLocale());
        } catch (RuntimeException ex) {
            if (log.isErrorEnabled()) {
                log.error("Could not load coser i18n bundle", ex);
            }
            // fallback for dev mode
            I18n.init(new ClassPathI18nInitializer(), config.getLocale());
        }

        // OpenMap sysout
        MapBean.suppressCopyright = true;

        // need to be done in Swing EDT (otherwize, don't work on javawebstart)
        // declare new classloader
        ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
        CoserClassLoader coserClassLoader = new CoserClassLoader(currentClassLoader);
        Thread.currentThread().setContextClassLoader(coserClassLoader);
        // set coser xwork validation file directory in classloader
        coserClassLoader.setValidatorsDirectory(config.getValidatorsDirectory());

        // init LAF (from configuration)
        try {
            String lafClassName = config.getLookAndFeel();
            for (UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
                if (laf.getClassName().equalsIgnoreCase(lafClassName)) {
                    UIManager.setLookAndFeel(laf.getClassName());
                }
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't init look and feel", ex);
            }
        }

        // update font size (after laf)
        config.updateSwingFont();

        File coserConfigDirectory = new File(config.getUserConfigDirectory(), "coser");
        try {
            FileUtils.forceMkdir(coserConfigDirectory);
        } catch (IOException e) {
            throw new CoserTechnicalException("Could not create user data directory", e);
        }
        File mainFrameFile = new File(coserConfigDirectory, "session.xml");

        context.session = SwingSession.newSession(mainFrameFile, true);

        // catch wall application exception
        // catch uncaught exceptions
        Thread.setDefaultUncaughtExceptionHandler(new CoserExceptionHandler());
        System.setProperty("sun.awt.exception.handler", CoserExceptionHandler.class.getName());

        return context;
    }

    public static void close() {
        context = null;
    }

    protected SwingSession session;

    protected CommandService commandService;

    protected ProjectService projectService;

    protected ControlService controlService;

    protected PublicationService publicationService;

    protected ClientResultService clientResultService;

    public SwingSession getSession() {
        return session;
    }

    public CommandService getCommandService() {
        return commandService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public ControlService getControlService() {
        return controlService;
    }

    public PublicationService getPublicationService() {
        return publicationService;
    }

    public ClientResultService getClientResultService() {
        return clientResultService;
    }

    protected CoserUIApplicationContext() {
        super(new CoserConfig());

        projectService = new ProjectService(config);
        commandService = new CommandService(config);
        controlService = new ControlService(config);
        publicationService = new PublicationService(config);
        clientResultService = new ClientResultService(config);
    }

    @Override
    public CoserConfig getConfig() {
        return (CoserConfig) config;
    }

}

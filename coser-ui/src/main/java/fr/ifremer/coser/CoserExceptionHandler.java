/*
 * #%L
 * Coser :: UI
 * %%
 * Copyright (C) 2012 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.coser;

import fr.ifremer.coser.ui.util.ErrorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * Coser global exception handler.
 *
 * Catch all application uncaught and display it in a custom JoptionPane
 * or JXErrorPane.
 *
 * See http://stackoverflow.com/a/4448569/1165234 for details.
 *
 * @author echatellier
 * @since 1.3
 */
public class CoserExceptionHandler implements UncaughtExceptionHandler {

    private static final Log log = LogFactory.getLog(CoserExceptionHandler.class);

    @Override
    public void uncaughtException(Thread t, Throwable ex) {
        handleException(t.getName(), ex);
    }

    public void handle(Throwable thrown) {
        // for EDT exceptions
        handleException(Thread.currentThread().getName(), thrown);
    }

    protected void handleException(String tname, Throwable ex) {
        if (log.isErrorEnabled()) {
            log.error("Global application exception", ex);
        }
        ErrorHelper errorHelper = new ErrorHelper();
        errorHelper.showErrorDialog(null, ex.getMessage(), ex);
    }
}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// affiche la carte correspondant à la selection dans la liste
function coserShowSelectedZoneMap(select, prefix) {
	// cache toutes les autres cartes
	$('div[id^=' + prefix + ']').hide();
	// affiche la carte selectionnee
	$('#' + prefix + $(select).val()).show();
}

// selection tous les elements d'un select
function coserSelectAll(select) {
	$(select).children("option").prop('selected',true);
    $(select).change();
}

// deselection tous les elements d'un select
function coserUnSelectAll(select) {
	$(select).children("option").prop('selected',false);
    $(select).change();
}

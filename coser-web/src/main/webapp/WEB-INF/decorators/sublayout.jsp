<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2010 - 2016 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <title><decorator:title default="Coser"/></title>
       <script type="text/javascript" language="javascript" src="<s:url value='/js/jquery-1.8.2.min.js' />"></script>
       <script type="text/javascript" language="javascript" src="<s:url value='/js/coser.js' />"></script>
       <link rel="stylesheet" type="text/css" href="<s:url value='/styles/coser.css' />" />
       <link rel="icon" type="image/png" href="<s:url value='/favicon.png' />" />
       <decorator:head/>

       <s:if test="%{!analyticsId.trim().isEmpty()}" >
       <!-- Script for Google Analytics -->
       <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', '<s:property value='analyticsId' />']);
         _gaq.push(['_trackPageview']);

         (function() {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();  
       </script>
       </s:if>
    </head>
    <body>

       <div id="conteneur_int">
         <div id="head" class="clearfix">
           <a href="http://sih.ifremer.fr/" class="logoSIH" target="_blank">Système d'Information Halieutique</a>
           <h1>
             <s:text name="message.layout.title" />
           </h1>
           <a href="http://www.ifremer.fr/" class="logoIfremer" target="_blank">L'Ifremer</a>
         </div>

         <div id="middle" class="clearfix">

           <div class="top_content clearfix">
             <a href="<s:url action='index' namespace='/' />" class="accueil"><s:text name="message.index.title" /></a>
             <a href="<s:url namespace="/" action="locale">
               <s:param name="request_locale">en</s:param>
             </s:url>" class="anglais">Anglais</a>
             <a href="<s:url namespace="/" action="locale">
               <s:param name="request_locale">fr</s:param>
             </s:url>" class="francais">Francais</a>
             <!-- <a href="<s:url namespace="/" action="locale">
               <s:param name="request_locale">es</s:param>
             </s:url>" class="espagnol">Espagnol</a> -->
           </div>
                 
           <div class="page">
             <!--<span class="clt"></span>-->
             <div class="content">
               <div class="clearfix">

                 <decorator:body/>

               </div>
             </div>
     
             <!--<span class="clb2">&nbsp;</span>-->
           </div>
           <div class="part_bottom">
             <div class="col_droite">
               <div>
                 <h3><s:text name="message.layout.oceanicdatatitle" /></h3>
                 <ul>
                   <li><a href="http://www.ifremer.fr/sismer/index_FR.htm" class="lien" target="_blank"><s:text name="message.layout.oceanicdata1" /></a></li>
                   <li><a href="http://sih.ifremer.fr/" class="lien" target="_blank"><s:text name="message.layout.oceanicdata2" /></a></li>
                 </ul>
               </div>
             </div>
           </div>
         </div>
       </div>
       <div id="footer">
         <ul class="clearfix">
           <li>
           	 <a href="http://coser.codelutin.com" title="Documentation de l'application" target="_blank">
           	   Coser
           	 </a>
           </li>
           <li>
             <a href="http://forge.codelutin.com/projects/coser/roadmap?completed=1" title="Modifications faites pour cette version" target="_blank">
           		<s:property value='applicationVersion' />
             </a>
           </li>
           <li>
             <a href="mailto:harmonie@ifremer.fr" title="Contacter un responsable">
               Contact
             </a>
           </li>
           <li>
             <a href="http://www.gnu.org/licenses/agpl.html" title="Licence AGPL v3" target="_blank">
               AGPLv3
             </a>
           </li>
           <li>
             Copyright 2010 - 2017
             <a href="http://www.ifremer.fr" title="Ifremer" target="_blank">Ifremer</a>,
             <a href="http://www.codelutin.com" title="Code Lutin" target="_blank">Code Lutin</a>
           </li>
         </ul>
       </div>
    </body>
</html>

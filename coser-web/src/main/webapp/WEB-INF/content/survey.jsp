<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2010 - 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.survey.maintitle" /></title>
       <script type="text/javascript" src="<s:url value='/js/jquery.expander.min.js' />"></script>
       <script type="text/javascript">
		$(document).ready(function() {
		  // override some default options
		  $('div.expandable').expander({
		    slicePoint:       1565,  // default is 100
		    expandText:         '<s:text name="message.common.jsreadmore" />', // default is 'read more...'
		    collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
		    userCollapseText: '[^]'  // default is '[collapse expanded text]'
		  });
		  
		});
		</script>
    </head>
    <body>
          <a name="top"></a>
          <h2>
            <s:text name="message.survey.maintitle" />
          </h2>
          
          <ul>
          	<li>
          	    <a href="#merdunord"><s:text name="message.survey.merdunord" /></a>
          		<ul>
          			<li><a href="#sudmerdunord"><s:text name="message.survey.merdunord.sudmerdunord" /></a></li>
          		</ul>
          	</li>
          	<li>
          	    <a href="#fmancheorientale"><s:text name="message.survey.mancheorientale" /></a>
          	    <ul>
          			<li><a href="#mancheorientale"><s:text name="message.survey.mancheorientale.mancheorientale" /></a></li>
          			<li><a href="#baiedesomme"><s:text name="message.survey.mancheorientale.baiedesomme" /></a></li>
          			<li><a href="#baiedeseine"><s:text name="message.survey.mancheorientale.baiedeseine" /></a></li>
          		</ul>
            </li>
          	<li>
          	    <a href="#mancheoccidentale"><s:text name="message.survey.mancheoccidentale" /></a>
          	    <ul>
          			<li><a href="#flamanville"><s:text name="message.survey.mancheoccidentale.flamanville" /></a></li>
          		</ul>
            </li>
          	<li>
          	    <a href="#atlantique"><s:text name="message.survey.atlantique" /></a>
          	    <ul>
          			<li><a href="#celtique"><s:text name="message.survey.atlantique.celtique" /></a></li>
          			<li><a href="#gascogne"><s:text name="message.survey.atlantique.gascogne" /></a></li>
          			<li><a href="#vilaine"><s:text name="message.survey.atlantique.vilaine" /></a></li>
          		</ul>
            </li>
          	<li>
          	    <a href="#mediterranee"><s:text name="message.survey.mediterranee" /></a>
          	    <ul>
          			<li><a href="#golfelion"><s:text name="message.survey.mediterranee.golfelion" /></a></li>
          			<li><a href="#estcorse"><s:text name="message.survey.mediterranee.estcorse" /></a></li>
          		</ul>
            </li>
          </ul>

          <div class="expandable">
	          <p><s:text name="message.survey.paragraph1" /></p>
	          <p><s:text name="message.survey.paragraph2" /></p>
	          
	          <p>
	            <p><s:text name="message.survey.dataengintitle" /></p>
	            <ul>
	              <li><s:text name="message.survey.dataenginfond" /></li>
	              <li><s:text name="message.survey.dataenginperche" /></li>
	              <li><s:text name="message.survey.dataengincasier" /></li>
	            </ul>
	          </p>
	          
	          <p><s:text name="message.survey.paragraph3" /></p>
	          <p><s:text name="message.survey.paragraph4" /></p>
	          <p><s:text name="message.survey.paragraph5" /></p>
	          <p><s:text name="message.survey.paragraph6" /></p>
          </div>

          <h2>
            <s:text name="message.survey.detailstitle" />
          </h2>
          
          <!--  Facade -->
          <a name="merdunord"></a>
          <h3><s:text name="message.survey.merdunord" /></h3>
          <a name="sudmerdunord"></a>
          
          <h4><s:text name="message.survey.merdunord.sudmerdunord" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.merdunord.sudmerdunord.desc" /></p>
          <p>
            <s:text name="message.survey.merdunord.sudmerdunord.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14708/" target="_blank"><s:text name="message.survey.merdunord.sudmerdunord.ibts6" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14709/" target="_blank"><s:text name="message.survey.merdunord.sudmerdunord.ibts7" /></a></li>
            </ul>
          </p>
          
          <!--  Facade -->
          <a name="fmancheorientale"></a>
          <h3><s:text name="message.survey.mancheorientale" /></h3>
          <a name="mancheorientale"></a>
          <h4><s:text name="message.survey.mancheorientale.mancheorientale" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.mancheorientale.mancheorientale.desc" /></p>
          <p>
            <s:text name="message.survey.mancheorientale.mancheorientale.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14705/" target="_blank"><s:text name="message.survey.mancheorientale.mancheorientale.cgfs1" /></a></li>
            </ul>
          </p>
          
          <a name="baiedesomme"></a>
          <h4><s:text name="message.survey.mancheorientale.baiedesomme" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.mancheorientale.baiedesomme.desc" /></p>
          <p>
            <s:text name="message.survey.mancheorientale.baiedesomme.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14710/" target="_blank"><s:text name="message.survey.mancheorientale.baiedesomme.noursomme1" /></a></li>
            <li><a href="https://archimer.ifremer.fr/doc/00435/54610/" target="_blank"><s:text name="message.survey.mancheorientale.baiedesomme.noursomme2" /></a></li>
            </ul>
          </p>
          
          <a name="baiedeseine"></a>
          <h4><s:text name="message.survey.mancheorientale.baiedeseine" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.mancheorientale.baiedeseine.desc" /></p>
          <p>
            <s:text name="message.survey.mancheorientale.baiedeseine.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14714/" target="_blank"><s:text name="message.survey.mancheorientale.baiedeseine.nourseine1" /></a></li>
            <li><a href="https://archimer.ifremer.fr/doc/00435/54610/" target="_blank"><s:text name="message.survey.mancheorientale.baiedeseine.nourseine2" /></a></li>
            </ul>
          </p>

          <!--  Facade -->
          <a name="mancheoccidentale"></a>
          <h3><s:text name="message.survey.mancheoccidentale" /></h3>
          <a name="flamanville"></a>
          <h4><s:text name="message.survey.mancheoccidentale.flamanville" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.mancheoccidentale.flamanville.desc" /></p>
          <p>
            <s:text name="message.survey.mancheoccidentale.flamanville.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14706/" target="_blank"><s:text name="message.survey.mancheoccidentale.flamanville.crustaflam1" /></a></li>
            </ul>
          </p>
          
          <!--  Facade -->
          <a name="atlantique"></a>
          <h3><s:text name="message.survey.atlantique" /></h3>
          <a name="celtique"></a>
          <h4><s:text name="message.survey.atlantique.celtique" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.atlantique.celtique.desc" /></p>
          <p>
            <s:text name="message.survey.atlantique.celtique.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14707/" target="_blank"><s:text name="message.survey.atlantique.celtique.evhoe1" /></a></li>
            </ul>
          </p>
          
          <a name="gascogne"></a>
          <h4><s:text name="message.survey.atlantique.gascogne" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <h5><s:text name="message.survey.atlantique.gascogne.evhoe" /></h5>
          <p><s:text name="message.survey.atlantique.gascogne.evhoe.desc" /></p>
          <p>
            <s:text name="message.survey.atlantique.gascogne.evhoe.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14707/" target="_blank"><s:text name="message.survey.atlantique.gascogne.evhoe1" /></a></li>
            </ul>
          </p>
          <h5><s:text name="message.survey.atlantique.gascogne.pelgas" /></h5>
          <p><s:text name="message.survey.atlantique.gascogne.pelgas.desc" /></p>
          <p>
              <s:text name="message.survey.atlantique.gascogne.pelgas.plus" />
          <ul>
              <li><a href="http://archimer.ifremer.fr/doc/00191/30259" target="_blank"><s:text name="message.survey.atlantique.gascogne.pelgas1" /></a></li>
          </ul>
          </p>
          
          <a name="vilaine"></a>
          <h4><s:text name="message.survey.atlantique.vilaine" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.atlantique.vilaine.desc" /></p>
          <p>
            <s:text name="message.survey.atlantique.vilaine.plus" />
            <ul>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14713/" target="_blank"><s:text name="message.survey.atlantique.vilaine.nourvil1" /></a></li>
            <li><a href="https://archimer.ifremer.fr/doc/00435/54610/" target="_blank"><s:text name="message.survey.atlantique.vilaine.nourvil2" /></a></li>
            </ul>
          </p>
          
          <!--  Facade -->
          <a name="mediterranee"></a>
          <h3><s:text name="message.survey.mediterranee" /></h3>
          <a name="golfelion"></a>
          <h4><s:text name="message.survey.mediterranee.golfelion" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.mediterranee.golfelion.desc" /></p>
          <p>
            <s:text name="message.survey.mediterranee.golfelion.plus" />
            <ul>
            <li><a href="#" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits1" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14711/" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits2" /></a></li>
            <li><a href="http://www.sibm.it/SITO%20MEDITS/file.doc/MEDITS-Manual_V3-1999.pdf" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits3" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14712/" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits4" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00002/11321/" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits5" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00117/22783/" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits6" /></a></li>
            <li><a href="<s:url value='/pdf/Medits_Handbook_2017_version_9_5-60417r.pdf' />" target="_blank"><s:text name="message.survey.mediterranee.golfelion.medits9" /></a></li>
            </ul>
          </p>
          
          <a name="estcorse"></a>
          <h4><s:text name="message.survey.mediterranee.estcorse" />
            <div style="float:right;text-align:right;font-weight:normal"><a href="#top"><s:text name="message.common.anchortop" /></a></div>
          </h4>
          <p><s:text name="message.survey.mediterranee.estcorse.desc" /></p>
          <p>
            <s:text name="message.survey.mediterranee.estcorse.plus" />
            <ul>
            <li><a href="#" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits1" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14711/" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits2" /></a></li>
            <li><a href="http://www.sibm.it/SITO%20MEDITS/file.doc/MEDITS-Manual_V3-1999.pdf" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits3" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00036/14712/" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits4" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00002/11321/" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits5" /></a></li>
            <li><a href="http://archimer.ifremer.fr/doc/00117/22783/" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits6" /></a></li>
            <li><a href="<s:url value='/pdf/Medits_Handbook_2017_version_9_5-60417r.pdf' />" target="_blank"><s:text name="message.survey.mediterranee.estcorse.medits9" /></a></li>
            </ul>
          </p>
    </body>
</html>

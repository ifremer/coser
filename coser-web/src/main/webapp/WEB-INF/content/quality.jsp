<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2010 - 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.quality.title" /></title>
    </head>
    <body>

          <h2><s:text name="message.quality.title" /></h2>

          <p><s:text name="message.quality.paragraph1" /></p>
          <ul>
            <li><s:text name="message.quality.paragraph2" /></li>
            <li><s:text name="message.quality.paragraph3" /></li>
            <li><s:text name="message.quality.paragraph4" /></li>
          </ul>
          <p>
            <s:text name="message.quality.paragraph5">
              <s:param><a href="mailto:<s:property value='adminEmail' />"><s:property value='adminEmail' /></a></s:param>
            </s:text>
          </p>
    </body>
</html>

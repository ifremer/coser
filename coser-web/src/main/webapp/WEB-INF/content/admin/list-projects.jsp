<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2011 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.admin.listprojects.title" /></title>
    </head>
    <body>

          <h2><s:text name="message.admin.listprojects.title" /></h2>

          <h3><s:text name="message.admin.listprojects.select.resultType" /></h3>

          <s:form action="list-projects" method="get">
            <select name="selectedRepositoryType" id="selectedRepositoryType">
              <s:iterator value="repositoryTypes" var="repositoryType">
                <option value="<s:property value="key" />"
                  <s:if test="#repositoryType.key.equals(selectedRepositoryType)">
                    selected="selected"
                  </s:if>>
                  <s:property value="%{getText(#repositoryType.value.labelKey)}" /></option>
              </s:iterator>
            </select>
            <s:submit value="%{getText('message.common.validform')}"/>
          </s:form>

          <s:if test="selectedRepositoryType == @fr.ifremer.coser.result.repository.echobase.EchoBaseResultRepositoryType@ID">
            <h3><s:text name="message.admin.listprojects.projects" /></h3>
            <p><s:text name="message.admin.listprojects.projects.comment" /></p>

            <form action="delete-projects" class="listprojects" method="post">
              <s:hidden name="repositoryType" value="%{selectedRepositoryType}"/>
              <s:hidden name="resultType" value="%{@fr.ifremer.coser.result.ResultType@MAP_AND_INDICATOR}"/>
              <s:set name="results" value="%{getResults(@fr.ifremer.coser.result.ResultType@MAP_AND_INDICATOR)}"/>
              <s:iterator value="facades">
                <div class="listprojects-facade">
                  <span class="title"><s:text name="message.common.facade" /> : <s:property value='value' /></span>
                  <s:iterator value="%{getZonesByFacades().get(key)}" var="zone">
                    <div class="listprojects-zone">
                      <span class="title"><s:text name="message.common.zone" /> : <s:property value="getZoneDisplayName(#zone)"/></span>
                      <div class="listprojects-result">
                        <s:if test="%{#results.get(#zone) == null}">
                          Aucun projet trouvé
                        </s:if>
                        <s:else>
                          <input type="checkbox" name="zonesId" value="<s:property value="zone" />" />
                          <s:property value="%{#results.get(#zone)}" />
                        </s:else>
                      </div>
                    </div>
                  </s:iterator>
                </div>
              </s:iterator>
              <s:submit value='%{getText("message.admin.listprojects.deleteselected")}'/>
            </form>
          </s:if>
          <s:elseif test="selectedRepositoryType == @fr.ifremer.coser.result.repository.legacy.LegacyResultRepositoryType@ID">

            <h3><s:text name="message.admin.listprojects.indicatorsprojects" /></h3>
            <p><s:text name="message.admin.listprojects.indicatorsprojects.comment" /></p>

            <form action="delete-projects" class="listprojects" method="post">
              <s:hidden name="repositoryType" value="%{selectedRepositoryType}"/>
              <s:hidden name="resultType" value="%{@fr.ifremer.coser.result.ResultType@INDICATOR}"/>
              <s:set name="results" value="%{getResults(@fr.ifremer.coser.result.ResultType@INDICATOR)}"/>
              <s:iterator value="facades">
                <div class="listprojects-facade">
                  <span class="title"><s:text name="message.common.facade" /> : <s:property value='value' /></span>
                  <s:iterator value="%{getZonesByFacades().get(key)}" var="zone">
                    <div class="listprojects-zone">
                      <span class="title"><s:text name="message.common.zone" /> : <s:property value="getZoneDisplayName(#zone)"/></span>
                      <div class="listprojects-result">
                        <s:if test="%{#results.get(#zone) == null}">
                          Aucun projet trouvé
                        </s:if>
                        <s:else>
                          <input type="checkbox" name="zonesId" value="<s:property value="zone" />" />
                          <s:property value="%{#results.get(#zone)}" />
                        </s:else>
                      </div>
                    </div>
                  </s:iterator>
                </div>
              </s:iterator>
              <s:submit value='%{getText("message.admin.listprojects.deleteselected")}'/>
            </form>

            <h3><s:text name="message.admin.listprojects.mapsprojects" /></h3>

            <form action="delete-projects" class="listprojects" method="post">
              <s:hidden name="repositoryType" value="%{selectedRepositoryType}"/>
              <s:hidden name="resultType" value="%{@fr.ifremer.coser.result.ResultType@MAP}"/>
              <s:set name="results" value="%{getResults(@fr.ifremer.coser.result.ResultType@MAP)}"/>
              <s:iterator value="facades">
                <div class="listprojects-facade">
                  <span class="title"><s:text name="message.common.facade" /> : <s:property value='value' /></span>
                  <s:iterator value="%{getZonesByFacades().get(key)}" var="zone">
                    <div class="listprojects-zone">
                      <span class="title"><s:text name="message.common.zone" /> : <s:property value="getZoneDisplayName(#zone)"/></span>
                      <div class="listprojects-result">
                        <s:if test="%{#results.get(#zone) == null}">
                          Aucun projet trouvé
                        </s:if>
                        <s:else>
                          <input type="checkbox" name="zonesId" value="<s:property value="zone" />" />
                          <s:property value="%{#results.get(#zone)}" />
                        </s:else>
                      </div>
                    </div>
                  </s:iterator>
                </div>
              </s:iterator>
              <s:submit value='%{getText("message.admin.listprojects.deleteselected")}'/>
            </form>
          </s:elseif>
          <s:else>
            TODO
          </s:else>
    </body>
</html>

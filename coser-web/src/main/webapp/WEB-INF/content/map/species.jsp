<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2010 - 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.map.title" /></title>
       <script type="text/javascript">
       function enableValidButton() {
    	    if ($('#specieslist').val() != "") {
              $('#speciesvalid').removeAttr('disabled');
    	    } else {
    	      $('#speciesvalid').attr('disabled', 'disabled');
    	    }
       };
           
       </script>
    </head>
    <body>

        <!-- breadcrumb -->
        <div class="breadcrumb">
          <ul>
            <li><s:a action="facade" namespace='/map'><s:text name="message.common.facade" /></s:a> &gt;</li>
            <li>
              <s:a action="zone" namespace='/map'>
                <s:param name="facade" value="facade" />
                <s:property value='facadeDisplayName' />
              </s:a> &gt;
            </li>
            <li>
              <s:a action="species" namespace='/map'>
                <s:param name="facade" value="facade" />
                <s:param name="zone" value="zone" />
                <s:property value='zoneDisplayName' />
              </s:a>
            </li>
          </ul>
        </div>
        <!-- end breadcrumb -->

        <h2><s:text name="message.map.title" /></h2>

        <img src="<s:url action='map-data'>
          <s:param name="facade" value="%{facade}" />
          <s:param name="zone" value="%{zone}" />
        </s:url>" style="float:right;margin-left:10px;" width="230" height="230"/>

        <s:if test="%{species.isEmpty()}">
          <s:text name="message.common.noresults"></s:text>
        </s:if>
        <s:else>
          <s:form action="map" method="get">
            <s:select id="specieslist" name="species" list="species" label="%{getText('message.common.selectspecies')}"
            emptyOption="true" value="" onchange="enableValidButton()"/>
            <s:hidden name="facade" property="facade"/>
            <s:hidden name="zone" property="zone"/>
            <s:submit id="speciesvalid" value="%{getText('message.common.validform')}" disabled="true" />
          </s:form>
        </s:else>
    </body>
</html>

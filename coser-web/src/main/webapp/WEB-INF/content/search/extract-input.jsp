<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2012 Ifremer, Codelutin, Chemit Tony
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.search.extract.title" /></title>
      <style type="text/css">
        .select {
          width:100%;
        }
      </style>
      <script type="text/javascript">
          var checkNextAction = function() {

              var valid = $('#selectZones option:selected').length > 0;
              if (valid) {
                  valid = $('#selectTypes option:selected').length > 0;
              }
              var action = $('#nextAction');
              if (valid) {
                  action.removeAttr('disabled');
              } else {
                  action.attr('disabled', 'disabled');
              }
              $('#extractPart').hide();
          };
          var checkSubmitAction = function() {

              var valid = true;

              if (valid && $('#selectSpecies').is(":visible")) {
                  valid = $('#selectSpecies option:selected').length > 0;
              }
              if (valid && $('#selectComIndicators').is(":visible")) {
                  valid = $('#selectComIndicators option:selected').length > 0;
              }
              if (valid && $('#selectPopIndicators ').is(":visible")) {
                  valid = $('#selectPopIndicators option:selected').length > 0;
              }
              var action = $('#submitAction');
              if (valid) {
                  action.removeAttr('disabled');
              } else {
                  action.attr('disabled', 'disabled');
              }
              return valid;
          };
        <s:if test="!selectZones.empty">

          $(document).ready(function () {
              $('.select1').change(checkNextAction).trigger( "change" );
              $('.select2').change(checkSubmitAction).trigger( "change" );
              checkNextAction();
              checkSubmitAction();
              $('#extractPart').show();
          });

        </s:if>
        <s:else>
          $(document).ready(function () {
              $('.select1').change(checkNextAction).trigger( "change" );

              checkNextAction();
          });

        </s:else>

      </script>
    </head>
    <body>

        <form action="<s:url action="extract" />" method="post">

          <h2><s:text name="message.search.extract.zonetype" /></h2>
          
          <table style="width:100%">
           <tr>
            <td><s:text name="message.common.zones" /> :</td>
            <td>
              <select name="selectZones" multiple="multiple" id="selectZones"
                      class="select select1">
                <s:iterator value="zones" var="zone">
                  <option value="<s:property value="key" />"
                  <s:if test="selectZones.contains(#zone.key)">
                    selected="selected"
                  </s:if>>
                  <s:property value="value" /></option>
                </s:iterator>
              </select></td>
              <td style="vertical-align:top">
                <img src="<s:url value='/images/stock_select_table.png' />"
                    onClick="javascript:coserSelectAll($('#selectZones'))"
                    title="<s:text name="message.common.selectall" />" />
                <br />
                <img src="<s:url value='/images/stock_select_clear.png' />"
                    onClick="javascript:coserUnSelectAll($('#selectZones'))"
                    title="<s:text name="message.common.selectnone" />" /></td>
           </tr>
           <tr>
            <td><s:text name="message.common.datatypes" /> :</td>
            <td>
              <select name="selectTypes" multiple="multiple" id="selectTypes"
                      class="select select1">
                <s:iterator value="types" var="type">
                  <option value="<s:property value="key" />"
                    <s:if test="selectTypes.contains(#type.key)">
                      selected="selected"
                    </s:if>>
                    <s:property value="value" /></option>
                </s:iterator>
              </select>
            </td>
              <td style="vertical-align:top">
                <img src="<s:url value='/images/stock_select_table.png' />"
                    onClick="javascript:coserSelectAll($('#selectTypes'))"
                    title="<s:text name="message.common.selectall" />"/>
                <br />
                <img src="<s:url value='/images/stock_select_clear.png' />"
                    onClick="javascript:coserUnSelectAll($('#selectTypes'))"
                    title="<s:text name="message.common.selectnone" />" />
              </td>
           </tr>
           <tr>
            <td colspan="2"><s:submit id='nextAction'
                                      value="%{getText('message.search.extract.updatelists')}" /></td>
           </tr>
          </table>

          <s:if test="!selectZones.empty">
<span id="extractPart">
          <s:if test="needSpeciesOrIndicators">

          <h2><s:text name="message.search.extract.speciesindicators" /></h2>

          <table style="width:100%">
          <s:if test="comIndicators.size() != 0">
             <tr>
              <td><s:text name="message.common.indicatorsof" /><br />
              <s:text name="message.common.community" />&thinsp;:</td>
              <td>
                <select name="selectComIndicators" multiple="multiple"
                        class="select select2" size="10" id="selectComIndicators">
                  <s:iterator value="comIndicators" var="comIndicator">
                    <option value="<s:property value="key" />"
                    <s:if test="selectComIndicators.contains(#comIndicator.key)">
                      selected="selected"
                    </s:if>>
                    <s:property value="value" /></option>
                  </s:iterator>
                </select></td>
                <td style="vertical-align:top">
                  <img src="<s:url value='/images/stock_select_table.png' />"
                    onClick="javascript:coserSelectAll($('#selectComIndicators'))"
                    title="<s:text name="message.common.selectall" />" />
                  <br />
                  <img src="<s:url value='/images/stock_select_clear.png' />"
                    onClick="javascript:coserUnSelectAll($('#selectComIndicators'))"
                    title="<s:text name="message.common.selectnone" />" /></td>
             </tr>
           </s:if>
           <s:if test="popIndicators.size() != 0">
             <tr>
              <td><s:text name="message.common.indicatorsof" /><br />
              <s:text name="message.common.population" />&thinsp;:</td>
              <td>
                <select name="selectPopIndicators" multiple="multiple"
                        class="select select2"  size="10" id="selectPopIndicators">
                  <s:iterator value="popIndicators" var="popIndicator">
                    <option value="<s:property value="key" />"
                    <s:if test="selectPopIndicators.contains(#popIndicator.key)">
                      selected="selected"
                    </s:if>>
                    <s:property value="value" /></option>
                  </s:iterator>
                </select></td>
                <td style="vertical-align:top">
                  <img src="<s:url value='/images/stock_select_table.png' />"
                    onClick="javascript:coserSelectAll($('#selectPopIndicators'))"
                    title="<s:text name="message.common.selectall" />" />
                  <br />
                  <img src="<s:url value='/images/stock_select_clear.png' />"
                    onClick="javascript:coserUnSelectAll($('#selectPopIndicators'))"
                    title="<s:text name="message.common.selectnone" />" /></td>
             </tr>
           </s:if>
           <s:if test="species != null">
               <tr>
                   <td><s:text name="message.common.species" /> :</td>
                   <td>
                       <select name="selectSpecies" multiple="multiple"
                               class="select select2" size="10" id="selectSpecies">
                           <s:iterator value="species" var="specy">
                               <option value="<s:property value="key" />"
                                   <s:if test="selectSpecies.contains(#specy.key)">
                                       selected="selected"
                                   </s:if>>
                                   <s:property value="value" /></option>
                           </s:iterator>
                       </select></td>
                   <td style="vertical-align:top">
                       <img src="<s:url value='/images/stock_select_table.png' />"
                            onClick="javascript:coserSelectAll($('#selectSpecies'))"
                            title="<s:text name="message.common.selectall" />" />
                       <br />
                       <img src="<s:url value='/images/stock_select_clear.png' />"
                            onClick="javascript:coserUnSelectAll($('#selectSpecies'))"
                            title="<s:text name="message.common.selectnone" />" /></td>
               </tr>
           </s:if>
          </s:if>
           <tr>
            <td colspan="2"><s:submit name="submitAction"
                                      value="%{getText('message.search.extract.extract')}"
                                      disabled="true"/></td>
           </tr>
          </table>
          </s:if>
</span>
        </form>
    </body>
</html>

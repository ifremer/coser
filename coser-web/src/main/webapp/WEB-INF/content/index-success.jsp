<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2010 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.index.title" /></title>
    </head>
    <body>

          <p><s:text name="message.index.paragraph1" /></p>
          <p><s:text name="message.index.paragraph2" /></p>
          <p><s:text name="message.index.paragraph3" /></p>

          <p>
            <h2><s:text name="message.index.datatypetitle" /></h2>
            <s:text name="message.index.paragraph4">
              <s:param><a href="http://sih.ifremer.fr/">sih.ifremer.fr</a></s:param>
            </s:text>
            <ul class="datalist">
              <li><s:a action="facade" namespace='/map'><s:text name="message.index.datatypemap" /></s:a></li>
              <li><s:a action="facade" namespace='/pop'><s:text name="message.index.datatypepop" /></s:a></li>
              <li><s:a action="facade" namespace='/com'><s:text name="message.index.datatypecom" /></s:a></li>
              <li><s:a action="facade" namespace='/source'><s:text name="message.index.datatypesource" /></s:a></li>
            </ul>
          </p>
          
          <p>
            <h2><s:text name="message.index.extractdatatitle" /></h2>
            <p><s:text name="message.index.paragraph5" /></p>
            <s:a action="extract" namespace="/search">
              <s:text name="message.index.extractdatalink" />
            </s:a>
          </p>

          <p>
            <h2><s:text name="message.index.surveytitle" /></h2>
            <s:a action="survey" namespace='/'>
              <s:text name="message.index.surveyparagraph" />
            </s:a>
          </p>
          
          <p>
            <h2><s:text name="message.index.thankstitle" /></h2>
            <s:text name="message.index.thanksparagraph1" />
          </p>
          
          <!--p>
            <h2><s:text name="message.index.partnertitle" /></h2>
            <s:text name="message.index.partnerparagraph1" />
          </p-->

          <p>
            <span style="font-weight:bold"><s:text name="message.index.qualitytitle"/>&thinsp;:</span>
            <s:a action="quality"><s:text name="message.index.qualitymessage" /></s:a>
          </p>
          
          <!--p>
            <span style="font-weight:bold"><s:text name="message.index.documentstitle"/>&thinsp;:</span>
            <s:a action="documents"><s:text name="message.index.documentsmessage" /></s:a>
          </p-->

          <p class="annotations">
            <span style="font-weight:bold"><s:text name="message.index.quotetitle"/>&thinsp;:</span>
            <s:text name="message.index.quotemessage">
              <s:param value="dataUpdateDate"/>
              <s:param value="contextUrl"/>
            </s:text></p>
    </body>
</html>

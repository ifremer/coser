<!--
  #%L
  Coser :: Web
  %%
  Copyright (C) 2010 - 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <title><s:text name="message.source.title" /></title>
    </head>
    <body>

        <!-- breadcrumb -->
        <div class="breadcrumb">
          <ul>
            <li><s:a action="facade" namespace='/source'><s:text name="message.common.facade" /></s:a> &gt;</li>
            <li>
              <s:a action="zone" namespace='/source'>
                <s:param name="facade" value="facade" />
                <s:property value='facadeDisplayName' />
              </s:a>
            </li>
          </ul>
        </div>
        <!-- end breadcrumb -->

        <h2><s:text name="message.source.title" /></h2>

        <s:if test="%{zones.isEmpty()}">
          <s:text name="message.common.noresults"></s:text>
        </s:if>
        <s:else>
          <s:form action="source" method="get">
            <s:hidden name="facade" property="facade"/>
            <s:select name="zone" list="zones" label="%{getText('message.common.selectzone')}" emptyOption="true"
              onchange="javascript:coserShowSelectedZoneMap(this, 'coserzonemap')"/>
            <s:submit value="%{getText('message.common.validform')}"/>
          </s:form>
        </s:else>
        
        <hr />

        <!-- utilisé dynamiquement par le script JS -->
        <s:iterator value="zonesPictures.entrySet()">
          <div id="coserzonemap<s:property value='%{key}' />" style="display:none">
            <div>
              <img src="<s:url value='/images/zones/' /><s:property value='%{value}' />" />
            </div>

            <s:property value='%{zonesMetaInfo.get(key)}' />
          </div>
        </s:iterator>
        <div id="coserzonemap">
            <img src="<s:url value='/images/facadesmap.png' />" />
        </div>
        
    </body>
</html>

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.pop;

import fr.ifremer.coser.result.request.GetPopulationIndicatorResultDataRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;

/**
 * Télécharge les données qui ont servi a généré le graph au format CSV.
 *
 * Parametre : zone, species, indicator.
 *
 * @author chatellier
 */
public class GraphDownloadAction extends AbstractCoserJspAction {

    private static final long serialVersionUID = 3385467755357775199L;

    protected String facade;

    protected String zone;

    protected String species;

    protected String indicator;

    protected FileResult result;

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getSource() {
        return result.getSource();
    }

    @Action(results = {@Result(type = "stream", params = {"contentType", "text/csv", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String execute() {
        GetPopulationIndicatorResultDataRequest request =
                requestBuilder(GetPopulationIndicatorResultDataRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        addSpecies(species).
                        addIndicator(indicator).
                        toRequest();
        result = getService().toFileResult(request);
        return SUCCESS;
    }

    public String getFilename() {
        return indicator + ".csv";
    }

    public InputStream getInputStream() {
        InputStream input = result.getInputStream();
        return input;
    }

}

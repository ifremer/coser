/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.source;

import fr.ifremer.coser.result.request.GetZonesForExtractRawDataRequest;
import fr.ifremer.coser.web.actions.common.CommonZone;

/**
 * Affiche la liste des sous zones (zone).
 *
 * @author chatellier
 */
public class ZoneAction extends CommonZone {

    private static final long serialVersionUID = 3385467755357775199L;

    @Override
    protected GetZonesForExtractRawDataRequest createZonesRequest() {
        GetZonesForExtractRawDataRequest request =
                requestBuilder(GetZonesForExtractRawDataRequest.class).
                        addFacade(facade).
                        toRequest();
        return request;
    }

}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.common;

import fr.ifremer.coser.result.CoserRequest;

import java.util.Map;

/**
 * Recupere la liste des indicateurs à partir d'une zone et d'une espece (peut
 * être null dans le cas des communautés).
 *
 * @author chatellier
 */
public abstract class CommonIndicator extends AbstractCoserJspAction {


    private static final long serialVersionUID = 1663244944108703571L;

    protected String facade;

    protected String zone;

    protected String zonePicture;

    protected Map<String, String> indicators;

    /**
     * @return the request to get zones
     */
    protected abstract CoserRequest createZonesRequest();

    /**
     * @return the request to get indicators
     */
    protected abstract CoserRequest createIndicatorsRequest();

    public String getFacade() {
        return facade;
    }

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Map<String, String> getIndicators() {
        return indicators;
    }

    @SuppressWarnings("unused")
    public String getZonePicture() {
        return zonePicture;
    }

    public String getFacadeDisplayName() {
        return getService().getFacadeDisplayName(facade);
    }

    public String getZoneDisplayName() {
        CoserRequest zonesRequest = createZonesRequest();
        return getService().getZoneDisplayName(zonesRequest, zone);
    }

    @Override
    public String execute() {

        CoserRequest request = createIndicatorsRequest();
        indicators = getService().toMap(request);
        zonePicture = getService().getZonePictures().get(zone);

        return SUCCESS;
    }

}

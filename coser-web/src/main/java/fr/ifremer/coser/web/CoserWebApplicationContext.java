package fr.ifremer.coser.web;

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coser.DefaultCoserApplicationContext;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

/**
 * Created on 3/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserWebApplicationContext extends DefaultCoserApplicationContext {

    /**
     * Shared application context.
     */
    protected static CoserWebApplicationContext context;

    public static CoserWebApplicationContext get() {
        Preconditions.checkState(context != null, "Application was not initialized!");
        return context;
    }

    public static void init() {

        DefaultI18nInitializer i18nInitializer = new DefaultI18nInitializer("coser-i18n");
        // To see on screen none translated sentences
        i18nInitializer.setMissingKeyReturnNull(true);
        I18n.init(i18nInitializer, null);

        context = new CoserWebApplicationContext();
    }

    public static void close() {
        context = null;
    }

    public CoserWebApplicationContext() {
        super(new CoserWebConfig());
    }

    @Override
    public CoserWebConfig getConfig() {
        return (CoserWebConfig) config;
    }
}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.source;

import fr.ifremer.coser.result.request.GetZonesForExtractRawDataRequest;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;

import java.util.Map;

/**
 * Affiche les liens de téléchargement du zip pour le projet choisit.
 *
 * @author chatellier
 */
public class SourceAction extends AbstractCoserJspAction {

    private static final long serialVersionUID = 3385467755357775199L;

    protected String facade;

    protected String zone;

    protected String zonePicture;

    public String getFacade() {
        return facade;
    }

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getZonePicture() {
        return zonePicture;
    }

    public String getFacadeDisplayName() {
        return getService().getFacadeDisplayName(facade);
    }

    public String getZoneDisplayName() {
        GetZonesForExtractRawDataRequest zonesRequest =
                requestBuilder(GetZonesForExtractRawDataRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        toRequest();
        return getService().getZoneDisplayName(zonesRequest, zone);
    }

    @Override
    public String execute() {
        Map<String, String> zonePictures = getService().getZonePictures();
        zonePicture = zonePictures.get(zone);
        return SUCCESS;
    }

}

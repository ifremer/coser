/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.admin;

import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.request.DeleteResultsRequest;
import fr.ifremer.coser.web.actions.common.AbstractCoserAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;

import java.util.List;

/**
 * Project list action.
 *
 * @author chatellier
 */
@Result(type = "redirect", location = "list-projects", params = {"selectedRepositoryType", "${repositoryType}"})
public class DeleteProjectsAction extends AbstractCoserAction {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(DeleteProjectsAction.class);

    /**
     * Type of result repository.
     */
    protected String repositoryType;

    /**
     * Type of result to delete.
     */
    protected ResultType resultType;

    /**
     * List of result id to delete.
     */
    protected List<String> zonesId;

    public void setRepositoryType(String repositoryType) {
        this.repositoryType = repositoryType;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    public void setZonesId(List<String> zonesId) {
        this.zonesId = zonesId;
    }

    @Override
    public String execute() {

        if (log.isInfoEnabled()) {
            log.info(String.format("Delete zone (type %s) (repository type %s) : %s",
                                   resultType,
                                   repositoryType,
                                   zonesId));
        }

        DeleteResultsRequest request = requestBuilder(DeleteResultsRequest.class).
                addRepositoryType(repositoryType).
                addResultType(resultType).
                addZoneList(zonesId).
                toRequest();

        getService().deleteResults(request);

        return SUCCESS;
    }

    public String getRepositoryType() {
        return repositoryType;
    }

}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.common;

import fr.ifremer.coser.result.CoserRequest;

import java.util.Map;

/**
 * Affiche la liste des sous zones (zone) et leurs liste des cartes
 * et commentaires associés.
 *
 * @author chatellier
 */
public abstract class CommonZone extends AbstractCoserJspAction {

    private static final long serialVersionUID = 3385467755357775199L;

    protected String facade;

    protected Map<String, String> zones;

    protected Map<String, String> zonesPictures;

    protected Map<String, String> zonesMetaInfo;

    /**
     * @return the request to get zones
     */
    protected abstract CoserRequest createZonesRequest();

    public String getFacade() {
        return facade;
    }

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public Map<String, String> getZones() {
        return zones;
    }

    public Map<String, String> getZonesPictures() {
        return zonesPictures;
    }

    public Map<String, String> getZonesMetaInfo() {
        return zonesMetaInfo;
    }

    public String getFacadeDisplayName() {
        return getService().getFacadeDisplayName(facade);
    }

    @Override
    public String execute() {

        CoserRequest request = createZonesRequest();
        zones = getService().toMap(request);
        zonesPictures = getService().getZonePictures();
        zonesMetaInfo = getService().getZoneMetaInfo(getLocale());

        return SUCCESS;
    }

}

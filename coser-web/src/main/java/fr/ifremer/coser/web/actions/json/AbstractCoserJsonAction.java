/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.coser.web.actions.json;

import fr.ifremer.coser.web.actions.common.AbstractCoserAction;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

/**
 * Json abstract action.
 *
 * Just define in your implementation the getter to expose.
 *
 * Created on 3/21/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
@ParentPackage("json-default")
@Results({@Result(name = AbstractCoserJsonAction.SUCCESS, type = "json")})
public abstract class AbstractCoserJsonAction extends AbstractCoserAction {

    private static final long serialVersionUID = 1L;

}

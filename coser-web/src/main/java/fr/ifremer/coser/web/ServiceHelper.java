package fr.ifremer.coser.web;

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.xwork2.ActionSupport;
import fr.ifremer.coser.bean.ZoneMap;
import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.CoserRequestContext;
import fr.ifremer.coser.result.CoserRequestExecutor;
import fr.ifremer.coser.result.repository.ResultRepository;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.request.DeleteResultsRequest;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.result.result.MapResult;
import fr.ifremer.coser.services.WebResultService;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Wrap any call to business layer.
 *
 * Created on 3/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
//TODO Inject this in action
public class ServiceHelper {

    protected final ActionSupport action;

    protected final CoserWebApplicationContext applicationContext;

    protected final WebResultService webResultService;

    public ServiceHelper(ActionSupport action) {
        Preconditions.checkNotNull(action);
        this.applicationContext = CoserWebApplicationContext.get();
        this.action = action;
        this.webResultService = new WebResultService(applicationContext);
    }

    // --------------------------------------------------------------------- //
    // --- Facade methods -------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public Map<String, String> getFacades() {
        return applicationContext.getZoneMap().getFacades();
    }

    public String getFacadeDisplayName(String facade) {
        Map<String, String> facades = getFacades();
        return facades.get(facade);
    }

    // --------------------------------------------------------------------- //
    // --- Zone methods ---------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public Map<String, List<String>> getZoneByFacade() {
        return applicationContext.getZoneMap().getZoneByFacade();
    }

    public Map<String, String> getZonesForFacade(String facade) {
        ZoneMap zoneMap = applicationContext.getZoneMap();
        List<String> zoneIds = zoneMap.getZonesForFacade(facade);
        Map<String, String> map = Maps.newHashMap();
        for (String zoneId : zoneIds) {
            String zoneName = zoneMap.getZoneFullNameWithNoFacade(zoneId);
            map.put(zoneId, zoneName);
        }
        return map;
    }

    public Map<String, String> getZonePictures() {
        return applicationContext.getZoneMap().getZonePictures();
    }

    public Map<String, String> getZoneMetaInfo(Locale locale) {
        return applicationContext.getZoneMap().getZoneMetaInfo(locale);
    }

    public String getZoneFullName(String zoneId) {
        return applicationContext.getZoneMap().getZoneFullName(zoneId);
    }

    public String getZoneDisplayName(CoserRequest request, String zone) {
        Map<String, String> availableZones = toMap(request);
        String displayName = availableZones.get(zone);
        return displayName;
    }

    // --------------------------------------------------------------------- //
    // --- Species methods ------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public String getSpeciesDisplayName(CoserRequest request, String species) {
        Map<String, String> availableSpecies = toMap(request);
        String displayName = availableSpecies.get(species);
        return displayName;
    }

    // --------------------------------------------------------------------- //
    // --- Indicator methods ----------------------------------------------- //
    // --------------------------------------------------------------------- //

    public Set<String> getIndicatorIds() {
        return applicationContext.getIndicatorMap().getIds();
    }

    public String getIndicatorDisplayName(CoserRequest request, String indicator) {
        Map<String, String> availableIndicators = toMap(request);
        String displayName = availableIndicators.get(indicator);
        return displayName;
    }

    // --------------------------------------------------------------------- //
    // --- Result methods -------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public FileResult extractRawDataAndResults(Locale locale,
                                               ExtractRawDataAndResultsRequest request) {
        CoserRequestContext context = newRequestContext(locale);
        FileResult result = webResultService.extractRawDataAndResults(context, request);
        return result;
    }

    public void deleteResults(DeleteResultsRequest request) {
        CoserRequestContext context = newRequestContext();
        webResultService.deleteResults(context, request);
    }

    public void registerNewUploadedResults(String login, File resultFile) {
        CoserRequestContext context = newRequestContext();
        webResultService.registerNewUploadedResults(context, login, resultFile);
    }

    public FileResult toFileResult(CoserRequest request) {
        CoserRequestContext context = newRequestContext();
        FileResult result = webResultService.executeUnique(context, request).toFileResult();
        return result;
    }

    public FileResult toFirstFileResult(CoserRequest request) {
        CoserRequestContext context = newRequestContext();
        FileResult result = webResultService.executeFirst(context, request).toFileResult();
        return result;
    }

    public MapResult toMapResult(CoserRequest request) {
        CoserRequestContext context = newRequestContext();
        MapResult result = webResultService.executeUnique(context, request).toMapResult();
        return result;
    }

    public Map<String, String> toMap(CoserRequest request) {
        CoserRequestContext context = newRequestContext();
        Map<String, String> result = webResultService.executeAll(context, request).toMap();
        return result;
    }

    // --------------------------------------------------------------------- //
    // --- Misc methods ---------------------------------------------------- //
    // --------------------------------------------------------------------- //

    public CoserWebConfig getConfig() {
        return applicationContext.getConfig();
    }

    public void reloadProjects() {
        webResultService.resetRepositories();
    }

    public Map<String, ResultRepositoryType> getRepositoryTypes() {
        Map<String, ResultRepositoryType> result = webResultService.getRepositoryTypes();
        return result;
    }

    public ResultRepositoryType getMatchingRepositoryType(CoserRequest request) {
        CoserRequestContext context = newRequestContext();
        CoserRequestExecutor coserRequestExecutor = webResultService.executeAll(context, request);
        //FIXME We tak the first repository...
        ResultRepository matchingRepository = coserRequestExecutor.getFirstMatchingRepository();
        return matchingRepository.getResultRepositoryType();
    }

    // --------------------------------------------------------------------- //
    // --- Internal methods ------------------------------------------------ //
    // --------------------------------------------------------------------- //

    protected CoserRequestContext newRequestContext() {
        return newRequestContext(action.getLocale());
    }

    protected CoserRequestContext newRequestContext(Locale locale) {
        return webResultService.newRequestContext(locale);
    }

}

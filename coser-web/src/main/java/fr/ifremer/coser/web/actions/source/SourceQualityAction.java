/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.source;

/**
 * Affiche l'avertissement qualité avant le téléchargement du zip.
 *
 * @author chatellier
 */
public class SourceQualityAction extends SourceAction {

    private static final long serialVersionUID = 3385467755357775199L;

    @Override
    public String execute() {
        return SUCCESS;
    }
}

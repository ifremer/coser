/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.admin;

import com.google.common.collect.Maps;
import fr.ifremer.coser.result.ResultType;
import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.request.GetAllResultsRequest;
import fr.ifremer.coser.web.CoserWebException;
import fr.ifremer.coser.web.actions.common.AbstractCoserAction;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Project list action.
 *
 * @author chatellier
 */
public class ListProjectsAction extends AbstractCoserAction {

    private static final long serialVersionUID = 1L;

    // All types of result repository
    protected Map<String, ResultRepositoryType> repositoryTypes;

    // Selected result repository type
    protected String selectedRepositoryType;

    protected Map<String, String> facades;

    protected Map<String, List<String>> zonesByFacades;

    protected Map<ResultType, Map<String, String>> results;

    public Map<String, ResultRepositoryType> getRepositoryTypes() {
        return repositoryTypes;
    }

    public String getSelectedRepositoryType() {
        return selectedRepositoryType;
    }

    public void setSelectedRepositoryType(String selectedRepositoryType) {
        this.selectedRepositoryType = selectedRepositoryType;
    }

    public Map<String, String> getFacades() {
        return facades;
    }

    public Map<String, List<String>> getZonesByFacades() {
        return zonesByFacades;
    }

    public Map<String, String> getResults(ResultType resultType) {
        return results.get(resultType);
    }

    public String getZoneDisplayName(String zoneId) {
        return getService().getZoneFullName(zoneId);
    }

    @Override
    public String execute() {

        repositoryTypes = getService().getRepositoryTypes();
        if (MapUtils.isEmpty(repositoryTypes)) {
            throw new CoserWebException("No result repository type defined!");
        }

        if (StringUtils.isEmpty(selectedRepositoryType)) {

            // use first value
            ResultRepositoryType repositoryType = repositoryTypes.values().iterator().next();
            selectedRepositoryType = repositoryType.getId();
        }

        facades = getService().getFacades();
        zonesByFacades = getService().getZoneByFacade();

        GetAllResultsRequest request = requestBuilder(GetAllResultsRequest.class).
                addRepositoryType(getSelectedRepositoryType()).
                toRequest();

        results = Maps.newEnumMap(ResultType.class);
        if (selectedRepositoryType != null) {
            ResultRepositoryType repositoryType = repositoryTypes.get(selectedRepositoryType);
            Set<ResultType> resultTypes = repositoryType.getResultTypes();
            for (ResultType resultType : resultTypes) {

                // get projects for this type
                request.setResultType(resultType);
                Map<String, String> resultsForType = getService().toMap(request);
                results.put(resultType, resultsForType);
            }
        }

        return SUCCESS;
    }

}

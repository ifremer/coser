/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.map;

import fr.ifremer.coser.result.request.GetMapResultRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;

/**
 * Appelé par le navigateur pour récuperer le contenu de l'image.
 *
 * @author chatellier
 */
public class MapDataAction extends AbstractCoserJspAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MapDataAction.class);

    private static final long serialVersionUID = 1663244944108703571L;

    protected String facade;

    protected String zone;

    protected String species;

    protected FileResult result;

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getSource() {
        return result.getSource();
    }

    @Action(results = {@Result(type = "stream", params = {"contentType", "image/png"})})
    public String execute() {
        // work with null species (get Repartition-stations map)
        GetMapResultRequest request = requestBuilder(GetMapResultRequest.class).
                addFacade(facade).
                addZone(zone).
                addSpecies(species == null ? GetMapResultRequest.NULL_SPECIES : species).
                toRequest();

        if (log.isInfoEnabled()) {
            log.info("Looking for map of species: " + request.getSpecies());
        }
        result = getService().toFileResult(request);
        return SUCCESS;
    }

    public InputStream getInputStream() {
        InputStream input = result.getInputStream();
        return input;
    }

}

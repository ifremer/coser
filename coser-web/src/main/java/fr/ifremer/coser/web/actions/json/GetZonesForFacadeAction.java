package fr.ifremer.coser.web.actions.json;

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Map;

/**
 * Get all zones for a given facade.
 *
 * Created on 3/21/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class GetZonesForFacadeAction extends AbstractCoserJsonAction {

    private static final long serialVersionUID = 1L;

    /**
     * Selected facade.
     */
    protected String facade;

    protected Map<String, String> data;

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public Map<String, String> getData() {
        if (data == null) {
            data = getService().getZonesForFacade(facade);
        }
        return data;
    }

}

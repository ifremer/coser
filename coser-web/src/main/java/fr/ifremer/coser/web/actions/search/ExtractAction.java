/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.search;

import com.google.common.collect.Lists;
import fr.ifremer.coser.result.request.ExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetIndicatorsForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetSpeciesForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.request.GetZonesForExtractRawDataAndResultsRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.util.DataType;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Affiche le formulaire de recherche pour extraction d'un zip contenant
 * les sources des projets choisit, et un pdf avec les graphiques et les données
 * des graphiques.
 *
 * @author echatellier
 * @since 1.4
 */
@InterceptorRefs({
        @InterceptorRef("defaultStack"),
        @InterceptorRef(value = "execAndWait",
                params = {"excludeMethods", "execute,quality"})
})
public class ExtractAction extends AbstractCoserJspAction implements ServletRequestAware {

    private static final long serialVersionUID = 8497086194191374797L;

    public static final String DOWNLOAD = "download";

    public static final String LOCALE_ATTRIBUTE = "locale";

    protected Map<String, String> zones;

    protected Map<String, String> types;

    protected List<String> selectZones;

    protected List<DataType> selectTypes;

    protected Map<String, String> species;

    protected List<String> selectSpecies;

    protected Map<String, String> comIndicators;

    protected List<String> selectComIndicators;

    protected Map<String, String> popIndicators;

    protected List<String> selectPopIndicators;

    protected String submitAction;

    protected boolean accepted;

    //FIXME Should only use the struts2 session ?
    protected transient HttpServletRequest servletRequest;

    protected FileResult resultFile;

    public Map<String, String> getZones() {
        return zones;
    }

    public List<String> getSelectZones() {
        return selectZones;
    }

    public void setSelectZones(List<String> selectZones) {
        this.selectZones = selectZones;
    }

    public Map<String, String> getTypes() {
        return types;
    }

    public List<DataType> getSelectTypes() {
        return selectTypes;
    }

    public void setSelectTypes(List<DataType> selectTypes) {
        this.selectTypes = selectTypes;
    }

    public List<String> getSelectSpecies() {
        return selectSpecies;
    }

    public void setSelectSpecies(List<String> selectSpecies) {
        this.selectSpecies = selectSpecies;
    }

    public List<String> getSelectComIndicators() {
        return selectComIndicators;
    }

    public void setSelectComIndicators(List<String> selectComIndicators) {
        this.selectComIndicators = selectComIndicators;
    }

    public List<String> getSelectPopIndicators() {
        return selectPopIndicators;
    }

    public void setSelectPopIndicators(List<String> selectPopIndicators) {
        this.selectPopIndicators = selectPopIndicators;
    }

    public Map<String, String> getSpecies() {
        return species;
    }

    public Map<String, String> getComIndicators() {
        return comIndicators;
    }

    public Map<String, String> getPopIndicators() {
        return popIndicators;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    // used by validation
    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    /**
     * Pour le hack du execute and wait.
     * On met des choses dans la session pour pouvoir les récupérer.
     */
    @Override
    public void setServletRequest(HttpServletRequest servletrequest) {
        this.servletRequest = servletrequest;
    }

    @Override
    public String execute() {

        String result;

        if (StringUtils.isNotBlank(submitAction)) {
            result = SUCCESS;
        } else {
            result = INPUT;

            types = DataType.getExtractTypes(getLocale());

            // renvoi la liste des id subzone-survey et leurs label associé
            GetZonesForExtractRawDataAndResultsRequest zonesRequest =
                    requestBuilder(GetZonesForExtractRawDataAndResultsRequest.class).
                            toRequest();
            zones = getService().toMap(zonesRequest);

            if (CollectionUtils.isNotEmpty(selectZones) && CollectionUtils.isNotEmpty(selectTypes)) {

                GetIndicatorsForExtractRawDataAndResultsRequest indicatorsRequest =
                        requestBuilder(GetIndicatorsForExtractRawDataAndResultsRequest.class).
                                addZoneList(selectZones).
                                toRequest();

                if (selectTypes.contains(DataType.COMMUNITY)) {

                    indicatorsRequest.setExtractTypeList(Lists.newArrayList(DataType.COMMUNITY));
                    comIndicators = getService().toMap(indicatorsRequest);
                }

                if (selectTypes.contains(DataType.POPULATION)) {
                    indicatorsRequest.setExtractTypeList(Lists.newArrayList(DataType.POPULATION));
                    popIndicators = getService().toMap(indicatorsRequest);
                }

                if (DataType.isNeedSpecies(selectTypes)) {

                    GetSpeciesForExtractRawDataAndResultsRequest speciesRequest =
                            requestBuilder(GetSpeciesForExtractRawDataAndResultsRequest.class).
                                    addZoneList(selectZones).
                                    addExtractTypeList(selectTypes).
                                    toRequest();
                    species = getService().toMap(speciesRequest);

                }
            }
        }

        return result;
    }

    @Action(value = "extract-quality",
            results = {
                    @Result(name = "input", location = "/WEB-INF/content/search/extract-success.jsp"),
                    @Result(name = DOWNLOAD, type = "redirect", params = {"location", "${location}"})})
    public String quality() {
        // petit hack pour mettre la locale dans la session car
        // après, on n'a plus accès au context dans le executeAndWait
        servletRequest.getSession().setAttribute(LOCALE_ATTRIBUTE, getLocale());
        return DOWNLOAD;
    }

    /**
     * C'est super dur des faire une redirection sur une url.
     * Construction d'une url complete.
     *
     * @return l'url de redirection
     */
    public String getLocation() {
        String url = "extract-download?accepted=true";
        if (selectZones != null) {
            for (String zone : selectZones) {
                url += "&selectZones=" + zone;
            }
        }
        if (selectTypes != null) {
            for (DataType type : selectTypes) {
                url += "&selectTypes=" + type;
            }
        }
        if (selectSpecies != null) {
            for (String species : selectSpecies) {
                url += "&selectSpecies=" + species;
            }
        }
        if (selectComIndicators != null) {
            for (String selectComIndicator : selectComIndicators) {
                url += "&selectComIndicators=" + selectComIndicator;
            }
        }
        if (selectPopIndicators != null) {
            for (String selectPopIndicator : selectPopIndicators) {
                url += "&selectPopIndicators=" + selectPopIndicator;
            }
        }
        return url;
    }

    @Action(value = "extract-download",
            results = {
                    @Result(name = "wait", location = "/WEB-INF/content/search/extract-wait.jsp"),
                    @Result(type = "stream", params = {"contentType", "application/zip", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String download() {

        Locale locale = (Locale) servletRequest.getSession().getAttribute(LOCALE_ATTRIBUTE);

        ExtractRawDataAndResultsRequest request =
                requestBuilder(locale, ExtractRawDataAndResultsRequest.class).
                        addZoneList(selectZones).
                        addExtractTypeList(selectTypes).
                        addSpeciesList(selectSpecies).
                        addCommunityIndicatorList(selectComIndicators).
                        addPopulationIndicatorList(selectPopIndicators).
                        toRequest();

        resultFile = getService().extractRawDataAndResults(locale, request);
        return SUCCESS;
    }

    public InputStream getInputStream() {
        InputStream is = resultFile.getInputStream();
        return is;
    }

    public String getFilename() {
        return "Indicateurs_Ifremer.zip";
    }

    public boolean isNeedSpeciesOrIndicators() {
        return CollectionUtils.isNotEmpty(selectTypes) &&
               (DataType.isNeedSpecies(selectTypes) || DataType.isIndicator(selectTypes));
    }

}

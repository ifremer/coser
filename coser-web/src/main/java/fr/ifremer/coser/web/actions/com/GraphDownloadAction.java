/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.com;

import fr.ifremer.coser.result.request.GetCommunityIndicatorResultDataRequest;
import fr.ifremer.coser.result.request.GetSpeciesListForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.result.FileResult;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.util.Map;

/**
 * Télécharge les données qui ont servi a généré le graph au format CSV.
 *
 * Parametre : zone, indicator.
 *
 * @author chatellier
 */
public class GraphDownloadAction extends AbstractCoserJspAction {

    private static final long serialVersionUID = 3385467755357775199L;

    protected String facade;

    protected String zone;

    protected String indicator;

    /** La liste actuellement selectionnée (cas null géré). */
    protected String list;

    protected FileResult result;

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getSource() {
        return result.getSource();
    }

    @Action(results = {@Result(type = "stream", params = {"contentType", "application/zip", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String execute() {

        GetCommunityIndicatorResultDataRequest request =
                requestBuilder(GetCommunityIndicatorResultDataRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        addIndicator(indicator).
                        addSpecies(list).
                        toRequest();

        if (StringUtils.isEmpty(list)) {

            // Get the first species found in indicators file
            GetSpeciesListForCommunityIndicatorResultRequest speciesRequest =
                    requestBuilder(GetSpeciesListForCommunityIndicatorResultRequest.class).
                            addFacade(facade).
                            addZone(zone).
                            addIndicator(indicator).
                            toRequest();
            Map<String, String> speciesMap = getService().toMap(speciesRequest);
            if (MapUtils.isNotEmpty(speciesMap)) {
                request.setSpecies(speciesMap.keySet().iterator().next());
            }
        }

        result = getService().toFileResult(request);
        return SUCCESS;
    }

    public String getFilename() {
        return indicator + ".zip";
    }

    public InputStream getInputStream() {
        InputStream input = result.getInputStream();
        return input;
    }

}

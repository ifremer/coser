/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.pop;

import fr.ifremer.coser.result.request.GetSpeciesForPopulationIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetZonesForPopulationIndicatorResultRequest;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

/**
 * Action index, recupere la liste des resultats.
 *
 * @author chatellier
 */
public class SpeciesAction extends AbstractCoserJspAction {

    private static final long serialVersionUID = 1663244944108703571L;

    private static final Log log = LogFactory.getLog(SpeciesAction.class);

    protected String facade;

    protected String zone;

    protected Map<String, String> species;

    protected String zonePicture;

    public String getFacade() {
        return facade;
    }

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Map<String, String> getSpeciesList() {
        return species;
    }

    public String getZonePicture() {
        return zonePicture;
    }

    public String getFacadeDisplayName() {
        return getService().getFacadeDisplayName(facade);
    }

    public String getZoneDisplayName() {
        GetZonesForPopulationIndicatorResultRequest request =
                requestBuilder(GetZonesForPopulationIndicatorResultRequest.class).
                        addFacade(facade).
                        toRequest();
        return getService().getZoneDisplayName(request, zone);
    }

    @Override
    public String execute() {

        if (log.isInfoEnabled()) {
            log.info("Looking for species for zone " + zone);
        }

        GetSpeciesForPopulationIndicatorResultRequest request =
                requestBuilder(GetSpeciesForPopulationIndicatorResultRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        toRequest();
        species = getService().toMap(request);

        if (log.isDebugEnabled()) {
            log.debug("Species are : " + species);
        }

        zonePicture = getService().getZonePictures().get(zone);

        return SUCCESS;
    }

}

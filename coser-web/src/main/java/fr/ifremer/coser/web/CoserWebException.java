/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web;

import fr.ifremer.coser.CoserTechnicalException;

/**
 * Coser web runtime exception.
 *
 * @author chatellier
 */
public class CoserWebException extends CoserTechnicalException {


    private static final long serialVersionUID = -1002725698959514244L;

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message the detail message
     */
    public CoserWebException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   the cause
     */
    public CoserWebException(String message, Throwable cause) {
        super(message, cause);
    }

}

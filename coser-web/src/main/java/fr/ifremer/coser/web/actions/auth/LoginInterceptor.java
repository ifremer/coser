/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2011 Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.auth;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

/**
 * Authentication interceptor.
 *
 * @author chatellier
 */
public class LoginInterceptor extends AbstractInterceptor {

    private static final long serialVersionUID = 1L;

    public static final String SESSION_PARAMETER_LOGIN = "login";

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        ActionContext context = invocation.getInvocationContext();
        Map<String, Object> session = context.getSession();
        Object login = session.get(SESSION_PARAMETER_LOGIN);
        String result;
        if (login == null) {
            // do login
            result = "redirect-login";
        } else {
            result = invocation.invoke();
        }
        return result;
    }
}

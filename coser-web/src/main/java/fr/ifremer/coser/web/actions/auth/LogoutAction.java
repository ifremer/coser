/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2011 Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.auth;

import fr.ifremer.coser.web.actions.common.AbstractCoserAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

/**
 * Logout action.
 *
 * @author tony chemit - chemit@codelutin.com
 */
@Result(type = "redirect", location = "/index")
public class LogoutAction extends AbstractCoserAction implements SessionAware {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(LogoutAction.class);

    protected transient Map<String, Object> session;

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    @Override
    public String execute() throws Exception {
        Object login = session.remove(LoginInterceptor.SESSION_PARAMETER_LOGIN);
        if (login != null) {
            if (log.isInfoEnabled()) {
                log.info("Logout for user: " + login);
            }
        }
        return SUCCESS;
    }
}

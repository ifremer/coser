/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.source;

import fr.ifremer.coser.result.request.ExtractRawDataRequest;
import fr.ifremer.coser.result.result.FileResult;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;

/**
 * Force le téléchargement du zip.
 *
 * @author chatellier
 */
public class SourceDataAction extends SourceAction {

    private static final long serialVersionUID = 3385467755357775199L;

    protected boolean accepted;

    protected FileResult result;

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    // used by validator
    public boolean isAccepted() {
        return accepted;
    }

    @Action(results = {@Result(type = "stream", params = {"contentType", "application/zip", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String execute() {

        ExtractRawDataRequest request = requestBuilder(ExtractRawDataRequest.class).
                addFacade(facade).
                addZone(zone).
                toRequest();
        result = getService().toFirstFileResult(request);
        return SUCCESS;
    }

    public String getFilename() {
        return "source.zip";
    }

    public InputStream getInputStream() {
        return result.getInputStream();
    }

}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions;

import fr.ifremer.coser.web.CoserWebConfig;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.nuiton.util.StringUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * Upload results action.
 *
 * Cette action est appelée par l'interface swing cliente.
 *
 * @author chatellier
 */
public class UploadResultAction extends AbstractCoserJspAction {


    private static final long serialVersionUID = 3887268253160622587L;

    private static final Log log = LogFactory.getLog(UploadResultAction.class);

    protected File resultFile;

    protected String login;

    protected String sha1Password;

    public File getResultFile() {
        return resultFile;
    }

    public void setResultFile(File resultFile) {
        this.resultFile = resultFile;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setSha1Password(String sha1Password) {
        this.sha1Password = sha1Password;
    }

    public String getSha1Password() {
        return sha1Password;
    }

    @Override
    public String execute() {

        if (log.isInfoEnabled()) {
            log.info("Result action called");
        }

        // check 
        CoserWebConfig config = getService().getConfig();
        if (config.getAdminPassword() == null || config.getAdminLogin() == null) {
            if (log.isWarnEnabled()) {
                log.warn("No admin password set, cannot enable result upload");
            }
        } else {

            if (config.getAdminLogin().equals(login) && equalsSHA1Password(config, sha1Password)) {
                if (resultFile != null) {
                    getService().registerNewUploadedResults(login, resultFile);
                    return SUCCESS;
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("File is null");
                    }
                }
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Wrong login/password : login = " + login);
                }
                HttpServletResponse response = ServletActionContext.getResponse();
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        }

        return INPUT;
    }

    /**
     * Check if sha1 password equals to config password.
     *
     * Config password can be plain or sha1 encoded.
     *
     * @param config       config
     * @param sha1Password sha1 to check
     * @return equality
     */
    protected boolean equalsSHA1Password(CoserWebConfig config, String sha1Password) {

        // first test sha1 equality
        String configSha1Password = config.getAdminPassword();
        boolean result = configSha1Password.equals(sha1Password);

        // second test to encode sha1 of plain password
        if (!result) {
            configSha1Password = StringUtil.encodeSHA1(configSha1Password);
            result = configSha1Password.equals(sha1Password);
        }

        return result;
    }
}

package fr.ifremer.coser.web;

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.coser.CoserBusinessConfig;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;

import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * To generate configuration report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserWebConfigProvider implements ApplicationConfigProvider {

    @Override
    public String getName() {
        return "coserWeb";
    }

    @Override
    public String getDescription(Locale locale) {
        return l(locale, "coser.config.coserWeb.configuration.description");
    }

    @Override
    public ConfigOptionDef[] getOptions() {
        List<ConfigOptionDef> options = Lists.<ConfigOptionDef>newArrayList(CoserWebConfig.CoserWebOption.values());
        options.addAll(Lists.newArrayList(CoserBusinessConfig.CoserBusinessOption.values()));
        return options.toArray(new ConfigOptionDef[options.size()]);
    }

    @Override
    public ConfigActionDef[] getActions() {
        return new ConfigActionDef[0];
    }
}

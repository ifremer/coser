package fr.ifremer.coser.web;

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.coser.bean.ZoneMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Date;
import java.util.Map;

/**
 * To listen start and end of the application.
 *
 * On start we will init the application context ({@link CoserWebApplicationContext#init()}).
 *
 * On stop, just release close the application context ({@link CoserWebApplicationContext#close()}).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CoserApplicationListener implements ServletContextListener {

    /** Logger. */
    protected static final Log log =
            LogFactory.getLog(CoserApplicationListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        if (log.isInfoEnabled()) {
            log.info("Application starting at " + new Date() + "...");
        }
        CoserWebApplicationContext.init();

        CoserWebApplicationContext coserWebApplicationContext = CoserWebApplicationContext.get();

        checkConfiguration(coserWebApplicationContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (log.isInfoEnabled()) {
            log.info("Application is ending at " + new Date() + "...");
        }
        CoserWebApplicationContext.close();
    }

    protected void checkConfiguration(CoserWebApplicationContext coserWebApplicationContext) {


        // check that zone pictures are sane
        ZoneMap zoneMap = coserWebApplicationContext.getZoneMap();

        Map<String, String> zonePictures = zoneMap.getZonePictures();
        for (Map.Entry<String, String> entry : zonePictures.entrySet()) {

            if (StringUtils.isBlank(entry.getValue())) {

                if (log.isErrorEnabled()) {
                    log.error(String.format("Zone with no picture: %s", entry.getKey()));
                }
            }
        }

    }
}

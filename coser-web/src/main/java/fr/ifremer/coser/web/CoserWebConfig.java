/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web;

import fr.ifremer.coser.CoserBusinessConfig;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.util.Version;

import static org.nuiton.i18n.I18n.n;

/**
 * Coser web configuration.
 *
 * @author chatellier
 */
public class CoserWebConfig extends CoserBusinessConfig {

    public CoserWebConfig() {
        // init configuration with default options
        loadDefaultOptions(CoserWebOption.values());
    }

    public String getApplicationVersion() {
        String result = getOption(CoserWebOption.APPLICATION_VERSION.key);
        return result;
    }

    /**
     * Get administrator email.
     *
     * @return admin email
     */
    public String getAdminEmail() {
        String result = getOption(CoserWebOption.ADMIN_EMAIL.key);
        return result;
    }

    /**
     * Get admin login.
     *
     * @return admin login
     */
    public String getAdminLogin() {
        String result = getOption(CoserWebOption.ADMIN_LOGIN.key);
        return result;
    }

    /**
     * Get admin password.
     *
     * @return admin password
     */
    public String getAdminPassword() {
        String result = getOption(CoserWebOption.ADMIN_PASSWORD.key);
        return result;
    }

    /**
     * Get analytics id.
     *
     * @return analytics id
     */
    public String getAnalyticsId() {
        String result = getOption(CoserWebOption.ANALYTICS_ID.key);
        return result;
    }

    public enum CoserWebOption implements ConfigOptionDef {

        /** Context name for multiple deployment. */
        CONTEXT_NAME(APP_NAME, null, String.class, "coser"),
        // see : http://www.nuiton.org/issues/1862
        ENCODING_HACK(CONTEXT_NAME.getDefaultValue() + "." + CONFIG_ENCODING, null, String.class, "UTF-8"),
        CONFIG_FILE(CONTEXT_NAME.defaultValue + "." + CONFIG_FILE_NAME, n("coser.config.config.file.description"), String.class, "coserweb.properties"),
        APPLICATION_VERSION("coser.application.version", n("coser.config.application.version.description"), Version.class, null),
        ADMIN_EMAIL("coser.admin.email", n("coser.config.config.file.description"), String.class, "harmonie@ifremer.fr"),
        ADMIN_LOGIN("coser.admin.login", n("coser.config.admin.login.description"), String.class, null),
        ADMIN_PASSWORD("coser.admin.password", n("coser.config.admin.password.description"), String.class, null),
        ANALYTICS_ID("coser.analytics.id", n("coser.config.analytics.id.description"), String.class, "UA-27739588-1");

        private final String key;

        private final String description;

        private final String defaultValue;

        private final Class<?> type;

        CoserWebOption(String key, String description, Class<?> type, String defaultValue) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isTransient() {
            return false;
        }

        @Override
        public boolean isFinal() {
            return false;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            // not used
        }

        @Override
        public void setTransient(boolean isTransient) {
            // not used
        }

        @Override
        public void setFinal(boolean isFinal) {
            // not used
        }

        public String getDescription() {
            return description;
        }

        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }
}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.pop;

import fr.ifremer.coser.result.CoserRequest;
import fr.ifremer.coser.result.request.GetIndicatorsForPopulationIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetSpeciesForPopulationIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetZonesForPopulationIndicatorResultRequest;
import fr.ifremer.coser.web.actions.common.CommonIndicator;

/**
 * Action index, recupere la liste des resultats.
 *
 * @author chatellier
 */
public class IndicatorAction extends CommonIndicator {

    private static final long serialVersionUID = 1663244944108703571L;

    protected String species;

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    @Override
    protected CoserRequest createZonesRequest() {
        GetZonesForPopulationIndicatorResultRequest request =
                requestBuilder(GetZonesForPopulationIndicatorResultRequest.class).
                        addFacade(facade).
                        toRequest();
        return request;
    }

    @Override
    protected GetIndicatorsForPopulationIndicatorResultRequest createIndicatorsRequest() {
        GetIndicatorsForPopulationIndicatorResultRequest request =
                requestBuilder(GetIndicatorsForPopulationIndicatorResultRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        addSpecies(species).
                        toRequest();
        return request;
    }

    public String getSpeciesDisplayName() {
        GetSpeciesForPopulationIndicatorResultRequest request =
                requestBuilder(GetSpeciesForPopulationIndicatorResultRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        toRequest();
        return getService().getSpeciesDisplayName(request, species);
    }
}

/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions;

import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Action index, recupere la date de derniere mise à jour.
 *
 * @author chatellier
 */
public class IndexAction extends AbstractCoserJspAction {

    private static final long serialVersionUID = 1663244944108703571L;

    protected Date dataUpdateDate;

    public Date getDataUpdateDate() {
        return dataUpdateDate;
    }

    /**
     * Return l'url de l'application context inclut.
     *
     * @return l'url du context
     */
    public String getContextUrl() {
        HttpServletRequest request = ServletActionContext.getRequest();

        String url = request.getScheme() + "://" + request.getServerName();
        if (request.getServerPort() != 80) {
            url += ":" + request.getServerPort();
        }
        url += request.getContextPath();

        return url;
    }

    @Override
    public String execute() {

        dataUpdateDate = getService().getConfig().getLastDataUpdateDate();
        return SUCCESS;

    }

}

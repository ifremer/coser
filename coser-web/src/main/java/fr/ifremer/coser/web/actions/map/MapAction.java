/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.map;

import fr.ifremer.coser.result.repository.ResultRepositoryType;
import fr.ifremer.coser.result.repository.echobase.EchoBaseResultRepositoryType;
import fr.ifremer.coser.result.request.GetMatchingRepositoryTypeForMapResultRequest;
import fr.ifremer.coser.result.request.GetSpeciesForMapResultRequest;
import fr.ifremer.coser.result.request.GetZonesForMapResultRequest;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Action index, recupere la liste des resultats.
 *
 * @author chatellier
 */
public class MapAction extends AbstractCoserJspAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MapAction.class);

    private static final long serialVersionUID = 1663244944108703571L;

    protected String facade;

    protected String zone;

    protected String species;

    protected ResultRepositoryType matchingrepositoryType;

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getFacade() {
        return facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getFacadeDisplayName() {
        return getService().getFacadeDisplayName(facade);
    }

    public String getZoneDisplayName() {
        GetZonesForMapResultRequest request =
                requestBuilder(GetZonesForMapResultRequest.class).
                        addFacade(facade).
                        toRequest();
        return getService().getZoneDisplayName(request, zone);
    }

    public String getSpeciesDisplayName() {
        GetSpeciesForMapResultRequest request =
                requestBuilder(GetSpeciesForMapResultRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        toRequest();
        return getService().getSpeciesDisplayName(request, species);
    }

    public boolean isEchobaseResult() {
        return getMatchingrepositoryType().getId().equals(EchoBaseResultRepositoryType.ID);
    }

    protected ResultRepositoryType getMatchingrepositoryType() {
        if (matchingrepositoryType == null) {
            GetMatchingRepositoryTypeForMapResultRequest request =
                    requestBuilder(GetMatchingRepositoryTypeForMapResultRequest.class).
                            addFacade(facade).
                            addZone(zone).
                            addSpecies(species).
                            toRequest();
            try {
                matchingrepositoryType = getService().getMatchingRepositoryType(request);
            } catch (Exception e) {
                log.error("Error getting matching repository type", e);
            }
        }
        return matchingrepositoryType;
    }

}

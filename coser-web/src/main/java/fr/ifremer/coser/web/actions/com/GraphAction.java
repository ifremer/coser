/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.com;

import fr.ifremer.coser.result.request.GetIndicatorsForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetSpeciesListForCommunityIndicatorResultRequest;
import fr.ifremer.coser.result.request.GetZonesForCommunityIndicatorResultRequest;
import fr.ifremer.coser.web.actions.common.AbstractCoserJspAction;

import java.util.Map;

/**
 * Affiche le graphique demandé.
 *
 * Parametre : zone, species, indicator.
 *
 * @author chatellier
 */
public class GraphAction extends AbstractCoserJspAction {

    private static final long serialVersionUID = 3385467755357775199L;

    protected String facade;

    protected String zone;

    protected String indicator;

    /** La liste des nom de liste du fichier de communauté pour l'indicateur. */
    protected Map<String, String> lists;

    /** La liste actuellement selectionnée (cas null géré). */
    protected String list;

    public String getFacade() {
        return facade;
    }

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public Map<String, String> getLists() {
        return lists;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getFacadeDisplayName() {
        return getService().getFacadeDisplayName(facade);
    }

    public String getZoneDisplayName() {
        GetZonesForCommunityIndicatorResultRequest request =
                requestBuilder(GetZonesForCommunityIndicatorResultRequest.class).
                        addFacade(facade).
                        toRequest();
        return getService().getZoneDisplayName(request, zone);
    }

    public String getIndicatorDisplayName() {
        GetIndicatorsForCommunityIndicatorResultRequest request =
                requestBuilder(GetIndicatorsForCommunityIndicatorResultRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        toRequest();
        return getService().getIndicatorDisplayName(request, indicator);
    }

    @Override
    public String execute() {
        GetSpeciesListForCommunityIndicatorResultRequest request =
                requestBuilder(GetSpeciesListForCommunityIndicatorResultRequest.class).
                        addFacade(facade).
                        addZone(zone).
                        addIndicator(indicator).
                        toRequest();
        lists = getService().toMap(request);
        return SUCCESS;
    }

}

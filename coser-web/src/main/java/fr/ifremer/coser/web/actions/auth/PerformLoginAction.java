/*
 * #%L
 * Coser :: Web
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.coser.web.actions.auth;

import fr.ifremer.coser.web.CoserWebConfig;
import fr.ifremer.coser.web.actions.common.AbstractCoserAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.nuiton.util.StringUtil;

import java.util.Map;

/**
 * Perform login action.
 *
 * @author chatellier
 */
@Result(type = "redirect", location = "/admin/index")
public class PerformLoginAction extends AbstractCoserAction implements SessionAware {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(PerformLoginAction.class);

    protected String login;

    protected String password;

    protected transient Map<String, Object> session;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    @Override
    public String execute() throws Exception {
        CoserWebConfig config = getService().getConfig();
        String result;
        if (config.getAdminLogin().equals(login) && equalsSHA1Password(config, password)) {
            if (log.isInfoEnabled()) {
                log.info("Successfull login: " + login);
            }
            session.put(LoginInterceptor.SESSION_PARAMETER_LOGIN, login);

            result = SUCCESS;
        } else {
            addActionError("Invalid login/password");
            result = INPUT;
        }
        return result;
    }

    /**
     * Check if sha1 password equals to config password.
     *
     * Config password can be plain or sha1 encoded.
     *
     * @param config   config
     * @param password password to check
     * @return equality
     */
    protected boolean equalsSHA1Password(CoserWebConfig config, String password) {

        // first test sha1 equality
        String configSha1Password = config.getAdminPassword();
        String sha1Password = StringUtil.encodeSHA1(password);
        boolean result = configSha1Password.equals(sha1Password);

        // second test to encode sha1 of plain password
        if (!result) {
            configSha1Password = StringUtil.encodeSHA1(configSha1Password);
            result = configSha1Password.equals(sha1Password);
        }

        return result;
    }

}  
